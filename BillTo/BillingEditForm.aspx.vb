Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class BillingEditForm
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtname As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtCityName As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtSubmissionCityName As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtFreq As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtadd As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtContact As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtDepartment As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlcompany As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtemail As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtarearemarks As System.Web.UI.WebControls.TextBox

    Protected WithEvents btnproceed As System.Web.UI.WebControls.Button
    Protected WithEvents btnr As System.Web.UI.WebControls.Button
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents ddldiscount As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddldiscountdecimal As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlpayment As System.Web.UI.WebControls.DropDownList
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnReset As System.Web.UI.WebControls.Button
    Protected WithEvents pnl2ndform As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    Protected WithEvents ddCCardType As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtCCN As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtExpDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents Panel2 As System.Web.UI.WebControls.Panel
    Protected WithEvents pnlCc As System.Web.UI.WebControls.Panel
    Protected WithEvents btnReset1 As System.Web.UI.WebControls.Button
    Protected WithEvents hidCCN As System.Web.UI.WebControls.TextBox

    Protected WithEvents txtEscalation As System.Web.UI.WebControls.TextBox

    Protected WithEvents txtCCNTemp As System.Web.UI.WebControls.TextBox
    Protected WithEvents Checkbox1 As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkIsVIP As System.Web.UI.WebControls.CheckBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Dim CCNoNew As String
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        btnSubmit.Attributes("onClick") = "return firstpage();"

        If Not Page.IsPostBack Then
            populateddl()
            Dim dtrreader As SqlDataReader
            Dim accessdata As clsutility
            accessdata = New clsutility
            Dim valDisc As String
            Dim valDiscDec As String
            Dim arrDisc As Array

            dtrreader = accessdata.funcGetSQLDataReader(" select POD.*,C.ClientCoName from CorIntPODDetails POD, CORIntClientCoMaster  C where C.clientcoid=POD.clientcoid and POD.PODID =" & Request.QueryString("id") & " ")
            dtrreader.Read()



            txtname.Text = dtrreader("SubmissionName") & ""
            autoselec_ddl(ddlcompany, dtrreader("ClientCoID"))

            txtCityName.Text = dtrreader("CityName") & ""
            txtSubmissionCityName.Text = dtrreader("SubmissionLocation") & ""
            txtFreq.Text = dtrreader("Frequency_Submission") & ""
            txtadd.Text = dtrreader("BillingAddress") & ""

            txtContact.Text = dtrreader("ContactNo") & ""
            txtDepartment.Text = dtrreader("Department") & ""
            txtEmail.Text = dtrreader("EmailID") & ""
            txtarearemarks.Text = dtrreader("Remarks") & ""

            dtrreader.Close()
            accessdata.Dispose()
        End If
    End Sub
    Sub populateddl()
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility
        ddlcompany.DataSource = objAcessdata.funcGetSQLDataReader("select ClientCoName ,ClientCoID   from CORIntClientCoMaster order by ClientCoName ")
        ddlcompany.DataValueField = "ClientCoID"
        ddlcompany.DataTextField = "ClientCoName"
        ddlcompany.DataBind()
        ddlcompany.Items.Insert(0, New ListItem("", ""))
        objAcessdata.Dispose()
    End Sub
   
    Function autoselec_ddl(ByVal ddlname As DropDownList, ByVal selectvalue As Int32)
        If Not IsDBNull(selectvalue) Then
            If Not selectvalue = 0 Then
                ddlname.Items.FindByValue(selectvalue).Selected = True
            Else
                ddlname.SelectedValue = 0
            End If
        End If
    End Function


    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        ' Code added by Binary Semantics on 29-March-07
        ' Call function to check for email exist or not

        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand

        Dim intuniqcheck As SqlParameter
        Dim intuniqvalue As Int32
        Dim intInc As Int32

        Dim ChrMod As String

        cmd = New SqlCommand("procEditBilling", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@rowid", Request.QueryString("id"))
        cmd.Parameters.Add("@name", txtname.Text)
        cmd.Parameters.Add("@clientcoid", ddlcompany.SelectedItem.Value)
        cmd.Parameters.Add("@CityName", txtCityName.Text)
        cmd.Parameters.Add("@SubmissionCity", txtSubmissionCityName.Text)
        cmd.Parameters.Add("@Frequency", txtFreq.Text)
        cmd.Parameters.Add("@BillAddress", txtadd.Text)
        cmd.Parameters.Add("@ContactNo", txtContact.Text)
        cmd.Parameters.Add("@Department", txtDepartment.Text)
        cmd.Parameters.Add("@emailid", txtemail.Text)
        cmd.Parameters.Add("@remarks", txtarearemarks.Text)
        cmd.Parameters.Add("@ModifiedBy", Session("loggedin_user"))
        intuniqcheck = cmd.Parameters.Add("@uniqcheckval", SqlDbType.Int)
        intuniqcheck.Direction = ParameterDirection.Output

        'Try
        MyConnection.Open()
        cmd.ExecuteNonQuery()
        intuniqvalue = cmd.Parameters("@uniqcheckval").Value
        MyConnection.Close()
        If Not intuniqvalue = 0 Then
            lblErrorMsg.Visible = True
            lblErrorMsg.Text = "Client company Billing Submission Details already exist."
            Exit Sub
        Else

            lblErrorMsg.Visible = False

            ' Code commented by Binary Semantics on 28-March-07
            pnlmainform.Visible = False
            'pnl2ndform.Visible = False

            pnlconfirmation.Visible = True

            lblMessage.Text = "You have Updated the Client Company Billing Submission Details successfully"
            hyplnkretry.Text = "Edit another Client Company Billing Submission Details "
            hyplnkretry.NavigateUrl = "BillingEditSearch.aspx"
            '   Catch
            '   End Try
        End If
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function

End Class
