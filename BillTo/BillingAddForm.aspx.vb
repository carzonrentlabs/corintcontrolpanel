Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Mail
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System

Public Class BillingAddForm
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtname As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtCityName As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtSubmissionCityName As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtFreq As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtadd As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtContact As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtDepartment As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlcompany As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtemail As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtarearemarks As System.Web.UI.WebControls.TextBox
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnReset As System.Web.UI.WebControls.Button
    Protected WithEvents pnl2ndform As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    Protected WithEvents hdndcoval As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    Protected WithEvents btnproceed As System.Web.UI.WebControls.Button
    Protected WithEvents btnr As System.Web.UI.WebControls.Button
    Protected WithEvents pnlCc As System.Web.UI.WebControls.Panel


    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Code added by BSL on 03-April-07, define event on company combo
        If Not Page.IsPostBack Then
            populateddl()
        End If
    End Sub

    Sub populateddl()
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility
        ddlcompany.DataSource = objAcessdata.funcGetSQLDataReader("select ClientCoName, ClientCoID from CORIntClientCoMaster where active=1 order by ClientCoName ")
        ddlcompany.DataValueField = "ClientCoID"
        ddlcompany.DataTextField = "ClientCoName"
        ddlcompany.DataBind()
        ddlcompany.Items.Insert(0, New ListItem("", ""))
    End Sub

    Function autoselec_ddl(ByVal ddlname As DropDownList, ByVal selectvalue As Int32)
        If Not IsDBNull(selectvalue) Then
            If Not selectvalue = 0 Then
                ddlname.Items.FindByValue(selectvalue).Selected = True
            Else
                ddlname.SelectedValue = 0
            End If
        End If
    End Function

    Private Sub btnr_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Response.Redirect("BillingAddForm.aspx")
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("BillingAddForm.aspx")
    End Sub


    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand

        Dim intuniqcheck As SqlParameter
        Dim intuniqvalue As Int32
        Dim intInc As Int32

        Dim intPODIDParam As SqlParameter
        Dim intPODID As Int32
        cmd = New SqlCommand("procAddBilling", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("@name", txtname.Text)
        cmd.Parameters.Add("@clientcoid", ddlcompany.SelectedItem.Value)
        cmd.Parameters.Add("@CityName", txtCityName.Text)
        cmd.Parameters.Add("@SubmissionCity", txtSubmissionCityName.Text)
        cmd.Parameters.Add("@Frequency", txtFreq.Text)
        cmd.Parameters.Add("@BillAddress", txtadd.Text)
        cmd.Parameters.Add("@ContactNo", txtContact.Text)
        cmd.Parameters.Add("@Department", txtDepartment.Text)
        cmd.Parameters.Add("@emailid", txtemail.Text)
        cmd.Parameters.Add("@remarks", txtarearemarks.Text)
        cmd.Parameters.Add("@CreatedBY", Session("loggedin_user"))
        intuniqcheck = cmd.Parameters.Add("@uniqcheckval", SqlDbType.Int)
        intuniqcheck.Direction = ParameterDirection.Output
        intPODIDParam = cmd.Parameters.Add("@PODID", SqlDbType.Int)
        intPODIDParam.Direction = ParameterDirection.Output

        '  Try
        MyConnection.Open()
        cmd.ExecuteNonQuery()

        intuniqvalue = cmd.Parameters("@uniqcheckval").Value

        If intuniqvalue = 0 Then
            intPODID = cmd.Parameters("@PODID").Value
        End If


        MyConnection.Close()

        If Not intuniqvalue = 0 Then
            lblErrorMsg.Visible = True
            lblErrorMsg.Text = "Client Billing Submission Details already exists."
            Exit Sub
        Else
            lblErrorMsg.Visible = False
            pnlmainform.Visible = False
            pnlconfirmation.Visible = True

            lblMessage.Text = "You have added the Client Billing Submission Details successfully"
            hyplnkretry.Text = "Add another Client Billing Submission Details"
            hyplnkretry.NavigateUrl = "BillingAddForm.aspx"
            '   Catch
            '   End Try
        End If
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function

End Class
