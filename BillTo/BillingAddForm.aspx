<%@ Page Language="vb" AutoEventWireup="false" Inherits="BillingAddForm" src="BillingAddForm.aspx.vb" %>
<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
<title>CarzonRent :: Internal software</title>
<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
<meta content="JavaScript" name="vs_defaultClientScript">
<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
<script language="JavaScript" src="../utilityfunction.js"></script>
<script language="JavaScript" src="../JScripts/Datefunc.js"></script>
<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
<script language="javascript">
function firstpage()
{

	var txtname = document.forms[0].txtname.value; 
	txtname = txtname.replace(/^\s+/,""); 	//Removes Left Blank Spaces
	txtname = txtname.replace(/\s+$/,""); 	//Removes Right Blank Spaces
	
	if (txtname == "")
	{
		alert("Please Enter first name");
		document.forms[0].txtname.focus();
		return false;
	}

	if (document.forms[0].txtCityName.value == "")
	{
		alert("Please Enter City name");
		document.forms[0].txtCityName.focus();
		return false;
	}
	
	if (document.forms[0].txtSubmissionCityName.value == "")
	{
		alert("Please Enter Submission City name");
		document.forms[0].txtSubmissionCityName.focus();
		return false;
	}
	
	if (document.forms[0].txtFreq.value == "")
	{
		alert("Please Enter Frequecy");
		document.forms[0].txtFreq.focus();
		return false;
	}
	
	if (document.forms[0].txtadd.value == "")
	{
		alert("Please Enter Addresss");
		document.forms[0].txtadd.focus();
		return false;
	}

	if (document.forms[0].txtContact.value == "")
	{
		alert("Please Enter Contact No.");
		document.forms[0].txtContact.focus();
		return false;
	}
	
	if (document.forms[0].txtDepartment.value == "")
	{
		alert("Please Enter Department name");
		document.forms[0].txtDepartment.focus();
		return false;
	}		
		
	if(document.forms[0].txtemail.value!="")
	{
		var theStr=document.forms[0].txtemail.value;
		var atIndex = theStr.indexOf('@'); 
		var dotIndex = theStr.indexOf('.', atIndex); 
		theSub = theStr.substring(0, dotIndex+1) 

		if ((atIndex < 1)||(atIndex != theStr.lastIndexOf('@'))||(dotIndex < atIndex + 2)||(theStr.length <= theSub.length)) 
		{ 
			alert("Email ID is not a valid Email ID");
			return false;
		}
	}
	
	var strvalues
	strvalues = ('ddlcompany,txtadd,txtemail')
	return checkmandatory(strvalues);
}
	
</script>
</HEAD>
<body>
<form id="Form1" method="post" runat="server">
<uc1:headerctrl id="Headerctrl1" runat="server"></uc1:headerctrl>
<table align="center">
	<asp:panel id="pnlmainform" Runat="server">
		<TBODY>
			<TR>
			  <TD align="center" colSpan="2"><B><U>Add Billing Submission Details</U></B>
					<BR>
					<BR>
			  </TD>
			</TR>
			<TR>
				<TD align="center" colSpan="2">
					<asp:Label id="lblErrorMsg" runat="server" visible="false" cssclass="subRedHead"></asp:Label></TD>
			</TR>
			<TR>
				<TD>* Submission Name</TD>
				<TD>
					<asp:textbox id="txtname" runat="server" size="30" CssClass="input"></asp:textbox></TD>
			</TR>
			<TR>
				<TD>* Client Company</TD>
				<TD>
				<asp:DropDownList id="ddlcompany" runat="server" cssclass="input">
				</asp:DropDownList>
				</TD>
			</TR>
			<TR>
				<TD>* Client City</TD>
				<TD>
				<asp:textbox id="txtCityName" runat="server" size="30" CssClass="input"></asp:textbox>
				</TD>
			</TR>
			<TR>
				<TD>* Submission City</TD>
				<TD>
				<asp:textbox id="txtSubmissionCityName" runat="server" size="30" CssClass="input"></asp:textbox>
				</TD>
			</TR>
			<TR>
				<TD>Frequency</TD>
				<TD>
					<asp:textbox id="txtFreq" runat="server" size="30" CssClass="input"></asp:textbox></TD>
			</TR>
			<TR>
				<TD>* Billing Address</TD>
				<TD>
					<asp:textbox id="txtadd" size="30" runat="server" MaxLength="2000" CssClass="input" Rows="3" Columns="40" TextMode="MultiLine"></asp:textbox>
				</TD>
			</TR>
			<TR>
				<TD>* Contact No</TD>
			  <TD><asp:TextBox ID="txtContact" runat="server" size="30" CssClass="input"></asp:TextBox></TD>
			</TR>
			<TR>
				<TD>* Department</TD>
				<TD>
					<asp:textbox id="txtDepartment" runat="server" size="30" CssClass="input">
					</asp:textbox>
				</TD>
			</TR>
			<TR>
				<TD>* Email ID</TD>
				<TD>
					<asp:textbox id="txtEmail" runat="server" size="30" CssClass="input"></asp:textbox></TD>
			</TR>
			<TR>
				<TD valign="top">Remarks(<SPAN class="shwText" id="shwMessage">0</SPAN>/2000 chars)</TD>
				<TD>
					<asp:textbox id="txtarearemarks" size="30" runat="server" MaxLength="2000" CssClass="input" Rows="3" Columns="40" TextMode="MultiLine"></asp:textbox>
			</TD>
			</TR>
			<TR>
				<TD align="center" colSpan="2">
					<asp:button id="btnSubmit" runat="server" CssClass="input" Text="Submit"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;
					<asp:button id="btnReset" runat="server" CssClass="input" Text="Reset"></asp:button></TD>
			</TR>
	</asp:panel>
	<asp:panel id="pnlconfirmation" Runat="server" Visible="False">
		<TR align="center">
			<TD colSpan="2">
				<asp:Label id="lblMessage" runat="server"></asp:Label></TD>
		</TR>
		<TR align="center">
			<TD colSpan="2">
				<asp:HyperLink id="hyplnkretry" runat="server"></asp:HyperLink></TD>
		</TR>
	</asp:panel>
	</TBODY>
</table>
</form>
</body>
</HTML>