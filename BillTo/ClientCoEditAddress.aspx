<%@ Page Language="vb" AutoEventWireup="false" Src="ClientCoEditAddress.aspx.vb" Inherits="ClientCoEditAddress"%>
<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CarzonRent :: Internal software Control panel</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="JavaScript" src="../utilityfunction.js"></script>
		<script language="JavaScript" src="../JScripts/Datefunc.js"></script>
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
		<script language="javascript">
		function validation()
		{
			var strvalues
			strvalues=('txtaddress')
			return checkmandatory(strvalues);		
		}		

		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<uc1:Headerctrl id="Headerctrl1" runat="server"></uc1:Headerctrl>
			<TABLE id="Table1" align="center">
				<asp:panel id="pnlmainform" Runat="server">
					<TBODY>
						<TR>
							<TD align="center" colSpan="2"><STRONG><U>Additional address</U></STRONG>
							</TD>
						</TR>
						<tr><td colspan="2" align="center"><asp:Label id="lblErrorMsg" cssclass="subRedHead" visible="false" runat="server"></asp:Label></td></tr>
						<TR>
							<TD align="center" colSpan="2">
								<asp:Label id="lblerr" CssClass="subRedHead" runat="server" Visible="False"></asp:Label></TD>
						</TR>
						<TR>
							<TD> Address ID</TD>
							<TD>
							<asp:textbox id="txtaddressID" runat="server" MaxLength="250" CssClass="input" Enabled="false"></asp:textbox>
							</TD>
						</TR>
						<TR>
							<TD>* Billing Address</TD>
							<TD>
							<asp:textbox TextMode="MultiLine" Rows="5" id="txtbilladdress" runat="server" MaxLength="350" CssClass="input"></asp:textbox>
							</TD>
						</TR>
						<TR>
							<TD height="30" colSpan="2" align="center">
								<asp:button id="btnsubmit" runat="server" CssClass="button" Text="Submit"></asp:button>&nbsp;&nbsp;
								<asp:button id="btnreset" runat="server" CssClass="button" Text="Reset" CausesValidation="False"></asp:button>&nbsp;&nbsp;
						  <asp:button id="btnClose" runat="server" CssClass="button" Text="Close"></asp:button></TD>
						</TR>
				</asp:panel><asp:panel id="pnlconfirmation" Runat="server" Visible="False">
					<TR align="center">
						<TD colSpan="2">
							<asp:Label id="lblMessage" runat="server"></asp:Label></TD>
					</TR>
					<TR align="center">
						<TD colSpan="2">
							<asp:HyperLink id="hyplnkretry2" runat="server" onClick="window.close();"></asp:HyperLink><br><br></TD>
					</TR>
					<TR align="center">
						<TD colSpan="2">
							<asp:HyperLink id="hyplnkretry" runat="server"></asp:HyperLink></TD>
					</TR>
				</asp:panel></TBODY></TABLE>
		</form>
	</body>
</HTML>