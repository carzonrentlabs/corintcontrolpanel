Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Mail
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class ClientCoEditAddress
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btnsubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnClose As System.Web.UI.WebControls.Button
    Protected WithEvents btnreset As System.Web.UI.WebControls.Button
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents hyplnkretry2 As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    Protected WithEvents txtaddressID As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtbilladdress As System.Web.UI.WebControls.TextBox

    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    Protected WithEvents lblerr As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btnsubmit.Attributes("onClick") = "return validation();"
        btnClose.Attributes("onClick") = "window.close();"
        If Not Page.IsPostBack Then
            Dim dtrreader As SqlDataReader
            Dim accessdata As clsutility
            accessdata = New clsutility
            dtrreader = accessdata.funcGetSQLDataReader("select top 1 ClientCoAddID, ClientCoID, Counter, clientcoBilladdress from CORIntClientCoAddMaster where ClientCoID=" & Request.QueryString("ClientID") & " order by ClientCoAddID ")
            dtrreader.Read()

            txtbilladdress.Text = dtrreader("clientcoBilladdress")
            txtaddressID.Text = dtrreader("ClientCoAddID")
            dtrreader.Close()
            accessdata.Dispose()
        End If
    End Sub
    Private Sub btnsubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsubmit.Click


        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        Dim intuniqcheck As SqlParameter
        Dim intuniqvalue as int32

        cmd = New SqlCommand("procClientCoAddressEdit_POD", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("@ClientCoAddID", txtaddressID.Text)
        cmd.Parameters.Add("@ClientCoID", Request.Querystring("ClientID"))
        cmd.Parameters.Add("@clientcoBilladdress", txtbilladdress.Text)
        cmd.Parameters.Add("@modifiedby", Session("loggedin_user"))
       intuniqcheck = cmd.Parameters.Add("@uniqcheckval", SqlDbType.Int)
       intuniqcheck.Direction = ParameterDirection.Output

  '      Try
            MyConnection.Open()
            cmd.ExecuteNonQuery()
           intuniqvalue=cmd.Parameters("@uniqcheckval").Value
            MyConnection.Close()

           if not intuniqvalue = 0 then
                lblErrorMsg.visible=true
                lblErrorMsg.text="This address already exists."
                exit sub
            else
		        lblErrorMsg.visible=false
                pnlmainform.Visible = False
                pnlconfirmation.Visible = True

                lblMessage.Text = "You have updated the address successfully"
                hyplnkretry.Text = "Edit more addresses for this company"
            hyplnkretry.NavigateUrl = "BillToSearch.aspx" '& Request.Querystring("ClientID")
            'hyplnkretry2.Text = "Close"
                hyplnkretry2.NavigateUrl = "#"
            end if
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function

    Private Sub btnreset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnreset.Click
        Response.Redirect("ClientCoEditAddress.aspx?ID="& Request.Querystring("ID") & "&RID=" & Request.Querystring("RID"))
    End Sub

End Class
