﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Ebillingsubmission.aspx.cs" Inherits="Ebillingsubmission" %>

<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="~/usercontrol/Headerctrl.ascx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CarzonRent :: Internal software control panel</title>
     <link href="../HertzInt.css" type="text/css" rel="stylesheet">
    <script type="text/javascript">
        function ValidateControl() {
            if (document.getElementById("<%=ddlClientCoName.ClientID%>").selectedIndex == 0) {
                alert("Select client name");
                document.getElementById("<%=ddlClientCoName.ClientID%>").focus();
                return false;
            }
            else if (document.getElementById("<%=ddlUserName.ClientID%>").selectedIndex == 0) {
                alert("Select user name.");
                document.getElementById("<%=ddlUserName.ClientID%>").focus();
                return false;
            }

    }

    </script>
</head>
<body>
    <form id="from" runat="server">
        <uc1:Headerctrl ID="Headerctrl1" runat="server"></uc1:Headerctrl>
        <center>
        <table border="0">
            <tr>
                <td colspan="2" style="text-align:center;"><b>E-Billing Client Mapping</b></td>
            </tr>
            <tr>
                <td colspan="2"><asp:Label ID ="lblMessage" runat ="server" Visible="false"></asp:Label></td>
            </tr>
            <tr>
                <td><b>Client Name</b></td>
                <td>
                    <asp:DropDownList ID="ddlClientCoName" runat="server"></asp:DropDownList>&nbsp;
                    <asp:RequiredFieldValidator ID="rfValidateClientName" runat ="server" ControlToValidate ="ddlClientCoName" ErrorMessage="*" InitialValue="0" Display ="Dynamic" ForeColor="Red" SetFocusOnError="true"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td><b>User Name</b> </td>
                <td>
                    <asp:DropDownList ID="ddlUserName" runat="server"></asp:DropDownList>&nbsp;
                    <asp:RequiredFieldValidator ID="rgvlidateUserName" runat ="server" ControlToValidate ="ddlUserName" ErrorMessage="*" InitialValue="0" Display ="Dynamic" ForeColor="Red" SetFocusOnError="true"></asp:RequiredFieldValidator>
                </td>

            </tr>
            <tr>
                <td>Active</td>
                <td><asp:CheckBox ID="chkActive" runat="server" /></td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                </td>
            </tr>
        </table>
            </center>

    </form>
</body>
</html>
