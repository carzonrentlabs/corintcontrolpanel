﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

public partial class Ebillingsubmission : System.Web.UI.Page
{
    string connectionString = System.Configuration.ConfigurationSettings.AppSettings["corConnectString"];
    SqlConnection myConnection = null;
    SqlCommand cmd = null;
    SqlDataAdapter adp = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        lblMessage.Visible = false;
        if (!Page.IsPostBack)
        {
            BindClient();
            BindUser();
            btnSubmit.Attributes.Add("onclick", "ValidateControl()");
            if (Request.QueryString.HasKeys())
            {
                DataSet ds = new DataSet();
                string id= Request.QueryString["ID"];
                using (myConnection = new SqlConnection(connectionString))
                {
                    adp = new SqlDataAdapter("select SysUserId,ClientCoId,Active from CorIntClientUserMapping where ID=" + id, myConnection);
                    adp.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        ddlClientCoName.SelectedValue =Convert.ToString(ds.Tables[0].Rows[0]["ClientCoId"]);
                        ddlUserName.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["SysUserId"]);
                        if (Convert.ToBoolean(ds.Tables[0].Rows[0]["Active"]) == true)
                        {
                            chkActive.Checked = true;
                        }
                        else
                        {
                            chkActive.Checked = false;
                        }
                    }
                    
                }
                
                
            }

        }
        btnSubmit.Attributes.Add("onclick", "ValidateControl()");
    }

    private void BindUser()
    {
        DataSet dsUser = new DataSet();
        using (myConnection = new SqlConnection(connectionString))
        {
            adp = new SqlDataAdapter("select fname+' '+mname+' '+lname as username,sysuserid from CORIntSysUsersMaster order by fname+' '+mname+' '+lname ", myConnection);
            adp.Fill(dsUser);
            ddlUserName.DataSource = dsUser.Tables[0];
            ddlUserName.DataTextField = "username";
            ddlUserName.DataValueField = "sysuserid";
            ddlUserName.DataBind();
            ddlUserName.Items.Insert(0, new ListItem("--Select--", "0"));
        }

    }

    private void BindClient()
    {
        DataSet dsClient = new DataSet();
        using (myConnection = new SqlConnection(connectionString))
        {
            adp = new SqlDataAdapter("select ClientCoName, ClientCoID from CorIntClientCoMaster where active=1 order by ClientCoName ", myConnection);
            adp.Fill(dsClient);
            ddlClientCoName.DataSource = dsClient.Tables[0];
            ddlClientCoName.DataTextField = "ClientCoName";
            ddlClientCoName.DataValueField = "ClientCoID";
            ddlClientCoName.DataBind();
            ddlClientCoName.Items.Insert(0, new ListItem("--Select--", "0"));
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (ddlClientCoName.SelectedIndex == 0)
        {
            lblMessage.Visible = true;
            lblMessage.Text = "Select client name";
            lblMessage.ForeColor = System.Drawing.Color.Red;
        }
        else if (ddlUserName.SelectedIndex == 0)
        {
            lblMessage.Visible = true;
            lblMessage.Text = "Select user name";
            lblMessage.ForeColor = System.Drawing.Color.Red;
        }
        else
        {
            try
            {
                SqlParameter regIdentity;
                int status;
                using (myConnection = new SqlConnection(connectionString))
                {
                    myConnection.Open();
                    cmd = new SqlCommand();
                    cmd.CommandText = "Prc_GetAccessRightEbillingSubmission";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ClientCoId", ddlClientCoName.SelectedValue);
                    cmd.Parameters.AddWithValue("@UserId", ddlUserName.SelectedValue);
                    cmd.Parameters.AddWithValue("@SysUserID", Session["loggedin_user"]);
                    cmd.Parameters.AddWithValue("@Active",chkActive.Checked);
                    regIdentity = cmd.Parameters.AddWithValue("@ID", SqlDbType.Int);
                    regIdentity.Direction = ParameterDirection.Output;
                    cmd.Connection = myConnection;
                    cmd.ExecuteNonQuery();
                    status = Convert.ToInt32(cmd.Parameters["@ID"].Value);
                    if (status > 1)
                    {
                        lblMessage.Visible = true;
                        lblMessage.Text = "Registration successfully";
                        lblMessage.ForeColor = System.Drawing.Color.Red;
                        ddlClientCoName.SelectedIndex = 0;
                        ddlUserName.SelectedIndex = 0;
                    }
                    else
                    {
                        lblMessage.Visible = true;
                        lblMessage.Text = "Registration update successfully.";
                        lblMessage.ForeColor = System.Drawing.Color.Red;
                        ddlClientCoName.SelectedIndex = 0;
                        ddlUserName.SelectedIndex = 0;
                    }
                }
            }
            catch (Exception Ex)
            {

                lblMessage.Visible = true;
                lblMessage.Text = Ex.Message;
                lblMessage.ForeColor = System.Drawing.Color.Red;
            }
        }

    }
}