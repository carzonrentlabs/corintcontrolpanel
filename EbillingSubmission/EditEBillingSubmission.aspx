﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EditEBillingSubmission.aspx.cs" Inherits="EbillingSubmission_EditEBillingSubmission" %>

<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="~/usercontrol/Headerctrl.ascx" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CarzonRent :: Internal software control panel</title>
    <link href="../HertzInt.css" type="text/css" rel="stylesheet">
    <script type="text/javascript">
        function ValidateControl() {
            if (document.getElementById("<%=ddlClientCoName.ClientID%>").selectedIndex == 0) {
                alert("Select client name");
                document.getElementById("<%=ddlClientCoName.ClientID%>").focus();
                return false;
            }
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <uc1:Headerctrl ID="Headerctrl1" runat="server"></uc1:Headerctrl>
        <center>
            <table border="0">
                <tr>
                    <td colspan="3" style="text-align: center;"><b>E-Billing Client Mapping</b></td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label></td>
                </tr>
                <tr>
                    <td><b>Client Name</b></td>
                    <td>
                        <asp:DropDownList ID="ddlClientCoName" runat="server"></asp:DropDownList>&nbsp;
                    <asp:RequiredFieldValidator ID="rfValidateClientName" runat="server" ControlToValidate="ddlClientCoName" ErrorMessage="*" InitialValue="0" Display="Dynamic" ForeColor="Red" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </td>
                    <td align="center">
                        <asp:Button ID="btnSubmit" runat="server" Text="Get" OnClick="btnSubmit_Click" />
                    </td>
                </tr>
                <tr>
                    <td colspan ="3" style="text-align:center;">
                        <asp:GridView ID="grvUseDetails" runat ="server" AutoGenerateColumns="False" BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" CellPadding="3" CellSpacing="2" EnableModelValidation="True"
                             DataKeyNames ="Id,SysUserID,ClientCoID" OnRowCommand="grvUseDetails_RowCommand">
                            <Columns>
                                <asp:TemplateField HeaderText="S.No.">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSNo" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                <asp:BoundField DataField="ClientConame" HeaderText="Client Name" />
                                <asp:BoundField DataField="loginId" HeaderText="Login Id" />
                                <asp:BoundField DataField="UserName" HeaderText="User Name" />
                                <asp:BoundField DataField="UserStatus" HeaderText="Status" />
                                <asp:TemplateField HeaderText="Edit">
                                    <ItemTemplate >
                                        <asp:Button ID ="btnEdit" runat="server" Text ="Edit" CommandName="BKsharma"  CommandArgument ="<%#((GridViewRow)Container).RowIndex %>"/>
                                      <%-- <asp:LinkButton ID="btnEdit" Text="Edit" runat="server"  CommandName="Edit"  CommandArgument ="<%#((GridViewRow)Container).RowIndex %>"/>--%>
                                        </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                            <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                            <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                            <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                            <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>

        </center>
    </form>
</body>
</html>
