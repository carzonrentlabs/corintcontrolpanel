using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System.Xml;
using System.Collections;

///<summary>
///Summary description for clslAdmin
///</summary>
///
namespace CCS
{
    public class clsAdmin
    {
        public int Retval;
        public string dbUser = "carzonrent";
        public string dbPWD = "cor@991yz";
        public clsAdmin()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        #region DashBoard

        public DataSet Top20_CMB(string txtDate)
        {
            SqlParameter[] ObjParam = new SqlParameter[1];
            ObjParam[0] = new SqlParameter("@DateTo", SqlDbType.DateTime);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = txtDate;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_Corporatewise_MonthlyBilling", ObjParam);
        }

        public DataSet Corporate_V_Analysis(string txtDate)
        {
            SqlParameter[] ObjParam = new SqlParameter[1];
            ObjParam[0] = new SqlParameter("@CurrentDate", SqlDbType.DateTime);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = txtDate;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_Corporate_Variance_Analysis_2", ObjParam);
        }

        public DataSet BranchWiseCarWisePickupSummary(DateTime txtDate)
        {
            SqlParameter[] ObjParam = new SqlParameter[1];
            ObjParam[0] = new SqlParameter("@dateTo", SqlDbType.DateTime);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = txtDate;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_CarwiseDutiesSummary", ObjParam);
        }

        public DataSet Top20_Clients(string txtDate)
        {
            SqlParameter[] ObjParam = new SqlParameter[1];
            ObjParam[0] = new SqlParameter("@Date", SqlDbType.DateTime);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = txtDate;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_top20clients", ObjParam);
        }

        public DataSet DashBoard_ImplantBookingDetails(string txtDate)
        {
            SqlParameter[] ObjParam = new SqlParameter[1];
            ObjParam[0] = new SqlParameter("@Date1", SqlDbType.DateTime);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = txtDate;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prcDashBoard_ImplantWiseBookingReport", ObjParam);
        }

        public DataSet DashBoard_CategoryWisePickupSummary(string txtDate)
        {
            SqlParameter[] ObjParam = new SqlParameter[1];
            ObjParam[0] = new SqlParameter("@Date", SqlDbType.DateTime);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = txtDate;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_Categorywise_Pickup_Summary", ObjParam);
        }

        public DataSet DashBoard_CategoryWisePickupSummary_New(string txtDate)
        {
            SqlParameter[] ObjParam = new SqlParameter[1];
            ObjParam[0] = new SqlParameter("@Date", SqlDbType.DateTime);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = txtDate;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_Categorywise_Pickup_Summary_New", ObjParam);
        }

        public DataSet GetBranchwiseCategorywisePickup(DateTime pickupdate)
        {
            try
            {
                SqlParameter[] ObjparamUser = new SqlParameter[1];
                ObjparamUser[0] = new SqlParameter("@date", pickupdate);
                ObjparamUser[0].Direction = ParameterDirection.Input;

                return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_Categorywise_Branchwise_Pickup_Summary_New", ObjparamUser);
            }
            catch
            {
                return null;
            }

        }

        public DataSet BranchWiseCatWise_DSClosed_Revenue(DateTime ApprovedDate)
        {
            try
            {
                SqlParameter[] ObjparamUser = new SqlParameter[1];
                ObjparamUser[0] = new SqlParameter("@date", SqlDbType.DateTime);
                ObjparamUser[0].Direction = ParameterDirection.Input;
                ObjparamUser[0].Value = ApprovedDate;

                return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prcDashBoard_BranchWiseCatWise_DSClosed_Revenue", ObjparamUser);
            }
            catch
            {
                return null;
            }

        }

        public DataSet BranchWiseClosed_Pending_Duties(string txtDate)
        {
            SqlParameter[] ObjParam = new SqlParameter[1];
            ObjParam[0] = new SqlParameter("@Date", SqlDbType.DateTime);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = txtDate;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_BranchWise_Closed_Pending_Duties", ObjParam);
        }

        public DataSet DashBoard_CategoryWiseClosingSummary(string txtDate)
        {
            SqlParameter[] ObjParam = new SqlParameter[1];
            ObjParam[0] = new SqlParameter("@fromDate", SqlDbType.DateTime);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = txtDate;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_Category_SummaryDetail", ObjParam);
        }

        public DataSet DashBoard_CatWiseBranchWisePickSummary(string txtDate)
        {
            SqlParameter[] ObjParam = new SqlParameter[1];
            ObjParam[0] = new SqlParameter("@date", SqlDbType.DateTime);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = txtDate;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_Categorywise_Branchwise_Pickup_Summary", ObjParam);
        }

        public DataSet DashBoard_CatWiseBranchWisePicClosingkSummary(string txtDate)
        {
            SqlParameter[] ObjParam = new SqlParameter[1];
            ObjParam[0] = new SqlParameter("@Date1", SqlDbType.DateTime);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = txtDate;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prcDashBoard_BranchWiseCatWise_DSSummary", ObjParam);
        }

        public DataSet Top20_CMBcity(string txtDate, int city)
        {
            SqlParameter[] ObjParam = new SqlParameter[2];
            ObjParam[0] = new SqlParameter("@DateTo", txtDate);
            ObjParam[1] = new SqlParameter("@cityid", city);

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_Corporatewise_MonthlyBilling_citywise", ObjParam);
        }

        public DataSet Corporate_V_Analysiscity(string txtDate, int city)
        {
            SqlParameter[] ObjParam = new SqlParameter[2];
            ObjParam[0] = new SqlParameter("@CurrentDate", txtDate);
            ObjParam[1] = new SqlParameter("@cityid", city);

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_Corporate_Variance_Analysis_2_citywise", ObjParam);
        }

        public DataSet BranchWiseCarWisePickupSummarycity(DateTime txtDate, int city)
        {
            SqlParameter[] ObjParam = new SqlParameter[2];
            ObjParam[0] = new SqlParameter("@dateTo", txtDate);
            ObjParam[1] = new SqlParameter("@cityid", city);

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_CarwiseDutiesSummary_citywise", ObjParam);
        }

        public DataSet Top20_Clientscity(string txtDate, int city)
        {
            SqlParameter[] ObjParam = new SqlParameter[2];
            ObjParam[0] = new SqlParameter("@Date", txtDate);
            ObjParam[1] = new SqlParameter("@cityid", city);

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_top20clients_CityWise", ObjParam);
        }

        public DataSet Top20_Clientscity_New(string txtDate, int city)
        {
            SqlParameter[] ObjParam = new SqlParameter[2];
            ObjParam[0] = new SqlParameter("@Date", txtDate);
            ObjParam[1] = new SqlParameter("@cityid", city);

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_top20clients_CityWise_New", ObjParam);
        }

        public DataSet DashBoard_ImplantBookingDetailscity(string txtDate, int cityid)
        {
            SqlParameter[] ObjParam = new SqlParameter[2];
            ObjParam[0] = new SqlParameter("@Date1", txtDate);
            ObjParam[1] = new SqlParameter("@cityid", cityid);

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prcDashBoard_ImplantWiseBookingReport_CityWise", ObjParam);
        }

        public DataSet DashBoard_CategoryWisePickupSummarycity(string txtDate, int city)
        {
            SqlParameter[] ObjParam = new SqlParameter[2];
            ObjParam[0] = new SqlParameter("@Date", txtDate);
            ObjParam[1] = new SqlParameter("@CityId", city);

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_Categorywise_Pickup_Summary_Citywise", ObjParam);
        }

        public DataSet DashBoard_CategoryWisePickupSummary_Newcity(string txtDate, int city)
        {
            SqlParameter[] ObjParam = new SqlParameter[2];
            ObjParam[0] = new SqlParameter("@Date", txtDate);
            ObjParam[1] = new SqlParameter("@CityId", city);

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_Categorywise_Pickup_Summary_Citywise", ObjParam);
        }

        public DataSet GetBranchwiseCategorywisePickupcity(DateTime pickupdate, int city)
        {
            try
            {
                SqlParameter[] ObjParam = new SqlParameter[2];
                ObjParam[0] = new SqlParameter("@Date", pickupdate);
                ObjParam[1] = new SqlParameter("@CityId", city);

                return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_Categorywise_Branchwise_Pickup_Summary_New_CityWise", ObjParam);
            }
            catch
            {
                return null;
            }

        }

        public DataSet BranchWiseCatWise_DSClosed_Revenuecity(DateTime ApprovedDate, int city)
        {
            try
            {
                SqlParameter[] ObjparamUser = new SqlParameter[2];
                ObjparamUser[0] = new SqlParameter("@date", ApprovedDate);
                ObjparamUser[1] = new SqlParameter("@CityId", city);

                return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prcDashBoard_BranchWiseCatWise_DSClosed_Revenue_CityWise", ObjparamUser);
            }
            catch
            {
                return null;
            }

        }

        public DataSet BranchWiseClosed_Pending_Dutiescity(string txtDate, int city)
        {
            SqlParameter[] ObjParam = new SqlParameter[2];
            ObjParam[0] = new SqlParameter("@date", txtDate);
            ObjParam[1] = new SqlParameter("@Cityid", city);

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_BranchWise_Closed_Pending_Duties_CityWise", ObjParam);
        }


        public DataSet DashBoard_CategoryWiseClosingSummarycity(string txtDate, int cityid)
        {
            SqlParameter[] ObjParam = new SqlParameter[2];
            ObjParam[0] = new SqlParameter("@fromDate", txtDate);
            ObjParam[1] = new SqlParameter("@cityid", cityid);

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_Category_SummaryDetail_Citywise", ObjParam);
        }

        public DataSet DashBoard_CatWiseBranchWisePickSummarycity(string txtDate, int city)
        {
            SqlParameter[] ObjParam = new SqlParameter[2];
            ObjParam[0] = new SqlParameter("@date", txtDate);
            ObjParam[1] = new SqlParameter("@cityid", city);

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_Categorywise_Branchwise_Pickup_Summary_New_CityWise", ObjParam);
        }

        public DataSet DashBoard_CatWiseBranchWisePicClosingkSummarycity(string txtDate, int city)
        {
            SqlParameter[] ObjParam = new SqlParameter[2];
            ObjParam[0] = new SqlParameter("@Date1", txtDate);
            ObjParam[1] = new SqlParameter("@cityid", city);

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prcDashBoard_BranchWiseCatWise_DSSummary_Citywise", ObjParam);
        }

        public DataTable DashBoard_BranchWise_Variance_Analysis(string txtdate, int city)
        {
            SqlParameter[] ObjParam = new SqlParameter[2];
            ObjParam[0] = new SqlParameter("@CurrentDate", txtdate);
            ObjParam[1] = new SqlParameter("@cityid", city);

            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "prc_Variance_Analysis_2_citywise", ObjParam);
        }

        public DataSet BranchwisePickupRevenue(string txtdate, int city)
        {
            SqlParameter[] ObjParam = new SqlParameter[2];
            ObjParam[0] = new SqlParameter("@date", txtdate);
            ObjParam[1] = new SqlParameter("@cityid", city);

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_Branchwise_Pickup_Revenue_Summary_CityWise", ObjParam);
        }

        public DataSet CashFlowSummary(string txtdate, int city)
        {
            SqlParameter[] ObjParam = new SqlParameter[2];
            ObjParam[0] = new SqlParameter("@date", txtdate);
            ObjParam[1] = new SqlParameter("@cityid", city);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_CC_BTC_Flow_CityWise", ObjParam);
        }


        public DataSet CRMSummary(string txtdate, int city)
        {
            SqlParameter[] ObjParam = new SqlParameter[2];
            ObjParam[0] = new SqlParameter("@date", txtdate);
            ObjParam[1] = new SqlParameter("@cityid", city);
            return SqlHelper.ExecuteEasyCabsDataset(CommandType.StoredProcedure, "prc_getComplaintSummary", ObjParam);
        }

        public DataTable DashBoard_Branch_RevenueBudgetAnalysis(string txtdate, int city)
        {
            SqlParameter[] ObjParam = new SqlParameter[2];
            ObjParam[0] = new SqlParameter("@CurrentDate", txtdate);
            ObjParam[1] = new SqlParameter("@cityid", city);

            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "prc_Budget_Variance_Analysis_4_citywise", ObjParam);
        }

        #endregion

        public DataSet CCSBatchCreation_BulkData(int ClientCoID, int LocationID, string DateFrom, string DateTo, int ApprovedBySSC)
        {
            SqlParameter[] ObjParam = new SqlParameter[5];
            ObjParam[0] = new SqlParameter("@ClientCoID", SqlDbType.Int);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = ClientCoID;

            ObjParam[1] = new SqlParameter("@LocationID", SqlDbType.Int);
            ObjParam[1].Direction = ParameterDirection.Input;
            ObjParam[1].Value = LocationID;

            ObjParam[2] = new SqlParameter("@DateFrom", SqlDbType.VarChar);
            ObjParam[2].Direction = ParameterDirection.Input;
            ObjParam[2].Value = DateFrom;

            ObjParam[3] = new SqlParameter("@DateTo", SqlDbType.VarChar);
            ObjParam[3].Direction = ParameterDirection.Input;
            ObjParam[3].Value = DateTo;

            ObjParam[4] = new SqlParameter("@ApprovedBySSC", SqlDbType.Int);
            ObjParam[4].Direction = ParameterDirection.Input;
            ObjParam[4].Value = ApprovedBySSC;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "CCSBatchCreation_BulkData", ObjParam);
        }

        public DataSet GetCCS_Batches(int CityID, string FromDate, string ToDate)
        {
            SqlParameter[] ObjParam = new SqlParameter[3];
            ObjParam[0] = new SqlParameter("@CityID", SqlDbType.Int);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = CityID;

            ObjParam[1] = new SqlParameter("@FromDate", SqlDbType.VarChar);
            ObjParam[1].Direction = ParameterDirection.Input;
            ObjParam[1].Value = FromDate;

            ObjParam[2] = new SqlParameter("@ToDate", SqlDbType.VarChar);
            ObjParam[2].Direction = ParameterDirection.Input;
            ObjParam[2].Value = ToDate;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "getCCSBatches", ObjParam);
        }

        public int CheckLogin(string userName, string uPassword)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];

            ObjparamUser[0] = new SqlParameter("@UserName", SqlDbType.NVarChar, 50);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = userName;

            ObjparamUser[1] = new SqlParameter("@Password", SqlDbType.NVarChar, 50);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = uPassword;

            return Convert.ToInt32(SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "uspChkLogin", ObjparamUser));
        }

        public DataSet GetLocations_UserAccessWise(int UserID)
        {
            SqlParameter[] ObjParam = new SqlParameter[1];
            ObjParam[0] = new SqlParameter("@UserID", SqlDbType.Int);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = UserID;
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "getLocations_UserAccessWise", ObjParam);
        }

        #region SSC

        public DataSet GetSSC_Batches(int CityID, string FromDate, string ToDate, int Status)
        {
            SqlParameter[] ObjParam = new SqlParameter[4];
            ObjParam[0] = new SqlParameter("@CityID", SqlDbType.Int);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = CityID;

            ObjParam[1] = new SqlParameter("@FromDate", SqlDbType.VarChar);
            ObjParam[1].Direction = ParameterDirection.Input;
            ObjParam[1].Value = FromDate;

            ObjParam[2] = new SqlParameter("@ToDate", SqlDbType.VarChar);
            ObjParam[2].Direction = ParameterDirection.Input;
            ObjParam[2].Value = ToDate;

            ObjParam[3] = new SqlParameter("@Status", SqlDbType.Int);
            ObjParam[3].Direction = ParameterDirection.Input;
            ObjParam[3].Value = Status;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "getSSCBatches", ObjParam);
        }

        public DataSet GetSSC_Batches2(int CityID, string FromDate, string ToDate, int Status)
        {
            SqlParameter[] ObjParam = new SqlParameter[4];
            ObjParam[0] = new SqlParameter("@CityID", SqlDbType.Int);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = CityID;

            ObjParam[1] = new SqlParameter("@FromDate", SqlDbType.VarChar);
            ObjParam[1].Direction = ParameterDirection.Input;
            ObjParam[1].Value = FromDate;

            ObjParam[2] = new SqlParameter("@ToDate", SqlDbType.VarChar);
            ObjParam[2].Direction = ParameterDirection.Input;
            ObjParam[2].Value = ToDate;

            ObjParam[3] = new SqlParameter("@Status", SqlDbType.Int);
            ObjParam[3].Direction = ParameterDirection.Input;
            ObjParam[3].Value = Status;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "getSSCBatches2", ObjParam);
        }

        public DataSet GetBTCBatchDetail(int BatchNo)
        {
            SqlParameter[] ObjParam = new SqlParameter[1];
            ObjParam[0] = new SqlParameter("@BatchID", SqlDbType.Int);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = BatchNo;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prcGetBTCBatchUpdate", ObjParam);
        }

        public void ApproveBTCBatch(int BatchNo, int BookingID, int ApprovedYN, int SysUserID, int Flg)
        {
            SqlParameter[] ObjParam = new SqlParameter[5];
            ObjParam[0] = new SqlParameter("@BatchID", SqlDbType.Int);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = BatchNo;

            ObjParam[1] = new SqlParameter("@BookingID", SqlDbType.Int);
            ObjParam[1].Direction = ParameterDirection.Input;
            ObjParam[1].Value = BookingID;

            ObjParam[2] = new SqlParameter("@ApprovedYN", SqlDbType.Bit);
            ObjParam[2].Direction = ParameterDirection.Input;
            ObjParam[2].Value = ApprovedYN;

            ObjParam[3] = new SqlParameter("@SysUserID", SqlDbType.Int);
            ObjParam[3].Direction = ParameterDirection.Input;
            ObjParam[3].Value = SysUserID;

            ObjParam[4] = new SqlParameter("@Flg", SqlDbType.Int);
            ObjParam[4].Direction = ParameterDirection.Input;
            ObjParam[4].Value = Flg;

            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "prcUpdateBtchBatch", ObjParam);
        }

        public DataSet GetSSCBatchDetail(int BatchNo)
        {
            SqlParameter[] ObjParam = new SqlParameter[1];
            ObjParam[0] = new SqlParameter("@BatchID", SqlDbType.Int);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = BatchNo;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_SSCBatchModify", ObjParam);
        }

        public DataSet SSCBatchModify_AddBookingID(int BookingID, int BatchID)
        {
            SqlParameter[] ObjParam = new SqlParameter[2];
            ObjParam[0] = new SqlParameter("@BookingID", SqlDbType.Int);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = BookingID;

            ObjParam[1] = new SqlParameter("@BatchID", SqlDbType.Int);
            ObjParam[1].Direction = ParameterDirection.Input;
            ObjParam[1].Value = BatchID;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SSCBatchModify_AddBookingID", ObjParam);
        }

        public void SSCBatchModify_SaveBatch(int BatchID, int BookingID, string Company, string Guest,
            DateTime PickupDate, int Parking, int Toll, int StateTax, int Email, int Photocopies
            , int SysUserID, int ApprovedYN, int Flg)
        {
            SqlParameter[] ObjParam = new SqlParameter[13];
            ObjParam[0] = new SqlParameter("@BatchID", BatchID);
            ObjParam[1] = new SqlParameter("@BookingID", BookingID);
            ObjParam[2] = new SqlParameter("@Company", Company);
            ObjParam[3] = new SqlParameter("@Guest", Guest);
            ObjParam[4] = new SqlParameter("@PickupDate", PickupDate);
            ObjParam[5] = new SqlParameter("@Parking", Parking);
            ObjParam[6] = new SqlParameter("@Toll", Toll);
            ObjParam[7] = new SqlParameter("@StateTax", StateTax);
            ObjParam[8] = new SqlParameter("@Email", Email);
            ObjParam[9] = new SqlParameter("@Photocopies", Photocopies);
            ObjParam[10] = new SqlParameter("@SysUserID", SysUserID);
            ObjParam[11] = new SqlParameter("@ApprovedYN", ApprovedYN);
            ObjParam[12] = new SqlParameter("@Flg", Flg);

            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "SSCBatchModify_SaveBatch", ObjParam);
        }

        public void SSCBatchUpdate(DataTable dtGridView)
        {
            string connectionstring = System.Configuration.ConfigurationManager.AppSettings["ConnectionString"];
            //string connectionstring = "server=216.144.195.101,1444;database=livecarzonrent;uid=carzonrent;pwd=cor@991yz";
            SqlConnection myConnection = new SqlConnection(connectionstring);
            myConnection.Open();
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("Select * from corIntBTCBatchDetail where 1=0", myConnection);
            SqlCommand cmdDelete = new SqlCommand("Delete from corIntBTCBatchDetail where batchno=" + dtGridView.Rows[0]["Batchid"].ToString(), myConnection);
            da.SelectCommand = cmd;
            da.DeleteCommand = cmdDelete;
            // da.Fill(ds);
            SqlCommandBuilder cBldr = new SqlCommandBuilder(da);
            da.Fill(ds);


            try
            {
                DataTable dt = ds.Tables["Table"];
                dt.TableName = "corIntBTCBatchDetail";

                for (int i = 0; i < dtGridView.Rows.Count; i++)
                {
                    DataRow dr = ds.Tables[0].NewRow();
                    dr["BatchNo"] = Convert.ToInt32(dtGridView.Rows[i]["BatchID"].ToString());
                    dr["DSNo"] = Convert.ToInt32(dtGridView.Rows[i]["BookingID"].ToString());
                    dr["Company"] = dtGridView.Rows[i]["Company"].ToString();
                    dr["Guest"] = dtGridView.Rows[i]["Guest"].ToString();
                    dr["PickUpDate"] = dtGridView.Rows[i]["PickupDate"].ToString();
                    dr["Parking"] = dtGridView.Rows[i]["Parking"].ToString().Trim() == "True" ? 1 : 0;
                    dr["Toll"] = dtGridView.Rows[i]["Toll"].ToString().Trim() == "True" ? 1 : 0;
                    dr["StateTax"] = dtGridView.Rows[i]["StateTax"].ToString().Trim() == "True" ? 1 : 0;
                    dr["Email"] = dtGridView.Rows[i]["Email"].ToString().Trim() == "True" ? 1 : 0;
                    dr["Photocopies"] = dtGridView.Rows[i]["Photocopies"].ToString().Trim() == "True" ? 1 : 0;

                    if (dtGridView.Rows[i]["Status"].ToString().Trim() == "Approved")
                    {
                        dr["ApprovedYN"] = 1;
                    }
                    else
                    {
                        dr["ApprovedYN"] = 0;
                    }
                    dt.Rows.Add(dr);
                }
                da.DeleteCommand.ExecuteNonQuery();
                da.Update(ds, "corIntBTCBatchDetail");
                ds.AcceptChanges();
                da.Dispose();
            }
            catch
            {
            }
            myConnection.Close();
            myConnection.Dispose();
        }

        public DataSet GetDataForExcelSSC(int ClientCoID, string fromdate, string todate, int status1, string Status2, int radio, int senttossc, int ccsbatch, int BatchNo, int CityID)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[10];

            ObjparamUser[0] = new SqlParameter("@ClientCoID", ClientCoID);
            ObjparamUser[1] = new SqlParameter("@DateFrom", fromdate);
            ObjparamUser[2] = new SqlParameter("@DateTo", todate);
            ObjparamUser[3] = new SqlParameter("@Status1", status1);
            ObjparamUser[4] = new SqlParameter("@Status2", Status2);
            ObjparamUser[5] = new SqlParameter("@rdoOption", radio);
            ObjparamUser[6] = new SqlParameter("@SentToSSC", senttossc);
            ObjparamUser[7] = new SqlParameter("@CCSBatch", ccsbatch);
            ObjparamUser[8] = new SqlParameter("@BatchNo", BatchNo);
            ObjparamUser[9] = new SqlParameter("@CityID", CityID);

            //return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SSC_MIS_Test", ObjparamUser);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SSC_MIS", ObjparamUser);
        }

        public DataSet GetDataForExcelSSC_Pivot1(string fromdate, string todate, string ToDateRemoveYN)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[3];

            ObjparamUser[0] = new SqlParameter("@DateFrom", fromdate);
            ObjparamUser[1] = new SqlParameter("@DateTo", todate);
            ObjparamUser[2] = new SqlParameter("@ToDateRemoveYN", ToDateRemoveYN);

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SSC_MIS_Pivot", ObjparamUser);
        }

        public DataSet GetDataForExcelSSC_Pivot2(string fromdate, string todate, string ToDateRemoveYN)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[3];

            ObjparamUser[0] = new SqlParameter("@DateFrom", fromdate);
            ObjparamUser[1] = new SqlParameter("@DateTo", todate);
            ObjparamUser[2] = new SqlParameter("@ToDateRemoveYN", ToDateRemoveYN);

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SSC_MIS_Pivot_2", ObjparamUser);
        }

        #endregion

        public DataSet getCorpCompanyNames()
        {
            try
            {
                return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "getCorpCompanyNames");
            }
            catch
            {
                return null;
            }
        }

        public DataSet getCreditCompanyNames()
        {
            try
            {
                return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "getCreditLimitCompanyNames1");
            }
            catch
            {
                return null;
            }
        }

        public DataSet getCreditLimitCompanyNames()
        {
            try
            {
                return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "getCreditLimitCompanyNames");
            }
            catch
            {
                return null;
            }
        }

        //public DataSet getcityName()
        //{
        //    try
        //    {
        //        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "getcityname");
        //    }
        //    catch
        //    {
        //        return null;
        //    }
        //}

        public DataSet GetLoginDetails(string userName, string uPassword)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];

            ObjparamUser[0] = new SqlParameter("@UserName", SqlDbType.NVarChar, 50);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = userName;

            ObjparamUser[1] = new SqlParameter("@Password", SqlDbType.NVarChar, 50);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = uPassword;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspGetLoginDetails", ObjparamUser);
        }

        public DataSet GetCompanyName()
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, " UspGetCompanyName");
        }

        public DataSet GetBatch()
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "getbatchno");
        }

        public DataSet GetCompanyName1()
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GetClientName");
        }

        public DataSet GetCompanyNamek(int type)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];
            ObjparamUser[0] = new SqlParameter("@type", SqlDbType.Int);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = type;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "UspGetCompanyName_k", ObjparamUser);
        }

        public DataSet GetCityName()
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "UspCityName_1");
        }

        public DataSet GetCCSCorpCompanies()
        {
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GetCCSCorpCompanies");
        }

        public DataSet GetBatchCompany(int clientType)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];

            ObjparamUser[0] = new SqlParameter("@Ind", SqlDbType.Int, 50);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = clientType;



            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "UspGetBatchCompanyName", ObjparamUser);
        }

        public DataSet GetBatchData(string strCompId, string strDatefrom, string strDateto, int city)
        {

            SqlParameter[] ObjparamUser = new SqlParameter[5];

            ObjparamUser[0] = new SqlParameter("@CompID", SqlDbType.NVarChar);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = strCompId;

            ObjparamUser[1] = new SqlParameter("@invFrom", SqlDbType.NVarChar);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            //ObjparamUser[1].Value = strDatefrom;
            if (strDatefrom == "")
            {
                ObjparamUser[1].Value = "1/1/1753";
            }
            else
            {
                ObjparamUser[1].Value = strDatefrom;//Convert.ToDateTime(DepartureDate);
            }

            ObjparamUser[2] = new SqlParameter("@invTo", SqlDbType.NVarChar);
            ObjparamUser[2].Direction = ParameterDirection.Input;
            //ObjparamUser[2].Value = strDateto;
            if (strDateto == "")
            {
                ObjparamUser[2].Value = "12/31/9999";
            }
            else
            {
                ObjparamUser[2].Value = strDateto;//Convert.ToDateTime(DepartureDate);
            }

            ObjparamUser[3] = new SqlParameter("@CityID", SqlDbType.Int);
            ObjparamUser[3].Direction = ParameterDirection.Input;
            ObjparamUser[3].Value = city;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspBatchInvoice", ObjparamUser);

        }

        public DataSet GetBatchCount(string strCompId, string strDatefrom, string strDateto, int city, int batchID)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[5];

            ObjparamUser[0] = new SqlParameter("@CompID", SqlDbType.NVarChar);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = strCompId;

            ObjparamUser[1] = new SqlParameter("@invFrom", SqlDbType.NVarChar);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            //ObjparamUser[1].Value = strDatefrom;
            if (strDatefrom == "")
            {
                ObjparamUser[1].Value = "1/1/1753";
            }
            else
            {
                ObjparamUser[1].Value = strDatefrom;//Convert.ToDateTime(DepartureDate);
            }

            ObjparamUser[2] = new SqlParameter("@invTo", SqlDbType.NVarChar);
            ObjparamUser[2].Direction = ParameterDirection.Input;
            //ObjparamUser[2].Value = strDateto;
            if (strDateto == "")
            {
                ObjparamUser[2].Value = "12/31/9999";
            }
            else
            {
                ObjparamUser[2].Value = strDateto;//Convert.ToDateTime(DepartureDate);
            }

            ObjparamUser[3] = new SqlParameter("@CityID", SqlDbType.Int);
            ObjparamUser[3].Direction = ParameterDirection.Input;
            ObjparamUser[3].Value = city;

            ObjparamUser[4] = new SqlParameter("@batchID", SqlDbType.Int);
            ObjparamUser[4].Direction = ParameterDirection.Input;
            ObjparamUser[4].Value = batchID;

            if (strCompId == "C965")//if Company is Amex PreAuth
            {
                return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspInvoiceCount_11", ObjparamUser);
            }
            else

                return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspInvoiceCount", ObjparamUser);
        }

        public DataSet GetBatchData(string strCompId, string strDatefrom, string strDateto, int city, int CurrentPage, int pagesize, string Mode, int BatchId, int USerID)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[9];

            ObjparamUser[0] = new SqlParameter("@CompID", SqlDbType.NVarChar);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = strCompId;

            ObjparamUser[1] = new SqlParameter("@invFrom", SqlDbType.NVarChar);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            //ObjparamUser[1].Value = strDatefrom;
            if (strDatefrom == "")
            {
                ObjparamUser[1].Value = "1/1/1753";
            }
            else
            {
                ObjparamUser[1].Value = strDatefrom;//Convert.ToDateTime(DepartureDate);
            }

            ObjparamUser[2] = new SqlParameter("@invTo", SqlDbType.NVarChar);
            ObjparamUser[2].Direction = ParameterDirection.Input;
            //ObjparamUser[2].Value = strDateto;
            if (strDateto == "")
            {
                ObjparamUser[2].Value = "12/31/9999";
            }
            else
            {
                ObjparamUser[2].Value = strDateto;//Convert.ToDateTime(DepartureDate);
            }

            ObjparamUser[3] = new SqlParameter("@CityID", SqlDbType.Int);
            ObjparamUser[3].Direction = ParameterDirection.Input;
            ObjparamUser[3].Value = city;

            ObjparamUser[4] = new SqlParameter("@currentpage", SqlDbType.Int);
            ObjparamUser[4].Direction = ParameterDirection.Input;
            ObjparamUser[4].Value = CurrentPage;

            ObjparamUser[5] = new SqlParameter("@pagesize", SqlDbType.Int);
            ObjparamUser[5].Direction = ParameterDirection.Input;
            ObjparamUser[5].Value = pagesize;

            ObjparamUser[6] = new SqlParameter("@MODE", SqlDbType.Char);
            ObjparamUser[6].Direction = ParameterDirection.Input;
            ObjparamUser[6].Value = Mode;

            ObjparamUser[7] = new SqlParameter("@batchId", SqlDbType.BigInt);
            ObjparamUser[7].Direction = ParameterDirection.Input;
            ObjparamUser[7].Value = BatchId;

            ObjparamUser[8] = new SqlParameter("@userID", SqlDbType.Int);
            ObjparamUser[8].Direction = ParameterDirection.Input;
            ObjparamUser[8].Value = USerID;

            if (strCompId == "C965")//if Company is Amex PreAuth
            {
                return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspBatchInvoice_paging_masterlast11", ObjparamUser);
            }
            else if (strCompId == "C714")//if Company is Retail
            {
                return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspBatchInvoice_paging_Masterlast1_Retail", ObjparamUser);
            }
            else
            {
                return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspBatchInvoice_paging_masterlast1", ObjparamUser);
            }
        }

        public DataSet DeleteBatchID(int Userrid)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];

            ObjparamUser[0] = new SqlParameter("@UserID", SqlDbType.NVarChar);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = Userrid;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspDelTempBatchData", ObjparamUser);
        }

        public DataSet DeleteBatchIDNew(int Userrid, string CompID)
        {

            SqlParameter[] ObjparamUser = new SqlParameter[2];
            ObjparamUser[0] = new SqlParameter("@userid", SqlDbType.Int);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = Userrid;

            ObjparamUser[1] = new SqlParameter("@compid", SqlDbType.NVarChar);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = CompID;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspdeltempbatchdata_new", ObjparamUser);

        }

        public DataSet GetCountAfterDone(int Userrid)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];

            ObjparamUser[0] = new SqlParameter("@UserID", SqlDbType.Int);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = Userrid;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspTotalInvoiceafterdone", ObjparamUser);
        }

        public DataSet GetCountInKnockingOFF(int Userrid)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];

            ObjparamUser[0] = new SqlParameter("@UserID", SqlDbType.Int);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = Userrid;



            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspcountknocktemp", ObjparamUser);
        }

        public DataSet GetSearchReslutKnock(int Userrid, string invId, int currrentpage, int pagesize, string mode)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[8];

            ObjparamUser[0] = new SqlParameter("@UserID", SqlDbType.Int);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = Userrid;

            ObjparamUser[1] = new SqlParameter("@invoiceid", SqlDbType.NVarChar);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = invId;

            ObjparamUser[2] = new SqlParameter("@currentpage", SqlDbType.Int);
            ObjparamUser[2].Direction = ParameterDirection.Input;
            ObjparamUser[2].Value = currrentpage;

            ObjparamUser[3] = new SqlParameter("@pagesize", SqlDbType.Int);
            ObjparamUser[3].Direction = ParameterDirection.Input;
            ObjparamUser[3].Value = pagesize;

            ObjparamUser[4] = new SqlParameter("@mode", SqlDbType.NVarChar);
            ObjparamUser[4].Direction = ParameterDirection.Input;
            ObjparamUser[4].Value = mode;




            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspserchknockpage", ObjparamUser);
        }

        public DataSet GetBatchHeader(string Companyid)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];

            ObjparamUser[0] = new SqlParameter("@CompID", SqlDbType.NVarChar);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = Companyid;

            ObjparamUser[1] = new SqlParameter("@BatchID", SqlDbType.NVarChar);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = "";

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspBatchHeader", ObjparamUser);
        }

        public DataSet GetVoidInvoice(string BatchID, int currentpage, int size, int userid)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[6];



            ObjparamUser[1] = new SqlParameter("@BatchID", SqlDbType.NVarChar);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = BatchID;

            ObjparamUser[2] = new SqlParameter("@currentpage", SqlDbType.Int);
            ObjparamUser[2].Direction = ParameterDirection.Input;
            ObjparamUser[2].Value = currentpage;

            ObjparamUser[3] = new SqlParameter("@PageSize", SqlDbType.Int);
            ObjparamUser[3].Direction = ParameterDirection.Input;
            ObjparamUser[3].Value = size;

            ObjparamUser[4] = new SqlParameter("@userid", SqlDbType.Int);
            ObjparamUser[4].Direction = ParameterDirection.Input;
            ObjparamUser[4].Value = userid;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspGetBatchInvoice", ObjparamUser);
        }

        public DataSet GetBatchHeaderInVoid(string batchid)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];

            ObjparamUser[0] = new SqlParameter("@CompID", SqlDbType.NVarChar);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = "";

            ObjparamUser[1] = new SqlParameter("@BatchID", SqlDbType.NVarChar);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = batchid;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspBatchHeader", ObjparamUser);
        }

        public DataSet GetPaymnetIDForNotKnock(string Clientid, int Cityid)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];

            ObjparamUser[0] = new SqlParameter("@Clientid", SqlDbType.NVarChar);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = Clientid;

            ObjparamUser[1] = new SqlParameter("@Cityid", SqlDbType.Int);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = Cityid;


            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspBindPaymentID_NotKnock", ObjparamUser);
        }

        public int AddBAtch(DataTable DtBatch, string compId, string compType, string duedate, string DueDateOfPayment, string TotInvValue, int CreatedBy)
        {
            string strQuery;
            string strQueryGetFinID;
            string strreturnID;
            string strtransQuery;
            string Strtraninvoicesudate;
            ArrayList arrObj = new ArrayList();
            ArrayList arrUPDATEObj = new ArrayList();

            strQueryGetFinID = "Select isnull(max(Fin_YearID)+1,1) as FinID from CCSMStBatch  where Financial_year=year(getdate())";
            DataSet dsMaxFinID = SqlHelper.ExecuteDataset(CommandType.Text, strQueryGetFinID);
            string strMaxFinId2 = dsMaxFinID.Tables[0].Rows[0][0].ToString();

            strQuery = "Insert into CCSMStBatch(Clientid,CompType,Batch_SubmissionDate,PaymentDueDate,TotalInvoiceAmt,CreatedBy,Fin_YearID,UpdatedBy) Values (" + Convert.ToInt32(compId) + ",'" + compType + "','" + Convert.ToDateTime(duedate) + "' ,'" + Convert.ToDateTime(DueDateOfPayment) + "','" + Convert.ToDouble(TotInvValue) + "','" + CreatedBy + "','" + strMaxFinId2 + "','" + CreatedBy + "' )";
            SqlHelper.ExecuteDataset(CommandType.Text, strQuery);

            DataSet dsMaxID = SqlHelper.ExecuteDataset(CommandType.Text, "select max(BatchID) from CCSMStBatch");
            ///Max Id
            // string strS2 = "select max(FKBatchID) from CCStransBatchInvoice";
            string strId2 = dsMaxID.Tables[0].Rows[0][0].ToString();
            int intId2;
            if (strId2 == "")
            {
                intId2 = 1;
            }
            else
            {
                intId2 = Convert.ToInt32(strId2);
                intId2 = intId2;
            }
            ///Max Id

            for (int i = 0; i < DtBatch.Rows.Count; i++)
            {
                if (int.Parse(DtBatch.Rows[i]["flag"].ToString()) == 1)
                {
                    strtransQuery = "Insert into CCStransBatchInvoice(FKBatchID,FKInvoiceID,TotalInvoiceAmt,CreatedBy ) Values (" + intId2 + ",'" + Convert.ToInt32(DtBatch.Rows[i]["StrInvoiceID"]) + "','" + Convert.ToDouble(DtBatch.Rows[i]["TotCost"]) + "' ,'" + CreatedBy + "' )";
                    Strtraninvoicesudate = "UPDATE CORIntInvoice SET CCS_LinkedWithBatch =1 WHERE InvoiceID = " + Convert.ToInt32(DtBatch.Rows[i]["StrInvoiceID"]);
                    arrObj.Add(strtransQuery);
                    arrUPDATEObj.Add(Strtraninvoicesudate);
                }
            }

            SqlHelper.ExecuteNonQuery(arrObj);
            SqlHelper.ExecuteNonQuery(arrUPDATEObj);

            return intId2;
        }

        public int GetFinID(int BatchId)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];

            ObjparamUser[0] = new SqlParameter("@BatchID", SqlDbType.NVarChar);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = BatchId;

            ObjparamUser[1] = new SqlParameter("@status", SqlDbType.Int);
            ObjparamUser[1].Direction = ParameterDirection.ReturnValue;

            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "GetOnlYFinID", ObjparamUser);
            int status = Convert.ToInt32(ObjparamUser[1].Value.ToString());
            return status;
        }

        public DataTable MaxBatch()
        {
            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "SelectMaxBatchId");
        }

        public string SaveBatchwithPaging(string compid, string comptype, string duedateofpayment, double lblTotInvValue, int createdby, string submissiondate, string Remarks, string batchid, string Mode)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[15];

            ObjparamUser[0] = new SqlParameter("@CompID", SqlDbType.NVarChar);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = compid;

            ObjparamUser[1] = new SqlParameter("@compType", SqlDbType.NVarChar);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = comptype;

            ObjparamUser[2] = new SqlParameter("@lblDueDateOfPayment", SqlDbType.NVarChar);
            ObjparamUser[2].Direction = ParameterDirection.Input;
            ObjparamUser[2].Value = duedateofpayment;

            ObjparamUser[3] = new SqlParameter("@lblTotInvValue", SqlDbType.NVarChar);
            ObjparamUser[3].Direction = ParameterDirection.Input;
            ObjparamUser[3].Value = lblTotInvValue;

            ObjparamUser[4] = new SqlParameter("@createdby", SqlDbType.NVarChar);
            ObjparamUser[4].Direction = ParameterDirection.Input;
            ObjparamUser[4].Value = createdby;

            ObjparamUser[5] = new SqlParameter("@batchsubmissiondate", SqlDbType.NVarChar);
            ObjparamUser[5].Direction = ParameterDirection.Input;
            ObjparamUser[5].Value = submissiondate;

            ObjparamUser[6] = new SqlParameter("@Remarks", SqlDbType.NVarChar);
            ObjparamUser[6].Direction = ParameterDirection.Input;
            ObjparamUser[6].Value = Remarks;

            ObjparamUser[7] = new SqlParameter("@batchidN", SqlDbType.NVarChar);
            ObjparamUser[7].Direction = ParameterDirection.Input;
            ObjparamUser[7].Value = batchid;

            ObjparamUser[8] = new SqlParameter("@Mode", SqlDbType.NVarChar);
            ObjparamUser[8].Direction = ParameterDirection.Input;
            ObjparamUser[8].Value = Mode;

            //ObjparamUser[5] = new SqlParameter("@retVal", SqlDbType.Int);
            //ObjparamUser[5].Direction = ParameterDirection.ReturnValue;

            ObjparamUser[9] = new SqlParameter("@Batch", SqlDbType.VarChar, 20);
            ObjparamUser[9].Direction = ParameterDirection.Output;

            int status = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "uspCreateBatch", ObjparamUser);
            //int status = Convert.ToInt32(ObjparamUser[5].Value.ToString());
            //return status;
            if (status > 0)
            {
                string BatchID = ObjparamUser[9].Value.ToString();
                return BatchID;
            }
            else
            {
                return "0";
            }

        }

        public int GetFinIDForKnocking(int BatchId)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];

            ObjparamUser[0] = new SqlParameter("@BatchID", SqlDbType.NVarChar);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = BatchId;

            ObjparamUser[1] = new SqlParameter("@status", SqlDbType.Int);
            ObjparamUser[1].Direction = ParameterDirection.ReturnValue;

            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "GetOnlYFinIDForKnockingOF", ObjparamUser);
            int status = Convert.ToInt32(ObjparamUser[1].Value.ToString());
            return status;
        }

        public string GetFullFinID(int newBatchId)
        {
            string strreturnID = "select 'BD' +convert(varchar,Year(getdate())) + convert(varchar,[dbo].[udfDigit](" + newBatchId + "))";
            DataSet dsReturnMaxFinID = SqlHelper.ExecuteDataset(CommandType.Text, strreturnID);

            string strMaxReturnFinId2 = dsReturnMaxFinID.Tables[0].Rows[0][0].ToString();
            return strMaxReturnFinId2;
        }

        public string GetFullFinIDForKnocking(int newBatchId)
        {
            string strreturnID = "select 'KO' +convert(varchar,Year(getdate())) + convert(varchar,[dbo].[udfDigit](" + newBatchId + "))";
            DataSet dsReturnMaxFinID = SqlHelper.ExecuteDataset(CommandType.Text, strreturnID);

            string strMaxReturnFinId2 = dsReturnMaxFinID.Tables[0].Rows[0][0].ToString();
            return strMaxReturnFinId2;
        }

        public DataSet GetDataForExcel(int BatchId)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];

            ObjparamUser[0] = new SqlParameter("@BatchID", SqlDbType.NVarChar);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = BatchId;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspinvoiceExcelSheet", ObjparamUser);
        }

        public DataSet GetBatchID(string BatchId)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];

            ObjparamUser[0] = new SqlParameter("@BatchNo", SqlDbType.NVarChar);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = BatchId;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspBatchid", ObjparamUser);
        }

        public int InsertInvoices(int userid, int pageno, String InvoiceID)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[4];

            ObjparamUser[0] = new SqlParameter("@UserID", SqlDbType.Int);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = userid;

            ObjparamUser[1] = new SqlParameter("@pageNo", SqlDbType.Int);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = pageno;

            ObjparamUser[2] = new SqlParameter("@InvoiceID", SqlDbType.VarChar);
            ObjparamUser[2].Direction = ParameterDirection.Input;
            ObjparamUser[2].Value = InvoiceID;
            return SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "usptempBatchInvoice", ObjparamUser);
        }

        public int GenerateAmexBatches(int batchno2)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];
            ObjparamUser[0] = new SqlParameter("@batch2", SqlDbType.Int);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = batchno2;
            //return SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "uspAmexautoGenBatch_New1",ObjparamUser);
            return SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "uspAmexautoGenBatch_new1_SingleBatchAmex", ObjparamUser);
        }

        public int GenerateMasterVisaBatch(int batchno2)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];
            ObjparamUser[0] = new SqlParameter("@batch2", SqlDbType.Int);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = batchno2;
            return SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "prc_AutoGen_SingleBatchMasterVisa", ObjparamUser);
        }

        public int GenerateAmexBatches_Temp(int batchno2)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];
            ObjparamUser[0] = new SqlParameter("@batch2", SqlDbType.Int);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = batchno2;
            return SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "uspAmexautoGenBatch_New1_Temp", ObjparamUser);
        }

        public DataSet GetAmexBatchDetails(int batchno2)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];
            ObjparamUser[0] = new SqlParameter("@batchNo2", SqlDbType.Int);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = batchno2;
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_GetAmexBatchDetails", ObjparamUser);
        }

        public DataSet GetAmexBatchDetails_Temp(int batchno2)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];
            ObjparamUser[0] = new SqlParameter("@batchNo2", SqlDbType.Int);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = batchno2;
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_GetAmexBatchDetails_Temp", ObjparamUser);
        }

        public int DeleteInvoices(int userid, String InvoiceID)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[4];

            ObjparamUser[0] = new SqlParameter("@UserID", SqlDbType.Int);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = userid;

            ObjparamUser[1] = new SqlParameter("@InvoiceID", SqlDbType.VarChar);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = InvoiceID;

            return SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "dbo.usptempDeleteVoidInvoice", ObjparamUser);
        }

        public DataSet GetCity(string compid)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];

            ObjparamUser[0] = new SqlParameter("@CompId", SqlDbType.NVarChar);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = compid;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspcityofCompany", ObjparamUser);
        }

        public DataSet GetAllCity()
        {
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspallcity");
        }

        public DataSet GetBatchallcity()
        {
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspBatchallcity");
        }

        public DataSet GetBatchInvoice(string strBatchID, string strcompid, string submissionstatus, string sortstatus)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[6];

            ObjparamUser[0] = new SqlParameter("@BatchID", SqlDbType.NVarChar);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = strBatchID;

            ObjparamUser[1] = new SqlParameter("@CompID", SqlDbType.NVarChar);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = strcompid;

            ObjparamUser[2] = new SqlParameter("@SubmissionStatus", SqlDbType.NVarChar);
            ObjparamUser[2].Direction = ParameterDirection.Input;
            ObjparamUser[2].Value = submissionstatus;

            ObjparamUser[3] = new SqlParameter("@SortStatus", SqlDbType.NVarChar);
            ObjparamUser[3].Direction = ParameterDirection.Input;
            ObjparamUser[3].Value = sortstatus;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspsearchbatchinvoice", ObjparamUser);
        }

        public DataSet GetBatchInvoiceFroVoidAmend(string batchid)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[6];

            ObjparamUser[0] = new SqlParameter("@BatchID", SqlDbType.NVarChar);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = batchid;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspFetchBatch", ObjparamUser);
        }

        public int UpdateVoidBatch(string batchid, string remarks, int updatedby)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[4];

            ObjparamUser[0] = new SqlParameter("@BatchID", SqlDbType.NVarChar);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = batchid;

            ObjparamUser[1] = new SqlParameter("@Remarks", SqlDbType.NVarChar);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = remarks;

            ObjparamUser[2] = new SqlParameter("@UpdatedBy", SqlDbType.Int);
            ObjparamUser[2].Direction = ParameterDirection.Input;
            ObjparamUser[2].Value = updatedby;

            ObjparamUser[3] = new SqlParameter("@status", SqlDbType.Int);
            ObjparamUser[3].Direction = ParameterDirection.Output;

            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "uspUpdateBatchVoidStatus", ObjparamUser);
            int status = Convert.ToInt32(ObjparamUser[3].Value.ToString());
            return status;
        }

        public DataSet GetKnockingOffbatchWise(string batchid, int knockingoffid, string mode, int userid, int currentpage, int pagesize, int Cityid, string CompID)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[8];

            ObjparamUser[0] = new SqlParameter("@BatchID", SqlDbType.NVarChar);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = batchid;

            ObjparamUser[1] = new SqlParameter("@KnockingID", SqlDbType.Int);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = knockingoffid;

            ObjparamUser[2] = new SqlParameter("@Mode", SqlDbType.Char);
            ObjparamUser[2].Direction = ParameterDirection.Input;
            ObjparamUser[2].Value = mode;

            ObjparamUser[3] = new SqlParameter("@userID", SqlDbType.Int);
            ObjparamUser[3].Direction = ParameterDirection.Input;
            ObjparamUser[3].Value = userid;

            ObjparamUser[4] = new SqlParameter("@currentpage", SqlDbType.Int);
            ObjparamUser[4].Direction = ParameterDirection.Input;
            ObjparamUser[4].Value = currentpage;

            ObjparamUser[5] = new SqlParameter("@pagesize", SqlDbType.Int);
            ObjparamUser[5].Direction = ParameterDirection.Input;
            ObjparamUser[5].Value = pagesize;

            ObjparamUser[6] = new SqlParameter("@Cityid", SqlDbType.Int);
            ObjparamUser[6].Direction = ParameterDirection.Input;
            ObjparamUser[6].Value = Cityid;

            ObjparamUser[7] = new SqlParameter("@CompID", SqlDbType.NVarChar);
            ObjparamUser[7].Direction = ParameterDirection.Input;
            ObjparamUser[7].Value = CompID;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspKnockingOffBatchWise_11", ObjparamUser);
        }

        public DataSet GetKnockDeduction(string ComPID, string ApprovedKnock, string Deduction_ApprovedRemarks, int KnockingID, int Createdby, string Mode, int cityid)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[7];

            ObjparamUser[0] = new SqlParameter("@ComPID", SqlDbType.NVarChar);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = ComPID;

            ObjparamUser[1] = new SqlParameter("@ApprovedKnock", SqlDbType.NVarChar);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = ApprovedKnock;

            ObjparamUser[2] = new SqlParameter("@Deduction_ApprovedRemarks", SqlDbType.NVarChar);
            ObjparamUser[2].Direction = ParameterDirection.Input;
            ObjparamUser[2].Value = Deduction_ApprovedRemarks;

            ObjparamUser[3] = new SqlParameter("@KnockingID", SqlDbType.Int);
            ObjparamUser[3].Direction = ParameterDirection.Input;
            ObjparamUser[3].Value = KnockingID;

            ObjparamUser[4] = new SqlParameter("@Createdby", SqlDbType.Int);
            ObjparamUser[4].Direction = ParameterDirection.Input;
            ObjparamUser[4].Value = Createdby;

            ObjparamUser[5] = new SqlParameter("@Mode", SqlDbType.NVarChar);
            ObjparamUser[5].Direction = ParameterDirection.Input;
            ObjparamUser[5].Value = Mode;

            ObjparamUser[6] = new SqlParameter("@city", SqlDbType.Int);
            ObjparamUser[6].Direction = ParameterDirection.Input;
            ObjparamUser[6].Value = cityid;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspKnockDeduction", ObjparamUser);
        }

        public int UpdateKnockDeduction(string ComPID, string ApprovedKnock, string Deduction_ApprovedRemarks, int KnockingID, int Createdby, string Mode, int cityid)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[7];

            ObjparamUser[0] = new SqlParameter("@ComPID", SqlDbType.NVarChar);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = ComPID;

            ObjparamUser[1] = new SqlParameter("@ApprovedKnock", SqlDbType.NVarChar);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = ApprovedKnock;

            ObjparamUser[2] = new SqlParameter("@Deduction_ApprovedRemarks", SqlDbType.NVarChar);
            ObjparamUser[2].Direction = ParameterDirection.Input;
            ObjparamUser[2].Value = Deduction_ApprovedRemarks;

            ObjparamUser[3] = new SqlParameter("@KnockingID", SqlDbType.Int);
            ObjparamUser[3].Direction = ParameterDirection.Input;
            ObjparamUser[3].Value = KnockingID;

            ObjparamUser[4] = new SqlParameter("@Createdby", SqlDbType.Int);
            ObjparamUser[4].Direction = ParameterDirection.Input;
            ObjparamUser[4].Value = Createdby;

            ObjparamUser[5] = new SqlParameter("@Mode", SqlDbType.NVarChar);
            ObjparamUser[5].Direction = ParameterDirection.Input;
            ObjparamUser[5].Value = Mode;

            ObjparamUser[6] = new SqlParameter("@city", SqlDbType.Int);
            ObjparamUser[6].Direction = ParameterDirection.Input;
            ObjparamUser[6].Value = cityid;

            return SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "uspKnockDeduction", ObjparamUser);
        }

        public int Updateknocking(string Compid, int PaymentID, double DeductionCIPL, double UnappliedCredit, double TDSDeducted,
            double BankCharges, double OtherDeduction, double Discount, string Reason_ForDeductionCIPL,
            double GlobalDeduction, int CreatedBy, string Mode, string Resonofvoid, int knockingid)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[17];

            ObjparamUser[0] = new SqlParameter("@Compid", SqlDbType.NVarChar);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = Compid;

            ObjparamUser[1] = new SqlParameter("@PaymentID", SqlDbType.Int);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = PaymentID;

            ObjparamUser[2] = new SqlParameter("@DeductionCIPL", SqlDbType.Decimal);
            ObjparamUser[2].Direction = ParameterDirection.Input;
            ObjparamUser[2].Value = DeductionCIPL;

            ObjparamUser[3] = new SqlParameter("@UnappliedCredit", SqlDbType.Decimal);
            ObjparamUser[3].Direction = ParameterDirection.Input;
            ObjparamUser[3].Value = UnappliedCredit;

            ObjparamUser[4] = new SqlParameter("@TDSDeducted", SqlDbType.Decimal);
            ObjparamUser[4].Direction = ParameterDirection.Input;
            ObjparamUser[4].Value = TDSDeducted;

            ObjparamUser[5] = new SqlParameter("@BankCharges", SqlDbType.Decimal);
            ObjparamUser[5].Direction = ParameterDirection.Input;
            ObjparamUser[5].Value = BankCharges;

            ObjparamUser[6] = new SqlParameter("@OtherDeduction", SqlDbType.Decimal);
            ObjparamUser[6].Direction = ParameterDirection.Input;
            ObjparamUser[6].Value = OtherDeduction;

            ObjparamUser[7] = new SqlParameter("@Discount", SqlDbType.Decimal);
            ObjparamUser[7].Direction = ParameterDirection.Input;
            ObjparamUser[7].Value = Discount;

            ObjparamUser[8] = new SqlParameter("@Reason_ForDeductionCIPL", SqlDbType.NVarChar);
            ObjparamUser[8].Direction = ParameterDirection.Input;
            ObjparamUser[8].Value = Reason_ForDeductionCIPL;

            ObjparamUser[9] = new SqlParameter("@GlobalDeduction", SqlDbType.Decimal);
            ObjparamUser[9].Direction = ParameterDirection.Input;
            ObjparamUser[9].Value = GlobalDeduction;

            ObjparamUser[10] = new SqlParameter("@CreatedBy", SqlDbType.Int);
            ObjparamUser[10].Direction = ParameterDirection.Input;
            ObjparamUser[10].Value = CreatedBy;

            ObjparamUser[11] = new SqlParameter("@Reasonforvoid", SqlDbType.NVarChar);
            ObjparamUser[11].Direction = ParameterDirection.Input;
            ObjparamUser[11].Value = Resonofvoid;

            ObjparamUser[12] = new SqlParameter("@Mode", SqlDbType.Char);
            ObjparamUser[12].Direction = ParameterDirection.Input;
            ObjparamUser[12].Value = Mode;

            ObjparamUser[13] = new SqlParameter("@knockingid", SqlDbType.Int);
            ObjparamUser[13].Direction = ParameterDirection.Input;
            ObjparamUser[13].Value = knockingid;

            ObjparamUser[14] = new SqlParameter("@knockID", SqlDbType.Int);
            ObjparamUser[14].Direction = ParameterDirection.Output;

            int knockingID = 0;
            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "uspUpdateKnocking_Test", ObjparamUser);
            if (ObjparamUser[14].Value.ToString() != "")
            {
                knockingID = Convert.ToInt32(ObjparamUser[14].Value.ToString());
            }
            return knockingID;
        }

        public int UpdateknockingTrans(int KnockingID, int BatchID, int InvoiceID, double BillAmount,
            double AdjustedAmount, double Deductions, string Remarks, int CreatedBy)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[9];

            ObjparamUser[0] = new SqlParameter("@KnockingID", SqlDbType.Int);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = KnockingID;

            ObjparamUser[1] = new SqlParameter("@BatchID", SqlDbType.Int);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = BatchID;

            ObjparamUser[2] = new SqlParameter("@InvoiceID", SqlDbType.Int);
            ObjparamUser[2].Direction = ParameterDirection.Input;
            ObjparamUser[2].Value = InvoiceID;

            ObjparamUser[3] = new SqlParameter("@BillAmount", SqlDbType.Decimal);
            ObjparamUser[3].Direction = ParameterDirection.Input;
            ObjparamUser[3].Value = BillAmount;

            ObjparamUser[4] = new SqlParameter("@AdjustedAmount", SqlDbType.Decimal);
            ObjparamUser[4].Direction = ParameterDirection.Input;
            ObjparamUser[4].Value = AdjustedAmount;

            ObjparamUser[5] = new SqlParameter("@Deductions", SqlDbType.Decimal);
            ObjparamUser[5].Direction = ParameterDirection.Input;
            ObjparamUser[5].Value = Deductions;

            ObjparamUser[6] = new SqlParameter("@Remarks", SqlDbType.NVarChar);
            ObjparamUser[6].Direction = ParameterDirection.Input;
            ObjparamUser[6].Value = Remarks;

            ObjparamUser[7] = new SqlParameter("@CreatedBy", SqlDbType.Int);
            ObjparamUser[7].Direction = ParameterDirection.Input;
            ObjparamUser[7].Value = CreatedBy;

            //ObjparamUser[7] = new SqlParameter("@status", SqlDbType.Int);
            //ObjparamUser[7].Direction = ParameterDirection.Output;

            return SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "uspUpdateKnockingTrans", ObjparamUser);
            //int status = Convert.ToInt32(ObjparamUser[3].Value.ToString());
            //return status;
        }

        public int UpdateBatch(int batchid, int Pending_forCall, string remarks, string Follow_upDate, string Submission_status, string InvoiceStatus, string Dispute_Resolution_Status, int Payment_Available, double PaymentAmt_Promised, string Payment_Date_Promised, int Cheques_Ready, int Cheques_Picked, int Payment_Received, int CreatedBy, string AckDate, string SubmittedTo, string BatchType, double AmtRcvd, string AWBno)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[19];

            ObjparamUser[0] = new SqlParameter("@Batchid", SqlDbType.Int);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = batchid;

            ObjparamUser[1] = new SqlParameter("@Pending_forCall", SqlDbType.Bit);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = Pending_forCall;

            ObjparamUser[2] = new SqlParameter("@Batch_Remarks", SqlDbType.NVarChar);
            ObjparamUser[2].Direction = ParameterDirection.Input;
            ObjparamUser[2].Value = remarks;

            ObjparamUser[3] = new SqlParameter("@Follow_upDate", SqlDbType.DateTime);
            ObjparamUser[3].Direction = ParameterDirection.Input;
            // ObjparamUser[3].Value = Follow_upDate;
            if (Follow_upDate != "")
            {
                //ObjparamUser[3].Value = Follow_upDate;
                ObjparamUser[3].Value = Convert.ToDateTime(Follow_upDate);
            }
            else
            {
                ObjparamUser[3].Value = DBNull.Value;
            }

            ObjparamUser[4] = new SqlParameter("@Submission_status", SqlDbType.NVarChar);
            ObjparamUser[4].Direction = ParameterDirection.Input;
            ObjparamUser[4].Value = Submission_status;

            ObjparamUser[5] = new SqlParameter("@InvoiceStatus", SqlDbType.NVarChar);
            ObjparamUser[5].Direction = ParameterDirection.Input;
            ObjparamUser[5].Value = InvoiceStatus;

            ObjparamUser[6] = new SqlParameter("@Dispute_Resolution_Status", SqlDbType.NVarChar);
            ObjparamUser[6].Direction = ParameterDirection.Input;
            ObjparamUser[6].Value = Dispute_Resolution_Status;

            ObjparamUser[7] = new SqlParameter("@Payment_Available", SqlDbType.Int);
            ObjparamUser[7].Direction = ParameterDirection.Input;
            ObjparamUser[7].Value = Payment_Available;

            ObjparamUser[8] = new SqlParameter("@PaymentAmt_Promised", SqlDbType.Float);
            ObjparamUser[8].Direction = ParameterDirection.Input;
            ObjparamUser[8].Value = PaymentAmt_Promised;

            ObjparamUser[9] = new SqlParameter("@Payment_Date_Promised", SqlDbType.DateTime);
            ObjparamUser[9].Direction = ParameterDirection.Input;
            //ObjparamUser[9].Value = Payment_Date_Promised;
            if (Payment_Date_Promised != "")
            {
                //ObjparamUser[9].Value = Payment_Date_Promised;
                ObjparamUser[9].Value = Convert.ToDateTime(Payment_Date_Promised);
            }
            else
            {
                ObjparamUser[9].Value = DBNull.Value;
            }


            ObjparamUser[10] = new SqlParameter("@Cheques_Ready", SqlDbType.Int);
            ObjparamUser[10].Direction = ParameterDirection.Input;
            ObjparamUser[10].Value = Cheques_Ready;

            ObjparamUser[11] = new SqlParameter("@Cheques_Picked", SqlDbType.Int);
            ObjparamUser[11].Direction = ParameterDirection.Input;
            ObjparamUser[11].Value = Cheques_Picked;


            ObjparamUser[12] = new SqlParameter("@Payment_Received", SqlDbType.Int);
            ObjparamUser[12].Direction = ParameterDirection.Input;
            ObjparamUser[12].Value = Payment_Received;

            ObjparamUser[13] = new SqlParameter("@CreatedBy", SqlDbType.Int);
            ObjparamUser[13].Direction = ParameterDirection.Input;
            ObjparamUser[13].Value = CreatedBy;

            ObjparamUser[14] = new SqlParameter("@AckDate", SqlDbType.DateTime);
            ObjparamUser[14].Direction = ParameterDirection.Input;
            if (AckDate != "")
            {
                //ObjparamUser[9].Value = Payment_Date_Promised;
                ObjparamUser[14].Value = Convert.ToDateTime(AckDate);
            }
            else
            {
                ObjparamUser[14].Value = DBNull.Value;
            }

            ObjparamUser[15] = new SqlParameter("@SubmittedTo", SqlDbType.NVarChar);
            ObjparamUser[15].Direction = ParameterDirection.Input;
            ObjparamUser[15].Value = SubmittedTo;

            ObjparamUser[16] = new SqlParameter("@BatchType", SqlDbType.NVarChar);
            ObjparamUser[16].Direction = ParameterDirection.Input;
            ObjparamUser[16].Value = BatchType;

            ObjparamUser[17] = new SqlParameter("@AmtRcvd", SqlDbType.Float);
            ObjparamUser[17].Direction = ParameterDirection.Input;
            ObjparamUser[17].Value = AmtRcvd;

            ObjparamUser[18] = new SqlParameter("@AWBno", SqlDbType.NVarChar);
            ObjparamUser[18].Direction = ParameterDirection.Input;
            ObjparamUser[18].Value = AWBno;

            //ObjparamUser[14] = new SqlParameter("@status", SqlDbType.Int);
            //ObjparamUser[14].Direction = ParameterDirection.Output;

            return SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "uspBatchUpdate", ObjparamUser);
            //int status = Convert.ToInt32(ObjparamUser[14].Value.ToString());
            //return status;
        }

        public static int UpdateBatchSubmissionStatus(int batchid, DateTime BatchDate, string SubmittedLoc, string SubmittedTo, string ContNoSubTo, DateTime SubmittionDate, DateTime? AckDate, string AwbNo, int UpdatedBy)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[9];
            ObjparamUser[0] = new SqlParameter("@Batchid", SqlDbType.Int);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = batchid;
            ObjparamUser[1] = new SqlParameter("@BatchDate", SqlDbType.DateTime);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = BatchDate;
            ObjparamUser[2] = new SqlParameter("@SubmittedLoc", SqlDbType.NVarChar);
            ObjparamUser[2].Direction = ParameterDirection.Input;
            ObjparamUser[2].Value = SubmittedLoc;
            ObjparamUser[3] = new SqlParameter("@SubmittedTo", SqlDbType.NVarChar);
            ObjparamUser[3].Direction = ParameterDirection.Input;
            ObjparamUser[3].Value = SubmittedTo;
            ObjparamUser[4] = new SqlParameter("@ContNoSubTo", SqlDbType.NVarChar);
            ObjparamUser[4].Direction = ParameterDirection.Input;
            ObjparamUser[4].Value = ContNoSubTo;
            ObjparamUser[5] = new SqlParameter("@SubmittionDate", SqlDbType.DateTime);
            ObjparamUser[5].Direction = ParameterDirection.Input;
            ObjparamUser[5].Value = SubmittionDate;
            ObjparamUser[6] = new SqlParameter("@AckDate", SqlDbType.DateTime);
            ObjparamUser[6].Direction = ParameterDirection.Input;
            ObjparamUser[6].Value = AckDate;
            ObjparamUser[7] = new SqlParameter("@AwbNo", SqlDbType.NVarChar);
            ObjparamUser[7].Direction = ParameterDirection.Input;
            ObjparamUser[7].Value = AwbNo;
            ObjparamUser[8] = new SqlParameter("@UpdatedBy", SqlDbType.Int);
            ObjparamUser[8].Direction = ParameterDirection.Input;
            ObjparamUser[8].Value = UpdatedBy;
            return SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "UpdateBatchSubmissionStatus", ObjparamUser);

        }

        public int UpdateBatchNew(int batchid, int Pending_forCall, string remarks, string Follow_upDate, string Submission_status, string InvoiceStatus, string Dispute_Resolution_Status, int Payment_Available, double PaymentAmt_Promised, string Payment_Date_Promised, int Cheques_Ready, int Cheques_Picked, int Payment_Received, int CreatedBy, string AckDate, string SubmittedTo, string BatchType, double AmtRcvd, string AWBno,
                                  DateTime BatchDate, string submitedContNo, DateTime submissionDate)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[22];

            ObjparamUser[0] = new SqlParameter("@Batchid", SqlDbType.Int);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = batchid;

            ObjparamUser[1] = new SqlParameter("@Pending_forCall", SqlDbType.Bit);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = Pending_forCall;

            ObjparamUser[2] = new SqlParameter("@Batch_Remarks", SqlDbType.NVarChar);
            ObjparamUser[2].Direction = ParameterDirection.Input;
            ObjparamUser[2].Value = remarks;

            ObjparamUser[3] = new SqlParameter("@Follow_upDate", SqlDbType.DateTime);
            ObjparamUser[3].Direction = ParameterDirection.Input;
            // ObjparamUser[3].Value = Follow_upDate;
            if (Follow_upDate != "")
            {
                //ObjparamUser[3].Value = Follow_upDate;
                ObjparamUser[3].Value = Convert.ToDateTime(Follow_upDate);
            }
            else
            {
                ObjparamUser[3].Value = DBNull.Value;
            }

            ObjparamUser[4] = new SqlParameter("@Submission_status", SqlDbType.NVarChar);
            ObjparamUser[4].Direction = ParameterDirection.Input;
            ObjparamUser[4].Value = Submission_status;

            ObjparamUser[5] = new SqlParameter("@InvoiceStatus", SqlDbType.NVarChar);
            ObjparamUser[5].Direction = ParameterDirection.Input;
            ObjparamUser[5].Value = InvoiceStatus;

            ObjparamUser[6] = new SqlParameter("@Dispute_Resolution_Status", SqlDbType.NVarChar);
            ObjparamUser[6].Direction = ParameterDirection.Input;
            ObjparamUser[6].Value = Dispute_Resolution_Status;

            ObjparamUser[7] = new SqlParameter("@Payment_Available", SqlDbType.Int);
            ObjparamUser[7].Direction = ParameterDirection.Input;
            ObjparamUser[7].Value = Payment_Available;

            ObjparamUser[8] = new SqlParameter("@PaymentAmt_Promised", SqlDbType.Float);
            ObjparamUser[8].Direction = ParameterDirection.Input;
            ObjparamUser[8].Value = PaymentAmt_Promised;

            ObjparamUser[9] = new SqlParameter("@Payment_Date_Promised", SqlDbType.DateTime);
            ObjparamUser[9].Direction = ParameterDirection.Input;
            //ObjparamUser[9].Value = Payment_Date_Promised;
            if (Payment_Date_Promised != "")
            {
                //ObjparamUser[9].Value = Payment_Date_Promised;
                ObjparamUser[9].Value = Convert.ToDateTime(Payment_Date_Promised);
            }
            else
            {
                ObjparamUser[9].Value = DBNull.Value;
            }


            ObjparamUser[10] = new SqlParameter("@Cheques_Ready", SqlDbType.Int);
            ObjparamUser[10].Direction = ParameterDirection.Input;
            ObjparamUser[10].Value = Cheques_Ready;

            ObjparamUser[11] = new SqlParameter("@Cheques_Picked", SqlDbType.Int);
            ObjparamUser[11].Direction = ParameterDirection.Input;
            ObjparamUser[11].Value = Cheques_Picked;


            ObjparamUser[12] = new SqlParameter("@Payment_Received", SqlDbType.Int);
            ObjparamUser[12].Direction = ParameterDirection.Input;
            ObjparamUser[12].Value = Payment_Received;

            ObjparamUser[13] = new SqlParameter("@CreatedBy", SqlDbType.Int);
            ObjparamUser[13].Direction = ParameterDirection.Input;
            ObjparamUser[13].Value = CreatedBy;

            ObjparamUser[14] = new SqlParameter("@AckDate", SqlDbType.DateTime);
            ObjparamUser[14].Direction = ParameterDirection.Input;
            if (AckDate != "")
            {
                //ObjparamUser[9].Value = Payment_Date_Promised;
                ObjparamUser[14].Value = Convert.ToDateTime(AckDate);
            }
            else
            {
                ObjparamUser[14].Value = DBNull.Value;
            }

            ObjparamUser[15] = new SqlParameter("@SubmittedTo", SqlDbType.NVarChar);
            ObjparamUser[15].Direction = ParameterDirection.Input;
            ObjparamUser[15].Value = SubmittedTo;

            ObjparamUser[16] = new SqlParameter("@BatchType", SqlDbType.NVarChar);
            ObjparamUser[16].Direction = ParameterDirection.Input;
            ObjparamUser[16].Value = BatchType;

            ObjparamUser[17] = new SqlParameter("@AmtRcvd", SqlDbType.Float);
            ObjparamUser[17].Direction = ParameterDirection.Input;
            ObjparamUser[17].Value = AmtRcvd;

            ObjparamUser[18] = new SqlParameter("@AWBno", SqlDbType.NVarChar);
            ObjparamUser[18].Direction = ParameterDirection.Input;
            ObjparamUser[18].Value = AWBno;

            ObjparamUser[19] = new SqlParameter("@BatchDate", SqlDbType.DateTime);
            ObjparamUser[19].Direction = ParameterDirection.Input;
            ObjparamUser[19].Value = BatchDate;

            ObjparamUser[20] = new SqlParameter("@submitedContNo", SqlDbType.NVarChar);
            ObjparamUser[20].Direction = ParameterDirection.Input;
            ObjparamUser[20].Value = submitedContNo;

            ObjparamUser[21] = new SqlParameter("@submissionDate", SqlDbType.DateTime);
            ObjparamUser[21].Direction = ParameterDirection.Input;
            ObjparamUser[21].Value = submissionDate;

            //ObjparamUser[14] = new SqlParameter("@status", SqlDbType.Int);
            //ObjparamUser[14].Direction = ParameterDirection.Output;

            return SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "uspBatchUpdate_Test", ObjparamUser);
            //int status = Convert.ToInt32(ObjparamUser[14].Value.ToString());
            //return status;
        }

        public int UpdateVoidAgainst(int newbatchid, int oldbatchid, int updatedby)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[4];

            ObjparamUser[0] = new SqlParameter("@NewbatchId", SqlDbType.Int);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = newbatchid;

            ObjparamUser[1] = new SqlParameter("@OldBatchID", SqlDbType.Int);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = oldbatchid;

            ObjparamUser[2] = new SqlParameter("@UpdatedBy", SqlDbType.Int);
            ObjparamUser[2].Direction = ParameterDirection.Input;
            ObjparamUser[2].Value = updatedby;

            ObjparamUser[3] = new SqlParameter("@status", SqlDbType.Int);
            ObjparamUser[3].Direction = ParameterDirection.Output;

            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "uspVoidAgainst", ObjparamUser);
            int status = Convert.ToInt32(ObjparamUser[3].Value.ToString());
            return status;
        }


        public int VoidKnockingOff(int KnockingID, string ReasonforVoid, int Createdby)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[4];

            ObjparamUser[0] = new SqlParameter("@KnockingID", SqlDbType.Int);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = KnockingID;

            ObjparamUser[1] = new SqlParameter("@ReasonforVoid", SqlDbType.NVarChar);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = ReasonforVoid;

            ObjparamUser[2] = new SqlParameter("@Createdby", SqlDbType.Int);
            ObjparamUser[2].Direction = ParameterDirection.Input;
            ObjparamUser[2].Value = Createdby;

            ObjparamUser[3] = new SqlParameter("@status", SqlDbType.Int);
            ObjparamUser[3].Direction = ParameterDirection.ReturnValue;

            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "uspKnockVoid", ObjparamUser);
            int status = Convert.ToInt32(ObjparamUser[3].Value.ToString());
            return status;
        }

        public DataSet GetdataForRemarks(string batchid, string invId, int createdby, string remark, string mode)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[6];

            ObjparamUser[0] = new SqlParameter("@BatchID", SqlDbType.NVarChar);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = batchid;

            ObjparamUser[1] = new SqlParameter("@invoiceId", SqlDbType.NVarChar);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = invId;

            ObjparamUser[2] = new SqlParameter("@CreatedBy", SqlDbType.Int);
            ObjparamUser[2].Direction = ParameterDirection.Input;
            ObjparamUser[2].Value = createdby;

            ObjparamUser[3] = new SqlParameter("@Remarks", SqlDbType.NVarChar);
            ObjparamUser[3].Direction = ParameterDirection.Input;
            ObjparamUser[3].Value = remark;


            ObjparamUser[4] = new SqlParameter("@Mode", SqlDbType.NVarChar);
            ObjparamUser[4].Direction = ParameterDirection.Input;
            ObjparamUser[4].Value = mode;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspGetInvoiceRemarks", ObjparamUser);
        }

        public DataSet GetBatcStatusRights(int UserId)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[6];

            ObjparamUser[0] = new SqlParameter("@userid", SqlDbType.Int);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = UserId;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspGetBatchStatusRights", ObjparamUser);
        }

        public int AutoAdjust(int UserId)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[6];

            ObjparamUser[0] = new SqlParameter("@userid", SqlDbType.Int);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = UserId;

            return SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "uspknockingautoadjust", ObjparamUser);
        }

        public int resetknocking(int UserId)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[6];

            ObjparamUser[0] = new SqlParameter("@userid", SqlDbType.Int);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = UserId;

            return SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "uspknockingreset", ObjparamUser);
        }

        public DataSet deletetempknockdata(int UserId)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[6];

            ObjparamUser[0] = new SqlParameter("@userid", SqlDbType.Int);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = UserId;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspdelknocktemp", ObjparamUser);
        }

        public DataSet GEtinnerSeachKnock(string batchID, string InvoiceID, int knockingID, string mode)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[6];

            ObjparamUser[0] = new SqlParameter("@BatchID", SqlDbType.NVarChar);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = batchID;

            ObjparamUser[1] = new SqlParameter("@InvoiceID", SqlDbType.NVarChar);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = InvoiceID;

            ObjparamUser[2] = new SqlParameter("@KnockingID", SqlDbType.Int);
            ObjparamUser[2].Direction = ParameterDirection.Input;
            ObjparamUser[2].Value = knockingID;

            ObjparamUser[3] = new SqlParameter("@Mode", SqlDbType.NVarChar);
            ObjparamUser[3].Direction = ParameterDirection.Input;
            ObjparamUser[3].Value = mode;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspinnerSeachKnock", ObjparamUser);
        }

        public DataSet GEtinnerSeachKnock1(string batchID, string InvoiceID, int knockingID, string mode)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[6];

            ObjparamUser[0] = new SqlParameter("@BatchID", SqlDbType.NVarChar);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = batchID;

            ObjparamUser[1] = new SqlParameter("@InvoiceID", SqlDbType.NVarChar);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = InvoiceID;

            ObjparamUser[2] = new SqlParameter("@KnockingID", SqlDbType.Int);
            ObjparamUser[2].Direction = ParameterDirection.Input;
            ObjparamUser[2].Value = knockingID;

            ObjparamUser[3] = new SqlParameter("@Mode", SqlDbType.NVarChar);
            ObjparamUser[3].Direction = ParameterDirection.Input;
            ObjparamUser[3].Value = mode;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspinnerSeachKnock1", ObjparamUser);
        }


        //public DataSet GetPaymentID(string PaymentID, int city)
        //{
        //    SqlParameter[] ObjparamUser = new SqlParameter[2];

        //    ObjparamUser[0] = new SqlParameter("@Clientid", SqlDbType.NVarChar);
        //    ObjparamUser[0].Direction = ParameterDirection.Input;
        //    ObjparamUser[0].Value = PaymentID;

        //    ObjparamUser[1] = new SqlParameter("@cityid", SqlDbType.Int);
        //    ObjparamUser[1].Direction = ParameterDirection.Input;
        //    ObjparamUser[1].Value = city;


        //    return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspBindPaymentID", ObjparamUser);
        //}

        public DataSet GetPaymentID()
        {
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspBindPaymentIDopenunapplied");
        }

        public DataSet GetDistinctFYears()
        {
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspGetDistinctFinancialYears");
        }

        public DataSet GetSinglePaymentID(int FYear, int PayID)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];

            ObjparamUser[0] = new SqlParameter("@FYear", SqlDbType.Int);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = FYear;

            ObjparamUser[1] = new SqlParameter("@PayID", SqlDbType.Int);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = PayID;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspGetSinglePaymentID", ObjparamUser);
        }

        public DataSet GetKnockingOFFHeader(int PaymentID)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];

            ObjparamUser[0] = new SqlParameter("@PayId", SqlDbType.NVarChar);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = PaymentID;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "UspGetCompanyPaymentDetails1", ObjparamUser);
        }

        public DataSet GetPrevAdjAmount(int PaymentID)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];

            ObjparamUser[0] = new SqlParameter("@PaymentID", SqlDbType.NVarChar);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = PaymentID;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_PrevPaymentAdjusted", ObjparamUser);
        }

        public DataSet GetPrevAdjAmount_VoidAmmend(int PaymentID, int KnockingID)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];

            ObjparamUser[0] = new SqlParameter("@PaymentID", SqlDbType.NVarChar);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = PaymentID;

            ObjparamUser[1] = new SqlParameter("@KnockingID", SqlDbType.NVarChar);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = KnockingID;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prcVoidAmmend_PrevPaymentAdjusted", ObjparamUser);
        }

        public DataSet GetUpdateBatch(string BatchID, string CompID, string SubStatus, string Sort, int pageNO)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[5];

            ObjparamUser[0] = new SqlParameter("@BatchID", SqlDbType.NVarChar);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = BatchID;

            ObjparamUser[1] = new SqlParameter("@CompID", SqlDbType.NVarChar);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = CompID;

            ObjparamUser[2] = new SqlParameter("@SubmissionStatus", SqlDbType.NVarChar);
            ObjparamUser[2].Direction = ParameterDirection.Input;
            ObjparamUser[2].Value = SubStatus;

            ObjparamUser[3] = new SqlParameter("@SortStatus", SqlDbType.NVarChar);
            ObjparamUser[3].Direction = ParameterDirection.Input;
            ObjparamUser[3].Value = Sort;

            ObjparamUser[4] = new SqlParameter("@CurrentPage", SqlDbType.Int);
            ObjparamUser[4].Direction = ParameterDirection.Input;
            ObjparamUser[4].Value = pageNO;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspShowbatchStatus", ObjparamUser);
        }


        public static DataSet uspShowbatchInvoiceStatus(string BatchID, string CompID, string SubStatus, string Drang1, string Drang2, string Sort)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[6];

            ObjparamUser[0] = new SqlParameter("@BatchID", SqlDbType.NVarChar);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = BatchID;

            ObjparamUser[1] = new SqlParameter("@CompID", SqlDbType.NVarChar);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = CompID;

            ObjparamUser[2] = new SqlParameter("@SubmissionStatus", SqlDbType.NVarChar);
            ObjparamUser[2].Direction = ParameterDirection.Input;
            ObjparamUser[2].Value = SubStatus;

            ObjparamUser[3] = new SqlParameter("@dateRange1", SqlDbType.NVarChar);
            ObjparamUser[3].Direction = ParameterDirection.Input;
            ObjparamUser[3].Value = Drang1;

            ObjparamUser[4] = new SqlParameter("@dateRange2", SqlDbType.NVarChar);
            ObjparamUser[4].Direction = ParameterDirection.Input;
            ObjparamUser[4].Value = Drang2;


            ObjparamUser[5] = new SqlParameter("@SortStatus", SqlDbType.NVarChar);
            ObjparamUser[5].Direction = ParameterDirection.Input;
            ObjparamUser[5].Value = Sort;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspShowbatchStatus_invoicesubmission", ObjparamUser);
        }

        public DataSet GetKnockingPaymentDetails(int KnockingID)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[5];

            ObjparamUser[0] = new SqlParameter("@KnockingID", SqlDbType.Int);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = KnockingID;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "UspGetKnockingPaymentDetails", ObjparamUser);
        }


        public DataSet GetKnockSum(int Userid, int currentpage, int pagesize)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[5];

            ObjparamUser[0] = new SqlParameter("@Userid", SqlDbType.Int);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = Userid;

            ObjparamUser[1] = new SqlParameter("@currentpage", SqlDbType.Int);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = currentpage;

            ObjparamUser[2] = new SqlParameter("@pagesize", SqlDbType.Int);
            ObjparamUser[2].Direction = ParameterDirection.Input;
            ObjparamUser[2].Value = pagesize;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspknocksum", ObjparamUser);
        }

        public DataSet GetTotalInvValue(int Userid, int batchid)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[5];

            ObjparamUser[0] = new SqlParameter("@Userid", SqlDbType.Int);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = Userid;

            ObjparamUser[1] = new SqlParameter("@batchId", SqlDbType.Int);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = batchid;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "UspTotalInvoiceValue", ObjparamUser);
        }

        //Comment by Rakesh
        //public DataSet GetKnockingOffResult(string compname, string chkNo, string BankName, int intPaymentId, int city)
        //{
        //    SqlParameter[] ObjparamUser = new SqlParameter[5];

        //    ObjparamUser[0] = new SqlParameter("@CName", SqlDbType.NVarChar);
        //    ObjparamUser[0].Direction = ParameterDirection.Input;
        //    if (compname == "0")
        //    {
        //        compname = "";
        //    }
        //    ObjparamUser[0].Value = compname;

        //    ObjparamUser[1] = new SqlParameter("@CqNo", SqlDbType.NVarChar);
        //    ObjparamUser[1].Direction = ParameterDirection.Input;
        //    ObjparamUser[1].Value = chkNo;

        //    ObjparamUser[2] = new SqlParameter("@BName", SqlDbType.NVarChar);
        //    ObjparamUser[2].Direction = ParameterDirection.Input;
        //    ObjparamUser[2].Value = BankName;

        //    ObjparamUser[3] = new SqlParameter("@PaymentID", SqlDbType.Int);
        //    ObjparamUser[3].Direction = ParameterDirection.Input;
        //    ObjparamUser[3].Value = intPaymentId;

        //    ObjparamUser[4] = new SqlParameter("@cityid", SqlDbType.Int);
        //    ObjparamUser[4].Direction = ParameterDirection.Input;
        //    ObjparamUser[4].Value = city;

        //    return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "UspGetCompanyKnockingOFF", ObjparamUser);
        //}

        public DataSet GetKnockingOffResult(int intPaymentId, string chkNo)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];

            ObjparamUser[0] = new SqlParameter("@PaymentID", SqlDbType.Int);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = intPaymentId;

            ObjparamUser[1] = new SqlParameter("@CqNo", SqlDbType.NVarChar);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = chkNo;

            //return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "UspGetCompanyKnockingOFF", ObjparamUser);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "UspGetCompanyKnockingOFFopenpayment", ObjparamUser);
        }

        public DataSet GetKnockingOffResultInView(string compname, string chkNo, string BankName, int intPaymentId, int cityid)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[5];

            ObjparamUser[0] = new SqlParameter("@CName", SqlDbType.NVarChar);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            if (compname == "0")
            {
                compname = "";
            }
            ObjparamUser[0].Value = compname;

            ObjparamUser[1] = new SqlParameter("@CqNo", SqlDbType.NVarChar);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = chkNo;

            ObjparamUser[2] = new SqlParameter("@BName", SqlDbType.NVarChar);
            ObjparamUser[2].Direction = ParameterDirection.Input;
            ObjparamUser[2].Value = BankName;

            ObjparamUser[3] = new SqlParameter("@PaymentID", SqlDbType.Int);
            ObjparamUser[3].Direction = ParameterDirection.Input;
            ObjparamUser[3].Value = intPaymentId;

            ObjparamUser[4] = new SqlParameter("@cityid", SqlDbType.Int);
            ObjparamUser[4].Direction = ParameterDirection.Input;
            ObjparamUser[4].Value = cityid;


            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspKnockingView", ObjparamUser);
        }

        public int SendMailFoeBatchUpdate(string msg, string sub, string compid, string attachment)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[7];

            ObjparamUser[0] = new SqlParameter("@msg", SqlDbType.NVarChar);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = msg;

            ObjparamUser[1] = new SqlParameter("@sub", SqlDbType.NVarChar);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = sub;

            ObjparamUser[2] = new SqlParameter("@CompID", SqlDbType.NVarChar);
            ObjparamUser[2].Direction = ParameterDirection.Input;
            ObjparamUser[2].Value = compid;

            ObjparamUser[3] = new SqlParameter("@Atachment", SqlDbType.NVarChar);
            ObjparamUser[3].Direction = ParameterDirection.Input;
            ObjparamUser[3].Value = attachment;

            ObjparamUser[4] = new SqlParameter("@st", SqlDbType.Int);
            ObjparamUser[4].Direction = ParameterDirection.ReturnValue;

            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Send_Mail_Attachment", ObjparamUser);
            int status = Convert.ToInt32(ObjparamUser[4].Value.ToString());
            return status;
        }


        public int UpadatePageWise(int userid, int invoiceid, double totinvAmt, double adjAmt, double deductions, string remarks, double outstand, double unapplied)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[10];

            ObjparamUser[0] = new SqlParameter("@userid", SqlDbType.Int);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = userid;

            ObjparamUser[1] = new SqlParameter("@invoiceid", SqlDbType.Int);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = invoiceid;

            ObjparamUser[2] = new SqlParameter("@totalinvoiceAmt", SqlDbType.Decimal);
            ObjparamUser[2].Direction = ParameterDirection.Input;
            ObjparamUser[2].Value = totinvAmt;

            ObjparamUser[3] = new SqlParameter("@AdjustedAmt", SqlDbType.Decimal);
            ObjparamUser[3].Direction = ParameterDirection.Input;
            ObjparamUser[3].Value = adjAmt;

            ObjparamUser[4] = new SqlParameter("@deductions", SqlDbType.Decimal);
            ObjparamUser[4].Direction = ParameterDirection.Input;
            ObjparamUser[4].Value = deductions;

            ObjparamUser[5] = new SqlParameter("@remarks", SqlDbType.NVarChar);
            ObjparamUser[5].Direction = ParameterDirection.Input;
            ObjparamUser[5].Value = remarks;

            ObjparamUser[6] = new SqlParameter("@outstandingamount", SqlDbType.Decimal);
            ObjparamUser[6].Direction = ParameterDirection.Input;
            ObjparamUser[6].Value = outstand;

            ObjparamUser[7] = new SqlParameter("@unappliedcredit", SqlDbType.Decimal);
            ObjparamUser[7].Direction = ParameterDirection.Input;
            ObjparamUser[7].Value = unapplied;


            ObjparamUser[8] = new SqlParameter("@st", SqlDbType.Int);
            ObjparamUser[8].Direction = ParameterDirection.ReturnValue;

            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "uspsaveknocktemptab", ObjparamUser);
            int status = Convert.ToInt32(ObjparamUser[8].Value.ToString());
            return status;
        }

        public DataSet GetCallStatus(string BatchID, int PaymentAvailable, double PaymentAmtPromised, string PaymentPromisedDate, string Remarks, DateTime FollowupDate, int CreatedBy, string Mode)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[9];

            ObjparamUser[0] = new SqlParameter("@BatchID", SqlDbType.NVarChar);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = BatchID;

            ObjparamUser[1] = new SqlParameter("@PaymentAvailable", SqlDbType.NVarChar);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = PaymentAvailable;

            ObjparamUser[2] = new SqlParameter("@PaymentAmtPromised", SqlDbType.Decimal);
            ObjparamUser[2].Direction = ParameterDirection.Input;
            ObjparamUser[2].Value = PaymentAmtPromised;

            ObjparamUser[3] = new SqlParameter("@PaymentPromisedDate", SqlDbType.DateTime);
            ObjparamUser[3].Direction = ParameterDirection.Input;

            if (PaymentPromisedDate != "")
            {
                ObjparamUser[3].Value = Convert.ToDateTime(PaymentPromisedDate);
            }
            else
            {
                ObjparamUser[3].Value = DBNull.Value;
            }

            ObjparamUser[4] = new SqlParameter("@Remarks", SqlDbType.NVarChar);
            ObjparamUser[4].Direction = ParameterDirection.Input;
            ObjparamUser[4].Value = Remarks;


            ObjparamUser[5] = new SqlParameter("@FollowupDate", SqlDbType.DateTime);
            ObjparamUser[5].Direction = ParameterDirection.Input;
            ObjparamUser[5].Value = FollowupDate;


            ObjparamUser[6] = new SqlParameter("@CreatedBy", SqlDbType.Int);
            ObjparamUser[6].Direction = ParameterDirection.Input;
            ObjparamUser[6].Value = CreatedBy;

            ObjparamUser[7] = new SqlParameter("@Mode", SqlDbType.NVarChar);
            ObjparamUser[7].Direction = ParameterDirection.Input;
            ObjparamUser[7].Value = @Mode;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspGetCallStatusSummary", ObjparamUser);
        }

        public DataSet GetTranRemarksSub1(string BatchID)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[5];


            ObjparamUser[0] = new SqlParameter("@BatchID", SqlDbType.NVarChar);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = BatchID;



            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspReportTrRemarks_sub1", ObjparamUser);
        }

        public DataSet GetTranRemarksSub2(string BatchID, int currpage, int pagesize)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[5];


            ObjparamUser[0] = new SqlParameter("@BatchID", SqlDbType.NVarChar);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = BatchID;

            ObjparamUser[1] = new SqlParameter("@currentpage", SqlDbType.NVarChar);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = currpage;

            ObjparamUser[2] = new SqlParameter("@pagesize", SqlDbType.NVarChar);
            ObjparamUser[2].Direction = ParameterDirection.Input;
            ObjparamUser[2].Value = pagesize;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspReportTrRemarks_sub2", ObjparamUser);
        }

        public DataSet GetVoidKnoCkingOff(string CompId, int cityID, int currpage, int pagesize, string Strfrom, string strto)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[7];


            ObjparamUser[0] = new SqlParameter("@CompID", SqlDbType.NVarChar);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = CompId;

            ObjparamUser[1] = new SqlParameter("@cityid", SqlDbType.Int);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = cityID;

            ObjparamUser[2] = new SqlParameter("@currentpage", SqlDbType.Int);
            ObjparamUser[2].Direction = ParameterDirection.Input;
            ObjparamUser[2].Value = currpage;

            ObjparamUser[3] = new SqlParameter("@pagesize", SqlDbType.Int);
            ObjparamUser[3].Direction = ParameterDirection.Input;
            ObjparamUser[3].Value = pagesize;
            ObjparamUser[4] = new SqlParameter("@Start", SqlDbType.VarChar);
            ObjparamUser[4].Direction = ParameterDirection.Input;

            if (Strfrom == "")
            {
                ObjparamUser[4].Value = "1/1/1753";
            }
            else
            {
                ObjparamUser[4].Value = Strfrom;//Convert.ToDateTime(DepartureDate);
            }
            ObjparamUser[5] = new SqlParameter("@End", SqlDbType.VarChar);
            ObjparamUser[5].Direction = ParameterDirection.Input;
            if (strto == "")
            {
                ObjparamUser[5].Value = "12/31/9999";
            }
            else
            {
                ObjparamUser[5].Value = strto;//Convert.ToDateTime(DepartureDate);
            }

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspReportvoidKoncking", ObjparamUser);
        }

        public DataSet GetDedNotAccepetedCipl(string CompId, int cityId, int currpage, int pagesize)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[5];


            ObjparamUser[0] = new SqlParameter("@CompID", SqlDbType.NVarChar);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = CompId;

            ObjparamUser[1] = new SqlParameter("@CityID", SqlDbType.Int);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = cityId;

            ObjparamUser[2] = new SqlParameter("@currentpage", SqlDbType.Int);
            ObjparamUser[2].Direction = ParameterDirection.Input;
            ObjparamUser[2].Value = currpage;

            ObjparamUser[3] = new SqlParameter("@Pagesize", SqlDbType.Int);
            ObjparamUser[3].Direction = ParameterDirection.Input;
            ObjparamUser[3].Value = pagesize;


            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspReportDeduction_CIPL", ObjparamUser);
        }

        public DataSet GetVoidBatch(string CompId, string strFrom, string strTo, int curpage, int pagesize)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[5];


            ObjparamUser[0] = new SqlParameter("@CompID", SqlDbType.NVarChar);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = CompId;

            ObjparamUser[1] = new SqlParameter("@currentpage", SqlDbType.Int);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = curpage;

            ObjparamUser[2] = new SqlParameter("@pagesize", SqlDbType.Int);
            ObjparamUser[2].Direction = ParameterDirection.Input;
            ObjparamUser[2].Value = pagesize;

            ObjparamUser[3] = new SqlParameter("@From", SqlDbType.VarChar);
            ObjparamUser[3].Direction = ParameterDirection.Input;

            if (strFrom == "")
            {
                ObjparamUser[3].Value = "1/1/1753";
            }
            else
            {
                ObjparamUser[3].Value = strFrom;//Convert.ToDateTime(DepartureDate);
            }

            ObjparamUser[4] = new SqlParameter("@To", SqlDbType.VarChar);
            ObjparamUser[4].Direction = ParameterDirection.Input;
            if (strTo == "")
            {
                ObjparamUser[4].Value = "12/31/9999";
            }
            else
            {
                ObjparamUser[4].Value = strTo;
            }
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspReportVoidBatchDetail", ObjparamUser);
        }

        public DataSet GetPendingInvoice(string CompId, int cityId, string strFrom, string strTo, int curpage, int pagesize)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[6];


            ObjparamUser[0] = new SqlParameter("@CompID", SqlDbType.VarChar);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = CompId;

            ObjparamUser[1] = new SqlParameter("@cityid", SqlDbType.Int);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = cityId;

            ObjparamUser[2] = new SqlParameter("@From", SqlDbType.VarChar);
            ObjparamUser[2].Direction = ParameterDirection.Input;

            if (strFrom == "")
            {
                ObjparamUser[2].Value = "1/1/1753";
            }
            else
            {
                ObjparamUser[2].Value = strFrom;//Convert.ToDateTime(DepartureDate);
            }

            ObjparamUser[3] = new SqlParameter("@To", SqlDbType.VarChar);
            ObjparamUser[3].Direction = ParameterDirection.Input;
            if (strTo == "")
            {
                ObjparamUser[3].Value = "12/31/9999";
            }
            else
            {
                ObjparamUser[3].Value = strTo;
            }
            ObjparamUser[4] = new SqlParameter("@currentpage", SqlDbType.Int);
            ObjparamUser[4].Direction = ParameterDirection.Input;
            ObjparamUser[4].Value = curpage;

            ObjparamUser[5] = new SqlParameter("@pagesize", SqlDbType.Int);
            ObjparamUser[5].Direction = ParameterDirection.Input;
            ObjparamUser[5].Value = pagesize;
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspReportPendingInvoice", ObjparamUser);
        }


        public DataSet GetSubmitedInvoice(string compid, int cityid, string strFrom, string strTo, int CurrPage, int pagesize)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[6];

            ObjparamUser[0] = new SqlParameter("@CompID", SqlDbType.NVarChar);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = compid;

            ObjparamUser[1] = new SqlParameter("@CityID", SqlDbType.Int);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = cityid;

            ObjparamUser[2] = new SqlParameter("@Start", SqlDbType.VarChar);
            ObjparamUser[2].Direction = ParameterDirection.Input;

            if (strFrom == "")
            {
                ObjparamUser[2].Value = "1/1/1753";
            }
            else
            {
                ObjparamUser[2].Value = strFrom;//Convert.ToDateTime(DepartureDate);
            }

            ObjparamUser[3] = new SqlParameter("@End", SqlDbType.VarChar);
            ObjparamUser[3].Direction = ParameterDirection.Input;
            //ObjparamUser[2].Value = strDateto;
            if (strTo == "")
            {
                ObjparamUser[3].Value = "12/31/9999";
            }
            else
            {
                ObjparamUser[3].Value = strTo;//Convert.ToDateTime(DepartureDate);
            }


            ObjparamUser[4] = new SqlParameter("@currentpage", SqlDbType.Int);
            ObjparamUser[4].Direction = ParameterDirection.Input;
            ObjparamUser[4].Value = CurrPage;

            ObjparamUser[5] = new SqlParameter("@pagesize", SqlDbType.Int);
            ObjparamUser[5].Direction = ParameterDirection.Input;
            ObjparamUser[5].Value = pagesize;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspReportsubmitInvoice", ObjparamUser);
        }

        public DataSet GetTranRemarks(string BatchID, string ComName, int currentpage, int pagesize)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[5];

            ObjparamUser[0] = new SqlParameter("@CompName", SqlDbType.NVarChar);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = ComName;

            ObjparamUser[1] = new SqlParameter("@BatchID", SqlDbType.NVarChar);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = BatchID;

            ObjparamUser[2] = new SqlParameter("@currentpage", SqlDbType.Int);
            ObjparamUser[2].Direction = ParameterDirection.Input;
            ObjparamUser[2].Value = currentpage;

            ObjparamUser[3] = new SqlParameter("@pagesize", SqlDbType.Int);
            ObjparamUser[3].Direction = ParameterDirection.Input;
            ObjparamUser[3].Value = pagesize;


            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspReportTrRemarks", ObjparamUser);
        }

        public DataSet RptGetCompanyPaymentDetails(string CoId, int cityid, int currPage, int pagesize, string Strfrom, string strto)
        {
            DataSet dsFillData = new DataSet();
            SqlParameter[] objParams = new SqlParameter[8];
            objParams[0] = new SqlParameter("@CompID", CoId);
            objParams[1] = new SqlParameter("@Cityid", cityid);
            objParams[2] = new SqlParameter("@CurrentPage", currPage);

            objParams[3] = new SqlParameter("@pagesize", SqlDbType.Int);
            objParams[3].Direction = ParameterDirection.Input;
            objParams[3].Value = pagesize;
            objParams[4] = new SqlParameter("@Start", SqlDbType.VarChar);
            objParams[4].Direction = ParameterDirection.Input;

            if (Strfrom == "")
            {
                objParams[4].Value = "1/1/1753";
            }
            else
            {
                objParams[4].Value = Strfrom;//Convert.ToDateTime(DepartureDate);
            }
            objParams[5] = new SqlParameter("@End", SqlDbType.VarChar);
            objParams[5].Direction = ParameterDirection.Input;
            if (strto == "")
            {
                objParams[5].Value = "12/31/9999";
            }
            else
            {
                objParams[5].Value = strto;//Convert.ToDateTime(DepartureDate);
            }
            dsFillData = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspReportvoidPaymentDetail", objParams);
            return dsFillData;
        }


        public DataSet GetRPTUnappliedCreditData(string CompId, int cityId, int curpage, int pagesize)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[5];


            ObjparamUser[0] = new SqlParameter("@CompID", SqlDbType.VarChar);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = CompId;

            ObjparamUser[1] = new SqlParameter("@cityid", SqlDbType.Int);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = cityId;

            ObjparamUser[2] = new SqlParameter("@currentpage", SqlDbType.Int);
            ObjparamUser[2].Direction = ParameterDirection.Input;
            ObjparamUser[2].Value = curpage;

            ObjparamUser[3] = new SqlParameter("@pagesize", SqlDbType.Int);
            ObjparamUser[3].Direction = ParameterDirection.Input;
            ObjparamUser[3].Value = pagesize;


            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "RPT_uspGetUnappliedCredit", ObjparamUser);
        }


        public DataSet RptGetOutstandingAmountDetails(string CoId, string Dated, int currpage, int pagesize)
        {
            DataSet dsFillData = new DataSet();
            SqlParameter[] objParams = new SqlParameter[6];
            objParams[0] = new SqlParameter("@CompID", CoId);
            //objParams[1] = new SqlParameter("@Startdate", Dated);
            objParams[1] = new SqlParameter("@Startdate", SqlDbType.DateTime);
            objParams[1].Direction = ParameterDirection.Input;
            objParams[1].Value = Dated;
            objParams[2] = new SqlParameter("@CurrentPage", currpage);
            objParams[3] = new SqlParameter("@pagesize", pagesize);
            dsFillData = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspreportCreditLimit", objParams);
            return dsFillData;
        }

        public DataSet GetBatchCity(int PaymentId)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[6];

            ObjparamUser[0] = new SqlParameter("@PaymentID", SqlDbType.Int);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = PaymentId;

            //return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspsearchbatchCity", ObjparamUser);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspsearchbatchCityNew_11", ObjparamUser);
        }


        public DataSet Getsalesoncoustmerbasis(string strCompId, string city, string strDatefrom, string strDateto, string strasondate, int CurrentPage, int pagesize)
        {

            //strCompany, strFrom, strTo, strCity, strDate, currpage, Pagesize

            SqlParameter[] ObjparamUser = new SqlParameter[9];

            ObjparamUser[0] = new SqlParameter("@Comp", SqlDbType.NVarChar);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = strCompId;



            ObjparamUser[1] = new SqlParameter("@CityID", SqlDbType.NVarChar);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = city;



            ObjparamUser[2] = new SqlParameter("@From", SqlDbType.NVarChar);
            ObjparamUser[2].Direction = ParameterDirection.Input;

            if (strDatefrom == "")
            {
                ObjparamUser[2].Value = "1/1/1753";
            }
            else
            {
                ObjparamUser[2].Value = strDatefrom;//Convert.ToDateTime(DepartureDate);
            }

            ObjparamUser[3] = new SqlParameter("@To", SqlDbType.NVarChar);
            ObjparamUser[3].Direction = ParameterDirection.Input;
            //ObjparamUser[2].Value = strDateto;
            if (strDateto == "")
            {
                ObjparamUser[3].Value = "12/31/9999";
            }
            else
            {
                ObjparamUser[3].Value = strDateto;//Convert.ToDateTime(DepartureDate);
            }

            ObjparamUser[4] = new SqlParameter("@AsOnDate", SqlDbType.NVarChar);
            ObjparamUser[4].Direction = ParameterDirection.Input;

            if (strasondate == "")
            {
                ObjparamUser[4].Value = "12/31/9999";
            }
            else
            {
                ObjparamUser[4].Value = strasondate;//Convert.ToDateTime(DepartureDate);
            }


            ObjparamUser[5] = new SqlParameter("@currentpage", SqlDbType.Int);
            ObjparamUser[5].Direction = ParameterDirection.Input;
            ObjparamUser[5].Value = CurrentPage;


            ObjparamUser[6] = new SqlParameter("@pagesize", SqlDbType.Int);
            ObjparamUser[6].Direction = ParameterDirection.Input;
            ObjparamUser[6].Value = pagesize;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "usprpt_analysis_sales", ObjparamUser);
        }


        public DataSet GetAbcAnalysisOutstandingCustmerwise(string compid, string cityid, string asondate, int CurrPage, int Pagesize)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[5];

            ObjparamUser[0] = new SqlParameter("@Comp", SqlDbType.NVarChar);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = compid;


            ObjparamUser[1] = new SqlParameter("@cityid", SqlDbType.NVarChar);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = cityid;

            ObjparamUser[2] = new SqlParameter("@Asondate", SqlDbType.NVarChar);
            ObjparamUser[2].Direction = ParameterDirection.Input;

            if (asondate == "")
            {
                ObjparamUser[2].Value = "12/31/9999";
            }
            else
            {
                ObjparamUser[2].Value = asondate;//Convert.ToDateTime(DepartureDate);
            }

            ObjparamUser[3] = new SqlParameter("@CurrentPage", SqlDbType.Int);
            ObjparamUser[3].Direction = ParameterDirection.Input;
            ObjparamUser[3].Value = CurrPage;

            ObjparamUser[4] = new SqlParameter("@Pagesize", SqlDbType.Int);
            ObjparamUser[4].Direction = ParameterDirection.Input;
            ObjparamUser[4].Value = Pagesize;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "usprpt_ABC_basisof_Outstanding", ObjparamUser);
        }


        public DataSet GetAllCompanyName()
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "UspGetAllCompanyName");
        }

        public DataSet GetAmexBatch(int flag)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[1];
            ObjparamUser[0] = new SqlParameter("@flag", SqlDbType.Int);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = flag;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "UspGetAmexBatchNo", ObjparamUser);
        }

        public DataSet GetAmexBatch_Temp()
        {

            SqlParameter[] ObjparamUser = new SqlParameter[1];
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "UspGetAmexBatchNo_Temp");
        }


        public string CreateExcel(DataSet dsExcel, int[] strReportCol, string[] strReportcolHead, string strReportName, string strCompName, string strCity, string strFromdate, string strToDate, string strAsOnDate, string strmode, string fromtable)
        {
            string html = "";
            DataTable dtEx = new DataTable();
            dtEx = dsExcel.Tables[0];

            //****************To create an header of the excel************************************

            html = "<table border = 1 align = 'center'  width = 100%>  <tr>";
            html += "<td colspan='27'><font face = Tahoma size = 3 color = Blue><strong>";
            html += "Carzonrent (India) Private Limited </Strong> </font></td></tr>";
            html += "<tr> <td colspan='27'><font face = Tahoma size = 3 color = Black><strong>";
            html += "<br>" + strReportName + " </Strong> </font></td></tr>";
            html += "<tr><td colspan='27'><font face = Tahoma size = 2><Strong><U><br> </U> </Strong></font>";
            if (strCompName != "")
            {
                html += "<font face = Tahoma size = 2><b><br> Customer Name : " + strCompName + "  </font></b></td></tr>";
            }
            if (strCity != "")
            {
                html += "<tr><td colspan='27'><font face = Tahoma size = 2><b><br> Pickup city : " + strCity + "</font></b></td></tr>";
            }
            if (strFromdate != "")
            {
                html += "<tr><td colspan='27'><font face = Tahoma size = 2><b><br> Period From: " + strFromdate + "</font></b></td></tr>";
            }
            if (strToDate != "")
            {
                html += "<tr><td colspan='27'><font face = Tahoma size = 2><b><br> To : " + strToDate + "</font></b></td></tr>";
            }
            if (strAsOnDate != "")
            {
                html += "<tr><td colspan='27'><font face = Tahoma size = 2><b><br> As on Date : " + strAsOnDate + "</font></b></td></tr>";
            }
            if (strmode != "")
            {
                html += "<tr><td colspan='27'><font face = Tahoma size = 2><b><br> Mode of payment : " + strmode + "</font></b></td></tr>";
            }

            //html += "<tr><td colspan='27'><font face = Tahoma size = 2><b><br> Batch Id: " + lblBatchInvId + "</font></b></td></tr>";


            //************************To create array of column which are diplay on excel************************************

            //int[] iColumns = "{" + strReportCol + "}";
            int[] iColumns = strReportCol;
            string[] iColumnsheading = strReportcolHead;
            //***************************************************************************************************************

            for (int i = 0; i < dtEx.Rows.Count; i++)
            {
                if (i == 0)
                {
                    html += "<tr>";
                    if (fromtable == "T")
                    {
                        for (int j = 0; j < dtEx.Columns.Count; j++)
                        {
                            //html += "<td>" + dtEx.Columns[j].Caption.ToString() + "</td>";
                            html += "<td>" + "." + dtEx.Columns[j].Caption.ToString() + "</td>";
                        }
                    }
                    else
                    {
                        for (int j = 0; j < iColumnsheading.Length; j++)
                        {
                            //html += "<td>" + dtEx.Columns[j].Caption.ToString() + "</td>";
                            html += "<td>" + iColumnsheading[j].ToString() + "</td>";
                        }
                    }
                    html += "</tr>";
                }
                html += "<tr>";
                if (fromtable == "T")
                {
                    for (int j = 0; j < dtEx.Columns.Count; j++)
                    {
                        html += "<td>" + dtEx.Rows[i][j].ToString() + "</td>";
                    }
                }
                else
                {
                    for (int j = 0; j < iColumns.Length; j++)
                    {
                        html += "<td>" + dtEx.Rows[i][iColumns[j]].ToString() + "</td>";
                    }
                }
                html += "</tr>";
            }
            html += "</table>";
            return html;
        }

        public DataSet GetCorporateCompanyName()
        {
            try
            {
                SqlParameter[] ObjparamUser = new SqlParameter[2];

                return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "UspGetCorporateCompanyName");
            }
            catch
            {
                return null;
            }
        }

        public DataSet CCSBatchCreation_AddBookingID(int BookingID, int ClientCoID)
        {
            SqlParameter[] ObjParam = new SqlParameter[2];
            ObjParam[0] = new SqlParameter("@BookingID", SqlDbType.Int);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = BookingID;

            ObjParam[1] = new SqlParameter("@ClientCoID", SqlDbType.Int);
            ObjParam[1].Direction = ParameterDirection.Input;
            ObjParam[1].Value = ClientCoID;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "CCSBatchCreation_AddBookingID", ObjParam);
        }

        public void CCS_InsertTempBatchData(DataTable dtGridView, string TimeStamp, int UserID)
        {
            string ConString = System.Configuration.ConfigurationManager.AppSettings["ConnectionString"];
            SqlConnection MyCon = new SqlConnection(ConString);
            MyCon.Open();
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("Select * from CCSTempBatchData where 1=0", MyCon);
            da.SelectCommand = cmd;
            SqlCommandBuilder cBldr = new SqlCommandBuilder(da);
            da.Fill(ds);

            try
            {
                DataTable dt = ds.Tables["Table"];
                dt.TableName = "CCSTempBatchData";

                for (int i = 0; i < dtGridView.Rows.Count; i++)
                {
                    DataRow dr = ds.Tables[0].NewRow();
                    dr["TimeStamps"] = TimeStamp;
                    dr["BookingID"] = Convert.ToInt32(dtGridView.Rows[i]["BookingID"].ToString());
                    dr["InsertDate"] = DateTime.Now;
                    dr["InsertBy"] = UserID;
                    dt.Rows.Add(dr);
                }
                da.Update(ds, "CCSTempBatchData");
                ds.AcceptChanges();
                da.Dispose();
            }
            catch
            {
            }
            MyCon.Close();
            MyCon.Dispose();
        }

        public string CCSBatchCreation_SaveBatch(int ClientCoID, string CompType, int TotalInvoices, int TotalBatchAmt,
     int CreatedBy, string TimeStamp)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[8];

            ObjparamUser[0] = new SqlParameter("@ClientCoID", SqlDbType.Int);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = ClientCoID;

            ObjparamUser[1] = new SqlParameter("@CompType", SqlDbType.NVarChar);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = CompType;

            ObjparamUser[2] = new SqlParameter("@TotalInvoices", SqlDbType.Int);
            ObjparamUser[2].Direction = ParameterDirection.Input;
            ObjparamUser[2].Value = TotalInvoices;

            ObjparamUser[3] = new SqlParameter("@TotalBatchAmt", SqlDbType.Int);
            ObjparamUser[3].Direction = ParameterDirection.Input;
            ObjparamUser[3].Value = TotalBatchAmt;

            ObjparamUser[4] = new SqlParameter("@CreatedBy", SqlDbType.Int);
            ObjparamUser[4].Direction = ParameterDirection.Input;
            ObjparamUser[4].Value = CreatedBy;

            ObjparamUser[5] = new SqlParameter("@TimeStamp", SqlDbType.NVarChar);
            ObjparamUser[5].Direction = ParameterDirection.Input;
            ObjparamUser[5].Value = TimeStamp;

            ObjparamUser[6] = new SqlParameter("@BatchID", SqlDbType.VarChar, 20);
            ObjparamUser[6].Direction = ParameterDirection.Output;

            ObjparamUser[7] = new SqlParameter("@InternalBatch", SqlDbType.Int);
            ObjparamUser[7].Direction = ParameterDirection.Output;

            int status = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "CCSBatchCreation_SaveBatch", ObjparamUser);
            if (status > 0)
            {
                string BatchID = ObjparamUser[6].Value.ToString() + "~" + ObjparamUser[7].Value.ToString();
                return BatchID;
            }
            else
            {
                return "0~0";
            }
        }

        public int CheckForBatch_Knocking_Before_Void(int batchid)
        {
            SqlParameter[] objParam = new SqlParameter[1];
            objParam[0] = new SqlParameter("@Batchid", batchid);

            return Convert.ToInt32(SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "prc_check_Batch_Knocking", objParam));

        }

        public int InsertuspsaveKnocktemptab(int userid, int BatchID, string fkBatch, int invoiceid, string bookingid, double totinvAmt, double adjAmt, double deductions, string remarks, double outstand, double unapplied)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[13];

            ObjparamUser[0] = new SqlParameter("@userid", SqlDbType.Int);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = userid;

            ObjparamUser[1] = new SqlParameter("@BatchID", SqlDbType.Int);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = BatchID;

            ObjparamUser[2] = new SqlParameter("@fkBatch", SqlDbType.NVarChar);
            ObjparamUser[2].Direction = ParameterDirection.Input;
            ObjparamUser[2].Value = fkBatch;

            ObjparamUser[3] = new SqlParameter("@invoiceid", SqlDbType.Int);
            ObjparamUser[3].Direction = ParameterDirection.Input;
            ObjparamUser[3].Value = invoiceid;

            ObjparamUser[4] = new SqlParameter("@Bookingid", SqlDbType.NVarChar);
            ObjparamUser[4].Direction = ParameterDirection.Input;
            ObjparamUser[4].Value = bookingid;

            ObjparamUser[5] = new SqlParameter("@totalinvoiceAmt", SqlDbType.Decimal);
            ObjparamUser[5].Direction = ParameterDirection.Input;
            ObjparamUser[5].Value = totinvAmt;

            ObjparamUser[6] = new SqlParameter("@AdjustedAmt", SqlDbType.Decimal);
            ObjparamUser[6].Direction = ParameterDirection.Input;
            ObjparamUser[6].Value = adjAmt;

            ObjparamUser[7] = new SqlParameter("@deductions", SqlDbType.Decimal);
            ObjparamUser[7].Direction = ParameterDirection.Input;
            ObjparamUser[7].Value = deductions;

            ObjparamUser[8] = new SqlParameter("@remarks", SqlDbType.NVarChar);
            ObjparamUser[8].Direction = ParameterDirection.Input;
            ObjparamUser[8].Value = remarks;

            ObjparamUser[9] = new SqlParameter("@outstandingamount", SqlDbType.Decimal);
            ObjparamUser[9].Direction = ParameterDirection.Input;
            ObjparamUser[9].Value = outstand;

            ObjparamUser[10] = new SqlParameter("@unappliedcredit", SqlDbType.Decimal);
            ObjparamUser[10].Direction = ParameterDirection.Input;
            ObjparamUser[10].Value = unapplied;


            ObjparamUser[11] = new SqlParameter("@st", SqlDbType.Int);
            ObjparamUser[11].Direction = ParameterDirection.ReturnValue;

            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "uspsaveKnocktemptab_New", ObjparamUser);
            int status = Convert.ToInt32(ObjparamUser[11].Value.ToString());
            //int status = 0;
            return status;
        }

        public int SaveData_Mail(int clientID, string Email, string Remarks, bool status, int CreatedBy)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[7];

            ObjparamUser[0] = new SqlParameter("@ClientID", SqlDbType.Int);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = clientID;

            ObjparamUser[1] = new SqlParameter("@EmailID", SqlDbType.NVarChar);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = Email;

            ObjparamUser[2] = new SqlParameter("@Remarks", SqlDbType.NVarChar);
            ObjparamUser[2].Direction = ParameterDirection.Input;
            ObjparamUser[2].Value = Remarks;

            ObjparamUser[3] = new SqlParameter("@Status", SqlDbType.Int);
            ObjparamUser[3].Direction = ParameterDirection.Input;
            ObjparamUser[3].Value = status;

            ObjparamUser[4] = new SqlParameter("@CreatedBy", SqlDbType.Int);
            ObjparamUser[4].Direction = ParameterDirection.Input;
            ObjparamUser[4].Value = CreatedBy;

            ObjparamUser[5] = new SqlParameter("@ID", SqlDbType.Int);
            ObjparamUser[5].Direction = ParameterDirection.Output;

            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "InsertCreditLimit", ObjparamUser);
            int status1 = Convert.ToInt32(ObjparamUser[5].Value);
            return status1;
        }

        public static DataSet GetClientCreditMail(int ClientCoID)
        {
            try
            {

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@ClientCoID", ClientCoID);
                return SqlHelper.ExecuteDataset("GetClientCreditMail", param);
            }
            catch
            {
                return null;
            }
        }

        public static int UpdateCreditBalance(int ClientCoID, decimal BalanceAmt, string Remarks, int UpdatedBy)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[5];
                param[0] = new SqlParameter("@ClientCoID", SqlDbType.Int);
                param[0].Value = ClientCoID;

                param[1] = new SqlParameter("@BalanceAmt", DbType.Decimal);
                param[1].Value = BalanceAmt;

                param[2] = new SqlParameter("@Remarks", SqlDbType.VarChar);
                param[2].Value = Remarks;

                param[3] = new SqlParameter("@UpdatedBy", SqlDbType.Int);
                param[3].Value = UpdatedBy;

                param[4] = new SqlParameter("@ID", SqlDbType.Int);
                param[4].Direction = ParameterDirection.Output;
                SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "UpdateClientCreditBalance", param);
                int i = Convert.ToInt32(param[4].Value);
                return i;
            }
            catch
            {
                return 0;
            }
        }

        public static DataSet GetClientCreditBalance(int ClientCoID)
        {
            try
            {

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@ClientCoID", ClientCoID);
                return SqlHelper.ExecuteDataset("GetClientCreditBalance", param);
            }
            catch
            {
                return null;
            }
        }

        public static int CreditBalance(int ClientID, decimal BalanceAmt, string PaymentType, string remarks, bool Status, int CreateBy)
        {

            try
            {
                SqlParameter[] param = new SqlParameter[7];

                param[0] = new SqlParameter("@ClientCoID", SqlDbType.Int);
                param[0].Value = ClientID;

                param[1] = new SqlParameter("@BalanceAmt", DbType.Decimal);
                param[1].Value = BalanceAmt;

                param[2] = new SqlParameter("@PaymentType", SqlDbType.VarChar);
                param[2].Value = PaymentType;

                param[3] = new SqlParameter("@Remarks", SqlDbType.VarChar);
                param[3].Value = remarks;

                param[4] = new SqlParameter("@Status", SqlDbType.Bit);
                param[4].Value = Status;

                param[5] = new SqlParameter("@CreatedBy", SqlDbType.Int);
                param[5].Value = CreateBy;

                param[6] = new SqlParameter("@ID", SqlDbType.Int);
                param[6].Direction = ParameterDirection.Output;

                SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "InsertCreditBalance", param);
                int i = Convert.ToInt32(param[6].Value);
                return i;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public static int UpdateCreditMail(int ClientCoID, string MailID, string Remarks, bool Status, int UpdatedBy)
        {
            try
            {

                SqlParameter[] param = new SqlParameter[6];
                param[0] = new SqlParameter("@ClientCoID", SqlDbType.Int);
                param[0].Value = ClientCoID;

                param[1] = new SqlParameter("@MailID", SqlDbType.VarChar);
                param[1].Value = MailID;

                param[2] = new SqlParameter("@Remarks", SqlDbType.VarChar);
                param[2].Value = Remarks;

                param[3] = new SqlParameter("@Status", SqlDbType.Bit);
                param[3].Value = Status;

                param[4] = new SqlParameter("@UpdatedBy", SqlDbType.Int);
                param[4].Value = UpdatedBy;

                param[5] = new SqlParameter("@ID", SqlDbType.Int);
                param[5].Direction = ParameterDirection.Output;
                SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "UpdateClientCreditMail", param);
                int i = Convert.ToInt32(param[5].Value);
                return i;
            }
            catch
            {
                return 0;
            }
        }

        public DataSet GetMID(int UserID)
        {
            SqlParameter[] ObjParam = new SqlParameter[1];
            ObjParam[0] = new SqlParameter("@UserID", SqlDbType.Int);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = UserID;
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "getMID_UserAccessWise", ObjParam);
        }

        public DataSet CCManualBatch_AddBooking(int BookingID)
        {
            SqlParameter[] ObjParam = new SqlParameter[1];
            ObjParam[0] = new SqlParameter("@BookingID", SqlDbType.Int);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = BookingID;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "MIDBatch_AddBookingID", ObjParam);
        }

        public static int CCGenerateBatch(int UserID, string MIDDate, string MIDNo)
        {
            try
            {
                SqlParameter[] ObjParam = new SqlParameter[4];
                ObjParam[0] = new SqlParameter("@SessionID", SqlDbType.Int);
                ObjParam[0].Direction = ParameterDirection.Input;
                ObjParam[0].Value = UserID;

                ObjParam[1] = new SqlParameter("@MIDDate", SqlDbType.VarChar);
                ObjParam[1].Direction = ParameterDirection.Input;
                ObjParam[1].Value = MIDDate;

                ObjParam[2] = new SqlParameter("@MIDNo", SqlDbType.VarChar);
                ObjParam[2].Direction = ParameterDirection.Input;
                ObjParam[2].Value = MIDNo;

                ObjParam[3] = new SqlParameter("@BatchID", SqlDbType.Int);
                ObjParam[3].Direction = ParameterDirection.Output;

                SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "MIDGenerateBatch", ObjParam);
                int i = Convert.ToInt32(ObjParam[3].Value);
                return i;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public DataSet CCInsertBatchDetail
       (int BatchNo, string BookingID, string Company, string Guest, string location, string InvDate,
            double InvAmt, double ChargeAmt, int CreatedBy, bool status)
        {
            SqlParameter[] ObjParam = new SqlParameter[10];
            ObjParam[0] = new SqlParameter("@BatchNo", BatchNo);
            ObjParam[1] = new SqlParameter("@BookingID", BookingID);
            ObjParam[2] = new SqlParameter("@Company", Company);
            ObjParam[3] = new SqlParameter("@Guest", Guest);
            ObjParam[4] = new SqlParameter("@location", location);
            ObjParam[5] = new SqlParameter("@InvDate", InvDate);
            ObjParam[6] = new SqlParameter("@InvAmt", InvAmt);
            ObjParam[7] = new SqlParameter("@ChargeAmt", ChargeAmt);
            ObjParam[8] = new SqlParameter("@CreatedBy", CreatedBy);
            ObjParam[9] = new SqlParameter("@status", status);

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "MIDInsertBatchDetail", ObjParam);
        }


        public DataSet GetLocations_UserAccessWise1(int UserID)
        {
            SqlParameter[] ObjParam = new SqlParameter[1];
            ObjParam[0] = new SqlParameter("@UserID", SqlDbType.Int);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = UserID;
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "getLocations_UserAccessWise1", ObjParam);
        }

        public static DataSet GetCityCollectionBudget(int CityID)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@CityID", CityID);
                return SqlHelper.ExecuteDataset("GetCityCollectionBudget", param);
            }
            catch
            {
                return null;
            }
        }

        public DataSet GetCC_Batches(string FromDate, string ToDate, int Status)
        {
            SqlParameter[] ObjParam = new SqlParameter[3];
            ObjParam[0] = new SqlParameter("@FromDate", SqlDbType.VarChar);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = FromDate;

            ObjParam[1] = new SqlParameter("@ToDate", SqlDbType.VarChar);
            ObjParam[1].Direction = ParameterDirection.Input;
            ObjParam[1].Value = ToDate;

            ObjParam[2] = new SqlParameter("@Status", SqlDbType.Int);
            ObjParam[2].Direction = ParameterDirection.Input;
            ObjParam[2].Value = Status;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "getCCBatches", ObjParam);
        }

        public DataSet GetCCBatchDetail(int BatchNo)
        {
            SqlParameter[] ObjParam = new SqlParameter[1];
            ObjParam[0] = new SqlParameter("@BatchID", SqlDbType.Int);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = BatchNo;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_CCBatchModify", ObjParam);
        }

        public DataSet CCDeleteBatchDetail(int BatchNo)
        {
            SqlParameter[] ObjParam = new SqlParameter[1];
            ObjParam[0] = new SqlParameter("@BatchID", SqlDbType.Int);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = BatchNo;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_DeleteCCBatchDetail", ObjParam);
        }

        public DataSet GetCC_ApproveBatch(string FromDate, string ToDate, int Status)
        {
            SqlParameter[] ObjParam = new SqlParameter[3];

            ObjParam[0] = new SqlParameter("@FromDate", SqlDbType.VarChar);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = FromDate;

            ObjParam[1] = new SqlParameter("@ToDate", SqlDbType.VarChar);
            ObjParam[1].Direction = ParameterDirection.Input;
            ObjParam[1].Value = ToDate;

            ObjParam[2] = new SqlParameter("@Status", SqlDbType.Int);
            ObjParam[2].Direction = ParameterDirection.Input;
            ObjParam[2].Value = Status;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "getCC_ApproveBatch", ObjParam);
        }

        public void ApproveCCBatch(int BatchNo, int SysUserID)
        {
            SqlParameter[] ObjParam = new SqlParameter[2];
            ObjParam[0] = new SqlParameter("@BatchID", SqlDbType.Int);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = BatchNo;

            ObjParam[1] = new SqlParameter("@SysUserID", SqlDbType.Int);
            ObjParam[1].Direction = ParameterDirection.Input;
            ObjParam[1].Value = SysUserID;

            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "prcUpdateCCBatch_New", ObjParam);
        }

        public void VoidCCBatch(int BatchNo, int SysUserID)
        {
            SqlParameter[] ObjParam = new SqlParameter[2];
            ObjParam[0] = new SqlParameter("@BatchID", SqlDbType.Int);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = BatchNo;

            ObjParam[1] = new SqlParameter("@SysUserID", SqlDbType.Int);
            ObjParam[1].Direction = ParameterDirection.Input;
            ObjParam[1].Value = SysUserID;

            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "prcVoidCCBatch_New", ObjParam);
        }

        public static DataSet GetDateCollectionBudget(string FromDate)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@date", FromDate);
                return SqlHelper.ExecuteDataset("getCollection_Budget", param);
            }
            catch
            {
                return null;
            }
        }

        public static DataSet CollectionBudgetMaster(int CityID, int month_name, int year_name, decimal Amt, bool Active, int CreatedBy)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[6];

                param[0] = new SqlParameter("@CityID", SqlDbType.Int);
                param[0].Value = CityID;

                param[1] = new SqlParameter("@month_name", SqlDbType.Int);
                param[1].Value = month_name;

                param[2] = new SqlParameter("@year_name", SqlDbType.Int);
                param[2].Value = year_name;

                param[3] = new SqlParameter("@Amt", SqlDbType.Decimal);
                param[3].Value = Amt;

                param[4] = new SqlParameter("@Active", SqlDbType.Bit);
                param[4].Value = Active;

                param[5] = new SqlParameter("@CreatedBy", SqlDbType.Int);
                param[5].Value = CreatedBy;

                return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Proc_AddBudget_Collection_New", param);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static string CollectionBudgetBalance(int CityID, int month_name, int year_name, decimal Amt, bool Active, int CreatedBy)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[8];

                param[0] = new SqlParameter("@CityID", SqlDbType.Int);
                param[0].Value = CityID;

                param[1] = new SqlParameter("@month_name", SqlDbType.Int);
                param[1].Value = month_name;

                param[2] = new SqlParameter("@year_name", SqlDbType.Int);
                param[2].Value = year_name;

                param[3] = new SqlParameter("@Amt", SqlDbType.Decimal);
                param[3].Value = Amt;

                param[4] = new SqlParameter("@Active", SqlDbType.Bit);
                param[4].Value = Active;

                param[5] = new SqlParameter("@CreatedBy", SqlDbType.Int);
                param[5].Value = CreatedBy;

                param[6] = new SqlParameter("@StatusFlag", SqlDbType.Int);
                param[6].Direction = ParameterDirection.Output;

                param[7] = new SqlParameter("@ID", SqlDbType.Int);
                param[7].Direction = ParameterDirection.Output;

                SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_AddBudget_Collection", param);
                int StatusFlag = Convert.ToInt32(param[6].Value);
                int ID = Convert.ToInt32(param[7].Value);
                string CStatus = StatusFlag.ToString() + "#" + ID.ToString();
                return CStatus;
            }
            catch (Exception ex)
            {
                return "0";
            }
        }

        public static int UpdateCollectionMaster(int ID, decimal BalanceAmt, bool Status, int UpdatedBy)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[5];
                param[0] = new SqlParameter("@ID", SqlDbType.Int);
                param[0].Value = ID;

                param[1] = new SqlParameter("@BalanceAmt", DbType.Decimal);
                param[1].Value = BalanceAmt;

                param[2] = new SqlParameter("@Status", SqlDbType.Bit);
                param[2].Value = Status;

                param[3] = new SqlParameter("@UpdatedBy", SqlDbType.Int);
                param[3].Value = UpdatedBy;

                param[4] = new SqlParameter("@ID1", SqlDbType.Int);
                param[4].Direction = ParameterDirection.Output;
                SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "UpdateCityCollectionMaster", param);
                int i = Convert.ToInt32(param[4].Value);
                return i;
            }
            catch
            {
                return 0;
            }
        }

        public DataSet GetManualBatch()
        {
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspGetManualBatchNo");
        }

        public int GenerateManualBatches(int batchno3)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];
            ObjparamUser[0] = new SqlParameter("@batch3", SqlDbType.Int);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = batchno3;
            return SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "uspAutoGenBatch_Manual", ObjparamUser);
        }

        public DataSet GetManualBatchDetails(int batchno3)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];
            ObjparamUser[0] = new SqlParameter("@batchNo3", SqlDbType.Int);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = batchno3;
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_GetManualBatchDetails", ObjparamUser);
        }

        public DataSet GetUserDetail(int intuserid)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[1];
            ObjparamUser[0] = new SqlParameter("@UserID", intuserid);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_getFollowUp", ObjparamUser);
        }

        public DataSet GetUserDetailCreated(int intuserid)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[1];
            ObjparamUser[0] = new SqlParameter("@UserID", intuserid);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_getfollowupcreated", ObjparamUser);
        }

        public int CreateFollowUp(int UserID, int ClientCoId, string MettingDate, string MettingTime, string MettingAgenda, string ContactPerson, string ContactNo, string ActionTaken, string remarks, string NextmettingDate)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[11];

            ObjparamUser[0] = new SqlParameter("@ControllerID", SqlDbType.Int);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = UserID;

            ObjparamUser[1] = new SqlParameter("@ClientCoID", SqlDbType.Int);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = ClientCoId;

            ObjparamUser[2] = new SqlParameter("@MettingDate", SqlDbType.NVarChar);
            ObjparamUser[2].Direction = ParameterDirection.Input;
            ObjparamUser[2].Value = MettingDate;

            ObjparamUser[3] = new SqlParameter("@MettingTime", SqlDbType.NVarChar);
            ObjparamUser[3].Direction = ParameterDirection.Input;
            ObjparamUser[3].Value = MettingTime;

            ObjparamUser[4] = new SqlParameter("@MettingAgenda", SqlDbType.NVarChar);
            ObjparamUser[4].Direction = ParameterDirection.Input;
            ObjparamUser[4].Value = MettingAgenda;

            ObjparamUser[5] = new SqlParameter("@Contactperson", SqlDbType.NVarChar);
            ObjparamUser[5].Direction = ParameterDirection.Input;
            ObjparamUser[5].Value = ContactPerson;

            ObjparamUser[6] = new SqlParameter("@ContactNo", SqlDbType.NVarChar);
            ObjparamUser[6].Direction = ParameterDirection.Input;
            ObjparamUser[6].Value = ContactNo;

            ObjparamUser[7] = new SqlParameter("@Actiontaken", SqlDbType.NVarChar);
            ObjparamUser[7].Direction = ParameterDirection.Input;
            ObjparamUser[7].Value = ActionTaken;

            ObjparamUser[8] = new SqlParameter("@Remarks", SqlDbType.NVarChar);
            ObjparamUser[8].Direction = ParameterDirection.Input;
            ObjparamUser[8].Value = remarks;

            ObjparamUser[9] = new SqlParameter("@NextMettingDate", SqlDbType.NVarChar);
            ObjparamUser[9].Direction = ParameterDirection.Input;
            ObjparamUser[9].Value = NextmettingDate;

            ObjparamUser[10] = new SqlParameter("@ID", SqlDbType.Int);
            ObjparamUser[10].Direction = ParameterDirection.Output;

            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "prc_CreateFollowUp", ObjparamUser);
            int status = Convert.ToInt32(ObjparamUser[10].Value);
            return status;
        }

        public DataSet GetUsers(int UserID)
        {
            SqlParameter[] ObjParam = new SqlParameter[1];
            ObjParam[0] = new SqlParameter("@SysUserID", UserID);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_users", ObjParam);
        }

        public DataSet GetFollowUpReport(int UserID, string fromdate, string todate)
        {
            SqlParameter[] ObjParam = new SqlParameter[3];
            ObjParam[0] = new SqlParameter("@UserID", UserID);
            ObjParam[1] = new SqlParameter("@FromDate", fromdate);
            ObjParam[2] = new SqlParameter("@ToDate", todate);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_Getfollowupdetail", ObjParam);
        }

        public DataSet UpdateFollowup(int FollowUpID, string Mettingdate, string Agenda, string contactname, string contactno, string action, string remarks, string nextmettingdate, int UserID)
        {
            SqlParameter[] param = new SqlParameter[9];
            param[0] = new SqlParameter("@FollowUpID", FollowUpID);
            param[1] = new SqlParameter("@Mettingdate", Mettingdate);
            param[2] = new SqlParameter("@Agenda", Agenda);
            param[3] = new SqlParameter("@contactname", contactname);
            param[4] = new SqlParameter("@contactno", contactno);
            param[5] = new SqlParameter("@actionTaken", action);
            param[6] = new SqlParameter("@remarks", remarks);
            param[7] = new SqlParameter("@nextmettingdate", nextmettingdate);
            param[8] = new SqlParameter("@UserID", UserID);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "UpdateFollowUp", param);
        }

        public DataTable GetBatchCreatedDetail(String FromDate, String ToDate)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];

            ObjparamUser[0] = new SqlParameter("@DateFrom", FromDate);
            ObjparamUser[1] = new SqlParameter("@DateTo", ToDate);

            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "prc_getCompany_BatchDetail_New", ObjparamUser);
        }

        public static DataSet GetBatchInvoiceInfo(int BatchId, int userid)
        {
            SqlParameter[] parma = new SqlParameter[2];
            parma[0] = new SqlParameter("@BatchID", BatchId);
            parma[1] = new SqlParameter("@userID", userid);

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspGetVoidBatchInvoice", parma);
        }


        public DataTable GetVendorUtilizationDetail(String ToDate, int cityid)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];

            ObjparamUser[0] = new SqlParameter("@todate", ToDate);
            ObjparamUser[1] = new SqlParameter("@cityid", cityid);

            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_VendorCarUtilization_RevenueSharing_CityWise", ObjparamUser);
        }

        public DataTable GetBranchWiseDutiesSummary(String FromDate, String ToDate, int cityid)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[3];

            ObjparamUser[0] = new SqlParameter("@Fromdate", FromDate);
            ObjparamUser[1] = new SqlParameter("@todate", ToDate);
            ObjparamUser[2] = new SqlParameter("@cityid", cityid);

            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_getOpenDSDetails", ObjparamUser);
        }

        public DataTable GetDutiesAllocationSummary(String FromDate, String ToDate, int cityid)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[3];

            ObjparamUser[0] = new SqlParameter("@Fromdate", FromDate);
            ObjparamUser[1] = new SqlParameter("@todate", ToDate);
            ObjparamUser[2] = new SqlParameter("@cityid", cityid);

            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_GetAllocationSummary", ObjparamUser);
        }

        public DataTable GetDutiesAllocationDetail(String FromDate, String ToDate, int cityid)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[3];

            ObjparamUser[0] = new SqlParameter("@Fromdate", FromDate);
            ObjparamUser[1] = new SqlParameter("@todate", ToDate);
            ObjparamUser[2] = new SqlParameter("@cityid", cityid);

            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_GetAllocationDetail", ObjparamUser);
        }

        public int UpdateDetail(string Email, string Phone, int id)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[3];
            ObjparamUser[0] = new SqlParameter("@email", Email);
            ObjparamUser[1] = new SqlParameter("@phone", Phone);
            ObjparamUser[2] = new SqlParameter("@id", id);

            return SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "prc_update_Email_Phone", ObjparamUser);
            //return SqlHelper.ExecuteNonQuery(CommandType.Text, strQuery, ObjparamUser);
        }
        public DataTable ClsBindDeatils()
        {
            SqlParameter[] ObjparamUser = new SqlParameter[0];
            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "prc_retrieve_Email_Phone", ObjparamUser);

        }

        public int SaveThnxsDetails(string Name, string Mobile, string Email, string Location, int BatchId, int UpdatedBy)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[6];
            ObjparamUser[0] = new SqlParameter("@Name", Name);
            ObjparamUser[1] = new SqlParameter("@Mobile", Mobile);
            ObjparamUser[2] = new SqlParameter("@Email", Email);
            ObjparamUser[3] = new SqlParameter("@Location", Location);
            ObjparamUser[4] = new SqlParameter("@BatchId", BatchId);
            ObjparamUser[5] = new SqlParameter("@UpdatedBy", UpdatedBy);

            return SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "prc_UpdateClientSubmissionDeatil", ObjparamUser);

        }

        public DataTable ClsGetCity()
        {
            SqlParameter[] ObjparamUser = new SqlParameter[0];
            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_GetCity", ObjparamUser);

        }
        public static DataTable VerifyBatch(string BookingID, int UserId)
        {
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@BookingID", BookingID);
            param[1] = new SqlParameter("@UserID", UserId);
            return SqlHelper.ExecuteDatatable("prc_VerifyBatch", param);
        }
        public DataTable GetClientCreateBatch(int BookingID)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[1];
            ObjparamUser[0] = new SqlParameter("@BookingID", BookingID);
            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_GetClient_CreateBatch", ObjparamUser);

        }
        public string GetTotalInvoice(string BookingID)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[1];
            ObjparamUser[0] = new SqlParameter("@bookingId", BookingID);
            return SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "Prc_GetInvoice", ObjparamUser).ToString();

        }

        public DataSet ClsClientHide(int BatchID)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[1];
            ObjparamUser[0] = new SqlParameter("@BatchID", BatchID);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "PrcClient_HideBatchSubmissionD", ObjparamUser);

        }

        public string GetCreateBatch(string CompID, string compType, string DueDateOfPayment, string TotInvValue, int CreatedBy, string Batchsubmissiondate, string Remarks, string BatchIDN, string Mode)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[10];
            ObjparamUser[0] = new SqlParameter("@CompID", CompID);
            ObjparamUser[1] = new SqlParameter("@compType", compType);
            ObjparamUser[2] = new SqlParameter("@lblDueDateOfPayment", DueDateOfPayment);
            ObjparamUser[3] = new SqlParameter("@lblTotInvValue", TotInvValue);
            ObjparamUser[4] = new SqlParameter("@createdby", CreatedBy);
            ObjparamUser[5] = new SqlParameter("@Batchsubmissiondate", Batchsubmissiondate);
            ObjparamUser[6] = new SqlParameter("@Remarks", Remarks);
            ObjparamUser[7] = new SqlParameter("@BatchIDN", BatchIDN);
            ObjparamUser[8] = new SqlParameter("@Mode", Mode);
            ObjparamUser[9] = new SqlParameter("@Batch", SqlDbType.VarChar);
            ObjparamUser[9].Direction = ParameterDirection.Output;
            ObjparamUser[9].Size = 20;
            int status = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "uspCreateBatch_New", ObjparamUser);
            if (status > 0)
            {
                string BatchID = ObjparamUser[9].Value.ToString();
                return BatchID;
            }
            else
            {
                return "0";
            }
        }

        public DataTable GetBatchIDNew(string BatchID)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[1];
            ObjparamUser[0] = new SqlParameter("@BatchNo", BatchID);

            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_GetBatchID", ObjparamUser);
        }

        public int RefreshTableDetails(int UserId)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[1];
            ObjparamUser[0] = new SqlParameter("@UserId", UserId);
            return SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Prc_RefreshTable", ObjparamUser);

        }

        public DataTable VerifyCreateKnokingOffBatch(string PaymentId,int UserId)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];
            ObjparamUser[0] = new SqlParameter("@PaymentIds", PaymentId);
            ObjparamUser[1] = new SqlParameter("@UserID", UserId);
            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_AutoKnockingOff_New_1", ObjparamUser);
        
        }

        public int VerifyExcludeInvoiceBatch(string BookingId)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[1];
            ObjparamUser[0] = new SqlParameter("@BookingId", BookingId);
            return SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Prc_ExcludeInvoice", ObjparamUser);
        
        }

        public DataTable ClsGetCity1()
        {
            SqlParameter[] ObjparamUser = new SqlParameter[0];
            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_GetCity1", ObjparamUser);

        }
    }
}