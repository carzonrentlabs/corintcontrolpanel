using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System.Xml;

/// <summary>
/// Summary description for clsPayment
/// </summary>
namespace CCS
{
    public class clsPayment
    {
        public clsPayment()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public DataSet GetCorporateCompanyName()
        {
            try
            {
                SqlParameter[] ObjparamUser = new SqlParameter[2];

                return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "UspGetCorporateCompanyName");
            }
            catch
            {
                return null;
            }
        }

        public DataSet GetIndividualCompanyName()
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "UspGetIndividualCompanyName");
        }

        public DataSet GetAllCompanyName()
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "UspGetAllCompanyName");
        }

        public DataSet GetAllCompanyName1(int type)
        {
            SqlParameter[] objParams = new SqlParameter[1];
            objParams[0] = new SqlParameter("@type", type);

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "UspGetAllCompanyName1", objParams);
        }

        public DataSet GetCorporateCompanyData(string CName)
        {
            SqlParameter[] objParams = new SqlParameter[1];
            objParams[0] = new SqlParameter("@CName", CName);
            DataSet dsFillData = new DataSet();
            dsFillData = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "usp_GetCorporateCompanyData", objParams);
            return dsFillData;
        }

        public DataSet GetCorporateCompanyDataAdd(string CName)
        {
            SqlParameter[] objParams = new SqlParameter[1];
            objParams[0] = new SqlParameter("@CName", CName);
            DataSet dsFillData = new DataSet();
            dsFillData = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "usp_GetCorporateCompanyDataAdd", objParams);
            return dsFillData;
        }

        public DataSet GetMaxCorporateCompanyDataAdd(string CName)
        {
            SqlParameter[] objParams = new SqlParameter[1];
            objParams[0] = new SqlParameter("@CName", CName);
            DataSet dsFillData = new DataSet();
            dsFillData = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "usp_GetMaxAdditionalCreditLimitData", objParams);
            return dsFillData;
        }

        public DataSet GetBankName()
        {

            DataSet dsFillData = new DataSet();
            dsFillData = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "usp_GetBankname");
            return dsFillData;
        }

        public DataSet GetAdditionalCreditLimitData(string CName)
        {
            SqlParameter[] objParams = new SqlParameter[1];
            objParams[0] = new SqlParameter("@CName", CName);
            DataSet dsFillData = new DataSet();
            dsFillData = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "usp_GetAdditionalCreditLimitData", objParams);
            return dsFillData;
        }

        public int AddCreditLimit(string CoId, string AddLimit, DateTime ValidDate, string Reason, int CreatedBy)
        {
            SqlParameter[] objParams = new SqlParameter[6];
            objParams[0] = new SqlParameter("@CompID", CoId);
            objParams[1] = new SqlParameter("@ExtCreditLimit", AddLimit);
            objParams[2] = new SqlParameter("@Valid_upto", SqlDbType.DateTime);
            objParams[2].Value = ValidDate;
            //objParams[2] = new SqlParameter("@Valid_upto",SqlDbType.DateTime, ValidDate);
            objParams[3] = new SqlParameter("@ReasonForExtnd", Reason);
            objParams[4] = new SqlParameter("@CreatedBy", CreatedBy);
            objParams[5] = new SqlParameter("@UpdatedBy", CreatedBy);
            int result1;
            result1 = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "uspCreditLimit", objParams);
            return result1;
        }

        public DataSet GetCompanyServerDate(string CoId)
        {
            DataSet dsFillData = new DataSet();
            SqlParameter[] objParams = new SqlParameter[6];
            objParams[0] = new SqlParameter("@CName", CoId);
            dsFillData = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "UspGetCompanyName", objParams);
            return dsFillData;
        }

        public DataSet GetCompanyPaymentDetails(string CoId, string CqNo, string BName, int Cityid)
        {
            DataSet dsFillData = new DataSet();
            SqlParameter[] objParams = new SqlParameter[6];
            objParams[0] = new SqlParameter("@CName", CoId);
            objParams[1] = new SqlParameter("@CqNo", CqNo);
            objParams[2] = new SqlParameter("@BName", BName);
            objParams[3] = new SqlParameter("@Cityid", Cityid);
            dsFillData = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "UspGetCompanyPaymentDetails", objParams);

            return dsFillData;
        }

        public DataSet GetCompanyPaymentDetails1(int PayId)
        {
            DataSet dsFillData = new DataSet();
            SqlParameter[] objParams = new SqlParameter[6];
            objParams[0] = new SqlParameter("@PayId", PayId);
            dsFillData = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "UspGetCompanyPaymentDetails1", objParams);
            return dsFillData;
        }

        public int AddPaymentDetails(string CompID, string PAmt, string PMode, string Inumber, double Iamt, string DeBank, string DrBank, string Rname, DateTime Idate, DateTime CRDate, string Branch, string remarks, int UID, int cityid)
        {
            SqlParameter[] objParams = new SqlParameter[20];
            objParams[0] = new SqlParameter("@CompID", CompID);
            objParams[1] = new SqlParameter("@PayAmt", PAmt);
            objParams[2] = new SqlParameter("@PayMode", PMode);
            objParams[3] = new SqlParameter("@INo", Inumber);
            objParams[4] = new SqlParameter("@IAmt", Iamt);
            objParams[5] = new SqlParameter("@DeBank", DeBank);
            objParams[6] = new SqlParameter("@DrBank", DrBank);
            objParams[7] = new SqlParameter("@RName", Rname);
            objParams[8] = new SqlParameter("@IDate", SqlDbType.DateTime);
            objParams[8].Value = Idate;
            // objParams[8] = new SqlParameter("@IDate", Idate);
            objParams[9] = new SqlParameter("@CRDate", SqlDbType.DateTime);
            objParams[9].Value = CRDate;
            //objParams[9] = new SqlParameter("@CRDate", CRDate);
            objParams[10] = new SqlParameter("@DDBranch", Branch);
            objParams[11] = new SqlParameter("@Remarks", remarks);
            objParams[12] = new SqlParameter("@CreatedBy", UID);
            objParams[13] = new SqlParameter("@UpdatedBy", UID);
            objParams[14] = new SqlParameter("@Cityid", cityid);
            int Result2 = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "UspAddPaymentDetails", objParams);
            return Result2;
        }

        public int AddVoidPaymentDetails(string CompID, string PAmt, string PMode, string Inumber, double Iamt, string DeBank, string DrBank, string Rname, DateTime Idate, DateTime CRDate, string Branch, string remarks, int UID, int PayID, int cityid)
        {
            SqlParameter[] objParams = new SqlParameter[20];
            objParams[0] = new SqlParameter("@CompID", CompID);
            objParams[1] = new SqlParameter("@PayAmt", PAmt);
            objParams[2] = new SqlParameter("@PayMode", PMode);
            objParams[3] = new SqlParameter("@INo", Inumber);
            objParams[4] = new SqlParameter("@IAmt", Iamt);
            objParams[5] = new SqlParameter("@DeBank", DeBank);
            objParams[6] = new SqlParameter("@DrBank", DrBank);
            objParams[7] = new SqlParameter("@RName", Rname);
            objParams[8] = new SqlParameter("@IDate", SqlDbType.DateTime);
            objParams[8].Value = Idate;
            // objParams[8] = new SqlParameter("@IDate", Idate);
            objParams[9] = new SqlParameter("@CRDate", SqlDbType.DateTime);
            objParams[9].Value = CRDate;
            //objParams[9] = new SqlParameter("@CRDate", CRDate);
            objParams[10] = new SqlParameter("@DDBranch", Branch);
            objParams[11] = new SqlParameter("@Remarks", remarks);
            objParams[12] = new SqlParameter("@CreatedBy", UID);
            objParams[13] = new SqlParameter("@UpdatedBy", UID);
            objParams[14] = new SqlParameter("@PayID", PayID);
            objParams[15] = new SqlParameter("@Cityid", cityid);
            int Result2 = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "UspAddVoidPaymentDetails", objParams);
            return Result2;
        }

        public int AddVoidDetails(int PayId, string Remark, int CreatedBy)
        {
            SqlParameter[] objParams = new SqlParameter[6];
            objParams[0] = new SqlParameter("@PayID", PayId);
            objParams[1] = new SqlParameter("@Remark", Remark);
            objParams[5] = new SqlParameter("@UpdatedBy", CreatedBy);
            int result1;
            result1 = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "UspEditVoidPayment", objParams);

            return result1;
        }

        public int CheckPayentForVoid(int PayId)
        {
            SqlParameter[] objParams = new SqlParameter[6];
            objParams[0] = new SqlParameter("@PaymentId", SqlDbType.Int);
            objParams[0].Direction = ParameterDirection.Input;
            objParams[0].Value = PayId;

            objParams[1] = new SqlParameter("@a", SqlDbType.Int);
            objParams[1].Direction = ParameterDirection.ReturnValue;

            // return  Convert.ToInt32(SqlHelper.ExecuteScalar (CommandType.StoredProcedure, "uspcheckPaymentidstatus_1", objParams));
            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "uspcheckPaymentidstatus_1", objParams);
            int status = Convert.ToInt32(objParams[1].Value);
            return status;
            //int status = Convert.ToInt32(objParams[1].Value);
        }

        public string GetMaxPaymentID()
        {
            //return Convert.ToInt32(SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "UspGetMaxPaymentID"));
            return Convert.ToString(SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "UspGetMaxPaymentID"));
        }

        public DataSet GetCityName(int cityid)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];

            ObjparamUser[0] = new SqlParameter("@cityId", SqlDbType.Int);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = cityid;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspcityName", ObjparamUser);
        }

        public DataSet GetDetails(int BookingID)
        {
            SqlParameter[] objParams = new SqlParameter[6];
            objParams[0] = new SqlParameter("@BookingID", BookingID);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetBookingDetail", objParams);
        }

        public int AddReceipt_Recovery(int BookingID, double Amt, string Type, int CreatedBy)
        {
            SqlParameter[] objParams = new SqlParameter[4];
            objParams[0] = new SqlParameter("@BookingID", BookingID);
            objParams[1] = new SqlParameter("@Amt", Amt);
            objParams[2] = new SqlParameter("@Type", Type);
            objParams[3] = new SqlParameter("@UserID", CreatedBy);

            int result1;
            object obj = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "Prc_AddCashReceipt", objParams);
            //result1 = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Prc_AddCashReceipt", objParams);
            //return result1;
            result1 = Convert.ToInt32(obj);
            return result1;
        }

        public DataSet GetReceiptDetails(int BookingID)
        {
            SqlParameter[] objParams = new SqlParameter[1];
            objParams[0] = new SqlParameter("@BookingID", BookingID);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetReceiptDetails", objParams);
        }

       


        public int InsertReceipt(string BookingID, double ReceiptAmt, double RecoveryAmt, int CreatedBy, DateTime FromDate, DateTime ToDate)
        {
            SqlParameter[] objParams = new SqlParameter[6];
            objParams[0] = new SqlParameter("@BookingID", BookingID);
            objParams[1] = new SqlParameter("@ReceiptAmount", ReceiptAmt);
            objParams[2] = new SqlParameter("@RecoveryAmount", RecoveryAmt);
            objParams[3] = new SqlParameter("@UserID", CreatedBy);
            objParams[4] = new SqlParameter("@FromDate", FromDate);
            objParams[5] = new SqlParameter("@ToDate", ToDate);
            int result1;
            object obj = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "Prc_InsertReceipt", objParams);
           
            result1 = Convert.ToInt32(obj);
            return result1;
        }

        public DataSet IssueReceipt(int Receipt)
        {
            SqlParameter[] objParams = new SqlParameter[6];
            objParams[0] = new SqlParameter("@receiptID", Receipt);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_IssueReceipt", objParams);
        }
        //Export to excel receipt details
        public DataSet ReceiptDetails(int Receipt)
        {
            SqlParameter[] objParams = new SqlParameter[1];
            objParams[0] = new SqlParameter("@ReceiptID", Receipt);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetCashReceiptDetail", objParams);
        }

        public DataSet GetIssueReceiptDetails(string BookingID)
        {
            SqlParameter[] objParams = new SqlParameter[1];
            objParams[0] = new SqlParameter("@BookingID", BookingID);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetIssueDetails", objParams);
        }

        public DataSet GetReceiptToUploadDetails(string date, string Todate, int UserID)
        {
            SqlParameter[] objParams = new SqlParameter[3];
            objParams[0] = new SqlParameter("@Date", date);
            objParams[1] = new SqlParameter("@ToDate", Todate);
            objParams[2] = new SqlParameter("@UserID", UserID);
            //return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetReceiptToUpload", objParams);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetReceiptToUpload_New", objParams);
            
        }

        public DataSet GetPrintReceiptDetails(int Receipt)
        {
            SqlParameter[] objParams = new SqlParameter[1];
            objParams[0] = new SqlParameter("@ReceiptID", Receipt);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_PrintReceiptDetail", objParams);
        }

        public DataSet GetReceiptDetails_1(string Fromdate, string Todate, int UserID)
        {
            SqlParameter[] objParams = new SqlParameter[3];
            objParams[0] = new SqlParameter("@FromDate", Fromdate);
            objParams[1] = new SqlParameter("@ToDate", Todate);
            objParams[2] = new SqlParameter("@UserID", UserID);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetReceiptDetails_1", objParams);
        }

        public DataSet GetReceiptDetails_2(int UserID)
        {
            SqlParameter[] objParams = new SqlParameter[1];

            objParams[0] = new SqlParameter("@UserID", UserID);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetReceiptDetails_2", objParams);
        }

        public DataSet GetReceiptDetails_1_check(string Fromdate, string Todate, int UserID)
        {
            SqlParameter[] objParams = new SqlParameter[3];
            objParams[0] = new SqlParameter("@FromDate", Fromdate);
            objParams[1] = new SqlParameter("@ToDate", Todate);
            objParams[2] = new SqlParameter("@UserID", UserID);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetReceiptDetails_1_Check", objParams);
        }
        //Upload File Start
        //public class ReceiptUpload
        // {
        public int UploadVoucherFile(HttpFileCollection Files, int ReceiptID, int userID, string FileName)
        {
            try
            {
                //DateTime Today;
                //Today = DateTime.Now;

                //string FileName = "";
                int File1Status = 1;
                //FileName = ReceiptID.ToString() + "_"
                //     + DateTime.Now.Year.ToString() + "-"
                //    + DateTime.Now.Month.ToString() + "-"
                //   + DateTime.Now.Day.ToString() + ".pdf";

                for (int i = 0; i < Files.Count; i++)
                {
                    HttpPostedFile postFile = Files[i];
                    if (postFile.ContentLength > 0)
                    {

                        if ((postFile.ContentLength < 5000000) && (postFile.ContentType == "application/pdf"))
                        {
                            postFile.SaveAs("C:\\Upload\\Receipt\\" + FileName);
                            //postFile.SaveAs("C:\\" + FileName);
                            //Insta.Client.Insert_UploadEmail(BookingID, userID, FileName);
                        }
                        else
                        {
                            return -1;
                        }
                    }
                    else
                    {
                        //test
                    }
                }

                if ((File1Status == 0))
                {
                    return 0;
                }
                else
                {

                    return 1;  // single upload required
                }
            }
            catch
            {
                return 0;

            }
        }
        //}
        ////Upload File End

        public DataSet UpdateFileName(int ReceiptID, string FileName, int status)
        {
            SqlParameter[] objParams = new SqlParameter[3];
            objParams[0] = new SqlParameter("@ReceiptID", ReceiptID);
            objParams[1] = new SqlParameter("@FileName", FileName);
            objParams[2] = new SqlParameter("@Status", status);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_UpdateFileName", objParams);
        }

        public void UpdateReceiptStatus(string BookingID, string ReceiptID)
        {
            SqlParameter[] objParams = new SqlParameter[2];
            objParams[0] = new SqlParameter("@BookingID", BookingID);
            objParams[1] = new SqlParameter("@BatchID", ReceiptID);
            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Prc_UpdateReceiptStatus", objParams);
        }


        //For Sold out Exception
        public DataSet chkSoldOutException(string ClientID,int flag)
        {
            SqlParameter[] objParams = new SqlParameter[2];
            objParams[0] = new SqlParameter("@clientID", ClientID);
            objParams[1] = new SqlParameter("@flag", flag);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_SerachSoldOutException", objParams);
        }
        public int InsertSoldOutException(string clientId, string carCatId, string unitID, int CreatedBy, int Active)
        {
            SqlParameter[] objParams = new SqlParameter[5];
            objParams[0] = new SqlParameter("@ClientId", clientId);
            objParams[1] = new SqlParameter("@CarCatId", carCatId);
            objParams[2] = new SqlParameter("@UnitId", unitID);
            objParams[3] = new SqlParameter("@uid", CreatedBy);
            objParams[4] = new SqlParameter("@Active", Active);
            int result1;
            object obj = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "prc_InsertSoldOutException", objParams);
            result1 = Convert.ToInt32(obj);
            return result1;
        }

        public DataSet modifySoldOutException(int id)
        {
            SqlParameter[] objParams = new SqlParameter[1];
            objParams[0] = new SqlParameter("@Id", id);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_ModifySoldOut", objParams);
        }

        public int UpdateSoldOutException(int Id,int UnitId,int CatID ,int ClientId ,int status,int modifyBy )
        {
            SqlParameter[] objParams = new SqlParameter[6];
            objParams[0] = new SqlParameter("@Id", Id);
            objParams[1] = new SqlParameter("@UnitID", UnitId);
            objParams[2] = new SqlParameter("@CategoryID", CatID);
            objParams[3] = new SqlParameter("@ClientID", ClientId);
            objParams[4] = new SqlParameter("@Active", status);
            objParams[5] = new SqlParameter("@uid", modifyBy);
            return  SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "prc_UpdateSoldOutException", objParams);
        }

    }
}