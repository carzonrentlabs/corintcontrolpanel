﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System.Xml;
using System.Collections;
using CCS;

/// <summary>
/// Summary description for clsStateCity
/// </summary>
/// 
namespace SC
{
    public class clsStateCity
    {

        public int Retval;
        public string dbUser = "carzonrent";
        public string dbPWD = "cor@991yz";
        public clsStateCity()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        public int SaveStateMaster(string StateName, string StateShortCode, bool unionYN, int CreatedBy)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[4];
            ObjparamUser[0] = new SqlParameter("@StateName", StateName);
            ObjparamUser[1] = new SqlParameter("@StateShortCode", StateShortCode);
            ObjparamUser[2] = new SqlParameter("@unionYN", unionYN);
            ObjparamUser[3] = new SqlParameter("@CreatedBy", CreatedBy);

            return CCS.SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "dbo.bl_sp_saveStateMasterDetails", ObjparamUser);
        }

        public DataSet GetStateName()
        {
            return CCS.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "dbo.Bl_Sp_GetStateName");
        }

        public int UpdateStateMaster(int StateID, string StateName, string ShortCode, bool unionYN, bool Active, int UpdatedBy)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[6];

            ObjparamUser[0] = new SqlParameter("@StateID", StateID);
            ObjparamUser[1] = new SqlParameter("@StateName", StateName);
            ObjparamUser[2] = new SqlParameter("@StateShortCode", ShortCode);
            ObjparamUser[3] = new SqlParameter("@unionYN", unionYN);
            ObjparamUser[4] = new SqlParameter("@Active", Active);
            ObjparamUser[5] = new SqlParameter("@UpdatedBy", UpdatedBy);

            int status = 0;
            //return SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Sp_SaveSalesFollowUp", ObjparamUser);
            DataSet ds = new DataSet();
            ds = CCS.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "dbo.bl_Sp_Update_StateMasterDetails", ObjparamUser);
            if (ds.Tables[0].Rows.Count > 0)
            {
                status = Convert.ToInt32(ds.Tables[0].Rows[0]["ReturnID"]);
            }
            else
            {
                status = -1;
            }

            return status;
        }

        public DataSet GetStateMasterDtlsForUpdate(int StateID)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[1];
            ObjparamUser[0] = new SqlParameter("@StateId", StateID);

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "dbo.Bl_Sp_GetStateNameDtlsForUpdate", ObjparamUser);
        }
    }
}