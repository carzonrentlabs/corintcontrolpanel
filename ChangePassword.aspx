<%@ Page Language="vb" AutoEventWireup="false" src="ChangePassword.aspx.vb" Inherits="ChangePassword"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>ChangePassword</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="HertzInt.css" type="text/css" rel="stylesheet">
		<script language="javascript">
		function CheckLogin()
		{
			if (document.getElementById('txtPass').value == "" || document.getElementById('txtConfirmPass').value == "" )
			{
				alert("Enter password");
				document.getElementById('txtPass').focus();
				return false;
			}
			if (document.getElementById('txtPass').value != document.getElementById('txtConfirmPass').value)
			{	
				alert("Both password are not same.");
				document.getElementById('txtPass').value = "";
				document.getElementById('txtConfirmPass').value = "";
				document.getElementById('txtPass').focus();
				return false;
			}
			var minLength = 8;
			if (document.getElementById('txtPass').value.length < minLength) 
			{
			    alert('Your password must be at least ' + minLength + ' characters long. Try again.');
			    document.getElementById('txtPass').value = "";
			    document.getElementById('txtConfirmPass').value = "";
			    document.getElementById('txtPass').focus();
			    return false;
			}

			var ok = 0;
			var passValue = document.getElementById('txtPass').value;

			//- Upper case letters 
			if (passValue.match(/[A-Z]/) || passValue.match(/[a-z]/)) ok++;

			//- Digits 
			if (passValue.match(/[0-9]/)) ok++;

			//- Special characters (!, @, $, %, etc.) 
			if (passValue.match(/[@#$%&!*)(-+=^]/)) ok++;

			if (ok > 2) {
			    //alert('good password!'); 
			    //return false;
			}
			else {
			    alert('pick a stronger password - must contain at least one letter and one numeral and one special character');
			    //alert("The password must contain at least one letter and one numeral an.");
			    document.getElementById('txtPass').value = "";
			    document.getElementById('txtConfirmPass').value = "";
			    document.getElementById('txtPass').focus();
			    return false;

			}
			
			return true;
		}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<asp:panel id="pnlmain" Runat="server">
				<TABLE align="center">
					<TR>
						<TD class="redHead" align="center" colSpan="2"><BR>
							<BR>
							<BR>
							<BR>
							<BR>
							<BR>
							<BR>
							<B>Change Password :: Internal software<BR>
								control panel</B><BR>
							<BR>
						</TD>
					</TR>
					<TR>
						<TD class="subRedHead">New Password</TD>
						<TD align="center">
							<asp:textbox id="txtPass" runat="server" TextMode="Password" MaxLength="20" CssClass="input"></asp:textbox></TD>
					</TR>
					<TR>
						<TD class="subRedHead">Confirm Password</TD>
						<TD align="center">
							<asp:TextBox id="txtConfirmPass" runat="server" TextMode="Password" MaxLength="20" CssClass="input"></asp:TextBox></TD>
					</TR>
					<TR>
						<TD align="center" colSpan="2">
							<asp:Button id="btnLogin" runat="server" CssClass="input" Text="Login"></asp:Button></TD>
					</TR>
				</TABLE>
			</asp:panel>
		</form>
	</body>
</HTML>
