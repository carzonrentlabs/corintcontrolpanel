﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ViewUploadDocument.aspx.vb"
    Inherits="ViewUploadDocument" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>View Upload Document</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js" type="text/javascript"></script>

    <script type="text/javascript">
        var GB_ROOT_DIR = "http://insta.carzonrent.com/corIntControlpanel/greybox/";      
    </script>

    <script src="greybox/AJS.js" type="text/javascript"></script>

    <script src="greybox/AJS_fx.js" type="text/javascript"></script>

    <script src="greybox/gb_scripts.js" type="text/javascript"></script>

    <link href="greybox/gb_styles.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>

    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            $("#gvUploadedDocument a[id*='lnkView']").click(function () {
                var caption = "Vendor Document";
                var row = this.parentNode.parentNode;
                var rowIndex = row.rowIndex - 1;
                var linkId = this.id
                var hdFileName = linkId.replace("lnkView", "hdFileName")
                var fileName = $("#" + hdFileName).val();
                var _ClientId = fileName.substring(0, fileName.indexOf("_"))
                alert(_ClientId);
                if (fileName == 0) {

                    var url = "../Reports/FileNotFound.aspx"

                }
                else {
                    var _fileType = getUrlVars()["fType"];
                    alert(_fileType);
                    if (_ClientId != null && _fileType=='C') {
                        var url = "ImageViewer.aspx?fileName=" + fileName.toString() + "&_fileType=C";
                        alert(url)
                      
                    }
                    else if(_ClientId != null && _fileType=='S') {
                        var url = "http://192.168.2.10/corintcontrolpanel_test/ImageViewer.aspx?fileName=" + fileName.toString() + "&_fileType=S";
                        alert(url)
                        location.href = url;
                      
                    }      
                    else if(_ClientId != null && _fileType=='D') {
                        var url = "http://192.168.2.10/corintcontrolpanel_test/ImageViewer.aspx?fileName=" + fileName.toString() + "&_fileType=D";
                     
                    }             
                     else if(_ClientId != null && _fileType=='A') {
                         var url = "http://192.168.2.10/corintcontrolpanel_test/ImageViewer.aspx?fileName=" + fileName.toString() + "&_fileType=A";
                     
                    }  
                }
                

               // return GB_showCenter(caption, url, 500, 700)

            });

             function getUrlVars() {
                var vars = [], hash;
                var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
                for (var i = 0; i < hashes.length; i++) {
                    hash = hashes[i].split('=');
                    vars.push(hash[0]);
                    vars[hash[0]] = hash[1];
                }
                return vars;
            }
        });
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div align="center" id="dvView" runat="server">
            <h3>
                Vendor Document View
            </h3>
            <asp:GridView ID="gvUploadedDocument" runat="server" AutoGenerateColumns="false"
                EmptyDataText="No files uploaded">
                <Columns>
                    <asp:TemplateField HeaderText="File Name">
                        <ItemTemplate>
                            <asp:Label ID="lblFileName" Text='<% #Eval("Text") %>' runat="server"></asp:Label>
                            <asp:HiddenField ID="hdFileName" runat="server" Value='<%#Eval("Text") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Download">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkDownload" Text="Download" CommandArgument='<%# Eval("Value") %>'
                                runat="server" OnClick="DownloadFile"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkView" Text="View" CommandArgument='<%# Eval("Value") %>' runat="server"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <RowStyle BackColor="White" ForeColor="#330099" />
                <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
                <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
            </asp:GridView>
        </div>
    </form>
</body>
</html>
