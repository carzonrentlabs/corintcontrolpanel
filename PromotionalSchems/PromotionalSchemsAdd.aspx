<%@ Page Language="vb" AutoEventWireup="false" Src="PromotionalSchemsAdd.aspx.vb" Codebehind="PromotionalSchemsAdd.aspx.vb" Inherits="PromotionalSchemsAdd"%>
<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CarzonRent :: Internal software Control Panel</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../utilityfunction.js"></script>
		<script language="JavaScript" src="../JScripts/Datefunc.js"></script>
		<script language="JavaScript">
		function validate_input()
		{
			if(document.forms[0].txtPromotionCode.value=="")
				{
					alert("Please make sure all the fields marked with * are filled in.")
					return false;
				}
			else if(document.forms[0].txtPromotionName.value=="")
				{
					alert("Please make sure all the fields marked with * are filled in.")
					return false;
				}
			else if(document.forms[0].txtDiscount.value=="")
				{
					alert("Please make sure all the fields marked with * are filled in.")
					return false;
				}
			else if(document.forms[0].txtStartDate.value=="")
				{
					alert("Please make sure all the fields marked with * are filled in.")
					return false;
				}
			else if(document.forms[0].txtExpiryDate.value=="")
				{
					alert("Please make sure all the fields marked with * are filled in.")
					return false;
				}
			else if(document.forms[0].TxtSartCoupon.value=="")
				{
					alert("Please make sure all the fields marked with * are filled in.")
					return false;
				}
			else if(document.forms[0].TxtEndCoupon.value=="")
				{
					alert("Please make sure all the fields marked with * are filled in.")
					return false;
				}
			
			if(isNaN(document.forms[0].txtDiscount.value))
			{
				alert("% Discount should be numeric and upto two decimal digits only.")
				return false;	
			}
			else
			{
				if (parseFloat(document.forms[0].txtDiscount.value.indexOf("."))>0)
				{
					var FlotVal
					FlotVal=document.forms[0].txtDiscount.value.substr(parseFloat(document.forms[0].txtDiscount.value.indexOf(".")),document.forms[0].txtDiscount.value.length)
					if(parseFloat((FlotVal.length)-1)>2)
					{
					alert("% Discount should be numeric and upto two decimal digits only")
					return false;
					}
				}
			}
		}
		</script>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<uc1:headerctrl id="Headerctrl1" runat="server"></uc1:headerctrl>
			<table align="center">
				<TBODY>
					<TR>
						<TD align="center" colSpan="2"><B><U>Add a Promotion Master</U></B></TD>
					</TR>
					<tr>
						<td colspan="2" align="center"><asp:Label id="lblErrorMsg" cssclass="subRedHead" visible="false" runat="server"></asp:Label></td>
					</tr>
					<asp:Panel ID="pnlmainform" Runat="server">
						<TR>
							<TD>* Promotion Code
							</TD>
							<TD>
								<asp:textbox MaxLength="10" id="txtPromotionCode" runat="server" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>* Promotion Name
							</TD>
							<TD>
								<asp:textbox MaxLength="100" id="txtPromotionName" runat="server" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD vAlign="top">Promotion Description</TD>
							<TD>
								<asp:textbox id="txtPromotionDescription" onkeydown="showLength(this.form.txtPromotionDescription,'document.all.shwMessage',2000)"
									onkeyup="showLength(this.form.txtPromotionDescription,'document.all.shwMessage',2000)" runat="server"
									CssClass="input" MaxLength="2000" TextMode="MultiLine" Columns="50" Rows="3"></asp:textbox>
									<SPAN class="shwText" id="shwMessage">0</SPAN>/2000 chars
									</TD>
						</TR><TR>
							<TD vAlign="top">Service</TD>
						  <TD><asp:DropDownList ID="PMService" runat="server">
						    <asp:ListItem value="C">Chauffeur Drive</asp:ListItem>
						    <asp:ListItem value="S">Self Drive</asp:ListItem>
						    <asp:ListItem value="A">Airport</asp:ListItem>
						    <asp:ListItem value="T">City Transfer</asp:ListItem>
						  </asp:DropDownList>								</TD>
						</TR>
						<TR>
							<TD>* % Discount</TD>
							<TD>
								<asp:textbox id="txtDiscount" runat="server" MaxLength="50" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>* Start Date</TD>
							<TD>
								<asp:textbox id="txtStartDate" runat="server" CssClass="input" MaxLength="10" size="12"></asp:textbox><A onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;"
									href="javascript:show_calendar('Form1.txtStartDate');"><IMG height="21" src="../images/show-calendar.gif" width="24" border="0"></A>
								<%--	</FONT> 
								</SELECT>--%>
							</TD>
						</TR>
						<TR>
							<TD>* Expiry Date</TD>
							<TD>
								<asp:textbox id="txtExpiryDate" runat="server" CssClass="input" MaxLength="10" size="12"></asp:textbox><A onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;"
									href="javascript:show_calendar('Form1.txtExpiryDate');"><IMG height="21" src="../images/show-calendar.gif" width="24" border="0"></A>
									<%--</FONT> 
								</SELECT>--%>
								
							</TD>
						</TR>
						<TR>
							<TD>* Start Coupon</TD>
							<TD>
								<asp:textbox id="TxtSartCoupon" runat="server" MaxLength="20" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>* End Coupon</TD>
							<TD>
								<asp:textbox id="TxtEndCoupon" runat="server" MaxLength="20" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>* Max Count</TD>
							<TD>
								<asp:textbox id="txtMaxCount" runat="server" MaxLength="20" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
                            <TD width="278">* Car Category </TD>
                            <TD width="177"><asp:DropDownList id="ddlcarcat" runat="server" AutoPostBack="false"  CssClass="input"></asp:DropDownList></TD>
	                    </TR>
	                    
	                    <TR>
                            <TD>* Pick-up City Name</TD>
                            <TD><asp:DropDownList id=ddlcity runat="server" CssClass="input"></asp:DropDownList></TD>
	                    </TR>

						<TR>
							<TD>Active</TD>
							<TD>
								<asp:checkbox id="chkActive" runat="server" CssClass="input" Checked="True"></asp:checkbox></TD>
						</TR>
						<TR>
							<TD align="center" colSpan="2">
								<asp:button id="btn_Submit" runat="server" CssClass="input" Text="Submit"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;
								<INPUT type="reset" value="Reset" name="Reset" CssClass="button"></TD>
						</TR>
					</asp:Panel>
					<asp:Panel ID="pnlconfirmation" Visible="False" Runat="server">
						<TR align="center">
							<TD colSpan="2"><BR>
								<BR>
								<BR>
								<asp:Label id="lblMessage" runat="server"></asp:Label></TD>
						</TR>
						<TR align="center">
							<TD colSpan="2">
								<asp:HyperLink id="hyplnkretry" runat="server"></asp:HyperLink></TD>
						</TR>
					</asp:Panel></TBODY>
			</table>
		</form>
	</body>
</HTML>
