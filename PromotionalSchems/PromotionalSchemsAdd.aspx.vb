Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class PromotionalSchemsAdd
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btn_Submit As System.Web.UI.WebControls.Button
    Protected WithEvents txtPromotionCode As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtPromotionName As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtPromotionDescription As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtDiscount As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtStartDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtExpiryDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents TxtSartCoupon As System.Web.UI.WebControls.TextBox
    Protected WithEvents TxtEndCoupon As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkActive As System.Web.UI.WebControls.CheckBox
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents PMService As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlcarcat As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlcity As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtMaxCount As System.Web.UI.WebControls.TextBox

    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btn_Submit.Attributes("onClick") = "return validate_input();"
        lblErrorMsg.Visible = False
        If Not Page.IsPostBack Then
            populateddl()
        End If
    End Sub

    Private Sub btn_Submit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Submit.Click
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        Dim intuniqcheck As SqlParameter
        Dim intuniqvalue As Int32
        Dim intFlagCheck As SqlParameter
        Dim intFlag As Int32

        Dim intPkgidParam As SqlParameter
        Dim intPkgid As Int32
        Dim txtHr As String

        cmd = New SqlCommand("Proc_CP_AddPromotionMaster_Trips", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("@PromotionCode", txtPromotionCode.Text)
        cmd.Parameters.Add("@PromotionName", txtPromotionName.Text)
        cmd.Parameters.Add("@Service", PMService.SelectedValue)
        cmd.Parameters.Add("@PromotionDescription", txtPromotionDescription.Text)
        cmd.Parameters.Add("@PerDiscount", txtDiscount.Text)
        cmd.Parameters.Add("@StartDate", txtStartDate.Text)
        cmd.Parameters.Add("@ExpiryDate", txtExpiryDate.Text)
        cmd.Parameters.Add("@StartCoupon", TxtSartCoupon.Text)
        cmd.Parameters.Add("@EndCoupon", TxtEndCoupon.Text)
        cmd.Parameters.Add("@maxcount", txtMaxCount.Text)
        cmd.Parameters.Add("@CarcatID", ddlcarcat.SelectedItem.Value)
        cmd.Parameters.Add("@cityID", ddlcity.SelectedItem.Value)
        cmd.Parameters.Add("@Active", get_YNvalue(chkActive))
        cmd.Parameters.Add("@CreatedBy", Session("loggedin_user"))

        intuniqcheck = cmd.Parameters.Add("@PromotionID", SqlDbType.Int)
        intuniqcheck.Direction = ParameterDirection.Output
        intFlagCheck = cmd.Parameters.Add("@Flag", SqlDbType.Int)
        intFlagCheck.Direction = ParameterDirection.Output

        MyConnection.Open()
        cmd.ExecuteNonQuery()
        intuniqvalue = cmd.Parameters("@PromotionID").Value
        intFlag = cmd.Parameters("@Flag").Value

        MyConnection.Close()

        lblErrorMsg.Visible = True


        If intFlag = 0 Then

            pnlmainform.Visible = True
            pnlconfirmation.Visible = False

            lblErrorMsg.Text = "This Promotional scheme already exists!<br> The Promotion ID is <b>" & intuniqvalue & "</b>"
            hyplnkretry.Text = "Add another Promotional scheme"
            hyplnkretry.NavigateUrl = "PromotionalSchemsAdd.aspx"
        Else

            pnlmainform.Visible = False
            pnlconfirmation.Visible = True

            lblErrorMsg.Text = "You have added the Promotional scheme <br> The Promotion ID is <b>" & intuniqvalue & "</b>"
            hyplnkretry.Text = "Add another Promotional scheme"
            hyplnkretry.NavigateUrl = "PromotionalSchemsAdd.aspx"
        End If


    End Sub

    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function
    Sub populateddl()
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility

        ddlcity.DataSource = objAcessdata.funcGetSQLDataReader("select CityName ,CityID  from CORIntCityMaster   where active=1 and providerId=" & Session("provider_id") & " order by CityName")
        ddlcity.DataValueField = "CityID"
        ddlcity.DataTextField = "CityName"
        ddlcity.DataBind()
        ddlcity.Items.Insert(0, New ListItem("", ""))

        ddlcarcat.DataSource = objAcessdata.funcGetSQLDataReader("select CarCatName ,CarCatID  from CORIntCarCatMaster   where and providerId=" & Session("provider_id") & " active=1 order by CarCatName")
        ddlcarcat.DataValueField = "CarCatID"
        ddlcarcat.DataTextField = "CarCatName"
        ddlcarcat.DataBind()
        ddlcarcat.Items.Insert(0, New ListItem("", ""))

     


        objAcessdata.Dispose()

    End Sub
End Class
