<%@ Page Language="vb" AutoEventWireup="false" Src="PromotionLinkMasterAdd.aspx.vb" Codebehind="PromotionLinkMasterAdd.aspx.vb" Inherits="PromotionLinkMasterAdd"%>
<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CarzonRent :: Internal software Control Panel</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../utilityfunction.js"></script>
		<script language="JavaScript" src="../JScripts/Datefunc.js"></script>
		<script language="JavaScript">
		function validate_input()
		{
			if(document.Form1.ddlcompany.value=="")
				{
					alert("Please make sure all the fields marked with * are filled in.")
					return false;
				}
			else if(document.Form1.ddlPromotionID.value=="")
				{
					alert("Please make sure all the fields marked with * are filled in.")
					return false;
				}
		}
		</script>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<uc1:headerctrl id="Headerctrl1" runat="server"></uc1:headerctrl>
			<table align="center">
				<TBODY>
					<TR>
						<TD align="center" colSpan="2"><B><U>Add a Promotion Link Master</U></B></TD>
					</TR>
					<tr>
						<td colspan="2" align="center"><asp:Label id="lblErrorMsg" cssclass="subRedHead" visible="false" runat="server"></asp:Label></td>
					</tr>
					<asp:Panel ID="pnlmainform" Runat="server">
						<TR>
							<TD>* Company
							</TD>
							<TD>
								<asp:DropDownList id="ddlcompany" runat="server" cssclass="input" AutoPostBack="False"></asp:DropDownList></TD>
						</TR>
						<TR>
							<TD>* Promotion Code
							</TD>
							<TD>
								<asp:DropDownList id="ddlPromotionID" runat="server" cssclass="input" AutoPostBack="False"></asp:DropDownList></TD>
						</TR>
						
						<TR>
							<TD>Active</TD>
							<TD>
								<asp:checkbox id="chkActive" runat="server" CssClass="input" Checked="True"></asp:checkbox></TD>
						</TR>
						<TR>
							<TD align="center" colSpan="2">
								<asp:button id="btn_Submit" runat="server" CssClass="input" Text="Submit"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;
								<INPUT type="reset" value="Reset" name="Reset" CssClass="button"></TD>
						</TR>
					</asp:Panel>
					<asp:Panel ID="pnlconfirmation" Visible="False" Runat="server">
						<TR align="center">
							<TD colSpan="2"><BR>
								<BR>
								<BR>
								<asp:Label id="lblMessage" runat="server"></asp:Label></TD>
						</TR>
						<TR align="center">
							<TD colSpan="2">
								<asp:HyperLink id="hyplnkretry" runat="server"></asp:HyperLink></TD>
						</TR>
					</asp:Panel></TBODY>
			</table>
		</form>
	</body>
</HTML>
