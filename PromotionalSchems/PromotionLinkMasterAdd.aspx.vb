Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class PromotionLinkMasterAdd
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    Protected WithEvents ddlcompany As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlPromotionID As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtPromotionName As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtPromotionDescription As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtDiscount As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtStartDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtExpiryDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents TxtSartCoupon As System.Web.UI.WebControls.TextBox
    Protected WithEvents TxtEndCoupon As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkActive As System.Web.UI.WebControls.CheckBox
    Protected WithEvents btn_Submit As System.Web.UI.WebControls.Button
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btn_Submit.Attributes("onClick") = "return validate_input();"
        If Not Page.IsPostBack() Then
            populateddl()
        End If
    End Sub
    Sub populateddl()
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility
        ddlcompany.DataSource = objAcessdata.funcGetSQLDataReader("select ClientCoName  ,ClientCoID   from CORIntClientCoMaster    where active=1 order by ClientCoName ")
        ddlcompany.DataValueField = "ClientCoID"
        ddlcompany.DataTextField = "ClientCoName"
        ddlcompany.DataBind()
        ddlcompany.Items.Insert(0, New ListItem("", ""))

        ddlPromotionID.DataSource = objAcessdata.funcGetSQLDataReader("Select PromotionCode, PromotionID  from CorIntPromotionMaster where active=1 order by PromotionCode")
        ddlPromotionID.DataValueField = "PromotionID"
        ddlPromotionID.DataTextField = "PromotionCode"
        ddlPromotionID.DataBind()
        ddlPromotionID.Items.Insert(0, New ListItem("", ""))
        ddlPromotionID.Dispose()

    End Sub

    Private Sub btn_Submit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Submit.Click
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        Dim intuniqcheck As SqlParameter
        Dim intuniqvalue As Int32
        Dim intFlagCheck As SqlParameter
        Dim intFlag As Int32

        Dim intPkgidParam As SqlParameter
        Dim intPkgid As Int32
        Dim txtHr As String

        cmd = New SqlCommand("Proc_CP_AddCompPromotionLinkMaster", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("@ClientID", ddlcompany.SelectedValue)
        cmd.Parameters.Add("@PromotionID", ddlPromotionID.SelectedValue)
        cmd.Parameters.Add("@Active", get_YNvalue(chkActive))
        cmd.Parameters.Add("@CreatedBy", Session("loggedin_user"))

        intFlagCheck = cmd.Parameters.Add("@Flag", SqlDbType.Int)
        intFlagCheck.Direction = ParameterDirection.Output

        MyConnection.Open()
        cmd.ExecuteNonQuery()

        intFlag = cmd.Parameters("@Flag").Value
        MyConnection.Close()

        lblErrorMsg.Visible = True

        If intFlag = 0 Then

            pnlmainform.Visible = True
            pnlconfirmation.Visible = False

            lblErrorMsg.Text = "This Promotional scheme already exists for " & ddlcompany.SelectedItem.Text
            hyplnkretry.Text = "Add another Promotion Link Master"
            hyplnkretry.NavigateUrl = "PromotionLinkMasterAdd.aspx"
        Else

            pnlmainform.Visible = False
            pnlconfirmation.Visible = True

            lblErrorMsg.Text = "You have added the Promotion Link Master <br> The Promotion ID is <b>" & intFlag & "</b>"
            hyplnkretry.Text = "Add another Promotion Link Master"
            hyplnkretry.NavigateUrl = "PromotionLinkMasterAdd.aspx"
        End If

    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function
End Class
