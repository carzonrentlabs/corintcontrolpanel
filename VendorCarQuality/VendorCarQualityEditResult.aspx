<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Src="VendorCarQualityEditResult.aspx.vb" Inherits="VendorCarQualityEditResult"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CarzonRent :: Internal software Control Panel</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<uc1:Headerctrl id="Headerctrl1" runat="server"></uc1:Headerctrl>
			<table align="center">
				<tr>
					<td align="center"><STRONG><U>Edit a Car Vendor where</U></STRONG>
						<br>
						<br>
						<br>
					</td>
				</tr>
				<tr>	
					<td colspan="2" valign="top" >
						<asp:table ID="tblRecDetail" HorizontalAlign="Center" Runat="server" BorderColor="#cccc99"
							BorderStyle="Solid" GridLines="both" CellSpacing="0" CellPadding="0">
							<asp:tablerow>
							    <asp:tableheadercell>&nbsp;&nbsp;<asp:linkbutton runat="server" Text="ID" ID="Linkbutton1"></asp:linkbutton>&nbsp;&nbsp;</asp:tableheadercell>
								<asp:tableheadercell>&nbsp;&nbsp;<asp:linkbutton runat="server" Text="Vendor name" ID="Linkbutton2"></asp:linkbutton>&nbsp;&nbsp;</asp:tableheadercell>
								<asp:tableheadercell>&nbsp;&nbsp;<asp:linkbutton runat="server" Text="Registration #" ID="Linkbutton3"></asp:linkbutton>&nbsp;&nbsp;</asp:tableheadercell>
								<asp:tableheadercell>&nbsp;&nbsp;<asp:linkbutton runat="server" Text="Effective Date" ID="Linkbutton4"></asp:linkbutton>&nbsp;&nbsp;</asp:tableheadercell>								
								<asp:tableheadercell>&nbsp;&nbsp;<asp:linkbutton runat="server" Text="Quality Check" ID="Linkbutton5"></asp:linkbutton>&nbsp;&nbsp;</asp:tableheadercell>
								<asp:tableheadercell>
									&nbsp;&nbsp;
								</asp:tableheadercell>
							</asp:tablerow>
						</asp:table>
						<div id="MainDiv" runat="server" style="WIDTH:100%; HEIGHT:100%"></div>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
