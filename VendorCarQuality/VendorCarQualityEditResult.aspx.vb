Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.text
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Imports System.Web.UI
Public Class VendorCarQualityEditResult
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tblRecDetail As System.Web.UI.WebControls.Table

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not Page.IsPostBack Then
            getvalue("V.carvendorname")
        End If
    End Sub
    Sub getvalue(ByVal strorderby As String)
        Dim strquery As StringBuilder
        Dim dtrreader As SqlDataReader
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility

        strquery = New StringBuilder(" select a.*,b.regnno,c.CarVendorName from CORIntVendorCarQualityCheck as a (nolock)   " & "")
        strquery.Append(" inner join CorIntVendorCarMaster as b on a.VendorCarID=b.VendorCarID " & "")
        strquery.Append(" inner join CORIntCarVendorMaster as c on b.CarVendorID=c.CarVendorID " & "")

        If Request.QueryString("id") <> "-1" Then
            strquery.Append(" where b.CarVendorid=" & Request.QueryString("id") & " ")
        End If
        strquery.Append(" order by c.CarVendorName ")
        'response.write(strquery)
        'response.end()
        dtrreader = objAcessdata.funcGetSQLDataReader(strquery.ToString)
        While dtrreader.Read
            Dim Temprow As New TableRow
            Temprow.HorizontalAlign = HorizontalAlign.Center

            Dim Tempcell As New TableCell
            Tempcell.Controls.Add(New LiteralControl(dtrreader("ID") & ""))
            Temprow.Cells.Add(Tempcell)

            Dim Tempcel2 As New TableCell
            Tempcel2.Controls.Add(New LiteralControl(dtrreader("carvendorname") & ""))
            Temprow.Cells.Add(Tempcel2)

            Dim Tempcel3 As New TableCell
            Tempcel3.Controls.Add(New LiteralControl(dtrreader("regnno") & ""))
            Temprow.Cells.Add(Tempcel3)

            Dim Tempcel4 As New TableCell
            Tempcel4.Controls.Add(New LiteralControl(dtrreader("Effective_Date") & ""))
            Temprow.Cells.Add(Tempcel4)

            Dim Tempcel5 As New TableCell
            Tempcel5.Controls.Add(New LiteralControl(dtrreader("Quality_Check") & ""))
            Temprow.Cells.Add(Tempcel5)

            Dim Tempcel8 As New TableCell
            Tempcel8.Controls.Add(New LiteralControl("<a href=VendorCarQualityEditForm.aspx?ID=" & dtrreader("ID") & " >Edit</a>"))
            Temprow.Cells.Add(Tempcel8)
            tblRecDetail.Rows.Add(Temprow)

        End While
        dtrreader.Close()
        objAcessdata.Dispose()

    End Sub
    Sub SortGird(ByVal sender As Object, ByVal e As System.EventArgs)
        Select Case sender.id

            Case "Linkbutton1"
                getvalue("ID")
            Case "Linkbutton2"
                getvalue("carvendorname")
            Case "Linkbutton3"
                getvalue("regnno")
            Case "Linkbutton4"
                getvalue("EffectiveDate")
            Case "Linkbutton5"
                getvalue("Quality_Check")
        End Select
    End Sub

End Class