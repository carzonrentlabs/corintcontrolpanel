Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class VendorCarQualityEditForm
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ddlvendorname As System.Web.UI.WebControls.DropDownList
    Protected WithEvents chkAC As System.Web.UI.WebControls.CheckBox
    Protected WithEvents txtEffecDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnsubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnreset As System.Web.UI.WebControls.Button
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    Protected WithEvents chkDTO As System.Web.UI.WebControls.CheckBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btnsubmit.Attributes("onClick") = "return validation();"
        If Not Page.IsPostBack Then
            populateddl()
            Dim dtrreader As SqlDataReader
            Dim accessdata As clsutility
            accessdata = New clsutility
            dtrreader = accessdata.funcGetSQLDataReader("select b.*,a.Regnno from CORIntVendorCarMaster as a (nolock) inner join CORIntVendorCarQualityCheck as b (nolock) on a.VendorCarID=b.VendorCarID where  b.ID=" & Request.QueryString("id") & " and active = 1 ")
            dtrreader.Read()

            autoselec_ddl(ddlvendorname, dtrreader("vendorcarid"))
            'ddlvendorname.DataValueField = "vendorcarid"
            'ddlvendorname.DataTextField = "Regnno"
            'ddlvendorname.DataBind()
            'ddlvendorname.Items.Insert(0, New ListItem("Select Car No.", ""))

            'ddlvendorname.Text = dtrreader("regnno") & ""
            chkAC.Checked = dtrreader("Quality_Check")
            txtEffecDate.Text = dtrreader("Effective_Date")
            accessdata.Dispose()
        End If
    End Sub
    Sub populateddl()
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility

        ddlvendorname.DataSource = objAcessdata.funcGetSQLDataReader("select b.Regnno ,a.vendorcarid  from CORIntVendorCarQualityCheck as a (nolock) inner join CorIntvendorcarmaster as b (nolock) on a.vendorcarid=b.vendorcarid order by b.regnno")
        ddlvendorname.DataValueField = "vendorcarid"
        ddlvendorname.DataTextField = "Regnno"
        ddlvendorname.DataBind()
        ddlvendorname.Items.Insert(0, New ListItem("Select Car No.", ""))

        objAcessdata.Dispose()

    End Sub

    Function autoselec_ddl(ByVal ddlname As DropDownList, ByVal selectvalue As Int32)
        If Not selectvalue = "0" Then
            ddlname.Items.FindByValue(selectvalue).Selected = True
        End If
    End Function


    Private Sub btnreset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnreset.Click
        Response.Redirect("VendorCarQualityForm.aspx?id=" & Request.QueryString("id"))
    End Sub

    Private Sub btnsubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand

        Dim intuniqcheck As SqlParameter
        Dim intuniqvalue As int32

        cmd = New SqlCommand("procEditVendorCarQualityEdit", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("@vendorCarid", ddlvendorname.SelectedItem.Value)
        cmd.Parameters.Add("@acyn", get_YNvalue(chkAC))
        cmd.Parameters.Add("@EffectiveDate", txtEffecDate.Text)
        cmd.Parameters.Add("@Modifiedby", Session("loggedin_user"))

        '  Try
        MyConnection.Open()
        cmd.ExecuteNonQuery()
        MyConnection.Close()

        lblErrorMsg.visible = False
        pnlmainform.Visible = False
        pnlconfirmation.Visible = True
        lblMessage.Text = "You have updated the Vendor Car Quality successfully"
        hyplnkretry.Text = "Edit another Vendor Car Quality"
        hyplnkretry.NavigateUrl = "VendorCarQualitySearch.aspx"
        '   Catch
        '   End Try
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function

End Class
