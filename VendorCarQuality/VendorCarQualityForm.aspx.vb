Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class VendorCarQualityForm
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ddlvendorname As System.Web.UI.WebControls.DropDownList
    Protected WithEvents chkAC As System.Web.UI.WebControls.CheckBox
    Protected WithEvents txtEffecDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnsubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnreset As System.Web.UI.WebControls.Button
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    Protected WithEvents chkDTO As System.Web.UI.WebControls.CheckBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btnsubmit.Attributes("onClick") = "return validation();"
        If Not Page.IsPostBack Then
            'populateddl()
            Dim dtrreader As SqlDataReader
            Dim accessdata As clsutility
            accessdata = New clsutility
            ddlvendorname.DataSource = accessdata.funcGetSQLDataReader("select * from CORIntVendorCarMaster  where  carvendorid=" & Request.QueryString("id") & " and active = 1 ")
            'dtrreader.Read()
            ddlvendorname.DataValueField = "vendorcarid"
            ddlvendorname.DataTextField = "Regnno"
            ddlvendorname.DataBind()
            ddlvendorname.Items.Insert(0, New ListItem("Select Car No.", ""))

            'autoselec_ddl(ddlvendorname, dtrreader("vendorCarid"))
            'chkAC.Checked = dtrreader("acyn")
            'dtrreader.Close()
            accessdata.Dispose()
        End If
    End Sub

    Function autoselec_ddl(ByVal ddlname As DropDownList, ByVal selectvalue As Int32)
        If Not selectvalue = "0" Then
            'ddlname.Items.FindByValue(selectvalue).Selected = True
        End If
    End Function


    Private Sub btnreset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnreset.Click
        Response.Redirect("VendorCarQualityForm.aspx?id=" & Request.QueryString("id"))
    End Sub

    Private Sub btnsubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand

        Dim intuniqcheck As SqlParameter
        Dim intuniqvalue As int32

        cmd = New SqlCommand("procEditVendorCarQuality", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("@vendorCarid", ddlvendorname.SelectedItem.Value)
        cmd.Parameters.Add("@acyn", get_YNvalue(chkAC))
        cmd.Parameters.Add("@EffectiveDate", txtEffecDate.Text)
        cmd.Parameters.Add("@Createdby", Session("loggedin_user"))
        intuniqcheck = cmd.Parameters.Add("@uniqcheckval", SqlDbType.Int)
        intuniqcheck.Direction = ParameterDirection.Output

        '  Try
        MyConnection.Open()
        cmd.ExecuteNonQuery()
        intuniqvalue = cmd.Parameters("@uniqcheckval").Value
        MyConnection.Close()
        If Not intuniqvalue = 0 Then
            lblErrorMsg.visible = True
            lblErrorMsg.text = "Vendor Car Quality already exists for the month."
            Exit Sub
        Else
            lblErrorMsg.visible = False
            pnlmainform.Visible = False
            pnlconfirmation.Visible = True
            lblMessage.Text = "You have updated the Vendor Car successfully"
            hyplnkretry.Text = "Add another Vendor Car Quality"
            hyplnkretry.NavigateUrl = "VendorCarQuality.aspx"
        End If
        '   Catch
        '   End Try
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function

End Class
