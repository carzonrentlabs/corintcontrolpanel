<%@ Page Language="vb" AutoEventWireup="false" Src="VendorCarQualityEditForm.aspx.vb" Inherits="VendorCarQualityEditForm"%>
<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CarzonRent :: Internal software control panel</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="JavaScript" src="../utilityfunction.js"></script>
		<script language="JavaScript" src="../JScripts/Datefunc.js"></script>		
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
		<script language="javascript">
		function validation()
		{
			if (document.forms[0].txtEffecDate.value=="")
			{
					alert("Please Select Effective Date to be effective for the month..")
					return false;
			}
		}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<uc1:Headerctrl id="Headerctrl1" runat="server"></uc1:Headerctrl>
			<p>&nbsp;</p>
			<TABLE id="Table1" align="center">
				<asp:panel id="pnlmainform" Runat="server">
					<TBODY>
						<TR>
							<TD align="center" colSpan="2"><STRONG><U>Add a Vendor Car Quality</U></STRONG></TD>
						</TR>
						<TR>
							<TD align="center" colSpan="2">
								<asp:Label id="lblErrorMsg" runat="server" visible="false" cssclass="subRedHead"></asp:Label></TD>
						</TR>
						<TR>
							<TD align="center" colSpan="2">&nbsp;&nbsp;</TD>
						</TR>
						<TR>
							<TD>* Car Name
							</TD>
							<TD>
								<asp:DropDownList id="ddlvendorname" runat="server" CssClass="input" Enabled="false" ReadOnly="true"></asp:DropDownList></TD>
						</TR>
					 <TR>
							<TD>*&nbsp;Effective Month </TD>
						    <TD>
		<asp:textbox id="txtEffecDate" runat="server" CssClass="input" MaxLength="12" ReadOnly="true" size="12"></asp:textbox>
<!--<A onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;"
								href="javascript:show_calendar('Form1.txtEffecDate');"><IMG height="21" src="../images/show-calendar.gif" width="24" border="0"></A>-->
						</TD>
														
						</TR>
						<TR>
							<TD>Quality YN?</TD>
							<TD>
								<asp:checkbox id="chkAC" runat="server" CssClass="input" Checked="True"></asp:checkbox></TD>
						</TR>
					
						<TR>
							<TD align="center" colSpan="2">
								<asp:button id="btnsubmit" runat="server" CssClass="button" Text="Submit"></asp:button>&nbsp;&nbsp;
								<asp:button id="btnreset" runat="server" CssClass="button" Text="Reset" CausesValidation="False"></asp:button></TD>
						</TR>
				</asp:panel><asp:panel id="pnlconfirmation" Runat="server" Visible="False">
					<TR align="center">
						<TD colSpan="2">
							<asp:Label id="lblMessage" runat="server"></asp:Label></TD>
					</TR>
					<TR align="center">
						<TD colSpan="2">
							<asp:HyperLink id="hyplnkretry" runat="server"></asp:HyperLink></TD>
					</TR>
				</asp:panel></TBODY></TABLE>
		</form>
	</body>
</HTML>
