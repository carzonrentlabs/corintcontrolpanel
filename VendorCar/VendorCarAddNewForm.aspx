<%@ Page Language="vb" AutoEventWireup="false" Src="VendorCarAddNewForm.aspx.vb" Inherits="VendorCarAddNewForm"%>
<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CarzonRent :: Internal software control panel</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="JavaScript" src="../utilityfunction.js"></script>
		<script language="JavaScript" src="../JScripts/Datefunc.js"></script>
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
		
		<script language="javascript">
		function validation()
		{
	
			if(document.forms[0].ddlrevenue.value=="")
			{
				alert("Please select Revenue Sharing %")
				return false;
			}
			
			var strvalues
			strvalues=('txtregno','ddlrevenue')
			return checkmandatory(strvalues);
			}
			
		function dateReg(obj)
        {
            if(obj.value!="")
            {
                // alert(obj.value);
                var reg = /((0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[0-1])-(201[0-9]))/
                if(reg.test(obj.value))
                {
                    //alert('valid');
                }
                else
                {
                    alert('notvalid');
                    obj.value="";
                }
            }
        }			
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<uc1:Headerctrl id="Headerctrl1" runat="server"></uc1:Headerctrl>
			<TABLE id="Table1" align="center">
				<asp:panel id="pnlmainform" Runat="server">
					<TBODY>
						<TR>
							<TD align="center" colSpan="2"><STRONG><U>Add a Vendor Car</U></STRONG>
							</TD>
						</TR>
						<TR>
							<TD align="center" colSpan="2">
								<asp:Label id="lblErrorMsg" runat="server" visible="false" cssclass="subRedHead"></asp:Label></TD>
						</TR>
						<TR>
							<TD align="center" colSpan="2">&nbsp;&nbsp;</TD>
						</TR>
					<!--	<TR>
							<TD>* Vendor Name
							</TD>
							<TD>
								<asp:DropDownList id="ddlvendorname" runat="server" CssClass="input"></asp:DropDownList></TD>
						</TR>-->
						<TR>
							<TD>* Car No</TD>
							<TD>
								<asp:DropDownList id="txtregno" runat="server" CssClass="input"></asp:DropDownList></TD>
						</TR>
						
						
						
						
						
						<TR>
							<TD>Revenue Sharing %
							</TD>
							<TD>
								<asp:DropDownList id="ddlrevenue" runat="server" CssClass="input"></asp:DropDownList></TD>
						</TR>
						<TR>
							<TD>*&nbsp;Effective Date</TD>
		<TD>
								<asp:textbox id="txtEffecDate" runat="server" CssClass="input" MaxLength="12" onblur = "dateReg(this);" size="12"
								></asp:textbox><A onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;"
								href="javascript:show_calendar('Form1.txtEffecDate');"><IMG height="21" src="../images/show-calendar.gif" width="24" border="0"></A></TD>
														
						</TR>
						<TR>
							<TD align="center" colSpan="2">
								<asp:button id="btnsubmit" runat="server" CssClass="button" Text="Submit"></asp:button>&nbsp;&nbsp;
								<INPUT type="reset" value="Reset" name="Reset" CssClass="button"></TD>
						</TR>
				</asp:panel><asp:panel id="pnlconfirmation" Runat="server" Visible="False">
					<TR align="center">
						<TD colSpan="2">
							<asp:Label id="lblMessage" runat="server"></asp:Label></TD>
					</TR>
					<TR align="center">
						<TD colSpan="2">
							<asp:HyperLink id="hyplnkretry" runat="server"></asp:HyperLink></TD>
					</TR>
				</asp:panel></TBODY></TABLE>
		</form>
	</body>
</HTML>
