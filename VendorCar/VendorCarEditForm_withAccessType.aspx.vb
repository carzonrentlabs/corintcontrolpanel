Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports commonutility
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.IO
Imports System.IO.FileStream
Imports System.IO.File
Imports System.Net
Imports System.Text
Imports System.Object
Imports System.MarshalByRefObject
Imports System.Net.WebRequest
Public Class VendorCarEditForm
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ddlvendorname As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlmodel As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtregno As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtFC As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkAC As System.Web.UI.WebControls.CheckBox
    Protected WithEvents ddlfuel As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents chkdedicated As System.Web.UI.WebControls.CheckBox 'Commented on 03-01-2011
    Protected WithEvents ddlpayment As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlrevenue As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtRemarks As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkactive As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkmonthly As System.Web.UI.WebControls.CheckBox
    Protected WithEvents btnsubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnreset As System.Web.UI.WebControls.Button
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    'Protected WithEvents chkDTO As System.Web.UI.WebControls.CheckBox 'Commented on 03-01-2011
    'Code added by Rahul on 14 apr 2010
    Protected WithEvents txtInsuranceDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtFitnessDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkVDP As System.Web.UI.WebControls.CheckBox
    Protected WithEvents txtFileName As System.Web.UI.WebControls.TextBox
    Protected WithEvents FileUpload1 As System.Web.UI.WebControls.FileUpload
    'Protected WithEvents ddlyear As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtManufacturingYear As System.Web.UI.WebControls.TextBox

    Protected WithEvents ddlGrade As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlReportingUnit As System.Web.UI.WebControls.DropDownList

    Protected WithEvents HyperLink1 As System.Web.UI.WebControls.HyperLink

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btnsubmit.Attributes("onClick") = "return validation();"
        If Not Page.IsPostBack Then
            populateddl()
            Dim dtrreader As SqlDataReader
            Dim accessdata As clsutility
            accessdata = New clsutility
            dtrreader = accessdata.funcGetSQLDataReader("select *,isnull(FixedCost,0)as FC,isnull(VDPYN,0) as VDPYN1 from CORIntVendorCarMaster  where  vendorcarid=" & Request.QueryString("id") & " ")
            dtrreader.Read()

            autoselec_ddl(ddlvendorname, dtrreader("carvendorid"))
            autoselec_ddl(ddlmodel, dtrreader("carmodelid"))

            ddlGrade.Items.FindByValue(dtrreader("Grade")).Selected = True

            autoselec_ddl(ddlReportingUnit, dtrreader("FirstReportingUnit"))
            'autoselec_ddl(ddlyear, dtrreader("yearname"))
            'ddlyear

            txtregno.Text = dtrreader("regnno") & ""
            txtFC.Text = dtrreader("FC") & ""
            chkAC.Checked = dtrreader("acyn")
            ddlfuel.Items.FindByValue(dtrreader("fueltypeID")).Selected = True

            ddlpayment.Items.FindByValue(dtrreader("paymentoption")).Selected = True
            'chkdedicated.Checked = dtrreader("hertzdedicatedyn") 'Commented on 03-01-2011
            'chkDTO.Checked = dtrreader("dtoyn") 'Commented on 03-01-2011
            'Code added by Rahul on 14 apr 2010
            chkVDP.Checked = dtrreader("VDPYN1")

            txtInsuranceDate.Text = dtrreader("InsuranceDate")
            txtFitnessDate.Text = dtrreader("FitnessDate")

            txtManufacturingYear.Text = dtrreader("ManufacturingDate")

            autoselec_ddl(ddlrevenue, dtrreader("revenuesharepc"))
            txtRemarks.Text = dtrreader("remarks") & ""
            chkactive.Checked = dtrreader("active")
            chkmonthly.Checked = dtrreader("MonthlyYN")
            If (dtrreader("FileName") = "") Then
                txtFileName.Text = "No Image"
                'FileUpload1.Enabled = True
                FileUpload1.Visible = True
                HyperLink1.Text = ""
                'FileUpload1.Disabled = False
            Else
                txtFileName.Text = dtrreader("FileName")
                'FileUpload1.Disabled = True
                'FileUpload1.Enabled = False
                FileUpload1.Visible = False
                HyperLink1.Visible = True
                HyperLink1.Text = txtFileName.Text
                HyperLink1.NavigateUrl = "~/UploadImage/Addendum/" + txtFileName.Text
                HyperLink1.Target = "_blank"

                'FileUpload1.SaveAs(Server.MapPath("~/UploadImage/") + filename)

            End If
            dtrreader.Close()
            accessdata.Dispose()
        End If
    End Sub

    Sub populateddl()
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility

        Dim objAcessdataNw As clsutility
        objAcessdataNw = New clsutility
        Dim strquery As String
        Dim dtrreaderNew As SqlDataReader
        Dim strqueryvendor As String

        'Dim dtrreader As SqlDataReader
        Dim objAcessdataMapping As clsutility 'Changes
        objAcessdataMapping = New clsutility 'Changes
        Dim strqueryMapping As String 'Changes
        Dim dtrreaderMapping As SqlDataReader 'Changes
        Dim UnitID As String 'Changes


        'Dim AccessType As String

        strquery = New String("SELECT a.AccessType, a.UnitID, b.UnitCityID, c.Region FROM CORIntSysUsersMaster as a, CORIntUnitMaster as b, CORIntUnitCityMaster as c WHERE a.UnitID = b.UnitID and b.UnitCityID = c.UnitCityID and a.SysUserID =" & Session("loggedin_user"))
        dtrreaderNew = objAcessdataNw.funcGetSQLDataReader(strquery.ToString)

        Do While dtrreaderNew.Read

            'Changes
            If dtrreaderNew("AccessType") = "SU" Or dtrreaderNew("AccessType") = "RN" Then
                strqueryMapping = New String("select UM.UnitID, UM.referencedUnitID from corintunitmapping as UM, CORIntUnitMaster as b, CORIntUnitCityMaster as c WHERE UM.referencedUnitID = b.UnitID and b.UnitCityID = c.UnitCityID and UM.UnitID = " & dtrreaderNew("UnitID"))
            ElseIf dtrreaderNew("AccessType") = "CT" Then
                strqueryMapping = New String("select UM.UnitID, c.UnitCityID as referencedUnitID from corintunitmapping as UM, CORIntUnitMaster as b, CORIntUnitCityMaster as c WHERE UM.referencedUnitID = b.UnitID and b.UnitCityID = c.UnitCityID and UM.UnitID = " & dtrreaderNew("UnitID"))
            Else 'If dtrreaderNew("AccessType") = "RN" Then
                UnitID = dtrreaderNew("UnitID")
            End If

            If dtrreaderNew("AccessType") = "SU" Or dtrreaderNew("AccessType") = "CT" Or dtrreaderNew("AccessType") = "RN" Then
                dtrreaderMapping = objAcessdataMapping.funcGetSQLDataReader(strqueryMapping.ToString)

                Do While dtrreaderMapping.Read
                    UnitID = dtrreaderMapping("referencedUnitID")
                Loop

                dtrreaderMapping.Close()
            End If
            objAcessdataMapping.Dispose()
            'Changes

            If UnitID = "" Then
                UnitID = dtrreaderNew("UnitID")
            End If

            If dtrreaderNew("AccessType") = "SU" Or dtrreaderNew("AccessType") = "RN" Then
                strqueryvendor = New String("select distinct VCM.CarVendorID  ,VCM.CarVendorName  from CORIntCarVendorMaster as VCM, CORIntCityMaster as a, CORIntUnitCityMaster as b, CORIntUnitMaster as c WHERE VCM.CarVendorCityID = a.CityID and a.nearestUnitCityId=b.UnitCityId and a.Active = 1 and a.CityName <> '' and c.UnitCityID = b.UnitCityID and isnull(ApproveYN,0) = 1 and c.UnitID = '" & UnitID & "' order by CarVendorName")
            Else 'If dtrreaderNew("AccessType") = "CT" Then
                strqueryvendor = New String("select distinct VCM.CarVendorID  ,VCM.CarVendorName  from CORIntCarVendorMaster as VCM, CORIntCityMaster as a, CORIntUnitCityMaster as b, CORIntUnitMaster as c WHERE VCM.CarVendorCityID = a.CityID and a.nearestUnitCityId=b.UnitCityId and a.Active = 1 and a.CityName <> '' and c.UnitCityID = b.UnitCityID and isnull(ApproveYN,0) = 1 and c.UnitCityID = '" & UnitID & "' order by CarVendorName")
                'ElseIf dtrreaderNew("AccessType") = "RN" Then
                'strqueryvendor = New String("select distinct VCM.CarVendorID  ,VCM.CarVendorName  from CORIntCarVendorMaster as VCM, CORIntCityMaster as a, CORIntUnitCityMaster as b, CORIntUnitMaster as c WHERE VCM.CarVendorCityID = a.CityID and a.nearestUnitCityId=b.UnitCityId and a.Active = 1 and a.CityName <> '' and c.UnitCityID = b.UnitCityID and isnull(ApproveYN,0) = 1 and b.Region = '" & UnitID & "' order by CarVendorName")
                'Else
                'strqueryvendor = New String("select distinct VCM.CarVendorID  ,VCM.CarVendorName  from CORIntCarVendorMaster as VCM, CORIntCityMaster as a, CORIntUnitCityMaster as b, CORIntUnitMaster as c WHERE VCM.CarVendorCityID = a.CityID and a.nearestUnitCityId=b.UnitCityId and a.Active = 1 and a.CityName <> '' and c.UnitCityID = b.UnitCityID and isnull(ApproveYN,0) = 1 " & " order by CarVendorName")
            End If
        Loop

        dtrreaderNew.Close()
        objAcessdataNw.Dispose()

        'ddlvendorname.DataSource = objAcessdata.funcGetSQLDataReader("select CarVendorName ,carvendorid  from CORIntCarVendorMaster  order by CarVendorName")
        ddlvendorname.DataSource = objAcessdata.funcGetSQLDataReader(strqueryvendor)
        ddlvendorname.DataValueField = "carvendorid"
        ddlvendorname.DataTextField = "CarVendorName"
        ddlvendorname.DataBind()
        ddlvendorname.Items.Insert(0, New ListItem("", ""))

        ddlfuel.DataSource = objAcessdata.funcGetSQLDataReader("select FuelTypeID, FuelTypeName from CORIntFuelTypeMaster where active=1 order by FuelTypeName")
        ddlfuel.DataValueField = "FuelTypeID"
        ddlfuel.DataTextField = "FuelTypeName"
        ddlfuel.DataBind()
        ddlfuel.Items.Insert(0, New ListItem("", ""))

        ddlmodel.DataSource = objAcessdata.funcGetSQLDataReader("select M.carmodelid,C.carcompname+' '+M.carmodelname as carmodel from CORIntCarCompMaster C,CORIntCarModelMaster M where M.carcompid=C.carcompid  and M.active=1 order by carmodel")
        ddlmodel.DataValueField = "carmodelid"
        ddlmodel.DataTextField = "carmodel"
        ddlmodel.DataBind()
        ddlmodel.Items.Insert(0, New ListItem("", ""))

        ddlReportingUnit.DataSource = objAcessdata.funcGetSQLDataReader("select UnitID, UnitName from CORIntUnitMaster order by UnitName")
        ddlReportingUnit.DataValueField = "UnitID"
        ddlReportingUnit.DataTextField = "UnitName"
        ddlReportingUnit.DataBind()
        ddlReportingUnit.Items.Insert(0, New ListItem("", ""))

        'objAcessdata.funcpopulatenumddw(2020, 2000, ddlyear)
        objAcessdata.funcpopulatenumddw(100, 1, ddlrevenue)


        objAcessdata.Dispose()

    End Sub

    Function autoselec_ddl(ByVal ddlname As DropDownList, ByVal selectvalue As Int32)
        If Not selectvalue = "0" Then
            ddlname.Items.FindByValue(selectvalue).Selected = True
        End If
    End Function


    Private Sub btnreset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnreset.Click
        Response.Redirect("VendorCarEditForm.aspx?id=" & Request.QueryString("id"))
    End Sub

    Private Sub btnsubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsubmit.Click

        '**********************************************************************
        Dim str As String
        'Response.Write(FileUpload1.PostedFile)
        'Response.End()
        If txtFileName.Text = "No Image" Then
            Dim filename As String

            filename = Path.GetFileName(FileUpload1.FileName)
            'Response.Write(filename)
            'Response.End()

            str = filename

            'If (get_YNvalue(chkactive) = 0) Then
            If FileUpload1.PostedFile.FileName <> "" And FileUpload1.PostedFile.ContentLength > 0 Or get_YNvalue(chkactive) = 0 Then

                If filename = "" And get_YNvalue(chkactive) = 0 Then
                Else
                    FileUpload1.SaveAs(Server.MapPath("~/UploadImage/Addendum/") + filename)
                End If

                If str <> "" Or get_YNvalue(chkactive) = 0 Then


                    Dim MyConnection As SqlConnection
                    MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
                    Dim cmd As SqlCommand

                    Dim intuniqcheck As SqlParameter
                    Dim intuniqvalue As Int32
                    Dim revenueddl As Int32

                    If ddlrevenue.SelectedItem.Value = "" Then
                        revenueddl = 0
                    Else
                        revenueddl = ddlrevenue.SelectedItem.Value
                    End If

                    'Code added by Rahul on 14 apr 2010    
                    If txtFC.Text = "" Then
                        txtFC.Text = 0
                    End If

                    cmd = New SqlCommand("procEditVendorCarMaster", MyConnection)
                    cmd.CommandType = CommandType.StoredProcedure

                    cmd.Parameters.Add("@rowid", Request.QueryString("id"))
                    cmd.Parameters.Add("@carvendorid", ddlvendorname.SelectedItem.Value)
                    cmd.Parameters.Add("@carmodelid", ddlmodel.SelectedItem.Value)
                    cmd.Parameters.Add("@regnno", txtregno.Text)
                    cmd.Parameters.Add("@acyn", get_YNvalue(chkAC))
                    cmd.Parameters.Add("@fueltype", ddlfuel.SelectedItem.Value)
                    'cmd.Parameters.Add("@hertzdedicatedyn", get_YNvalue(chkdedicated)) 'Commented on 03-01-2011
                    ' Code added by BSL on 10 March '07
                    'cmd.Parameters.Add("@dto", get_YNvalue(chkDTO)) 'Commented on 03-01-2011
                    'Code added by Rahul on 14 apr 2010
                    cmd.Parameters.Add("@vdp", get_YNvalue(chkVDP))
                    cmd.Parameters.Add("@paymentoption", ddlpayment.SelectedItem.Value)
                    cmd.Parameters.Add("@revenuesharepc", revenueddl)
                    cmd.Parameters.Add("@remarks", txtRemarks.Text)
                    cmd.Parameters.Add("@active", get_YNvalue(chkactive))
                    cmd.Parameters.Add("@monthly", get_YNvalue(chkmonthly))
                    cmd.Parameters.Add("@modifiedby", Session("loggedin_user"))
                    cmd.Parameters.Add("@FixedCost", txtFC.Text)
                    cmd.Parameters.Add("@FileName", str)
                    cmd.Parameters.Add("@InsuranceDate", txtInsuranceDate.Text)
                    cmd.Parameters.Add("@FitnessDate", txtFitnessDate.Text)
                    'cmd.Parameters.Add("@year", ddlyear.SelectedItem.Value)
                    cmd.Parameters.Add("@ManufacturingYear", txtManufacturingYear.Text)

                    cmd.Parameters.Add("@Grade", ddlGrade.SelectedItem.Value)
                    cmd.Parameters.Add("@ReportingUnit", ddlReportingUnit.SelectedItem.Value)

                    intuniqcheck = cmd.Parameters.Add("@uniqcheckval", SqlDbType.Int)
                    intuniqcheck.Direction = ParameterDirection.Output



                    '  Try
                    MyConnection.Open()
                    cmd.ExecuteNonQuery()
                    intuniqvalue = cmd.Parameters("@uniqcheckval").Value
                    MyConnection.Close()
                    If intuniqvalue = 1 Then
                        lblErrorMsg.Visible = True
                        lblErrorMsg.Text = "Vendor car already exist."
                        Exit Sub
                    ElseIf intuniqvalue = 0 Then
                        lblErrorMsg.Visible = False
                        pnlmainform.Visible = False
                        pnlconfirmation.Visible = True
                        lblMessage.Text = "You have updated the Vendor Car successfully"
                        hyplnkretry.Text = "Edit another Vendor Car"
                        hyplnkretry.NavigateUrl = "VendorCarEditSearch.aspx"
                    ElseIf intuniqvalue = 2 Then
                        lblErrorMsg.Visible = True
                        lblErrorMsg.Text = "Image " & str & " already Uploaded."
                        Exit Sub
                    Else
                        lblErrorMsg.Visible = True
                        lblErrorMsg.Text = "Vendor car already exist."
                        Exit Sub
                    End If
                    '   Catch
                    '   End Try
                End If
            Else


                'End If
                lblErrorMsg.Visible = True
                lblErrorMsg.Text = "Please select Image to Upload."
                Exit Sub

            End If
        Else
            str = txtFileName.Text
            Dim MyConnection As SqlConnection
            MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
            Dim cmd As SqlCommand

            Dim intuniqcheck As SqlParameter
            Dim intuniqvalue As Int32
            Dim revenueddl As Int32

            If ddlrevenue.SelectedItem.Value = "" Then
                revenueddl = 0
            Else
                revenueddl = ddlrevenue.SelectedItem.Value
            End If

            'Code added by Rahul on 14 apr 2010    
            If txtFC.Text = "" Then
                txtFC.Text = 0
            End If

            cmd = New SqlCommand("procEditVendorCarMaster", MyConnection)
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.Add("@rowid", Request.QueryString("id"))
            cmd.Parameters.Add("@carvendorid", ddlvendorname.SelectedItem.Value)
            cmd.Parameters.Add("@carmodelid", ddlmodel.SelectedItem.Value)
            cmd.Parameters.Add("@regnno", txtregno.Text)
            cmd.Parameters.Add("@acyn", get_YNvalue(chkAC))
            cmd.Parameters.Add("@fueltype", ddlfuel.SelectedItem.Value)
            'cmd.Parameters.Add("@hertzdedicatedyn", get_YNvalue(chkdedicated)) 'Commented on 03-01-2011
            ' Code added by BSL on 10 March '07
            'cmd.Parameters.Add("@dto", get_YNvalue(chkDTO)) 'Commented on 03-01-2011
            'Code added by Rahul on 14 apr 2010
            cmd.Parameters.Add("@vdp", get_YNvalue(chkVDP))
            cmd.Parameters.Add("@paymentoption", ddlpayment.SelectedItem.Value)
            cmd.Parameters.Add("@revenuesharepc", revenueddl)
            cmd.Parameters.Add("@remarks", txtRemarks.Text)
            cmd.Parameters.Add("@active", get_YNvalue(chkactive))
            cmd.Parameters.Add("@monthly", get_YNvalue(chkmonthly))
            cmd.Parameters.Add("@modifiedby", Session("loggedin_user"))
            cmd.Parameters.Add("@FixedCost", txtFC.Text)
            cmd.Parameters.Add("@FileName", str)
            cmd.Parameters.Add("@InsuranceDate", txtInsuranceDate.Text)
            cmd.Parameters.Add("@FitnessDate", txtFitnessDate.Text)
            'cmd.Parameters.Add("@year", ddlyear.SelectedItem.Value)
            cmd.Parameters.Add("@ManufacturingYear", txtManufacturingYear.Text)

            cmd.Parameters.Add("@Grade", ddlGrade.SelectedItem.Value)
            cmd.Parameters.Add("@ReportingUnit", ddlReportingUnit.SelectedItem.Value)

            intuniqcheck = cmd.Parameters.Add("@uniqcheckval", SqlDbType.Int)
            intuniqcheck.Direction = ParameterDirection.Output


            'Try
            MyConnection.Open()
            cmd.ExecuteNonQuery()
            intuniqvalue = cmd.Parameters("@uniqcheckval").Value
            MyConnection.Close()
            If intuniqvalue = 1 Then
                lblErrorMsg.Visible = True
                lblErrorMsg.Text = "Vendor car already exist."
                Exit Sub
            ElseIf intuniqvalue = 0 Then
                lblErrorMsg.Visible = False
                pnlmainform.Visible = False
                pnlconfirmation.Visible = True
                lblMessage.Text = "You have updated the Vendor Car successfully"
                hyplnkretry.Text = "Edit another Vendor Car"
                hyplnkretry.NavigateUrl = "VendorCarEditSearch.aspx"
            ElseIf intuniqvalue = 2 Then
                lblErrorMsg.Visible = True
                lblErrorMsg.Text = "Image " & str & " already Uploaded."
                Exit Sub
            Else
                lblErrorMsg.Visible = True
                lblErrorMsg.Text = "Vendor car already exist."
                Exit Sub
            End If
            'Catch
            'End Try
        End If
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function

End Class
