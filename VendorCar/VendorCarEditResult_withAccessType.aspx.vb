Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.text
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Imports System.Web.UI
Public Class VendorCarEditResult
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tblRecDetail As System.Web.UI.WebControls.Table

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not Page.IsPostBack Then
            getvalue("V.carvendorname")
        End If
    End Sub
    Sub getvalue(ByVal strorderby As String)
        Dim strquery As StringBuilder
        Dim dtrreader As SqlDataReader
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility

        Dim objAcessdataMapping As clsutility 'Changes
        objAcessdataMapping = New clsutility 'Changes
        Dim strqueryMapping As String 'Changes
        Dim dtrreaderMapping As SqlDataReader 'Changes
        Dim UnitID As String 'Changes

        Dim objAcessdataNw As clsutility
        objAcessdataNw = New clsutility
        Dim strqueryNew As String
        Dim dtrreaderNew As SqlDataReader

        strqueryNew = New String("SELECT a.AccessType, a.UnitID, b.UnitCityID, c.Region FROM CORIntSysUsersMaster as a, CORIntUnitMaster as b, CORIntUnitCityMaster as c WHERE a.UnitID = b.UnitID and b.UnitCityID = c.UnitCityID and a.SysUserID =" & Session("loggedin_user"))
        dtrreaderNew = objAcessdataNw.funcGetSQLDataReader(strqueryNew.ToString)

        'strquery = New StringBuilder("select M.vendorcarid, W.CityName, Y.carcompname+' '+A.carmodelname as carmodel ,V.carvendorname,M.regnno, M.RevenueSharePC, (case M.active when '1' then 'Active' when '0' then 'Not Active' end) as active from CORIntVendorCarMaster  M, CORIntCarVendorMaster V, CORIntCarModelMaster A, CORIntCarCompMaster Y, CORIntCityMaster as W where V.carvendorid=M.carvendorid and M.carmodelid=A.carmodelid and Y.carcompid=A.carcompid and V.CarVendorCityID = W.CityID")
        strquery = New StringBuilder(" select distinct M.VendorCarid , W.CityName, Y.carcompname+' '+A.carmodelname as carmodel  " & "")
        strquery.Append(",V.carvendorname ,M.regnno , M.RevenueSharePC " & "")
        strquery.Append(" , (case M.active when '1' then 'Active' when '0' then 'Not Active' end) as active, isnull(M.VDPYN,0) as VDPYN, isnull(M.FixedCost,0) as FixedCost" & "")
        strquery.Append(" from CORIntVendorCarMaster  M inner join CORIntCarVendorMaster V on V.carvendorid=M.carvendorid  " & "")
        strquery.Append(" inner join CORIntCityMaster as W on   W.CityID  =V.CarVendorCityID " & "")
        strquery.Append(" inner join  CORIntCarModelMaster as A on  A.CarModelID=M.CarModelID " & "")
        strquery.Append(" inner join  CORIntCarCompMaster as Y on Y.carcompid=A.carcompid " & "")
        strquery.Append(" inner join CORIntUnitCityMaster as UCM on UCM.UnitCityID = W.nearestUnitCityId " & "")
        strquery.Append(" inner join CORIntUnitMaster as UM on UM.UnitCityID = UCM.UnitCityID " & "")

        'strquery.Append("left outer join CORIntVendorRevenueSharing as CVR on CVR.VendorCarID=M.VendorCarID " & "")
        ',isnull(CVR.RevenueSharePC,0) as NewRevenueShare

        If Request.QueryString("id") <> "-1" Then
            strquery.Append(" where M.carvendorid=" & Request.QueryString("id") & " ")
        End If

        Do While dtrreaderNew.Read

            'Changes
            If dtrreaderNew("AccessType") = "SU" Or dtrreaderNew("AccessType") = "RN" Then
                strqueryMapping = New String("select UM.UnitID, UM.referencedUnitID from corintunitmapping as UM, CORIntUnitMaster as b, CORIntUnitCityMaster as c WHERE UM.referencedUnitID = b.UnitID and b.UnitCityID = c.UnitCityID and UM.UnitID = " & dtrreaderNew("UnitID"))
            ElseIf dtrreaderNew("AccessType") = "CT" Then
                strqueryMapping = New String("select UM.UnitID, c.UnitCityID as referencedUnitID from corintunitmapping as UM, CORIntUnitMaster as b, CORIntUnitCityMaster as c WHERE UM.referencedUnitID = b.UnitID and b.UnitCityID = c.UnitCityID and UM.UnitID = " & dtrreaderNew("UnitID"))
            Else 'If dtrreaderNew("AccessType") = "RN" Then
                UnitID = dtrreaderNew("UnitID")
            End If

            If dtrreaderNew("AccessType") = "SU" Or dtrreaderNew("AccessType") = "CT" Or dtrreaderNew("AccessType") = "RN" Then
                dtrreaderMapping = objAcessdataMapping.funcGetSQLDataReader(strqueryMapping.ToString)

                Do While dtrreaderMapping.Read
                    UnitID = dtrreaderMapping("referencedUnitID")
                Loop

                dtrreaderMapping.Close()
            End If
            objAcessdataMapping.Dispose()
            'Changes

            If UnitID = "" Then
                UnitID = dtrreaderNew("UnitID")
            End If

            If dtrreaderNew("AccessType") = "SU" Or dtrreaderNew("AccessType") = "RN" Then
                strquery.Append(" and UM.UnitID = '" & UnitID & "' ")
            Else 'If dtrreaderNew("AccessType") = "CT" Then
                strquery.Append(" and UM.UnitCityID = '" & UnitID & "' ")
                'ElseIf dtrreaderNew("AccessType") = "RN" Then
                'strquery.Append(" and UCM.Region = '" & UnitID & "' ")
                'Else
            End If
        Loop

        strquery.Append(" and isnull(V.ApproveYN,0) = 1 and UM.active = 1 and W.active = 1 and UCM.active =1 order by " & strorderby & "")

        'Response.Write(strquery)
        'Response.End()

        dtrreader = objAcessdata.funcGetSQLDataReader(strquery.ToString)
        'Response.Write(strquery)
        'Response.End()
        While dtrreader.Read
            Dim Temprow As New TableRow
            Temprow.HorizontalAlign = HorizontalAlign.Center

            Dim Tempcell As New TableCell
            Tempcell.Controls.Add(New LiteralControl(dtrreader("VendorCarid") & ""))
            Temprow.Cells.Add(Tempcell)

            Dim Tempcel2 As New TableCell
            Tempcel2.Controls.Add(New LiteralControl(dtrreader("CityName") & ""))
            Temprow.Cells.Add(Tempcel2)

            Dim Tempcel3 As New TableCell
            Tempcel3.Controls.Add(New LiteralControl(dtrreader("carmodel") & ""))
            Temprow.Cells.Add(Tempcel3)

            Dim Tempcel4 As New TableCell
            Tempcel4.Controls.Add(New LiteralControl(dtrreader("carvendorname") & ""))
            Temprow.Cells.Add(Tempcel4)

            Dim Tempcel5 As New TableCell
            Tempcel5.Controls.Add(New LiteralControl(dtrreader("regnno") & ""))
            Temprow.Cells.Add(Tempcel5)

            Dim Tempcel6 As New TableCell
            Tempcel6.Controls.Add(New LiteralControl(dtrreader("RevenueSharePC") & ""))
            Temprow.Cells.Add(Tempcel6)

            'Code added by Rahul on 14 apr 2010
            Dim Tempcel7 As New TableCell
            Tempcel7.Controls.Add(New LiteralControl(dtrreader("VDPYN") & ""))
            Temprow.Cells.Add(Tempcel7)

            Dim Tempcel8 As New TableCell
            Tempcel8.Controls.Add(New LiteralControl(dtrreader("FixedCost") & ""))
            Temprow.Cells.Add(Tempcel8)

            Dim Tempcel9 As New TableCell
            Tempcel9.Controls.Add(New LiteralControl(dtrreader("active") & ""))
            Temprow.Cells.Add(Tempcel9)

            Dim Tempcel10 As New TableCell
            Tempcel10.Controls.Add(New LiteralControl("<a href=VendorCarEditForm.aspx?ID=" & dtrreader("vendorcarid") & " >Edit</a>"))
            Temprow.Cells.Add(Tempcel10)
            'adding the Tables rows to the table
            tblRecDetail.Rows.Add(Temprow)

        End While
        dtrreader.Close()
        objAcessdata.Dispose()

    End Sub
    Sub SortGird(ByVal sender As Object, ByVal e As System.EventArgs)
        Select Case sender.id

            Case "Linkbutton1"
                getvalue("V.VendorCarid")
            Case "Linkbutton2"
                getvalue("CityName")
            Case "Linkbutton3"
                getvalue("carmodel")
            Case "Linkbutton4"
                getvalue("V.carvendorname")
            Case "Linkbutton5"
                getvalue("regnno")
            Case "Linkbutton6"
                getvalue("RevenueSharePC")
                'Code added by Rahul on 14 apr 2010
            Case "Linkbutton7"
                getvalue("VDPYN")
            Case "Linkbutton8"
                getvalue("FixedCost")
            Case "Linkbutton9"
                getvalue("active")
        End Select
    End Sub

End Class