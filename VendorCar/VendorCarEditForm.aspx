<%@ Page Language="vb" AutoEventWireup="false" Src="VendorCarEditForm.aspx.vb" Inherits="VendorCarEditForm"%>
<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CarzonRent :: Internal software control panel</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="JavaScript" src="../utilityfunction.js"></script>
		<script type="text/javascript" language="JavaScript" src="../JScripts/Datefunc.js"></script>			
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
		<script language="javascript">
		function enableFC()
		{
			//alert(document.forms[0].chkDTO.checked);
			if(document.forms[0].chkVDP.checked==false)
			{
				document.forms[0].txtFC.value=0;
				document.forms[0].txtFC.disabled=true;
			}
			else
			{
				document.forms[0].txtFC.disabled=false;
			}
		}

		function validation()
		{
			if (document.forms[0].ddlpayment.value=="Revenue Sharing")
			{
				if(document.forms[0].ddlrevenue.value=="")
				{
					alert("Please select Revenue Sharing %")
					return false;
				}
				
			}
			var strvalues
			strvalues=('ddlvendorname,ddlmodel,txtManufacturingYear,txtregno,ddlfuel,ddlpayment,txtInsuranceDate,txtFitnessDate')
			return checkmandatory(strvalues);
		}
		function dateReg(obj)
        {
            if(obj.value!="")
            {
                // alert(obj.value);
                var reg = /((0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[0-1])-(201[0-9]))/
                if(reg.test(obj.value))
                {
                    //alert('valid');
                }
                else
                {
                    alert('notvalid');
                    obj.value="";
                }
            }
        }		
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout" onLoad="javascript:enableFC();">
		<form id="Form1" method="post" runat="server">
			<uc1:Headerctrl id="Headerctrl1" runat="server"></uc1:Headerctrl>
			<p>&nbsp;</p>
			<TABLE id="Table1" align="center">
				<asp:panel id="pnlmainform" Runat="server">
					<TBODY>
						<TR>
							<TD align="center" colSpan="2"><STRONG><U>Edit a Vendor Car</U></STRONG>
						    </TD>
						</TR>
						<TR>
							<TD align="center" colSpan="2">
								<asp:Label id="lblErrorMsg" runat="server" visible="false" cssclass="subRedHead"></asp:Label></TD>
						</TR>
						<TR>
							<TD align="center" colSpan="2">&nbsp;&nbsp;</TD>
						</TR>
						<TR>
							<TD>* Vendor Name
							</TD>
							<TD>
								<asp:DropDownList id="ddlvendorname" runat="server" CssClass="input" Enabled="false"></asp:DropDownList></TD>
						</TR>
						<TR>
							<TD>* Model</TD>
							<TD>
								<asp:DropDownList id="ddlmodel" runat="server" CssClass="input"></asp:DropDownList></TD>
						</TR>
						<TR>
							<TD>* Registration #
							</TD>
							<TD>
								<asp:textbox id="txtregno" runat="server" CssClass="input" MaxLength="20" ></asp:textbox></TD>
						</TR>
					     <TR>
						    <TD>* Manufacturing Date</TD>
						    <TD>
							<!--<asp:DropDownList id="ddlyear" runat="server" CssClass="input"></asp:DropDownList>-->
							<asp:textbox id="txtManufacturingYear" runat="server" CssClass="input" MaxLength="12" size="12"
								onblur = "dateReg(this);"></asp:textbox><a onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;" href="javascript:show_calendar('Form1.txtManufacturingYear');"><img height="21" src="../../images/show-calendar.gif" width="24" border="0"></a>							
							</TD>
					    </TR>
						<TR>
							<TD>AC?</TD>
							<TD>
								<asp:checkbox id="chkAC" runat="server" CssClass="input" Checked="True"></asp:checkbox></TD>
						</TR>
					    <TR>
						    <TD>* Fuel Type
						    </TD>
						    <TD>
							    <asp:DropDownList id="ddlfuel" runat="server" CssClass="input"></asp:DropDownList>
						    </TD>
					    </TR>
	                    <%--<TR>
					    <TD>Dedicated to Hertz?
					    </TD>
					    <TD>
    					    <asp:checkbox id="chkdedicated" runat="server" CssClass="input" Checked="True"></asp:checkbox>
                        </TD>
					    </TR>
					    <TR>
					    <TD>DTO?</TD>
					    <TD>
						    <asp:checkbox id="chkDTO" runat="server" CssClass="input"></asp:checkbox>
					    </TD>
					    </TR>--%>
					    <TR>
						    <TD>VDP?</TD>
						    <TD><asp:checkbox id="chkVDP" onclick="enableFC();" runat="server" CssClass="input"></asp:checkbox></TD>
					    </TR>
						<TR>
							<TD>* Payment Option</TD>
							<TD>
								<asp:DropDownList id="ddlpayment" runat="server" CssClass="input">
									<asp:listitem Text="" Value="" />
									<asp:listitem Text="Package-Wise" Value="Package-Wise" />
									<asp:listitem Text="Revenue Sharing" Value="Revenue Sharing" />
								</asp:DropDownList></TD>
						</TR>
					    <tr>
						    <td style="HEIGHT: 25px" valign="top">* Insurance Upto</td>
						    <td style="HEIGHT: 18px">
							    <asp:textbox id="txtInsuranceDate" runat="server" CssClass="input" MaxLength="12" size="12"
								    onblur = "dateReg(this);"></asp:textbox><a onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;"
								    href="javascript:show_calendar('Form1.txtInsuranceDate');"><img height="21" src="../images/show-calendar.gif" width="24" border="0"></a></td>
					    </tr>
					    <tr>
						    <td style="HEIGHT: 25px" valign="top">* Fitness Upto</td>
						    <td style="HEIGHT: 18px">
							    <asp:textbox id="txtFitnessDate" runat="server" CssClass="input" MaxLength="12" size="12"
								    onblur = "dateReg(this);"></asp:textbox><a onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;"
								    href="javascript:show_calendar('Form1.txtFitnessDate');"><img height="21" src="../images/show-calendar.gif" width="24" border="0"></a></td>
					    </tr>
						<TR>
							<TD>Revenue Sharing %
							</TD>
							<TD>
								<asp:DropDownList id="ddlrevenue" runat="server" CssClass="input"></asp:DropDownList></TD>
						</TR>
				        <TR>
						<TD>Fixed Cost
						</TD>
						<TD>
						<asp:textbox id="txtFC" runat="server" CssClass="input" MaxLength="20"></asp:textbox></TD>
						</TR>
						
						<TR>
						<TD>Grading</TD>
						<TD><asp:DropDownList id="ddlGrade" runat="server" CssClass="input">
							
							<asp:listitem Text="A" Value="A" />
							<asp:listitem Text="B" Value="B" />
							<asp:listitem Text="C" Value="C" />
						</asp:DropDownList></TD>
						</TR>
						
						<TR>
							<TD> Reporting Unit</TD>
							<TD><asp:DropDownList id="ddlReportingUnit" runat="server" CssClass="input"></asp:DropDownList></TD>
						</TR>
												
						<TR>
							<TD vAlign="top">Remarks(<SPAN class="shwText" id="shwMessage">0</SPAN>/2000 chars)</TD>
							<TD>
								<asp:textbox id="txtRemarks" onkeydown="showLength(this.form.txtRemarks,'document.all.shwMessage',2000)"
									onkeyup="showLength(this.form.txtRemarks,'document.all.shwMessage',2000)" runat="server"
									CssClass="input" MaxLength="2000" Rows="3" Columns="50" TextMode="MultiLine"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>Active</TD>
							<TD>
								<asp:checkbox id="chkactive" runat="server" CssClass="input" Checked="True"></asp:checkbox></TD>
						</TR>
						<TR>
							<TD>MonthlyYN</TD>
							<TD>
								<asp:checkbox id="chkmonthly" runat="server" CssClass="input" Checked="false"></asp:checkbox></TD>
						</TR>
						<TR>
							<TD>File Name
							</TD>
							<TD>
								<asp:textbox id="txtFileName" runat="server" Enabled="false" CssClass="input" MaxLength="20"></asp:textbox></TD>
						</TR>												
						<TR>
							<TD align="center" colSpan="2">
								<asp:button id="btnsubmit" runat="server" CssClass="button" Text="Submit"></asp:button>&nbsp;&nbsp;
								<asp:button id="btnreset" runat="server" CssClass="button" Text="Reset" CausesValidation="False"></asp:button>
								<asp:FileUpload ID="FileUpload1" runat="server" />
                                <asp:HyperLink ID="HyperLink1" Text="abc" runat="server"></asp:HyperLink>
								</TD>
						</TR>
						</TBODY>
				</asp:panel><asp:panel id="pnlconfirmation" Runat="server" Visible="False">
					<TR align="center">
						<TD colSpan="2">
							<asp:Label id="lblMessage" runat="server"></asp:Label></TD>
					</TR>
					<TR align="center">
						<TD colSpan="2">
							<asp:HyperLink id="hyplnkretry" runat="server"></asp:HyperLink></TD>
					</TR>
				</asp:panel></TABLE>
		</form>
	</body>
</HTML>
