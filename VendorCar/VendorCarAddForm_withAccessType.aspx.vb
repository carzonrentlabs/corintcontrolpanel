Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports commonutility
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.IO
Imports System.IO.FileStream
Imports System.IO.File
Imports System.Net
Imports System.Text
Imports System.Object
Imports System.MarshalByRefObject
Imports System.Net.WebRequest

Public Class VendorCarAddForm
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'Protected WithEvents ddlyear As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtManufacturingYear As System.Web.UI.WebControls.TextBox

    Protected WithEvents ddlvendorname As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlmodel As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtregno As System.Web.UI.WebControls.TextBox
    'Code added by Rahul on 14 apr 2010
    Protected WithEvents txtFC As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkAC As System.Web.UI.WebControls.CheckBox
    Protected WithEvents ddlfuel As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents chkdedicated As System.Web.UI.WebControls.CheckBox 'Commented on 03-01-2011
    Protected WithEvents ddlpayment As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlrevenue As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtRemarks As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkactive As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkmonthly As System.Web.UI.WebControls.CheckBox
    Protected WithEvents btnsubmit As System.Web.UI.WebControls.Button
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    Protected WithEvents Validationsummary1 As System.Web.UI.WebControls.ValidationSummary
    Protected WithEvents txtInsuranceDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtFitnessDate As System.Web.UI.WebControls.TextBox
    'Protected WithEvents chkDTO As System.Web.UI.WebControls.CheckBox 'Commented on 03-01-2011
    'Code added by Rahul on 14 apr 2010
    Protected WithEvents chkVDP As System.Web.UI.WebControls.CheckBox

    Protected WithEvents ddlGrade As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlReportingUnit As System.Web.UI.WebControls.DropDownList

    Protected WithEvents FileUpload1 As System.Web.UI.WebControls.FileUpload

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btnsubmit.Attributes("onClick") = "return validation();"
        If Not Page.IsPostBack Then
            populateddl()
        End If
    End Sub

    Sub populateddl()
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility

        Dim objAcessdataNw As clsutility
        objAcessdataNw = New clsutility
        Dim strquery As String
        Dim dtrreaderNew As SqlDataReader
        Dim strqueryvendor As String


        Dim dtrreader As SqlDataReader

        Dim objAcessdataMapping As clsutility 'Changes
        objAcessdataMapping = New clsutility 'Changes
        Dim strqueryMapping As String 'Changes
        Dim dtrreaderMapping As SqlDataReader 'Changes
        Dim UnitID As String 'Changes

        'Dim AccessType As String

        strquery = New String("SELECT a.AccessType, a.UnitID, b.UnitCityID, c.Region FROM CORIntSysUsersMaster as a, CORIntUnitMaster as b, CORIntUnitCityMaster as c WHERE a.UnitID = b.UnitID and b.UnitCityID = c.UnitCityID and a.SysUserID =" & Session("loggedin_user"))
        dtrreaderNew = objAcessdataNw.funcGetSQLDataReader(strquery.ToString)

        Do While dtrreaderNew.Read

            'Changes
            If dtrreaderNew("AccessType") = "SU" Or dtrreaderNew("AccessType") = "RN" Then
                strqueryMapping = New String("select UM.UnitID, UM.referencedUnitID from corintunitmapping as UM, CORIntUnitMaster as b, CORIntUnitCityMaster as c WHERE UM.referencedUnitID = b.UnitID and b.UnitCityID = c.UnitCityID and UM.UnitID = " & dtrreaderNew("UnitID"))
            ElseIf dtrreaderNew("AccessType") = "CT" Then
                strqueryMapping = New String("select UM.UnitID, c.UnitCityID as referencedUnitID from corintunitmapping as UM, CORIntUnitMaster as b, CORIntUnitCityMaster as c WHERE UM.referencedUnitID = b.UnitID and b.UnitCityID = c.UnitCityID and UM.UnitID = " & dtrreaderNew("UnitID"))
            Else 'If dtrreaderNew("AccessType") = "RN" Then
                UnitID = dtrreaderNew("UnitID")
            End If

            If dtrreaderNew("AccessType") = "SU" Or dtrreaderNew("AccessType") = "CT" Or dtrreaderNew("AccessType") = "RN" Then
                dtrreaderMapping = objAcessdataMapping.funcGetSQLDataReader(strqueryMapping.ToString)

                Do While dtrreaderMapping.Read
                    UnitID = dtrreaderMapping("referencedUnitID")
                Loop

                dtrreaderMapping.Close()

            End If


            objAcessdataMapping.Dispose()
            'Changes

            If UnitID = "" Then
                UnitID = dtrreaderNew("UnitID")
            End If

            If dtrreaderNew("AccessType") = "SU" Or dtrreaderNew("AccessType") = "RN" Then
                strqueryvendor = New String("select distinct VCM.CarVendorID  ,VCM.CarVendorName  from CORIntCarVendorMaster as VCM, CORIntCityMaster as a, CORIntUnitCityMaster as b, CORIntUnitMaster as c WHERE VCM.CarVendorCityID = a.CityID and a.nearestUnitCityId=b.UnitCityId and a.Active = 1 and a.CityName <> '' and c.UnitCityID = b.UnitCityID and isnull(ApproveYN,0) = 1 and c.UnitID = '" & UnitID & "' order by CarVendorName")
            Else 'If dtrreaderNew("AccessType") = "CT" Then
                strqueryvendor = New String("select distinct VCM.CarVendorID  ,VCM.CarVendorName  from CORIntCarVendorMaster as VCM, CORIntCityMaster as a, CORIntUnitCityMaster as b, CORIntUnitMaster as c WHERE VCM.CarVendorCityID = a.CityID and a.nearestUnitCityId=b.UnitCityId and a.Active = 1 and a.CityName <> '' and c.UnitCityID = b.UnitCityID and isnull(ApproveYN,0) = 1 and c.UnitCityID = '" & UnitID & "' order by CarVendorName")
                'ElseIf dtrreaderNew("AccessType") = "RN" Then
                'strqueryvendor = New String("select distinct VCM.CarVendorID  ,VCM.CarVendorName  from CORIntCarVendorMaster as VCM, CORIntCityMaster as a, CORIntUnitCityMaster as b, CORIntUnitMaster as c WHERE VCM.CarVendorCityID = a.CityID and a.nearestUnitCityId=b.UnitCityId and a.Active = 1 and a.CityName <> '' and c.UnitCityID = b.UnitCityID and isnull(ApproveYN,0) = 1 and b.Region = '" & UnitID & "' order by CarVendorName")
                'Else
                'strqueryvendor = New String("select distinct VCM.CarVendorID  ,VCM.CarVendorName  from CORIntCarVendorMaster as VCM, CORIntCityMaster as a, CORIntUnitCityMaster as b, CORIntUnitMaster as c WHERE VCM.CarVendorCityID = a.CityID and a.nearestUnitCityId=b.UnitCityId and a.Active = 1 and a.CityName <> '' and c.UnitCityID = b.UnitCityID and isnull(ApproveYN,0) = 1 " & " order by CarVendorName")
            End If
        Loop

        dtrreaderNew.Close()
        objAcessdataNw.Dispose()


        'ddlvendorname.DataSource = objAcessdata.funcGetSQLDataReader("select CarVendorName ,carvendorid  from CORIntCarVendorMaster   where active=1 and isnull(ApproveYN,0) = 1 order by CarVendorName")
        ddlvendorname.DataSource = objAcessdata.funcGetSQLDataReader(strqueryvendor)
        ddlvendorname.DataValueField = "carvendorid"
        ddlvendorname.DataTextField = "CarVendorName"
        ddlvendorname.DataBind()
        ddlvendorname.Items.Insert(0, New ListItem("", ""))

        ddlfuel.DataSource = objAcessdata.funcGetSQLDataReader("select FuelTypeID, FuelTypeName from CORIntFuelTypeMaster where active=1 order by FuelTypeName")
        ddlfuel.DataValueField = "FuelTypeID"
        ddlfuel.DataTextField = "FuelTypeName"
        ddlfuel.DataBind()
        ddlfuel.Items.Insert(0, New ListItem("", ""))

        ddlmodel.DataSource = objAcessdata.funcGetSQLDataReader("select M.carmodelid,C.carcompname+' '+M.carmodelname as carmodel from CORIntCarCompMaster C,CORIntCarModelMaster M where M.carcompid=C.carcompid  and M.active=1 order by carmodel")
        ddlmodel.DataValueField = "carmodelid"
        ddlmodel.DataTextField = "carmodel"
        ddlmodel.DataBind()
        ddlmodel.Items.Insert(0, New ListItem("", ""))

        ddlReportingUnit.DataSource = objAcessdata.funcGetSQLDataReader("select UnitID, UnitName from CORIntUnitMaster where active=1 order by UnitName")
        ddlReportingUnit.DataValueField = "UnitID"
        ddlReportingUnit.DataTextField = "UnitName"
        ddlReportingUnit.DataBind()
        ddlReportingUnit.Items.Insert(0, New ListItem("", ""))

        'objAcessdata.funcpopulatenumddw(2020, 2000, ddlyear)
        objAcessdata.funcpopulatenumddw(100, 1, ddlrevenue)
        objAcessdata.Dispose()
			
    End Sub
	


    Private Sub btnsubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsubmit.Click

        Dim str As String
        If FileUpload1.PostedFile.FileName <> "" And FileUpload1.PostedFile.ContentLength > 0 Then
            'str = FileUpload1.PostedFile.FileName
            str = Path.GetFileName(FileUpload1.FileName)

            If str <> "" Then
                Dim filename As String
                filename = Path.GetFileName(FileUpload1.FileName)
                'Response.Write(filename)
                'Response.End()
                str = filename
                FileUpload1.SaveAs(Server.MapPath("~/UploadImage/Addendum/") + filename)


                Dim intuniqcheck1 As SqlParameter
                Dim intuniqvalue1 As Int32
                If (ddlpayment.SelectedItem.Value = "Package-Wise") Then
                    'response.write(ddlvendorname.selectedItem.Value)
                    'response.write("<br>")
                    'response.write(ddlmodel.SelectedItem.Value)
                    'response.write("<br>")
                    'response.end()
                    Dim MyConnection1 As SqlConnection
                    MyConnection1 = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
                    Dim cmd1 As SqlCommand
                    cmd1 = New SqlCommand("procpkgsexists", MyConnection1)
                    cmd1.CommandType = CommandType.StoredProcedure
                    cmd1.Parameters.Add("@carvendorid", ddlvendorname.SelectedItem.Value)
                    cmd1.Parameters.Add("@carmodelid", ddlmodel.SelectedItem.Value)
                    intuniqcheck1 = cmd1.Parameters.Add("@uniqcheckval", SqlDbType.Int)
                    intuniqcheck1.Direction = ParameterDirection.Output

                    MyConnection1.Open()
                    cmd1.ExecuteNonQuery()
                    intuniqvalue1 = cmd1.Parameters("@uniqcheckval").Value
                    'While cmd1.Read
                    If intuniqvalue1 = 0 Then
                        Response.Write("Vendor Package for this Category does not exists.<br> Please enter Vendor Package first..")
                        Response.End()
                        'End While
                    End If
                    MyConnection1.Close()
                    'response.write("You Are Here")
                    'response.end()
                End If


                Dim MyConnection As SqlConnection
                MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
                Dim cmd As SqlCommand

                Dim intuniqcheck As SqlParameter
                Dim intuniqvalue As Int32
                cmd = New SqlCommand("procAddVendorCarMaster", MyConnection)
                cmd.CommandType = CommandType.StoredProcedure
                Dim revenueddl As Int32

                If ddlrevenue.SelectedItem.Value = "" Then
                    revenueddl = 0
                Else
                    revenueddl = ddlrevenue.SelectedItem.Value
                End If

                If txtFC.Text = "" Then
                    txtFC.Text = 0
                End If

                cmd.Parameters.Add("@carvendorid", ddlvendorname.SelectedItem.Value)
                cmd.Parameters.Add("@carmodelid", ddlmodel.SelectedItem.Value)
                cmd.Parameters.Add("@regnno", txtregno.Text)
                cmd.Parameters.Add("@acyn", get_YNvalue(chkAC))
                cmd.Parameters.Add("@fueltype", ddlfuel.SelectedItem.Value)
                'cmd.Parameters.Add("@hertzdedicatedyn", get_YNvalue(chkdedicated)) 'Commented on 03-01-2011
                ' Code added by BSL on 10 March '07
                'cmd.Parameters.Add("@dto", get_YNvalue(chkDTO)) 'Commented on 03-01-2011
                'Code added by Rahul on 14 apr 2010
                cmd.Parameters.Add("@vdp", get_YNvalue(chkVDP))
                cmd.Parameters.Add("@paymentoption", ddlpayment.SelectedItem.Value)
                cmd.Parameters.Add("@revenuesharepc", revenueddl)
                cmd.Parameters.Add("@remarks", txtRemarks.Text)
                cmd.Parameters.Add("@active", get_YNvalue(chkactive))
                cmd.Parameters.Add("@monthly", get_YNvalue(chkmonthly))
                cmd.Parameters.Add("@createdby", Session("loggedin_user"))
                'Code added by Rahul on 14 apr 2010
                cmd.Parameters.Add("@FixedCost", txtFC.Text)
                cmd.Parameters.Add("@InsuranceDate", txtInsuranceDate.Text)
                cmd.Parameters.Add("@FitnessDate", txtFitnessDate.Text)
                'cmd.Parameters.Add("@year", ddlyear.SelectedItem.Value)
                cmd.Parameters.Add("@ManufacturingYear", txtManufacturingYear.Text)
                cmd.Parameters.Add("@Grade", ddlGrade.SelectedItem.Value)
                cmd.Parameters.Add("@ReportingUnit", ddlReportingUnit.SelectedItem.Value)

                cmd.Parameters.Add("@FileName", str)

                intuniqcheck = cmd.Parameters.Add("@uniqcheckval", SqlDbType.Int)
                intuniqcheck.Direction = ParameterDirection.Output



                '  Try
                MyConnection.Open()
                cmd.ExecuteNonQuery()
                intuniqvalue = cmd.Parameters("@uniqcheckval").Value
                MyConnection.Close()

                If intuniqvalue = 1 Then
                    lblErrorMsg.Visible = True
                    lblErrorMsg.Text = "Vendor car already exist."
                    Exit Sub
                ElseIf intuniqvalue = 0 Then
                    lblErrorMsg.Visible = False
                    pnlmainform.Visible = False
                    pnlconfirmation.Visible = True
                    lblMessage.Text = "You have added the Vendor Car successfully"
                    hyplnkretry.Text = "Add another Vendor Car"
                    hyplnkretry.NavigateUrl = "VendorCarAddForm.aspx"
                ElseIf intuniqvalue = 2 Then
                    lblErrorMsg.Visible = True
                    lblErrorMsg.Text = "Image " & str & " already Uploaded."
                    Exit Sub
                Else
                    lblErrorMsg.Visible = True
                    lblErrorMsg.Text = "Vendor car already exist."
                    Exit Sub
                End If
                '   Catch
                '   End Try

            Else
                lblErrorMsg.Visible = True
                lblErrorMsg.Text = "Upload Image."
            End If
        Else
            lblErrorMsg.Visible = True
            lblErrorMsg.Text = "Upload Image."
        End If
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function

End Class
