Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class VendorCarAddNewForm
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtregno As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlrevenue As System.Web.UI.WebControls.DropDownList
    Protected WithEvents btnsubmit As System.Web.UI.WebControls.Button
    Protected WithEvents txtEffecDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    Protected WithEvents Validationsummary1 As System.Web.UI.WebControls.ValidationSummary
    Protected WithEvents chkDTO As System.Web.UI.WebControls.CheckBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btnsubmit.Attributes("onClick") = "return validation();"
        If Not Page.IsPostBack Then
            populateddl()
        End If
    End Sub

    Sub populateddl()
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility

        txtregno.DataSource = objAcessdata.funcGetSQLDataReader("select RegnNo ,vendorCarid  from CORIntVendorCarMaster   where active=1 order by RegnNo")
        txtregno.DataValueField = "vendorCarid"
        txtregno.DataTextField = "RegnNo"
        txtregno.DataBind()
        txtregno.Items.Insert(0, New ListItem("", ""))
       
        objAcessdata.funcpopulatenumddw(100, 1, ddlrevenue)
        objAcessdata.Dispose()

    End Sub


    Private Sub btnsubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsubmit.Click

        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand

        Dim intuniqcheck As SqlParameter
        Dim intuniqvalue as int32
        cmd = New SqlCommand("procAddVendorCarMaster_1", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure
        Dim revenueddl As Int32

        If ddlrevenue.SelectedItem.Value = "" Then
            revenueddl = 0
        Else
            revenueddl = ddlrevenue.SelectedItem.Value
        End If
             
        cmd.Parameters.Add("@vendorcarid", txtregno.selectedItem.Value)
        cmd.Parameters.Add("@regnno", txtregno.SelectedItem.Text)
        cmd.Parameters.Add("@createdDate", txtEffecDate.Text)    
        cmd.Parameters.Add("@revenuesharepc", revenueddl)
           intuniqcheck = cmd.Parameters.Add("@uniqcheckval", SqlDbType.Int)
           intuniqcheck.Direction = ParameterDirection.Output



        '  Try
        MyConnection.Open()
        cmd.ExecuteNonQuery()
        intuniqvalue=cmd.Parameters("@uniqcheckval").Value
        MyConnection.Close()

           if not intuniqvalue = 0 then
                pnlconfirmation.Visible = True
                lblErrorMsg.visible=true
                lblErrorMsg.text="Vendor car already exist."
                hyplnkretry.NavigateUrl = "VendorCarAddNewForm.aspx"
                exit sub
            else
		          lblErrorMsg.visible=false
                pnlmainform.Visible = False
                pnlconfirmation.Visible = True
                lblMessage.Text = "You have added the Vendor Car successfully"
                hyplnkretry.Text = "Add another Vendor Car"
                hyplnkretry.NavigateUrl = "VendorCarAddNewForm.aspx"
            end if
        '   Catch
        '   End Try
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function

End Class
