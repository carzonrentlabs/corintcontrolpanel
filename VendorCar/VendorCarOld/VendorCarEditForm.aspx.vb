Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class VendorCarEditForm
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ddlvendorname As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlmodel As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtregno As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtFC As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkAC As System.Web.UI.WebControls.CheckBox
    Protected WithEvents ddlfuel As System.Web.UI.WebControls.DropDownList
    Protected WithEvents chkdedicated As System.Web.UI.WebControls.CheckBox
    Protected WithEvents ddlpayment As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlrevenue As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtRemarks As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkactive As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkmonthly As System.Web.UI.WebControls.CheckBox
    Protected WithEvents btnsubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnreset As System.Web.UI.WebControls.Button
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    'Protected WithEvents chkDTO As System.Web.UI.WebControls.CheckBox
    'Code added by Rahul on 14 apr 2010
    Protected WithEvents txtInsuranceDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtFitnessDate As System.Web.UI.WebControls.TextBox

    'Protected WithEvents ddlyear As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtManufacturingYear As System.Web.UI.WebControls.TextBox

    Protected WithEvents chkVDP As System.Web.UI.WebControls.CheckBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btnsubmit.Attributes("onClick") = "return validation();"
        If Not Page.IsPostBack Then
            populateddl()
            Dim dtrreader As SqlDataReader
            Dim accessdata As clsutility
            accessdata = New clsutility
            dtrreader = accessdata.funcGetSQLDataReader("select *,isnull(FixedCost,0)as FC,isnull(VDPYN,0) as VDPYN1 from CORIntVendorCarMaster  where  vendorcarid=" & Request.QueryString("id") & " ")
            dtrreader.Read()

            autoselec_ddl(ddlvendorname, dtrreader("carvendorid"))
            autoselec_ddl(ddlmodel, dtrreader("carmodelid"))
            txtregno.Text = dtrreader("regnno") & ""
            txtFC.Text = dtrreader("FC") & ""
            chkAC.Checked = dtrreader("acyn")
            ddlfuel.Items.FindByValue(dtrreader("fueltypeID")).Selected = True
            ddlpayment.Items.FindByValue(dtrreader("paymentoption")).Selected = True
            'chkdedicated.Checked = dtrreader("hertzdedicatedyn")
            'chkDTO.Checked = dtrreader("dtoyn")
            'Code added by Rahul on 14 apr 2010
            chkVDP.Checked = dtrreader("VDPYN1")

            txtInsuranceDate.Text = dtrreader("InsuranceDate")
            txtFitnessDate.Text = dtrreader("FitnessDate")

            txtManufacturingYear.Text = dtrreader("ManufacturingDate")

            autoselec_ddl(ddlrevenue, dtrreader("revenuesharepc"))
            txtRemarks.Text = dtrreader("remarks") & ""
            chkactive.Checked = dtrreader("active")
            chkmonthly.Checked = dtrreader("MonthlyYN")
            dtrreader.Close()
            accessdata.Dispose()
        End If
    End Sub

    Sub populateddl()
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility

        ddlvendorname.DataSource = objAcessdata.funcGetSQLDataReader("select CarVendorName ,carvendorid  from CORIntCarVendorMaster  order by CarVendorName")
        ddlvendorname.DataValueField = "carvendorid"
        ddlvendorname.DataTextField = "CarVendorName"
        ddlvendorname.DataBind()
        ddlvendorname.Items.Insert(0, New ListItem("", ""))

        ddlfuel.DataSource = objAcessdata.funcGetSQLDataReader("select FuelTypeID, FuelTypeName from CORIntFuelTypeMaster where active=1 order by FuelTypeName")
        ddlfuel.DataValueField = "FuelTypeID"
        ddlfuel.DataTextField = "FuelTypeName"
        ddlfuel.DataBind()
        ddlfuel.Items.Insert(0, New ListItem("", ""))

        ddlmodel.DataSource = objAcessdata.funcGetSQLDataReader("select M.carmodelid,C.carcompname+' '+M.carmodelname as carmodel from CORIntCarCompMaster C,CORIntCarModelMaster M where M.carcompid=C.carcompid  and M.active=1 order by carmodel")
        ddlmodel.DataValueField = "carmodelid"
        ddlmodel.DataTextField = "carmodel"
        ddlmodel.DataBind()
        ddlmodel.Items.Insert(0, New ListItem("", ""))

        'objAcessdata.funcpopulatenumddw(2020, 2000, ddlyear)
        objAcessdata.funcpopulatenumddw(100, 1, ddlrevenue)


        objAcessdata.Dispose()

    End Sub

    Function autoselec_ddl(ByVal ddlname As DropDownList, ByVal selectvalue As Int32)
        If Not selectvalue = "0" Then
            ddlname.Items.FindByValue(selectvalue).Selected = True
        End If
    End Function


    Private Sub btnreset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnreset.Click
        Response.Redirect("VendorCarEditForm.aspx?id=" & Request.QueryString("id"))
    End Sub

    Private Sub btnsubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand

        Dim intuniqcheck As SqlParameter
        Dim intuniqvalue as int32
        Dim revenueddl As Int32

        If ddlrevenue.SelectedItem.Value = "" Then
            revenueddl = 0
        Else
            revenueddl = ddlrevenue.SelectedItem.Value
        End If

        'Code added by Rahul on 14 apr 2010    
        If txtFC.Text = "" Then
            txtFC.Text = 0
        End If

        cmd = New SqlCommand("procEditVendorCarMaster_New", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("@rowid", Request.QueryString("id"))
        cmd.Parameters.Add("@carvendorid", ddlvendorname.SelectedItem.Value)
        cmd.Parameters.Add("@carmodelid", ddlmodel.SelectedItem.Value)
        cmd.Parameters.Add("@regnno", txtregno.Text)
        cmd.Parameters.Add("@acyn", get_YNvalue(chkAC))
        cmd.Parameters.Add("@fueltype", ddlfuel.SelectedItem.Value)
        'cmd.Parameters.Add("@hertzdedicatedyn", get_YNvalue(chkdedicated))
        ' Code added by BSL on 10 March '07
        'cmd.Parameters.Add("@dto", get_YNvalue(chkDTO))
        'Code added by Rahul on 14 apr 2010
        cmd.Parameters.Add("@vdp", get_YNvalue(chkVDP))
        cmd.Parameters.Add("@paymentoption", ddlpayment.SelectedItem.Value)
        cmd.Parameters.Add("@revenuesharepc", revenueddl)
        cmd.Parameters.Add("@remarks", txtRemarks.Text)
        cmd.Parameters.Add("@active", get_YNvalue(chkactive))
        cmd.Parameters.Add("@monthly", get_YNvalue(chkmonthly))
        cmd.Parameters.Add("@modifiedby", Session("loggedin_user"))
        cmd.Parameters.Add("@FixedCost", txtFC.Text)
        cmd.Parameters.Add("@InsuranceDate", txtInsuranceDate.Text)
        cmd.Parameters.Add("@FitnessDate", txtFitnessDate.Text)
        'cmd.Parameters.Add("@year", ddlyear.SelectedItem.Value)
        cmd.Parameters.Add("@ManufacturingYear", txtManufacturingYear.Text)
       intuniqcheck = cmd.Parameters.Add("@uniqcheckval", SqlDbType.Int)
       intuniqcheck.Direction = ParameterDirection.Output



        '  Try
        MyConnection.Open()
        cmd.ExecuteNonQuery()
        intuniqvalue=cmd.Parameters("@uniqcheckval").Value
        MyConnection.Close()
           if not intuniqvalue = 0 then
                lblErrorMsg.visible=true
                lblErrorMsg.text="Vendor car already exist."
                exit sub
           else
		          lblErrorMsg.visible=false
                pnlmainform.Visible = False
                pnlconfirmation.Visible = True
                lblMessage.Text = "You have updated the Vendor Car successfully"
                hyplnkretry.Text = "Edit another Vendor Car"
                hyplnkretry.NavigateUrl = "VendorCarEditSearch.aspx"
           end if
        '   Catch
        '   End Try
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function

End Class
