<%@ Page Language="vb" AutoEventWireup="false" Src="CarModelEditForm.aspx.vb" Inherits="CarModelEditForm" %>

<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>CarzonRent :: Internal software</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../HertzInt.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" src="../utilityfunction.js" type="text/javascript"></script>
    <script src="../JScripts/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {

            $("#<%=txtSeatingCapacity.ClientID%>").keyup(function () {
                var strPass = $("#<%=txtSeatingCapacity.ClientID%>").val();
                var strLength = strPass.length;
                var lchar = strPass.charAt((strLength) - 1);
                var cCode = CalcKeyCode(lchar);
                if (cCode < 46 || cCode > 57 || cCode == 47) {
                    var myNumber = strPass.substring(0, (strLength) - 1);
                    $("#<%=txtSeatingCapacity.ClientID%>").val(myNumber);
                    alert("Enter only numeric value.")
                }
            });
            
        })
        function CalcKeyCode(aChar) {
            var character = aChar.substring(0, 1);
            var code = aChar.charCodeAt(0);
            return code;
        }
    </script>

</head>
<body onload="OnLoadshowLength(document.forms[0].txtarearemarks.value,shwMessage)">
    <form id="Form1" method="post" runat="server">
    <p>
        <uc1:Headerctrl id="Headerctrl1" runat="server">
        </uc1:Headerctrl><asp:ValidationSummary ID="Validationsummary1" runat="server" ShowMessageBox="true"
            HeaderText="Please make sure all the fields marked with * are filled in." ShowSummary="false">
        </asp:ValidationSummary>
    </p>
    <table align="center">
        <asp:Panel ID="pnlmainform" runat="server">
            <tbody>
                <tr>
                    <td align="center" colspan="2">
                        <b><u>Edit a Car Model</u></b>
                        <br />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <asp:Label ID="lblErrorMsg" CssClass="subRedHead" Visible="false" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Model Name (Eg. Ikon / Esteem, etc)*
                    </td>
                    <td>
                        <asp:TextBox ID="txtcarmodel" runat="server" MaxLength="50" CssClass="input"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" ControlToValidate="txtcarmodel"
                            Display="None" ErrorMessage=""></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        Company Name*
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlcompname" runat="server" CssClass="input">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" ControlToValidate="ddlcompname"
                            Display="None" ErrorMessage=""></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        Category Name*
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlcatname" runat="server" CssClass="input">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" ControlToValidate="ddlcatname"
                            Display="None" ErrorMessage=""></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                       Seating Capacity*
                    </td>
                    <td>
                        <asp:TextBox ID="txtSeatingCapacity" runat="server" MaxLength="50" CssClass="input"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" ControlToValidate="txtSeatingCapacity"
                            Display="None" ErrorMessage=""></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        Remarks(<span class="shwText" id="shwMessage">0</span>/2000 chars)
                    </td>
                    <td>
                        <asp:TextBox ID="txtarearemarks" onkeydown="showLength(this.form.txtarearemarks,'document.all.shwMessage',2000)"
                            onkeyup="showLength(this.form.txtarearemarks,'document.all.shwMessage',2000)"
                            runat="server" MaxLength="2000" CssClass="input" TextMode="MultiLine" Columns="50"
                            Rows="3"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Active
                    </td>
                    <td>
                        <asp:CheckBox ID="chkActive" runat="server" CssClass="input" Checked="True"></asp:CheckBox>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <asp:Button ID="btnSubmit" runat="server" CssClass="input" Text="Submit"></asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="Reset" cssclass="button" name="Reset" value="Reset" />
                    </td>
                </tr>
        </asp:Panel>
        <asp:Panel ID="pnlconfirmation" Visible="False" runat="server">
            <tr align="center">
                <td colspan="2">
                    <br />
                    <br />
                    <br />
                    <br />
                    <input type="hidden" name="txtarearemarks">
                    <span class="shwText" id="shwMessage"></span>
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </td>
            </tr>
            <tr align="center">
                <td colspan="2">
                    <asp:HyperLink ID="hyplnkretry" runat="server"></asp:HyperLink>
                </td>
            </tr>
        </asp:Panel>
        </TBODY></table>
    </form>
</body>
</html>
