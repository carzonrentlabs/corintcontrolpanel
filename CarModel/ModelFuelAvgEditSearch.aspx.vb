Imports commonutility
Imports System.Data
Imports System.Data.SqlClient

Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System


Public Class ModelFuelAvgEditSearch
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnReset As System.Web.UI.WebControls.Button
    Protected WithEvents ddlModelID As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlFuelTypeID As System.Web.UI.WebControls.DropDownList

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not Page.IsPostBack Then
            Dim accessdata As clsutility
            accessdata = New clsutility
		        ddlModelID.DataSource = accessdata.funcGetSQLDataReader("select CarModelID, CarModelName from CORIntCarModelMaster where Active = 1  order by CarModelName")
		        ddlModelID.DataValueField = "CarModelID"
		        ddlModelID.DataTextField = "CarModelName"
		        ddlModelID.DataBind()
		        ddlModelID.Items.Insert(0, New ListItem("Any", -1))
		
		        ddlFuelTypeID.DataSource = accessdata.funcGetSQLDataReader("select FuelTypeID, FuelTypeName from CORIntFuelTypeMaster where Active = 1  order by FuelTypeName")
		        ddlFuelTypeID.DataValueField = "FuelTypeID"
		        ddlFuelTypeID.DataTextField = "FuelTypeName"
		        ddlFuelTypeID.DataBind()
		        ddlFuelTypeID.Items.Insert(0, New ListItem("Any", -1))
            accessdata.Dispose()

        End If
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Response.Redirect("ModelFuelAvgEditResult.aspx?ModelID=" & ddlModelID.SelectedItem.Value & "&FuelTypeID=" & ddlFuelTypeID.SelectedItem.Value)
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("ModelFuelAvgEditSearch.aspx")
    End Sub
End Class