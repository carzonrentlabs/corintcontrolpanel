<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SaveImagePathCarModelMaster.aspx.vb"
    Inherits="CarModel_SaveImagePathCarModelMaster" %>

<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>CarzonRent :: Internal software</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="../HertzInt.css" type="text/css" rel="stylesheet">

    <script language="JavaScript" src="../utilityfunction.js"></script>

    <script type="text/javascript">
    function validation()
	{
			if(document.forms[0].fileUploadWeb.value==" ")
			{
				alert("Please Select Payment Image.");
					return false;
			}			
			if(document.forms[0].fileUploadMobile.value=="")
			{				
					alert("Please Select Image.");
					return false;				
			}
			var strvalues
            strvalues=('fileUploadWeb,fileUploadMobile')            	
            return checkmandatory(strvalues)
	}		
	function checkmandatory(ctrntrlnames)
	{
		var strvalues
		strvalues=ctrntrlnames
		var arrvalue
		arrvalue=strvalues.split(",")
		for (i=0;i<arrvalue.length;i++)
		{
			var cntrlName = eval('document.forms[0].' + arrvalue[i])
			if (cntrlName.value=="")
			{
				alert("Please make sure all the fields marked with * are filled in")
				return false;
			}
		}		
	}	
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <uc1:Headerctrl ID="Headerctrl1" runat="server"></uc1:Headerctrl>
        <p>
            <asp:ValidationSummary ID="ValidationSummary_Image" runat="server" ShowMessageBox="true"
                HeaderText="Please make sure all the fields marked with * are filled in." ShowSummary="false">
            </asp:ValidationSummary>
        </p>
        <table align="center">
            <tr>
                <td colspan="2" align="center">
                    <asp:Label ID="lblErrorMsg" runat="server" Text="" Visible="false" ></asp:Label></td>
            </tr>
            
            <tr>
                <td>
                    *Car Model:
                </td>
                <td>
                    <asp:DropDownList ID="ddlCarModel" runat="server">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="rvf_ddlCarModel" runat="server" ErrorMessage="*" ControlToValidate="ddlCarModel" InitialValue="0"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    *Car Image for Web:
                </td>
                <td>
                    <asp:FileUpload ID="fileUploadWeb" runat="server" />
                    <asp:RequiredFieldValidator ID="rvf_UploadWeb" runat="server" ErrorMessage="*" ControlToValidate="fileUploadWeb"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    *Car Image for Mobile:
                </td>
                <td>
                    <asp:FileUpload ID="fileUploadMobile" runat="server" />
                    <asp:RequiredFieldValidator ID="rvf_UploadMobile" runat="server" ErrorMessage="*"
                        ControlToValidate="fileUploadMobile"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;</td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:Button ID="btnUploadImage" runat="server" Text="Upload Images" OnClick="btnUploadImage_Click" /></td>
            </tr>
        </table>
        <div>
        </div>
    </form>
</body>
</html>
