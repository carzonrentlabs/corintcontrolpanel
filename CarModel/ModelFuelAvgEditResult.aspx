<%@ Page Language="vb" AutoEventWireup="false" Src="ModelFuelAvgEditResult.aspx.vb" Inherits="ModelFuelAvgEditResult"%>
<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CarzonRent :: Internal software</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<uc1:Headerctrl id="Headerctrl1" runat="server"></uc1:Headerctrl>
			<table align="center">
				<tr>
					<td align="center"><STRONG><U>Edit fuel avg. norm for a model where</U></STRONG>
						<br>
						<br>
						<br>
					</td>
				</tr>
				<tr>
					<td valign="top">
						<asp:table ID="tblRecDetail" HorizontalAlign=Center Runat="server" BorderColor=#cccc99 BorderStyle="Solid" GridLines=both CellSpacing="0" CellPadding="0" >
							<asp:tablerow>
								<asp:tableheadercell>
									&nbsp;&nbsp;<asp:linkbutton runat="server" Text="Car Model" OnClick="SortGird" ID="Linkbutton1"></asp:linkbutton>&nbsp;&nbsp;
								</asp:tableheadercell>
								<asp:tableheadercell>
									&nbsp;&nbsp;<asp:linkbutton runat="server" Text="Fuel type" OnClick="SortGird" ID="Linkbutton2"></asp:linkbutton>&nbsp;&nbsp;
								</asp:tableheadercell>
								<asp:tableheadercell>
									&nbsp;&nbsp;<asp:linkbutton runat="server" Text="Fuel avg. norm<br>KMs / Fuel unit" OnClick="SortGird" ID="Linkbutton3"></asp:linkbutton>&nbsp;&nbsp;
								</asp:tableheadercell>								
								<asp:tableheadercell>
									&nbsp;&nbsp;<asp:Label ID="lblActive" Text ="Active<br>Non Active" runat ="server" ></asp:Label>
								</asp:tableheadercell>
								<asp:tableheadercell>
									&nbsp;&nbsp;<asp:Label ID="lblEdit" Text ="Edit" runat ="server" ></asp:Label>
								</asp:tableheadercell>
							</asp:tablerow>
						</asp:table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
