Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class CarModelEditForm
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents Validationsummary1 As System.Web.UI.WebControls.ValidationSummary
    Protected WithEvents txtcarmodel As System.Web.UI.WebControls.TextBox
    Protected WithEvents Requiredfieldvalidator1 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents ddlcompname As System.Web.UI.WebControls.DropDownList
    Protected WithEvents Requiredfieldvalidator2 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents ddlcatname As System.Web.UI.WebControls.DropDownList
    Protected WithEvents Requiredfieldvalidator3 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator4 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents txtarearemarks As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkActive As System.Web.UI.WebControls.CheckBox
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    Protected WithEvents txtSeatingCapacity As System.Web.UI.WebControls.TextBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not Page.IsPostBack Then
            populateddl()
            Dim dtrreader As SqlDataReader
            Dim accessdata As clsutility
            accessdata = New clsutility
            dtrreader = accessdata.funcGetSQLDataReader("select * from CORIntCarModelMaster where  carmodelid=" & Request.QueryString("id") & " ")
            dtrreader.Read()
            txtcarmodel.Text = dtrreader("carmodelname") & ""
            ddlcompname.Items.FindByValue(dtrreader("carcompid")).Selected = True
            ddlcatname.Items.FindByValue(dtrreader("carcatid")).Selected = True
            txtarearemarks.Text = dtrreader("remarks") & ""
            chkActive.Checked = dtrreader("active")
            If Not IsDBNull(dtrreader("seatingcapacity")) Then
                txtSeatingCapacity.Text = dtrreader("seatingcapacity")
            End If
            dtrreader.Close()
            accessdata.Dispose()

        End If
    End Sub

    Sub populateddl()
        'Dim dtrreader As SqlDataReader
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility

        If Session("provider_Id") <> 1 Then
            ddlcompname.DataSource = objAcessdata.funcGetSQLDataReader("select carcompname,carcompid from CORIntCarCompMaster where active=1 and carcompname <>'(Radio Taxi)' order by carcompname")
        Else
            ddlcompname.DataSource = objAcessdata.funcGetSQLDataReader("select carcompname,carcompid from CORIntCarCompMaster where active=1  order by carcompname")
        End If

        ddlcompname.DataValueField = "carcompid"
        ddlcompname.DataTextField = "carcompname"
        ddlcompname.DataBind()
        ddlcompname.Items.Insert(0, New ListItem("", ""))

        ddlcatname.DataSource = objAcessdata.funcGetSQLDataReader("select CarCatID,carcatname from CORIntCarCatMaster where active=1  and providerId=" & Session("provider_Id") & " order by carcatname")
        ddlcatname.DataValueField = "CarCatID"
        ddlcatname.DataTextField = "carcatname"
        ddlcatname.DataBind()
        ddlcatname.Items.Insert(0, New ListItem("", ""))

        objAcessdata.Dispose()

    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        Dim intuniqcheck As SqlParameter
        Dim intuniqvalue as int32

        cmd = New SqlCommand("procEditCarModel", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("@rowid", Request.QueryString("id"))
        cmd.Parameters.Add("@carmodelname", txtcarmodel.Text)
        cmd.Parameters.Add("@carcompid", ddlcompname.SelectedItem.Value)
        cmd.Parameters.Add("@carcatid", ddlcatname.SelectedItem.Value)
        cmd.Parameters.Add("@remarks ", txtarearemarks.Text)
        cmd.Parameters.Add("@active", get_YNvalue(chkActive))
        cmd.Parameters.Add("@seatingcapacity ", txtSeatingCapacity.Text)
        cmd.Parameters.Add("@modifiedby", Session("loggedin_user"))
       intuniqcheck = cmd.Parameters.Add("@uniqcheckval", SqlDbType.Int)
       intuniqcheck.Direction = ParameterDirection.Output

        '  Try
        MyConnection.Open()
        cmd.ExecuteNonQuery()
        intuniqvalue=cmd.Parameters("@uniqcheckval").Value
        MyConnection.Close()
          if not intuniqvalue = 0 then
                lblErrorMsg.visible=true
                lblErrorMsg.text="Car Model already exist."
                exit sub
            else
		        lblErrorMsg.visible=false
                pnlmainform.Visible = False
                pnlconfirmation.Visible = True

                lblMessage.Text = "You have updated the Car Model successfully"
                hyplnkretry.Text = "Edit another Car Model"
                hyplnkretry.NavigateUrl = "CarModelEditSearch.aspx"
            end if
        '   Catch
        '   End Try
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function

End Class
