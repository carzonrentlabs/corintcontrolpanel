<%@ Page Language="vb" AutoEventWireup="false" Src="CarModelAddForm.aspx.vb" Inherits="CarModelAddForm"%>
<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
<title>CarzonRent :: Internal software</title>
<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
<meta content="JavaScript" name="vs_defaultClientScript">
<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
<script language="JavaScript" src="../utilityfunction.js"></script>
<script language="javascript">
function validation()
{
	//alert("You are here.");
	//alert(document.forms[0].txtcarmodel.value);
	if(document.forms[0].txtcarmodel.value == "")
	{
		alert("Please Enter Model Name.");
		document.forms[0].txtcarmodel.focus();
		return false;
	}

	if(document.forms[0].ddlcompname.value == "")
	{
		alert("Please Select Company Name.");
		document.forms[0].ddlcompname.focus();
		return false;
	}

	if(document.forms[0].ddlcatname.value == "")
	{
		alert("Please Select Category Name.");
		document.forms[0].ddlcatname.focus();
		return false;
	}

	if(document.forms[0].ddlFuelTypeID.value == "")
	{
		alert("Please Select Fuel Type.");
		document.forms[0].ddlFuelTypeID.focus();
		return false;
	}

	if(document.forms[0].ddlModelFuelAvg.value == "")
	{
		alert("Please Select Fuel Average.");
		document.forms[0].ddlModelFuelAvg.focus();
		return false;
	}
}
</script>
</HEAD>
<body>
<form id="Form1" method="post" runat="server">
<uc1:headerctrl id="Headerctrl1" runat="server"></uc1:headerctrl>
<p><asp:validationsummary id="Validationsummary1" runat="server" ShowMessageBox="true" HeaderText="Please make sure all the fields marked with * are filled in." ShowSummary="false"></asp:validationsummary></p>
<table align="center">
<TBODY>
<asp:panel id="pnlmainform" Runat="server">
	<TR>
	<TD align="center" colSpan="2"><B><U>Add a Car Model</U></B>
	<BR>
	<BR>
	</TD>
	</TR>
	<tr>
	<td colspan="2" align="center">
	<asp:Label id="lblErrorMsg" cssclass="subRedHead" visible="false" runat="server"></asp:Label>
	</td>
	</tr>
	<TR>
	<TD>* Model Name (Eg. Ikon / Esteem, etc)
	</TD>
	<TD>
	<asp:textbox id="txtcarmodel" runat="server" CssClass="input" MaxLength="50"></asp:textbox>
	<asp:requiredfieldvalidator id="Requiredfieldvalidator1" runat="server" errormessage="" Display="None" Controltovalidate="txtcarmodel"></asp:requiredfieldvalidator>
	</TD>
	</TR>
	<TR>
	<TD>* Company Name
	</TD>
	<TD>
	<asp:DropDownList id="ddlcompname" runat="server" CssClass="input"></asp:DropDownList>
	<asp:requiredfieldvalidator id="Requiredfieldvalidator2" runat="server" errormessage="" Display="None" Controltovalidate="ddlcompname"></asp:requiredfieldvalidator>
	</TD>
	</TR>
	<TR>
	<TD>* Category Name
	</TD>
	<TD>
	<asp:DropDownList id="ddlcatname" runat="server" CssClass="input"></asp:DropDownList>
	<asp:requiredfieldvalidator id="Requiredfieldvalidator3" runat="server" errormessage="" Display="None" Controltovalidate="ddlcatname"></asp:requiredfieldvalidator>
	</TD>
	</TR>
	<!--Added By Rahul on 19-Sep-2011-->
	<TR>
	<TD>* Fuel Type
	</TD>
	<TD>
	<asp:DropDownList id="ddlFuelTypeID" runat="server" CssClass="input"></asp:DropDownList>
	<asp:requiredfieldvalidator id="Requiredfieldvalidator4" runat="server" errormessage="" Display="None" Controltovalidate="ddlFuelTypeID"></asp:requiredfieldvalidator>
	</TD>
	</TR>
	<TR>
	<TD>* Fuel Average Norm
	</TD>
	<TD>
	<asp:DropDownList id="ddlModelFuelAvg" runat="server" CssClass="input"></asp:DropDownList> KMs / Fuel unit
	<asp:requiredfieldvalidator id="Requiredfieldvalidator5" runat="server" errormessage="" Display="None" Controltovalidate="ddlModelFuelAvg"></asp:requiredfieldvalidator>
	</TD>
	</TR>
	<!--Added By Rahul-->
	
	<TR>
	<TD vAlign="top">Remarks(<SPAN class="shwText" id="shwMessage">0</SPAN>/2000 chars)</TD>
	<TD>
	<asp:textbox id="txtarearemarks" onkeydown="showLength(this.form.txtarearemarks,'document.all.shwMessage',2000)" onkeyup="showLength(this.form.txtarearemarks,'document.all.shwMessage',2000)" runat="server" CssClass="input" MaxLength="2000" Rows="3" Columns="50" TextMode="MultiLine"></asp:textbox>
	</TD>
	</TR>
	<TR>
	<TD>Active</TD>
	<TD>
	<asp:checkbox id="chkActive" runat="server" CssClass="input" Checked="True"></asp:checkbox>
	</TD>
	</TR>
	<TR>
	<TD align="center" colSpan="2">
	<asp:button id="btnSubmit" runat="server" CssClass="input" Text="Submit"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;
	<input type="Reset" CssClass="button" name="Reset" value="Reset" />
	</TD>
	</TR>
</asp:panel>
<asp:Panel ID="pnlconfirmation" Visible="False" Runat="server">
	<TR align="center">
	<TD colSpan="2"><BR>
		<BR>
		<BR>
		<BR>
		<asp:Label id="lblMessage" runat="server"></asp:Label>
	</TD>
	</TR>
	<TR align="center">
	<TD colSpan="2">
		<asp:HyperLink id="hyplnkretry" runat="server"></asp:HyperLink>
	</TD>
	</TR>
</asp:Panel>
</TBODY>
</table>
</form>
</body>
</HTML>