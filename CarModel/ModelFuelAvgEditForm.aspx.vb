Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class ModelFuelAvgEditForm
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents Validationsummary1 As System.Web.UI.WebControls.ValidationSummary
    Protected WithEvents Requiredfieldvalidator2 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator3 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator4 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    Protected WithEvents ddlModelID As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlFuelTypeID As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlModelFuelAvg As System.Web.UI.WebControls.DropDownList
    Protected WithEvents chkactive As System.Web.UI.WebControls.CheckBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not Page.IsPostBack Then
            populateddl()
            Dim dtrreader As SqlDataReader
            Dim accessdata As clsutility
            accessdata = New clsutility
            dtrreader = accessdata.funcGetSQLDataReader("select * from CORIntModelFuelAvgMaster where  ModelFuelAvgID=" & Request.QueryString("id") & " ")
            dtrreader.Read()
            ddlModelID.Items.FindByValue(dtrreader("ModelID")).Selected = True
            ddlFuelTypeID.Items.FindByValue(dtrreader("FuelTypeID")).Selected = True
            ddlModelFuelAvg.Items.FindByValue(dtrreader("ModelFuelAvg")).Selected = True
            If IsDBNull(dtrreader("active")) = True Then
                chkactive.Checked = False
            Else
                chkactive.Checked = dtrreader("active")
            End If

            dtrreader.Close()
            accessdata.Dispose()

        End If
    End Sub

    Sub populateddl()
        Dim dtrreader As SqlDataReader
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility

        'ddlModelID.DataSource = objAcessdata.funcGetSQLDataReader("select CarModelID, CarModelName from CORIntCarModelMaster where Active = 1  order by CarModelName")
        ddlModelID.DataSource = objAcessdata.funcGetSQLDataReader("select CarModelID, CarModelName from CORIntCarModelMaster order by CarModelName")
        ddlModelID.DataValueField = "CarModelID"
        ddlModelID.DataTextField = "CarModelName"
        ddlModelID.DataBind()
        ddlModelID.Items.Insert(0, New ListItem("", ""))

        ddlFuelTypeID.DataSource = objAcessdata.funcGetSQLDataReader("select FuelTypeID, FuelTypeName from CORIntFuelTypeMaster where Active = 1  order by FuelTypeName")
        ddlFuelTypeID.DataValueField = "FuelTypeID"
        ddlFuelTypeID.DataTextField = "FuelTypeName"
        ddlFuelTypeID.DataBind()
        ddlFuelTypeID.Items.Insert(0, New ListItem("", ""))


        objAcessdata.funcpopulatenumddw(100, 1, ddlModelFuelAvg)

        objAcessdata.Dispose()

    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        Dim intuniqcheck As SqlParameter
        Dim intuniqvalue as int32

        cmd = New SqlCommand("procEditModelFuelAvg", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("@rowid", Request.QueryString("id"))
        cmd.Parameters.Add("@ModelID", ddlModelID.SelectedItem.Value)
        cmd.Parameters.Add("@FuelTypeID", ddlFuelTypeID.SelectedItem.Value)
        cmd.Parameters.Add("@ModelFuelAvg", ddlModelFuelAvg.SelectedItem.Value)
        intuniqcheck = cmd.Parameters.Add("@uniqcheckval", SqlDbType.Int)
        cmd.Parameters.Add("@Active", get_YNvalue(chkactive))
        cmd.Parameters.Add("@modifiedby", Session("loggedin_user"))
        intuniqcheck.Direction = ParameterDirection.Output

        '  Try
        MyConnection.Open()
        cmd.ExecuteNonQuery()
        intuniqvalue=cmd.Parameters("@uniqcheckval").Value
        MyConnection.Close()
          if not intuniqvalue = 0 then
                lblErrorMsg.visible=true
                lblErrorMsg.text="Car Model and fuel type avg. norm combination already exists."
                exit sub
            else
		        lblErrorMsg.visible=false
                pnlmainform.Visible = False
                pnlconfirmation.Visible = True

                lblMessage.Text = "You have updated the fuel avg. norm for the model successfully"
                hyplnkretry.Text = "Edit another fuel avg. norm for a model"
                hyplnkretry.NavigateUrl = "ModelFuelAvgEditSearch.aspx"
            end if
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function

End Class
