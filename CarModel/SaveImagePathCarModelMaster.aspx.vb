Imports System
Imports System.Collections
Imports System.ComponentModel
Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Drawing
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Web.Mail
Imports System.IO
Imports System.IO.FileStream
Imports System.IO.File
Imports System.Net
Imports System.Text
Imports System.Object
Imports System.MarshalByRefObject
Imports System.Net.WebRequest
Partial Class CarModel_SaveImagePathCarModelMaster
    Inherits System.Web.UI.Page

    Protected Sub btnUploadImage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUploadImage.Click
        If (Page.IsValid) Then
            btnUploadImage.Attributes("onClick") = "return validation();"
            Try

           
                Dim MyConnection As SqlConnection
                MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
                Dim cmd As SqlCommand
                Dim strFileWeb As String
                Dim strFileMobile As String
                If fileUploadWeb.PostedFile.FileName <> "" And fileUploadWeb.PostedFile.ContentLength > 0 Then
                    strFileWeb = Path.GetFileName(fileUploadWeb.FileName)
                    Dim strFileWebPath As String = System.IO.Path.GetDirectoryName(fileUploadWeb.FileName)
                    If strFileWeb <> "" Then
                        If fileUploadMobile.PostedFile.FileName <> "" And fileUploadMobile.PostedFile.ContentLength > 0 Then
                            strFileMobile = Path.GetFileName(fileUploadMobile.FileName)
                            Dim strFileMobilePath As String = System.IO.Path.GetDirectoryName(fileUploadMobile.FileName)
                            If strFileMobile <> "" Then
                                strFileWeb = Path.GetFileName(fileUploadWeb.FileName)
                                fileUploadWeb.SaveAs("D:\Upload\CarModel\" + strFileWeb)
                                strFileMobile = Path.GetFileName(fileUploadMobile.FileName)
                                fileUploadMobile.SaveAs("D:\Upload\CarModel\" + strFileMobile)
                                cmd = New SqlCommand("prc_uploadCarmodel", MyConnection)
                                cmd.CommandType = CommandType.StoredProcedure
                                cmd.Parameters.Add("@CarImageWebPath", "D:\Upload\CarModel\Web_" & strFileWeb)
                                cmd.Parameters.Add("@CarImageMobilePath", "D:\Upload\CarModel\Mobile_" & strFileMobile)
                                cmd.Parameters.Add("@CarModelID", ddlCarModel.SelectedValue.ToString())
                                MyConnection.Open()
                                cmd.ExecuteNonQuery()
                                MyConnection.Close()
                            Else
                                ' UserName exist, display message
                                lblErrorMsg.Visible = True
                                lblErrorMsg.Text = "Upload Image."
                            End If
                            '*************************************************************************************************
                        Else
                            lblErrorMsg.Visible = True
                            lblErrorMsg.Text = "Upload Image."
                        End If
                    Else
                        lblErrorMsg.Visible = True
                        lblErrorMsg.Text = "Upload Image."
                    End If
                Else
                    lblErrorMsg.Visible = True
                    lblErrorMsg.Text = "Upload Image."
                End If
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
        Else
            lblErrorMsg.Visible = True
            lblErrorMsg.Text = "Upload Image."
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            populateddl()
        End If
    End Sub
    Sub populateddl()
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility
        ddlCarModel.DataSource = objAcessdata.funcGetSQLDataReader("select CarModelID,CarModelName  from CORIntCarModelMaster where active=1 order by CarModelName")
        ddlCarModel.DataValueField = "CarModelID"
        ddlCarModel.DataTextField = "CarmodelName"
        ddlCarModel.DataBind()
        ddlCarModel.Items.Insert(0, New ListItem("Select Carmodel", "0"))
        objAcessdata.Dispose()
        objAcessdata.Dispose()

    End Sub
End Class
