Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.text
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Imports System.Web.UI
Public Class ModelFuelAvgEditResult
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tblRecDetail As System.Web.UI.WebControls.Table

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not Page.IsPostBack Then
            getvalue("b.CarModelName, c.FuelTypeName")
        End If

    End Sub
    Sub getvalue(ByVal strorderby As String)
        Dim strquery As StringBuilder
        Dim dtrreader As SqlDataReader
        Dim accessdata As clsutility
        accessdata = New clsutility
        strquery = New StringBuilder("select a.ModelFuelAvgID, a.ModelFuelAvg, b.CarModelName, c.FuelTypeName,(case a.active  when 1 then 'Active' else 'Non Active' end) as Active from CORIntModelFuelAvgMaster as a, CORIntCarModelMaster as b, CORIntFuelTypeMaster as c where a.ModelID = b.CarModelID and a.FuelTypeID = c.FuelTypeID ")
        If Request.QueryString("ModelID") <> "-1" Then
            strquery.Append(" and a.ModelID=" & Request.QueryString("ModelID") & " ")
        End If
        If Request.QueryString("FuelTypeID") <> "-1" Then
            strquery.Append(" and a.FuelTypeID=" & Request.QueryString("FuelTypeID") & " ")
        End If
        strquery.Append(" order by " & strorderby & "")

        dtrreader = accessdata.funcGetSQLDataReader(strquery.ToString)
        While dtrreader.Read
            Dim Temprow As New TableRow
            Temprow.HorizontalAlign = HorizontalAlign.Center
            Dim Tempcell As New TableCell
            Tempcell.Controls.Add(New LiteralControl(dtrreader("CarModelName") & ""))
            Temprow.Cells.Add(Tempcell)

            Dim Tempcel2 As New TableCell
            Tempcel2.Controls.Add(New LiteralControl(dtrreader("FuelTypeName") & ""))
            Temprow.Cells.Add(Tempcel2)

            Dim Tempcel3 As New TableCell
            Tempcel3.Controls.Add(New LiteralControl(dtrreader("ModelFuelAvg") & ""))
            Temprow.Cells.Add(Tempcel3)

            Dim Tempcel5 As New TableCell
            Tempcel5.Controls.Add(New LiteralControl(dtrreader("Active") & ""))
            Temprow.Cells.Add(Tempcel5)

            Dim Tempcel4 As New TableCell
            Tempcel4.Controls.Add(New LiteralControl("<a href=ModelFuelAvgEditForm.aspx?ID=" & dtrreader("ModelFuelAvgID") & " >Edit</a>"))
            Temprow.Cells.Add(Tempcel4)

            'adding the Tables rows to the table
            tblRecDetail.Rows.Add(Temprow)

        End While
        dtrreader.Close()
        accessdata.Dispose()


    End Sub
    Sub SortGird(ByVal sender As Object, ByVal e As System.EventArgs)
        Select Case sender.id
            Case "Linkbutton1"
                getvalue("b.CarModelName")
            Case "Linkbutton2"
                getvalue("c.FuelTypeName")
            Case "Linkbutton3"
                getvalue("a.ModelFuelAvg")
        End Select

    End Sub

End Class
