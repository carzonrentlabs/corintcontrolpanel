<%@ Page Language="vb" AutoEventWireup="false" Src="ModelFuelAvgEditForm.aspx.vb" Inherits="ModelFuelAvgEditForm"%>
<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CarzonRent :: Internal software</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../utilityfunction.js"></script>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<p>
				<uc1:Headerctrl id="Headerctrl1" runat="server"></uc1:Headerctrl><asp:validationsummary id="Validationsummary1" runat="server" ShowMessageBox="true" HeaderText="Please make sure all the fields marked with * are filled in."
					ShowSummary="false"></asp:validationsummary></p>
			<table align="center">
				<asp:panel id="pnlmainform" Runat="server">
					<TBODY>
						<tr><td colspan="2" align="center"><asp:Label id="lblErrorMsg" cssclass="subRedHead" visible="false" runat="server"></asp:Label></td></tr>
						<TR>
							<TD>* Model Name
							</TD>
							<TD>
								<asp:DropDownList id="ddlModelID" runat="server" CssClass="input"></asp:DropDownList>
								<asp:requiredfieldvalidator id="Requiredfieldvalidator2" runat="server" errormessage="" Display="None" Controltovalidate="ddlModelID"></asp:requiredfieldvalidator></TD>
						</TR>
						<TR>
							<TD>* Fuel Type
							</TD>
							<TD>
								<asp:DropDownList id="ddlFuelTypeID" runat="server" CssClass="input"></asp:DropDownList>
								<asp:requiredfieldvalidator id="Requiredfieldvalidator3" runat="server" errormessage="" Display="None" Controltovalidate="ddlFuelTypeID"></asp:requiredfieldvalidator></TD>
						</TR>
						<TR>
							<TD>* Fuel Average Norm
							</TD>
							<TD>
								<asp:DropDownList id="ddlModelFuelAvg" runat="server" CssClass="input"></asp:DropDownList> KMs / Fuel unit
								<asp:requiredfieldvalidator id="Requiredfieldvalidator4" runat="server" errormessage="" Display="None" Controltovalidate="ddlModelFuelAvg"></asp:requiredfieldvalidator></TD>
						</TR>
						
						<TR>
							<TD>* Active
							</TD>
							<TD><asp:checkbox id="chkactive" runat="server" CssClass="input" Checked="True"></asp:checkbox></TD>
						</TR>
						<TR>
							<TD align="center" colSpan="2">
								<asp:button id="btnSubmit" runat="server" CssClass="input" Text="Submit"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="Reset" CssClass="button" name="Reset" value="Reset" /></TD>
						</TR>
				</asp:panel>
				<asp:Panel ID="pnlconfirmation" Visible="False" Runat="server">
					<TR align="center">
						<TD colSpan="2"><BR>
							<BR>
							<BR>
							<BR>
							<INPUT type="hidden" name="txtarearemarks">
								<SPAN class="shwText" id="shwMessage"></SPAN>
							<asp:Label id="lblMessage" runat="server"></asp:Label></TD>
					</TR>
					<TR align="center">
						<TD colSpan="2">
							<asp:HyperLink id="hyplnkretry" runat="server"></asp:HyperLink></TD>
					</TR>
				</asp:Panel></TBODY></table>
		</form>
	</body>
</HTML>
