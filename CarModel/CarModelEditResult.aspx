<%@ Page Language="vb" AutoEventWireup="false" Src="CarModelEditResult.aspx.vb" Inherits="CarModelEditResult" %>

<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>CarzonRent :: Internal software</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <link href="../HertzInt.css" type="text/css" rel="stylesheet">
</head>
<body>
    <form id="Form1" method="post" runat="server">
    <uc1:Headerctrl id="Headerctrl1" runat="server">
    </uc1:Headerctrl>
    <table align="center">
        <tr>
            <td align="center">
                <strong><u>Edit a Car Model where</u></strong>
                <br>
                <br>
                <br>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <asp:Table ID="tblRecDetail" HorizontalAlign="Center" runat="server" BorderColor="#cccc99"
                    BorderStyle="Solid" GridLines="both" CellSpacing="0" CellPadding="0">
                    <asp:TableRow>
                        <asp:TableHeaderCell>
                            &nbsp;&nbsp;<asp:LinkButton runat="server" Text="Car Model Name" OnClick="SortGird"
                                ID="Linkbutton1"></asp:LinkButton>&nbsp;&nbsp;
                        </asp:TableHeaderCell>
                        <asp:TableHeaderCell>
                            &nbsp;&nbsp;<asp:LinkButton runat="server" Text="Car Company Name" OnClick="SortGird"
                                ID="Linkbutton2"></asp:LinkButton>&nbsp;&nbsp;
                        </asp:TableHeaderCell>
                        <asp:TableHeaderCell>
                            &nbsp;&nbsp;<asp:LinkButton runat="server" Text="Seating Capacity" OnClick="SortGird"
                                ID="Linkbutton3"></asp:LinkButton>&nbsp;&nbsp;
                        </asp:TableHeaderCell>
                        <asp:TableHeaderCell>
                            &nbsp;&nbsp;<asp:LinkButton runat="server" Text="Active" OnClick="SortGird" ID="Linkbutton4"></asp:LinkButton>&nbsp;&nbsp;
                        </asp:TableHeaderCell>
                        <asp:TableHeaderCell>
									&nbsp;&nbsp;
                        </asp:TableHeaderCell>
                    </asp:TableRow>
                </asp:Table>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
