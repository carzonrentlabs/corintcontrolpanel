Imports commonutility
Imports System.Data
Imports System.Data.SqlClient

Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System


Public Class CarModelEditSearch
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnReset As System.Web.UI.WebControls.Button
    Protected WithEvents ddModelName As System.Web.UI.WebControls.DropDownList

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not Page.IsPostBack Then
            Dim dtrreader As SqlDataReader
            Dim accessdata As clsutility
            accessdata = New clsutility
            dtrreader = accessdata.funcGetSQLDataReader("select CarModelID,CarModelName  from CORIntCarModelMaster  as a inner join CORIntCarCatMaster as b  on a.CarCatID=b.CarCatID  where ProviderId = " & Session("provider_Id") & " And a.Active = 1 And b.Active = 1 order by CarModelName")
            ddModelName.DataSource = dtrreader
            ddModelName.DataValueField = "CarModelID"
            ddModelName.DataTextField = "CarModelName"
            ddModelName.DataBind()
            ddModelName.Items.Insert(0, New ListItem("Any", -1))
            dtrreader.Close()
            accessdata.Dispose()

        End If
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Response.Redirect("CarModelEditResult.aspx?id=" & ddModelName.SelectedItem.Value)
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("CarModelEditSearch.aspx")
    End Sub
End Class
