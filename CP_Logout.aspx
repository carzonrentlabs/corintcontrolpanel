<%@ Page Language="vb" AutoEventWireup="false" Inherits="CP_Logout" src="CP_Logout.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CP_Logout</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="HertzInt.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<asp:panel id="pnlmain" Runat="server">
				<TABLE align="center">
					<TR>
						<TD class="redHead" align="center" colSpan="2"><BR>
						<B>You have successfully logged out</B><BR><BR>
							<BR>
							<BR>
							<BR>
							<asp:Label id="lblErrMsg" visible="false" runat="server"></asp:Label>
							<BR>
							<BR>
							<BR>
							<B>Log in to the CarzonRent :: Internal software</B><BR>
							<BR>
						</TD>
					</TR>
					<TR>
						<TD class="subRedHead">Login</TD>
						<TD align="center">
							<asp:textbox id="txtLogin" runat="server" MaxLength="20" CssClass="input"></asp:textbox></TD>
					</TR>
					<TR>
						<TD class="subRedHead">Password</TD>
						<TD align="center">
							<asp:TextBox id="txtPwd" runat="server" MaxLength="20" CssClass="input" TextMode="Password"></asp:TextBox></TD>
					</TR>
					<TR>
						<TD align="center" colSpan="2">
							<asp:Button id="btnLogin" runat="server" CssClass="input" Text="Login"></asp:Button></TD>
					</TR>
				</TABLE>
			</asp:panel><asp:panel id="pnlConfirmation" Visible="False" Runat="server">
				<TABLE align="center">
					<TR>
						<TD>
								<BR>
							<BR>
							<BR>
							<BR>
							<BR>
							<BR>
							<asp:Label id="lblMessage" runat="server"></asp:Label></TD>
					</TR>
					<TR align="center">
						<TD>
							<asp:HyperLink id="hyplnkretry" runat="server"></asp:HyperLink></TD>
					</TR>
				</TABLE>
			</asp:panel>
		</form>
	</body>
</HTML>
