Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class CarVendorEditForm
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtvendorname As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtaddress As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlcity As System.Web.UI.WebControls.DropDownList
    'City Added By Rahul for reporting purpose
    Protected WithEvents ddlcitymain As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtcontactfname As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtcontacmname As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtcontactlname As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtSTDCode As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtphone1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtphone2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtphone3 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtfax As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtemail As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlrating As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtservtax As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtvat As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtPAN As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtRemarks As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkactive As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkbooked As System.Web.UI.WebControls.CheckBox
    Protected WithEvents btnsubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnreset As System.Web.UI.WebControls.Button
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label   
    Protected WithEvents txtContractStartDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtContractEndDate As System.Web.UI.WebControls.TextBox
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btnsubmit.Attributes("onclick") = "return validation();"
        If Not Page.IsPostBack Then
            populateddl()
            Dim dtrreader As SqlDataReader
            Dim accessdata As clsutility
            accessdata = New clsutility
            dtrreader = accessdata.funcGetSQLDataReader("select * from CORIntCarVendorMaster  where  carvendorid=" & Request.QueryString("id") & " ")
            dtrreader.Read()

            autoselec_ddl(ddlcity, dtrreader("carvendorcityid"))
            autoselec_ddl(ddlcitymain, dtrreader("carvendorBasecityid"))
            autoselec_ddl(ddlrating, dtrreader("preferencerating"))
            txtvendorname.Text = dtrreader("carvendorname") & ""
            txtaddress.Text = dtrreader("carvendoraddress") & ""
            txtcontactfname.Text = dtrreader("contactfname") & ""
            txtcontacmname.Text = dtrreader("contactmname") & ""
            txtcontactlname.Text = dtrreader("contactlname") & ""

            txtSTDCode.Text = dtrreader("STDCode") & ""
            txtphone1.Text = dtrreader("contactph1") & ""
            txtphone2.Text = dtrreader("contactph2") & ""
            txtphone3.Text = dtrreader("contactph3") & ""
            txtfax.Text = dtrreader("fax") & ""
            txtemail.Text = dtrreader("emailid") & ""
            txtservtax.Text = dtrreader("servicetaxno") & ""
            txtvat.Text = dtrreader("vatregnno") & ""
            txtPAN.Text = dtrreader("PANNo") & ""

            txtContractStartDate.Text = dtrreader("ContractStartDate") & ""
            txtContractEndDate.Text = dtrreader("ContractEndDate") & ""

            txtRemarks.Text = dtrreader("remarks") & ""
            chkactive.Checked = dtrreader("active")
            chkbooked.Checked = dtrreader("BookedYN")
            dtrreader.Close()
            accessdata.Dispose()
        End If

    End Sub

    Sub populateddl()
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility

        ddlcity.DataSource = objAcessdata.funcGetSQLDataReader("select cityname,cityid from CORIntCityMaster  order by cityname")
        ddlcity.DataValueField = "cityid"
        ddlcity.DataTextField = "cityname"
        ddlcity.DataBind()
        ddlcity.Items.Insert(0, New ListItem("", ""))

        ddlcitymain.DataSource = objAcessdata.funcGetSQLDataReader("select cityname,cityid from CORIntCityMaster  order by cityname")
        ddlcitymain.DataValueField = "cityid"
        ddlcitymain.DataTextField = "cityname"
        ddlcitymain.DataBind()
        ddlcitymain.Items.Insert(0, New ListItem("", ""))


        objAcessdata.funcpopulatenumddw(10, 1, ddlrating)


        objAcessdata.Dispose()

    End Sub
    Function autoselec_ddl(ByVal ddlname As DropDownList, ByVal selectvalue As Int32)
        If Not selectvalue = "0" Then
            ddlname.Items.FindByValue(selectvalue).Selected = True
        End If
    End Function

    Private Sub btnreset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnreset.Click
        Response.Redirect("CarVendorEditForm.aspx?id=" & Request.QueryString("id"))
    End Sub

    Private Sub btnsubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsubmit.Click

        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        Dim intuniqcheck As SqlParameter
        Dim intuniqvalue as int32
        cmd = New SqlCommand("procEditCarVendorMaster_New", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("@rowid", Request.QueryString("id"))

        cmd.Parameters.Add("@carvendorname", txtvendorname.Text)
        cmd.Parameters.Add("@carvendoraddress", txtaddress.Text)
        cmd.Parameters.Add("@carvendorcityid", ddlcity.SelectedItem.Value)
        cmd.Parameters.Add("@carvendorBasecityid", ddlcitymain.SelectedItem.Value)
        cmd.Parameters.Add("@contactfname", txtcontactfname.Text)
        cmd.Parameters.Add("@contactmname", txtcontacmname.Text)
        cmd.Parameters.Add("@contactlname", txtcontactlname.Text)
        cmd.Parameters.Add("@STDCode", txtSTDCode.Text)
        cmd.Parameters.Add("@contactph1", txtphone1.Text)
        cmd.Parameters.Add("@contactph2", txtphone2.Text)
        cmd.Parameters.Add("@contactph3", txtphone3.Text)
        cmd.Parameters.Add("@fax", txtfax.Text)
        cmd.Parameters.Add("@emailid", txtemail.Text)
        cmd.Parameters.Add("@preferencerating", ddlrating.SelectedItem.Value)
        cmd.Parameters.Add("@servicetaxno", txtservtax.Text)
        cmd.Parameters.Add("@vatregnno", txtvat.Text)
        cmd.Parameters.Add("@PAN", txtPAN.Text)
        cmd.Parameters.Add("@ContractStartDate", txtContractStartDate.Text)
        cmd.Parameters.Add("@ContractEndDate", txtContractEndDate.Text)
        cmd.Parameters.Add("@remarks", txtRemarks.Text)
        cmd.Parameters.Add("@active", get_YNvalue(chkactive))
        cmd.Parameters.Add("@bookedYN", get_YNvalue(chkbooked))

        cmd.Parameters.Add("@modifiedby", Session("loggedin_user"))
       intuniqcheck = cmd.Parameters.Add("@uniqcheckval", SqlDbType.Int)
       intuniqcheck.Direction = ParameterDirection.Output

        '  Try
        MyConnection.Open()
        cmd.ExecuteNonQuery()
        intuniqvalue=cmd.Parameters("@uniqcheckval").Value
        MyConnection.Close()
           if not intuniqvalue = 0 then
                lblErrorMsg.visible=true
                lblErrorMsg.text="Car Vendor already exist."
                exit sub
            else
		        lblErrorMsg.visible=false
                pnlmainform.Visible = False
                pnlconfirmation.Visible = True

                lblMessage.Text = "You have updated the Car Vendor successfully"
                hyplnkretry.Text = "Edit another Car Vendor"
                hyplnkretry.NavigateUrl = "CarVendorEditSearch.aspx"
            end if
        '   Catch
        '   End Try
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function

End Class
