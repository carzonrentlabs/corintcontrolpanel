<%@ Page Language="vb" AutoEventWireup="false" src="CarVendorAddForm.aspx.vb" Inherits="CarVendorAddForm"%>
<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CarzonRent :: Internal software control panel</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script type="text/javascript" language="JavaScript" src="../../utilityfunction.js"></script>
		<script type="text/javascript" language="JavaScript" src="../../JScripts/Datefunc.js"></script>
		<LINK href="../../HertzInt.css" type="text/css" rel="stylesheet">
		<script type="text/javascript" language="javascript">
		function validation()
		{
				if (isNaN(document.forms[0].txtSTDCode.value))
				{
					alert("The STD code should be numeric only")
					return false
				}
					if(document.forms[0].txtemail.value!="")
				{
					
					var theStr=document.forms[0].txtemail.value;
					var atIndex = theStr.indexOf('@'); 
					var dotIndex = theStr.indexOf('.', atIndex); 
					theSub = theStr.substring(0, dotIndex+1) 

				if ((atIndex < 1)||(atIndex != theStr.lastIndexOf('@'))||(dotIndex < atIndex + 2)||(theStr.length <= theSub.length)) 
				{ 
					alert("Email ID is not a valid Email ID");
					return false;
				}
				} 	
	

			var strvalues
			strvalues=('txtvendorname,txtaddress,ddlcity, ddlcitymain,txtcontactfname,txtcontactlname, txtSTDCode,txtphone1,txtemail,ddlrating,txtservtax,txtContractStartDate,txtContractEndDate')
			return checkmandatory(strvalues);		
		}		
		function dateReg(obj)
        {
            if(obj.value!="")
            {
                // alert(obj.value);
                var reg = /((0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[0-1])-(201[0-9]))/
                if(reg.test(obj.value))
                {
                    //alert('valid');
                }
                else
                {
                    alert('notvalid');
                    obj.value="";
                }
            }
        }			

		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server" enctype="multipart/form-data">
			<uc1:Headerctrl id="Headerctrl1" runat="server"></uc1:Headerctrl>
			<TABLE id="Table1" align="center">
				<TBODY>
					<asp:panel id="pnlmainform" Runat="server">
						<TR>
							<TD align="center" colSpan="2"><STRONG><U>Add a Car Vendor</U></STRONG>
							</TD>
						</TR>
						<tr><td colspan="2" align="center"><asp:Label id="lblErrorMsg" cssclass="subRedHead" visible="false" runat="server"></asp:Label></td></tr>
						<TR>
							<TD align="center" colSpan="2">&nbsp;&nbsp;</TD>
						</TR>
						<TR>
							<TD>* Vendor Name
							</TD>
							<TD>
								<asp:textbox id="txtvendorname" runat="server" MaxLength="100" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>* Address</TD>
							<TD>
								<asp:textbox id="txtaddress" runat="server" MaxLength="250" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>* City
							</TD>
							<TD>
								<asp:DropDownList id="ddlcity" runat="server" CssClass="input"></asp:DropDownList></TD>
						</TR>
						<TR>
							<TD>* Main City
							</TD>
							<TD>
								<asp:DropDownList id="ddlcitymain" runat="server" CssClass="input"></asp:DropDownList></TD>
						</TR>
						<TR>
							<TD>* Contact - First Name
							</TD>
							<TD>
								<asp:textbox id="txtcontactfname" runat="server" MaxLength="50" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>Contact - Middle Name
							</TD>
							<TD>
								<asp:textbox id="txtcontacmname" runat="server" MaxLength="50" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>* Contact - Last Name</TD>
							<TD>
								<asp:textbox id="txtcontactlname" runat="server" MaxLength="50" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>* STD Code
							</TD>
							<TD>
								<asp:textbox id="txtSTDCode" size="10" runat="server" MaxLength="10" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>* Phone 1
							</TD>
							<TD>
								<asp:textbox id="txtphone1" runat="server" MaxLength="20" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>Phone 2</TD>
							<TD>
								<asp:textbox id="txtphone2" runat="server" MaxLength="20" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>Phone 3
							</TD>
							<TD>
								<asp:textbox id="txtphone3" runat="server" MaxLength="20" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>Fax
							</TD>
							<TD>
								<asp:textbox id="txtfax" runat="server" MaxLength="20" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>* Email ID
							</TD>
							<TD>
								<asp:textbox id="txtemail" runat="server" MaxLength="100" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>* Preference Rating (1 is hightest)
							</TD>
							<TD>
								<asp:DropDownList id="ddlrating" runat="server" CssClass="input"></asp:DropDownList></TD>
						</TR>
						<TR>
							<TD>*Service Tax #
							</TD>
							<TD>
								<asp:textbox id="txtservtax" runat="server" MaxLength="20" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>VAT Registration #
							</TD>
							<TD>
								<asp:textbox id="txtvat" runat="server" MaxLength="20" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>PAN Card #
							</TD>
							<TD>
								<asp:textbox id="txtPAN" runat="server" MaxLength="20" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD style="HEIGHT: 25px" vAlign="top">Contract Start Date</TD>
							<TD style="HEIGHT: 18px">
								<asp:textbox id="txtContractStartDate" runat="server" CssClass="input" MaxLength="12" size="12"
									onblur = "dateReg(this);"></asp:textbox><A onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;"
									href="javascript:show_calendar('Form1.txtContractStartDate');"><IMG height="21" src="../../images/show-calendar.gif" width="24" border="0"></A></TD>
						</TR>
						<TR>
							<TD style="HEIGHT: 25px" vAlign="top">Contract End Date</TD>
							<TD style="HEIGHT: 18px">
								<asp:textbox id="txtContractEndDate" runat="server" CssClass="input" MaxLength="12" size="12"
									onblur = "dateReg(this);"></asp:textbox><A onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;"
									href="javascript:show_calendar('Form1.txtContractEndDate');"><IMG height="21" src="../../images/show-calendar.gif" width="24" border="0"></A></TD>
						</TR>						
						<TR>
							<TD vAlign="top">Remarks(<SPAN class="shwText" id="shwMessage">0</SPAN>/2000 chars)</TD>
							<TD>
								<asp:textbox id="txtRemarks" onkeydown="showLength(this.form.txtRemarks,'document.all.shwMessage',2000)"
									onkeyup="showLength(this.form.txtRemarks,'document.all.shwMessage',2000)" runat="server"
									MaxLength="2000" CssClass="input" TextMode="MultiLine" Columns="50" Rows="3"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>Active</TD>
							<TD>
								<asp:checkbox id="chkactive" runat="server" CssClass="input" Checked="True"></asp:checkbox></TD>
						</TR>
												<TR>
							<TD>Package Sharing on Car Booked</TD>
							<TD>
								<asp:checkbox id="chkbooked" runat="server" CssClass="input" Checked="True"></asp:checkbox></TD>
						</TR>
						<TR>
							<TD align="center" colSpan="2">
								<asp:button id="btnsubmit" runat="server" CssClass="button" Text="Submit"></asp:button>&nbsp;&nbsp;
								
<input type="Reset" CssClass="button" name="Reset" value="Reset" /></TD>
						</TR>
				</asp:panel><asp:panel id="pnlconfirmation" Runat="server" Visible="False">
					<TR align="center">
						<TD colSpan="2">
							<asp:Label id="lblMessage" runat="server"></asp:Label></TD>
					</TR>
					<TR align="center">
						<TD colSpan="2">
							<asp:HyperLink id="hyplnkretry" runat="server"></asp:HyperLink></TD>
					</TR>
				</asp:panel></TBODY></TABLE>
		</form>
	</body>
</HTML>
