Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.text
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class CarVendorEditResult
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tblRecDetail As System.Web.UI.WebControls.Table

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not Page.IsPostBack Then
            getvalue("VCM.carvendorname")
        End If
    End Sub
    Sub getvalue(ByVal strorderby As String)
        Dim strquery As StringBuilder
        Dim dtrreader As SqlDataReader
        Dim accessdata As clsutility
        accessdata = New clsutility

        strquery = New StringBuilder("select distinct VCM.carvendorid,a.cityname,VCM.carvendorname, case VCM.active when '1' then 'Active' when '0' then 'Not Active' end as active from CORIntCarVendorMaster VCM, CORIntCityMaster a , CORIntUnitCityMaster as b, CORIntUnitMaster as c where a.cityid=VCM.carvendorcityid and a.nearestUnitCityId=b.UnitCityId and a.Active = 1 and a.CityName <> '' and c.UnitCityID = b.UnitCityID ")


        'Dim objAcessdataNw As clsutility
        'objAcessdataNw = New clsutility
        'Dim strqueryNew As String
        'Dim dtrreaderNew As SqlDataReader
        'Dim objAcessdataMapping As clsutility 'Changes
        'objAcessdataMapping = New clsutility 'Changes
        'Dim strqueryMapping As String 'Changes
        'Dim dtrreaderMapping As SqlDataReader 'Changes
        'Dim UnitID As String 'Changes

        'strqueryNew = New String("SELECT a.AccessType, a.UnitID, b.UnitCityID, c.Region FROM CORIntSysUsersMaster as a, CORIntUnitMaster as b, CORIntUnitCityMaster as c WHERE a.UnitID = b.UnitID and b.UnitCityID = c.UnitCityID and a.SysUserID =" & Session("loggedin_user"))
        'dtrreaderNew = objAcessdataNw.funcGetSQLDataReader(strqueryNew.ToString)

        'Do While dtrreaderNew.Read

        '    'Changes
        '    If dtrreaderNew("AccessType") = "SU" Or dtrreaderNew("AccessType") = "RN" Then
        '        strqueryMapping = New String("select UM.UnitID, UM.referencedUnitID from corintunitmapping as UM, CORIntUnitMaster as b, CORIntUnitCityMaster as c WHERE UM.referencedUnitID = b.UnitID and b.UnitCityID = c.UnitCityID and UM.UnitID = " & dtrreaderNew("UnitID"))
        '    ElseIf dtrreaderNew("AccessType") = "CT" Then
        '        strqueryMapping = New String("select UM.UnitID, c.UnitCityID as referencedUnitID from corintunitmapping as UM, CORIntUnitMaster as b, CORIntUnitCityMaster as c WHERE UM.referencedUnitID = b.UnitID and b.UnitCityID = c.UnitCityID and UM.UnitID = " & dtrreaderNew("UnitID"))
        '    Else
        '        UnitID = dtrreaderNew("UnitID")
        '    End If

        '    If dtrreaderNew("AccessType") = "SU" Or dtrreaderNew("AccessType") = "CT" Or dtrreaderNew("AccessType") = "RN" Then
        '        dtrreaderMapping = objAcessdataMapping.funcGetSQLDataReader(strqueryMapping.ToString)

        '        Do While dtrreaderMapping.Read
        '            UnitID = dtrreaderMapping("referencedUnitID")
        '        Loop

        '        dtrreaderMapping.Close()
        '    End If
        '    objAcessdataMapping.Dispose()
        '    'Changes

        '    If UnitID = "" Then
        '        UnitID = dtrreaderNew("UnitID")
        '    End If

        '    If dtrreaderNew("AccessType") = "SU" Or dtrreaderNew("AccessType") = "RN" Then
        '        strquery.Append(" and c.UnitID = '" & UnitID & "' ")
        '    Else
        '        strquery.Append(" and c.UnitCityID = '" & UnitID & "' ")
        '    End If
        'Loop


        'strquery.Append("select distinct VCM.carvendorid,a.cityname,VCM.carvendorname, case VCM.active when '1' then 'Active' when '0' then 'Not Active' end as active from CORIntCarVendorMaster VCM, CORIntCityMaster a , CORIntUnitCityMaster as b, CORIntUnitMaster as c where a.cityid=VCM.carvendorcityid and a.nearestUnitCityId=b.UnitCityId and a.Active = 1 and a.CityName <> '' and c.UnitCityID = b.UnitCityID ")
        If Request.QueryString("id") <> "-1" Then
            strquery.Append(" and VCM.carvendorid=" & Request.QueryString("id") & " ")
        End If

        'Response.Write(strquery)
        'Response.End()

        'dtrreaderNew.Close()
        'objAcessdataNw.Dispose()


        strquery.Append(" order by " & strorderby & "")

        dtrreader = accessdata.funcGetSQLDataReader(strquery.ToString)
        While dtrreader.Read
            Dim Temprow As New TableRow
            Temprow.HorizontalAlign = HorizontalAlign.Center
            Dim Tempcell As New TableCell
            Tempcell.Controls.Add(New LiteralControl(dtrreader("carvendorname") & ""))
            Temprow.Cells.Add(Tempcell)

            Dim Tempcel2 As New TableCell
            Tempcel2.Controls.Add(New LiteralControl(dtrreader("cityname") & ""))
            Temprow.Cells.Add(Tempcel2)

            Dim Tempcel3 As New TableCell
            Tempcel3.Controls.Add(New LiteralControl(dtrreader("active") & ""))
            Temprow.Cells.Add(Tempcel3)

            Dim Tempcel4 As New TableCell
            Tempcel4.Controls.Add(New LiteralControl("<a href=CarVendorEditForm.aspx?ID=" & dtrreader("carvendorid") & " >Edit</a>"))
            Temprow.Cells.Add(Tempcel4)
            'adding the Tables rows to the table
            tblRecDetail.Rows.Add(Temprow)

        End While
        dtrreader.Close()
        accessdata.Dispose()


    End Sub
    Sub SortGird(ByVal sender As Object, ByVal e As System.EventArgs)
        Select Case sender.id
            Case "Linkbutton1"
                getvalue("VCM.carvendorname")
            Case "Linkbutton2"
                getvalue("a.cityname")
            Case "Linkbutton3"
                getvalue("VCM.active")
        End Select

    End Sub

End Class
