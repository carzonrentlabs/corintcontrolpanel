Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Partial Class CarVendor_CCCharges
    Inherits System.Web.UI.Page

    Protected Sub ddlvendorname_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlvendorname.SelectedIndexChanged
        Dim accessdata As clsutility
        accessdata = New clsutility
        Dim dtrreader As SqlDataReader
        dtrreader = accessdata.funcGetSQLDataReader("select CCChargeYN from CORIntCarVendorMaster where CarVendorID=" + Convert.ToString(ddlvendorname.SelectedValue) + "")
        dtrreader.Read()
        If IsDBNull(dtrreader("CCChargeYN")) Then
            chkCCCharges.Checked = False
        ElseIf Convert.ToInt32(dtrreader("CCChargeYN")) = 0 Then
            chkCCCharges.Checked = False
        Else
            chkCCCharges.Checked = True
        End If
      
       
        'ddlvendorname.Items.Insert(0, New ListItem("Any", -1))
        'dtrreader.Close()
        accessdata.Dispose()

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim accessdata As clsutility
            accessdata = New clsutility
            Dim dtrreader As SqlDataReader
            dtrreader = accessdata.funcGetSQLDataReader("select CarVendorID  ,CarVendorName  from CORIntCarVendorMaster order by CarVendorName")
            ddlvendorname.DataSource = dtrreader
            ddlvendorname.DataValueField = "CarVendorID"
            ddlvendorname.DataTextField = "CarVendorName"
            ddlvendorname.DataBind()
            ddlvendorname.Items.Insert(0, New ListItem("Any", -1))
            dtrreader.Close()
            accessdata.Dispose()
        End If
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        Dim intuniqcheck As SqlParameter
        Dim intuniqvalue As Int32
        MyConnection.Open()
        cmd = New SqlCommand("prc_CCharges", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@VendorID", Convert.ToInt32(ddlvendorname.SelectedValue.ToString()))
        cmd.Parameters.Add("@CCChargeYN", get_YNvalue(chkCCCharges))
        intuniqvalue = cmd.ExecuteNonQuery()
        If intuniqvalue <> 0 Then
            lblMessage.Text = "Record Updated"
            ddlvendorname.SelectedIndex = 0
            chkCCCharges.Checked = False
        Else

            lblMessage.Text = "Record not Updated"

        End If

        MyConnection.Close()

    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("CCCharges.aspx.aspx")
    End Sub
End Class
