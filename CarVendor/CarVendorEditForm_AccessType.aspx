<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" src="CarVendorEditForm.aspx.vb" Inherits="CarVendorEditForm"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CarzonRent :: Internal software control panel</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="JavaScript" src="../utilityfunction.js"></script>
		<script language="JavaScript" src="../JScripts/Datefunc.js"></script>
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
		<script language="javascript">
		function validation()
		{
				if (isNaN(document.forms[0].txtSTDCode.value))
				{
					alert("The STD code should be numeric only")
					return false
				}
					if(document.forms[0].txtemail.value!="")
				{
					
					var theStr=document.forms[0].txtemail.value;
					var atIndex = theStr.indexOf('@'); 
					var dotIndex = theStr.indexOf('.', atIndex); 
					theSub = theStr.substring(0, dotIndex+1) 

				if ((atIndex < 1)||(atIndex != theStr.lastIndexOf('@'))||(dotIndex < atIndex + 2)||(theStr.length <= theSub.length)) 
				{ 
					alert("Email ID is not a valid Email ID");
					return false;
				}
				} 	
	

			var strvalues
			strvalues=('txtvendorname,txtaddress,ddlcity,txtcontactfname,txtcontactlname, txtSTDCode,txtphone1,txtemail,ddlrating,txtservtax,txtContractStartDate,txtContractEndDate')
			return checkmandatory(strvalues);		
		}		
function dateReg(obj)
{
    if(obj.value!="")
    {
        // alert(obj.value);
        var reg = /((0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[0-1])-(201[0-9]))/
        if(reg.test(obj.value))
        {
            //alert('valid');
        }
        else
        {
            alert('notvalid');
            obj.value="";
        }
    }
}
		

		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<uc1:Headerctrl id="Headerctrl1" runat="server"></uc1:Headerctrl>
			<TABLE id="Table1" align="center">
				<asp:panel id="pnlmainform" Runat="server">
					<TBODY>
						<TR>
							<TD align="center" colSpan="2"><STRONG><U>Edit a Car Vendor</U></STRONG>
							</TD>
						</TR>
						<TR>
							<TD align="center" colSpan="2">
								<asp:Label id="lblErrorMsg" runat="server" visible="false" cssclass="subRedHead"></asp:Label></TD>
						</TR>
						<TR>
							<TD align="center" colSpan="2">&nbsp;&nbsp;</TD>
						</TR>
						<TR>
							<TD>* Vendor Name
							</TD>
							<TD>
								<asp:textbox id="txtvendorname" runat="server" CssClass="input" MaxLength="100"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>* Address</TD>
							<TD>
								<asp:textbox id="txtaddress" runat="server" CssClass="input" MaxLength="250"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>* City
							</TD>
							<TD>
								<asp:DropDownList id="ddlcity" runat="server" CssClass="input"></asp:DropDownList></TD>
						</TR>
						<TR>
							<TD>* Contact - First Name
							</TD>
							<TD>
								<asp:textbox id="txtcontactfname" runat="server" CssClass="input" MaxLength="50"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>Contact - Middle Name
							</TD>
							<TD>
								<asp:textbox id="txtcontacmname" runat="server" CssClass="input" MaxLength="50"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>* Contact - Last Name</TD>
							<TD>
								<asp:textbox id="txtcontactlname" runat="server" CssClass="input" MaxLength="50"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>* STD Code
							</TD>
							<TD>
								<asp:textbox id="txtSTDCode" runat="server" CssClass="input" MaxLength="10" size="10"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>* Phone 1
							</TD>
							<TD>
								<asp:textbox id="txtphone1" runat="server" CssClass="input" MaxLength="20"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>Phone 2</TD>
							<TD>
								<asp:textbox id="txtphone2" runat="server" CssClass="input" MaxLength="20"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>Phone 3
							</TD>
							<TD>
								<asp:textbox id="txtphone3" runat="server" CssClass="input" MaxLength="20"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>Fax
							</TD>
							<TD>
								<asp:textbox id="txtfax" runat="server" CssClass="input" MaxLength="20"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>* Email ID
							</TD>
							<TD>
								<asp:textbox id="txtemail" runat="server" CssClass="input" MaxLength="100"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>* Preference Rating (1 is hightest)
							</TD>
							<TD>
								<asp:DropDownList id="ddlrating" runat="server" CssClass="input"></asp:DropDownList></TD>
						</TR>
						<TR>
							<TD>*Service Tax #
							</TD>
							<TD>
								<asp:textbox id="txtservtax" runat="server" CssClass="input" MaxLength="20"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>VAT Registration #
							</TD>
							<TD>
								<asp:textbox id="txtvat" runat="server" CssClass="input" MaxLength="20"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>PAN Card #
							</TD>
							<TD>
								<asp:textbox id="txtPAN" runat="server" CssClass="input" MaxLength="20"></asp:textbox></TD>
						</TR>
						<TR>
							<TD style="HEIGHT: 25px" vAlign="top">Contract Start Date</TD>
							<TD style="HEIGHT: 18px">
								<asp:textbox id="txtContractStartDate" runat="server" CssClass="input" MaxLength="12" size="12" onblur = "dateReg(this);">
								</asp:textbox><A onmouseover="window.status='Date Picker';return true;" onmouseout="window.status='';return true;"
									href="javascript:show_calendar('Form1.txtContractStartDate');"><IMG height="21" src="../images/show-calendar.gif" width="24" border="0"></A>
                             <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationExpression="((0[1-9]|1[012])/(0[1-9]|1[0-9]|2[0-9]|3[0-1])/(201[0-9]))" ControlToValidate="txtContractStartDate" runat="server" ErrorMessage="RegularExpressionValidator"></asp:RegularExpressionValidator>--%>

									</TD>
									
									
						</TR>
						<TR>
							<TD style="HEIGHT: 25px" vAlign="top">Contract End Date</TD>
							<TD style="HEIGHT: 18px">
								<asp:textbox id="txtContractEndDate" runat="server" CssClass="input" MaxLength="12" size="12" onblur = "dateReg(this);">
								</asp:textbox><A onmouseover="window.status='Date Picker';return true;" onmouseout="window.status='';return true;"
									href="javascript:show_calendar('Form1.txtContractEndDate');"><IMG height="21" src="../images/show-calendar.gif" width="24" border="0"></A>
                             <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator2" ValidationExpression="((0[1-9]|1[012])/(0[1-3]|[0-9])/[1-2][0-9][0-9][0-9])" ControlToValidate="txtContractEndDate" runat="server" ErrorMessage="RegularExpressionValidator"></asp:RegularExpressionValidator>--%>
									</TD>
						</TR>
						<TR>
							<TD vAlign="top">Remarks(<SPAN class="shwText" id="shwMessage">0</SPAN>/2000 chars)</TD>
							<TD>
								<asp:textbox id="txtRemarks" onkeydown="showLength(this.form.txtRemarks,'document.all.shwMessage',2000)"
									onkeyup="showLength(this.form.txtRemarks,'document.all.shwMessage',2000)" runat="server"
									CssClass="input" MaxLength="2000" Rows="3" Columns="50" TextMode="MultiLine"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>Active</TD>
							<TD>
								<asp:checkbox id="chkactive" runat="server" CssClass="input" Checked="True"></asp:checkbox></TD>
						</TR>
						<TR>
							<TD>BookedYN</TD>
							<TD>
								<asp:checkbox id="chkbooked" runat="server" CssClass="input" Checked="True"></asp:checkbox></TD>
						</TR>
						<TR>
							<TD>File Name
							</TD>
							<TD>
								<asp:textbox id="txtFileName" runat="server" Enabled="false" CssClass="input" MaxLength="20"></asp:textbox></TD>
						</TR>
						<TR>
							<TD align="center" colSpan="2">
								<asp:button id="btnsubmit" runat="server" CssClass="button" Text="Submit"></asp:button>&nbsp;&nbsp;
								<asp:button id="btnreset" runat="server" CssClass="button" Text="Reset" CausesValidation="False"></asp:button>
								<%--<input type="file" runat="server" disabled="disabled" name="FileUpload1" id="FileUpload1"/>--%>
								 <asp:FileUpload ID="FileUpload1" runat="server" />
                                <asp:HyperLink ID="HyperLink1" Text="abc" runat="server"></asp:HyperLink>
							</TD>
						</TR>
						</TBODY>
				</asp:panel><asp:panel id="pnlconfirmation" Runat="server" Visible="False">
					<TR align="center">
						<TD colSpan="2">
							<asp:Label id="lblMessage" runat="server"></asp:Label></TD>
					</TR>
					<TR align="center">
						<TD colSpan="2">
							<asp:HyperLink id="hyplnkretry" runat="server"></asp:HyperLink></TD>
					</TR>
				</asp:panel></TABLE>
		</form>
	</body>
</HTML>
