<%@ Page Language="vb" AutoEventWireup="false" src="CarVendorAddForm.aspx.vb" Inherits="CarVendorAddForm"%>
<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
		<title>CarzonRent :: Internal software control panel</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1"/>
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1"/>
		<meta name="vs_defaultClientScript" content="JavaScript"/>
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5"/>
		<script type="text/javascript" language="JavaScript" src="../utilityfunction.js"></script>
		<script type="text/javascript" language="JavaScript" src="../JScripts/Datefunc.js"></script>
		<link href="../HertzInt.css" type="text/css" rel="stylesheet"/>
		<script type="text/javascript" language="javascript">
		function validation()
		{
				if (isNaN(document.forms[0].txtSTDCode.value))
				{
					alert("The STD code should be numeric only")
					return false
				}
					if(document.forms[0].txtemail.value!="")
				{
					
					var theStr=document.forms[0].txtemail.value;
					var atIndex = theStr.indexOf('@'); 
					var dotIndex = theStr.indexOf('.', atIndex); 
					theSub = theStr.substring(0, dotIndex+1) 

				if ((atIndex < 1)||(atIndex != theStr.lastIndexOf('@'))||(dotIndex < atIndex + 2)||(theStr.length <= theSub.length)) 
				{ 
					alert("Email ID is not a valid Email ID");
					return false;
				}
				} 	
	

			var strvalues
			strvalues=('txtvendorname,txtaddress,ddlcity,txtcontactfname,txtcontactlname, txtSTDCode,txtphone1,txtemail,ddlrating,txtservtax,txtContractStartDate,txtContractEndDate,FileUpload1')
			return checkmandatory(strvalues);		
		}		
		function dateReg(obj)
        {
            if(obj.value!="")
            {
                // alert(obj.value);
                var reg = /((0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[0-1])-(201[0-9]))/
                if(reg.test(obj.value))
                {
                    //alert('valid');
                }
                else
                {
                    alert('notvalid');
                    obj.value="";
                }
            }
        }

		</script>
	</head>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server" enctype="multipart/form-data">
			<uc1:Headerctrl id="Headerctrl1" runat="server"></uc1:Headerctrl>
			<table id="Table1" align="center">
			<tbody>
				<asp:panel id="pnlmainform" Runat="server">
						<tr>
							<td align="center" colspan="2"><strong><u>Add a Car Vendor</u></strong>
							</td>
						</tr>
						<tr><td colspan="2" align="center"><asp:Label id="lblErrorMsg" cssclass="subRedHead" visible="false" runat="server"></asp:Label></td></tr>
						<tr>
							<td align="center" colspan="2">&nbsp;&nbsp;</td>
						</tr>
						<tr>
							<td>* Vendor Name
							</td>
							<td>
								<asp:textbox id="txtvendorname" runat="server" MaxLength="100" CssClass="input"></asp:textbox></td>
						</tr>
						<tr>
							<td>* Address</td>
							<td>
								<asp:textbox id="txtaddress" runat="server" MaxLength="250" CssClass="input"></asp:textbox></td>
						</tr>
						<tr>
							<td>* City
							</td>
							<td>
								<asp:DropDownList id="ddlcity" runat="server" CssClass="input"></asp:DropDownList></td>
						</tr>
						<tr>
							<td>* Contact - First Name
							</td>
							<td>
								<asp:textbox id="txtcontactfname" runat="server" MaxLength="50" CssClass="input"></asp:textbox></td>
						</tr>
						<tr>
							<td>Contact - Middle Name
							</td>
							<td>
								<asp:textbox id="txtcontacmname" runat="server" MaxLength="50" CssClass="input"></asp:textbox></td>
						</tr>
						<tr>
							<td>* Contact - Last Name</td>
							<td>
								<asp:textbox id="txtcontactlname" runat="server" MaxLength="50" CssClass="input"></asp:textbox></td>
						</tr>
						<tr>
							<td>* STD Code
							</td>
							<td>
								<asp:textbox id="txtSTDCode" size="10" runat="server" MaxLength="10" CssClass="input"></asp:textbox></td>
						</tr>
						<tr>
							<td>* Phone 1
							</td>
							<td>
								<asp:textbox id="txtphone1" runat="server" MaxLength="20" CssClass="input"></asp:textbox></td>
						</tr>
						<tr>
							<td>Phone 2</td>
							<td>
								<asp:textbox id="txtphone2" runat="server" MaxLength="20" CssClass="input"></asp:textbox></td>
						</tr>
						<tr>
							<td>Phone 3
							</td>
							<td>
								<asp:textbox id="txtphone3" runat="server" MaxLength="20" CssClass="input"></asp:textbox></td>
						</tr>
						<tr>
							<td>Fax
							</td>
							<td>
								<asp:textbox id="txtfax" runat="server" MaxLength="20" CssClass="input"></asp:textbox></td>
						</tr>
						<tr>
							<td>* Email ID
							</td>
							<td>
								<asp:textbox id="txtemail" runat="server" MaxLength="100" CssClass="input"></asp:textbox></td>
						</tr>
						<tr>
							<td>* Preference Rating (1 is hightest)
							</td>
							<td>
								<asp:DropDownList id="ddlrating" runat="server" CssClass="input"></asp:DropDownList></td>
						</tr>
						<tr>
							<td>*Service Tax #
							</td>
							<td>
								<asp:textbox id="txtservtax" runat="server" MaxLength="20" CssClass="input"></asp:textbox></td>
						</tr>
						<tr>
							<td>VAT Registration #
							</td>
							<td>
								<asp:textbox id="txtvat" runat="server" MaxLength="20" CssClass="input"></asp:textbox></td>
						</tr>
						<tr>
							<td>PAN Card #
							</td>
							<td>
								<asp:textbox id="txtPAN" runat="server" MaxLength="20" CssClass="input"></asp:textbox></td>
						</tr>
						<tr>
							<td style="HEIGHT: 25px" valign="top">Contract Start Date</td>
							<td style="HEIGHT: 18px">
								<asp:textbox id="txtContractStartDate" runat="server" CssClass="input" MaxLength="12" size="12"
									onblur = "dateReg(this);"></asp:textbox><a onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;"
									href="javascript:show_calendar('Form1.txtContractStartDate');"><img height="21" src="../images/show-calendar.gif" width="24" border="0"></a></td>
						</tr>
						<tr>
							<td style="HEIGHT: 25px" valign="top">Contract End Date</td>
							<td style="HEIGHT: 18px">
								<asp:textbox id="txtContractEndDate" runat="server" CssClass="input" MaxLength="12" size="12"
									onblur = "dateReg(this);"></asp:textbox><a onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;"
									href="javascript:show_calendar('Form1.txtContractEndDate');"><img height="21" src="../images/show-calendar.gif" width="24" border="0"></a></td>
						</tr>						
						<tr>
							<td valign="top">Remarks(<span class="shwText" id="shwMessage">0</SPAN>/2000 chars)</td>
							<td>
								<asp:textbox id="txtRemarks" onkeydown="showLength(this.form.txtRemarks,'document.all.shwMessage',2000)"
									onkeyup="showLength(this.form.txtRemarks,'document.all.shwMessage',2000)" runat="server"
									MaxLength="2000" CssClass="input" TextMode="MultiLine" Columns="50" Rows="3"></asp:textbox></td>
						</tr>
						<tr>
							<td>Active</td>
							<td>
								<asp:checkbox id="chkactive" runat="server" CssClass="input" Checked="True"></asp:checkbox></td>
						</tr>
						<TR>
							<TD>Package Sharing on Car Booked</TD>
							<TD>
								<asp:checkbox id="chkbooked" runat="server" CssClass="input" Checked="True"></asp:checkbox></TD>
						</TR>
						<tr>
							<td align="center" colspan="3">
								<asp:button id="btnsubmit" runat="server" CssClass="button" Text="Submit"></asp:button>&nbsp;&nbsp;
								
<input type="Reset" CssClass="button" name="Reset" value="Reset" /> &nbsp;&nbsp;
<%--<asp:button id="btnupload" runat="server" CssClass="button" Text="Upload"></asp:button>--%>
<%--<input type="file" runat="server" name="FileUpload1" id="FileUpload1"/>--%>
 <asp:FileUpload ID="FileUpload1" runat="server" />
</td>
						</tr>
				</asp:panel>
				
				<asp:panel id="pnlconfirmation" Runat="server" Visible="False">
					<tr align="center">
						<td colspan="2">
							<asp:Label id="lblMessage" runat="server"></asp:Label></td>
					</tr>
					<tr align="center">
						<td colspan="2">
							<asp:HyperLink id="hyplnkretry" runat="server"></asp:HyperLink></td>
					</tr>
				</asp:panel>
				</tbody></table>
		</form>
	</body>
</html>
