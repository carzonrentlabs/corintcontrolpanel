Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports commonutility
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.IO
Imports System.IO.FileStream
Imports System.IO.File
Imports System.Net
Imports System.Text
Imports System.Object
Imports System.MarshalByRefObject
Imports System.Net.WebRequest


Public Class CarVendorEditForm
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtvendorname As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtaddress As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlcity As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtcontactfname As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtcontacmname As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtcontactlname As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtSTDCode As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtphone1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtphone2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtphone3 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtfax As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtemail As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlrating As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtservtax As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtvat As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtPAN As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtRemarks As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkactive As System.Web.UI.WebControls.CheckBox
    Protected WithEvents btnsubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnreset As System.Web.UI.WebControls.Button
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label   
    Protected WithEvents txtContractStartDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtContractEndDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtFileName As System.Web.UI.WebControls.TextBox
    'Protected WithEvents FileUpload1 As System.Web.UI.HtmlControls.HtmlInputFile
    Protected WithEvents FileUpload1 As System.Web.UI.WebControls.FileUpload
    Protected WithEvents chkbooked As System.Web.UI.WebControls.CheckBox
    Protected WithEvents HyperLink1 As System.Web.UI.WebControls.HyperLink

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        'btnsubmit.Attributes("onclick") = "return validation();"
        If Not Page.IsPostBack Then
            populateddl()
            Dim dtrreader As SqlDataReader
            Dim accessdata As clsutility
            accessdata = New clsutility
            dtrreader = accessdata.funcGetSQLDataReader("select * from CORIntCarVendorMaster where carvendorid=" & Request.QueryString("id") & " ")
            dtrreader.Read()

            autoselec_ddl(ddlcity, dtrreader("carvendorcityid"))
            autoselec_ddl(ddlrating, dtrreader("preferencerating"))
            txtvendorname.Text = dtrreader("carvendorname") & ""
            txtaddress.Text = dtrreader("carvendoraddress") & ""
            txtcontactfname.Text = dtrreader("contactfname") & ""
            txtcontacmname.Text = dtrreader("contactmname") & ""
            txtcontactlname.Text = dtrreader("contactlname") & ""

            txtSTDCode.Text = dtrreader("STDCode") & ""
            txtphone1.Text = dtrreader("contactph1") & ""
            txtphone2.Text = dtrreader("contactph2") & ""
            txtphone3.Text = dtrreader("contactph3") & ""
            txtfax.Text = dtrreader("fax") & ""
            txtemail.Text = dtrreader("emailid") & ""
            txtservtax.Text = dtrreader("servicetaxno") & ""
            txtvat.Text = dtrreader("vatregnno") & ""
            txtPAN.Text = dtrreader("PANNo") & ""

            txtContractStartDate.Text = dtrreader("ContractStartDate") & ""
            'Response.Write("You are here.")
            'Response.End()
            txtContractEndDate.Text = dtrreader("ContractEndDate") & ""

            txtRemarks.Text = dtrreader("remarks") & ""
            chkactive.Checked = dtrreader("active")
            chkbooked.Checked = dtrreader("BookedYN")

            If (dtrreader("FileName") = "") Then
                txtFileName.Text = "No Image"
                'FileUpload1.Enabled = True
                FileUpload1.Visible = True
                HyperLink1.Text = ""
                'FileUpload1.Disabled = False
            Else
                txtFileName.Text = dtrreader("FileName")
                'FileUpload1.Disabled = True
                'FileUpload1.Enabled = False
                FileUpload1.Visible = False
                HyperLink1.Visible = True
                HyperLink1.Text = txtFileName.Text
                HyperLink1.NavigateUrl = "~/UploadImage/" + txtFileName.Text
                HyperLink1.Target = "_blank"

                'FileUpload1.SaveAs(Server.MapPath("~/UploadImage/") + filename)

            End If
            dtrreader.Close()
            accessdata.Dispose()
        End If

    End Sub

    Sub populateddl()
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility

        Dim objAcessdataNw As clsutility
        objAcessdataNw = New clsutility
        Dim strquery As String
        Dim dtrreader As SqlDataReader
        Dim strquerycity As String
        'Dim AccessType As String
        Dim objAcessdataMapping As clsutility 'Changes
        objAcessdataMapping = New clsutility 'Changes
        Dim strqueryMapping As String 'Changes
        Dim dtrreaderMapping As SqlDataReader 'Changes
        Dim UnitID As String 'Changes

        strquery = New String("SELECT a.AccessType, a.UnitID, b.UnitCityID, c.Region FROM CORIntSysUsersMaster as a, CORIntUnitMaster as b, CORIntUnitCityMaster as c WHERE a.UnitID = b.UnitID and b.UnitCityID = c.UnitCityID and a.SysUserID =" & Session("loggedin_user"))
        dtrreader = objAcessdataNw.funcGetSQLDataReader(strquery.ToString)

        Do While dtrreader.Read

            'Changes
            If dtrreader("AccessType") = "SU" Or dtrreader("AccessType") = "RN" Then
                strqueryMapping = New String("select UM.UnitID, UM.referencedUnitID from corintunitmapping as UM, CORIntUnitMaster as b, CORIntUnitCityMaster as c WHERE UM.referencedUnitID = b.UnitID and b.UnitCityID = c.UnitCityID and UM.UnitID = " & dtrreader("UnitID"))
            ElseIf dtrreader("AccessType") = "CT" Then
                strqueryMapping = New String("select UM.UnitID, c.UnitCityID as referencedUnitID from corintunitmapping as UM, CORIntUnitMaster as b, CORIntUnitCityMaster as c WHERE UM.referencedUnitID = b.UnitID and b.UnitCityID = c.UnitCityID and UM.UnitID = " & dtrreader("UnitID"))
            Else 'If dtrreader("AccessType") = "RN" Then
                UnitID = dtrreader("UnitID")
            End If

            If dtrreader("AccessType") = "SU" Or dtrreader("AccessType") = "CT" Or dtrreader("AccessType") = "RN" Then
                dtrreaderMapping = objAcessdataMapping.funcGetSQLDataReader(strqueryMapping.ToString)

                Do While dtrreaderMapping.Read
                    UnitID = dtrreaderMapping("referencedUnitID")
                Loop

                dtrreaderMapping.Close()
            End If
            objAcessdataMapping.Dispose()
            'Changes

            If UnitID = "" Then
                UnitID = dtrreader("UnitID")
            End If

            If dtrreader("AccessType") = "SU" Or dtrreader("AccessType") = "RN" Then
                strquerycity = New String("SELECT distinct a.CityName, a.CityID, b.Region FROM CORIntCityMaster as a, CORIntUnitCityMaster as b, CORIntUnitMaster as c WHERE a.nearestUnitCityId=b.UnitCityId and a.CityName <> '' and c.UnitCityID = b.UnitCityID and c.UnitID = '" & UnitID & "' ")
            Else 'If dtrreader("AccessType") = "CT" Then
                strquerycity = New String("SELECT distinct a.CityName, a.CityID, b.Region FROM CORIntCityMaster as a, CORIntUnitCityMaster as b, CORIntUnitMaster as c WHERE a.nearestUnitCityId=b.UnitCityId and a.CityName <> '' and c.UnitCityID = b.UnitCityID and c.UnitCityID = '" & UnitID & "' ")
                'ElseIf dtrreader("AccessType") = "RN" Then
                'strquerycity = New String("SELECT distinct a.CityName, a.CityID, b.Region FROM CORIntCityMaster as a, CORIntUnitCityMaster as b, CORIntUnitMaster as c WHERE a.nearestUnitCityId=b.UnitCityId and a.Active = 1 and a.CityName <> '' and c.UnitCityID = b.UnitCityID and b.Region = '" & UnitID & "' ")
                'Else
                'strquerycity = New String("SELECT distinct a.CityName, a.CityID, b.Region FROM CORIntCityMaster as a, CORIntUnitCityMaster as b, CORIntUnitMaster as c WHERE a.nearestUnitCityId=b.UnitCityId and a.Active = 1 and a.CityName <> '' and c.UnitCityID = b.UnitCityID ")
            End If


        Loop

        'Response.Write(dtrreader("AccessType"))
        'Response.Write("query : " & strquerycity)
        'Response.End()
        dtrreader.Close()
        objAcessdataNw.Dispose()

        ddlcity.DataSource = objAcessdata.funcGetSQLDataReader(strquerycity)
        ddlcity.DataValueField = "cityid"
        ddlcity.DataTextField = "cityname"
        ddlcity.DataBind()
        ddlcity.Items.Insert(0, New ListItem("", ""))

        objAcessdata.funcpopulatenumddw(10, 1, ddlrating)


        objAcessdata.Dispose()

    End Sub
    Function autoselec_ddl(ByVal ddlname As DropDownList, ByVal selectvalue As Int32)
        If Not selectvalue = "0" Then
            ddlname.Items.FindByValue(selectvalue).Selected = True
        End If
    End Function

    Private Sub btnreset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnreset.Click
        Response.Redirect("CarVendorEditForm.aspx?id=" & Request.QueryString("id"))
    End Sub

    Private Sub btnsubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsubmit.Click


        '**********************************************************************
        Dim str As String
        'Response.Write(FileUpload1.PostedFile)
        'Response.End()
        If txtFileName.Text = "No Image" Then
            Dim filename As String
            'str = FileUpload1.PostedFile.FileName
            filename = Path.GetFileName(FileUpload1.FileName)
            'Response.Write(filename)
            'Response.End()
            str = filename
            'Response.Write(str)
            'Response.End()



            If FileUpload1.PostedFile.FileName <> "" And FileUpload1.PostedFile.ContentLength > 0 Then

                FileUpload1.SaveAs(Server.MapPath("~/UploadImage/") + filename)
                If str <> "" Then


                    Dim MyConnection As SqlConnection
                    MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
                    Dim cmd As SqlCommand
                    Dim intuniqcheck As SqlParameter
                    Dim intuniqvalue As Int32
                    cmd = New SqlCommand("procEditCarVendorMaster", MyConnection)
                    cmd.CommandType = CommandType.StoredProcedure

                    cmd.Parameters.Add("@rowid", Request.QueryString("id"))

                    cmd.Parameters.Add("@carvendorname", txtvendorname.Text)
                    cmd.Parameters.Add("@carvendoraddress", txtaddress.Text)
                    cmd.Parameters.Add("@carvendorcityid", ddlcity.SelectedItem.Value)
                    cmd.Parameters.Add("@contactfname", txtcontactfname.Text)
                    cmd.Parameters.Add("@contactmname", txtcontacmname.Text)
                    cmd.Parameters.Add("@contactlname", txtcontactlname.Text)
                    cmd.Parameters.Add("@STDCode", txtSTDCode.Text)
                    cmd.Parameters.Add("@contactph1", txtphone1.Text)
                    cmd.Parameters.Add("@contactph2", txtphone2.Text)
                    cmd.Parameters.Add("@contactph3", txtphone3.Text)
                    cmd.Parameters.Add("@fax", txtfax.Text)
                    cmd.Parameters.Add("@emailid", txtemail.Text)
                    cmd.Parameters.Add("@preferencerating", ddlrating.SelectedItem.Value)
                    cmd.Parameters.Add("@servicetaxno", txtservtax.Text)
                    cmd.Parameters.Add("@vatregnno", txtvat.Text)
                    cmd.Parameters.Add("@PAN", txtPAN.Text)
                    cmd.Parameters.Add("@ContractStartDate", txtContractStartDate.Text)
                    cmd.Parameters.Add("@ContractEndDate", txtContractEndDate.Text)
                    cmd.Parameters.Add("@remarks", txtRemarks.Text)
                    cmd.Parameters.Add("@active", get_YNvalue(chkactive))
                    cmd.Parameters.Add("@bookedYN", get_YNvalue(chkbooked))
                    cmd.Parameters.Add("@modifiedby", Session("loggedin_user"))
                    cmd.Parameters.Add("@FileName", str)
                    intuniqcheck = cmd.Parameters.Add("@uniqcheckval", SqlDbType.Int)
                    intuniqcheck.Direction = ParameterDirection.Output

                    '  Try
                    MyConnection.Open()
                    cmd.ExecuteNonQuery()
                    intuniqvalue = cmd.Parameters("@uniqcheckval").Value
                    MyConnection.Close()
                    'If Not intuniqvalue = 0 Then
                    If intuniqvalue = 1 Then
                        lblErrorMsg.Visible = True
                        lblErrorMsg.Text = "Car Vendor already exist."
                        Exit Sub
                    ElseIf intuniqvalue = 0 Then
                        lblErrorMsg.Visible = False
                        pnlmainform.Visible = False
                        pnlconfirmation.Visible = True

                        lblMessage.Text = "You have updated the Car Vendor successfully"
                        hyplnkretry.Text = "Edit another Car Vendor"
                        hyplnkretry.NavigateUrl = "CarVendorEditSearch.aspx"
                    ElseIf intuniqvalue = 2 Then
                        lblErrorMsg.Visible = True
                        lblErrorMsg.Text = "Image " & str & " already Uploaded."
                        Exit Sub
                    Else
                        lblErrorMsg.Visible = True
                        lblErrorMsg.Text = "Car Vendor already exist."
                        Exit Sub
                    End If
                    '   Catch
                    '   End Try

                    '*************************************************************************************************
                End If
            Else
                lblErrorMsg.Visible = True
                lblErrorMsg.Text = "Please select Image to Upload."
                Exit Sub
            End If
        Else
            str = txtFileName.Text
            Dim MyConnection As SqlConnection
            MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
            Dim cmd As SqlCommand
            Dim intuniqcheck As SqlParameter
            Dim intuniqvalue As Int32
            'Response.Write(txtContractStartDate.Text)
            'Response.Write("<br>")
            'Response.Write(txtContractEndDate.Text)
            'Response.End()
            cmd = New SqlCommand("procEditCarVendorMaster", MyConnection)
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.Add("@rowid", Request.QueryString("id"))

            cmd.Parameters.Add("@carvendorname", txtvendorname.Text)
            cmd.Parameters.Add("@carvendoraddress", txtaddress.Text)
            cmd.Parameters.Add("@carvendorcityid", ddlcity.SelectedItem.Value)
            cmd.Parameters.Add("@contactfname", txtcontactfname.Text)
            cmd.Parameters.Add("@contactmname", txtcontacmname.Text)
            cmd.Parameters.Add("@contactlname", txtcontactlname.Text)
            cmd.Parameters.Add("@STDCode", txtSTDCode.Text)
            cmd.Parameters.Add("@contactph1", txtphone1.Text)
            cmd.Parameters.Add("@contactph2", txtphone2.Text)
            cmd.Parameters.Add("@contactph3", txtphone3.Text)
            cmd.Parameters.Add("@fax", txtfax.Text)
            cmd.Parameters.Add("@emailid", txtemail.Text)
            cmd.Parameters.Add("@preferencerating", ddlrating.SelectedItem.Value)
            cmd.Parameters.Add("@servicetaxno", txtservtax.Text)
            cmd.Parameters.Add("@vatregnno", txtvat.Text)
            cmd.Parameters.Add("@PAN", txtPAN.Text)
            cmd.Parameters.Add("@ContractStartDate", txtContractStartDate.Text)
            cmd.Parameters.Add("@ContractEndDate", txtContractEndDate.Text)
            cmd.Parameters.Add("@remarks", txtRemarks.Text)
            cmd.Parameters.Add("@active", get_YNvalue(chkactive))
            cmd.Parameters.Add("@bookedYN", get_YNvalue(chkbooked))
            cmd.Parameters.Add("@modifiedby", Session("loggedin_user"))
            cmd.Parameters.Add("@FileName", str)
            intuniqcheck = cmd.Parameters.Add("@uniqcheckval", SqlDbType.Int)
            intuniqcheck.Direction = ParameterDirection.Output

            '  Try
            MyConnection.Open()
            cmd.ExecuteNonQuery()
            intuniqvalue = cmd.Parameters("@uniqcheckval").Value
            MyConnection.Close()
            'If Not intuniqvalue = 0 Then
            If intuniqvalue = 1 Then
                lblErrorMsg.Visible = True
                lblErrorMsg.Text = "Car Vendor already exist."
                Exit Sub
            ElseIf intuniqvalue = 0 Then
                lblErrorMsg.Visible = False
                pnlmainform.Visible = False
                pnlconfirmation.Visible = True

                lblMessage.Text = "You have updated the Car Vendor successfully"
                hyplnkretry.Text = "Edit another Car Vendor"
                hyplnkretry.NavigateUrl = "CarVendorEditSearch.aspx"
            ElseIf intuniqvalue = 2 Then
                lblErrorMsg.Visible = True
                lblErrorMsg.Text = "Image " & str & " already Uploaded."
                Exit Sub
            Else
                lblErrorMsg.Visible = True
                lblErrorMsg.Text = "Car Vendor already exist."
                Exit Sub
            End If
            '   Catch
            '   End Try


        End If
        '*************************************************************************************************
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function

End Class
