<%@ Page Language="vb" AutoEventWireup="false" Src="NonCarVendorEditSearch.aspx.vb" Inherits="NonCarVendorEditSearch"%>
<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CarzonRent :: Internal software control panel</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
				<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<uc1:Headerctrl id="Headerctrl1" runat="server"></uc1:Headerctrl>
						<TABLE align="center">
				<TR>
					<TD colspan="2" align="center"><STRONG><U> Edit a Vendor Chauffeur where</U></STRONG>
						<br>
						<br>
					</TD>
				</TR>
				<TR>
					<TD>Vendor Name</TD>
					<TD>
						<asp:DropDownList id="ddlvendorname" runat="server" CssClass="input"></asp:DropDownList></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD colspan="2" align="center">
						<asp:Button id="btnSubmit" runat="server" Text="Submit" CssClass="button"></asp:Button>
						<asp:Button id="btnReset" runat="server" Text="Reset" CssClass="button"></asp:Button></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
