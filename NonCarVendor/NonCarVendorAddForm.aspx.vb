Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class NonCarVendorAddForm
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtvendname As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtaddress As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlcity As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtcontfname As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtcontmname As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtlname As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtphone1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtphone2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtphne3 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtfax As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtemail As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlventype As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtRemarks As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkactive As System.Web.UI.WebControls.CheckBox
    Protected WithEvents btnsubmit As System.Web.UI.WebControls.Button
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btnsubmit.Attributes("onClick") = "return validation();"
        If Not Page.IsPostBack Then
            populateddl()
        End If
    End Sub
    Sub populateddl()
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility

 
        ddlcity.DataSource = objAcessdata.funcGetSQLDataReader("select cityname,cityid from CORIntCityMaster  where active=1 order by cityname")
        ddlcity.DataValueField = "cityid"
        ddlcity.DataTextField = "cityname"
        ddlcity.DataBind()
        ddlcity.Items.Insert(0, New ListItem("", ""))

        ddlventype.DataSource = objAcessdata.funcGetSQLDataReader("select NonCarVendorType ,NonCarVendorTypeID  from CORIntNonCarVendorTypeMaster   where active=1 order by NonCarVendorType")
        ddlventype.DataValueField = "NonCarVendorTypeID"
        ddlventype.DataTextField = "NonCarVendorType"
        ddlventype.DataBind()
        ddlventype.Items.Insert(0, New ListItem("", ""))


        objAcessdata.Dispose()

    End Sub

    Private Sub btnsubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsubmit.Click

        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand

        Dim intuniqcheck As SqlParameter
        Dim intuniqvalue as int32

        cmd = New SqlCommand("procAddNonCarVendorMaster", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("@noncarvendorname", txtvendname.Text)
        cmd.Parameters.Add("@noncarvendoraddress", txtaddress.Text)
        cmd.Parameters.Add("@noncarvendorcityid", ddlcity.SelectedItem.Value)
        cmd.Parameters.Add("@contactfname", txtcontfname.Text)
        cmd.Parameters.Add("@contactmname", txtcontmname.Text)
        cmd.Parameters.Add("@contactlname", txtlname.Text)
        cmd.Parameters.Add("@contactph1", txtphone1.Text)
        cmd.Parameters.Add("@contactph2", txtphone2.Text)
        cmd.Parameters.Add("@contactph3", txtphne3.Text)
        cmd.Parameters.Add("@fax", txtfax.Text)
        cmd.Parameters.Add("@emailid", txtemail.Text)
        cmd.Parameters.Add("@noncarvendortypeid", ddlventype.SelectedItem.Value)
        cmd.Parameters.Add("@remarks", txtRemarks.Text)
        cmd.Parameters.Add("@active", get_YNvalue(chkactive))
        cmd.Parameters.Add("@createdby", Session("loggedin_user"))
       intuniqcheck = cmd.Parameters.Add("@uniqcheckval", SqlDbType.Int)
       intuniqcheck.Direction = ParameterDirection.Output

        '  Try
        MyConnection.Open()
        cmd.ExecuteNonQuery()
           intuniqvalue=cmd.Parameters("@uniqcheckval").Value
        MyConnection.Close()
           if not intuniqvalue = 0 then
                lblErrorMsg.visible=true
                lblErrorMsg.text="Non-car vendor already exist."
                exit sub
            else
		        lblErrorMsg.visible=false
                pnlmainform.Visible = False
                pnlconfirmation.Visible = True

                lblMessage.Text = "You have added the Non-Car Vendor successfully"
                hyplnkretry.Text = "Add another Non-Car Vendor"
                hyplnkretry.NavigateUrl = "NonCarVendorAddForm.aspx"
            end if
        '   Catch
        '   End Try
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function

End Class
