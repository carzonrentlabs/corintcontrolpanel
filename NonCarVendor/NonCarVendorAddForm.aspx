<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Src="NonCarVendorAddForm.aspx.vb" Inherits="NonCarVendorAddForm"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CarzonRent :: Internal software control panel</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="JavaScript" src="../utilityfunction.js"></script>
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
				<script language="javascript">
		function validation()
		{
			
	
			if(document.forms[0].txtemail.value!="")
				{
					
					var theStr=document.forms[0].txtemail.value;
					var atIndex = theStr.indexOf('@'); 
					var dotIndex = theStr.indexOf('.', atIndex); 
					theSub = theStr.substring(0, dotIndex+1) 

				if ((atIndex < 1)||(atIndex != theStr.lastIndexOf('@'))||(dotIndex < atIndex + 2)||(theStr.length <= theSub.length)) 
				{ 
					alert("Email ID is not a valid Email ID");
					return false;
				}
				} 	
			
			
			var strvalues
			strvalues=('txtvendname,txtaddress,ddlcity,txtcontfname,txtlname,txtphone1,ddlventype')
			return checkmandatory(strvalues);		
		}		
		
		
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<uc1:Headerctrl id="Headerctrl1" runat="server"></uc1:Headerctrl>
			<TABLE id="Table1" align="center">
				<asp:panel id="pnlmainform" Runat="server">
					<TBODY>
						<TR>
							<TD align="center" colSpan="2"><STRONG><U>Add a Non-Car Vendor</U></STRONG>
							</TD>
						</TR>
						<tr><td colspan="2" align="center"><asp:Label id="lblErrorMsg" cssclass="subRedHead" visible="false" runat="server"></asp:Label></td></tr>
						<TR>
							<TD align="center" colSpan="2">&nbsp;&nbsp;</TD>
						</TR>
						<TR>
							<TD>* Vendor Name
							</TD>
							<TD>
								<asp:textbox id="txtvendname" runat="server" MaxLength="100" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>* Address</TD>
							<TD>
								<asp:textbox id="txtaddress" runat="server" MaxLength="250" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>* City
							</TD>
							<TD>
								<asp:DropDownList id="ddlcity" runat="server" CssClass="input"></asp:DropDownList></TD>
						</TR>
						<TR>
							<TD>* Contact - First Name</TD>
							<TD>
								<asp:textbox id="txtcontfname" runat="server" MaxLength="50" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>Contact - Middle Name</TD>
							<TD>
								<asp:textbox id="txtcontmname" runat="server" MaxLength="50" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>* Contact - Last Name</TD>
							<TD>
								<asp:textbox id="txtlname" runat="server" MaxLength="50" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>* Phone 1</TD>
							<TD>
								<asp:textbox id="txtphone1" runat="server" MaxLength="20" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>Phone 2</TD>
							<TD>
								<asp:textbox id="txtphone2" runat="server" MaxLength="20" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>Phone 3</TD>
							<TD>
								<asp:textbox id="txtphne3" runat="server" MaxLength="20" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>Fax</TD>
							<TD>
								<asp:textbox id="txtfax" runat="server" MaxLength="20" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>* Email ID</TD>
							<TD>
								<asp:textbox id="txtemail" runat="server" MaxLength="20" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>* Vendor Type</TD>
							<TD>
								<asp:DropDownList id="ddlventype" runat="server" CssClass="input"></asp:DropDownList></TD>
						</TR>
						<TR>
							<TD vAlign="top">Remarks(<SPAN class="shwText" id="shwMessage">0</SPAN>/2000 chars)</TD>
							<TD>
								<asp:textbox id="txtRemarks" onkeydown="showLength(this.form.txtRemarks,'document.all.shwMessage',2000)"
									onkeyup="showLength(this.form.txtRemarks,'document.all.shwMessage',2000)" runat="server"
									MaxLength="2000" CssClass="input" Rows="3" Columns="50" TextMode="MultiLine"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>Active</TD>
							<TD>
								<asp:checkbox id="chkactive" runat="server" CssClass="input" Checked="True"></asp:checkbox></TD>
						</TR>
						<TR>
							<TD align="center" colSpan="2">
								<asp:button id="btnsubmit" runat="server" CssClass="button" Text="Submit"></asp:button>&nbsp;&nbsp;
								<input type="Reset" CssClass="button" name="Reset" value="Reset" /></TD>
						</TR>
				</asp:panel><asp:panel id="pnlconfirmation" Runat="server" Visible="False">
					<TR align="center">
						<TD colSpan="2">
							<asp:Label id="lblMessage" runat="server"></asp:Label></TD>
					</TR>
					<TR align="center">
						<TD colSpan="2">
							<asp:HyperLink id="hyplnkretry" runat="server"></asp:HyperLink></TD>
					</TR>
				</asp:panel></TBODY></TABLE>
		</form>
	</body>
</HTML>
