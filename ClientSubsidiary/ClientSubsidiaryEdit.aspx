<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ClientSubsidiaryEdit.aspx.vb"
    Inherits="ClientSubsidiaryEdit" Src="ClientSubsidiaryEdit.aspx.vb" %>

<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>CarzonRent :: Internal software Control panel</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <link rel="stylesheet" href="/resources/demos/style.css">
    <link href="../HertzInt.css" type="text/css" rel="stylesheet">
    <script language="javascript">
        function validation() {
            if (document.forms[0].txtSubName.value == "") {
                alert("Please Enter Subsidiary Name..");
                document.forms[0].txtSubName.focus();
                return false;
            }
            if (document.forms[0].txtSubAdd.value == "") {
                alert("Please Enter Subsidiary Address..");
                document.forms[0].txtSubName.focus();
                return false;
            }
        }
        $(document).ready(function () {
            $("#<%=txtSEZExpiry.ClientID %>").datepicker();
        });
    </script>
</head>
<body ms_positioning="GridLayout">
    <blockquote>
        <form id="Form1" method="post" runat="server">
            <uc1:Headerctrl ID="Headerctrl1" runat="server"></uc1:Headerctrl>
            <table id="Table1" align="center">
                <tbody>
                    <asp:Panel ID="pnlmainform" runat="server">
                        <tr>
                            <td align="center" colspan="2">
                                <strong><u>Edit a Client Subsidiary</u></strong>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">&nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr style="display:none">
                            <td>ID
                            </td>
                            <td>
                                <asp:Label ID="ID1" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>Client Name
                            </td>
                            <td>
                                <asp:Label ID="lblclientName" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td> * Subsidiary Name
                            </td>
                            <td>
                                <asp:TextBox ID="txtSubName" runat="server" Width="300"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td> * Subsidiary Address
                            </td>
                            <td>
                                <asp:TextBox ID="txtSubAdd" runat="server" CssClass="input" Rows="10" Columns="50" TextMode="MultiLine"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>SEZ ClientYN
                            </td>
                            <td>
                                <asp:CheckBox ID="chkSEZYN" Enabled="false" runat="server" CssClass="input" Checked="false"></asp:CheckBox>
                            </td>
                        </tr>
                        <tr>
                            <td>SEZ Expiry date
                            </td>
                            <td>
                                <asp:TextBox ID="txtSEZExpiry" runat="server" CssClass="input"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>Do Not Apply Service TaxYN
                            </td>
                            <td>
                                <asp:CheckBox ID="chkDoNotApplyServiceTaxYN" Enabled="false" runat="server" CssClass="input" Checked="false"></asp:CheckBox>
                            </td>
                        </tr>
                        <tr>
                            <td>Active
                            </td>
                            <td>
                                <asp:CheckBox ID="chk_active" runat="server" Checked="True"></asp:CheckBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <asp:Button ID="btnsubmit" runat="server" Text="Submit"></asp:Button>&nbsp;&nbsp;
                            <asp:Button ID="btnreset" runat="server" Text="Reset" CausesValidation="False"></asp:Button>
                            </td>
                        </tr>
                    </asp:Panel>
                    <asp:Panel ID="pnlconfirmation" runat="server" Visible="False">
                        <tr align="center">
                            <td colspan="2">
                                <asp:Label ID="lblErrorMsg" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr align="center">
                            <td colspan="2">
                                <asp:HyperLink ID="hyplnkretry" runat="server"></asp:HyperLink>
                            </td>
                        </tr>
                    </asp:Panel>
                </tbody>
            </table>
        </form>
    </blockquote>
</body>
</html>
