<%@ Page Language="vb" AutoEventWireup="false" Src="ClientSubsidiaryAdd.aspx.vb"
    CodeBehind="ClientSubsidiaryAdd.aspx.vb" Inherits="ClientSubsidiaryAdd" %>

<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>CarzonRent :: Internal software Control panel</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="../HertzInt.css" type="text/css" rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script language="javascript">
        function validation() {
            if (document.forms[0].ddlClient.value == "") {
                alert("Please select Client Name..")
                document.forms[0].ddlClient.focus();
                return false;
            }
            if (document.forms[0].ddlSubName.value == "") {
                alert("Please select Subsidiary Name..")
                document.forms[0].ddlSubName.focus();;
                return false;
            }
            if (document.forms[0].ddlSubAdd.value == "") {
                alert("Please select Subsidiary Address..")
                document.forms[0].ddlSubAdd.focus();
                return false;
            }
        }

        $(document).ready(function () {
            $("#<%=txtSEZExpiry.ClientID %>").datepicker();
        });
    </script>

</head>
<body ms_positioning="GridLayout">
    <form id="Form1" method="post" runat="server">
        <uc1:Headerctrl ID="Headerctrl1" runat="server"></uc1:Headerctrl>
        <table id="Table1" align="center">
            <asp:Panel ID="pnlmainform" runat="server">
                <tbody>
                    <tr>
                        <td align="center" colspan="2">
                            <strong><u>Add a Client Subsidiary</u></strong>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">&nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>* Client Name
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlClient" runat="server" CssClass="input">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>* Subsidiary Name
                        </td>
                        <td>
                            <asp:TextBox ID="ddlSubName" runat="server" CssClass="input" Width="300"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>* Subsidiary Address
                        </td>
                        <td>
                            <asp:TextBox ID="ddlSubAdd" runat="server" CssClass="input" Rows="10" Columns="50" TextMode="MultiLine"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>SEZ ClientYN
                        </td>
                        <td>
                            <asp:CheckBox ID="chkSEZYN" runat="server" CssClass="input" Checked="false"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td>SEZ Expiry date
                        </td>
                        <td>
                            <asp:TextBox ID="txtSEZExpiry" runat="server" CssClass="input"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Do Not Apply Service TaxYN
                        </td>
                        <td>
                            <asp:CheckBox ID="chkDoNotApplyServiceTaxYN" runat="server" CssClass="input" Checked="false"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Active
                        </td>
                        <td>
                            <asp:CheckBox ID="chkactive" runat="server" CssClass="input" Checked="True" Enabled="false"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <asp:Button ID="btnsubmit" runat="server" CssClass="button" Text="Submit"></asp:Button>&nbsp;&nbsp;
                        <asp:Button ID="btnreset" runat="server" CssClass="button" Text="Reset" CausesValidation="False"></asp:Button>
                        </td>
                    </tr>
            </asp:Panel>
            <asp:Panel ID="pnlconfirmation" runat="server" Visible="False">
                <tr align="center">
                    <td colspan="2">
                        <asp:Label ID="lblErrorMsg" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr align="center">
                    <td colspan="2">
                        <asp:HyperLink ID="hyplnkretry" runat="server"></asp:HyperLink>
                    </td>
                </tr>
            </asp:Panel>
            </TBODY>
        </table>
    </form>
</body>
</html>
