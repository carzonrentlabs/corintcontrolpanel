Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class ClientSubsidiaryAdd
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ddlClient As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlSubName As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlSubAdd As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnsubmit As System.Web.UI.WebControls.Button
    Protected WithEvents chkActive As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkDoNotApplyServiceTaxYN As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkSEZYN As System.Web.UI.WebControls.CheckBox
    Protected WithEvents txtSEZExpiry As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink

    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        btnsubmit.Attributes("onClick") = "return validation();"
        If Not Page.IsPostBack() Then
            populateddl()
        End If

    End Sub
    Sub populateddl()
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility

        ddlClient.DataSource = objAcessdata.funcGetSQLDataReader("select ClientCoName, ClientCoID from CorIntClientCoMaster where  providerId=" & Session("provider_Id") & " and active=1 order by ClientCoName ")
        ddlClient.DataValueField = "ClientCoID"
        ddlClient.DataTextField = "ClientCoName"
        ddlClient.DataBind()
        ddlClient.Items.Insert(0, New ListItem("Please Select Client Name", ""))

    End Sub

    Private Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        Dim MyConnection As SqlConnection
        Dim strdate As String
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        Dim intFlagCheck As SqlParameter
        Dim intFlag As Int32

        Dim intFGRParam As SqlParameter
        Dim intFGRid As Int32
        Dim txtHr As String

        'If (TxtAmount.Text) = "" Then
        'TxtAmount.Text = 0
        'End If


        cmd = New SqlCommand("uspAddSubsidiary", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@ClientID", ddlClient.SelectedValue)
        cmd.Parameters.AddWithValue("@SubName", ddlSubName.Text)
        cmd.Parameters.AddWithValue("@SubAdd", ddlSubAdd.Text)
        cmd.Parameters.AddWithValue("@CreatedBy", Session("loggedin_user"))
        cmd.Parameters.AddWithValue("@Active", get_YNvalue(chkActive))
        cmd.Parameters.AddWithValue("@DoNotApplyServiceTaxYN", get_YNvalue(chkDoNotApplyServiceTaxYN))
        cmd.Parameters.AddWithValue("@SEZClientYN", get_YNvalue(chkSEZYN))
        cmd.Parameters.AddWithValue("@SEZExpiry", txtSEZExpiry.Text)

        intFlagCheck = cmd.Parameters.Add("@StatusFlag", SqlDbType.Int)
        intFlagCheck.Direction = ParameterDirection.Output

        intFGRParam = cmd.Parameters.Add("@ID", SqlDbType.Int)
        intFGRParam.Direction = ParameterDirection.Output

        MyConnection.Open()


        cmd.ExecuteNonQuery()
        intFlag = cmd.Parameters("@StatusFlag").Value
        intFGRid = cmd.Parameters("@ID").Value

        MyConnection.Close()
        lblErrorMsg.Visible = True

        If Trim("" & intFlag) = "2" Then

            pnlmainform.Visible = False
            pnlconfirmation.Visible = True

            lblErrorMsg.Text = "The Client Subsidiary already exists!<br> The Subsidiary ID is <b>" & intFGRid & "</b>"
            hyplnkretry.Text = "Add another Subsidiary"
            hyplnkretry.NavigateUrl = "ClientSubsidiaryAdd.aspx"
        Else

            pnlmainform.Visible = False
            pnlconfirmation.Visible = True

            lblErrorMsg.Text = "You have added the Subsidiary." ' <br> The Subsidiary ID is <b>" & intFGRid & "</b>"
            hyplnkretry.Text = "Add another Subsidiary"
            hyplnkretry.NavigateUrl = "ClientSubsidiaryAdd.aspx"
        End If
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function
End Class
