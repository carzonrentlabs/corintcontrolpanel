Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class ClientSubsidiarySearch
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents ddlClientID As System.Web.UI.WebControls.DropDownList


    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not Page.IsPostBack() Then
            populateddl()
        End If
    End Sub
    Sub populateddl()
        Dim dtrreader As SqlDataReader
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility

        dtrreader = objAcessdata.funcGetSQLDataReader("select b.ClientCoName, b.ClientCoID from CORIntClientCoMaster as b where exists (select 1 from CORIntClientCoSubsidiary as a where a.clientcoid = b.ClientCoID and a.active=1) and b.active=1 and ProviderId=" & Session("provider_Id") & "  order by b.ClientCoName ")

        ddlClientID.DataSource = dtrreader
        ddlClientID.DataValueField = "ClientCoID"
        ddlClientID.DataTextField = "ClientCoName"
        ddlClientID.DataBind()
        ddlClientID.Items.Insert(0, New ListItem("Any", -1))
        dtrreader.Close()
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Response.Redirect("ClientSubsidiaryEditList.aspx?ClientID=" & ddlClientID.SelectedItem.Value)
    End Sub
End Class
