Imports commonutility
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class ClientSubsidiaryEdit
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Protected WithEvents ID1 As System.Web.UI.WebControls.Label
    Protected WithEvents lblclientName As System.Web.UI.WebControls.Label
    Protected WithEvents txtSubName As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtSubAdd As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnsubmit As System.Web.UI.WebControls.Button
    Protected WithEvents chkActive As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkDoNotApplyServiceTaxYN As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkSEZYN As System.Web.UI.WebControls.CheckBox
    Protected WithEvents txtSEZExpiry As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents chk_Active As System.Web.UI.WebControls.CheckBox
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel



    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btnsubmit.Attributes("onClick") = "return validation();"
        If Not Page.IsPostBack Then

            'populateddl()
            If Trim("" & Request.QueryString("ID")) <> "" Then
                Dim intID As Integer
                intID = Request.QueryString("ID")
                If Not IsPostBack() Then
                    getvalue(intID)
                End If
            Else
                Response.Write("Bad request.")
                Response.End()
            End If
        End If
    End Sub

    'Sub populateddl()
    'End Sub

    Sub getvalue(ByVal intID As Integer)
        Dim strquery As String
        'Dim StrUrl As StringBuilder
        'Dim str As StringBuilder
        Dim dtrreader As SqlDataReader
        Dim accessdata As clsutility
        Dim createby As String = ""
        Dim ModifiedBy As String = ""

        accessdata = New clsutility

        strquery = "Exec Proc_EditSubsidiaryDetail " & intID & ",2," & Session("provider_Id")

        dtrreader = accessdata.funcGetSQLDataReader(strquery.ToString)
        'If dtrreader.HasRows Then
        While dtrreader.Read
            ID1.Text = dtrreader("SubID")
            lblclientName.Text = dtrreader("ClientCoName")
            txtSubName.Text = dtrreader("SubName")
            txtSubAdd.Text = dtrreader("SubAddress")
            If dtrreader("DoNotApplyServiceTaxYN") Then
                chkDoNotApplyServiceTaxYN.Checked = True
            Else
                chkDoNotApplyServiceTaxYN.Checked = False
            End If

            If dtrreader("SEZClientYN") Then
                chkSEZYN.Checked = True
            Else
                chkSEZYN.Checked = False
            End If

            If (IsDBNull(dtrreader("SEZExpiry"))) Then
            Else
                txtSEZExpiry.Text = dtrreader("SEZExpiry")
            End If
            If dtrreader("Active") Then
                chk_Active.Checked = True
                'chk_Active.Enabled = True
            Else
                chk_Active.Checked = False
                'chk_Active.Enabled = False
            End If
        End While
        'End If
    End Sub
    Function autoselec_ddl(ByVal ddlname As DropDownList, ByVal selectvalue As Int32)
        If Not selectvalue = "0" Then
            'If Not IsDBNull(selectvalue) Then
            ddlname.Items.FindByValue(selectvalue).Selected = True
        End If
    End Function
    Private Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        Dim Flag As Integer
        'Dim intAmount As Integer

        cmd = New SqlCommand("Proc_EditSubsidiary", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@ID", Request.QueryString("ID"))
        cmd.Parameters.AddWithValue("@SubName", txtSubName.Text)
        cmd.Parameters.AddWithValue("@SubAdd", txtSubAdd.Text)
        cmd.Parameters.AddWithValue("@Active", get_YNvalue(chk_Active))
        cmd.Parameters.AddWithValue("@ModifiedBy", Session("loggedin_user"))
        cmd.Parameters.AddWithValue("@DoNotApplyServiceTaxYN", get_YNvalue(chkDoNotApplyServiceTaxYN))
        cmd.Parameters.AddWithValue("@SEZClientYN", get_YNvalue(chkSEZYN))
        cmd.Parameters.AddWithValue("@SEZExpiry", txtSEZExpiry.Text)

        MyConnection.Open()
        Flag = cmd.ExecuteNonQuery()

        MyConnection.Close()
        lblErrorMsg.Visible = True
        'Response.Write("Flag=" & Flag)

        If Trim("" & Flag) = "0" Then

            pnlmainform.Visible = False
            pnlconfirmation.Visible = True

            lblErrorMsg.Text = "Unable to edit Subsidiary!<br> The Subsidiary ID is <b>" & Request.QueryString("id") & "</b>"
            hyplnkretry.Text = "Edit another Subsidiary"
            hyplnkretry.NavigateUrl = "ClientSubsidiarySearch.aspx"
        Else

            pnlmainform.Visible = False
            pnlconfirmation.Visible = True

            lblErrorMsg.Text = "You have edited the Subsidiary" '<br> The Subsidiary ID is <b>" & Request.QueryString("id") & "</b>"
            hyplnkretry.Text = "Edit another Subsidiary"
            hyplnkretry.NavigateUrl = "ClientSubsidiarySearch.aspx"
        End If
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function
End Class
