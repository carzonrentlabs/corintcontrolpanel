Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System

Public Class UnitAddGST
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    Protected WithEvents txtEffectiveDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents txtGSTIN As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtGSTAdd As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlUnitCty As System.Web.UI.WebControls.DropDownList
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btnSubmit.Attributes("onClick") = "return validate_input();"
        If Not Page.IsPostBack Then
            populateddl()
        End If

    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        Dim intuniqcheck As SqlParameter
        Dim intuniqvalue As Int32

        cmd = New SqlCommand("procAddUnitGSTIN", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@unitid", ddlUnitCty.SelectedItem.Value)
        cmd.Parameters.AddWithValue("@GSTIN", txtGSTIN.Text)
        cmd.Parameters.AddWithValue("@GSTRegisteredAddress", txtGSTAdd.Text)
        cmd.Parameters.AddWithValue("@createdby", Session("loggedin_user"))
        cmd.Parameters.AddWithValue("@EffectiveDate", txtEffectiveDate.Text)
        intuniqcheck = cmd.Parameters.Add("@uniqcheckval", SqlDbType.Int)
        intuniqcheck.Direction = ParameterDirection.Output

        MyConnection.Open()
        cmd.ExecuteNonQuery()
        intuniqvalue = cmd.Parameters("@uniqcheckval").Value
        MyConnection.Close()
        If Not intuniqvalue = 0 Then
            lblErrorMsg.Visible = True
            lblErrorMsg.Text = "Unit GSTIN already exist."
            Exit Sub
        Else
            lblErrorMsg.Visible = False
            pnlmainform.Visible = False
            pnlconfirmation.Visible = True

            lblMessage.Text = "You have added the Unit GSTIN successfully"
            hyplnkretry.Text = "Add another Unit GSTIN"
            hyplnkretry.NavigateUrl = "UnitAddGST.aspx"
        End If

    End Sub

    Sub populateddl()
        Dim dtrreader As SqlDataReader
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility

        ddlUnitCty.DataSource = objAcessdata.funcGetSQLDataReader("select UnitID, Unitname from CORIntUnitMaster  where active=1  order by Unitname")
        ddlUnitCty.DataValueField = "UnitID"
        ddlUnitCty.DataTextField = "unitname"
        ddlUnitCty.DataBind()
        ddlUnitCty.Items.Insert(0, New ListItem("", ""))
        objAcessdata.Dispose()

    End Sub

End Class
