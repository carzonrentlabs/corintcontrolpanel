Imports commonutility
Imports System.text
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Imports System.Web.UI
Public Class UnitEditGSTResult
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tblRecDetail As System.Web.UI.WebControls.Table

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not Page.IsPostBack Then
            getvalue("Unitname")
        End If
    End Sub

    Sub getvalue(ByVal strorderby As String)
        Dim strquery As StringBuilder
        Dim dtrreader As SqlDataReader
        Dim accessdata As clsutility
        accessdata = New clsutility
        strquery = New StringBuilder("select a.ID, b.UnitID, b.UnitName, a.EffectiveDate , a.GSTIN, a.GSTRegisteredAddress from CorIntUnitGSTMaster as a  inner join CORIntUnitMaster as b on a.unitid = b.UnitID ")
        If Request.QueryString("id") <> "-1" Then
            strquery.Append(" where b.unitid=" & Request.QueryString("id") & " ")
        End If
        strquery.Append(" order by " & strorderby & "")

        dtrreader = accessdata.funcGetSQLDataReader(strquery.ToString)
        While dtrreader.Read
            Dim Temprow As New TableRow
            Temprow.HorizontalAlign = HorizontalAlign.Center


            Dim Tempcell As New TableCell
            Tempcell.Controls.Add(New LiteralControl(dtrreader("UnitID") & ""))
            Temprow.Cells.Add(Tempcell)

            Dim Tempcel2 As New TableCell
            Tempcel2.Controls.Add(New LiteralControl(dtrreader("UnitName") & ""))
            Temprow.Cells.Add(Tempcel2)

            Dim Tempcel3 As New TableCell
            Tempcel3.Controls.Add(New LiteralControl(dtrreader("EffectiveDate") & ""))
            Temprow.Cells.Add(Tempcel3)

            Dim Tempcel4 As New TableCell
            Tempcel4.Controls.Add(New LiteralControl(dtrreader("GSTIN") & ""))
            Temprow.Cells.Add(Tempcel4)

            Dim Tempcel5 As New TableCell
            Tempcel5.Controls.Add(New LiteralControl(dtrreader("GSTRegisteredAddress") & ""))
            Temprow.Cells.Add(Tempcel5)

            Dim Tempcel8 As New TableCell
            Tempcel8.Controls.Add(New LiteralControl("<a href=UnitEditGSTForm.aspx?ID=" & dtrreader("ID") & " >Edit</a>"))
            Temprow.Cells.Add(Tempcel8)

            'adding the Tables rows to the table
            tblRecDetail.Rows.Add(Temprow)

        End While
        dtrreader.Close()
    End Sub
    Sub SortGird(ByVal sender As Object, ByVal e As System.EventArgs)
        Select Case sender.id
            Case "Linkbutton1"
                getvalue("b.UnitID")
            Case "Linkbutton2"
                getvalue("b.UnitName")
            Case "Linkbutton3"
                getvalue("a.EffectiveDate")
            Case "Linkbutton4"
                getvalue("a.GSTIN")
            Case "Linkbutton5"
                getvalue("a.GSTRegisteredAddress")
        End Select

    End Sub

End Class
