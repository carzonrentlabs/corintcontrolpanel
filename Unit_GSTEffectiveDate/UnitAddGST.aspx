<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" Src="UnitAddGST.aspx.vb" Inherits="UnitAddGST" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>CarzonRent :: Internal software Control Panel</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="../HertzInt.css" type="text/css" rel="stylesheet">

    <script language="JavaScript" src="../utilityfunction.js"></script>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

    <script language="JavaScript">

        $(document).ready(function () {
            $("#<%=txtEffectiveDate.ClientID%>").datepicker();
        });

        function validate_input() {
            if (document.forms[0].ddlUnitCty.value == "") {
                alert("Please select Unit Name.")
                document.forms[0].ddlUnitCty.focus();
                return false;
            }

            if (document.forms[0].txtEffectiveDate.value == "") {
                alert("Please select Effective Date.")
                document.forms[0].txtEffectiveDate.focus();
                return false;
            }

            if (document.forms[0].txtGSTIN.value == "") {
                alert("Please enter GSTIN.")
                document.forms[0].txtGSTIN.focus();
                return false;
            }

            if (document.forms[0].txtGSTAdd.value == "") {
                alert("Please enter GST Address.")
                document.forms[0].txtGSTAdd.focus();
                return false;
            }
        }

        function dateReg(obj) {
            if (obj.value != "") {
                // alert(obj.value);
                var reg = /((0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[0-1])-(201[0-9]))/
                if (reg.test(obj.value)) {
                    //alert('valid');
                }
                else {
                    alert('notvalid');
                    obj.value = "";
                }
            }
        }
    </script>

</head>
<body>
    <form id="Form1" method="post" runat="server">
        <uc1:Headerctrl ID="Headerctrl1" runat="server"></uc1:Headerctrl>
        <table align="center">
            <tbody>
                <tr>
                    <td align="center">
                        <b><u>Add a Unit GST </u></b>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="lblErrorMsg" CssClass="subRedHead" Visible="false" runat="server"></asp:Label></td>
                </tr>
                <asp:Panel ID="pnlmainform" runat="server">
                    <tr>
                        <td>* Unit City Name
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlUnitCty" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>*GSTIN</td>
                        <td>
                            <asp:TextBox ID="txtGSTIN" runat="server" CssClass="input" Enabled="true"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>*GST Address</td>
                        <td>
                            <asp:TextBox ID="txtGSTAdd" runat="server" CssClass="input" Enabled="true"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>* Effective Date</td>
                        <td>
                            <asp:TextBox ID="txtEffectiveDate" runat="server" CssClass="input"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <asp:Button ID="btnSubmit" runat="server" CssClass="input" Text="Submit"></asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;
                            <input type="Reset" cssclass="button" name="Reset" value="Reset" /></td>
                    </tr>
                </asp:Panel>
                <asp:Panel ID="pnlconfirmation" Visible="False" runat="server">
                    <tr align="center">
                        <td colspan="2">
                            <br>
                            <br>
                            <br>
                            <asp:Label ID="lblMessage" runat="server"></asp:Label></td>
                    </tr>
                    <tr align="center">
                        <td colspan="2">
                            <asp:HyperLink ID="hyplnkretry" runat="server"></asp:HyperLink></td>
                    </tr>
                </asp:Panel>
            </tbody>
        </table>
    </form>
</body>
</html>
