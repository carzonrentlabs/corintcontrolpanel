Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class AgencyMasterAdd
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ddlAgency As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddladdress As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlCityID As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlphone As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlcont As System.Web.UI.WebControls.TextBox

    Protected WithEvents ddlmobile As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlemail As System.Web.UI.WebControls.TextBox
    Protected WithEvents TxtAmount As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnsubmit As System.Web.UI.WebControls.Button
    Protected WithEvents chkActive As System.Web.UI.WebControls.CheckBox
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents btnreset As System.Web.UI.WebControls.Button
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        btnsubmit.Attributes("onClick") = "return validation();"
        If Not Page.IsPostBack() Then
            populateddl()
        End If

    End Sub
    Sub populateddl()
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility
        Dim dtrreader As SqlDataReader
        Dim accessdata As clsutility
        accessdata = New clsutility
        dtrreader = accessdata.funcGetSQLDataReader("select cityid,cityname from corintcitymaster where active=1 order by cityname")
        ddlCityID.DataSource = dtrreader
        ddlCityID.DataValueField = "cityid"
        ddlCityID.DataTextField = "cityname"
        ddlCityID.DataBind()
        ddlCityID.Items.Insert(0, New ListItem("Any", 0))
        dtrreader.Close()
    End Sub

    '  Private Sub btnreset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnreset.Click
    '       Response.Redirect("AgencyMasterEdit.aspx?id=" & Request.QueryString("id"))
    '  End Sub

    Private Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        Dim MyConnection As SqlConnection
        Dim strdate As String
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        Dim intFlagCheck As SqlParameter
        Dim intFlag As Int32

        Dim intFGRParam As SqlParameter
        Dim intFGRid As Int32
        Dim txtHr As String

        cmd = New SqlCommand("UspAddAgency", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@AgencyName", ddlAgency.Text)
        cmd.Parameters.Add("@City", ddlCityID.SelectedValue)
        cmd.Parameters.Add("@Address", ddladdress.Text)
        cmd.Parameters.Add("@ContactPerson", ddlcont.Text)
        cmd.Parameters.Add("@Phone", ddlphone.Text)
        cmd.Parameters.Add("@Mobile", ddlmobile.Text)
        cmd.Parameters.Add("@EmailID", ddlemail.Text)
        cmd.Parameters.Add("@CreatedBy", Session("loggedin_user"))
        cmd.Parameters.Add("@Active", get_YNvalue(chkActive))

        intFlagCheck = cmd.Parameters.Add("@StatusFlag", SqlDbType.Int)
        intFlagCheck.Direction = ParameterDirection.Output

        intFGRParam = cmd.Parameters.Add("@ID", SqlDbType.Int)
        intFGRParam.Direction = ParameterDirection.Output

        MyConnection.Open()


        cmd.ExecuteNonQuery()
        intFlag = cmd.Parameters("@StatusFlag").Value
        intFGRid = cmd.Parameters("@ID").Value

        MyConnection.Close()
        lblErrorMsg.Visible = True

        If Trim("" & intFlag) = "2" Then

            pnlmainform.Visible = False
            pnlconfirmation.Visible = True

            lblErrorMsg.Text = "This Add Agency Master already exists!<br> The Agency Master ID is <b>" & intFGRid & "</b>"
            hyplnkretry.Text = "Add another Agency Master"
            hyplnkretry.NavigateUrl = "AgencyMasterAdd.aspx"
        Else

            pnlmainform.Visible = False
            pnlconfirmation.Visible = True

            lblErrorMsg.Text = "You have added the Agency Master <br> The Agency Master ID is <b>" & intFGRid & "</b>"
            hyplnkretry.Text = "Add another Agency Master"
            hyplnkretry.NavigateUrl = "AgencyMasterAdd.aspx"
        End If
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function
End Class
