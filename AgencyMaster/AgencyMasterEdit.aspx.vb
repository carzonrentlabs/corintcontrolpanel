Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class AgencyMasterEdit
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ID1 As System.Web.UI.WebControls.Label
    Protected WithEvents ddlAgency As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlCityID As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddladdress As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlcont As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlphone As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlmobile As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlemail As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnsubmit As System.Web.UI.WebControls.Button
    Protected WithEvents chkActive As System.Web.UI.WebControls.CheckBox
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents chk_Active As System.Web.UI.WebControls.CheckBox
    Protected WithEvents btnreset As System.Web.UI.WebControls.Button
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btnsubmit.Attributes("onclick") = "return validation();"
        If Not Page.IsPostBack Then
            populateddl()
            Dim dtrreader As SqlDataReader
            Dim accessdata As clsutility
            accessdata = New clsutility
            dtrreader = accessdata.funcGetSQLDataReader("select * from CorIntAgencyMaster where  AgencyID=" & Request.QueryString("id") & " ")
            dtrreader.Read()
            ID1.Text = dtrreader("AgencyID")
            autoselec_ddl(ddlCityID, dtrreader("CityID"))
            ddlAgency.Text = dtrreader("AgencyName")
            ddladdress.Text = dtrreader("Address") & ""
            ddlcont.Text = dtrreader("ContactPerson") & ""
            ddlphone.Text = dtrreader("Phone") & ""
            ddlmobile.Text = dtrreader("Mobile") & ""
            ddlemail.Text = dtrreader("emailid") & ""
            chk_active.Checked = dtrreader("Active")
            dtrreader.Close()
            accessdata.Dispose()
        End If

    End Sub

    Sub populateddl()
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility
        ddlCityID.DataSource = objAcessdata.funcGetSQLDataReader("select cityname,cityid from CORIntCityMaster  where active=1 order by cityname")
        ddlCityID.DataValueField = "cityid"
        ddlCityID.DataTextField = "cityname"
        ddlCityID.DataBind()
        ddlCityID.Items.Insert(0, New ListItem("", ""))
        objAcessdata.Dispose()

    End Sub
    Function autoselec_ddl(ByVal ddlname As DropDownList, ByVal selectvalue As Int32)
        If Not selectvalue = "0" Then
            ddlname.Items.FindByValue(selectvalue).Selected = True
        End If
    End Function

    Private Sub btnreset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnreset.Click
        Response.Redirect("AgencyMasterEdit.aspx?id=" & Request.QueryString("id"))
    End Sub

    Private Sub btnsubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsubmit.Click

        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        'Dim intuniqcheck As SqlParameter
        Dim intuniqvalue As int32
        cmd = New SqlCommand("procEditAgencyMaster", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("@AgencyID", ID1.Text)
        cmd.Parameters.Add("@AgencyName", ddlAgency.Text)
        cmd.Parameters.Add("@CityID", ddlCityID.SelectedItem.Value)
        cmd.Parameters.Add("@Address", ddladdress.Text)
        cmd.Parameters.Add("@ContactPerson", ddlcont.Text)
        cmd.Parameters.Add("@Phone", ddlphone.Text)
        cmd.Parameters.Add("@Mobile", ddlmobile.Text)
        cmd.Parameters.Add("@EmailID", ddlemail.Text)
        cmd.Parameters.Add("@active", get_YNvalue(chk_active))
        cmd.Parameters.Add("@modifiedby", Session("loggedin_user"))
        '  intuniqcheck = cmd.Parameters.Add("@uniqcheckval", SqlDbType.Int)
        '  intuniqcheck.Direction = ParameterDirection.Output

        '  Try
        MyConnection.Open()
        cmd.ExecuteNonQuery()
        '  intuniqvalue = cmd.Parameters("@uniqcheckval").Value
        MyConnection.Close()
        '  If Not intuniqvalue = 0 Then
        '  lblErrorMsg.visible = True
        '  lblErrorMsg.text = "Agency Master already exist."
        '  Exit Sub
        '  Else
        lblErrorMsg.visible = False
        pnlmainform.Visible = False
        pnlconfirmation.Visible = True

        lblMessage.Text = "You have updated the Agency Master successfully"
        hyplnkretry.Text = "Edit another Agency Master"
        hyplnkretry.NavigateUrl = "AgencyMasterSearch.aspx"
        ' End If
        '   Catch
        '   End Try
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function

End Class
