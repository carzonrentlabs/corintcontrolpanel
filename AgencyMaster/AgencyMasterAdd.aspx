<%@ Page Language="vb" AutoEventWireup="false" Src="AgencyMasterAdd.aspx.vb" Codebehind="AgencyMasterAdd.aspx.vb" Inherits="AgencyMasterAdd"%>
<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CarzonRent :: Internal software Control panel</title>
		<meta  content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta  content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript" >
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" >
		<script language="JavaScript" src="../utilityfunction.js"></script>
		<script language="JavaScript" src="../JScripts/Datefunc.js"></script>
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
		<script language="javascript">
		function validation()
		{
			var strAgency = document.getElementById('ddlAgency').value;
			strAgency = strAgency.replace(/^\s+/,""); //Removes Left Blank Spaces
			strAgency = strAgency.replace(/\s+$/,""); //Removes Right Blank Spaces
			
			var straddress = document.getElementById('ddladdress').value;
			straddress = straddress.replace(/^\s+/,""); //Removes Left Blank Spaces
			straddress = straddress.replace(/\s+$/,""); //Removes Right Blank Spaces
			
			var strcont = document.getElementById('ddlcont').value;
			strcont = strcont.replace(/^\s+/,""); //Removes Left Blank Spaces
			strcont = strcont.replace(/\s+$/,""); //Removes Right Blank Spaces
			
			var strphone = document.getElementById('ddlphone').value;
			strphone = strphone.replace(/^\s+/,""); //Removes Left Blank Spaces
			strphone = strphone.replace(/\s+$/,""); //Removes Right Blank Spaces
			
			var strmobile = document.getElementById('ddlmobile').value;
			strmobile = strmobile.replace(/^\s+/,""); //Removes Left Blank Spaces
			strmobile = strmobile.replace(/\s+$/,""); //Removes Right Blank Spaces
			
			//if((document.getElementById('ddlAgency').value) == ""  || (document.getElementById('ddladdress').value) == "" || (document.getElementById('ddlcont').value) == ""  || (document.getElementById('ddlphone').value) == ""  || (document.getElementById('ddlmobile').value) == ""  )
			if( strAgency== ""  || straddress == "" || strcont == ""  || strphone == ""  || strmobile == ""  )
			{
				alert("Please do not leave the field blank.")
				document.getElementById('ddlAgency').focus();
				return false;	
			}
			//if(document.forms[0].ddlemail.value!="")
			if(document.getElementById('ddlemail').value!="")
				{
					
					//var theStr=document.forms[0].ddlemail.value;
					var theStr=document.getElementById('ddlemail').value;
					var atIndex = theStr.indexOf('@'); 
					var dotIndex = theStr.indexOf('.', atIndex); 
					theSub = theStr.substring(0, dotIndex+1) 
		if ((atIndex < 1)||(atIndex != theStr.lastIndexOf('@'))||(dotIndex < atIndex + 2)||(theStr.length <= theSub.length)) 
		{ 
				alert("Email ID is not a valid Email ID");
				document.getElementById('ddlemail').focus();
				return false;
		}
				} 	
			else
			{
				alert("Email ID is not a valid Email ID");
				document.getElementById('ddlemail').focus();
				return false;
			}

			
			return true;	
		}		
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<uc1:Headerctrl id="Headerctrl1" runat="server"></uc1:Headerctrl>
			<TABLE id="Table1" align="center">
				<asp:panel id="pnlmainform" Runat="server">
					<TBODY>
						<TR>
							<TD align="center" colSpan="2"><STRONG><U>Add a Agency Master</U></STRONG></TD>
						</TR>
						<TR>
							<TD align="center" colSpan="2">&nbsp;&nbsp;</TD>
						</TR>
						<TR>
							<TD>* Agency Name</TD>
							<TD><asp:TextBox id="ddlAgency" runat="server" CssClass="input"></asp:TextBox></TD>
						</TR>
						<TR>
							<TD>* City</TD>
							<TD><asp:DropDownList id="ddlCityID" runat="server" CssClass="input"></asp:DropDownList></TD>
						</TR>
						<TR>
							<TD>* Address</TD>
							<TD><asp:TextBox id="ddladdress" runat="server" CssClass="input"></asp:TextBox></TD>
						</TR>
						<TR>
							<TD>* Contact Person</TD>
							<TD><asp:TextBox id="ddlcont" runat="server" CssClass="input"></asp:TextBox></TD>
						</TR>
							<TR>
							<TD>* Phone</TD>
							<TD><asp:TextBox id="ddlphone" runat="server" CssClass="input"></asp:TextBox></TD>
						</TR>
						<TR>
							<TD>* Mobile</TD>
							<TD><asp:TextBox id="ddlmobile" runat="server" CssClass="input"></asp:TextBox></TD>
						</TR>
						<TR>
							<TD>* Email ID</TD>
							<TD><asp:TextBox ID="ddlemail" runat="server" CssClass="input"></asp:TextBox></TD>
						</TR>
							
						<TR>
							<TD>Active</TD>
							<TD><asp:checkbox id="chkactive" runat="server" CssClass="input" Checked="True"></asp:checkbox></TD>
						</TR>
						<TR>
							<TD align="center" colSpan="2">
				<asp:button id="btnsubmit" runat="server" CssClass="button" Text="Submit"></asp:button>&nbsp;&nbsp;
				<asp:button id="btnreset" runat="server" CssClass="button" Text="Reset" CausesValidation="False"></asp:button></TD>
						</TR>
				</asp:panel>
				<asp:panel id="pnlconfirmation" Runat="server" Visible="False">
					<TR align="center">
						<TD colSpan="2"><asp:Label id="lblErrorMsg" runat="server"></asp:Label></TD>
					</TR>
					<TR align="center">
						<TD colSpan="2"><asp:HyperLink id="hyplnkretry" runat="server"></asp:HyperLink></TD>
					</TR>
				</asp:panel></TBODY></TABLE>
		</form>
	</body>
</HTML>