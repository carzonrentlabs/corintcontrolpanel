Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System

Public Class ScheduleEditCoForm
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ddFrequency As System.Web.UI.WebControls.DropDownList
    Protected WithEvents btnsubmit As System.Web.UI.WebControls.Button
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents lblCompany As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label


    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not Page.IsPostBack Then
            Dim dtrreader As SqlDataReader
            Dim accessdata As clsutility
            accessdata = New clsutility
            dtrreader = accessdata.funcGetSQLDataReader("select Frequency from CORIntDSInvEmailScheduleMaster where ClientCoID=" & Request.QueryString("id") & " ")
            dtrreader.Read()
            ddFrequency.Items.FindByValue(dtrreader("Frequency")).Selected = True
            dtrreader.Close()

            dtrreader = accessdata.funcGetSQLDataReader("select ClientCoName from CORIntClientCoMaster where ClientCoID = " & Request.QueryString("id") & " ")
            dtrreader.Read()
            lblCompany.Text = dtrreader("ClientCoName")
            dtrreader.Close()
        End If
    End Sub

    Private Sub btnsubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand

        cmd = New SqlCommand("procEditDSInvEmailScheduleCo", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@ClientCoID ", Request.QueryString("id"))
        cmd.Parameters.Add("@Frequency", ddFrequency.SelectedItem.Value)
        cmd.Parameters.Add("@ModifiedBy", Session("loggedin_user"))
        Try
         MyConnection.Open()
         cmd.ExecuteNonQuery()
         MyConnection.Close()
            lblErrorMsg.visible=false
            pnlmainform.Visible = False
            pnlconfirmation.Visible = True

            lblMessage.Text = "You have updated the schedule successfully"
            hyplnkretry.Text = "Edit another schedule"
            hyplnkretry.NavigateUrl = "ScheduleEditCoSearch.aspx"
        Catch
        End Try
    End Sub
End Class