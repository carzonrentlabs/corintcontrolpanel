<%@ Page Language="vb" AutoEventWireup="false" Src="ScheduleAddCoForm.aspx.vb" Inherits="ScheduleAddCoForm"%>
<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CarzonRent :: Internal software Control Panel</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../utilityfunction.js"></script>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<uc1:Headerctrl id="Headerctrl1" runat="server"></uc1:Headerctrl>
			<P></P>
			<TABLE align="center">
				
					<TBODY>
						<TR>
							<TD align="center" colSpan="2"><STRONG><U>Add the schedule for <asp:Label id="lblCompany" cssclass="subRedHead" runat="server"></asp:Label></U></STRONG>
							</TD>
						</TR>
						<tr><td colspan="2" align="center"><asp:Label id="lblErrorMsg" cssclass="subRedHead" visible="false" runat="server"></asp:Label></td>

						<asp:Panel ID="pnlmainform" Runat="server">
						<TR>
							<TD vAlign="top">* Frequency</TD>
							<TD>
								<asp:dropdownlist id="ddFrequency" runat="server" CssClass="input">
									<asp:listitem Text="Daily" Value='D' />
									<asp:listitem Text="Weekly" Value='W' />
									<asp:listitem Text="Bi-weekly" Value='B' />
									<asp:listitem Text="Monthly" Value='M' />
								</asp:dropdownlist></TD>
						</TR>
						<TR>
							<TD align="center" colSpan="2">
								<asp:button id="btnsubmit" runat="server" CssClass="button" Text="Submit"></asp:button>&nbsp;&nbsp;
								<input type="Reset" CssClass="button" name="Reset" value="Reset" /></TD>
						</TR>
				</asp:Panel>
				<asp:Panel ID="pnlconfirmation" Visible="False" Runat="server">
					<TR align="center">
						<TD colSpan="2">
						<SPAN class="shwText" id="shwMessage"></SPAN>
							<asp:Label id="lblMessage" runat="server"></asp:Label></TD>
					</TR>
					<TR align="center">
						<TD colSpan="2">
							<asp:HyperLink id="hyplnkretry" runat="server"></asp:HyperLink></TD>
					</TR>
				</asp:Panel></TBODY>
			</TABLE>
		</form>
	</body>
</HTML>
