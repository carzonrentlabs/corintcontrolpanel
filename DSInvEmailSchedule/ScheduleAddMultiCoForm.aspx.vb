Imports commonutility
Imports System.text
Imports System.data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class ScheduleAddMultiCoForm
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblusername As System.Web.UI.WebControls.Label
    Protected WithEvents lstavailablemodule As System.Web.UI.WebControls.ListBox
    Protected WithEvents lstAssignedModules As System.Web.UI.WebControls.ListBox
    Protected WithEvents btnsubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnreset As System.Web.UI.WebControls.Button
    Protected WithEvents hdnSelectedlst As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        btnsubmit.Attributes("onClick") = "return postForm();"
        'Put user code to initialize the page here
        If Not Page.IsPostBack Then
            Dim dtrreader As SqlDataReader
            Dim accessdata As clsutility
            accessdata = New clsutility
            dtrreader = accessdata.funcGetSQLDataReader("select ClientCoID, ClientCoName from CORIntClientCoMaster where Active = 1 and ClientCoID Not in (select ClientCoID from CORIntDSInvEmailScheduleMaster) order by ClientCoName")
            lstavailablemodule.DataSource = dtrreader
            lstavailablemodule.DataValueField = "ClientCoID"
            lstavailablemodule.DataTextField = "ClientCoName"
            lstavailablemodule.DataBind()
            dtrreader.Close()
            accessdata.Dispose()

            if Request.QueryString("Freq") = "D" then
               lblusername.Text = "Daily"
            elseif Request.QueryString("Freq") = "W" then
               lblusername.Text = "Weekly"
            elseif Request.QueryString("Freq") = "B" then
               lblusername.Text = "Bi-monthly"
            elseif Request.QueryString("Freq") = "M" then
               lblusername.Text = "Monthly"
            end if
        End If
    End Sub

    Private Sub btnsubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsubmit.Click

        Dim arrModuleid As Array
        Dim intcounter As Int32
        arrModuleid = Request.Form("hdnSelectedlst").Split(",")

        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand

        For intcounter = 0 To UBound(arrModuleid)
            cmd = New SqlCommand("procAddDSInvEmailScheduleMultiCo", MyConnection)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@ClientCoID ", arrModuleid(intcounter))
            cmd.Parameters.Add("@Frequency", Request.QueryString("Freq"))
            cmd.Parameters.Add("@CreatedBy", Session("loggedin_user"))
            MyConnection.Open()
            Try
                cmd.ExecuteNonQuery()
            Catch exp As SqlException
            End Try
            MyConnection.Close()

        Next

        pnlmainform.Visible = False
        pnlconfirmation.Visible = True

        lblMessage.Text = "You have add the schedules successfully"
        hyplnkretry.Text = "Add schedules for more companies"
        hyplnkretry.NavigateUrl = "ScheduleAddMultiCoSearch.aspx"
       
    End Sub

    Private Sub btnreset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnreset.Click
        Response.Redirect("ScheduleAddMultiCoForm.aspx?Freq=" & Request.QueryString("Freq") & "")
    End Sub
End Class
