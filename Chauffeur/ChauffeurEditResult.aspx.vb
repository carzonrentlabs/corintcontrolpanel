Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.text
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Imports System.Web.UI
Public Class ChauffeurEditResult
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tblRecDetail As System.Web.UI.WebControls.Table

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not Page.IsPostBack Then
            getvalue("chaufername")
        End If
    End Sub
    Sub getvalue(ByVal strorderby As String)
        Dim strquery As StringBuilder
        Dim dtrreader As SqlDataReader
        Dim accessdata As clsutility
        accessdata = New clsutility
        strquery = New StringBuilder("select m.unitname,C.licenceno,C.chauffeurid,C.fname+' '+C.mname+' '+C.lname as chaufername,case C.active when '1' then 'Active' when '0' then 'Not Active' end as active, C.Mobile, C.LicenceTo, C.BadgeNo from CORIntChauffeurMaster C, CORIntUnitMaster M where M.unitid = C.unitid")
        If Request.QueryString("id") <> "-1" Then
            strquery.Append(" and C.chauffeurid=" & Request.QueryString("id") & " ")
        End If
        strquery.Append(" order by " & strorderby & "")

        '   Response.Write(strquery.ToString)
        '   Response.End()

        dtrreader = accessdata.funcGetSQLDataReader(strquery.ToString)
        While dtrreader.Read
            Dim Temprow As New TableRow
            Temprow.HorizontalAlign = HorizontalAlign.Center
            Dim Tempcell As New TableCell
            Tempcell.Controls.Add(New LiteralControl(dtrreader("chaufername") & ""))
            Temprow.Cells.Add(Tempcell)

            Dim Tempcel2 As New TableCell
            Tempcel2.Controls.Add(New LiteralControl(dtrreader("unitname") & ""))
            Temprow.Cells.Add(Tempcel2)

            Dim Tempcel3 As New TableCell
            Tempcel3.Controls.Add(New LiteralControl(dtrreader("licenceno") & ""))
            Temprow.Cells.Add(Tempcel3)

            Dim Tempcel6 As New TableCell
            Tempcel6.Controls.Add(New LiteralControl(dtrreader("Mobile") & ""))
            Temprow.Cells.Add(Tempcel6)

            Dim Tempcel7 As New TableCell
            Tempcel7.Controls.Add(New LiteralControl(dtrreader("BadgeNo") & ""))
            Temprow.Cells.Add(Tempcel7)

            Dim Tempcel8 As New TableCell
            Tempcel8.Controls.Add(New LiteralControl(dtrreader("LicenceTo") & ""))
            Temprow.Cells.Add(Tempcel8)

            Dim Tempcel4 As New TableCell
            Tempcel4.Controls.Add(New LiteralControl(dtrreader("active") & ""))
            Temprow.Cells.Add(Tempcel4)

            Dim Tempcel5 As New TableCell
            Tempcel5.Controls.Add(New LiteralControl("<a href=ChauffeurEditForm.aspx?ID=" & dtrreader("chauffeurid") & " >Edit</a>"))
            Temprow.Cells.Add(Tempcel5)
            'adding the Tables rows to the table
            tblRecDetail.Rows.Add(Temprow)

        End While
        dtrreader.Close()
        accessdata.Dispose()


    End Sub
    Sub SortGird(ByVal sender As Object, ByVal e As System.EventArgs)
        Select Case sender.id
            Case "Linkbutton1"
                getvalue("chaufername")
            Case "Linkbutton2"
                getvalue("m.unitname")
            Case "Linkbutton4"
                getvalue("C.licenceno")
            Case "Linkbutton6"
                getvalue("C.Mobile")
            Case "Linkbutton7"
                getvalue("C.BadgeNo")
            Case "Linkbutton8"
                getvalue("C.LicenceTo")
            Case "Linkbutton3"
                getvalue("active")
        End Select

    End Sub

End Class
