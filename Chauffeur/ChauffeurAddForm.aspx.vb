Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class ChauffeurAddForm
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtfname As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtmidname As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtlname As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtarearemarks As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkActive As System.Web.UI.WebControls.CheckBox
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnReset As System.Web.UI.WebControls.Button
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    Protected WithEvents ddlunit As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtphone As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtMobile As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtdoj As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtlicence As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlpaceissue As System.Web.UI.WebControls.DropDownList
    Protected WithEvents TextlivValidfrom As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtlicvalidto As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtesi As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtbadge As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtpf As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtac As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtbankname As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtbasic As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtnighthalt As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtovernight As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtCarwash As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtoutstation As System.Web.UI.WebControls.TextBox
    Protected WithEvents txthra As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtuniform As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtcon As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtothers As System.Web.UI.WebControls.TextBox
    Protected WithEvents txttotsal As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnproceed As System.Web.UI.WebControls.Button
    Protected WithEvents pnl2ndform As System.Web.UI.WebControls.Panel
    Protected WithEvents txtadd As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtdob As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtpmtadd As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtrefphone As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddbloofgrp As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddAttendrat As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddAtt As System.Web.UI.WebControls.DropDownList
    Protected WithEvents dddisplin As System.Web.UI.WebControls.DropDownList
    Protected WithEvents dduniform As System.Web.UI.WebControls.DropDownList
    Protected WithEvents dddrivskill As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddcarcare As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddresponserat As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddpunctuality As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddaccident As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddtrun As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddmachineknow As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddbehaviuor As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddoverall As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtRefname As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtrefadd As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtrefinfo As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtreligion As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtbelongs As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtlang As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label


    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btnproceed.Attributes("onClick") = "return checkbeforenext();"
        btnSubmit.Attributes("onClick") = "return checkvalue();"
        If Not Page.IsPostBack Then
            populateddl()
        End If
    End Sub

    Sub populateddl()
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility

        ddlunit.DataSource = objAcessdata.funcGetSQLDataReader("select unitname,unitid from CORIntUnitMaster  where active=1 order by unitname")
        ddlunit.DataValueField = "unitid"
        ddlunit.DataTextField = "unitname"
        ddlunit.DataBind()
        ddlunit.Items.Insert(0, New ListItem("", ""))

        ddlpaceissue.DataSource = objAcessdata.funcGetSQLDataReader("select cityname,cityid from CORIntCityMaster  where active=1 order by cityname")
        ddlpaceissue.DataValueField = "cityid"
        ddlpaceissue.DataTextField = "cityname"
        ddlpaceissue.DataBind()
        ddlpaceissue.Items.Insert(0, New ListItem("", ""))

        objAcessdata.funcpopulatenumddw(10, 1, ddAttendrat)
        objAcessdata.funcpopulatenumddw(10, 1, ddAtt)
        objAcessdata.funcpopulatenumddw(10, 1, dddisplin)
        objAcessdata.funcpopulatenumddw(10, 1, dduniform)
        objAcessdata.funcpopulatenumddw(10, 1, dddrivskill)
        objAcessdata.funcpopulatenumddw(10, 1, ddcarcare)
        objAcessdata.funcpopulatenumddw(10, 1, ddresponserat)
        objAcessdata.funcpopulatenumddw(10, 1, ddpunctuality)
        objAcessdata.funcpopulatenumddw(10, 1, ddaccident)
        objAcessdata.funcpopulatenumddw(10, 1, ddtrun)
        objAcessdata.funcpopulatenumddw(10, 1, ddmachineknow)
        objAcessdata.funcpopulatenumddw(10, 1, ddbehaviuor)
        objAcessdata.funcpopulatenumddw(10, 1, ddoverall)

        objAcessdata.Dispose()

    End Sub

    Private Sub btnproceed_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnproceed.Click
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        Dim intuniqcheck As SqlParameter
        Dim intuniqvalue as int32
        'execueting the stored procedure for checking the user login validity by using the output parameter
        cmd = New SqlCommand("procCheckChaufLisenNo", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@active", get_YNvalue(chkActive))
        cmd.Parameters.Add("@licenceno", txtlicence.Text)
        intuniqcheck = cmd.Parameters.Add("@uniqcheckval", SqlDbType.Int)
        intuniqcheck.Direction = ParameterDirection.Output
        MyConnection.Open()
        cmd.ExecuteNonQuery()
        'getting the value to know that whethre the user is vallid user for login or not
        intuniqvalue=cmd.Parameters("@uniqcheckval").Value
        MyConnection.Close()

        if not intuniqvalue = 0 then
                lblErrorMsg.visible=true
                lblErrorMsg.text="Licence# already exist."
                exit sub
        else
            pnlmainform.Visible = False
            pnl2ndform.Visible = True
        end if


    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
         Dim intuniqcheck As SqlParameter
        Dim intuniqvalue as int32

        cmd = New SqlCommand("procaddChauffeurMaster", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("@fname", txtfname.Text)
        cmd.Parameters.Add("@mname", txtmidname.Text)
        cmd.Parameters.Add("@lname", txtlname.Text)
        cmd.Parameters.Add("@unitid", ddlunit.SelectedItem.Value)
        cmd.Parameters.Add("@address", txtadd.Text)
        cmd.Parameters.Add("@phone", txtphone.Text)
        cmd.Parameters.Add("@mobile", txtMobile.Text)
        cmd.Parameters.Add("@bloodgroup", ddbloofgrp.SelectedItem.Value)
        cmd.Parameters.Add("@dob", txtdob.Text)
        cmd.Parameters.Add("@joindate", txtdoj.Text)
        cmd.Parameters.Add("@licenceno", txtlicence.Text)
        cmd.Parameters.Add("@issuecityid", ddlpaceissue.SelectedItem.Value)
        cmd.Parameters.Add("@licencefrom", TextlivValidfrom.Text)
        cmd.Parameters.Add("@licenceto", txtlicvalidto.Text)
        cmd.Parameters.Add("@permanentadd", txtpmtadd.Text)
        cmd.Parameters.Add("@esino", txtesi.Text)
        cmd.Parameters.Add("@badgeno", txtbadge.Text)
        cmd.Parameters.Add("@attendance", ddAttendrat.SelectedItem.Value)
        cmd.Parameters.Add("@attitude", ddAtt.SelectedItem.Value)
        cmd.Parameters.Add("@discipline", dddisplin.SelectedItem.Value)
        cmd.Parameters.Add("@uniform", dduniform.SelectedItem.Value)
        cmd.Parameters.Add("@drivingskills", dddrivskill.SelectedItem.Value)
        cmd.Parameters.Add("@carcare", ddcarcare.SelectedItem.Value)
        cmd.Parameters.Add("@clientresponse", ddresponserat.SelectedItem.Value)
        cmd.Parameters.Add("@punctuality", ddresponserat.SelectedItem.Value)
        cmd.Parameters.Add("@accident", ddaccident.SelectedItem.Value)
        cmd.Parameters.Add("@turnout", ddtrun.SelectedItem.Value)
        cmd.Parameters.Add("@machineknowhow", ddmachineknow.SelectedItem.Value)
        cmd.Parameters.Add("@behaviour", ddbehaviuor.SelectedItem.Value)
        cmd.Parameters.Add("@overallperformance", ddoverall.SelectedItem.Value)
        cmd.Parameters.Add("@refname", txtRefname.Text)
        cmd.Parameters.Add("@refaddress", txtrefadd.Text)
        cmd.Parameters.Add("@refph", txtrefphone.Text)
        cmd.Parameters.Add("@refinfo", txtrefinfo.Text)
        cmd.Parameters.Add("@pfno", txtpf.Text)
        cmd.Parameters.Add("@acno", txtac.Text)
        cmd.Parameters.Add("@bankname", txtbankname.Text)
        cmd.Parameters.Add("@religion", txtreligion.Text)
        cmd.Parameters.Add("@belongregion", txtbelongs.Text)
        cmd.Parameters.Add("@languageknown", txtlang.Text)
        cmd.Parameters.Add("@salbasic", txtbasic.Text)
        cmd.Parameters.Add("@salnighthalt", txtnighthalt.Text)
        cmd.Parameters.Add("@salovertimehr", txtovernight.Text)
        cmd.Parameters.Add("@salcarwash", txtCarwash.Text)
        cmd.Parameters.Add("@saloutstation", txtoutstation.Text)
        cmd.Parameters.Add("@salhra", txthra.Text)
        cmd.Parameters.Add("@saluniform", txtuniform.Text)
        cmd.Parameters.Add("@salconveyance", txtcon.Text)
        cmd.Parameters.Add("@salothers", txtothers.Text)
        cmd.Parameters.Add("@saltotal", txttotsal.Text)
        cmd.Parameters.Add("@remarks", txtarearemarks.Text)
        cmd.Parameters.Add("@active", get_YNvalue(chkActive))
        cmd.Parameters.Add("@createdby", Session("loggedin_user"))

        '  Try
        MyConnection.Open()
        cmd.ExecuteNonQuery()
        MyConnection.Close()
        pnlmainform.Visible = False
        pnl2ndform.Visible = False
        pnlconfirmation.Visible = True

        lblMessage.Text = "You have added the Chauffeur successfully"
        hyplnkretry.Text = "Add another Chauffeur"
        hyplnkretry.NavigateUrl = "ChauffeurAddForm.aspx"
        '   Catch
        '   End Try
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function
    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("ChauffeurAddForm.aspx?id=" & Request.QueryString("id"))
    End Sub
End Class
