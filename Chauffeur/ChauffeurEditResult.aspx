<%@ Page Language="vb" AutoEventWireup="false" Src="ChauffeurEditResult.aspx.vb" Inherits="ChauffeurEditResult"%>
<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CarzonRent :: Internal software Control Panel</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<uc1:Headerctrl id="Headerctrl1" runat="server"></uc1:Headerctrl>
			<table align="center">
				<tr>
					<td align="center"><STRONG><U>Edit a Chauffeur Package where</U></STRONG>
						<br>
						<br>
						<br>
					</td>
				</tr>
				<tr>
					<td valign="top">
						<asp:table ID="tblRecDetail" HorizontalAlign="Center" Runat="server" BorderColor="#cccc99"
							BorderStyle="Solid" GridLines="both" CellSpacing="0" CellPadding="0">
							<asp:tablerow>
								<asp:tableheadercell>
									&nbsp;&nbsp;
<asp:linkbutton runat="server" Text="Chauffeur Name" OnClick="SortGird" ID="Linkbutton1"></asp:linkbutton>&nbsp;&nbsp;
								</asp:tableheadercell>
								<asp:tableheadercell>
									&nbsp;&nbsp;
<asp:linkbutton runat="server" Text="Unit Name" OnClick="SortGird" ID="Linkbutton2"></asp:linkbutton>&nbsp;&nbsp;
								</asp:tableheadercell>
								<asp:tableheadercell>
									&nbsp;&nbsp;
<asp:linkbutton runat="server" Text="Licence #" OnClick="SortGird" ID="Linkbutton4"></asp:linkbutton>&nbsp;&nbsp;
								</asp:tableheadercell>
								<asp:tableheadercell>
									&nbsp;&nbsp;
<asp:linkbutton runat="server" Text="Mobile #" OnClick="SortGird" ID="Linkbutton6"></asp:linkbutton>&nbsp;&nbsp;
								</asp:tableheadercell>
								<asp:tableheadercell>
									&nbsp;&nbsp;
<asp:linkbutton runat="server" Text="Badge #" OnClick="SortGird" ID="Linkbutton7"></asp:linkbutton>&nbsp;&nbsp;
								</asp:tableheadercell>
								<asp:tableheadercell>
									&nbsp;&nbsp;
<asp:linkbutton runat="server" Text="Licence Expiry" OnClick="SortGird" ID="Linkbutton8"></asp:linkbutton>&nbsp;&nbsp;
								</asp:tableheadercell>
								<asp:tableheadercell>
									&nbsp;&nbsp;
<asp:linkbutton runat="server" Text="Active" OnClick="SortGird" ID="Linkbutton3"></asp:linkbutton>&nbsp;&nbsp;
								</asp:tableheadercell>
								<asp:tableheadercell>
									&nbsp;&nbsp;
								</asp:tableheadercell>
							</asp:tablerow>
						</asp:table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
