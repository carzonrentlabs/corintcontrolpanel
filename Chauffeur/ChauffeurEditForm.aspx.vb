Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class ChauffeurEditForm
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtfname As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtmidname As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtlname As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlunit As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtphone As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtMobile As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtdoj As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtlicence As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlpaceissue As System.Web.UI.WebControls.DropDownList
    Protected WithEvents TextlivValidfrom As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtlicvalidto As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtesi As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtbadge As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtpf As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtac As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtbankname As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtbasic As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtnighthalt As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtovernight As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtCarwash As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtoutstation As System.Web.UI.WebControls.TextBox
    Protected WithEvents txthra As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtuniform As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtcon As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtothers As System.Web.UI.WebControls.TextBox
    Protected WithEvents txttotsal As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtarearemarks As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkActive As System.Web.UI.WebControls.CheckBox
    Protected WithEvents btnproceed As System.Web.UI.WebControls.Button
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents txtadd As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddbloofgrp As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtdob As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtpmtadd As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddAttendrat As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddAtt As System.Web.UI.WebControls.DropDownList
    Protected WithEvents dddisplin As System.Web.UI.WebControls.DropDownList
    Protected WithEvents dduniform As System.Web.UI.WebControls.DropDownList
    Protected WithEvents dddrivskill As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddcarcare As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddresponserat As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddpunctuality As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddaccident As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddtrun As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddmachineknow As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddbehaviuor As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddoverall As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtRefname As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtrefadd As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtrefphone As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtrefinfo As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtreligion As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtbelongs As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtlang As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnReset As System.Web.UI.WebControls.Button
    Protected WithEvents pnl2ndform As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btnproceed.Attributes("onClick") = "return checkbeforenext();"
        btnSubmit.Attributes("onClick") = "return checkvalue();"
        If Not Page.IsPostBack Then
            populateddl()
            Dim dtrreader As SqlDataReader
            Dim accessdata As clsutility
            accessdata = New clsutility
            dtrreader = accessdata.funcGetSQLDataReader("select * from CORIntChauffeurMaster where  chauffeurid=" & Request.QueryString("id") & " ")
            dtrreader.Read()
            txtfname.Text = dtrreader("fname") & ""
            txtmidname.Text = dtrreader("mname") & ""
            txtlname.Text = dtrreader("lname") & ""
            txtadd.Text = dtrreader("address") & ""
            txtphone.Text = dtrreader("phone") & ""
            txtMobile.Text = dtrreader("mobile") & ""
            ddbloofgrp.Items.FindByValue(dtrreader("bloodgroup")).Selected = True
            txtdob.Text = dtrreader("dob") & "" 'Convert.ToDateTime(dtrreader("dob")).ToString("dd-MM-yyyy") & ""
            txtdoj.Text = dtrreader("joindate") & "" 'Convert.ToDateTime(dtrreader("joindate")).ToString("dd-MM-yyyy") & ""
            txtlicence.Text = dtrreader("licenceno") & ""
            autoselec_ddl(ddlpaceissue, dtrreader("issuecityid"))
            TextlivValidfrom.Text = dtrreader("licencefrom") & "" 'Convert.ToDateTime(dtrreader("licencefrom")).ToString("dd-MM-yyyy") & ""
            txtlicvalidto.Text = dtrreader("licenceto") & "" 'Convert.ToDateTime(dtrreader("licenceto")).ToString("dd-MM-yyyy") & ""
            txtpmtadd.Text = dtrreader("permanentadd") & ""
            txtesi.Text = dtrreader("esino") & ""
            txtbadge.Text = dtrreader("badgeno") & ""
            autoselec_ddl(ddAttendrat, dtrreader("attendance"))
            autoselec_ddl(ddAtt, dtrreader("attitude"))
            autoselec_ddl(dddisplin, dtrreader("discipline"))
            autoselec_ddl(dduniform, dtrreader("uniform"))
            autoselec_ddl(dddrivskill, dtrreader("drivingskills"))
            autoselec_ddl(ddcarcare, dtrreader("carcare"))
            autoselec_ddl(ddresponserat, dtrreader("clientresponse"))
            autoselec_ddl(ddpunctuality, dtrreader("punctuality"))
            autoselec_ddl(ddaccident, dtrreader("accident"))
            autoselec_ddl(ddtrun, dtrreader("turnout"))
            autoselec_ddl(ddmachineknow, dtrreader("machineknowhow"))
            autoselec_ddl(ddbehaviuor, dtrreader("behaviour"))
            autoselec_ddl(ddoverall, dtrreader("overallperformance"))
            txtRefname.Text = dtrreader("refname") & ""
            txtrefadd.Text = dtrreader("refaddress") & ""
            txtrefphone.Text = dtrreader("refph") & ""
            txtrefinfo.Text = dtrreader("refinfo") & ""
            txtpf.Text = dtrreader("pfno") & ""
            txtac.Text = dtrreader("acno") & ""
            txtbankname.Text = dtrreader("bankname") & ""
            txtreligion.Text = dtrreader("religion") & ""
            txtbelongs.Text = dtrreader("belongregion") & ""
            txtlang.Text = dtrreader("languageknown") & ""
            txtbasic.Text = dtrreader("salbasic") & ""
            txtnighthalt.Text = dtrreader("salnighthalt") & ""
            txtovernight.Text = dtrreader("salovertimehr") & ""
            txtCarwash.Text = dtrreader("salcarwash") & ""
            txtoutstation.Text = dtrreader("saloutstation") & ""
            txthra.Text = dtrreader("salhra") & ""
            txtuniform.Text = dtrreader("saluniform") & ""
            txtcon.Text = dtrreader("salconveyance") & ""
            txtothers.Text = dtrreader("salothers") & ""
            txttotsal.Text = dtrreader("saltotal") & ""
            autoselec_ddl(ddlunit, dtrreader("unitid"))
            txtarearemarks.Text = dtrreader("remarks") & ""
            chkActive.Checked = dtrreader("active")
            dtrreader.Close()
            accessdata.Dispose()

        End If
    End Sub
    Sub populateddl()
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility

        ddlunit.DataSource = objAcessdata.funcGetSQLDataReader("select unitname,unitid from CORIntUnitMaster  where active=1 order by unitname")
        ddlunit.DataValueField = "unitid"
        ddlunit.DataTextField = "unitname"
        ddlunit.DataBind()
        ddlunit.Items.Insert(0, New ListItem("", ""))

        ddlpaceissue.DataSource = objAcessdata.funcGetSQLDataReader("select cityname,cityid from CORIntCityMaster  where active=1 order by cityname")
        ddlpaceissue.DataValueField = "cityid"
        ddlpaceissue.DataTextField = "cityname"
        ddlpaceissue.DataBind()
        ddlpaceissue.Items.Insert(0, New ListItem("", ""))

        objAcessdata.funcpopulatenumddw(10, 1, ddAttendrat)
        objAcessdata.funcpopulatenumddw(10, 1, ddAtt)
        objAcessdata.funcpopulatenumddw(10, 1, dddisplin)
        objAcessdata.funcpopulatenumddw(10, 1, dduniform)
        objAcessdata.funcpopulatenumddw(10, 1, dddrivskill)
        objAcessdata.funcpopulatenumddw(10, 1, ddcarcare)
        objAcessdata.funcpopulatenumddw(10, 1, ddresponserat)
        objAcessdata.funcpopulatenumddw(10, 1, ddpunctuality)
        objAcessdata.funcpopulatenumddw(10, 1, ddaccident)
        objAcessdata.funcpopulatenumddw(10, 1, ddtrun)
        objAcessdata.funcpopulatenumddw(10, 1, ddmachineknow)
        objAcessdata.funcpopulatenumddw(10, 1, ddbehaviuor)
        objAcessdata.funcpopulatenumddw(10, 1, ddoverall)

        objAcessdata.Dispose()
    End Sub

    Function autoselec_ddl(ByVal ddlname As DropDownList, ByVal selectvalue As Int32)
        ddlname.Items.FindByValue(selectvalue).Selected = True
    End Function

    Private Sub btnproceed_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnproceed.Click
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        Dim intuniqcheck As SqlParameter
        Dim intuniqvalue as int32
        'execueting the stored procedure for checking the user login validity by using the output parameter
        cmd = New SqlCommand("procCheckChaufLisenNo", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@rowid", Request.QueryString("id"))
        cmd.Parameters.AddWithValue("@active", get_YNvalue(chkActive))
        cmd.Parameters.AddWithValue("@licenceno", txtlicence.Text)
        intuniqcheck = cmd.Parameters.AddWithValue("@uniqcheckval", SqlDbType.Int)
        intuniqcheck.Direction = ParameterDirection.Output
        MyConnection.Open()
        cmd.ExecuteNonQuery()
        'getting the value to know that whethre the user is vallid user for login or not
        intuniqvalue=cmd.Parameters("@uniqcheckval").Value
        MyConnection.Close()

        if not intuniqvalue = 0 then
                lblErrorMsg.visible=true
                lblErrorMsg.text="Licence# already exist."
                exit sub
        else
            pnlmainform.Visible = False
            pnl2ndform.Visible = True
        end if
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        Dim intuniqcheck As SqlParameter
        Dim intuniqvalue as int32

        cmd = New SqlCommand("procEditChauffeurMaster", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.AddWithValue("@rowid", Request.QueryString("id"))
        cmd.Parameters.AddWithValue("@fname", txtfname.Text)
        cmd.Parameters.AddWithValue("@mname", txtmidname.Text)
        cmd.Parameters.AddWithValue("@lname", txtlname.Text)
        cmd.Parameters.AddWithValue("@unitid", ddlunit.SelectedItem.Value)
        cmd.Parameters.AddWithValue("@address", txtadd.Text)
        cmd.Parameters.AddWithValue("@phone", txtphone.Text)
        cmd.Parameters.AddWithValue("@mobile", txtMobile.Text)
        cmd.Parameters.AddWithValue("@bloodgroup", ddbloofgrp.SelectedItem.Value)
        cmd.Parameters.AddWithValue("@dob", txtdob.Text)
        cmd.Parameters.AddWithValue("@joindate", txtdoj.Text)
        cmd.Parameters.AddWithValue("@licenceno", txtlicence.Text)
        cmd.Parameters.AddWithValue("@issuecityid", ddlpaceissue.SelectedItem.Value)
        cmd.Parameters.AddWithValue("@licencefrom", TextlivValidfrom.Text)
        cmd.Parameters.AddWithValue("@licenceto", txtlicvalidto.Text)
        cmd.Parameters.AddWithValue("@permanentadd", txtpmtadd.Text)
        cmd.Parameters.AddWithValue("@esino", txtesi.Text)
        cmd.Parameters.AddWithValue("@badgeno", txtbadge.Text)
        cmd.Parameters.AddWithValue("@attendance", ddAttendrat.SelectedItem.Value)
        cmd.Parameters.AddWithValue("@attitude", ddAtt.SelectedItem.Value)
        cmd.Parameters.AddWithValue("@discipline", dddisplin.SelectedItem.Value)
        cmd.Parameters.AddWithValue("@uniform", dduniform.SelectedItem.Value)
        cmd.Parameters.AddWithValue("@drivingskills", dddrivskill.SelectedItem.Value)
        cmd.Parameters.AddWithValue("@carcare", ddcarcare.SelectedItem.Value)
        cmd.Parameters.AddWithValue("@clientresponse", ddresponserat.SelectedItem.Value)
        cmd.Parameters.AddWithValue("@punctuality", ddresponserat.SelectedItem.Value)
        cmd.Parameters.AddWithValue("@accident", ddaccident.SelectedItem.Value)
        cmd.Parameters.AddWithValue("@turnout", ddtrun.SelectedItem.Value)
        cmd.Parameters.AddWithValue("@machineknowhow", ddmachineknow.SelectedItem.Value)
        cmd.Parameters.AddWithValue("@behaviour", ddbehaviuor.SelectedItem.Value)
        cmd.Parameters.AddWithValue("@overallperformance", ddoverall.SelectedItem.Value)
        cmd.Parameters.AddWithValue("@refname", txtRefname.Text)
        cmd.Parameters.AddWithValue("@refaddress", txtrefadd.Text)
        cmd.Parameters.AddWithValue("@refph", txtrefphone.Text)
        cmd.Parameters.AddWithValue("@refinfo", txtrefinfo.Text)
        cmd.Parameters.AddWithValue("@pfno", txtpf.Text)
        cmd.Parameters.AddWithValue("@acno", txtac.Text)
        cmd.Parameters.AddWithValue("@bankname", txtbankname.Text)
        cmd.Parameters.AddWithValue("@religion", txtreligion.Text)
        cmd.Parameters.AddWithValue("@belongregion", txtbelongs.Text)
        cmd.Parameters.AddWithValue("@languageknown", txtlang.Text)
        cmd.Parameters.AddWithValue("@salbasic", txtbasic.Text)
        cmd.Parameters.AddWithValue("@salnighthalt", txtnighthalt.Text)
        cmd.Parameters.AddWithValue("@salovertimehr", txtovernight.Text)
        cmd.Parameters.AddWithValue("@salcarwash", txtCarwash.Text)
        cmd.Parameters.AddWithValue("@saloutstation", txtoutstation.Text)
        cmd.Parameters.AddWithValue("@salhra", txthra.Text)
        cmd.Parameters.AddWithValue("@saluniform", txtuniform.Text)
        cmd.Parameters.AddWithValue("@salconveyance", txtcon.Text)
        cmd.Parameters.AddWithValue("@salothers", txtothers.Text)
        cmd.Parameters.AddWithValue("@saltotal", txttotsal.Text)
        cmd.Parameters.AddWithValue("@remarks", txtarearemarks.Text)
        cmd.Parameters.AddWithValue("@active", get_YNvalue(chkActive))
        cmd.Parameters.AddWithValue("@modifiedby", Session("loggedin_user"))


        '  Try
        MyConnection.Open()
        cmd.ExecuteNonQuery()
        MyConnection.Close()
        pnlmainform.Visible = False
        pnl2ndform.Visible = False
        pnlconfirmation.Visible = True

        lblMessage.Text = "You have updated the Chauffeur successfully"
        hyplnkretry.Text = "Edit another Chauffeur"
        hyplnkretry.NavigateUrl = "ChauffeurEditSearch.aspx"
        '   Catch
        '   End Try
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("ChauffeurEditForm.aspx?ID=" & Request.QueryString("id"))
    End Sub
End Class
