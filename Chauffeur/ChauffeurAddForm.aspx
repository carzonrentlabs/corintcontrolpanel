<%@ Page Language="vb" AutoEventWireup="false" Src="ChauffeurAddForm.aspx.vb" Inherits="ChauffeurAddForm" %>

<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>CarzonRent :: Internal software control panel</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

    <script language="JavaScript" src="../utilityfunction.js"></script>
    <script language="JavaScript" src="../JScripts/Datefunc.js"></script>
    <link href="../HertzInt.css" type="text/css" rel="stylesheet">
    <script language="JavaScript">

        $(document).ready(function () {
            $("#<%=txtdoj.ClientID %>").datepicker({ dateFormat: 'mm/dd/yy' });
            $("#<%=TextlivValidfrom.ClientID%>").datepicker({ dateFormat: 'mm/dd/yy' });
            $("#<%=txtlicvalidto.ClientID%>").datepicker({ dateFormat: 'mm/dd/yy' });
            $("#<%=txtdob.ClientID%>").datepicker({ dateFormat: 'mm/dd/yy' });

		    });

		    function checkbeforenext() {

		        if (document.forms[0].txtfname.value == "") {
		            alert("Please make sure all the fields marked with * are filled in.")
		            return false;
		        }
		        if (document.forms[0].txtlname.value == "") {
		            alert("Please make sure all the fields marked with * are filled in.")
		            return false;
		        }
		        if (document.forms[0].ddlunit.value == "") {
		            alert("Please make sure all the fields marked with * are filled in.")
		            return false;
		        }
		        if (document.forms[0].txtphone.value == "") {
		            alert("Please make sure all the fields marked with * are filled in.")
		            return false;
		        }

		        if (document.forms[0].txtMobile.value == "") {
		            alert("Please make sure all the fields marked with * are filled in.")
		            return false;
		        }

		        if (document.forms[0].txtdoj.value == "") {
		            alert("Please make sure all the fields marked with * are filled in.")
		            return false;
		        }
		        if (document.forms[0].txtlicence.value == "") {
		            alert("Please make sure all the fields marked with * are filled in.")
		            return false;
		        }
		        if (document.forms[0].ddlpaceissue.value == "") {
		            alert("Please make sure all the fields marked with * are filled in.")
		            return false;
		        }

		        if (document.forms[0].TextlivValidfrom.value == "") {
		            alert("Please make sure all the fields marked with * are filled in.")
		            return false;
		        }

		        if (document.forms[0].txtlicvalidto.value == "") {
		            alert("Please make sure all the fields marked with * are filled in.")
		            return false;
		        }


		        if (document.forms[0].txtbasic.value == "") {
		            alert("Please make sure all the fields marked with * are filled in.")
		            return false;
		        }
		        else {
		            if (isNaN(document.forms[0].txtbasic.value)) {
		                alert("Basic Salary should be numeric and upto two decimal digits only.")
		                return false;
		            }
		            else {
		                if (parseFloat(document.forms[0].txtbasic.value.indexOf(".")) > 0) {
		                    var FlotVal
		                    FlotVal = document.forms[0].txtbasic.value.substr(parseFloat(document.forms[0].txtbasic.value.indexOf(".")), document.forms[0].txtbasic.value.length)
		                    if (parseFloat((FlotVal.length) - 1) > 2) {
		                        alert("Basic Salary should be numeric and upto two decimal digits only")
		                        return false;
		                    }

		                }
		            }

		        }

		        if (document.forms[0].txtnighthalt.value == "") {
		            alert("Please make sure all the fields marked with * are filled in.")
		            return false;
		        }
		        else {
		            if (isNaN(document.forms[0].txtnighthalt.value)) {
		                alert("Night Halt Charges should be numeric and upto two decimal digits only.")
		                return false;
		            }
		            else {
		                if (parseFloat(document.forms[0].txtnighthalt.value.indexOf(".")) > 0) {
		                    var FlotVal
		                    FlotVal = document.forms[0].txtnighthalt.value.substr(parseFloat(document.forms[0].txtnighthalt.value.indexOf(".")), document.forms[0].txtnighthalt.value.length)
		                    if (parseFloat((FlotVal.length) - 1) > 2) {
		                        alert("Night Halt Charges  should be numeric and upto two decimal digits only")
		                        return false;
		                    }

		                }
		            }

		        }


		        if (document.forms[0].txtovernight.value == "") {
		            alert("Please make sure all the fields marked with * are filled in.")
		            return false;
		        }
		        else {
		            if (isNaN(document.forms[0].txtovernight.value)) {
		                alert("Overtime Charge/Hr should be numeric and upto two decimal digits only.")
		                return false;
		            }
		            else {
		                if (parseFloat(document.forms[0].txtovernight.value.indexOf(".")) > 0) {
		                    var FlotVal
		                    FlotVal = document.forms[0].txtovernight.value.substr(parseFloat(document.forms[0].txtovernight.value.indexOf(".")), document.forms[0].txtovernight.value.length)
		                    if (parseFloat((FlotVal.length) - 1) > 2) {
		                        alert("Overtime Charge/Hr should be numeric and upto two decimal digits only")
		                        return false;
		                    }

		                }
		            }

		        }

		        if (document.forms[0].txtCarwash.value == "") {
		            alert("Please make sure all the fields marked with * are filled in.")
		            return false;
		        }
		        else {
		            if (isNaN(document.forms[0].txtCarwash.value)) {
		                alert("Car Wash Allowance should be numeric and upto two decimal digits only.")
		                return false;
		            }
		            else {
		                if (parseFloat(document.forms[0].txtCarwash.value.indexOf(".")) > 0) {
		                    var FlotVal
		                    FlotVal = document.forms[0].txtCarwash.value.substr(parseFloat(document.forms[0].txtCarwash.value.indexOf(".")), document.forms[0].txtCarwash.value.length)
		                    if (parseFloat((FlotVal.length) - 1) > 2) {
		                        alert("Car Wash Allowance should be numeric and upto two decimal digits only")
		                        return false;
		                    }

		                }
		            }

		        }


		        if (document.forms[0].txtoutstation.value == "") {
		            alert("Please make sure all the fields marked with * are filled in.")
		            return false;
		        }
		        else {
		            if (isNaN(document.forms[0].txtoutstation.value)) {
		                alert("Outstation Allowance should be numeric and upto two decimal digits only.")
		                return false;
		            }
		            else {
		                if (parseFloat(document.forms[0].txtoutstation.value.indexOf(".")) > 0) {
		                    var FlotVal
		                    FlotVal = document.forms[0].txtoutstation.value.substr(parseFloat(document.forms[0].txtoutstation.value.indexOf(".")), document.forms[0].txtoutstation.value.length)
		                    if (parseFloat((FlotVal.length) - 1) > 2) {
		                        alert("Outstation Allowance should be numeric and upto two decimal digits only")
		                        return false;
		                    }

		                }
		            }

		        }

		        if (document.forms[0].txthra.value == "") {
		            alert("Please make sure all the fields marked with * are filled in.")
		            return false;
		        }
		        else {
		            if (isNaN(document.forms[0].txthra.value)) {
		                alert("HRA should be numeric and upto two decimal digits only.")
		                return false;
		            }
		            else {
		                if (parseFloat(document.forms[0].txthra.value.indexOf(".")) > 0) {
		                    var FlotVal
		                    FlotVal = document.forms[0].txthra.value.substr(parseFloat(document.forms[0].txthra.value.indexOf(".")), document.forms[0].txthra.value.length)
		                    if (parseFloat((FlotVal.length) - 1) > 2) {
		                        alert("HRA should be numeric and upto two decimal digits only")
		                        return false;
		                    }

		                }
		            }

		        }

		        if (document.forms[0].txtuniform.value == "") {
		            alert("Please make sure all the fields marked with * are filled in.")
		            return false;
		        }
		        else {
		            if (isNaN(document.forms[0].txtuniform.value)) {
		                alert("Uniform Allowance should be numeric and upto two decimal digits only.")
		                return false;
		            }
		            else {
		                if (parseFloat(document.forms[0].txtuniform.value.indexOf(".")) > 0) {
		                    var FlotVal
		                    FlotVal = document.forms[0].txtuniform.value.substr(parseFloat(document.forms[0].txtuniform.value.indexOf(".")), document.forms[0].txtuniform.value.length)
		                    if (parseFloat((FlotVal.length) - 1) > 2) {
		                        alert("Uniform Allowance should be numeric and upto two decimal digits only")
		                        return false;
		                    }

		                }
		            }

		        }

		        if (document.forms[0].txtcon.value == "") {
		            alert("Please make sure all the fields marked with * are filled in.")
		            return false;
		        }
		        else {
		            if (isNaN(document.forms[0].txtcon.value)) {
		                alert("Conveyance should be numeric and upto two decimal digits only.")
		                return false;
		            }
		            else {
		                if (parseFloat(document.forms[0].txtcon.value.indexOf(".")) > 0) {
		                    var FlotVal
		                    FlotVal = document.forms[0].txtcon.value.substr(parseFloat(document.forms[0].txtcon.value.indexOf(".")), document.forms[0].txtcon.value.length)
		                    if (parseFloat((FlotVal.length) - 1) > 2) {
		                        alert("Conveyance be numeric and upto two decimal digits only")
		                        return false;
		                    }

		                }

		            }

		        }
		        if (document.forms[0].txtothers.value == "") {
		            alert("Please make sure all the fields marked with * are filled in.")
		            return false;
		        }
		        else {
		            if (isNaN(document.forms[0].txtothers.value)) {
		                alert("Others should be numeric and upto two decimal digits only.")
		                return false;
		            }
		            else {
		                if (parseFloat(document.forms[0].txtothers.value.indexOf(".")) > 0) {
		                    var FlotVal
		                    FlotVal = document.forms[0].txtothers.value.substr(parseFloat(document.forms[0].txtothers.value.indexOf(".")), document.forms[0].txtothers.value.length)
		                    if (parseFloat((FlotVal.length) - 1) > 2) {
		                        alert("Others should be numeric and upto two decimal digits only")
		                        return false;
		                    }

		                }
		            }

		        }
		        if (document.forms[0].txttotsal.value == "") {
		            alert("Please make sure all the fields marked with * are filled in.")
		            return false;
		        }
		        else {
		            if (isNaN(document.forms[0].txttotsal.value)) {
		                alert("Total Salary should be numeric and upto two decimal digits only.")
		                return false;
		            }
		            else {
		                if (parseFloat(document.forms[0].txttotsal.value.indexOf(".")) > 0) {
		                    var FlotVal
		                    FlotVal = document.forms[0].txttotsal.value.substr(parseFloat(document.forms[0].txttotsal.value.indexOf(".")), document.forms[0].txttotsal.value.length)
		                    if (parseFloat((FlotVal.length) - 1) > 2) {
		                        alert("Total Salary should be numeric and upto two decimal digits only")
		                        return false;
		                    }

		                }

		            }

		        }


		    }

		    function CheckDecimal(objname, textboxname) {
		        if (parseFloat(objname.value.indexOf(".")) > 0) {
		            var FlotVal
		            FlotVal = objname.value.substr(parseFloat(objname.value.indexOf(".")), objname.value.length)
		            if (parseFloat((FlotVal.length) - 1) > 2) {
		                alert(textboxname + " should be numeric and upto two decimal digits only")
		                return false;
		            }

		        }
		    }

		    function checkvalue() {
		        var strvalues
		        strvalues = ('txtadd,txtdob,txtpmtadd,ddAttendrat,ddAtt,dddisplin,dduniform,dddrivskill,ddcarcare,ddresponserat,ddpunctuality,ddaccident,ddtrun,ddmachineknow,ddbehaviuor,ddoverall')
		        return checkmandatory(strvalues);
		    }
		    function dateReg(obj) {
		        if (obj.value != "") {
		            // alert(obj.value);
		            var reg = /((0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[0-1])-(201[0-9]))/
		            if (reg.test(obj.value)) {
		                //alert('valid');
		            }
		            else {
		                alert('notvalid');
		                obj.value = "";
		            }
		        }
		    }

    </script>
</head>
<body ms_positioning="GridLayout">
    <form id="Form1" name="Form1" method="post" runat="server">
        <uc1:Headerctrl ID="Headerctrl1" runat="server"></uc1:Headerctrl>
        <table align="center">

            <asp:Panel ID="pnlmainform" runat="server">
                <tbody>
                    <tr>
                        <td align="center" colspan="2"><b><u>Add a Chauffeur � Page 1</u></b>
                            <br>
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <asp:Label ID="lblErrorMsg" CssClass="subRedHead" Visible="false" runat="server"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>* First Name
                        </td>
                        <td>
                            <asp:TextBox ID="txtfname" runat="server" CssClass="input" MaxLength="50"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>Middle Name</td>
                        <td>
                            <asp:TextBox ID="txtmidname" runat="server" CssClass="input" MaxLength="50"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>* Last Name</td>
                        <td>
                            <asp:TextBox ID="txtlname" runat="server" CssClass="input" MaxLength="50"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>* Unit</td>
                        <td>
                            <asp:DropDownList ID="ddlunit" runat="server" CssClass="input"></asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td>* Phone</td>
                        <td>
                            <asp:TextBox ID="txtphone" runat="server" CssClass="input" MaxLength="250"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>* Mobile #</td>
                        <td>
                            <asp:TextBox ID="txtMobile" runat="server" CssClass="input" MaxLength="20"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>* Date of Joining</td>
                        <td>
                            <asp:TextBox ID="txtdoj" runat="server" CssClass="input"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>* Licence #</td>
                        <td>
                            <asp:TextBox ID="txtlicence" runat="server" CssClass="input" MaxLength="20"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>* Place of issue</td>
                        <td>
                            <asp:DropDownList ID="ddlpaceissue" runat="server"></asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td>* Licence Valid From</td>
                        <td>
                            <asp:TextBox ID="TextlivValidfrom" runat="server" CssClass="input"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>* Licence Valid To</td>
                        <td>
                            <asp:TextBox ID="txtlicvalidto" runat="server" CssClass="input"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>ESI #</td>
                        <td>
                            <asp:TextBox ID="txtesi" runat="server" CssClass="input" MaxLength="20"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>Badge #</td>
                        <td>
                            <asp:TextBox ID="txtbadge" runat="server" CssClass="input" MaxLength="20"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>PF #</td>
                        <td>
                            <asp:TextBox ID="txtpf" runat="server" CssClass="input" MaxLength="20"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>A/C #</td>
                        <td>
                            <asp:TextBox ID="txtac" runat="server" CssClass="input" MaxLength="20"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>Bank Name</td>
                        <td>
                            <asp:TextBox ID="txtbankname" runat="server" CssClass="input" MaxLength="250"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>* Basic Salary</td>
                        <td>
                            <asp:TextBox ID="txtbasic" runat="server" CssClass="input"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>* Night Halt Charges</td>
                        <td>
                            <asp:TextBox ID="txtnighthalt" runat="server" CssClass="input"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>* Overtime Charge/Hr</td>
                        <td>
                            <asp:TextBox ID="txtovernight" runat="server" CssClass="input"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>* Car Wash Allowance</td>
                        <td>
                            <asp:TextBox ID="txtCarwash" runat="server" CssClass="input"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>* Outstation Allowance</td>
                        <td>
                            <asp:TextBox ID="txtoutstation" runat="server" CssClass="input"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>* HRA</td>
                        <td>
                            <asp:TextBox ID="txthra" runat="server" CssClass="input"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>* Uniform Allowance</td>
                        <td>
                            <asp:TextBox ID="txtuniform" runat="server" CssClass="input"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>* Conveyance</td>
                        <td>
                            <asp:TextBox ID="txtcon" runat="server" CssClass="input"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>* Others</td>
                        <td>
                            <asp:TextBox ID="txtothers" runat="server" CssClass="input"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>* Total Salary</td>
                        <td>
                            <asp:TextBox ID="txttotsal" runat="server" CssClass="input"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td valign="top">Remarks(<span class="shwText" id="shwMessage">0</span>/2000 chars)</td>
                        <td>
                            <asp:TextBox ID="txtarearemarks" onkeydown="showLength(this.form.txtarearemarks,'document.all.shwMessage',2000)"
                                onkeyup="showLength(this.form.txtarearemarks,'document.all.shwMessage',2000)" runat="server"
                                CssClass="input" MaxLength="2000" TextMode="MultiLine" Columns="50" Rows="3"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>Active</td>
                        <td>
                            <asp:CheckBox ID="chkActive" runat="server" CssClass="input" Checked="True"></asp:CheckBox></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <asp:Button ID="btnproceed" runat="server" CssClass="input" Text="Next > >"></asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
            </asp:Panel>
            <asp:Panel ID="pnl2ndform" runat="server" Visible="False">
                <tr>
                    <td align="center" colspan="2"><b><u>Add a Chauffeur � Page 2</u></b>
                        <br>
                        <br>
                    </td>
                </tr>
                <tr>
                    <td>* Residence Address</td>
                    <td>
                        <asp:TextBox ID="txtadd" runat="server" CssClass="input" MaxLength="250"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>Blood Group</td>
                    <td>
                        <asp:DropDownList ID="ddbloofgrp" runat="server" CssClass="input">
                            <asp:ListItem Text="" Selected="True" Value="" />
                            <asp:ListItem Text="A+" Value="A+" />
                            <asp:ListItem Text="A-" Value="A-" />
                            <asp:ListItem Text="B+" Value="B+" />
                            <asp:ListItem Text="B-" Value="B-" />
                            <asp:ListItem Text="AB+" Value="AB+" />
                            <asp:ListItem Text="AB-" Value="AB-" />
                            <asp:ListItem Text="O+" Value="O+" />
                            <asp:ListItem Text="O-" Value="O-" />
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td>* Date of Birth</td>
                    <td>
                        <asp:TextBox ID="txtdob" runat="server" CssClass="input" MaxLength="10" size="12"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>* Permanent Address</td>
                    <td>
                        <asp:TextBox ID="txtpmtadd" runat="server" CssClass="input" MaxLength="250"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>* Attendance Rating</td>
                    <td>
                        <asp:DropDownList ID="ddAttendrat" runat="server" CssClass="input"></asp:DropDownList></td>
                </tr>
                <tr>
                    <td>* Attitude Rating</td>
                    <td>
                        <asp:DropDownList ID="ddAtt" runat="server" CssClass="input"></asp:DropDownList></td>
                </tr>
                <tr>
                    <td>* Discipline Rating</td>
                    <td>
                        <asp:DropDownList ID="dddisplin" runat="server" CssClass="input"></asp:DropDownList></td>
                </tr>
                <tr>
                    <td>* Uniform Rating</td>
                    <td>
                        <asp:DropDownList ID="dduniform" runat="server" CssClass="input"></asp:DropDownList></td>
                </tr>
                <tr>
                    <td>* Driving Skills Rating</td>
                    <td>
                        <asp:DropDownList ID="dddrivskill" runat="server" CssClass="input"></asp:DropDownList></td>
                </tr>
                <tr>
                    <td>* Car Care Rating</td>
                    <td>
                        <asp:DropDownList ID="ddcarcare" runat="server" CssClass="input"></asp:DropDownList></td>
                </tr>
                <tr>
                    <td>* Client Response Rating</td>
                    <td>
                        <asp:DropDownList ID="ddresponserat" runat="server" CssClass="input"></asp:DropDownList></td>
                </tr>
                <tr>
                    <td>* Punctuality Rating</td>
                    <td>
                        <asp:DropDownList ID="ddpunctuality" runat="server" CssClass="input"></asp:DropDownList></td>
                </tr>
                <tr>
                    <td>* Accident Rating</td>
                    <td>
                        <asp:DropDownList ID="ddaccident" runat="server" CssClass="input"></asp:DropDownList></td>
                </tr>
                <tr>
                    <td>* Turn Out Rating</td>
                    <td>
                        <asp:DropDownList ID="ddtrun" runat="server" CssClass="input"></asp:DropDownList></td>
                </tr>
                <tr>
                    <td>* Machine Know-How Rating</td>
                    <td>
                        <asp:DropDownList ID="ddmachineknow" runat="server" CssClass="input"></asp:DropDownList></td>
                </tr>
                <tr>
                    <td>* Behaviour Rating</td>
                    <td>
                        <asp:DropDownList ID="ddbehaviuor" runat="server" CssClass="input"></asp:DropDownList></td>
                </tr>
                <tr>
                    <td>* Overall Performance Rating</td>
                    <td>
                        <asp:DropDownList ID="ddoverall" runat="server" CssClass="input"></asp:DropDownList></td>
                </tr>
                <tr>
                    <td>Reference Name</td>
                    <td>
                        <asp:TextBox ID="txtRefname" runat="server" CssClass="input" MaxLength="150"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>Reference Address</td>
                    <td>
                        <asp:TextBox ID="txtrefadd" runat="server" CssClass="input" MaxLength="250"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>Reference Phone</td>
                    <td>
                        <asp:TextBox ID="txtrefphone" runat="server" CssClass="input" MaxLength="20"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>Reference Info</td>
                    <td>
                        <asp:TextBox ID="txtrefinfo" runat="server" CssClass="input" MaxLength="250"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>Religion</td>
                    <td>
                        <asp:TextBox ID="txtreligion" runat="server" CssClass="input" MaxLength="50"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>Belongs to</td>
                    <td>
                        <asp:TextBox ID="txtbelongs" runat="server" CssClass="input" MaxLength="100"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>Languages Known</td>
                    <td>
                        <asp:TextBox ID="txtlang" runat="server" CssClass="input" MaxLength="100"></asp:TextBox></td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <asp:Button ID="btnSubmit" runat="server" CssClass="input" Text="Submit"></asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;
							<asp:Button ID="btnReset" runat="server" CssClass="input" Text="Reset"></asp:Button></td>
                </tr>
            </asp:Panel>
            <asp:Panel ID="pnlconfirmation" runat="server" Visible="False">
                <tr align="center">
                    <td colspan="2">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label></td>
                </tr>
                <tr align="center">
                    <td colspan="2">
                        <asp:HyperLink ID="hyplnkretry" runat="server"></asp:HyperLink></td>
                </tr>
            </asp:Panel>
        </table>
    </form>
</body>
</html>
