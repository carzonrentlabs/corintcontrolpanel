<%@ Page Language="vb" AutoEventWireup="false" Inherits="FacilitatorAddForm" Src="FacilitatorAddForm.aspx.vb" %>

<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>CarzonRent :: Internal software Control panel</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">

    <script language="JavaScript" src="../utilityfunction.js"></script>

    <script language="JavaScript" src="../JScripts/Datefunc.js"></script>

    <link rel="stylesheet" href="../HertzInt.css" type="text/css">

    <script src="../JScripts/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script language="javascript">


        $(document).ready(function () {
            $("#txtph1").keydown(function (event) {
                // Allow only backspace and delete
                if (event.keyCode == 46 || event.keyCode == 8) {
                    // let it happen, don't do anything
                }
                else {
                    // Ensure that it is a number and stop the keypress
                    if (event.keyCode < 48 || event.keyCode > 57) {
                        event.preventDefault();
                    }
                }
            });
        });
        function validatepage() {

            if (isNaN(document.forms[0].txtph1.value)) {
                alert("The Mobile # should be numeric only");
                return false;
            }
            if (document.forms[0].txtph1.value.length != 10) {
                alert("The Mobile # must have 10 digits");
                return false;
            }
            if (document.forms[0].txtemail.value != "") {

                var theStr = document.forms[0].txtemail.value;
                var atIndex = theStr.indexOf('@');
                var dotIndex = theStr.indexOf('.', atIndex);
                theSub = theStr.substring(0, dotIndex + 1)

                if ((atIndex < 1) || (atIndex != theStr.lastIndexOf('@')) || (dotIndex < atIndex + 2) || (theStr.length <= theSub.length)) {
                    alert("Email ID is not a valid Email ID");
                    return false;
                }
            }
            if (document.forms[0].txtEmailID2.value != "") {

                var theStr = document.forms[0].txtEmailID2.value;
                var atIndex = theStr.indexOf('@');
                var dotIndex = theStr.indexOf('.', atIndex);
                theSub = theStr.substring(0, dotIndex + 1)

                if ((atIndex < 1) || (atIndex != theStr.lastIndexOf('@')) || (dotIndex < atIndex + 2) || (theStr.length <= theSub.length)) {
                    //alert("The personal Email ID is not a valid Email ID");
                    //return false;
                }
            }
            var strvalues
            strvalues = ('txtfname,txtlname,ddlcompany,txtph1,txtemail')
            return checkmandatory(strvalues);
        }



    </script>

</head>
<body ms_positioning="GridLayout">
    <form id="Form1" method="post" runat="server">
        <uc1:Headerctrl ID="Headerctrl1" runat="server"></uc1:Headerctrl>
        <table align="center">
            <asp:Panel ID="pnlmainform" runat="server">
                <tbody>
                    <tr>
                        <td align="center" colspan="2">
                            <b><u>Add a Facilitator</u></b>
                            <br>
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <asp:Label ID="lblErrorMsg" CssClass="subRedHead" Visible="false" runat="server"></asp:Label></td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <asp:Label CssClass="subredhead" ID="lblerr" runat="server" Visible="False"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>* First Name</td>
                        <td>
                            <asp:TextBox ID="txtfname" runat="server" CssClass="input" MaxLength="50"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>* Last Name</td>
                        <td>
                            <asp:TextBox ID="txtlname" runat="server" CssClass="input" MaxLength="50"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>* Mobile #</td>
                        <td>
                            <asp:TextBox ID="txtph1" runat="server" MaxLength="10" CssClass="input"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>* Client Company</td>
                        <td>
                            <asp:DropDownList ID="ddlcompany" runat="server" CssClass="input">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td>* Email ID</td>
                        <td>
                            <asp:TextBox ID="txtemail" runat="server" CssClass="input" MaxLength="100"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>2nd Facilitator Email ID</td>
                        <td>
                            <asp:TextBox ID="txtEmailID2" runat="server" MaxLength="100" CssClass="input"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>Active</td>
                        <td>
                            <asp:CheckBox ID="chkActive" runat="server" CssClass="input" Checked="True"></asp:CheckBox></td>
                    </tr>
                    <tr>
                        <td>Facilitator MandateYN</td>
                        <td>
                            <asp:CheckBox ID="chkMandateFacilitator" runat="server" CssClass="input" Checked="True"></asp:CheckBox></td>
                    </tr>

                    <tr>
                        <td>Send SMS At Dispatch</td>
                        <td>
                            <asp:CheckBox ID="chkDispatch" runat="server" CssClass="input" Checked="false"></asp:CheckBox></td>
                    </tr>
                    <tr>
                        <td>Can Manage Facilitators</td>
                        <td>
                            <asp:CheckBox ID="chkCanCreateFacilitator" runat="server" CssClass="input" Checked="false"></asp:CheckBox></td>
                    </tr>
                    <tr>
                        <td>Is Company Wide Access Enabled</td>
                        <td>
                            <asp:CheckBox ID="chkIsCompanyWideAccessEnabled" runat="server" CssClass="input" Checked="false"></asp:CheckBox></td>
                    </tr>
                    <tr>
                        <td valign="top">Remarks(<span class="shwText" id="shwMessage">0</span>/2000 chars)</td>
                        <td>
                            <asp:TextBox ID="txtarearemarks" onkeydown="showLength(this.form.txtarearemarks,'document.all.shwMessage',2000)"
                                onkeyup="showLength(this.form.txtarearemarks,'document.all.shwMessage',2000)"
                                runat="server" CssClass="input" MaxLength="2000" TextMode="MultiLine" Columns="50"
                                Rows="3"></asp:TextBox></td>
                    </tr>

                    <tr>
                        <td align="center" colspan="2">
                            <asp:Button ID="btnSubmit" runat="server" CssClass="input" Text="Submit"></asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;
                            <input type="Reset" cssclass="button" name="Reset" value="Reset" /></td>
                    </tr>
            </asp:Panel>
            <asp:Panel ID="pnlconfirmation" runat="server" Visible="False">
                <tr align="center">
                    <td colspan="2">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label></td>
                </tr>
                <tr align="center">
                    <td colspan="2">
                        <asp:HyperLink ID="hyplnkretry" runat="server"></asp:HyperLink></td>
                </tr>
            </asp:Panel>
            </TBODY>
        </table>
    </form>
</body>
</html>
