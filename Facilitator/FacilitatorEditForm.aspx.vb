Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class FacilitatorEditForm
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblerr As System.Web.UI.WebControls.Label
    Protected WithEvents txtfname As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtmname As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtlname As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlcompany As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents txtdesig As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtadd As System.Web.UI.WebControls.TextBox
    'Protected WithEvents hdnClientCoAddID As System.Web.UI.HtmlControls.HtmlInputHidden
    'Protected WithEvents txtSTDCode As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtph1 As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtph As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtResAdd As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtResPhone As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtalternatefname As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtalternatemname As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtalternatelname As System.Web.UI.WebControls.TextBox
    'Protected WithEvents tctalternatecphone As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtfax As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtemail As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtDOB As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtAnniv As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtEmailID2 As System.Web.UI.WebControls.TextBox
    'Protected WithEvents ddlmaried As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtarearemarks As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkActive As System.Web.UI.WebControls.CheckBox
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnReset As System.Web.UI.WebControls.Button
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    Protected WithEvents chkDispatch As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkMandateFacilitator As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkCanCreateFacilitator As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkIsCompanyWideAccessEnabled As System.Web.UI.WebControls.CheckBox
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btnSubmit.Attributes("onClick") = "return  validatepage();"
        If Not Page.IsPostBack Then
            populateddl()
            Dim objAcessdata As clsutility
            objAcessdata = New clsutility
            Dim dtrreader As SqlDataReader
            dtrreader = objAcessdata.funcGetSQLDataReader("select isnull(a.MandateYN,0) as FacilitatorM,isnull(a.CanCreateFacilitator,0)as CanCreateFacilitator,isnull(a.IsCompanyWideAccessEnabled,0) as IsCompanyWideAccessEnabled,a.*, b.ClientCoAddress from CORIntFacilitatorMaster as a, CORIntClientCoAddMaster as b  where a.ClientCoAddID = b.ClientCoAddID and a.facilitatorid =" & Request.QueryString("id") & " ")
            dtrreader.Read()

            txtfname.Text = dtrreader("fname") & ""
            txtlname.Text = dtrreader("lname") & ""
            autoselec_ddl(ddlcompany, dtrreader("clientcoid"))
            txtph1.Text = dtrreader("phone1") & ""
            txtemail.Text = dtrreader("emailid") & ""
            txtEmailID2.Text = dtrreader("EmailID2") & ""
            txtarearemarks.Text = dtrreader("remarks") & ""
            chkActive.Checked = dtrreader("active")
            chkDispatch.Checked = dtrreader("DispatchSMSYN")
            chkMandateFacilitator.Checked = dtrreader("FacilitatorM")
            chkCanCreateFacilitator.Checked = dtrreader("CanCreateFacilitator")
            chkIsCompanyWideAccessEnabled.Checked = dtrreader("IsCompanyWideAccessEnabled")
            dtrreader.Close()
            objAcessdata.Dispose()
        End If

    End Sub
    Function autoselec_ddl(ByVal ddlname As DropDownList, ByVal selectvalue As Int32)
        If Not IsDBNull(selectvalue) Then
            If Not selectvalue = 0 Then
                Try
                    ddlname.Items.FindByValue(selectvalue).Selected = True
                Catch ex As Exception

                End Try

            End If
        End If
    End Function
    Sub populateddl()
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility
        ddlcompany.DataSource = objAcessdata.funcGetSQLDataReader("select ClientCoName  ,ClientCoID   from CORIntClientCoMaster where providerId=" & Session("provider_Id") & "   order by ClientCoName ")
        ddlcompany.DataValueField = "ClientCoID"
        ddlcompany.DataTextField = "ClientCoName"
        ddlcompany.DataBind()
        ddlcompany.Items.Insert(0, New ListItem("", ""))
        objAcessdata.Dispose()
    End Sub
    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        Dim intuniqcheck As SqlParameter
        Dim intuniqvalue as int32

        cmd = New SqlCommand("procEditFacilitator_New", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("@rowid", Request.QueryString("id"))
        cmd.Parameters.Add("@fname", txtfname.Text)
        cmd.Parameters.Add("@lname", txtlname.Text)
        cmd.Parameters.Add("@clientcoid", ddlcompany.SelectedItem.Value)
       
        cmd.Parameters.Add("@phone1", txtph1.Text)
       

        cmd.Parameters.Add("@emailid", txtemail.Text)
       
        cmd.Parameters.Add("@EmailID2", txtEmailID2.Text)
        cmd.Parameters.Add("@active", get_YNvalue(chkActive))
        cmd.Parameters.Add("@DispatchYN", get_YNvalue(chkDispatch))
        cmd.Parameters.Add("@CanCreateFacilitator", get_YNvalue(chkCanCreateFacilitator))
        cmd.Parameters.Add("@IsCompanyWideAccessEnabled", get_YNvalue(chkIsCompanyWideAccessEnabled))

        cmd.Parameters.Add("@remarks", txtarearemarks.Text)

        cmd.Parameters.Add("@modifiedby", Session("loggedin_user"))
        cmd.Parameters.Add("@FacilitatorMandate", get_YNvalue(chkMandateFacilitator))
        intuniqcheck = cmd.Parameters.Add("@uniqcheckval", SqlDbType.Int)
        intuniqcheck.Direction = ParameterDirection.Output


    '    Try
            MyConnection.Open()
            cmd.ExecuteNonQuery()
        intuniqvalue = cmd.Parameters("@uniqcheckval").Value
            MyConnection.Close()
           if not intuniqvalue = 0 then
                lblErrorMsg.visible=true
                lblErrorMsg.text="Emailid already exist."
                exit sub
            else
		            lblErrorMsg.visible=false
                    pnlmainform.Visible = False
                    pnlconfirmation.Visible = True
                    lblMessage.Text = "You have updated the Facilitator successfully"
                    hyplnkretry.Text = "Edit another Facilitator"
                    hyplnkretry.NavigateUrl = "FacilitatorEditSearch.aspx?id=" & Request.QueryString("id")
            end if
     '   Catch
      ''      lblerr.Visible = True
      '      lblerr.Text = "Emailid already exists"
      '  End Try
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("FacilitatorEditForm.aspx?id=" & Request.QueryString("id"))
    End Sub
End Class
