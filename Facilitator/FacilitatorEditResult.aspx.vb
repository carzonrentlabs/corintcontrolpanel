Imports commonutility
Imports System.text
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Imports System.Web.UI
Public Class FacilitatorEditResult
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tblRecDetail As System.Web.UI.WebControls.Table

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not Page.IsPostBack Then
            getvalue("indname")
        End If
    End Sub
    Sub getvalue(ByVal strorderby As String)
        Dim strquery As StringBuilder
        Dim dtrreader As SqlDataReader
        Dim accessdata As clsutility
        accessdata = New clsutility
        strquery = New StringBuilder("select M.facilitatorid,c.clientconame,M.fname+' '+M.lname as indname,M.designation, M.Phone1, M.Phone2, M.emailid,case M.active when '1' then 'Active' when '0' then 'Not Active' end as active, M.facpwd from CORIntFacilitatorMaster M,CORIntClientCoMaster C where C.ClientCoID = M.clientcoid")
        If Request.QueryString("id") <> "-1" Then
            strquery.Append(" and C.ProviderId=" & Session("provider_Id") & " and M.facilitatorid=" & Request.QueryString("id") & " ")
        Else
            strquery.Append(" and C.ProviderId=" & Session("provider_Id") & " ")
        End If
        strquery.Append(" order by " & strorderby & "")

        ' Response.Write(strquery.ToString)
        '  Response.End()
        dtrreader = accessdata.funcGetSQLDataReader(strquery.ToString)
        While dtrreader.Read
            Dim Temprow As New TableRow
            Temprow.HorizontalAlign = HorizontalAlign.Center


            Dim Tempcell As New TableCell
            Tempcell.Controls.Add(New LiteralControl(dtrreader("indname") & ""))
            Temprow.Cells.Add(Tempcell)

            Dim Tempcel2 As New TableCell
            Tempcel2.Controls.Add(New LiteralControl(dtrreader("clientconame") & ""))
            Temprow.Cells.Add(Tempcel2)

            'Dim Tempcel3 As New TableCell
            'Tempcel3.Controls.Add(New LiteralControl(dtrreader("designation") & ""))
            'Temprow.Cells.Add(Tempcel3)

            Dim Tempcel7 As New TableCell
            Tempcel7.Controls.Add(New LiteralControl(dtrreader("Phone1") & ""))
            Temprow.Cells.Add(Tempcel7)

            'Dim Tempcel8 As New TableCell
            'Tempcel8.Controls.Add(New LiteralControl(dtrreader("Phone2") & ""))
            'Temprow.Cells.Add(Tempcel8)

            Dim Tempcel4 As New TableCell
            Tempcel4.Controls.Add(New LiteralControl(dtrreader("emailid") & ""))
            Temprow.Cells.Add(Tempcel4)

            Dim Tempcel5 As New TableCell
            Tempcel5.Controls.Add(New LiteralControl(dtrreader("active") & ""))
            Temprow.Cells.Add(Tempcel5)

            Dim Tempcel9 As New TableCell
            Tempcel9.Controls.Add(New LiteralControl(dtrreader("facpwd") & ""))
            Temprow.Cells.Add(Tempcel9)

            Dim Tempcel6 As New TableCell
            Tempcel6.Controls.Add(New LiteralControl("<a href=FacilitatorEditForm.aspx?ID=" & dtrreader("facilitatorid") & " >Edit</a>"))
            Temprow.Cells.Add(Tempcel6)
            'adding the Tables rows to the table
            tblRecDetail.Rows.Add(Temprow)

        End While
        dtrreader.Close()
    End Sub
    Sub SortGird(ByVal sender As Object, ByVal e As System.EventArgs)
        Select Case sender.id
            Case "Linkbutton1"
                getvalue("indname")
            Case "Linkbutton2"
                getvalue("c.clientconame")
            Case "Linkbutton3"
                getvalue("designation")
            Case "Linkbutton7"
                getvalue("M.Phone1")
            Case "Linkbutton8"
                getvalue("M.Phone2")
            Case "Linkbutton4"
                getvalue("M.emailid")
            Case "Linkbutton5"
                getvalue("M.active")
        End Select

    End Sub

End Class
