<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" Src="FacilitatorEditResult.aspx.vb"
    Inherits="FacilitatorEditResult" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>CarzonRent :: Internal software Control panel</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <link href="../HertzInt.css" type="text/css" rel="stylesheet">
</head>
<body ms_positioning="GridLayout">
    <form id="Form1" method="post" runat="server">
    <uc1:Headerctrl ID="Headerctrl1" runat="server"></uc1:Headerctrl>
    <table align="center">
        <tbody>
            <tr>
                <td align="center">
                    <strong><u>Edit a Client Company Individual where</u></strong>
                </td>
            </tr>
            <tr>
                <td valign="top">
                    <asp:Table ID="tblRecDetail" HorizontalAlign="Center" runat="server" BorderColor="#cccc99"
                        BorderStyle="Solid" GridLines="both" CellSpacing="0" CellPadding="0">
                        <asp:TableRow>
                            <asp:TableHeaderCell>
                                &nbsp;&nbsp;
                                <asp:LinkButton runat="server" Text="Facilitator Name" OnClick="SortGird" ID="Linkbutton1"></asp:LinkButton>&nbsp;&nbsp;
                            </asp:TableHeaderCell>
                            <asp:TableHeaderCell>
                                &nbsp;&nbsp;
                                <asp:LinkButton runat="server" Text="Company Name" OnClick="SortGird" ID="Linkbutton2"></asp:LinkButton>&nbsp;&nbsp;
                            </asp:TableHeaderCell>
                            <%--<asp:TableHeaderCell>
                                &nbsp;&nbsp;
                                <asp:LinkButton runat="server" Text="Designation" OnClick="SortGird" ID="Linkbutton3"></asp:LinkButton>&nbsp;&nbsp;
                            </asp:TableHeaderCell>--%>
                            <asp:TableHeaderCell>
                                &nbsp;&nbsp;
                                <asp:LinkButton runat="server" Text="Mobile" OnClick="SortGird" ID="Linkbutton7"></asp:LinkButton>&nbsp;&nbsp;
                            </asp:TableHeaderCell>
                            <%--<asp:TableHeaderCell>
                                &nbsp;&nbsp;
                                <asp:LinkButton runat="server" Text="Phone" OnClick="SortGird" ID="Linkbutton8"></asp:LinkButton>&nbsp;&nbsp;
                            </asp:TableHeaderCell>--%>
                            <asp:TableHeaderCell>
                                &nbsp;&nbsp;
                                <asp:LinkButton runat="server" Text="Email ID" OnClick="SortGird" ID="Linkbutton5"></asp:LinkButton>&nbsp;&nbsp;
                            </asp:TableHeaderCell>
                            <asp:TableHeaderCell>
                                &nbsp;&nbsp;
                                <asp:LinkButton runat="server" Text="Active" OnClick="SortGird" ID="Linkbutton4"></asp:LinkButton>&nbsp;&nbsp;
                            </asp:TableHeaderCell>
                            <asp:TableHeaderCell>
                                &nbsp;&nbsp;
                                <asp:LinkButton runat="server" Text="pwd" OnClick="SortGird" ID="Linkbutton9"></asp:LinkButton>&nbsp;&nbsp;
                            </asp:TableHeaderCell>
                            <asp:TableHeaderCell>
									&nbsp;&nbsp;
                            </asp:TableHeaderCell>
                        </asp:TableRow>
                    </asp:Table>
                </td>
            </tr>
        </tbody>
    </table>
    </form>
</body>
</html>
