<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Src="AddressList.aspx.vb" Inherits="AddressList"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CarzonRent :: Internal software Control panel</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
		<script language="JavaScript">
		   function SendAdd(ClientCoAddress, ClientCoAddID)
		   {
		      var ClientCoAddress = ClientCoAddress;
		      var ClientCoAddID = ClientCoAddID;
		      window.opener.document.Form1.txtadd.value = ClientCoAddress;
		      window.opener.document.Form1.hdnClientCoAddID.value = ClientCoAddID;
		      window.close();
		   }
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<table align="center">
				<tr>
					<td align="center"><STRONG><U>Select the address</U></STRONG></td>
				</tr>
				<tr>
					<td valign="top">
						<asp:table ID="tblRecDetail" HorizontalAlign="Center" Runat="server" BorderColor="#cccc99"
							BorderStyle="Solid" GridLines="both" CellSpacing="0" CellPadding="0">
							<asp:tablerow>
								<asp:tableheadercell>
									&nbsp;&nbsp;
<asp:linkbutton runat="server" Text="No. " OnClick="SortGird" ID="Linkbutton1"></asp:linkbutton>&nbsp;&nbsp;
								</asp:tableheadercell>
								<asp:tableheadercell>
									&nbsp;&nbsp;
<asp:linkbutton runat="server" Text="Address" OnClick="SortGird" ID="Linkbutton2"></asp:linkbutton>&nbsp;&nbsp;
								</asp:tableheadercell>
								<asp:tableheadercell>
									&nbsp;&nbsp;
								</asp:tableheadercell>
							</asp:tablerow>
						</asp:table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>