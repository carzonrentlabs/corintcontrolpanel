Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Mail
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class FacilitatorAddForm
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtfname As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtmname As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtlname As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlcompany As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents txtdesig As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtadd As System.Web.UI.WebControls.TextBox
    'Protected WithEvents hdnClientCoAddID As System.Web.UI.HtmlControls.HtmlInputHidden
    'Protected WithEvents txtSTDCode As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtph1 As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtph As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtResAdd As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtResPhone As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtalternatefname As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtalternatemname As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtalternatelname As System.Web.UI.WebControls.TextBox
    'Protected WithEvents tctalternatecphone As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtfax As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtemail As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtDOB As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtAnniv As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtEmailID2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtarearemarks As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkActive As System.Web.UI.WebControls.CheckBox
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    Protected WithEvents ddlmaried As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblerr As System.Web.UI.WebControls.Label

    Protected WithEvents chkDispatch As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkCanCreateFacilitator As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkIsCompanyWideAccessEnabled As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkMandateFacilitator As System.Web.UI.WebControls.CheckBox
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btnSubmit.Attributes("onClick") = "return  validatepage();"
        If Not Page.IsPostBack Then
            populateddl()
        End If
    End Sub

    Sub populateddl()
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility
        ddlcompany.DataSource = objAcessdata.funcGetSQLDataReader("select ClientCoName  ,ClientCoID   from CORIntClientCoMaster  where active=1 and providerId=" & Session("provider_Id") & " order by ClientCoName ")
        ddlcompany.DataValueField = "ClientCoID"
        ddlcompany.DataTextField = "ClientCoName"
        ddlcompany.DataBind()
        ddlcompany.Items.Insert(0, New ListItem("", ""))
        objAcessdata.Dispose()
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        Dim FacPwd As String = RandomString(5, False)

        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand


        Dim intuniqcheck As SqlParameter
        Dim intuniqvalue as int32
        Dim intFacilitatorIDParam As SqlParameter
        Dim intFacilitatorID as int32
        cmd = New SqlCommand("procAddFacilitator_New", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("@fname", txtfname.Text)
        cmd.Parameters.Add("@lname", txtlname.Text)
        cmd.Parameters.Add("@phone1", txtph1.Text)
        cmd.Parameters.Add("@clientcoid", ddlcompany.SelectedItem.Value)
        cmd.Parameters.Add("@emailid", txtemail.Text)
      
        cmd.Parameters.Add("@EmailID2", txtEmailID2.Text)
        cmd.Parameters.Add("@active", get_YNvalue(chkActive))
        cmd.Parameters.Add("@DispatchYN", get_YNvalue(chkDispatch))
        cmd.Parameters.Add("@CanCreateFacilitator", get_YNvalue(chkCanCreateFacilitator)) 'Akhilesh
        cmd.Parameters.Add("@IsCompanyWideAccessEnabled", get_YNvalue(chkIsCompanyWideAccessEnabled)) 'Akhilesh
        cmd.Parameters.Add("@remarks", txtarearemarks.Text)
        cmd.Parameters.Add("@createdby", Session("loggedin_user"))
        cmd.Parameters.Add("@FacilitatorMandate", get_YNvalue(chkMandateFacilitator))
        cmd.Parameters.Add("@FacPwd", FacPwd)
        intuniqcheck = cmd.Parameters.Add("@uniqcheckval", SqlDbType.Int)
        intuniqcheck.Direction = ParameterDirection.Output
        intFacilitatorIDParam = cmd.Parameters.Add("@FacilitatorID", SqlDbType.Int)
        intFacilitatorIDParam.Direction = ParameterDirection.Output


     '   Try
            MyConnection.Open()
            cmd.ExecuteNonQuery()
           intuniqvalue=cmd.Parameters("@uniqcheckval").Value
        If (Not intuniqvalue = 0) Then

        Else
            intFacilitatorID = cmd.Parameters("@FacilitatorID").Value
        End If
            MyConnection.Close()

        If (Not intuniqvalue = 0) Then
            lblErrorMsg.visible = True
            lblErrorMsg.text = "Emailid already exist."
            Exit Sub
        Else
            lblErrorMsg.visible = False
            pnlmainform.Visible = False
            pnlconfirmation.Visible = True

            Dim sremailmessage As System.Text.StringBuilder
            sremailmessage = New System.Text.StringBuilder("Dear " & txtfname.Text & " " & txtlname.Text & ",<br> You have been assigned a unique ID as a facilitator, associated with your company at Carzonrent (I) Pvt Ltd.<br> ")
            sremailmessage.Append("Kindly give this unique ID to the Carzonrent customer service representative while requesting a booking as a facilitator.<br> ")
            sremailmessage.Append("Your unique facilitator ID is: " & intFacilitatorID & " <br> ")
            sremailmessage.Append("You can also make your booking with CarzonRent online, using the following link:<br>https://www.carzonrent.com/CorporateSite/ClientLogin.asp?CoID=" & ddlcompany.SelectedItem.Value & " <br> ")
            sremailmessage.Append("Warm Regards, <br>")
            sremailmessage.Append("Carzonrent (I) Pvt Ltd")


            procSendMail(sremailmessage.ToString, txtemail.Text)

            lblMessage.Text = "You have added the Facilitator successfully"
            hyplnkretry.Text = "Add another Facilitator"
            hyplnkretry.NavigateUrl = "FacilitatorAddForm.aspx"
        End If
        '     Catch
        '         lblerr.Visible = True
        '         lblerr.Text = "Emailid already exists"
        '    End Try
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function
    Function procSendMail(ByVal strMessage As String, ByVal EMailTO As String) As Boolean

        Dim stresult As Boolean

        Dim StrMailBody As String
        StrMailBody = strMessage
        Dim objMail As MailMessage
        objMail = New MailMessage
        objMail.From = "reserve@carzonrent.com"
        objMail.To = EMailTO

        objMail.Subject = "Your unique facilitator ID at Carzonrent"

        objMail.Body = StrMailBody.ToString
        objMail.BodyFormat = MailFormat.Html


        Try
            '    System.Web.Mail.SmtpMail.Send(objMail)
            SmtpMail.SmtpServer = ""
            SmtpMail.Send(objMail)
            stresult = True
        Catch
            stresult = False
        End Try

        Return stresult

    End Function

    Private Function RandomString(ByVal size As Integer, ByVal lowerCase As Boolean) As String
        Dim RandomSpcecialCharacter = String.Empty
        Dim RandomNumber = String.Empty
        Dim RandomCapital = String.Empty
        Dim random As Random = New Random
        'Dim SpcecialCharacter = New List(Of String)() {"@", "#", "$", "+", ")"}
        'Dim Number = New List(Of String)() {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"}
        'Dim Capital = New List(Of String)() {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"}


        '  Dim SpcecialCharacter As String = {"@", "#", "$", "+", ")"}
        '  Dim Number As String = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"}
        ' Dim Capital = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"}

        'Dim SpcecialCharacter(4) As String
        'SpcecialCharacter(0) = "@"
        'SpcecialCharacter(1) = "#"
        'SpcecialCharacter(2) = "$"
        'SpcecialCharacter(3) = "+"
        'SpcecialCharacter(4) = ")"
        Dim SpcecialCharacter() As String = {"@", "#", "$", "^", "&", "*", "(", ")", "_", "+", "-"}
        Dim Number() As String = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"}
        Dim Capital() As String = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"}
        Dim index As Integer = random.Next(SpcecialCharacter.Length)
        RandomSpcecialCharacter = SpcecialCharacter(index)

        Dim NumberIndex As Integer = random.Next(Number.Length)
        RandomNumber = Number(NumberIndex)
        Dim CapitalIndex As Integer = random.Next(Capital.Length)
        RandomCapital = Capital(CapitalIndex)
        Dim builder As StringBuilder = New StringBuilder
        Dim ch As Char
        Dim i As Integer = 0
        Do While (i < size)
            ch = Convert.ToChar(Convert.ToInt32(Math.Floor(((26 * random.NextDouble) _
                                    + 65))))
            builder.Append(ch)
            i = (i + 1)
        Loop

        Return builder.ToString().ToLower() + RandomSpcecialCharacter.ToString() + RandomNumber.ToString() + RandomCapital.ToString()




        'Return (builder.ToString.ToLower _
        '            + (RandomSpcecialCharacter.ToString _
        '            + (RandomNumber.ToString + RandomCapital.ToString)))
    End Function

End Class
