﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//using CCS;
using System.Data;
using System.Data.Sql;
using SC;   

public partial class State_EditStateMaster : System.Web.UI.Page
{
    clsStateCity clsad = new clsStateCity();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindState();
        }
    }
    private void BindState()
    {
        DataSet DsState = new DataSet();
        DsState = clsad.GetStateName();
        ddlState.DataTextField = "StateName";
        ddlState.DataValueField = "StateID";
        ddlState.DataSource = DsState;
        ddlState.DataBind();
    }
   
    protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataSet dsHQ = new DataSet();
        dsHQ = clsad.GetStateMasterDtlsForUpdate(Convert.ToInt32(ddlState.SelectedValue));
        if (dsHQ.Tables[0].Rows.Count > 0)
        {
            txtStateName.Text = ddlState.SelectedItem.Text;
            txtShortCode.Text = dsHQ.Tables[0].Rows[0]["StateShortCode"].ToString();
            ddlUnitYN.SelectedValue = dsHQ.Tables[0].Rows[0]["UnionTerritoryYN"].ToString();
            ddlActive.SelectedValue = dsHQ.Tables[0].Rows[0]["Active"].ToString(); 
        }
        else
        {
            ClearControl();
            lblMessage.Text = "Details Not Exist For Selected State !";
            lblMessage.Visible = true;
            lblMessage.ForeColor = System.Drawing.Color.Red;
        }
    }

    public void ClearControl()
    {
        ddlState.Focus();
        txtStateName.Text = string.Empty;
        txtShortCode.Text = string.Empty;
        //ddlState.DataSource = null;
        //ddlState.DataBind();
        ddlState.SelectedIndex = -1;
        //ddlState.Items.Clear();

        //ddlUnitYN.DataSource = null;
        //ddlUnitYN.DataBind();
        ddlUnitYN.SelectedIndex = -1;
        //ddlUnitYN.Items.Clear();

        //ddlActive.DataSource = null;
        //ddlActive.DataBind();
        ddlActive.SelectedIndex = -1;
        //ddlActive.Items.Clear(); 
    }

    protected void btnupdate_Click(object sender, EventArgs e)
    {
        try
        {
            if (ddlState.SelectedIndex == 0)
            {
                Page.RegisterStartupScript("ScriptDescription", "<script type=\"text/javascript\">alert('Please Select State !');</script>");
                ddlState.Focus();
                return;
            }

            if (txtStateName.Text == "")
            {
                Page.RegisterStartupScript("ScriptDescription", "<script type=\"text/javascript\">alert('Please Enter State Name !');</script>");
                ddlState.Focus();
                return;
            }

            if (txtShortCode.Text == "")
            {
                Page.RegisterStartupScript("ScriptDescription", "<script type=\"text/javascript\">alert('Please Enter State Short Code Name !');</script>");
                txtShortCode.Focus();
                return;
            }

            int rslt = clsad.UpdateStateMaster(Convert.ToInt32(ddlState.SelectedValue)
                , txtStateName.Text.Trim(), txtShortCode.Text,  Convert.ToBoolean(ddlUnitYN.SelectedValue) ,   
                Convert.ToBoolean(ddlActive.SelectedValue),
                Convert.ToInt32(Session["loggedin_user"]));

            if (rslt == 0)
            {
                Page.RegisterStartupScript("ScriptDescription", "<script type=\"text/javascript\">alert('Record Update Successfully !');</script>");
                ClearControl();
                BindState();
            }
            else if (rslt == -1)
            {
                Page.RegisterStartupScript("ScriptDescription", "<script type=\"text/javascript\">alert('Record already Present for the State !');</script>");
            }
            else
            {
                Page.RegisterStartupScript("ScriptDescription", "<script type=\"text/javascript\">alert('Not Submitted Successfully !');</script>");
            }
        }
        catch (Exception ex)
        {
            lblMessage.Text = ex.Message;
            lblMessage.Visible = true;
            lblMessage.ForeColor = System.Drawing.Color.Red;
        }
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        ClearControl(); 
    }
}