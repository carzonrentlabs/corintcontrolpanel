﻿<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="~/usercontrol/Headerctrl.ascx" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EditStateMaster.aspx.cs" Inherits="State_EditStateMaster" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>CarzonRent :: Internal software</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <link href="../HertzInt.css" type="text/css" rel="stylesheet">

    <script language="JavaScript" src="../utilityfunction.js"></script>

    <style type="text/css">
        .style1
        {
            font-size: medium;
            text-decoration: underline;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
      <uc1:Headerctrl ID="Headerctrl1" runat="server"></uc1:Headerctrl>
      <br />
    <div align="center">
   <asp:Panel ID="Panel1" runat="server" Width="30%" align="center" 
        BorderColor="Black" BorderStyle="Double">
             
            <table align="center" >
            <tr>
                
                <td class="style1" colspan="2" style="text-align: center">
                    <strong>Edit State Master</strong></td>
            </tr>
            <tr>
                <td class="style2" colspan="2" style="text-align: center">
                    <asp:Label ID="lblMessage" runat="server" Text="" 
                        style="font-weight: 700; color: #CC0000;"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="text-align: left">
                    Select State :</td>
                <td style="text-align: left">
                
                <asp:DropDownList ID="ddlState" runat="server" 
                        onselectedindexchanged="ddlState_SelectedIndexChanged" AutoPostBack="True">
                </asp:DropDownList>
                   
                </td>
            </tr>

            <tr>
                <td style="text-align: left">
                    State Name :</td>
                <td>
                
                 <asp:TextBox ID="txtStateName" runat="server"></asp:TextBox>
                </td>
            </tr>
                <tr>
                    <td style="text-align: left">
                        Short Code :</td>
                    <td style="text-align: left">
                        <asp:TextBox ID="txtShortCode" runat="server" MaxLength="2" Width="40px"></asp:TextBox>
                    </td>
                </tr>
            <tr>
                <td>
                    Union territory YN :</td>
                <td style="text-align: left">
                    <asp:DropDownList ID="ddlUnitYN" runat="server">
                    <asp:ListItem Selected ="False" Text ="False" Value ="False"></asp:ListItem>
                     <asp:ListItem Text ="True" Value ="True"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
                <tr>
                    <td style="text-align: left">
                        Active :</td>
                    <td style="text-align: left">
                        <asp:DropDownList ID="ddlActive" runat="server">
                            <asp:ListItem Selected="True" Text="True" Value="True"></asp:ListItem>
                            <asp:ListItem Text="False" Value="False"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    <asp:Button ID="btnupdate" runat="server" Text="Update" 
                        onclick="btnupdate_Click"/>
                    &nbsp;<asp:Button ID="btnClear" runat="server" 
                        Text="Reset" onclick="btnClear_Click" />
                </td>
            </tr>
        </table>

           </asp:Panel>
    </div>
    </form>
</body>
</html>
