﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SC;

public partial class State_StateMaster : System.Web.UI.Page
{
   // clsAdmin objcrm = new clsAdmin();
    clsStateCity objcrm = new clsStateCity();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
        }
    }

    public void ClearControl()
    {
        txtStateName.Focus(); 
        txtStateName.Text = string.Empty;
        txtShortCode.Text = string.Empty;   
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            if (txtStateName.Text == "")
            {
                //lblMessage.Text = "Please Enter State Name !";
                //lblMessage.Visible = true;
                //lblMessage.ForeColor = System.Drawing.Color.Red;
                //return;
                Page.RegisterStartupScript("ScriptDescription", "<script type=\"text/javascript\">alert('Please Enter State Name !');</script>");
                txtStateName.Focus();
                return;
            }

            int rslt = objcrm.SaveStateMaster(txtStateName.Text, txtShortCode.Text  ,Convert.ToBoolean(ddlUnitYN.SelectedValue),
                Convert.ToInt32(Session["loggedin_user"]));
            if (rslt > 0)
            {
                //lblMessage.Text = "Submitted Successfully";
                //lblMessage.Visible = true;
                //lblMessage.ForeColor = System.Drawing.Color.Red;
                Page.RegisterStartupScript("ScriptDescription", "<script type=\"text/javascript\">alert('Submitted Successfully !');</script>");
                ClearControl();
            }
            else if (rslt == -1)
            {
                //lblMessage.Text = "Record already Present for the State !";
                //lblMessage.Visible = true;
                //lblMessage.ForeColor = System.Drawing.Color.Red;
                Page.RegisterStartupScript("ScriptDescription", "<script type=\"text/javascript\">alert('Record already Present for the State !');</script>");
            }
            else
            {
                //lblMessage.Text = "Not Submitted Successfully !";
                //lblMessage.Visible = true;
                //lblMessage.ForeColor = System.Drawing.Color.Red;
                Page.RegisterStartupScript("ScriptDescription", "<script type=\"text/javascript\">alert('Not Submitted Successfully !');</script>");
            }
        }
        catch (Exception ex)
        {
            lblMessage.Text = ex.Message;
            lblMessage.Visible = true;
            lblMessage.ForeColor = System.Drawing.Color.Red;
        }
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        ClearControl();
    }
}