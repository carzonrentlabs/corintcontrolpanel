﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using CCS;

public partial class Locking_AddEditLockingPeriod : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            this.BindGrid();
        }

    }
    public DataSet GetLockData()
    {
        return CCS.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "dbo.GetLockData");
    }

    private void BindGrid()
    {
        try
        {
            gvLockPeriod.DataSource = GetLockData();
            gvLockPeriod.DataBind();
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }
    protected void gvLockPeriod_SelectedIndexChanged(object sender, EventArgs e)
    {
        GridViewRow row = gvLockPeriod.SelectedRow;
        ddlyear.SelectedValue = row.Cells[1].Text;
        ddlmonth.SelectedItem.Text = row.Cells[2].Text;

        txtlock.Text = row.Cells[3].Text;

        btnproceed.Visible = false;
        btnupdate.Visible = true;
    }

    protected void btnproceed_Click(object sender, EventArgs e)
    {

        if (txtlock.Text == "")
        {
            //Page.RegisterStartupScript("ScriptDescription", "<script type=\"text/javascript\">alert('Please Enter lock period !');</script>");
            //txtlock.Focus();
            //return;
        }
        else
        {
            int a = 0;
            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["corConnectString"].ToString());
            conn.Open();
            SqlCommand cmd = new SqlCommand("ProcaddLockingPeriod", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@year", ddlyear.SelectedValue);
            cmd.Parameters.AddWithValue("@month", ddlmonth.SelectedValue);
            cmd.Parameters.AddWithValue("@lock", txtlock.Text.Trim());
            cmd.Parameters.Add("@uniqcheckval", SqlDbType.Int);
            cmd.Parameters["@uniqcheckval"].Direction = ParameterDirection.Output;
            cmd.ExecuteNonQuery();
            a = (int)cmd.Parameters["@uniqcheckval"].Value;
            conn.Close();
            if (a == 1)
            {
                Page.RegisterStartupScript("UserMsg", "<Script language='javascript'>alert('This locking period already exists!');</script>");
            }
            else
            {
                Page.RegisterStartupScript("UserMsg", "<Script language='javascript'>alert('Record added successfully.');</script>");
                txtlock.Text = "";
                BindGrid();
            }
        }


    }

    protected void btnupdate_Click(object sender, EventArgs e)
    {

        if (txtlock.Text == "")
        {
            //Page.RegisterStartupScript("ScriptDescription", "<script type=\"text/javascript\">alert('Please Enter lock period !');</script>");
            //txtlock.Focus();
            //return;
        }
        else
        {
            int a = 0;
            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["corConnectString"].ToString());
            conn.Open();
            SqlCommand cmd = new SqlCommand("ProcEditLockingPeriod", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@year", ddlyear.SelectedValue);
            cmd.Parameters.AddWithValue("@month", ddlmonth.SelectedValue);
            cmd.Parameters.AddWithValue("@lock", txtlock.Text.Trim());

            a = cmd.ExecuteNonQuery();

            conn.Close();
            if (a > 0)
            {
                Page.RegisterStartupScript("UserMsg", "<Script language='javascript'>alert('Record added successfully.');</script>");
                txtlock.Text = "";
            }
            else
            {
                Page.RegisterStartupScript("UserMsg", "<Script language='javascript'>alert('some error occured!!');</script>");
                txtlock.Text = "";
            }
        }


    }

    protected void btnreset_Click(object sender, EventArgs e)
    {
        Response.Redirect("AddEditLockingPeriod.aspx");
    }
}