<%@ Page Language="vb" AutoEventWireup="false" Inherits="AddLockingPeriod" Src="AddLockingPeriod.aspx.vb" %>

<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>CarzonRent :: Internal software control panel</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <script language="JavaScript" src="../utilityfunction.js"></script>
    <link href="../HertzInt.css" type="text/css" rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

    <script language="JavaScript">

        $(document).ready(function () {
            $("#<%=txtJan.ClientID%>").datepicker();
            $("#<%=txtFeb.ClientID%>").datepicker();
            $("#<%=txtMar.ClientID%>").datepicker();
            $("#<%=txtApr.ClientID%>").datepicker();
            $("#<%=txtMay.ClientID%>").datepicker();
            $("#<%=txtJun.ClientID%>").datepicker();
            $("#<%=txtJul.ClientID%>").datepicker();
            $("#<%=txtAug.ClientID%>").datepicker();
            $("#<%=txtSep.ClientID%>").datepicker();
            $("#<%=txtOct.ClientID%>").datepicker();
            $("#<%=txtNov.ClientID%>").datepicker();
            $("#<%=txtDec.ClientID%>").datepicker();
        });

        function checkbeforenext() {

            var now = new Date();
            var year = now.getYear();


            //	    if(document.Form1.txtCityName.value=="")
            //		{
            //			alert("Please select City.")
            //			document.Form1.txtCityName.focus();
            //			return false;
            //		}
            if (document.Form1.txtJan.value == "") {
                alert("Please select Date.")
                document.Form1.txtJan.focus();
                return false;
            }
            if (document.Form1.txtJan.value != "") {
                var now = document.Form1.txtJan.value;
                var Month = now.split("/");

                alert(Month[2]);
                if (Month[0] < "01" || Month[2] != (year)) {
                    alert("Date is incorrect");
                    document.Form1.txtJan.focus();
                    return false;
                }
            }
            if (document.Form1.txtFeb.value == "") {
                alert("Please select Date.")
                document.Form1.txtFeb.focus();
                return false;
            }
            if (document.Form1.txtFeb.value != "") {
                var now = document.Form1.txtFeb.value;
                var Month = now.split("/");

                if (Month[0] < "02" || Month[2] != (year)) {
                    alert("Date is incorrect");
                    document.Form1.txtFeb.focus();
                    return false;
                }
            }
            if (document.Form1.txtMar.value == "") {
                alert("Please select Date.")
                document.Form1.txtMar.focus();
                return false;
            }
            if (document.Form1.txtMar.value != "") {
                var now = document.Form1.txtMar.value;
                var Month = now.split("/");

                if (Month[0] < "03" || Month[2] != (year)) {
                    alert("Date is incorrect");
                    document.Form1.txtMar.focus();
                    return false;
                }
            }
            if (document.Form1.txtApr.value == "") {
                alert("Please select Date.")
                document.Form1.txtApr.focus();
                return false;
            }
            if (document.Form1.txtApr.value != "") {
                var now = document.Form1.txtApr.value;
                var Month = now.split("/");

                if (Month[0] < "04" || Month[2] != year) {
                    alert("Date is incorrect");
                    document.Form1.txtApr.focus();
                    return false;
                }
            }
            if (document.Form1.txtMay.value == "") {
                alert("Please select Date.")
                document.Form1.txtMay.focus();
                return false;
            }
            if (document.Form1.txtMay.value != "") {
                var now = document.Form1.txtMay.value;
                var Month = now.split("/");

                if (Month[0] < "05" || Month[2] != year) {
                    alert("Date is incorrect");
                    document.Form1.txtMay.focus();
                    return false;
                }
            }
            if (document.Form1.txtJun.value == "") {
                alert("Please select Date.")
                document.Form1.txtJun.focus();
                return false;
            }
            if (document.Form1.txtJun.value != "") {
                var now = document.Form1.txtJun.value;
                var Month = now.split("/");

                if (Month[0] < "06" || Month[2] != year) {
                    alert("Date is incorrect");
                    document.Form1.txtJun.focus();
                    return false;
                }
            }
            if (document.Form1.txtJul.value == "") {
                alert("Please select Date.")
                document.Form1.txtJul.focus();
                return false;
            }
            if (document.Form1.txtJul.value != "") {
                var now = document.Form1.txtJul.value;
                var Month = now.split("/");

                if (Month[0] < "07" || Month[2] != year) {
                    alert("Date is incorrect");
                    document.Form1.txtJul.focus();
                    return false;
                }
            }
            if (document.Form1.txtAug.value == "") {
                alert("Please select Date.")
                document.Form1.txtAug.focus();
                return false;
            }
            if (document.Form1.txtAug.value != "") {
                var now = document.Form1.txtAug.value;
                var Month = now.split("/");

                if (Month[0] < "08" || Month[2] != year) {
                    alert("Date is incorrect");
                    document.Form1.txtAug.focus();
                    return false;
                }

            }
            if (document.Form1.txtSep.value == "") {
                alert("Please select Date.")
                document.Form1.txtSep.focus();
                return false;
            }
            if (document.Form1.txtSep.value != "") {
                var now = document.Form1.txtSep.value;
                var Month = now.split("/");

                if (Month[0] < "09" || Month[2] != year) {
                    alert("Date is incorrect");
                    document.Form1.txtSep.focus();
                    return false;
                }
            }
            if (document.Form1.txtOct.value == "") {
                alert("Please select Date.")
                document.Form1.txtOct.focus();
                return false;
            }
            if (document.Form1.txtOct.value != "") {
                var now = document.Form1.txtOct.value;
                var Month = now.split("/");

                if (Month[0] < "10" || Month[2] != year) {
                    alert("Date is incorrect");
                    document.Form1.txtOct.focus();
                    return false;
                }
            }
            if (document.Form1.txtNov.value == "") {
                alert("Please select Date.")
                document.Form1.txtNov.focus();
                return false;
            }
            if (document.Form1.txtNov.value != "") {
                var now = document.Form1.txtNov.value;
                var Month = now.split("/");

                if (Month[0] < "11" || Month[2] != year) {
                    alert("Date is incorrect");
                    document.Form1.txtNov.focus();
                    return false;
                }
            }
            if (document.Form1.txtDec.value == "") {
                alert("Please select Date.")
                document.Form1.txtDec.focus();
                return false;
            }
            if (document.Form1.txtDec.value != "") {
                //var now = document.Form1.txtDec.value;
                //var Month  = now.split("/");

                //if(Month[0]<"12" || Month[2]!=year)
                //if(Month[0]<"12")					
                //{
                //alert("Date is incorrect");
                //document.Form1.txtDec.focus();
                //return false;
                //}
            }

        }

        function dateReg(obj) {
            if (obj.value != "") {
                // alert(obj.value);
                var reg = /((0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[0-1])-(201[0-9]))/
                if (reg.test(obj.value)) {
                    //alert('valid');
                }
                else {
                    alert('notvalid');
                    obj.value = "";
                }
            }
        }

    </script>
</head>
<body ms_positioning="GridLayout">
    <form id="Form1" name="Form1" method="post" runat="server">
        <uc1:Headerctrl ID="Headerctrl1" runat="server"></uc1:Headerctrl>
        <table align="center">
            <tbody>
                <asp:Panel ID="pnlmainform" runat="server">
                    <tr>
                        <td align="center" colspan="2"><b><u>Add a Locking Period</u></b><br>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <asp:Label ID="lblErrorMsg" runat="server" Visible="false" CssClass="subRedHead">Add a Lock</asp:Label></td>
                    </tr>
                    <!--<TR>
							<TD>*&nbsp;City Name&nbsp;</TD>
							<TD>
								<asp:DropDownList id="txtCityName" runat="server" CssClass="input"></asp:DropDownList></TD>
						</TR> -->
                    <tr>
                        <td>*&nbsp;January</td>
                        <td>
                            <asp:TextBox ID="txtJan" runat="server" CssClass="input"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>*&nbsp;February</td>
                        <td>
                            <asp:TextBox ID="txtFeb" runat="server" CssClass="input"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>*&nbsp;March</td>
                        <td>
                            <asp:TextBox ID="txtMar" runat="server" CssClass="input"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>*&nbsp;April</td>
                        <td>
                            <asp:TextBox ID="txtApr" runat="server" CssClass="input"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>*&nbsp;May</td>
                        <td>
                            <asp:TextBox ID="txtMay" runat="server" CssClass="input"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>*&nbsp;June</td>
                        <td>
                            <asp:TextBox ID="txtJun" runat="server" CssClass="input"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>*&nbsp;July</td>
                        <td>
                            <asp:TextBox ID="txtJul" runat="server" CssClass="input"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>*&nbsp;August</td>
                        <td>
                            <asp:TextBox ID="txtAug" runat="server" CssClass="input"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>*&nbsp;September</td>
                        <td>
                            <asp:TextBox ID="txtSep" runat="server" CssClass="input"></asp:TextBox>

                        </td>
                    </tr>
                    <tr>
                        <td>*&nbsp;October</td>
                        <td>
                            <asp:TextBox ID="txtOct" runat="server" CssClass="input"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>*&nbsp;November</td>
                        <td>
                            <asp:TextBox ID="txtNov" runat="server" CssClass="input"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>*&nbsp;December</td>
                        <td>
                            <asp:TextBox ID="txtDec" runat="server" CssClass="input"></asp:TextBox>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <asp:Button ID="btnproceed" runat="server" CssClass="input" Text="Save"></asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                </asp:Panel>
                <asp:Panel ID="pnlconfirmation" runat="server" Visible="False">
                    <tr align="center">
                        <td colspan="2">
                            <asp:Label ID="lblMessage" runat="server"></asp:Label></td>
                    </tr>
                    <tr align="center">
                        <td colspan="2">
                            <asp:HyperLink ID="hyplnkretry" runat="server"></asp:HyperLink></td>
                    </tr>
                </asp:Panel>
            </tbody>
        </table>
    </form>
</body>
</html>
