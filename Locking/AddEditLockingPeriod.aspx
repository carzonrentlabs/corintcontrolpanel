﻿<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="~/usercontrol/Headerctrl.ascx" %>

<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddEditLockingPeriod.aspx.cs" Inherits="Locking_AddEditLockingPeriod" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CarzonRent :: Internal software control panel</title>
    <style type="text/css">
        .hiddencol {
            display: none;
        }
    </style>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <script language="JavaScript" src="../utilityfunction.js"></script>
    <script language="JavaScript" src="../JScripts/Datefunc.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css" />
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <link href="../HertzInt.css" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" href="/resources/demos/style.css" />


    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%=txtlock.ClientID %>").datepicker();

        });

        function ValidateControl() {
            if (document.getElementById("<%=ddlyear.ClientID%>").selectedIndex == 0) {
                alert("Select year");
                document.getElementById("<%=ddlyear.ClientID%>").focus();
                return false;
            }
            else if (document.getElementById("<%=ddlmonth.ClientID%>").selectedIndex == 0) {
                alert("Select month");
                document.getElementById("<%=ddlmonth.ClientID%>").focus();
                return false;
            }
            else if (document.getElementById("<%=txtlock.ClientID%>").value == '') {
                alert("Enter Locking Period");
                document.getElementById("<%=txtlock.ClientID%>").focus();
                return false;
            }
            else if (document.getElementById("<%=txtlock.ClientID%>").value != "") {
                var now = document.getElementById("<%=txtlock.ClientID%>").value;
                var dmonth = document.getElementById("<%=ddlmonth.ClientID%>").selectedIndex
                var Month = now.split("/");

                if (Month[2] > document.getElementById("<%=ddlyear.ClientID%>").value && Month[0] >= 2) {
                    alert("Date can not be greater than one month of selected month!");
                    document.getElementById("<%=txtlock.ClientID%>").focus();
                    return false;
                }
                else if (Month[0] < dmonth || Month[0] > dmonth + 1) {

                    if (Month[2] == document.getElementById("<%=ddlyear.ClientID%>").value) {
                        alert("Date cant not be earlier or greater than one month of selected month!");
                        document.getElementById("<%=txtlock.ClientID%>").focus();
                        return false;
                    }
                }
                else if (Month[0] == dmonth) {
                    alert("Date can not be of same month of selected month!");
                    document.getElementById("<%=txtlock.ClientID%>").focus();
                    return false;
                }


            }
        }
        function dateReg(obj) {
            if (obj.value != "") {
                // alert(obj.value);
                var reg = /((0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[0-1])-(201[0-9]))/
                if (reg.test(obj.value)) {
                    //alert('valid');
                }
                else {
                    alert('date not valid!');
                    obj.value = "";
                }
            }
        }
    </script>
</head>
<body ms_positioning="GridLayout">
    <form id="form1" runat="server">

        <div>

            <uc1:Headerctrl ID="Headerctrl2" runat="server"></uc1:Headerctrl>
            <table align="center">
                <tbody>
                    <asp:Panel ID="pnlmainform" runat="server">
                        <tr>
                            <td align="center" colspan="2"><b><u>Add/Edit a Locking Period</u></b><br>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <asp:Label ID="lblErrorMsg" runat="server" Visible="false" CssClass="subRedHead">Edit a Lock</asp:Label></td>
                        </tr>
                        <tr>
                            <td>*&nbsp;Year&nbsp;</td>
                            <td>
                                <asp:DropDownList ID="ddlyear" runat="server" CssClass="input">
                                    <asp:ListItem Text="-Select-" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="2022" Value="2022"></asp:ListItem>
									<asp:ListItem Text="2023" Value="2023"></asp:ListItem>
									<asp:ListItem Text="2024" Value="2024"></asp:ListItem>
									<asp:ListItem Text="2025" Value="2025"></asp:ListItem>
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td>*&nbsp;Month&nbsp;</td>
                            <td>
                                <asp:DropDownList ID="ddlmonth" runat="server" CssClass="input">
                                    <asp:ListItem Text="-Select-" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="Jan" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Feb" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="Mar" Value="3"></asp:ListItem>
                                    <asp:ListItem Text="Apr" Value="4"></asp:ListItem>
                                    <asp:ListItem Text="May" Value="5"></asp:ListItem>
                                    <asp:ListItem Text="Jun" Value="6"></asp:ListItem>
                                    <asp:ListItem Text="Jul" Value="7"></asp:ListItem>
                                    <asp:ListItem Text="Aug" Value="8"></asp:ListItem>
                                    <asp:ListItem Text="Sep" Value="9"></asp:ListItem>
                                    <asp:ListItem Text="Oct" Value="10"></asp:ListItem>
                                    <asp:ListItem Text="Nov" Value="11"></asp:ListItem>
                                    <asp:ListItem Text="Dec" Value="12"></asp:ListItem>

                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td>*&nbsp;Lock Period</td>
                            <td>
                                <asp:TextBox ID="txtlock" runat="server" CssClass="input" Width="75px" MaxLength="50"></asp:TextBox>
                                <%--onblur="dateReg(this);"--%>
                            </td>
                        </tr>

                        <tr>
                            <td align="center" colspan="2">
                                <asp:Button ID="btnproceed" OnClientClick="return ValidateControl();" OnClick="btnproceed_Click" runat="server" CssClass="input" Text="Save"></asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;
                              <asp:Button ID="btnupdate" runat="server" OnClientClick="return ValidateControl();" OnClick="btnupdate_Click" Visible="false" CssClass="input" Text="Update"></asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;
                              <asp:Button ID="btnreset" runat="server"  OnClick="btnreset_Click" Visible="true" CssClass="input" Text="Reset"></asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;
                            
                            </td>



                        </tr>
                        <tr>
                            <td align="center" colspan="2"></td>
                        </tr>
                    </asp:Panel>
                    <asp:Panel ID="pnlconfirmation" runat="server" Visible="False">
                        <tr align="center">
                            <td colspan="2">
                                <asp:Label ID="lblMessage" runat="server"></asp:Label></td>
                        </tr>
                        <tr align="center">
                            <td colspan="2">
                                <asp:HyperLink ID="hyplnkretry" runat="server"></asp:HyperLink></td>
                        </tr>
                    </asp:Panel>
                </tbody>
            </table>

        </div>

        <div style="position: center; margin-top:20px" align="center">
            <asp:GridView ID="gvLockPeriod" runat="server" DataKeyNames="Lockid"
                OnSelectedIndexChanged="gvLockPeriod_SelectedIndexChanged" AutoGenerateColumns="false">
                <Columns>
                    <asp:BoundField HeaderText="Lockid" DataField="Lockid" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                    <asp:BoundField HeaderText="Year" DataField="year"></asp:BoundField>
                    <asp:BoundField HeaderText="Month" DataField="month"></asp:BoundField>
                    <asp:BoundField HeaderText="Lock Period(MM/DD/YYYY)" DataField="lock"></asp:BoundField>

                    <%-- <asp:TemplateField HeaderText="Year" ItemStyle-Width="150">
                        <ItemTemplate>
                            <asp:Label ID="lblYear" runat="server" Text='<%# Eval("year") %>'></asp:Label>
                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Month" ItemStyle-Width="150">
                        <ItemTemplate>
                            <asp:Label ID="lblMonth" runat="server" Text='<%# Eval("month") %>'></asp:Label>
                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Lock(DD/MM/YYYY)" ItemStyle-Width="150">
                        <ItemTemplate>
                            <asp:Label ID="lblLock" runat="server" Text='<%# Eval("lock") %>'></asp:Label>
                        </ItemTemplate>

                    </asp:TemplateField>--%>
                    <asp:CommandField HeaderText="Update" ShowSelectButton="True" />

                </Columns>
            </asp:GridView>
        </div>

    </form>
</body>
</html>
