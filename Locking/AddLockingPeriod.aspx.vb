Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System

Public Class AddLockingPeriod
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    Protected WithEvents btnproceed As System.Web.UI.WebControls.Button
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents txtCityName As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lnk As System.Web.UI.WebControls.LinkButton
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    Protected WithEvents txtApr As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtMay As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtJun As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtJul As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtAug As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtSep As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtOct As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtNov As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtDec As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtJan As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtFeb As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtMar As System.Web.UI.WebControls.TextBox


    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btnproceed.Attributes("onClick") = "return checkbeforenext();"
        'If Not Page.IsPostBack Then
        'populateddl()
        'End If
    End Sub

    'Sub populateddl()
    '    Dim objAcessdata As clsutility
    '    objAcessdata = New clsutility

    '   txtCityName.DataSource = objAcessdata.funcGetSQLDataReader("select cityname,cityid from CORIntCityMaster where cityid not in (select cityid from CORIntLockingPeriod where deleted='N') and active=1 order by cityname")
    '    txtCityName.DataValueField = "cityid"
    '    txtCityName.DataTextField = "cityname"
    '    txtCityName.DataBind()
    '    txtCityName.Items.Insert(0, New ListItem("", ""))
    'End Sub

    Private Sub btnproceed_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnproceed.Click
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        'Dim i As Int16
        Dim intuniqcheck As SqlParameter
        Dim intuniqvalue As Int32
        'execueting the stored procedure for checking the user login validity by using the output parameter
        '  For i = 0 To 11
        MyConnection.Open()
        If txtJan.Text <> "" Then
            cmd = New SqlCommand("procaddLockingPeriod", MyConnection)
            cmd.CommandType = CommandType.StoredProcedure
            'cmd.Parameters.Add("@cityid", txtCityName.SelectedValue)
            cmd.Parameters.Add("@month", 1)
            cmd.Parameters.Add("@lock", txtJan.Text)

            cmd.ExecuteNonQuery()
        End If
        If txtFeb.Text <> "" Then
            cmd = New SqlCommand("procaddLockingPeriod", MyConnection)
            cmd.CommandType = CommandType.StoredProcedure
            'cmd.Parameters.Add("@cityid", txtCityName.SelectedValue)
            cmd.Parameters.Add("@month", 2)
            cmd.Parameters.Add("@lock", txtFeb.Text)
            '  MyConnection.Open()
            cmd.ExecuteNonQuery()
        End If
        If txtMar.Text <> "" Then
            cmd = New SqlCommand("procaddLockingPeriod", MyConnection)
            cmd.CommandType = CommandType.StoredProcedure
            'cmd.Parameters.Add("@cityid", txtCityName.SelectedValue)
            cmd.Parameters.Add("@month", 3)
            cmd.Parameters.Add("@lock", txtMar.Text)
            '  MyConnection.Open()
            cmd.ExecuteNonQuery()
        End If
        If txtApr.Text <> "" Then
            cmd = New SqlCommand("procaddLockingPeriod", MyConnection)
            cmd.CommandType = CommandType.StoredProcedure
            'cmd.Parameters.Add("@cityid", txtCityName.SelectedValue)
            cmd.Parameters.Add("@month", 4)
            cmd.Parameters.Add("@lock", txtApr.Text)
            '  MyConnection.Open()
            cmd.ExecuteNonQuery()
        End If
        If txtMay.Text <> "" Then
            cmd = New SqlCommand("procaddLockingPeriod", MyConnection)
            cmd.CommandType = CommandType.StoredProcedure
            'cmd.Parameters.Add("@cityid", txtCityName.SelectedValue)
            cmd.Parameters.Add("@month", 5)
            cmd.Parameters.Add("@lock", txtMay.Text)
            ' MyConnection.Open()
            cmd.ExecuteNonQuery()
        End If
        If txtJun.Text <> "" Then
            cmd = New SqlCommand("procaddLockingPeriod", MyConnection)
            cmd.CommandType = CommandType.StoredProcedure
            'cmd.Parameters.Add("@cityid", txtCityName.SelectedValue)
            cmd.Parameters.Add("@month", 6)
            cmd.Parameters.Add("@lock", txtJun.Text)
            ' MyConnection.Open()
            cmd.ExecuteNonQuery()
        End If
        If txtJul.Text <> "" Then
            cmd = New SqlCommand("procaddLockingPeriod", MyConnection)
            cmd.CommandType = CommandType.StoredProcedure
            'cmd.Parameters.Add("@cityid", txtCityName.SelectedValue)
            cmd.Parameters.Add("@month", 7)
            cmd.Parameters.Add("@lock", txtJul.Text)
            'MyConnection.Open()
            cmd.ExecuteNonQuery()
        End If
        If txtAug.Text <> "" Then
            cmd = New SqlCommand("procaddLockingPeriod", MyConnection)
            cmd.CommandType = CommandType.StoredProcedure
            'cmd.Parameters.Add("@cityid", txtCityName.SelectedValue)
            cmd.Parameters.Add("@month", 8)
            cmd.Parameters.Add("@lock", txtAug.Text)
            'MyConnection.Open()
            cmd.ExecuteNonQuery()
        End If
        If txtSep.Text <> "" Then
            cmd = New SqlCommand("procaddLockingPeriod", MyConnection)
            cmd.CommandType = CommandType.StoredProcedure
            'cmd.Parameters.Add("@cityid", txtCityName.SelectedValue)
            cmd.Parameters.Add("@month", 9)
            cmd.Parameters.Add("@lock", txtSep.Text)
            'MyConnection.Open()
            cmd.ExecuteNonQuery()
        End If
        If txtOct.Text <> "" Then
            cmd = New SqlCommand("procaddLockingPeriod", MyConnection)
            cmd.CommandType = CommandType.StoredProcedure
            'cmd.Parameters.Add("@cityid", txtCityName.SelectedValue)
            cmd.Parameters.Add("@month", 10)
            cmd.Parameters.Add("@lock", txtOct.Text)
            'MyConnection.Open()
            cmd.ExecuteNonQuery()
        End If
        If txtNov.Text <> "" Then
            cmd = New SqlCommand("procaddLockingPeriod", MyConnection)
            cmd.CommandType = CommandType.StoredProcedure
            'cmd.Parameters.Add("@cityid", txtCityName.SelectedValue)
            cmd.Parameters.Add("@month", 11)
            cmd.Parameters.Add("@lock", txtNov.Text)
            'MyConnection.Open()
            cmd.ExecuteNonQuery()
        End If
        If txtDec.Text <> "" Then
            cmd = New SqlCommand("procaddLockingPeriod", MyConnection)
            cmd.CommandType = CommandType.StoredProcedure
            'cmd.Parameters.Add("@cityid", txtCityName.SelectedValue)
            cmd.Parameters.Add("@month", 12)
            cmd.Parameters.Add("@lock", txtDec.Text)
            'MyConnection.Open()
            cmd.ExecuteNonQuery()
        End If
        ' Next
        'getting the value to know that whethre the user is vallid user for login or not
        'intuniqvalue = cmd.Parameters("@uniqcheckval").Value
        MyConnection.Close()
        pnlmainform.Visible = False

        pnlconfirmation.Visible = True
        lblMessage.Text = "You have added the Record successfully"
        hyplnkretry.Text = "Add another Lock"
        hyplnkretry.NavigateUrl = "addLockingPeriod.aspx"
        'If Not intuniqvalue = 0 Then
        '    lblErrorMsg.Visible = True
        '    lblErrorMsg.Text = "Data already exist."

        'End If


    End Sub





End Class
