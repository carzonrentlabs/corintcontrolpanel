<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SearchLock" Src="SearchLock.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CarzonRent :: Internal software control panel</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="JavaScript" src="../utilityfunction.js"></script>
		<script language="JavaScript" src="../JScripts/Datefunc.js"></script>
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
		<script language="JavaScript">
		
			function checkbeforenext()
			{
			
			var now = new Date();
			var year        = now.getYear();

			
				if(document.Form1.txtCityName.value=="")
				{
					alert("Please select City.")
					document.Form1.txtCityName.focus();
					return false;
				}
				//document.Form1.action ="editLockingPeriod.aspx"
			}
			
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" name="Form1" method="post" runat="server">
			<uc1:headerctrl id="Headerctrl1" runat="server"></uc1:headerctrl>
			<table align="center">
				<TBODY>
					<asp:panel id="pnlmainform" Runat="server">
						<TR>
							<TD align="center" colSpan="2"><B><U>Search for Lock</U></B><BR>
							</TD>
						</TR>
						<TR>
							<TD align="center" colSpan="2">
								<asp:Label id="lblErrorMsg" runat="server" cssclass="subRedHead" visible="false"></asp:Label></TD>
						</TR>
						<TR>
							<TD>*&nbsp;City Name&nbsp;</TD>
							<TD>
								<asp:DropDownList id="txtCityName" runat="server" CssClass="input"></asp:DropDownList></TD>
						</TR>
						<TR>
							<TD align="center" colSpan="2">
								<asp:button id="btnproceed" runat="server" CssClass="input" Text="Continue"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;
							</TD>
						</TR>
					</asp:panel></TBODY></table>
		</form>
	</body>
</HTML>
