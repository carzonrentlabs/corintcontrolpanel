Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class SearchLock
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    Protected WithEvents txtCityName As System.Web.UI.WebControls.DropDownList
    Protected WithEvents btnproceed As System.Web.UI.WebControls.Button
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here

        btnproceed.Attributes("onClick") = "return checkbeforenext();"
        If Not Page.IsPostBack Then
            populateddl()
        End If
    End Sub
    Sub populateddl()
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility

        txtCityName.DataSource = objAcessdata.funcGetSQLDataReader("select distinct(l.cityid) ,c.cityname from CORIntCityMaster c,CORIntLockingPeriod l where l.cityid=c.cityid and c.active=1 order by c.cityname")
        txtCityName.DataValueField = "cityid"
        txtCityName.DataTextField = "cityname"
        txtCityName.DataBind()
        txtCityName.Items.Insert(0, New ListItem("", ""))
    End Sub

    Private Sub btnproceed_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnproceed.Click
        Response.Redirect("EditLockingPeriod.aspx?id=" & txtCityName.SelectedItem.Value)
    End Sub
End Class
