Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class EditLockingPeriod
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    Protected WithEvents btnproceed As System.Web.UI.WebControls.Button
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents txtCityName As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtJan As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtFeb As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtMar As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtApr As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtMay As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtJun As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtJul As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtAug As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtSep As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtOct As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtNov As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtDec As System.Web.UI.WebControls.TextBox
    Protected WithEvents lnk As System.Web.UI.WebControls.LinkButton
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        'btnproceed.Attributes("onClick") = "return checkbeforenext();"
        If Not Page.IsPostBack Then
            'populateddl()
            Dim month As Int16
            Dim dtrreader As SqlDataReader
            Dim accessdata As clsutility
            accessdata = New clsutility
            Dim strQuery1 As String
            strQuery1 = "select month,lock from CORIntLockingPeriod"
            'strQuery1 = "select distinct(cityid),month,lock from CORIntLockingPeriod where  cityid=" & Request.QueryString("id") & " "

            'dtrreader = accessdata.funcGetSQLDataReader("select distinct(cityid),month,lock from CORIntLockingPeriod where  cityid=" & Request.QueryString("id") & " ")
            dtrreader = accessdata.funcGetSQLDataReader("select month,lock from CORIntLockingPeriod")

            ' txtcityname.Text = dtrreader("cityname") & ""
            'If dtrreader("jan") <> "1/1/1900" Then
            While dtrreader.Read()

                month = dtrreader("month") & ""
                'Else
                'txtJan.Text = ""
                'End If
                If month = 1 Then
                    txtJan.Text = dtrreader("lock") & ""
                    ' Else
                    '    txtFeb.Text = ""
                End If
                If month = 2 Then
                    txtFeb.Text = dtrreader("lock") & ""
                    ' Else
                    '    txtFeb.Text = ""
                End If
                If month = 3 Then
                    txtMar.Text = dtrreader("lock") & ""
                    ' Else
                    '    txtFeb.Text = ""
                End If
                If month = 4 Then
                    txtApr.Text = dtrreader("lock") & ""
                End If
                If month = 5 Then
                    txtMay.Text = dtrreader("lock") & ""
                    ' Else
                    '    txtApr.Text = ""
                End If
                If month = 6 Then
                    txtJun.Text = dtrreader("lock") & ""
                End If
                If month = 7 Then
                    txtJul.Text = dtrreader("lock") & ""
                End If
                If month = 8 Then
                    txtAug.Text = dtrreader("lock") & ""
                End If
                If month = 9 Then
                    txtSep.Text = dtrreader("lock") & ""
                End If
                If month = 10 Then
                    txtOct.Text = dtrreader("lock") & ""
                End If
                If month = 11 Then
                    txtNov.Text = dtrreader("lock") & ""
                End If
                If month = 12 Then
                    txtDec.Text = dtrreader("lock") & ""
                End If
                ' If dtrreader("may") <> "1/1/1900" Then
                ' txtMay.Text = dtrreader("may") & ""
                ' Else
                '     txtMay.Text = ""
                ' End If
                ' If dtrreader("jun") <> "1/1/1900" Then
                ' txtJun.Text = dtrreader("jun") & ""
                ' Else
                '    txtJun.Text = ""
                'End If
                'If dtrreader("jul") <> "1/1/1900" Then
                ' txtJul.Text = dtrreader("jul") & ""
                'Else
                '    txtJul.Text = ""
                ' End If
                ' If dtrreader("aug") <> "1/1/1900" Then
                '  txtAug.Text = dtrreader("aug") & ""
                ' Else
                '     txtAug.Text = ""
                ' End If
                ' If dtrreader("sep") <> "1/1/1900" Then
                ' txtSep.Text = dtrreader("sep") & ""
                'Else
                '   txtSep.Text = ""
                'End If
                ' If dtrreader("oct") <> "1/1/1900" Then
                ' txtOct.Text = dtrreader("oct") & ""
                ' Else
                '     txtOct.Text = ""
                ' End If
                ' If dtrreader("nov") <> "1/1/1900" Then
                '  txtNov.Text = dtrreader("nov") & ""
                ' Else
                '    txtNov.Text = ""
                'End If
                'If dtrreader("dec") <> "1/1/1900" Then
                ' txtDec.Text = dtrreader("dec") & ""
                'Else
                '    txtDec.Text = ""
                'End If

                'txtCityName.Items.FindByValue(dtrreader("cityid")).Selected = True
            End While
            dtrreader.Close()
            accessdata.Dispose()

        End If
    End Sub
    '  Sub populateddl()
    '     Dim objAcessdata As clsutility
    '    objAcessdata = New clsutility

    '   txtCityName.DataSource = objAcessdata.funcGetSQLDataReader("select c.cityname,l.cityid from CORIntCityMaster c,CORIntLockingPeriod l where l.cityid=c.cityid and c.active=1 order by c.cityname")
    '   txtCityName.DataValueField = "cityid"
    '   txtCityName.DataTextField = "cityname"
    '  txtCityName.DataBind()
    ' txtCityName.Items.Insert(0, New ListItem("", ""))
    'End Sub
    Private Sub btnproceed_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnproceed.Click
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        Dim intuniqcheck As SqlParameter
        Dim intuniqvalue As Int32
        'execueting the stored procedure for checking the user login validity by using the output parameter
        MyConnection.Open()
        If txtJan.Text <> "" Then
            cmd = New SqlCommand("proceditLockingPeriod", MyConnection)
            cmd.CommandType = CommandType.StoredProcedure
            'cmd.Parameters.Add("@cityid", txtCityName.SelectedValue)
            cmd.Parameters.Add("@month", 1)
            cmd.Parameters.Add("@lock", txtJan.Text)

            cmd.ExecuteNonQuery()
        End If
        If txtFeb.Text <> "" Then
            cmd = New SqlCommand("proceditLockingPeriod", MyConnection)
            cmd.CommandType = CommandType.StoredProcedure
            'cmd.Parameters.Add("@cityid", txtCityName.SelectedValue)
            cmd.Parameters.Add("@month", 2)
            cmd.Parameters.Add("@lock", txtFeb.Text)
            '  MyConnection.Open()
            cmd.ExecuteNonQuery()
        End If
        If txtMar.Text <> "" Then
            cmd = New SqlCommand("proceditLockingPeriod", MyConnection)
            cmd.CommandType = CommandType.StoredProcedure
            'cmd.Parameters.Add("@cityid", txtCityName.SelectedValue)
            cmd.Parameters.Add("@month", 3)
            cmd.Parameters.Add("@lock", txtMar.Text)
            '  MyConnection.Open()
            cmd.ExecuteNonQuery()
        End If
        If txtApr.Text <> "" Then
            cmd = New SqlCommand("proceditLockingPeriod", MyConnection)
            cmd.CommandType = CommandType.StoredProcedure
            'cmd.Parameters.Add("@cityid", txtCityName.SelectedValue)
            cmd.Parameters.Add("@month", 4)
            cmd.Parameters.Add("@lock", txtApr.Text)
            '  MyConnection.Open()
            cmd.ExecuteNonQuery()
        End If
        If txtMay.Text <> "" Then
            cmd = New SqlCommand("proceditLockingPeriod", MyConnection)
            cmd.CommandType = CommandType.StoredProcedure
            'cmd.Parameters.Add("@cityid", txtCityName.SelectedValue)
            cmd.Parameters.Add("@month", 5)
            cmd.Parameters.Add("@lock", txtMay.Text)
            ' MyConnection.Open()
            cmd.ExecuteNonQuery()
        End If
        If txtJun.Text <> "" Then
            cmd = New SqlCommand("proceditLockingPeriod", MyConnection)
            cmd.CommandType = CommandType.StoredProcedure
            ' cmd.Parameters.Add("@cityid", txtCityName.SelectedValue)
            cmd.Parameters.Add("@month", 6)
            cmd.Parameters.Add("@lock", txtJun.Text)
            ' MyConnection.Open()
            cmd.ExecuteNonQuery()
        End If
        If txtJul.Text <> "" Then
            cmd = New SqlCommand("proceditLockingPeriod", MyConnection)
            cmd.CommandType = CommandType.StoredProcedure
            'cmd.Parameters.Add("@cityid", txtCityName.SelectedValue)
            cmd.Parameters.Add("@month", 7)
            cmd.Parameters.Add("@lock", txtJul.Text)
            'MyConnection.Open()
            cmd.ExecuteNonQuery()
        End If
        If txtAug.Text <> "" Then
            cmd = New SqlCommand("proceditLockingPeriod", MyConnection)
            cmd.CommandType = CommandType.StoredProcedure
            'cmd.Parameters.Add("@cityid", txtCityName.SelectedValue)
            cmd.Parameters.Add("@month", 8)
            cmd.Parameters.Add("@lock", txtAug.Text)
            'MyConnection.Open()
            cmd.ExecuteNonQuery()
        End If
        If txtSep.Text <> "" Then
            cmd = New SqlCommand("proceditLockingPeriod", MyConnection)
            cmd.CommandType = CommandType.StoredProcedure
            'cmd.Parameters.Add("@cityid", txtCityName.SelectedValue)
            cmd.Parameters.Add("@month", 9)
            cmd.Parameters.Add("@lock", txtSep.Text)
            'MyConnection.Open()
            cmd.ExecuteNonQuery()
        End If
        If txtOct.Text <> "" Then
            cmd = New SqlCommand("proceditLockingPeriod", MyConnection)
            cmd.CommandType = CommandType.StoredProcedure
            'cmd.Parameters.Add("@cityid", txtCityName.SelectedValue)
            cmd.Parameters.Add("@month", 10)
            cmd.Parameters.Add("@lock", txtOct.Text)
            'MyConnection.Open()
            cmd.ExecuteNonQuery()
        End If
        If txtNov.Text <> "" Then
            cmd = New SqlCommand("proceditLockingPeriod", MyConnection)
            cmd.CommandType = CommandType.StoredProcedure
            'cmd.Parameters.Add("@cityid", txtCityName.SelectedValue)
            cmd.Parameters.Add("@month", 11)
            cmd.Parameters.Add("@lock", txtNov.Text)
            'MyConnection.Open()
            cmd.ExecuteNonQuery()
        End If
        If txtDec.Text <> "" Then
            cmd = New SqlCommand("proceditLockingPeriod", MyConnection)
            cmd.CommandType = CommandType.StoredProcedure
            ' cmd.Parameters.Add("@cityid", txtCityName.SelectedValue)
            cmd.Parameters.Add("@month", 12)
            cmd.Parameters.Add("@lock", txtDec.Text)
            'MyConnection.Open()
            cmd.ExecuteNonQuery()
        End If

        'getting the value to know that whethre the user is vallid user for login or not
        'intuniqvalue = cmd.Parameters("@uniqcheckval").Value
        MyConnection.Close()
        pnlmainform.Visible = False

        pnlconfirmation.Visible = True
        lblMessage.Text = "You have Updated the Record successfully"
        hyplnkretry.Text = "Update another Lock"
        hyplnkretry.NavigateUrl = "SearchLock.aspx"
        'If Not intuniqvalue = 0 Then
        '    lblErrorMsg.Visible = True
        '    lblErrorMsg.Text = "Data already exist."

        'End If


    End Sub

End Class
