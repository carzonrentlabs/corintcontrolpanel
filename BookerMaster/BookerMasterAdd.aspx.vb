Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class BookerMasterAdd
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtbooker As System.Web.UI.WebControls.TextBox
    Protected WithEvents AgencyID As System.Web.UI.WebControls.DropDownList
    Protected WithEvents btnsubmit As System.Web.UI.WebControls.Button
    Protected WithEvents chkActive As System.Web.UI.WebControls.CheckBox
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        btnsubmit.Attributes("onClick") = "return validation();"
        If Not Page.IsPostBack() Then
            populateddl()
        End If
    End Sub
    Sub populateddl()
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility

        AgencyID.DataSource = objAcessdata.funcGetSQLDataReader("select AgencyName, AgencyID from CorIntAgencyMaster where active=1 order by AgencyName ")
        AgencyID.DataValueField = "AgencyID"
        AgencyID.DataTextField = "AgencyName"
        AgencyID.DataBind()
        AgencyID.Items.Insert(0, New ListItem("Any", 0))

   
    End Sub

    Private Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        Dim intFlagCheck As SqlParameter
        Dim intFlag As Int32
        Dim intSDParam As SqlParameter
        Dim intSDid As Int32
        Dim txtHr As String

        'If Trim(desc.Text) = "" Then
        'Response.write("Please enter the Description")
        'Response.end()
        'End If

        cmd = New SqlCommand("Proc_AddBookerMaster", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("@BookerName", txtbooker.Text)
        cmd.Parameters.Add("@AgencyID", AgencyID.SelectedItem.Value)
        cmd.Parameters.Add("@Active", get_YNvalue(chkActive))
        cmd.Parameters.Add("@CreatedBy", Session("loggedin_user"))
        intFlagCheck = cmd.Parameters.Add("@StatusFlag", SqlDbType.Int)
        intFlagCheck.Direction = ParameterDirection.Output
        intSDParam = cmd.Parameters.Add("@ID", SqlDbType.Int)
        intSDParam.Direction = ParameterDirection.Output

        MyConnection.Open()

        cmd.ExecuteNonQuery()
        intFlag = cmd.Parameters("@StatusFlag").Value
        '   response.write(intFlag)
        '   response.end()
        intSDid = cmd.Parameters("@ID").Value

        MyConnection.Close()
        lblErrorMsg.visible = True

        If Trim("" & intFlag) = "2" Then
            pnlmainform.visible = False
            pnlconfirmation.visible = True
            lblErrorMsg.Text = "This Booker Master already exists!<br> The ID is <b>" & intSDid & "</b>"
            'lblErrorMsg.visible = True
            'response.write(lblErrorMsg.Text)
            hyplnkretry.Text = "Add another Booker Master"
            hyplnkretry.NavigateUrl = "BookerMasterAdd.aspx"
        Else
            pnlmainform.Visible = False
            pnlconfirmation.Visible = True
            ' response.write("You Are here")
            lblErrorMsg.Text = "You have added the new Booker Master <br> The ID is <b>" & intSDid & "</b>"
            hyplnkretry.Text = "Add another Booker Master"
            hyplnkretry.NavigateUrl = "BookerMasterAdd.aspx"
        End If
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function
End Class