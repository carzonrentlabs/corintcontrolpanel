<%@ Page Language="vb" AutoEventWireup="false" Src="BookerMasterSearch.aspx.vb" Codebehind="BookerMasterSearch.aspx.vb" Inherits="BookerMasterSearch"%>
<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>CarzonRent :: Internal software Control panel</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="JavaScript" src="../utilityfunction.js"></script>
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
	</head>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<uc1:Headerctrl id="Headerctrl1" runat="server"></uc1:Headerctrl>
			<TABLE id="Table1" align="center">
				<TBODY>
					<TR>
						<TD align="center" colSpan="2"><STRONG><U>Edit a Booker Master</U></STRONG></TD>
					</TR>
					<TR>
						<TD>* Booker ID </TD>
						<TD>
							<asp:DropDownList id="BookerID" runat="server" CssClass="input"></asp:DropDownList></TD>
					</TR>
					<TR>
						<TD align="center" colSpan="2">
							<asp:Button ID="btnSubmit" Text="Serach" Runat="server"></asp:Button>
						</TD>
					</TR>
						
				</TBODY>
			</TABLE>
		</form>
	</body>
</html>
