Imports System
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
imports syste.web.mail
Namespace commonutility
    Public Class clsutility
        Implements IDisposable
        Private oConnection As SqlConnection
        Private disposed As Boolean = False
        Public Sub New()
            Dim strConnection As String = System.Configuration.ConfigurationSettings.AppSettings("corConnectString")
            oConnection = New SqlConnection(strConnection)
        End Sub
        Public Function funcGetSQLDataReader(ByVal strSQL) As SqlDataReader
            Dim oCommand = New SqlCommand(strSQL, oConnection)
            oConnection.Open()
            Try
                Return oCommand.ExecuteReader(CommandBehavior.CloseConnection)
            Catch

            Finally
                oCommand.Dispose()

            End Try
        End Function

        public function funcpopulatenumddw(ByVal uppperlimit As int32, ByVal lowerlimit As int32, ByVal DDWControl As System.Web.UI.WebControls.DropDownList)
           DDWControl.Items.Insert(0, New ListItem("", ""))
           Dim intcounter As Int32
           dim intstvalue as int32=1
          For intcounter = lowerlimit To uppperlimit

               DDWControl.Items.Insert(intstvalue, New ListItem(intcounter, intcounter))
                intstvalue=intstvalue+1
          Next

        end function


     

        Public Overloads Sub Dispose() Implements IDisposable.Dispose
            Dispose(True)
            ' This object will be cleaned up by the Dispose method.
            ' Therefore, you should call GC.SupressFinalize to
            ' take this object off the finalization queue 
            ' and prevent finalization code for this object
            ' from executing a second time.
            GC.SuppressFinalize(Me)
        End Sub

        ' Dispose(bool disposing) executes in two distinct scenarios.
        ' If disposing equals true, the method has been called directly
        ' or indirectly by a user's code. Managed and unmanaged resources
        ' can be disposed.
        ' If disposing equals false, the method has been called by the 
        ' runtime from inside the finalizer and you should not reference 
        ' other objects. Only unmanaged resources can be disposed.
        Private Overloads Sub Dispose(ByVal disposing As Boolean)
            ' Check to see if Dispose has already been called.
            If Not Me.disposed Then
                ' If disposing equals true, dispose all managed 
                ' and unmanaged resources.
                If disposing Then
                    ' Dispose managed resources.
                    oConnection.Dispose()
                End If

                ' Call the appropriate methods to clean up 
                ' unmanaged resources here.
                ' If disposing is false, 
                ' only the following code is executed.
            End If
            disposed = True
        End Sub

        ' This finalizer will run only if the Dispose method 
        ' does not get called.
        ' It gives your base class the opportunity to finalize.
        ' Do not provide finalize methods in types derived from this class.
        Protected Overrides Sub Finalize()
            ' Do not re-create Dispose clean-up code here.
            ' Calling Dispose(false) is optimal in terms of
            ' readability and maintainability.
            Dispose(False)
            MyBase.Finalize()
        End Sub

    End Class
End Namespace

