Imports commonutility
Imports System.text
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Imports System.Web.UI
Public Class UnitEditResult
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tblRecDetail As System.Web.UI.WebControls.Table

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not Page.IsPostBack Then
            getvalue("Unitname")
        End If
    End Sub

    Sub getvalue(ByVal strorderby As String)
        Dim strquery As StringBuilder
        Dim dtrreader As SqlDataReader
        Dim accessdata As clsutility
        accessdata = New clsutility
        strquery = New StringBuilder("select * from CORIntCityTaxRateMaster ")
        If Request.QueryString("id") <> "-1" Then
            strquery.Append(" where unitid=" & Request.QueryString("id") & " ")
        End If
        strquery.Append(" order by " & strorderby & "")

        dtrreader = accessdata.funcGetSQLDataReader(strquery.ToString)
        While dtrreader.Read
            Dim Temprow As New TableRow
            Temprow.HorizontalAlign = HorizontalAlign.Center


            Dim Tempcell As New TableCell
            Tempcell.Controls.Add(New LiteralControl(dtrreader("UnitID") & ""))
            Temprow.Cells.Add(Tempcell)

            Dim Tempcel2 As New TableCell
            Tempcel2.Controls.Add(New LiteralControl(dtrreader("UnitName") & ""))
            Temprow.Cells.Add(Tempcel2)

            Dim Tempcel3 As New TableCell
            Tempcel3.Controls.Add(New LiteralControl(dtrreader("EffectiveDate") & ""))
            Temprow.Cells.Add(Tempcel3)

            Dim Tempcel4 As New TableCell
            Tempcel4.Controls.Add(New LiteralControl(dtrreader("servicetaxpercent") & ""))
            Temprow.Cells.Add(Tempcel4)

            Dim Tempcel9 As New TableCell
            Tempcel9.Controls.Add(New LiteralControl(dtrreader("SwachhBharatTaxPercent") & ""))
            Temprow.Cells.Add(Tempcel9)


            Dim Tempcel5 As New TableCell
            Tempcel5.Controls.Add(New LiteralControl(dtrreader("educesspercent") & ""))
            Temprow.Cells.Add(Tempcel5)

            Dim Tempcel6 As New TableCell
            Tempcel6.Controls.Add(New LiteralControl(dtrreader("hducesspercent") & ""))
            Temprow.Cells.Add(Tempcel6)


            Dim Tempcel7 As New TableCell
            Tempcel7.Controls.Add(New LiteralControl(dtrreader("dstpercent") & ""))
            Temprow.Cells.Add(Tempcel7)

            Dim Tempce19 As New TableCell
            Tempce19.Controls.Add(New LiteralControl(dtrreader("gstenabledyn") & ""))
            Temprow.Cells.Add(Tempce19)

            Dim Tempce21 As New TableCell
            Tempce21.Controls.Add(New LiteralControl(dtrreader("cgstpercent") & ""))
            Temprow.Cells.Add(Tempce21)

            Dim Tempce20 As New TableCell
            Tempce20.Controls.Add(New LiteralControl(dtrreader("sgstpercent") & ""))
            Temprow.Cells.Add(Tempce20)

            Dim Tempce22 As New TableCell
            Tempce22.Controls.Add(New LiteralControl(dtrreader("igstpercent") & ""))
            Temprow.Cells.Add(Tempce22)

            Dim Tempcel8 As New TableCell
            Tempcel8.Controls.Add(New LiteralControl("<a href=UnitEditForm.aspx?ID=" & dtrreader("CityTaxRateID") & " >Edit</a>"))
            Temprow.Cells.Add(Tempcel8)


            'adding the Tables rows to the table
            tblRecDetail.Rows.Add(Temprow)

        End While
        dtrreader.Close()
    End Sub
    Sub SortGird(ByVal sender As Object, ByVal e As System.EventArgs)
        Select Case sender.id
            Case "Linkbutton1"
                getvalue("UnitID")
            Case "Linkbutton2"
                getvalue("UnitName")
            Case "Linkbutton3"
                getvalue("EffectiveDate")
            Case "Linkbutton4"
                getvalue("servicetaxpercent")
            Case "Linkbutton9"
                getvalue("SwachhBharatTaxPercent")
            Case "Linkbutton5"
                getvalue("educesspercent")
            Case "Linkbutton6"
                getvalue("hducesspercent")
            Case "Linkbutton7"
                getvalue("dstpercent")


        End Select

    End Sub

End Class
