<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" Src="UnitEditSearch.aspx.vb" Inherits="UnitEditSearch" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>CarzonRent :: Internal software Control panel</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <link href="../HertzInt.css" type="text/css" rel="stylesheet">
</head>
<body>
    <form id="Form1" method="post" runat="server">
    <uc1:Headerctrl ID="Headerctrl1" runat="server"></uc1:Headerctrl>
    <table align="center">
        <tr>
            <td colspan="2" align="center">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <strong><u>Edit a Unit Taxes where</u></strong>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                Unit name
            </td>
            <td>
                <asp:DropDownList ID="ddUnitName" runat="server" CssClass="input">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="button"></asp:Button>&nbsp;
                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="button"></asp:Button>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
