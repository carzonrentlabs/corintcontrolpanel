<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" Src="UnitAddForm.aspx.vb" Inherits="UnitAddForm" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>CarzonRent :: Internal software Control Panel</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="../HertzInt.css" type="text/css" rel="stylesheet">

    <script language="JavaScript" src="../utilityfunction.js"></script>

    <%--<script language="JavaScript" src="../JScripts/Datefunc.js"></script>--%>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

    <script language="JavaScript">
        
        $(document).ready(function () {
            $("#<%=txtEffectiveDate.ClientID%>").datepicker();
        });

        function validate_input() {

            if (document.forms[0].ddlUnitCty.value == "") {
                alert("Please make sure all the fields marked with * are filled in.")
                document.forms[0].ddlUnitCty.focus();
                return false;
            }

            //	if(document.forms[0].txtservicetx.value=="")
            //	{
            //		alert("Please make sure all the fields marked with * are filled in.")
            //		document.forms[0].txtservicetx.focus();
            //		return false;
            //	}

            //	if(isNaN(document.forms[0].txtservicetx.value))
            //	{
            //		alert("Service Tax % should be numeric and upto two decimal digits only.")
            //		return false;	
            //	}
            //	else
            //	{
            //		if (parseFloat(document.forms[0].txtservicetx.value.indexOf("."))>0)
            //		{
            //			var FlotVal
            //			FlotVal=document.forms[0].txtservicetx.value.substr(parseFloat(document.forms[0].txtservicetx.value.indexOf(".")),document.forms[0].txtservicetx.value.length)
            //			if(parseFloat((FlotVal.length)-1)>2)
            //			{
            //				alert("Service Tax % should be numeric and upto two decimal digits only")
            //				return false;
            //			}
            //		}
            //	}

            //	if(document.forms[0].txtSwachhBharatTax.value=="")
            //	{
            //		alert("Please make sure all the fields marked with * are filled in.")
            //		document.forms[0].txtSwachhBharatTax.focus();
            //		return false;
            //	}
            //		
            //	if(isNaN(document.forms[0].txtSwachhBharatTax.value))
            //	{
            //		alert("Swachh Bharat Tax % should be numeric and upto two decimal digits only.")
            //		return false;	
            //	}
            //	else
            //	{
            //		if (parseFloat(document.forms[0].txtSwachhBharatTax.value.indexOf("."))>0)
            //		{
            //			var FlotVal
            //			FlotVal=document.forms[0].txtSwachhBharatTax.value.substr(parseFloat(document.forms[0].txtSwachhBharatTax.value.indexOf(".")),document.forms[0].txtSwachhBharatTax.value.length)
            //			if(parseFloat((FlotVal.length)-1)>2)
            //			{
            //				alert("Swachh Bharat Tax % should be numeric and upto two decimal digits only")
            //				return false;
            //			}
            //		}
            //	}

            //		
            //	if(document.forms[0].txtedut.value=="")
            //	{
            //		alert("Please make sure all the fields marked with * are filled in.")
            //		document.forms[0].txtedut.focus();
            //		return false;
            //	}

            //	if(document.forms[0].txthdut.value=="")
            //	{
            //		alert("Please make sure all the fields marked with * are filled in.")
            //		document.forms[0].txthdut.focus();
            //		return false;
            //	}
            //		
            //	if(isNaN(document.forms[0].txtedut.value))
            //	{
            //		alert("Education Cess % should be numeric and upto two decimal digits only.")
            //		document.forms[0].txtedut.focus();
            //		return false;	
            //	}
            //	else
            //	{
            //		if (parseFloat(document.forms[0].txtedut.value.indexOf("."))>0)
            //		{
            //			var FlotVal
            //			FlotVal=document.forms[0].txtedut.value.substr(parseFloat(document.forms[0].txtedut.value.indexOf(".")),document.forms[0].txtedut.value.length)
            //			if(parseFloat((FlotVal.length)-1)>2)
            //			{
            //				alert("Education Cess % should be numeric and upto two decimal digits only")
            //				return false;
            //			}
            //		}
            //	}
            //			
            //	if(isNaN(document.forms[0].txthdut.value))
            //	{
            //		alert("Higher & Secondary Education Cess % should be numeric and upto two decimal digits only.")
            //		return false;	
            //	}
            //	else
            //	{
            //		if (parseFloat(document.forms[0].txthdut.value.indexOf("."))>0)
            //		{
            //			var FlotVal
            //			FlotVal=document.forms[0].txthdut.value.substr(parseFloat(document.forms[0].txthdut.value.indexOf(".")),document.forms[0].txthdut.value.length)
            //			if(parseFloat((FlotVal.length)-1)>2)
            //			{
            //				alert("Higher & Secondary Education Cess % should be numeric and upto two decimal digits only.")
            //				return false;
            //			}
            //		}
            //	}



            if (document.forms[0].txtEffectiveDate.value == "") {
                alert("Please make sure all the fields marked with * are filled in.")
                document.forms[0].txtEffectiveDate.focus();
                return false;
            }


            if (document.forms[0].txtCgst.value == "") {
                alert("Please make sure all the fields marked with * are filled in.")
                document.forms[0].txtCgst.focus();
                return false;
            }

            if (document.forms[0].txtSgst.value == "") {
                alert("Please make sure all the fields marked with * are filled in.")
                document.forms[0].txtSgst.focus();
                return false;
            }

            if (document.forms[0].txtIgst.value == "") {
                alert("Please make sure all the fields marked with * are filled in.")
                document.forms[0].txtIgst.focus();
                return false;
            }

            if (isNaN(document.forms[0].txtCgst.value)) {
                alert("CGST % should be numeric and upto two decimal digits only.")
                document.forms[0].txtCgst.focus();
                return false;
            }
            else {
                if (parseFloat(document.forms[0].txtCgst.value.indexOf(".")) > 0) {
                    var FlotVal
                    FlotVal = document.forms[0].txtCgst.value.substr(parseFloat(document.forms[0].txtCgst.value.indexOf(".")), document.forms[0].txtCgst.value.length)
                    if (parseFloat((FlotVal.length) - 1) > 2) {
                        alert("CGST % should be numeric and upto two decimal digits only")
                        document.forms[0].txtCgst.focus();
                        return false;
                    }
                }
            }

            if (isNaN(document.forms[0].txtSgst.value)) {
                alert("SGST % should be numeric and upto two decimal digits only.")
                document.forms[0].txtSgst.focus();
                return false;
            }
            else {
                if (parseFloat(document.forms[0].txtSgst.value.indexOf(".")) > 0) {
                    var FlotVal
                    FlotVal = document.forms[0].txtSgst.value.substr(parseFloat(document.forms[0].txtSgst.value.indexOf(".")), document.forms[0].txtSgst.value.length)
                    if (parseFloat((FlotVal.length) - 1) > 2) {
                        alert("SGST % should be numeric and upto two decimal digits only")
                        document.forms[0].txtSgst.focus();
                        return false;
                    }
                }
            }

            if (isNaN(document.forms[0].txtIgst.value)) {
                alert("IGST % should be numeric and upto two decimal digits only.")
                document.forms[0].txtIgst.focus();
                return false;
            }
            else {
                if (parseFloat(document.forms[0].txtIgst.value.indexOf(".")) > 0) {
                    var FlotVal
                    FlotVal = document.forms[0].txtIgst.value.substr(parseFloat(document.forms[0].txtIgst.value.indexOf(".")), document.forms[0].txtIgst.value.length)
                    if (parseFloat((FlotVal.length) - 1) > 2) {
                        alert("IGST % should be numeric and upto two decimal digits only")
                        document.forms[0].txtIgst.focus();
                        return false;
                    }
                }
            }

            //        if (document.forms[0].txtdst.value == "") {
            //            alert("Please make sure all the fields marked with * are filled in.")
            //            document.forms[0].txtdst.focus();
            //            return false;
            //        }

            //        if (isNaN(document.forms[0].txtdst.value)) 
            //        {
            //            alert("DST % should be numeric and upto three decimal digits only.")
            //            return false;
            //        }
            //        else 
            //        {
            //                if (parseFloat(document.forms[0].txtdst.value.indexOf(".")) > 0) 
            //                {
            //                var FlotVal
            //                FlotVal = document.forms[0].txtdst.value.substr(parseFloat(document.forms[0].txtdst.value.indexOf(".")), document.forms[0].txtdst.value.length)
            //                if (parseFloat((FlotVal.length) - 1) > 3) 
            //                {
            //                    alert("DST % should be numeric and upto three decimal digits only")
            //                    return false;
            //                }
            //            }
            //        }
        }

        function dateReg(obj) {
            if (obj.value != "") {
                // alert(obj.value);
                var reg = /((0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[0-1])-(201[0-9]))/
                if (reg.test(obj.value)) {
                    //alert('valid');
                }
                else {
                    alert('notvalid');
                    obj.value = "";
                }
            }
        }
    </script>

</head>
<body>
    <form id="Form1" method="post" runat="server">
        <uc1:Headerctrl ID="Headerctrl1" runat="server"></uc1:Headerctrl>
        <table align="center">
            <tbody>
                <tr>
                    <td align="center">
                        <b><u>Add a Unit Tax </u></b>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="lblErrorMsg" CssClass="subRedHead" Visible="false" runat="server"></asp:Label></td>
                </tr>
                <asp:Panel ID="pnlmainform" runat="server">
                    <tr>
                        <td>* Unit City Name
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlUnitCty" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>Service Tax %</td>
                        <td>
                            <asp:TextBox ID="txtservicetx" runat="server" CssClass="input" MaxLength="10" Enabled="false" Text="0"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>Swachh Bharat Tax %</td>
                        <td>
                            <asp:TextBox ID="txtSwachhBharatTax" runat="server" CssClass="input" MaxLength="10" Enabled="false" Text="0"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>Education Cess %</td>
                        <td>
                            <asp:TextBox ID="txtedut" runat="server" CssClass="input" MaxLength="10" Enabled="false" Text="0"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>Higher & Secondary Education Cess %</td>
                        <td>
                            <asp:TextBox ID="txthdut" runat="server" CssClass="input" MaxLength="10" Enabled="false" Text="0"></asp:TextBox></td>
                    </tr>

                    <%--<tr>
                <td>
                    GSTYN
                    <asp:RadioButtonList ID="rdGstYN" runat="server" RepeatDirection="Horizontal"
                        AutoPostBack="true">
                        <asp:ListItem Text="Yes" Value="1" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="No" Value="2"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>--%>

                    <tr>
                        <td style="height: 24px" valign="top">GSTYN
                        </td>
                        <td style="height: 24px">
                            <asp:RadioButton ID="rdStatus1" runat="server" Text="Yes" GroupName="ActiveStatus" Enabled="false"
                                Checked="True"></asp:RadioButton>
                            <asp:RadioButton ID="rdStatus2" runat="server" Text="No"
                                GroupName="ActiveStatus"></asp:RadioButton>
                        </td>
                    </tr>

                    <tr>
                        <td>*CGST %</td>
                        <td>
                            <asp:TextBox ID="txtCgst" runat="server" CssClass="input" MaxLength="10" Enabled="true"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>*SGST %</td>
                        <td>
                            <asp:TextBox ID="txtSgst" runat="server" CssClass="input" MaxLength="10" Enabled="true"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>*IGST %</td>
                        <td>
                            <asp:TextBox ID="txtIgst" runat="server" CssClass="input" MaxLength="10" Enabled="true"></asp:TextBox></td>
                    </tr>


                    <tr>
                        <td>DST %</td>
                        <td>
                            <asp:TextBox ID="txtdst" runat="server" CssClass="input" MaxLength="10" Enabled="false" Text="0"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>* Effective Date</td>
                        <td>
                            <asp:TextBox ID="txtEffectiveDate" runat="server" CssClass="input"></asp:TextBox>
                            <%--<asp:TextBox ID="txtEffectiveDate" runat="server" CssClass="input" onblur="dateReg(this);"
                                Width="75px" MaxLength="50"></asp:TextBox><a onmouseover="window.status='Date Picker';return true;"
                                    onmouseout="window.status='';return true;" href="javascript:show_calendar('Form1.txtEffectiveDate');"><img
                                        height="21" src="../images/show-calendar.gif" width="24" border="0"></a>--%>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <asp:Button ID="btnSubmit" runat="server" CssClass="input" Text="Submit"></asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;
                            <input type="Reset" cssclass="button" name="Reset" value="Reset" /></td>
                    </tr>
                </asp:Panel>
                <asp:Panel ID="pnlconfirmation" Visible="False" runat="server">
                    <tr align="center">
                        <td colspan="2">
                            <br>
                            <br>
                            <br>
                            <asp:Label ID="lblMessage" runat="server"></asp:Label></td>
                    </tr>
                    <tr align="center">
                        <td colspan="2">
                            <asp:HyperLink ID="hyplnkretry" runat="server"></asp:HyperLink></td>
                    </tr>
                </asp:Panel>
            </tbody>
        </table>
    </form>
</body>
</html>
