Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class UnitEditForm
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ddlUnitCty As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtEffectiveDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtfname As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtservicetx As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtedut As System.Web.UI.WebControls.TextBox
    Protected WithEvents txthdut As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtdst As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtarearemarks As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    Protected WithEvents txtSwachhBharatTax As System.Web.UI.WebControls.TextBox

    Protected WithEvents txtCgst As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtSgst As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtIgst As System.Web.UI.WebControls.TextBox
    'Protected WithEvents rdGstYN As System.Web.UI.WebControls.RadioButtonList

    Protected WithEvents rdStatus1 As System.Web.UI.WebControls.RadioButton
    Protected WithEvents rdStatus2 As System.Web.UI.WebControls.RadioButton

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btnSubmit.Attributes("onClick") = "return validate_input();"
        If Not Page.IsPostBack Then
            populateddl()

            Dim dtrreader As SqlDataReader
            Dim accessdata As clsutility
            accessdata = New clsutility
            dtrreader = accessdata.funcGetSQLDataReader("select * from CORIntCityTaxRateMaster where CityTaxRateID=" & Request.QueryString("id") & " ")
            dtrreader.Read()

            'txtrefname.Text = dtrreader("unitname") & ""
            'ddlUnitCty.SelectedItem.Value = dtrreader
            ddlUnitCty.Items.FindByValue(dtrreader("UnitID")).Selected = True
            ddlUnitCty.Enabled = False
            txtEffectiveDate.Text = dtrreader("EffectiveDate")
            txtservicetx.Text = dtrreader("servicetaxpercent") & ""
            txtSwachhBharatTax.Text = dtrreader("SwachhBharatTaxPercent") & ""

            txtedut.Text = dtrreader("educesspercent") & ""
            txthdut.Text = dtrreader("Hducesspercent") & ""

            Dim activeStatus As String
            activeStatus = dtrreader("gstenabledYN") & ""

            If activeStatus = "True" Then
                rdStatus1.Checked = True
                rdStatus2.Checked = False
            Else
                rdStatus1.Checked = False
                rdStatus2.Checked = True
            End If

            ' txtedut.Text = dtrreader("educesspercent") & ""
            txtSgst.Text = dtrreader("sgstpercent") & ""
            txtCgst.Text = dtrreader("cgstpercent") & ""
            txtIgst.Text = dtrreader("igstpercent") & ""

            txtdst.Text = dtrreader("dstpercent") & ""
            dtrreader.Close()
            accessdata.Dispose()

        End If
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        Dim intuniqcheck As SqlParameter
        Dim intuniqvalue As int32

        cmd = New SqlCommand("prc_EditUnitTaxRate", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@CityTaxRateID", Request.QueryString("id"))
        'cmd.Parameters.Add("@UnitID", ddlUnitCty.SelectedItem.Value)
        'cmd.Parameters.Add("@UnitName", ddlUnitCty.SelectedItem.Text)
        cmd.Parameters.Add("@servicetaxpercent ", txtservicetx.Text)
        cmd.Parameters.Add("@educesspercent ", txtedut.Text)
        cmd.Parameters.Add("@hducesspercent ", txthdut.Text)

        Dim activeStatus As Integer
        If rdStatus1.Checked = True Then
            activeStatus = 1
        Else
            activeStatus = 0
        End If

        cmd.Parameters.Add("@gstenabledyn ", activeStatus)
        cmd.Parameters.Add("@sgstpercent ", txtSgst.Text)
        cmd.Parameters.Add("@cgstpercent ", txtCgst.Text)
        cmd.Parameters.Add("@igstpercent ", txtIgst.Text)

        cmd.Parameters.Add("@dstpercent ", txtdst.Text)
        cmd.Parameters.Add("@modifiedby", Session("loggedin_user"))
        cmd.Parameters.Add("@EffectiveDate", txtEffectiveDate.Text)
        cmd.Parameters.Add("@SwachhBharatTax ", txtSwachhBharatTax.Text)
        intuniqcheck = cmd.Parameters.Add("@uniqcheckval", SqlDbType.Int)
        intuniqcheck.Direction = ParameterDirection.Output
        '  Try
        MyConnection.Open()
        cmd.ExecuteNonQuery()
        intuniqvalue = cmd.Parameters("@uniqcheckval").Value
        MyConnection.Close()
        If Not intuniqvalue = 0 Then
            lblErrorMsg.visible = True
            lblErrorMsg.text = "Unit already exist."
            Exit Sub
        Else
            lblErrorMsg.visible = False
            pnlmainform.Visible = False
            pnlconfirmation.Visible = True

            lblMessage.Text = "You have updated the Unit successfully"
            hyplnkretry.Text = "Edit another Unit"
            hyplnkretry.NavigateUrl = "UnitEditSearch.aspx"
        End If
    End Sub

    Sub populateddl()
        Dim dtrreader As SqlDataReader
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility

        ddlUnitCty.DataSource = objAcessdata.funcGetSQLDataReader("select UnitID ,Unitname from CorIntUnitMaster where active = 1 order by Unitname")
        ddlUnitCty.DataValueField = "UnitID"
        ddlUnitCty.DataTextField = "Unitname"
        ddlUnitCty.DataBind()
        ddlUnitCty.Items.Insert(0, New ListItem("", ""))
        objAcessdata.Dispose()

    End Sub

End Class
