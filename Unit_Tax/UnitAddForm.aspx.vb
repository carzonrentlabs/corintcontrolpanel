Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System

Public Class UnitAddForm
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    Protected WithEvents txtEffectiveDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtservicetx As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label

    Protected WithEvents txtsvtax As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtedut As System.Web.UI.WebControls.TextBox
    Protected WithEvents txthdut As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtdst As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlDesig As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlUnitCty As System.Web.UI.WebControls.DropDownList
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    Protected WithEvents txtSwachhBharatTax As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtCgst As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtSgst As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtIgst As System.Web.UI.WebControls.TextBox
    'Protected WithEvents rdGstYN As System.Web.UI.WebControls.RadioButtonList

    Protected WithEvents rdStatus1 As System.Web.UI.WebControls.RadioButton
    Protected WithEvents rdStatus2 As System.Web.UI.WebControls.RadioButton


    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btnSubmit.Attributes("onClick") = "return validate_input();"
        If Not Page.IsPostBack Then
            populateddl()
        End If

    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        Dim intuniqcheck As SqlParameter
        Dim intuniqvalue as int32

        cmd = New SqlCommand("procAddUnitTaxRate", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@unitname", ddlUnitCty.SelectedItem.Text)
        cmd.Parameters.Add("@unitid", ddlUnitCty.SelectedItem.Value)
        cmd.Parameters.Add("@servicetaxpercent", txtservicetx.Text)
        cmd.Parameters.Add("@educesspercent", txtedut.Text)
        cmd.Parameters.Add("@hducesspercent", txthdut.Text)

         Dim activeStatus As Integer
        If rdStatus1.Checked = True Then
            activeStatus = 1
        Else
            activeStatus = 0
        End If

        cmd.Parameters.Add("@gstenabledyn", activeStatus)

        cmd.Parameters.Add("@cgstpercent", txtCgst.Text)
        cmd.Parameters.Add("@sgstpercent", txtSgst.Text)
        cmd.Parameters.Add("@igstpercent", txtIgst.Text)

        cmd.Parameters.Add("@dstpercent", txtdst.Text)
        cmd.Parameters.Add("@createdby", Session("loggedin_user"))
        cmd.Parameters.Add("@EffectiveDate", txtEffectiveDate.Text)
        cmd.Parameters.Add("@SwachhBharatTax", txtSwachhBharatTax.Text)
        intuniqcheck = cmd.Parameters.Add("@uniqcheckval", SqlDbType.Int)
        intuniqcheck.Direction = ParameterDirection.Output

        MyConnection.Open()
        cmd.ExecuteNonQuery()
        intuniqvalue=cmd.Parameters("@uniqcheckval").Value
        MyConnection.Close()
          if not intuniqvalue = 0 then
                lblErrorMsg.visible=true
                lblErrorMsg.text="Unit already exist."
                exit sub
          else
		        lblErrorMsg.visible=false
                pnlmainform.Visible = False
                pnlconfirmation.Visible = True

                lblMessage.Text = "You have added the Unit successfully"
                hyplnkretry.Text = "Add another Unit"
                hyplnkretry.NavigateUrl = "UnitAddForm.aspx"
         end if

    End Sub

    Sub populateddl()
        Dim dtrreader As SqlDataReader
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility

        ddlUnitCty.DataSource = objAcessdata.funcGetSQLDataReader("select UnitID, Unitname from CORIntUnitMaster  where active=1  order by Unitname")
        ddlUnitCty.DataValueField = "UnitID"
        ddlUnitCty.DataTextField = "unitname"
        ddlUnitCty.DataBind()
        ddlUnitCty.Items.Insert(0, New ListItem("", ""))
        objAcessdata.Dispose()

    End Sub

End Class
