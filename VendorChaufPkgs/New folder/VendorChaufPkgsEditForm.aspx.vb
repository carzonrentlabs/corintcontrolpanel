Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class VendorChaufPkgsEditForm
    Inherits System.Web.UI.Page
    Dim txthr As String

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ddlvendorname As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlcarcat As System.Web.UI.WebControls.DropDownList
    Protected WithEvents chkairport As System.Web.UI.WebControls.CheckBox
    Protected WithEvents ddlcityname As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlpackagehr As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlpkgkm As System.Web.UI.WebControls.DropDownList
    Protected WithEvents chkoutstation As System.Web.UI.WebControls.CheckBox
    Protected WithEvents txtrate As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtratehr As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtratekm As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtxtraHr As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtExtraKM As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtoutallowance As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtnighstay As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtRemarks As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkactive As System.Web.UI.WebControls.CheckBox
    Protected WithEvents btnsubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnreset As System.Web.UI.WebControls.Button
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    Protected WithEvents ddlHr As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlMin As System.Web.UI.WebControls.DropDownList

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btnsubmit.Attributes("onClick") = "return validation();"
        If Not Page.IsPostBack Then
            populateddl()
            Dim dtrreader As SqlDataReader
            Dim accessdata As clsutility
            accessdata = New clsutility
            Dim valDisc As String
            Dim valDiscDec As String
            Dim arrDisc As Array

            dtrreader = accessdata.funcGetSQLDataReader("select apttransyn,outstationyn,carvendorid,carcatid,cityid,pkghrs,pkgkms,pkgrate,extrahrrate,extrakmrate,thresholdextrahr,thresholdextrakm,outstationallowance,nightstayallowance,remarks,isnull(active,0) as active,isnull(MonthlyBillingYN,0) as MonthlyBillingYN  from CORIntVendorChaufPkgsMaster   where  vendorpkgid=" & Request.QueryString("id") & " ")
            dtrreader.Read()

            chkairport.Checked = dtrreader("apttransyn")
            chkoutstation.Checked = dtrreader("outstationyn")
            autoselec_ddl(ddlvendorname, dtrreader("carvendorid"))
            autoselec_ddl(ddlcarcat, dtrreader("carcatid"))

	    If IsDBNull(dtrreader("cityid")) = True Then
                ddlcityname.SelectedValue = ""
            Else
                autoselec_ddl(ddlcityname, dtrreader("cityid"))
            End If

            'autoselec_ddl(ddlcityname, dtrreader("cityid"))
            autoselec_ddl(ddlpackagehr, dtrreader("pkghrs"))
            autoselec_ddl(ddlpkgkm, dtrreader("pkgkms"))
            txtrate.Text = dtrreader("pkgrate") & ""
            txtratehr.Text = dtrreader("extrahrrate") & ""
            txtratekm.Text = dtrreader("extrakmrate") & ""
            txthr = dtrreader("thresholdextrahr") & ""

            arrDisc = CStr(txthr).Split(".")

            If arrDisc.Length = 2 Then
                valDisc = arrDisc(0)
                valDiscDec = arrDisc(1)
                If valDisc = "" Then valDisc = "0"
                If valDiscDec = "00" Or valDiscDec = "" Then valDiscDec = "0"
            End If

            autoselec_ddl(ddlHr, valDisc)
            autoselec_ddl(ddlMin, valDiscDec)

            txtExtraKM.Text = dtrreader("thresholdextrakm") & ""
            txtoutallowance.Text = dtrreader("outstationallowance") & ""
            txtnighstay.Text = dtrreader("nightstayallowance") & ""
            txtRemarks.Text = dtrreader("remarks") & ""
            chkactive.Checked = dtrreader("active")

            dtrreader.Close()
            accessdata.Dispose()
        End If
    End Sub
    Sub populateddl()
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility



        ddlvendorname.DataSource = objAcessdata.funcGetSQLDataReader("select CarVendorName   ,CarVendorID   from CORIntCarVendorMaster    where active=1 order by CarVendorName")
        ddlvendorname.DataValueField = "CarVendorID"
        ddlvendorname.DataTextField = "CarVendorName"
        ddlvendorname.DataBind()
        ddlvendorname.Items.Insert(0, New ListItem("", ""))

        ddlcarcat.DataSource = objAcessdata.funcGetSQLDataReader("select CarCatName ,CarCatID  from CORIntCarCatMaster   where active=1 order by CarCatName")
        ddlcarcat.DataValueField = "CarCatID"
        ddlcarcat.DataTextField = "CarCatName"
        ddlcarcat.DataBind()
        ddlcarcat.Items.Insert(0, New ListItem("", ""))

        ddlcityname.DataSource = objAcessdata.funcGetSQLDataReader("select CityName  ,CityID   from CORIntCityMaster   where active=1 order by CityName")
        ddlcityname.DataValueField = "CityID"
        ddlcityname.DataTextField = "CityName"
        ddlcityname.DataBind()
        ddlcityname.Items.Insert(0, New ListItem("", ""))


        objAcessdata.funcpopulatenumddw(1000, 0, ddlpackagehr)
        objAcessdata.funcpopulatenumddw(5000, 0, ddlpkgkm)
        objAcessdata.Dispose()

    End Sub


    Function autoselec_ddl(ByVal ddlname As DropDownList, ByVal selectvalue As Int32)
        If Not selectvalue = "0" Then
            ddlname.Items.FindByValue(selectvalue).Selected = True
        End If
    End Function


    Private Sub btnsubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        cmd = New SqlCommand("procEditVendorChaufPkgsMaster", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("@rowid", Request.QueryString("id"))
        cmd.Parameters.Add("@carvendorid", ddlvendorname.SelectedItem.Value)
        cmd.Parameters.Add("@carcatid", ddlcarcat.SelectedItem.Value)
        cmd.Parameters.Add("@apttransyn", get_YNvalue(chkairport))
        cmd.Parameters.Add("@cityid", ddlcityname.SelectedItem.Value)
        If Not ddlpackagehr.SelectedItem.Value = "" Then
            cmd.Parameters.Add("@pkghrs", ddlpackagehr.SelectedItem.Value)
        End If
        If Not ddlpkgkm.SelectedItem.Value = "" Then
            cmd.Parameters.Add("@pkgkms", ddlpkgkm.SelectedItem.Value)
        End If
        cmd.Parameters.Add("@outstationyn", get_YNvalue(chkoutstation))

        cmd.Parameters.Add("@pkgrate", txtrate.Text)

        If Not txtratehr.Text = "" Then
            cmd.Parameters.Add("@extrahrrate", txtratehr.Text)
        End If
        If Not txtratekm.Text = "" Then
            cmd.Parameters.Add("@extrakmrate", txtratekm.Text)
        End If
        txthr = ddlHr.SelectedItem.Value + "." + ddlMin.SelectedItem.Value
        cmd.Parameters.Add("@thresholdextrahr", txthr)

        If Not txtExtraKM.Text = "" Then
            cmd.Parameters.Add("@thresholdextrakm", txtExtraKM.Text)
        End If
        If Not txtoutallowance.Text = "" Then
            cmd.Parameters.Add("@outstationallowance", txtoutallowance.Text)
        End If
        If Not txtnighstay.Text = "" Then
            cmd.Parameters.Add("@nightstayallowance", txtnighstay.Text)
        End If

        cmd.Parameters.Add("@remarks", txtRemarks.Text)
        cmd.Parameters.Add("@active", get_YNvalue(chkactive))
        cmd.Parameters.Add("@modifiedby", Session("loggedin_user"))
'		cmd.Parameters.Add("@cityTransfer",0)

        '  Try
        MyConnection.Open()
        cmd.ExecuteNonQuery()
        MyConnection.Close()
        pnlmainform.Visible = False
        pnlconfirmation.Visible = True

        lblMessage.Text = "You have updated the Vendor Chauffeur Package successfully"
        hyplnkretry.Text = "Edit another Vendor Chauffeur Package"
        hyplnkretry.NavigateUrl = "VendorChaufPkgsEditSearch.aspx"
        '   Catch
        '   End Try
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function

End Class
