<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" src="VendorChaufPkgsEditForm.aspx.vb" Inherits="VendorChaufPkgsEditForm"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>VendorChaufPkgsEditForm</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="JavaScript" src="../utilityfunction.js"></script>
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
		<script language="javascript">
		function validation()
		{
	
				if(isNaN(document.forms[0].txtrate.value))
					{
						alert("Rate should be numeric and upto two decimal digits only.")
						return false;	
					}
					else
					{
					if (parseFloat(document.forms[0].txtrate.value.indexOf("."))>0)
						{
							var FlotVal
							FlotVal=document.forms[0].txtrate.value.substr(parseFloat(document.forms[0].txtrate.value.indexOf(".")),document.forms[0].txtrate.value.length)
							if(parseFloat((FlotVal.length)-1)>2)
							{
							alert("Rate should be numeric and upto two decimal digits only")
							return false;
							}
						}	
					}
				if(isNaN(document.forms[0].txtratehr.value))
					{
						alert("Rate / Extra Hr should be numeric and upto two decimal digits only.")
						return false;	
					}
					else
					{
					if (parseFloat(document.forms[0].txtratehr.value.indexOf("."))>0)
						{
							var FlotVal
							FlotVal=document.forms[0].txtratehr.value.substr(parseFloat(document.forms[0].txtratehr.value.indexOf(".")),document.forms[0].txtratehr.value.length)
							if(parseFloat((FlotVal.length)-1)>2)
							{
							alert("Rate / Extra Hr should be numeric and upto two decimal digits only")
							return false;
							}

						}	
					}					
					
				if(isNaN(document.forms[0].txtratekm.value))
					{
						alert("Rate / Extra KM should be numeric and upto two decimal digits only.")
						return false;	
					}
					else
					{
					if (parseFloat(document.forms[0].txtratekm.value.indexOf("."))>0)
						{
							var FlotVal
							FlotVal=document.forms[0].txtratekm.value.substr(parseFloat(document.forms[0].txtratekm.value.indexOf(".")),document.forms[0].txtratekm.value.length)
							if(parseFloat((FlotVal.length)-1)>2)
							{
							alert("Rate / Extra KM should be numeric and upto two decimal digits only")
							return false;
							}

						}	
					}		
				if(isNaN(document.forms[0].txtratehr.value))
					{
						alert("Threshold Extra Hr (Crosses to next package if reached) should be numeric only.")
						return false;	
					}

				if(isNaN(document.forms[0].txtExtraKM.value))
					{
						alert("Threshold Extra KM (Crosses to next package if reached) should be numeric only.")
						return false;	
					}

				if(isNaN(document.forms[0].txtoutallowance.value))
					{
						alert("Outstation Allowance should be numeric and upto two decimal digits only.")
						return false;	
					}
					else
					{
					if (parseFloat(document.forms[0].txtoutallowance.value.indexOf("."))>0)
						{
							var FlotVal
							FlotVal=document.forms[0].txtoutallowance.value.substr(parseFloat(document.forms[0].txtoutallowance.value.indexOf(".")),document.forms[0].txtoutallowance.value.length)
							if(parseFloat((FlotVal.length)-1)>2)
							{
							alert("Outstation Allowance should be numeric and upto two decimal digits only")
							return false;
							}

						}	
					}		
				if(isNaN(document.forms[0].txtnighstay.value))
					{
						alert("Night Stay Allowance should be numeric and upto two decimal digits only.")
						return false;	
					}
					else
					{
					if (parseFloat(document.forms[0].txtnighstay.value.indexOf("."))>0)
						{
							var FlotVal
							FlotVal=document.forms[0].txtnighstay.value.substr(parseFloat(document.forms[0].txtnighstay.value.indexOf(".")),document.forms[0].txtnighstay.value.length)
							if(parseFloat((FlotVal.length)-1)>2)
							{
							alert("Night Stay Allowance should be numeric and upto two decimal digits only")
							return false;
							}

						}	
					}

					//if(document.forms[0].chkairport.checked==true)
					//{
					//if(document.forms[0].ddlcityname.value=="")
					//	{
					//	alert("Please select city name")
					//	return false;
					//	}
					//}


			var strvalues
			strvalues=('ddlvendorname,ddlcarcat,ddlpackagehr,ddlpkgkm,txtrate')
			return checkmandatory(strvalues);		
		}		
		function airportcheckbox()
		{
		
				if(document.forms[0].chkoutstation.checked==true)
					{
					document.forms[0].chkairport.checked=false;
					document.forms[0].ddlcityname.disabled=true
					document.forms[0].txtoutallowance.disabled=false;
					document.forms[0].txtnighstay.disabled=false;
					document.forms[0].ddlpackagehr.value=1;
					document.forms[0].ddlpackagehr.disabled=true;
					document.forms[0].txtratehr.disabled=true;
					document.forms[0].ddlHr.disabled=true;
					document.forms[0].ddlMin.disabled=true;	
					document.forms[0].txtExtraKM.disabled=true;
					}
				else
					{
					document.forms[0].ddlpackagehr.value=0;
					document.forms[0].ddlpackagehr.disabled=false;
					document.forms[0].txtratehr.disabled=false;
					document.forms[0].ddlHr.disabled=false;
					document.forms[0].ddlMin.disabled=false;	
					document.forms[0].txtExtraKM.disabled=false;
					}	
		}
		function enablecity()
		{
		
				if(document.forms[0].chkairport.checked==true)
					{
					document.forms[0].ddlcityname.disabled=false
					document.forms[0].chkoutstation.checked=false;
					document.forms[0].txtoutallowance.disabled=true;
					document.forms[0].txtnighstay.disabled=true;
					document.forms[0].ddlpackagehr.value=0;
					document.forms[0].ddlpackagehr.disabled=false;
					document.forms[0].txtratehr.disabled=false;
					document.forms[0].ddlHr.disabled=false;
					document.forms[0].ddlMin.disabled=false;	
					document.forms[0].txtExtraKM.disabled=false;
					}
				else
					{
					document.forms[0].ddlcityname.disabled=true
					document.forms[0].txtoutallowance.disabled=false;
					document.forms[0].txtnighstay.disabled=false;
					}	
		}
		function CalcOSRate()
        {
   		   if(document.forms[0].chkoutstation.checked==true)
           {
              if (document.forms[0].ddlpkgkm.value != "" && document.forms[0].txtratekm.value != "")
              {
                  document.forms[0].txtrate.value = parseFloat(document.forms[0].ddlpkgkm.value) * document.forms[0].txtratekm.value;
              }
           }
       }
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<uc1:Headerctrl id="Headerctrl1" runat="server"></uc1:Headerctrl>
			<TABLE id="Table1" align="center">
				<asp:panel id="pnlmainform" Runat="server">
					<TBODY>
						<TR>
							<TD align="center" colSpan="2"><STRONG><U>Edit a Vendor Package</U></STRONG>
							</TD>
						</TR>
						<TR>
							<TD align="center" colSpan="2">&nbsp;&nbsp;</TD>
						</TR>
						<TR>
							<TD>* Vendor Name
							</TD>
							<TD>
								<asp:DropDownList id="ddlvendorname" runat="server" Enabled="false" CssClass="input"></asp:DropDownList></TD>
						</TR>
						<TR>
							<TD>* Car Category</TD>
							<TD>
								<asp:DropDownList id="ddlcarcat" runat="server" CssClass="input"></asp:DropDownList></TD>
						</TR>
						<TR>
							<TD>Airport Transfer</TD>
							<TD>
								<asp:checkbox id="chkairport" onclick="enablecity();" runat="server" CssClass="input"></asp:checkbox></TD>
						</TR>
						<TR>
							<TD>City Name
							</TD>
							<TD>
								<asp:DropDownList id="ddlcityname" runat="server" CssClass="input"></asp:DropDownList></TD>
						</TR>
						<TR>
							<TD>Package Hours
							</TD>
							<TD>
								<asp:DropDownList id="ddlpackagehr" runat="server" CssClass="input"></asp:DropDownList></TD>
						</TR>
						<TR>
							<TD>Package KMs
							</TD>
							<TD>
								<asp:DropDownList id="ddlpkgkm" runat="server" CssClass="input"></asp:DropDownList></TD>
						</TR>
						<TR>
							<TD>Outstation</TD>
							<TD>
								<asp:checkbox id="chkoutstation" onclick="airportcheckbox();" runat="server" CssClass="input"></asp:checkbox></TD>
						</TR>
						<TR>
							<TD>* Rate
							</TD>
							<TD>
								<asp:textbox id="txtrate" runat="server" CssClass="input" MaxLength="50"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>Rate / Extra Hr</TD>
							<TD>
								<asp:textbox id="txtratehr" runat="server" CssClass="input" MaxLength="50"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>Rate / Extra KM</TD>
							<TD>
								<asp:textbox id="txtratekm"  onblur="CalcOSRate();" runat="server" CssClass="input" MaxLength="50"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>Threshold Extra Hr (Crosses to next package if reached)
							</TD>
							<TD>
								<asp:DropDownList id="ddlHr" runat="server">
									<asp:listitem Text="00" Value="0" />
									<asp:listitem Text="01" Value="1" />
									<asp:listitem Text="02" Value="2" />
								</asp:DropDownList>
								<asp:DropDownList id="ddlMin" runat="server">
									<asp:listitem Text="00" Value="00" />
									<asp:listitem Text="15" Value="25" />
									<asp:listitem Text="30" Value="50" />
									<asp:listitem Text="45" Value="75" />
								</asp:DropDownList>							
								</TD>
						</TR>
						<TR>
							<TD>Threshold Extra KM (Crosses to next package if reached)</TD>
							<TD>
								<asp:textbox id="txtExtraKM" runat="server" CssClass="input" MaxLength="50"></asp:textbox>							
							</TD>
						</TR>
						<TR>
							<TD>Outstation Allowance
							</TD>
							<TD>
								<asp:textbox id="txtoutallowance" runat="server" CssClass="input" MaxLength="50"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>Night Stay Allowance
							</TD>
							<TD>
								<asp:textbox id="txtnighstay" runat="server" CssClass="input" MaxLength="50"></asp:textbox></TD>
						</TR>
						<TR>
							<TD vAlign="top">Remarks(<SPAN class="shwText" id="shwMessage">0</SPAN>/2000 chars)</TD>
							<TD>
								<asp:textbox id="txtRemarks" onkeydown="showLength(this.form.txtRemarks,'document.all.shwMessage',2000)"
									onkeyup="showLength(this.form.txtRemarks,'document.all.shwMessage',2000)" runat="server"
									CssClass="input" MaxLength="2000" Rows="3" Columns="50" TextMode="MultiLine"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>Active</TD>
							<TD>
								<asp:checkbox id="chkactive" runat="server" CssClass="input" Checked="True"></asp:checkbox></TD>
						</TR>

						<TR>
							<TD align="center" colSpan="2">
								<asp:button id="btnsubmit" runat="server" CssClass="button" Text="Submit"></asp:button>&nbsp;&nbsp;
								<asp:button id="btnreset" runat="server" CssClass="button" Text="Reset" CausesValidation="False"></asp:button></TD>
						</TR>
				</asp:panel><asp:panel id="pnlconfirmation" Runat="server" Visible="False">
					<TR align="center">
						<TD colSpan="2">
							<asp:Label id="lblMessage" runat="server"></asp:Label></TD>
					</TR>
					<TR align="center">
						<TD colSpan="2">
							<asp:HyperLink id="hyplnkretry" runat="server"></asp:HyperLink></TD>
					</TR>
				</asp:panel></TBODY></TABLE>
		</form>
	</body>
</HTML>
