Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.text
Imports System.Web.UI.WebControls
Imports System.Web.UI
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class VendorChaufPkgsEditResult
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tblRecDetail As System.Web.UI.WebControls.Table

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not Page.IsPostBack Then
            getvalue("C.carvendorname")
        End If
    End Sub
    Sub getvalue(ByVal strorderby As String)

        'Dim dtrreader As SqlDataReader
        'Dim accessdata As clsutility
        'accessdata = New clsutility

        'Dim objAcessdataNw As clsutility
        'objAcessdataNw = New clsutility
        'Dim strquery As String
        'Dim dtrreaderNew As SqlDataReader
        'Dim strqueryvendor As StringBuilder

        'Dim objAcessdataMapping As clsutility 'Changes
        'objAcessdataMapping = New clsutility 'Changes
        'Dim strqueryMapping As String 'Changes
        'Dim dtrreaderMapping As SqlDataReader 'Changes
        'Dim UnitID As String 'Changes
        'Dim strquerycity As String

        ''Dim AccessType As String

        'strquery = New String("SELECT a.AccessType, a.UnitID, b.UnitCityID, c.Region FROM CORIntSysUsersMaster as a, CORIntUnitMaster as b, CORIntUnitCityMaster as c WHERE a.UnitID = b.UnitID and b.UnitCityID = c.UnitCityID and a.SysUserID =" & Session("loggedin_user"))
        'dtrreaderNew = objAcessdataNw.funcGetSQLDataReader(strquery.ToString)

        'Do While dtrreaderNew.Read

        '    'Changes
        '    If dtrreaderNew("AccessType") = "SU" Or dtrreaderNew("AccessType") = "RN" Then
        '        strqueryMapping = New String("select UM.UnitID, UM.referencedUnitID from corintunitmapping as UM, CORIntUnitMaster as b, CORIntUnitCityMaster as c WHERE UM.referencedUnitID = b.UnitID and b.UnitCityID = c.UnitCityID and UM.UnitID = " & dtrreaderNew("UnitID"))
        '    ElseIf dtrreaderNew("AccessType") = "CT" Then
        '        strqueryMapping = New String("select UM.UnitID, c.UnitCityID as referencedUnitID from corintunitmapping as UM, CORIntUnitMaster as b, CORIntUnitCityMaster as c WHERE UM.referencedUnitID = b.UnitID and b.UnitCityID = c.UnitCityID and UM.UnitID = " & dtrreaderNew("UnitID"))
        '    Else 'If dtrreaderNew("AccessType") = "RN" Then
        '        UnitID = dtrreaderNew("UnitID")
        '    End If

        '    If dtrreaderNew("AccessType") = "SU" Or dtrreaderNew("AccessType") = "CT" Or dtrreaderNew("AccessType") = "RN" Then
        '        dtrreaderMapping = objAcessdataMapping.funcGetSQLDataReader(strqueryMapping.ToString)

        '        Do While dtrreaderMapping.Read
        '            UnitID = dtrreaderMapping("referencedUnitID")
        '        Loop

        '        dtrreaderMapping.Close()

        '    End If


        '    objAcessdataMapping.Dispose()
        '    'Changes

        '    If UnitID = "" Then
        '        UnitID = dtrreaderNew("UnitID")
        '    End If


        '    strqueryvendor = New StringBuilder("select M.vendorpkgid,C.carvendorname,A.carcatname,B.cityname, ")
        '    strqueryvendor.Append(" M.apttransyn,M.outstationyn,convert(varchar(10),m.pkghrs)+'/'+convert(varchar(10),")
        '    strqueryvendor.Append(" M.pkgkms) as pkg,case M.active when '1' then 'Active' when '0' then 'Not Active' end")
        '    strqueryvendor.Append(" as active from CORIntVendorChaufPkgsMaster M ")
        '    strqueryvendor.Append(" Left outer join CORIntCityMaster B on B.cityid=M.cityid ")
        '    strqueryvendor.Append(" inner join CORIntCarVendorMaster C on M.carvendorid=C.carvendorid ")
        '    strqueryvendor.Append(" inner join CORIntCarCatMaster A on A.carcatid=M.carcatid ")
        '    strqueryvendor.Append(" inner join CORIntCityMaster CM on CM.cityid = C.CarVendorCityID ")
        '    strqueryvendor.Append(" inner join CORIntUnitCityMaster as UCM on CM.nearestUnitCityId = UCM.UnitCityId ")
        '    strqueryvendor.Append(" inner join CORIntUnitMaster as UM on  UM.UnitCityID = UCM.UnitCityID where isnull(C.ApproveYN,0) = 1  ")

        '    If dtrreaderNew("AccessType") = "SU" Or dtrreaderNew("AccessType") = "RN" Then
        '        strqueryvendor.Append("  and UM.UnitID = '" & UnitID & "' ")
        '    Else
        '        strqueryvendor.Append("  and UM.UnitCityID = '" & UnitID & "' ")
        '    End If

        '    If Request.QueryString("id") <> "-1" Then
        '        strqueryvendor.Append(" and M.carvendorid=" & Request.QueryString("id") & " ")
        '    End If
        '    strqueryvendor.Append(" order by " & strorderby & "")
        'Loop


        'dtrreaderNew.Close()
        'objAcessdataNw.Dispose()


        'dtrreader = accessdata.funcGetSQLDataReader(strqueryvendor.ToString)

        Dim strquery As StringBuilder
        Dim dtrreader As SqlDataReader
        Dim accessdata As clsutility
        accessdata = New clsutility
        strquery = New StringBuilder("select M.vendorpkgid,C.carvendorname,A.carcatname,B.cityname,M.apttransyn,M.outstationyn,convert(varchar(10),m.pkghrs)+'/'+convert(varchar(10),M.pkgkms) as pkg,case M.active when '1' then 'Active' when '0' then 'Not Active' end as active from CORIntVendorChaufPkgsMaster M Left outer join CORIntCityMaster B on B.cityid=M.cityid, CORIntCarVendorMaster C, CORIntCarCatMaster A where M.carvendorid=C.carvendorid and A.carcatid=M.carcatid")
        If Request.QueryString("id") <> "-1" Then
            strquery.Append(" and M.carvendorid=" & Request.QueryString("id") & " ")
        End If
        strquery.Append(" order by " & strorderby & "")
        dtrreader = accessdata.funcGetSQLDataReader(strquery.ToString)

        While dtrreader.Read
            Dim Temprow As New TableRow
            Temprow.HorizontalAlign = HorizontalAlign.Center
            Dim Tempcell As New TableCell
            Tempcell.Controls.Add(New LiteralControl(dtrreader("carvendorname") & ""))
            Temprow.Cells.Add(Tempcell)

            Dim Tempcel2 As New TableCell
            Tempcel2.Controls.Add(New LiteralControl(dtrreader("carcatname") & ""))
            Temprow.Cells.Add(Tempcel2)

            Dim Tempcel3 As New TableCell
            Tempcel3.Controls.Add(New LiteralControl(dtrreader("apttransyn") & ""))
            Temprow.Cells.Add(Tempcel3)

            Dim Tempcel4 As New TableCell
            Tempcel4.Controls.Add(New LiteralControl(dtrreader("cityname") & ""))
            Temprow.Cells.Add(Tempcel4)

            Dim Tempcel5 As New TableCell
            Tempcel5.Controls.Add(New LiteralControl(dtrreader("pkg") & ""))
            Temprow.Cells.Add(Tempcel5)

            Dim Tempcel6 As New TableCell
            Tempcel6.Controls.Add(New LiteralControl(dtrreader("outstationyn") & ""))
            Temprow.Cells.Add(Tempcel6)

            Dim Tempcel7 As New TableCell
            Tempcel7.Controls.Add(New LiteralControl(dtrreader("active") & ""))
            Temprow.Cells.Add(Tempcel7)

            Dim Tempcel8 As New TableCell
            Tempcel8.Controls.Add(New LiteralControl("<a href=VendorChaufPkgsEditForm.aspx?ID=" & dtrreader("vendorpkgid") & " >Edit</a>"))
            Temprow.Cells.Add(Tempcel8)
            'adding the Tables rows to the table
            tblRecDetail.Rows.Add(Temprow)

        End While
        dtrreader.Close()
        accessdata.Dispose()


    End Sub
    Sub SortGird(ByVal sender As Object, ByVal e As System.EventArgs)
        Select Case sender.id
            Case "Linkbutton1"
                getvalue("V.carvendorname")
            Case "Linkbutton2"
                getvalue("carcatname")
            Case "Linkbutton3"
                getvalue("apttransyn")
            Case "Linkbutton4"
                getvalue("cityname")
            Case "Linkbutton5"
                getvalue("pkg")
            Case "Linkbutton6"
                getvalue("outstationyn")
            Case "Linkbutton7"
                getvalue("active")
        End Select

    End Sub


End Class
