<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" Src="CityEditForm.aspx.vb" Inherits="CityEditForm" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>CarzonRent :: Internal software Control panel</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <link href="../HertzInt.css" type="text/css" rel="stylesheet">
    <script language="JavaScript" src="../utilityfunction.js" type="text/javascript"></script>

    <script src="../JScripts/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {

            $("#<%=txtlat.ClientID%>").keyup(function () {
                var strPass = $("#<%=txtlat.ClientID%>").val();
                var strLength = strPass.length;
                var lchar = strPass.charAt((strLength) - 1);
                var cCode = CalcKeyCode(lchar);
                if (cCode < 46 || cCode > 57 || cCode == 47) {
                    var myNumber = strPass.substring(0, (strLength) - 1);
                    $("#<%=txtlat.ClientID%>").val(myNumber);
                    alert("Enter only numeric value.")
                }
            });
            $("#<%=txtlon.ClientID%>").keyup(function () {
                var strPass = $("#<%=txtlon.ClientID%>").val();
                var strLength = strPass.length;
                var lchar = strPass.charAt((strLength) - 1);
                var cCode = CalcKeyCode(lchar);
                if (cCode < 46 || cCode > 57 || cCode == 47) {
                    var myNumber = strPass.substring(0, (strLength) - 1);
                    $("#<%=txtlon.ClientID%>").val(myNumber);
                    alert("Enter only numeric value.")
                }
            });
        })
        function CalcKeyCode(aChar) {
            var character = aChar.substring(0, 1);
            var code = aChar.charCodeAt(0);
            return code;
        }
    </script>
</head>
<body onload="OnLoadshowLength(document.forms[0].txtarearemarks.value,shwMessage)">
    <form id="Form1" method="post" runat="server">
        <uc1:Headerctrl ID="Headerctrl1" runat="server"></uc1:Headerctrl>
        <p>
            <asp:ValidationSummary ID="Validationsummary1" runat="server" ShowSummary="false"
                HeaderText="Please make sure all the fields marked with * are filled in." ShowMessageBox="true"></asp:ValidationSummary>
        </p>
        <table align="center">
            <asp:Panel ID="pnlmainform" runat="server">
                <tbody>
                    <tr>
                        <td align="center" colspan="2">
                            <b><u>Edit a City</u></b>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <asp:Label ID="lblErrorMsg" CssClass="subRedHead" Visible="false" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>* City Name
                        </td>
                        <td>
                            <asp:TextBox ID="txtcityname" runat="server" MaxLength="100" CssClass="input"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" ControlToValidate="txtcityname"
                                Display="None" ErrorMessage="" SetFocusOnError ="true"></asp:RequiredFieldValidator>
                        </td>
                    </tr>

                    <tr>
                        <td>* State Name</td>
                        <td>
                            <asp:DropDownList ID="ddlState" runat="server" CssClass="input" AutoPostBack="true">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="Requiredfieldvalidator5" runat="server" ErrorMessage=""
                                Display="None" ControlToValidate="ddlState" SetFocusOnError="true"></asp:RequiredFieldValidator></td>
                    </tr>

                    <%-- <tr>
                    <td>
                        * State Name
                    </td>
                    <td>
                        <asp:TextBox ID="txtStatename" runat="server" MaxLength="100" CssClass="input"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" ControlToValidate="txtStatename"
                            Display="None" ErrorMessage=""></asp:RequiredFieldValidator>
                    </td>
                </tr>--%>
                    <tr>
                        <td>* Nearest Unit City
                        </td>
                        <td>
                            <asp:DropDownList ID="ddCityName" runat="server" CssClass="input">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" ControlToValidate="ddCityName"
                                Display="None" ErrorMessage="" SetFocusOnError="true"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>* Actual City 
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlActualCity" runat="server" CssClass="input">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" ErrorMessage=""
                                Display="None" ControlToValidate="ddlActualCity" SetFocusOnError="true"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <%-- <tr>
                    <td>
                        Easy Cabs City
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlEasyCabsCity" runat="server" CssClass="input">
                        </asp:DropDownList>
                    </td>
                </tr>--%>
                    <tr>
                        <td>* Latitude
                        </td>
                        <td>
                            <asp:TextBox ID="txtlat" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" ErrorMessage=""
                                Display="None" ControlToValidate="txtlat"></asp:RequiredFieldValidator></td>

                    </tr>
                    <tr>
                        <td>* Longitude
                        </td>
                        <td>
                            <asp:TextBox ID="txtlon" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="Requiredfieldvalidator6" runat="server" ErrorMessage=""
                                Display="None" ControlToValidate="txtlon"></asp:RequiredFieldValidator></td>

                    </tr>
                    <tr>
                        <td valign="top">Remarks(<span class="shwText" id="shwMessage">0</span>/2000 chars)
                        </td>
                        <td>
                            <asp:TextBox ID="txtarearemarks" onkeydown="showLength(this.form.txtarearemarks,'document.all.shwMessage',2000)"
                                onkeyup="showLength(this.form.txtarearemarks,'document.all.shwMessage',2000)"
                                runat="server" MaxLength="2000" CssClass="input" TextMode="MultiLine" Columns="50"
                                Rows="3"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Active
                        </td>
                        <td>
                            <asp:CheckBox ID="chkActive" runat="server" CssClass="input" Checked="True"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Inventory Allocation
                        </td>
                        <td>
                            <asp:CheckBox ID="chkInventoryAllocation" runat="server" CssClass="input" Checked="false"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Convenience Fees
                        </td>
                        <td>
                            <asp:CheckBox ID="chkConvenienceFees" runat="server" CssClass="input" Checked="false"
                                AutoPostBack="true" OnCheckedChanged="chkConvenienceFees_CheckedChanged"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Cor Drive YN
                        </td>
                        <td>
                            <asp:CheckBox ID="chkCorDriveYN" runat="server" CssClass="input" Checked="false"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Additional KM
                        </td>
                        <td>
                            <asp:TextBox ID="txtAdditionalKm" runat="server" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <asp:Button ID="btnSubmit" runat="server" CssClass="input" Text="Submit"></asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="Reset" cssclass="button" name="Reset" value="Reset" />
                        </td>
                    </tr>
            </asp:Panel>
            <asp:Panel ID="pnlconfirmation" Visible="False" runat="server">
                <tr align="center">
                    <td colspan="2">
                        <input type="hidden" name="txtarearemarks">
                        <span class="shwText" id="shwMessage"></span>
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr align="center">
                    <td colspan="2">
                        <asp:HyperLink ID="hyplnkretry" runat="server"></asp:HyperLink>
                    </td>
                </tr>
            </asp:Panel>
            </TBODY>
        </table>
    </form>
</body>
</html>
