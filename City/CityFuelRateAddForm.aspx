<%@ Page Language="vb" AutoEventWireup="false" Src="CityFuelRateAddForm.aspx.vb" Inherits="CityFuelRateAddForm"%>
<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CarzonRent :: Internal software</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../JScripts/Datefunc.js"></script>
		<script language="JavaScript" src="../utilityfunction.js"></script>
		<script language="JavaScript">
			function checkvalue()
			{
				if (isNaN(document.forms[0].txtCityFuelRate.value))
				{
					alert("Fuel rate should be numeric and upto two decimal digits only")
					return false;
				}
				else
				{

					if (parseFloat(document.forms[0].txtCityFuelRate.value.indexOf("."))>0)
					{
						var FlotVal
						FlotVal=document.forms[0].txtCityFuelRate.value.substr(parseFloat(document.forms[0].txtCityFuelRate.value.indexOf(".")),document.forms[0].txtCityFuelRate.value.length)
						if(parseFloat((FlotVal.length)-1)>2)
							{
								alert("Fuel rate should be numeric and upto two decimal digits only")
								return false;
							}
					}
				}
			var strvalues
			strvalues=('ddlCityID,ddlFuelTypeID,txtCityFuelRate,txtDateApplied')
			return checkmandatory(strvalues);
         }
		</script>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<uc1:headerctrl id="Headerctrl1" runat="server"></uc1:headerctrl>
			<p><asp:validationsummary id="Validationsummary1" runat="server" ShowMessageBox="true" HeaderText="Please make sure all the fields marked with * are filled in."
					ShowSummary="false"></asp:validationsummary></p>
			<table align="center">
				<asp:panel id="pnlmainform" Runat="server">
					<TBODY>
						<TR>
							<TD align="center" colSpan="2"><B><U>Assign new fuel rates for a city</U></B>
								<BR>
								<BR>
							</TD>
						</TR>
						<tr><td colspan="2" align="center"><asp:Label id="lblErrorMsg" cssclass="subRedHead" visible="false" runat="server"></asp:Label></td></tr>
						<TR>
							<TD>* City
							</TD>
							<TD>
								<asp:DropDownList id="ddlCityID" runat="server" CssClass="input"></asp:DropDownList>
						</TR>
						<TR>
							<TD>* Fuel Type
							</TD>
							<TD>
								<asp:DropDownList id="ddlFuelTypeID" runat="server" CssClass="input"></asp:DropDownList>
						</TR>
						<TR>
							<TD>* Fuel Rate
							</TD>
							<TD>
								INR<asp:TextBox id="txtCityFuelRate" runat="server" CssClass="input"></asp:TextBox> / Fuel unit
						</TR>
						<TR>
							<TD>* Date applicable from
							</TD>
							<TD>
								<asp:textbox id="txtDateApplied" runat="server" MaxLength="12" CssClass="input" 
								size="12"></asp:textbox><A onmouseover="window.status='Date Picker';return true;" onmouseout="window.status='';return true;"
								href="javascript:show_calendar('Form1.txtDateApplied');"><IMG height="21" src="../images/show-calendar.gif" width="24" border="0"></A></FONT>
						</TR>
						<TR>
							<TD align="center" colSpan="2">
								<asp:button id="btnSubmit" runat="server" CssClass="input" Text="Submit"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="Reset" CssClass="button" name="Reset" value="Reset" /></TD>
						</TR>
				</asp:panel>
				<asp:Panel ID="pnlconfirmation" Visible="False" Runat="server">
					<TR align="center">
						<TD colSpan="2"><BR>
							<BR>
							<BR>
							<BR>
							<asp:Label id="lblMessage" runat="server"></asp:Label></TD>
					</TR>
					<TR align="center">
						<TD colSpan="2">
							<asp:HyperLink id="hyplnkretry" runat="server"></asp:HyperLink></TD>
					</TR>
				</asp:Panel></TBODY></table>
		</form>
	</body>
</HTML>
