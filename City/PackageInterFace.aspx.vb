Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Imports System.Data.Sql
Imports System.Data.OleDb
Imports System.IO
Imports System.Text
Imports System.Web.UI

Partial Class PackageInterFace
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Text = ""
        lblPackageID.Text = ""
        'Dim formattedDate As String = DateTime.Now.ToString("dd-MMM-yyyy")
        'Response.Write(formattedDate)

        If Not Page.IsPostBack Then
            populateddl()
        End If

    End Sub

    Sub populateddl()
        Dim oConnection As SqlConnection
        oConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("corConnectString"))
        'Dim objQuery As clsQuery
        'objQuery = New clsQuery
        Dim objDataSet As DataSet
        objDataSet = New DataSet
        Dim objCommand As SqlCommand
        Dim objDataAdapter As SqlDataAdapter
        oConnection.Open()

        objCommand = New SqlCommand
        objCommand.CommandType = CommandType.StoredProcedure
        objCommand.CommandText = "Prc_CityDetails"
        objCommand.Connection = oConnection

        objDataAdapter = New SqlDataAdapter(objCommand)
        objDataAdapter.Fill(objDataSet)


        objDataSet.AcceptChanges()

        dgSummary.DataSource = objDataSet 'objQuery.ExecuteProcedure("ProcCarWiseRevenueRPT")
        dgSummary.DataBind()

        oConnection.Close()
        objDataSet.Dispose()
        objCommand.Dispose()
        objDataAdapter.Dispose()

    End Sub

    Protected Sub btnUploadPkg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUploadPkg.Click
        Dim str As String

        str = Session("loggedin_user")

        Dim strFile As String
        Dim filename As String
        Dim Newfilename As String
        Dim Dt As DataTable = New DataTable()

        Dim di As DirectoryInfo = New DirectoryInfo("C:\MyDir")
        If di.Exists Then
            'MsgBox("That path exists already.")
        Else
            di.Create()
        End If

        If UploadPackage.PostedFile.FileName <> "" And UploadPackage.PostedFile.ContentLength > 0 Then
            strFile = Path.GetFileName(UploadPackage.FileName)
            'End If
            filename = Path.GetFileName(UploadPackage.FileName)
            If File.Exists("C:\MyDir\" + filename) Then
                File.Delete("C:\MyDir\" + filename)
            End If
            If strFile <> "" And txtEffectiveDate.Text <> "" Then

                If filename = "PetrolDiesel.xlsx" Then
                    Newfilename = System.DateTime.Now.Year.ToString() + "_" + System.DateTime.Now.Month.ToString() + "_" + System.DateTime.Now.Day.ToString() + "_" + System.DateTime.Now.Hour.ToString() + "_" + System.DateTime.Now.Minute.ToString() + "_" + System.DateTime.Now.Second.ToString() + ".xlsx"

                    UploadPackage.SaveAs("C:\MyDir\" + Newfilename)

                    Dim connStringExcel As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\MyDir\" + Newfilename + ";Extended Properties=""Excel 12.0;HDR=YES;"""
                    Dim excelConn As New OleDbConnection(connStringExcel)
                    Dim excelCmd As New OleDbCommand("Select * From [PetrolDiesel$]", excelConn)

                    Try
                        excelConn.Open()
                        Dim excelReader As OleDbDataReader = excelCmd.ExecuteReader()
                        Dim Pkglist As New ArrayList
                        Dim Citylist As New ArrayList
                        Dim CarCatlist As New ArrayList
                        Dim intPkgidParam As SqlParameter

                        Dim MyConnection As SqlConnection
                        MyConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("corConnectString"))
                        Try
                            MyConnection.Open()
                            While (excelReader.Read())
                                If IsNothing(excelReader("CityName")) Or IsDBNull(excelReader("FuelType")) Then
                                    lblMessage.Text = "Please check the Excel Sheet Data."
                                Else
                                    Dim cmd As SqlCommand
                                    Dim intFuelRate As Int32

                                    cmd = New SqlCommand("procAddCityFuelRate_Upload", MyConnection)
                                    cmd.CommandType = CommandType.StoredProcedure
                                    cmd.CommandTimeout = 6000
                                    cmd.Parameters.AddWithValue("@CityName", excelReader("CityName").ToString())
                                    cmd.Parameters.AddWithValue("@FuelTypeName", excelReader("FuelType").ToString())
                                    cmd.Parameters.AddWithValue("@DateApplied", Convert.ToDateTime(txtEffectiveDate.Text.ToString()))
                                    cmd.Parameters.AddWithValue("@CityFuelRate", Convert.ToDouble(excelReader("FuelRate")))
                                    cmd.Parameters.AddWithValue("@CreatedBy", Convert.ToInt32(Session("loggedin_user")))

                                    intPkgidParam = cmd.Parameters.Add("@FuelID", SqlDbType.Int)
                                    intPkgidParam.Direction = ParameterDirection.Output

                                    cmd.ExecuteNonQuery()

                                    intFuelRate = cmd.Parameters("@FuelID").Value

                                    If (Convert.ToInt32(intFuelRate) < 0) Then
                                        If (lblPackageID.Text = "") Then
                                            lblPackageID.Text = "Package Id's ="
                                        End If
                                        If (Convert.ToInt32(intFuelRate) = -1) Then
                                            lblPackageID.Text += "Invalid City Name,"
                                        Else
                                            lblPackageID.Text += "Invalid City Name,"
                                        End If
                                    ElseIf (Convert.ToInt32(intFuelRate) <> 0) Then
                                        If (lblPackageID.Text = "") Then
                                            lblPackageID.Text = "Package Id's ="
                                        End If

                                        lblPackageID.Text += Convert.ToString(intFuelRate) + ","
                                    End If
                                End If
                            End While
                            excelConn.Close()
                            excelConn.Dispose()
                            If File.Exists("C:\MyDir\" + Newfilename) Then
                                File.Delete("C:\MyDir\" + Newfilename)
                            End If
                            lblMessage.Text = "Packages Insert successfully!"
                        Catch exs As Exception
                            lblMessage.Text = exs.Message
                        Finally
                            MyConnection.Close()
                        End Try
                    Catch exo As Exception
                        lblMessage.Text = exo.Message
                        If File.Exists("C:\MyDir\" + Newfilename) Then
                            File.Delete("C:\MyDir\" + Newfilename)
                        End If
                    Finally

                    End Try
                Else
                    lblMessage.Text = "Please select package file!"
                End If
            End If

        Else
            lblMessage.Text = "Please Select File!"
        End If
    End Sub
End Class
