<%@ Page Language="vb" AutoEventWireup="false" src="AddCity.aspx.vb" Inherits="AddCity"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>AddCity</title>
        <script type="text/javascript" >
            $(function ()
            {
                $("#<%=btnsubmit.ClientID%>").click(function()
                { 
                    if ($("#<%=ddlProvider.ClientID%>").val() == 0)
                    {
                        alert("Select provider name");
                        $("#<%=ddlProvider.ClientID%>").focus();
                        return false;
                    }
                    else if ($("#<%=ddlProvider.ClientID%>").val())
                });
            });

        </script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<TABLE height="438" cellSpacing="0" cellPadding="0" width="202" border="0" ms_2d_layout="TRUE">
			<TR vAlign="top">
				<TD width="202" height="438">
					<form id="Form1" method="post" runat="server">
						<TABLE height="185" cellSpacing="0" cellPadding="0" width="426" border="0" ms_2d_layout="TRUE">
							<TR vAlign="top">
								<TD width="10" height="15"></TD>
								<TD width="214"></TD>
								<TD width="56"></TD>
								<TD width="146"></TD>
							</TR>
							<TR vAlign="top">
								<TD height="65"></TD>
								<TD colSpan="3"><br>
								</TD>
							</TR>
                            <TR vAlign="top">
								<TD colSpan="2" height="80">Provider Name</TD>
								<TD colSpan="2">
									<asp:DropDownList ID="ddlProvider" runat="server"></asp:DropDownList> </TD>
							</TR>
							<TR vAlign="top">
								<TD colSpan="2" height="80"></TD>
								<TD colSpan="2">
									<asp:TextBox id="txtDesc" runat="server" MaxLength="200" Height="60px" Width="200px"></asp:TextBox></TD>
							</TR>
							<TR vAlign="top">
								<TD colSpan="3" height="25"></TD>
								<TD><asp:button id="btnsubmit" runat="server" CssClass="button" Text="Submit"></asp:button></TD>
							</TR>
						</TABLE>
					</form>
				</TD>
			</TR>
		</TABLE>
	</body>
</HTML>
