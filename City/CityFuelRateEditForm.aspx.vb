Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class CityFuelRateEditForm
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    Protected WithEvents lblCityID As System.Web.UI.WebControls.Label
    Protected WithEvents lblFuelTypeID As System.Web.UI.WebControls.Label
    Protected WithEvents lblDateApplied As System.Web.UI.WebControls.Label
    Protected WithEvents txtCityFuelRate As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblCityIDHid As System.Web.UI.WebControls.Label
    Protected WithEvents lblFuelTypeIDHid As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        btnSubmit.Attributes("onClick") = "return checkvalue();"
        If Not Page.IsPostBack Then
            populateddl()
            Dim dtrreader As SqlDataReader
            Dim accessdata As clsutility
            accessdata = New clsutility
            dtrreader = accessdata.funcGetSQLDataReader("select a.*, b.CityName, c.FuelTypeName from CORIntCityFuelRateMaster as a, CORIntCityMaster as b, CORIntFuelTypeMaster as c  where a.CityID = b.CityID and a.FuelTypeID = c.FuelTypeID and a.CityFuelRateID=" & Request.QueryString("id") & " ")
            dtrreader.Read()
            lblCityID.Text = dtrreader("CityName")
            lblFuelTypeID.Text = dtrreader("FuelTypeName")
            lblDateApplied.Text = dtrreader("DateApplied")
            txtCityFuelRate.Text = dtrreader("CityFuelRate")
            lblCityIDHid.Text = dtrreader("CityID")
            lblFuelTypeIDHid.Text = dtrreader("FuelTypeID")

            dtrreader.Close()
            accessdata.Dispose()

        End If
    End Sub

    Sub populateddl()
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        Dim intuniqcheck As SqlParameter
        Dim intuniqvalue as int32

        cmd = New SqlCommand("procEditCityFuelRate", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("@rowid", Request.QueryString("id"))
        cmd.Parameters.Add("@CityID", lblCityIDHid.Text)
        cmd.Parameters.Add("@FuelTypeID", lblFuelTypeIDHid.Text)
        cmd.Parameters.Add("@DateApplied", lblDateApplied.Text)
        cmd.Parameters.Add("@CityFuelRate", txtCityFuelRate.Text)
        cmd.Parameters.Add("@ModifiedBy", Session("loggedin_user"))
       intuniqcheck = cmd.Parameters.Add("@uniqcheckval", SqlDbType.Int)
       intuniqcheck.Direction = ParameterDirection.Output

        '  Try
        MyConnection.Open()
        cmd.ExecuteNonQuery()
        intuniqvalue=cmd.Parameters("@uniqcheckval").Value
        MyConnection.Close()
          if not intuniqvalue = 0 then
                lblErrorMsg.visible=true
                lblErrorMsg.text="City and fuel type combination already exists for this date."
                exit sub
            else
		        lblErrorMsg.visible=false
                pnlmainform.Visible = False
                pnlconfirmation.Visible = True

                lblMessage.Text = "You have updated the fuel rate for the city successfully"
                hyplnkretry.Text = "Edit another fuel rate"
                hyplnkretry.NavigateUrl = "CityFuelRateEditSearch.aspx"
            end if
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function

End Class
