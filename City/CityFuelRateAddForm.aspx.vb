Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class CityFuelRateAddForm
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents Validationsummary1 As System.Web.UI.WebControls.ValidationSummary
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    Protected WithEvents ddlCityID As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlFuelTypeID As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtCityFuelRate As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtDateApplied As System.Web.UI.WebControls.TextBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btnSubmit.Attributes("onClick") = "return checkvalue();"
        txtDateApplied.Attributes.Add("Readonly", "Readonly")
        If Not Page.IsPostBack Then
            populateddl()
        End If

    End Sub

    Sub populateddl()

        Dim objAcessdata As clsutility
        objAcessdata = New clsutility

        ddlCityID.DataSource = objAcessdata.funcGetSQLDataReader("select CityID, CityName from CORIntCityMaster where Active = 1  order by CityName")
        ddlCityID.DataValueField = "CityID"
        ddlCityID.DataTextField = "CityName"
        ddlCityID.DataBind()
        ddlCityID.Items.Insert(0, New ListItem("", ""))

        ddlFuelTypeID.DataSource = objAcessdata.funcGetSQLDataReader("select FuelTypeID, FuelTypeName from CORIntFuelTypeMaster where Active = 1  order by FuelTypeName")
        ddlFuelTypeID.DataValueField = "FuelTypeID"
        ddlFuelTypeID.DataTextField = "FuelTypeName"
        ddlFuelTypeID.DataBind()
        ddlFuelTypeID.Items.Insert(0, New ListItem("", ""))

        objAcessdata.Dispose()
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand


        Dim intuniqcheck As SqlParameter
        Dim intuniqvalue as int32

        cmd = New SqlCommand("procAddCityFuelRate", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("@CityID", ddlCityID.SelectedItem.Value)
        cmd.Parameters.Add("@FuelTypeID", ddlFuelTypeID.SelectedItem.Value)
        cmd.Parameters.Add("@DateApplied", txtDateApplied.Text)
        cmd.Parameters.Add("@CityFuelRate", txtCityFuelRate.Text)
        cmd.Parameters.Add("@CreatedBy", Session("loggedin_user"))
       intuniqcheck = cmd.Parameters.Add("@uniqcheckval", SqlDbType.Int)
       intuniqcheck.Direction = ParameterDirection.Output

        '  Try
        MyConnection.Open()
        cmd.ExecuteNonQuery()
        intuniqvalue=cmd.Parameters("@uniqcheckval").Value
        MyConnection.Close()
          if not intuniqvalue = 0 then
                lblErrorMsg.visible=true
                lblErrorMsg.text="City and fuel type combination already exists for this date."
                exit sub
            else
		        lblErrorMsg.visible=false
                pnlmainform.Visible = False
                pnlconfirmation.Visible = True

                lblMessage.Text = "You have added the fuel rate for the city successfully"
                hyplnkretry.Text = "Assign new fuel rates for cities"
                hyplnkretry.NavigateUrl = "CityFuelRateAddForm.aspx"
            end if    
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function
End Class
