Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class CityAddForm
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    Protected WithEvents txtcityname As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkActive As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkConvenienceFees As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkInventoryAllocation As System.Web.UI.WebControls.CheckBox
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    Protected WithEvents txtarearemarks As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtStatename As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddCityName As System.Web.UI.WebControls.DropDownList
    Protected WithEvents Validationsummary1 As System.Web.UI.WebControls.ValidationSummary
    Protected WithEvents Requiredfieldvalidator1 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator2 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator3 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents txtAdditionalKm As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkCorDriveYN As System.Web.UI.WebControls.CheckBox
    Protected WithEvents txtlat As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtlon As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlActualCity As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlEasyCabsCity As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlState As System.Web.UI.WebControls.DropDownList
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not Page.IsPostBack Then

            populateddl()
            'txtStatename.ReadOnly = True
        End If

    End Sub

    Sub populateddl()
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility

        ddCityName.DataSource = objAcessdata.funcGetSQLDataReader("select unitcityid,cityname from corintunitcitymaster where active='1' order by cityname")
        ddCityName.DataValueField = "unitcityid"
        ddCityName.DataTextField = "cityname"
        ddCityName.DataBind()
        ddCityName.Items.Insert(0, New ListItem("", ""))

        ddlState.DataSource = objAcessdata.funcGetSQLDataReader("SELECT [StateId] ,[StateName] FROM CorIntStateMaster where active=1 order by statename asc")
        ddlState.DataValueField = "StateId"
        ddlState.DataTextField = "StateName"
        ddlState.DataBind()
        ddlState.Items.Insert(0, New ListItem("", ""))


        'ddlActualCity.DataSource = objAcessdata.funcGetSQLDataReader("select cityname,cityid from CORIntCityMaster  where active=1 and ProviderId=" & Session("provider_Id") & " order by cityname")
        ddlActualCity.DataSource = objAcessdata.funcGetSQLDataReader("select cityname,cityid from CORIntCityMaster  where active=1  order by cityname")
        ddlActualCity.DataValueField = "cityid"
        ddlActualCity.DataTextField = "cityname"
        ddlActualCity.DataBind()
        ddlActualCity.Items.Insert(0, New ListItem("", ""))

        'ddlEasyCabsCity.DataSource = objAcessdata.funcGetSQLDataReader("select cityname,cityid from CORIntCityMaster  where active=1 order by cityname")
        'ddlEasyCabsCity.DataValueField = "cityid"
        'ddlEasyCabsCity.DataTextField = "cityname"
        'ddlEasyCabsCity.DataBind()
        'ddlEasyCabsCity.Items.Insert(0, New ListItem("", ""))

        objAcessdata.Dispose()
    End Sub

    Private Sub BindData()
        'txtStatename.Text = ddlState.SelectedItem.Text
    End Sub


    Protected Sub ddlState_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlState.SelectedIndexChanged
        BindData()
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("corConnectString"))
        Dim cmd As SqlCommand

        Dim intuniqcheck As SqlParameter
        Dim intuniqvalue As Int32

        cmd = New SqlCommand("procAdddcity", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure


        If txtcityname.Text = "" Then
            lblErrorMsg.Visible = True
            lblErrorMsg.Text = "Please enter city name !"
            Return
        End If

        If ddlState.SelectedItem.Text = "" Or ddlState.SelectedIndex = 0 Then
            lblErrorMsg.Visible = True
            lblErrorMsg.Text = "Please enter state name !"
            Return
        End If



        cmd.Parameters.Add("@cityname", txtcityname.Text)
        cmd.Parameters.Add("@statename", ddlState.SelectedItem.Text)
        cmd.Parameters.Add("@StateId", ddlState.SelectedItem.Value)
        cmd.Parameters.Add("@nearsetunitcityid", ddCityName.SelectedItem.Value)
        cmd.Parameters.Add("@remarks ", txtarearemarks.Text)
        cmd.Parameters.Add("@active", get_YNvalue(chkActive))
        cmd.Parameters.Add("@InventoryAllocationYN", get_YNvalue(chkInventoryAllocation))
        cmd.Parameters.Add("@ConvenienceFees", get_YNvalue(chkConvenienceFees))
        cmd.Parameters.Add("@CorDriveyn", get_YNvalue(chkCorDriveYN))
        If chkConvenienceFees.Checked Then
            cmd.Parameters.Add("@AdditionKm", Convert.ToInt32(txtAdditionalKm.Text))
        Else
            cmd.Parameters.Add("@AdditionKm", 0)
        End If
        cmd.Parameters.Add("@Lat", Convert.ToDecimal(txtlat.Text))
        cmd.Parameters.Add("@Lon", Convert.ToDecimal(txtlon.Text))

        cmd.Parameters.Add("@ActualCityId", ddlActualCity.SelectedItem.Value)
        cmd.Parameters.Add("@EasyCabCityId", 0) 'ddlEasyCabsCity.SelectedItem.Value)
        cmd.Parameters.Add("@ProviderId ", Session("provider_Id"))


        cmd.Parameters.Add("@createdby ", Session("loggedin_user"))
        intuniqcheck = cmd.Parameters.Add("@uniqcheckval", SqlDbType.Int)
        intuniqcheck.Direction = ParameterDirection.Output



        '  Try
        MyConnection.Open()
        cmd.ExecuteNonQuery()
        intuniqvalue = cmd.Parameters("@uniqcheckval").Value
        MyConnection.Close()
        If Not intuniqvalue = 0 Then
            lblErrorMsg.Visible = True
            lblErrorMsg.Text = "City already exist."
            Exit Sub
        Else
            lblErrorMsg.Visible = False

            pnlmainform.Visible = False
            pnlconfirmation.Visible = True

            lblMessage.Text = "You have added the City successfully"
            hyplnkretry.Text = "Add another City"
            hyplnkretry.NavigateUrl = "CityAddForm.aspx"
        End If
        '   Catch
        '   End Try
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function

    Protected Sub chkConvenienceFees_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If chkConvenienceFees.Checked = True Then
            txtAdditionalKm.Enabled = True
        Else
            txtAdditionalKm.Enabled = False
            txtAdditionalKm.Text = 0
        End If

    End Sub
End Class
