<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>

<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PackageInterFace.aspx.vb"
    Inherits="PackageInterFace" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>CarzonRent :: Internal software Control panel</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../HertzInt.css" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css" />
    <script src="//code.jquery.com/jquery-1.10.2.js" type="text/jscript"></script>
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js" type="text/jscript"></script>
    <link rel="stylesheet" href="/resources/demos/style.css" />
    <script type="text/javascript" language="javascript">

        $(document).ready(function () {
            $("#<%=txtEffectiveDate.ClientID %>").datepicker();
        });

        function validate_EffectiveDate() {
            if (document.getElementById("<%=txtEffectiveDate.ClientID%>").value == "") {
                alert("Select Package Effective Date From.");
                document.getElementById("<%=txtEffectiveDate.ClientID %>").focus();
                return false;

            }
        }
    </script>
</head>
<body>
    <form id="Form1" method="post" runat="server">
    <uc1:Headerctrl ID="Headerctrl1" runat="server"></uc1:Headerctrl>
    <table align="center">
        <tbody>
            <tr>
                <td colspan="3">
                    <asp:Label ID="lblMessage" runat="server" ForeColor="red"></asp:Label><br />
                    <asp:Label ID="lblPackageID" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Date Applied From
                </td>
                <td>
                    <asp:TextBox ID="txtEffectiveDate" runat="server" CssClass="input"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:FileUpload ID="UploadPackage" runat="server" />
                    <asp:Button ID="btnUploadPkg" runat="server" Text="Upload Fuel Rate" OnClientClick="return validate_EffectiveDate();" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center" style="width: 100%">
                    Below is sample Excel File Format and file name should be (PetrolDiesel.xlsx)
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center" style="width: 100%">
                    <table border="1">
                        <tr style="background-color: Yellow;">
                            <td>
                                CityName
                            </td>
                            <td>
                                FuelType
                            </td>
                            <td>
                                FuelRate
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Delhi
                            </td>
                            <td>
                                Petrol
                            </td>
                            <td>
                                54
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Delhi
                            </td>
                            <td>
                                Diesel
                            </td>
                            <td>
                                55
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center" style="width: 100%">
                    <table border="1">
                        <asp:DataGrid ID="dgSummary" runat="server" Width="100%" AutoGenerateColumns="False"
                            CellPadding="0" BorderStyle="None">
                            <SelectedItemStyle Wrap="False"></SelectedItemStyle>
                            <EditItemStyle Wrap="False"></EditItemStyle>
                            <HeaderStyle Wrap="False" HorizontalAlign="Center" Height="20px" CssClass="subRedHead"
                                BackColor="#FFE5BA"></HeaderStyle>
                            <Columns>
                                <asp:BoundColumn DataField="cityid" HeaderText="City Id">
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="cityname" HeaderText="City Name">
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                </asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    </form>
</body>
</html>
