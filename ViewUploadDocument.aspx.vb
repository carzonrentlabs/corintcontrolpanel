Imports System
Imports System.IO
Imports System.Collections.Generic
Imports System.Web.UI.WebControls
Imports System.IO.FileStream
Imports System.Data
Imports System.Data.SqlClient


Partial Class ViewUploadDocument
    Inherits System.Web.UI.Page


    Private Sub BindGrid(ByVal _file_path As String, ByVal _fileName As String)
        Try
            If Directory.Exists(_file_path) Then

                Dim filePaths() As String = Directory.GetFiles(_file_path, _fileName & "_" & "*.*", SearchOption.AllDirectories) '_file_path, _fileName, SearchOption.AllDirectories)

                'Dim comparer As IComparer = New DateComparer()
                'Array.Sort(files, comparer)
                'Dim Files() As String
                'filePaths = System.IO.Directory.GetFiles("C:\")
                Array.Sort(filePaths)

                Dim files As New List(Of ListItem)()

                For Each filePath As String In filePaths
                    files.Add(New ListItem(Path.GetFileName(filePath), filePath))
                Next
                gvUploadedDocument.DataSource = files
                gvUploadedDocument.DataBind()
            Else
                Response.Write("No files updloaded")
                Response.End()

            End If

        Catch ex As Exception
            Console.WriteLine(ex.Message.ToString)
        End Try

    End Sub


    Private Sub BindGrid(ByVal _file_path As String, ByVal _fileName As String, ByVal _ClientName As String)
        Try
            If Directory.Exists(_file_path) Then

                Dim filePaths() As String = Directory.GetFiles(_file_path, _fileName & "_" & "*.*", SearchOption.AllDirectories) '_file_path, _fileName, SearchOption.AllDirectories)
                Dim files As New List(Of ListItem)()
                Array.Sort(filePaths)
                If filePaths.Length = 0 Then
                    filePaths = Directory.GetFiles(_file_path, _ClientName & "*.*", SearchOption.AllDirectories) '_file_path, _fileName, SearchOption.AllDirectories)
                End If

                For Each filePath As String In filePaths
                    files.Add(New ListItem(Path.GetFileName(filePath), filePath))
                Next
                gvUploadedDocument.DataSource = files
                gvUploadedDocument.DataBind()
            Else
                Response.Write("No files updloaded")
                Response.End()

            End If

        Catch ex As Exception
            Console.WriteLine(ex.Message.ToString)
        End Try

    End Sub

    Protected Sub DownloadFile(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim filePath As String = DirectCast(sender, LinkButton).CommandArgument
            Response.ContentType = ContentType
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(filePath))
            Response.WriteFile(filePath)
            Response.End()
        Catch ex As Exception
            Console.WriteLine(ex.Message.ToString)
        End Try

    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim _file_path As String = ""

        If Not Request.QueryString("ClientId") Is Nothing Then
            If Not Request.QueryString("fType") Is Nothing Then
                If Request.QueryString("fType") = "A" Then
                    _file_path = "F:\Upload\ClientContract\"
                    '_file_path = "D:\MailUpload\ClientContract\"
                    'BindGrid(_file_path, Request.QueryString("ClientId").ToString())
                    'Response.Write(_file_path)
                    'Response.End()
                    Dim MyConnection As SqlConnection
                    Dim ClientName As String
                    MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
                    Dim cmd As SqlCommand
                    Dim SqlDataAdapter As SqlDataAdapter = New SqlDataAdapter()
                    Dim intFlagCheck As SqlParameter
                    cmd = New SqlCommand("prc_getClientName", MyConnection)
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add("@ClientID", Request.QueryString("ClientId").ToString())
                    MyConnection.Open()
                    Dim ds As DataSet = New DataSet()
                    SqlDataAdapter = New SqlDataAdapter(cmd)
                    SqlDataAdapter.Fill(ds)
                    If ds.Tables(0).Rows.Count > 0 Then
                        ClientName = ds.Tables(0).Rows(0)("ClientCoName").ToString()
                    End If
                    cmd.ExecuteNonQuery()
                    BindGrid(_file_path, Request.QueryString("ClientId").ToString(), ds.Tables(0).Rows(0)("ClientCoName").ToString())
                    'BindGrid(_file_path, Request.QueryString("ClientId").ToString(), ClientName)
                End If
                If Request.QueryString("fType") = "C" Then
                    _file_path = "F:\Upload\ClientBilling_CAM\"
                    BindGrid(_file_path, Request.QueryString("ClientId").ToString())
                Else
                    If Request.QueryString("fType") = "S" Then
                        _file_path = "F:\Upload\ClientBilling_SOP" & "\"
                        BindGrid(_file_path, Request.QueryString("ClientId").ToString())
                    End If
                End If
            End If
        End If

    End Sub



End Class
