<%@ Page Language="vb" AutoEventWireup="false" Inherits="AttendanceReport" src="AttendanceReport.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>AttendanceReport</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../JScripts/Datefunc.js"></script>
		<script language="JavaScript" src="../utilityfunction.js"></script>
		<script language="javascript">
		function valid()
		{
			if (document.getElementById('txtFromDate').value == "")
			{
				alert("Select Date");
				return false;
			}
			return true;
		}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<table width="780" align="center">
				<TBODY>
					<tr>
						<td><IMG src="../images/logo_carzonret.gif"></td>
						<td align="right"><IMG src="../images/logo_hertz.gif" width="100" align="right"></td>
					</tr>
					<TR>
						<TD class="subRedHead" align="center" colSpan="2">Daily Attendance Report</TD>
					</TR>
					<TR>
						<TD colSpan="2">
							<HR>
						</TD>
					</TR>
					<TR>
						<TD colSpan="2">
							<TABLE width="100%" border="0">
								<TR>
									<TD>&nbsp;</TD>
									<TD>Location</TD>
									<TD><asp:dropdownlist id="cboUnit" runat="server" CssClass="input"></asp:dropdownlist></TD>
									<TD>Date *</TD>
									<TD vAlign="top">
										<asp:textbox id="txtFromDate" runat="server" Width="80" ReadOnly></asp:textbox><A onmouseover="window.status='Date Picker';return true;" onmouseout="window.status='';return true;"
											href="javascript:show_calendar('Form1.txtFromDate');"><IMG height="21" src="../images/show-calendar.gif" width="24" border="0"></A></TD>
									<TD align="right"><asp:Button id="btnSearch" runat="server" Text="Search"></asp:Button>&nbsp;
										<asp:Button id="btnReturn" runat="server" Text="Back"></asp:Button>&nbsp;&nbsp;
									</TD>
								</TR>
							</TABLE>
						</TD>
					</TR>
					<TR>
						<TD align="center" colSpan="2">
							<DIV style="OVERFLOW-Y: scroll; OVERFLOW-X: scroll; WIDTH: 780px; HEIGHT: 450px; visible: "
								noWrap>
								<asp:datagrid id="dgSummary" runat="server" Width="100%" CellSpacing="2" CellPadding="0" OnItemDataBound="dgSummary_ItemDataBound" AutoGenerateColumns="False">
									<SelectedItemStyle Wrap="False"></SelectedItemStyle>
									<EditItemStyle Wrap="False"></EditItemStyle>
									<HeaderStyle Font-Bold="True" Wrap="False" HorizontalAlign="Center" BackColor="#FFE5BA"></HeaderStyle>
									<Columns>
										<asp:BoundColumn></asp:BoundColumn>
										<asp:BoundColumn HeaderText="S.No"></asp:BoundColumn>
										<asp:BoundColumn DataField="UserName" HeaderText="User Name"></asp:BoundColumn>
										<asp:BoundColumn DataField="UnitName" HeaderText="Location"></asp:BoundColumn>
										<asp:BoundColumn DataField="Status" HeaderText="Status"></asp:BoundColumn>
										<asp:BoundColumn DataField="Remarks" HeaderText="Remarks"></asp:BoundColumn>
										<asp:BoundColumn DataField="AttendanceTime" HeaderText="AttendanceTime"></asp:BoundColumn>
									</Columns>
								</asp:datagrid></DIV>
						</TD>
					</TR>
				</TBODY>
			</table>
		</form>
	</body>
</HTML>
