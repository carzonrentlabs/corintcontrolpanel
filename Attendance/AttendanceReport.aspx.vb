'/* 
' * Developed By :		Vivek Sharma on 28th June '07
' * Description	:		
' */
Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.text
Imports System
Public Class AttendanceReport
    Inherits System.Web.UI.Page
    Public objCommand As SqlCommand
    Public oConnection As SqlConnection
    Public objDataAdapter As SqlDataAdapter

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents dgSummary As System.Web.UI.WebControls.DataGrid
    Protected WithEvents btnReturn As System.Web.UI.WebControls.Button
    Protected WithEvents txtFromDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnSearch As System.Web.UI.WebControls.Button
    Protected WithEvents cboUnit As System.Web.UI.WebControls.DropDownList

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        btnSearch.Attributes("onClick") = "return valid();"
        If Not Page.IsPostBack Then
            FillUnit()
            dgSummary.DataSource = ""
            dgSummary.DataBind()
        End If
    End Sub

    Sub FillUnit()
        Dim strCity As String
        Dim dsCity As DataSet

        Dim oConnection As SqlConnection
        oConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))

        ' Get City ID of Login User
        strCity = " SELECT UnitID, UnitName FROM CORIntUnitMaster "

        ' // Open connection
        oConnection.Open()
        objCommand = New SqlCommand(strCity.ToString(), oConnection)
        ' // Create Data Adapter object and fill city id
        dsCity = New DataSet
        objDataAdapter = New SqlDataAdapter(objCommand)
        objDataAdapter.Fill(dsCity)
        cboUnit.DataSource = dsCity.Tables(0)
        cboUnit.DataTextField = dsCity.Tables(0).Columns(1).ToString()
        cboUnit.DataValueField = dsCity.Tables(0).Columns(0).ToString()
        cboUnit.DataBind()
        cboUnit.Items.Insert(0, New ListItem("All", "0"))
    End Sub

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim strQuery As StringBuilder
        Dim strDate As String
        Dim dsReport As DataSet

        Dim oConnection As SqlConnection
        oConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))

        strQuery = New StringBuilder(" SELECT distinct a.SysUserID, att.AttendanceDate, (a.FName + ' ' + ISNULL(a.LName,'')) as UserName, att.Remarks, b.UnitName ")
        strQuery.Append(" , Replace(Replace(Replace(Replace(att.Status, 'L', 'Leave'), 'T', 'Travel'), 'P', 'Present'), 'H', 'Half-Day') as Status, convert(char(10), att.AttendanceTime, 108) as AttendanceTime ")
        strQuery.Append(" FROM CORIntAttendance as att INNER JOIN CORIntSysUsersMaster as a ON att.SysUserID = a.SysUserID ")
        strQuery.Append(" , CORIntUnitMaster as b ")
        'strQuery.Append(" , CORIntUnitMaster as b, CORIntUnitCityMaster as c, CORIntCityMaster as d ")
        strQuery.Append(" WHERE (att.MarkAttendance = 'Y') and att.CityID = b.UnitID ")
        'strQuery.Append(" and b.UnitCityID = c.UnitCityID and c.CityName = d.CityName ")
        strQuery.Append(" AND convert(CHAR(10), att.AttendanceDate, 101) = '" & txtFromDate.Text & "' ")

        If Not cboUnit.SelectedValue.Equals("0") Then
            strQuery.Append(" AND att.CityID = " & Convert.ToInt32(cboUnit.SelectedValue))
        End If
        strQuery.Append(" ORDER BY a.SysUserID ")

        ' // Open connection
        oConnection.Open()
        objCommand = New SqlCommand(strQuery.ToString(), oConnection)
        dsReport = New DataSet
        objDataAdapter = New SqlDataAdapter(objCommand)
        objDataAdapter.Fill(dsReport)

        oConnection.Close()
        objCommand.Dispose()

        dgSummary.DataSource = dsReport.Tables(0)
        dgSummary.DataBind()
    End Sub

    Sub dgSummary_ItemDataBound(ByVal s As Object, ByVal e As DataGridItemEventArgs)
        Dim i As Integer
        Dim intSNo As Integer

        intSNo = 1
        For i = 0 To e.Item.ItemIndex
            e.Item.Cells(1).Text = intSNo

            intSNo = intSNo + 1
        Next
    End Sub

    Private Sub btnReturn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReturn.Click
        Session("loggedin_user") = ""
        Response.Redirect("index_login.aspx")
    End Sub
End Class
