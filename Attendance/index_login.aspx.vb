'/* 
' * Developed By :		Vivek Sharma on 23rd June '07
' * Description	:		
' */
Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.text
Imports System

Public Class index_login
    Inherits System.Web.UI.Page
    Public objCommand As SqlCommand
    Public oConnection As SqlConnection
    'Public objDataSet As DataSet
    Public objDataAdapter As SqlDataAdapter

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtLogin As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtPwd As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnLogin As System.Web.UI.WebControls.Button
    Protected WithEvents pnlmain As System.Web.UI.WebControls.Panel
    Protected WithEvents pnlConfirmation As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents dbRowCount As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
    End Sub
    'this rocedure is called when the login button is pressed by the user
    Private Sub btnLogin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLogin.Click
        Dim MyConnection As SqlConnection
        Dim intLogincheckvalue As SqlParameter
        Dim logedinuseid As SqlParameter
        Dim intloginvalue As Int32
        Dim intuserid As Int32
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        'execueting the stored procedure for checking the user login validity by using the output parameter
        cmd = New SqlCommand("procLoginCheck", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@loginid", txtLogin.Text.ToString)
        cmd.Parameters.Add("@loginpwd", txtPwd.Text.ToString)
        intLogincheckvalue = cmd.Parameters.Add("@returnvalue", SqlDbType.Int)
        logedinuseid = cmd.Parameters.Add("@logedinuserid", SqlDbType.Int)
        intLogincheckvalue.Direction = ParameterDirection.Output
        logedinuseid.Direction = ParameterDirection.Output
        MyConnection.Open()
        cmd.ExecuteNonQuery()
        'getting the value to know that whethre the user is vallid user for login or not
        intloginvalue = cmd.Parameters("@returnvalue").Value
        intuserid = cmd.Parameters("@logedinuserid").Value
        MyConnection.Close()
        check_loginvalue(intloginvalue, intuserid)

    End Sub
    'sub checking the login and navigating the user to the related links
    Sub check_loginvalue(ByVal returnvalue As Int32, ByVal intuserid As Int32)
        '1=wrong login id
        '2=user not active
        '3=user is active but password is wrong
        '0=user login succesfull
        Select Case returnvalue
            Case 0
                Session("loggedin_user") = intuserid
                fn_MarkAttendance()
                Response.Redirect("AttendanceSheet.aspx")
            Case 1
                pnlmain.Visible = False
                pnlConfirmation.Visible = True
                lblMessage.Text = "You are not a valid user. You have not entered the correct Login ID"
                hyplnkretry.Text = "Please try again"
                hyplnkretry.NavigateUrl = "index_login.aspx"

            Case 2
                pnlmain.Visible = False
                pnlConfirmation.Visible = True
                lblMessage.Text = "You have been deactivated as a system user"

            Case 3
                pnlmain.Visible = False
                pnlConfirmation.Visible = True
                lblMessage.Text = "You have not entered the correct password"
                hyplnkretry.Text = "Please try again"
                hyplnkretry.NavigateUrl = "index_login.aspx"
        End Select

    End Sub

    Sub fn_MarkAttendance()
        ' Create object for query execution
        Dim strQuery As StringBuilder

        Dim strCity As String
        Dim dsCity As DataSet

        Dim strCount As String
        Dim dsCount As DataSet
        Dim dsCityUser As DataSet

        Dim oConnection As SqlConnection
        oConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))

        Dim dt1 As DateTime
        dt1 = DateTime.Now.AddHours(12.5)
        Dim dt As String
        Dim dtTime As String
        dt = FormatDateTime(dt1, DateFormat.ShortDate)
        dtTime = FormatDateTime(dt1, DateFormat.ShortTime)

        ' Get City ID of Login User
        strCity = " SELECT b.UnitID FROM CORIntSysUsersMaster as a, CORIntUnitMaster as b WHERE a.UnitID = b.UnitID and a.SysUserID = " & Session("loggedin_user")

        ' // Open connection
        oConnection.Open()
        objCommand = New SqlCommand(strCity.ToString(), oConnection)
        ' // Create Data Adapter object and fill city id
        dsCity = New DataSet
        objDataAdapter = New SqlDataAdapter(objCommand)
        objDataAdapter.Fill(dsCity)

        ' Query for check records for current date and user city
        'strCount = " SELECT Count(*) as Rcount FROM CORIntAttendance WHERE convert(CHAR(10), AttendanceDate, 103) = convert(CHAR(10), dt, 103) and CityID =  " & Convert.ToInt32(dsCity.Tables(0).Rows(0)(0))
        strCount = " SELECT Count(*) as Rcount FROM CORIntAttendance WHERE AttendanceDate =  '" & dt & "' and CityID =  " & Convert.ToInt32(dsCity.Tables(0).Rows(0)(0))
        objCommand = New SqlCommand(strCount.ToString(), oConnection)
        dsCount = New DataSet
        objDataAdapter = New SqlDataAdapter(objCommand)
        objDataAdapter.Fill(dsCount)

        ' Check city users exist in attendance table
        If Convert.ToInt32(dsCount.Tables(0).Rows(0)(0)) = 0 Then
            ' Get all user of city
            strQuery = New StringBuilder(" SELECT (a.FName + ' ' + ISNULL(a.LName,'')) as UserName, a.SysUserID, b.UnitID ")
            strQuery.Append(" FROM CORIntSysUsersMaster as a ")
            strQuery.Append(" , CORIntUnitMaster as b ")
            strQuery.Append(" WHERE a.UnitID = b.UnitID and a.Active = 1 and a.OutSiderYN = 0 ")
            strQuery.Append(" and b.UnitID = " & Convert.ToInt32(dsCity.Tables(0).Rows(0)(0)))

            objCommand = New SqlCommand(strQuery.ToString(), oConnection)
            dsCityUser = New DataSet
            objDataAdapter = New SqlDataAdapter(objCommand)
            objDataAdapter.Fill(dsCityUser)

            ' Insert in attendance table
            Dim insertQuery As String
            Dim i As Integer
            Dim intCityID As Integer
            Dim intSysUserID As Integer
            ' Insert all city users in attendance table
            For i = 0 To dsCityUser.Tables(0).Rows.Count - 1
                intCityID = Convert.ToInt32(dsCityUser.Tables(0).Rows(i)(2))
                intSysUserID = Convert.ToInt32(dsCityUser.Tables(0).Rows(i)(1))

                ' Insert record
                insertQuery = " INSERT INTO CORIntAttendance(CityID, SysUserID, AttendanceDate, Status) VALUES(" & intCityID & ",  " & intSysUserID & ", '" & dt & "', 'P')"
                objCommand = New SqlCommand(insertQuery.ToString(), oConnection)
                objCommand.ExecuteNonQuery()
            Next

        End If
        Dim strQuery1 As String
        strQuery1 = "SELECT Count(*) as Rcount FROM CORIntAttendance  WHERE AttendanceDate =  '" & dt & "' and CityID =  " & Convert.ToInt32(dsCity.Tables(0).Rows(0)(0)) & " and SysUserID = " & Session("loggedin_user") & " and markattendance='Y' "
        objCommand = New SqlCommand(strQuery1, oConnection)
        dsCount = New DataSet
        objDataAdapter = New SqlDataAdapter(objCommand)
        objDataAdapter.Fill(dsCount)

        ' Update user attendance of city and date
        If Convert.ToInt32(dsCount.Tables(0).Rows(0)(0)) = 0 Then
            Dim updateQuery As String
            updateQuery = "UPDATE CORIntAttendance SET MarkAttendance = 'Y', Remarks = 'Present', AttendanceTime = '" & dtTime & "' WHERE CityID = " & Convert.ToInt32(dsCity.Tables(0).Rows(0)(0)) & " and AttendanceDate = '" & dt & "' and SysUserID = " & Session("loggedin_user")
            objCommand = New SqlCommand(updateQuery.ToString(), oConnection)
            objCommand.ExecuteNonQuery()
        End If

        oConnection.Close()
        objCommand.Dispose()
    End Sub
End Class
