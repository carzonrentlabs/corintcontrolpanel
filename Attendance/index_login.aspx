<%@ Page Language="vb" AutoEventWireup="false" Inherits="index_login" src="index_login.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Log in to the CarzonRent :: Internal software Control panel</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="HertzInt.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<asp:panel id="pnlmain" Runat="server">
				<TABLE width="780" align="center">
					<TR>
						<TD align="left">
							<H3>Daily Attendance Sheet</H3>
						</TD>
						<TD align="right"><IMG src="../images/logo_hertz.gif" width="100" align="right"></TD>
					</TR>
					<TR>
						<TD class="redHead" align="center" colSpan="2"><BR>
							<BR>
							<BR>
							<BR>
							<BR>
							<BR>
							<BR>
							<BR>
							<BR>
						</TD>
					</TR>
					<TR>
						<TD align="center" colSpan="2">
							<TABLE width="50%">
								<TR>
									<TD class="subRedHead">Login</TD>
									<TD align="left">
										<asp:textbox id="txtLogin" runat="server" CssClass="input" MaxLength="20" Width="160px"></asp:textbox></TD>
								</TR>
								<TR>
									<TD class="subRedHead">Password</TD>
									<TD align="left">
										<asp:TextBox id="txtPwd" runat="server" CssClass="input" MaxLength="20" TextMode="Password" Width="160px"></asp:TextBox></TD>
								</TR>
								<TR>
									<TD align="center" colSpan="2">
										<asp:Button id="btnLogin" runat="server" CssClass="input" Text="Login"></asp:Button></TD>
								</TR>
							</TABLE>
						</TD>
					</TR>
					<TR>
						<TD class="redHead" align="center" colSpan="2"><BR>
							<BR>
							<BR>
							<BR>
							<BR>
							<BR>
							<BR>
							<BR>
							<BR>
						</TD>
					</TR>
					<TR>
						<TD align="center" colSpan="2"><FONT color="#3300ff"><B>� 2007 Carzonrent</B> India 
								Pvt. Ltd.</FONT>
						</TD>
					</TR>
				</TABLE>
			</asp:panel><asp:panel id="pnlConfirmation" Visible="False" Runat="server">
				<TABLE align="center">
					<TR>
						<TD><BR>
							<BR>
							<BR>
							<BR>
							<BR>
							<BR>
							<asp:Label id="lblMessage" runat="server"></asp:Label></TD>
					</TR>
					<TR align="center">
						<TD>
							<asp:HyperLink id="hyplnkretry" runat="server"></asp:HyperLink></TD>
					</TR>
				</TABLE>
			</asp:panel>
			<input type="hidden" id="dbRowCount" runat="server">
		</form>
	</body>
</HTML>
