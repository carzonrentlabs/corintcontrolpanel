<%@ Page Language="vb" AutoEventWireup="false" Inherits="AttendanceSheet" src="AttendanceSheet.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>AttendanceSheet</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../JScripts/Datefunc.js"></script>
		<script language="JavaScript" src="../utilityfunction.js"></script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<table width="780" align="center">
				<TBODY>
					<tr>
						<td><IMG src="../images/logo_carzonret.gif"></td>
						<td align="right"><IMG src="../images/logo_hertz.gif" width="100" align="right"></td>
					</tr>
					<asp:panel id="pnlmainform" Runat="server">
						<TR>
							<TD class="subRedHead" align="center" colSpan="2">Attendance Sheet</TD>
						</TR>
						<TR>
							<TD colSpan="2">
								<HR>
							</TD>
						</TR>
						<TR>
							<TD align="center" colSpan="2">
								<DIV style="OVERFLOW-Y: scroll; OVERFLOW-X: scroll; WIDTH: 780px; HEIGHT: 450px; visible: "
									noWrap>
									<asp:datagrid id="dgSummary" runat="server" OnItemDataBound="dgSummary_ItemDataBound" Width="100%"
										CellSpacing="2" CellPadding="0" AutoGenerateColumns="False">
										<SelectedItemStyle Wrap="False"></SelectedItemStyle>
										<EditItemStyle Wrap="False"></EditItemStyle>
										<HeaderStyle Font-Bold="True" Wrap="False" HorizontalAlign="Center" BackColor="#FFE5BA"></HeaderStyle>
										<Columns>
											<asp:BoundColumn></asp:BoundColumn>
											<asp:BoundColumn HeaderText="S.No"></asp:BoundColumn>
											<asp:BoundColumn DataField="UserName" HeaderText="User Name"></asp:BoundColumn>
											<asp:TemplateColumn HeaderText="Status">
												<ItemTemplate>
													<asp:DropDownList id="cboStatus" runat="server">
														<asp:ListItem Value="P">Present</asp:ListItem>
														<asp:ListItem Value="L">Leave</asp:ListItem>
														<asp:ListItem Value="T">Travel</asp:ListItem>
														<asp:ListItem Value="H">Half Day</asp:ListItem>
													</asp:DropDownList>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Remarks">
												<ItemTemplate>
													<asp:TextBox id="txtRemarks" runat="server" Columns="60"></asp:TextBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn DataField="EID" HeaderText="EID"></asp:BoundColumn>
											<asp:BoundColumn DataField="UnitID" HeaderText="CityID"></asp:BoundColumn>
											<asp:BoundColumn DataField="SysUserID" HeaderText="SysUserID"></asp:BoundColumn>
											<asp:BoundColumn DataField="AttendanceTime" HeaderText="AttendanceTime"></asp:BoundColumn>
											<asp:BoundColumn DataField="Status" HeaderText="Stat"></asp:BoundColumn>
											<asp:BoundColumn DataField="Remarks" HeaderText="Rem"></asp:BoundColumn>
											<asp:BoundColumn DataField="MarkAttendance" HeaderText="Att Status"></asp:BoundColumn>
											<asp:TemplateColumn HeaderText="Mark Attendance">
												<ItemTemplate>
													<asp:CheckBox id="chkMark" runat="server"></asp:CheckBox>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
									</asp:datagrid></DIV>
							</TD>
						</TR>
						<TR bgColor="#ffe5e0">
							<TD align="right">
								<asp:button id="btnSubmit" runat="server" Text="Submit"></asp:button>&nbsp;
								<asp:Button id="btnReturn" runat="server" Text="Back"></asp:Button></TD>
							<TD align="center"><B><A href="AttendanceReport.aspx">View Attendance</A></B></TD>
						</TR>
					</asp:panel><asp:panel id="pnlconfirmation" Runat="server" Visible="False">
						<TR align="center">
							<TD align="center" colSpan="2"><BR>
								<BR>
								<H2>
									<asp:Label id="lblMessage" runat="server"></asp:Label></H2>
							</TD>
						</TR>
						<TR>
							<TD align="center" colSpan="2"><BR>
								<BR>
								<asp:Button id="btnBack" runat="server" Text="Mark New Attendance"></asp:Button></TD>
						</TR>
					</asp:panel></TBODY></table>
			<input id="dbRowCount" type="hidden" runat="server">
		</form>
	</body>
</HTML>
