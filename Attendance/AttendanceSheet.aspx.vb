'/* 
' * Developed By :		Vivek Sharma on 23rd June '07
' * Description	:		
' */
Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.text
Imports System

Public Class AttendanceSheet
    Inherits System.Web.UI.Page
    Public objDataReader As SqlDataReader
    Public objDataAdapter As SqlDataAdapter
    Public objDataSet As DataSet
    Public objCommand As SqlCommand
    Protected WithEvents dbRowCount As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents btnReturn As System.Web.UI.WebControls.Button
    Public oConnection As SqlConnection

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents dgSummary As System.Web.UI.WebControls.DataGrid
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnBack As System.Web.UI.WebControls.Button

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not Page.IsPostBack Then
            Dim strCount As String
            Dim dsCount As DataSet

            Dim strName As String
            Dim dsName As DataSet

            Dim oConnection As SqlConnection
            oConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))

            strCount = " SELECT Count(*) as rCount FROM CORIntSysUsersModAccessMaster WHERE ModFuncID = 44 and SysUserID = " & Session("loggedin_user")
            objCommand = New SqlCommand(strCount.ToString(), oConnection)
            dsCount = New DataSet
            objDataAdapter = New SqlDataAdapter(objCommand)
            objDataAdapter.Fill(dsCount)

            If Convert.ToInt32(dsCount.Tables(0).Rows(0)(0)) = 0 Then
                pnlmainform.Visible = False
                pnlconfirmation.Visible = True

                strName = " SELECT (FName + ' ' + ISNULL(LName,'')) as UserName FROM CORIntSysUsersMaster WHERE SysUserID = " & Session("loggedin_user")
                objCommand = New SqlCommand(strName.ToString(), oConnection)
                dsName = New DataSet
                objDataAdapter = New SqlDataAdapter(objCommand)
                objDataAdapter.Fill(dsName)

                lblMessage.Text = "Hi! " & dsName.Tables(0).Rows(0)(0) & " Your Attendance is marked for today."
            Else
                populateGrid()
            End If
        End If
    End Sub
    Sub populateGrid()
        ' Create object for query execution
        Dim strQuery As StringBuilder
        Dim strCity As String
        Dim dsCity As DataSet

        Dim strCount As String
        Dim dsCount As DataSet
        Dim dsCityUser As DataSet

        Dim dt1 As DateTime
        dt1 = DateTime.Now.AddHours(12.5)
        Dim dt As String
        Dim dtTime As String
        dt = FormatDateTime(dt1, DateFormat.ShortDate)
        dtTime = FormatDateTime(dt1, DateFormat.ShortTime)

        Dim oConnection As SqlConnection
        oConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))

        ' Get City ID of Login User
        strCity = " SELECT b.UnitID FROM CORIntSysUsersMaster as a, CORIntUnitMaster as b WHERE a.UnitID = b.UnitID and a.SysUserID = " & Session("loggedin_user")

        ' // Open connection
        oConnection.Open()
        objCommand = New SqlCommand(strCity.ToString(), oConnection)
        ' // Create Data Adapter object and fill city id
        dsCity = New DataSet
        objDataAdapter = New SqlDataAdapter(objCommand)
        objDataAdapter.Fill(dsCity)

        ' Query for check records for current date and user city
        strCount = " SELECT Count(*) as Rcount FROM CORIntAttendance WHERE AttendanceDate = '" & dt & "' and CityID =  " & Convert.ToInt32(dsCity.Tables(0).Rows(0)(0))
        objCommand = New SqlCommand(strCount.ToString(), oConnection)
        dsCount = New DataSet
        objDataAdapter = New SqlDataAdapter(objCommand)
        objDataAdapter.Fill(dsCount)

        strQuery = New StringBuilder(" SELECT (a.FName + ' ' + ISNULL(a.LName,'')) as UserName ")

        ' Check city users exist in attendance table
        If Convert.ToInt32(dsCount.Tables(0).Rows(0)(0)) <> 0 Then
            ' Get updated records
            strQuery.Append(" , ISNULL(att.Remarks, '') as Remarks, att.Status as Status ")
            strQuery.Append(" , att.EID as EID, b.UnitID, a.SysUserID, att.AttendanceDate, att.MarkAttendance, convert(char(10), att.AttendanceTime, 108) as AttendanceTime ")
            strQuery.Append(" FROM CORIntSysUsersMaster as a LEFT OUTER JOIN CORIntAttendance as att ON a.SysUserID = att.SysUserID ")

        Else
            ' Get records for insert
            strQuery.Append(" , '' as Remarks, 'P' as Status ")
            strQuery.Append(" , 0 as EID, b.UnitID, a.SysUserID, '' as AttendanceDate, 'N' as MarkAttendance, '' as AttendanceTime ")
            strQuery.Append(" FROM CORIntSysUsersMaster as a ")

        End If

        strQuery.Append(" , CORIntUnitMaster as b ")
        strQuery.Append(" WHERE a.UnitID = b.UnitID and a.Active = 1 and a.OutSiderYN = 0 ")
        strQuery.Append(" and b.UnitID = " & Convert.ToInt32(dsCity.Tables(0).Rows(0)(0)))

        If Convert.ToInt32(dsCount.Tables(0).Rows(0)(0)) <> 0 Then
            strQuery.Append(" and att.AttendanceDate = '" & dt & "' ")
        End If

        strQuery.Append(" ORDER BY a.FName ")

        objCommand = New SqlCommand(strQuery.ToString(), oConnection)

        ' // Create Data Adapter object and fill value in that
        objDataSet = New DataSet
        objDataAdapter = New SqlDataAdapter(objCommand)
        objDataAdapter.Fill(objDataSet)

        oConnection.Close()
        objCommand.Dispose()

        dbRowCount.Value = objDataSet.Tables(0).Rows.Count

        dgSummary.DataSource = objDataSet.Tables(0)
        dgSummary.DataBind()
    End Sub
    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        ' Create object for query execution
        Dim strQuery As String
        Dim dtrreader As SqlDataReader
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility

        Dim i As Integer
        Dim intCityID As Integer
        Dim intSysUserID As Integer
        Dim strStatus As String
        Dim strRemarks As String
        Dim strMark As String
        Dim intEID As Integer
        Dim cStatus As DropDownList
        Dim tRemarks As TextBox
        Dim cMark As CheckBox

        Dim dt1 As DateTime
        dt1 = DateTime.Now.AddHours(12.5)
        Dim dt As String
        Dim dtTime As String
        dt = FormatDateTime(dt1, DateFormat.ShortDate)
        dtTime = FormatDateTime(dt1, DateFormat.ShortTime)

        Dim oConnection As SqlConnection
        oConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))

        ' // Open connection
        oConnection.Open()

        For i = 0 To dbRowCount.Value - 1
            intCityID = Convert.ToInt32(dgSummary.Items(i).Cells(6).Text)
            intSysUserID = Convert.ToInt32(dgSummary.Items(i).Cells(7).Text)
            intEID = Convert.ToInt32(dgSummary.Items(i).Cells(5).Text)

            cStatus = dgSummary.Items(i).FindControl("cboStatus")
            strStatus = cStatus.SelectedValue

            tRemarks = dgSummary.Items(i).FindControl("txtRemarks")
            strRemarks = tRemarks.Text

            cMark = dgSummary.Items(i).FindControl("chkMark")
            If cMark.Checked Then
                strMark = "Y"
            Else
                strMark = "N"
            End If

            If intEID = 0 Then
                ' Insert record
                strQuery = " INSERT INTO CORIntAttendance(CityID, SysUserID, AttendanceDate, AttendanceTime, Status, Remarks) VALUES(" & intCityID & ",  " & intSysUserID & ", '" & dt & "', '" & dtTime & "', '" & strStatus & "', '" & strRemarks & "')"

            Else
                ' Update record
                strQuery = "UPDATE CORIntAttendance SET Status = '" & strStatus & "', Remarks = '" & strRemarks & "', MarkAttendance = '" & strMark & "', AttendanceTime = '" & dtTime & "' WHERE EID = " & intEID
            End If

            objCommand = New SqlCommand(strQuery.ToString(), oConnection)
            objCommand.ExecuteNonQuery()
        Next

        oConnection.Close()
        objCommand.Dispose()

        'populateGrid()
        Session("loggedin_user") = ""
        Response.Redirect("index_login.aspx")
    End Sub

    Sub dgSummary_ItemDataBound(ByVal s As Object, ByVal e As DataGridItemEventArgs)
        Dim cStatus As DropDownList
        Dim tRemarks As TextBox
        Dim cMark As CheckBox
        Dim i As Integer
        Dim intSNo As Integer
        e.Item.Cells(5).Visible = False
        e.Item.Cells(6).Visible = False
        e.Item.Cells(7).Visible = False
        e.Item.Cells(8).Visible = False
        e.Item.Cells(9).Visible = False
        e.Item.Cells(10).Visible = False
        e.Item.Cells(11).Visible = False

        intSNo = 1
        For i = 0 To e.Item.ItemIndex
            e.Item.Cells(1).Text = intSNo

            cStatus = e.Item.Cells(3).FindControl("cboStatus")
            cStatus.SelectedValue = e.Item.Cells(9).Text.ToString()
            If e.Item.Cells(11).Text.ToString().Equals("Y") Then
                cStatus.Enabled = False
            End If

            tRemarks = e.Item.Cells(3).FindControl("txtRemarks")

            If e.Item.Cells(10).Text.ToString() = "&nbsp;" Then
                tRemarks.Text = ""
            Else
                tRemarks.Text = e.Item.Cells(10).Text.ToString()
            End If

            If e.Item.Cells(11).Text.ToString().Equals("Y") Then
                tRemarks.Attributes.Add("ReadOnly", True)
            End If

            cMark = e.Item.Cells(12).FindControl("chkMark")
            If e.Item.Cells(11).Text.ToString().Equals("Y") Then
                cMark.Checked = True
                cMark.Enabled = False
            Else
                cMark.Checked = False
            End If

            intSNo = intSNo + 1
        Next
    End Sub

    Private Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Session("loggedin_user") = ""
        Response.Redirect("index_login.aspx")
    End Sub

    Private Sub btnReturn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReturn.Click
        Session("loggedin_user") = ""
        Response.Redirect("index_login.aspx")
    End Sub
End Class
