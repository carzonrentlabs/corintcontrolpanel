Imports commonutility
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.EnterpriseServices
Imports System.Configuration
Imports System
Public Class PkgsIndv2IndvAssignSearch
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents hyplnkretry2 As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    Protected WithEvents ddlCoFrom As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlCoTo As System.Web.UI.WebControls.DropDownList
    Protected WithEvents btnsubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnreset As System.Web.UI.WebControls.Button

   Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    Protected WithEvents lblerr As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btnsubmit.Attributes("onClick") = "return validation();"
        If Not Page.IsPostBack Then
            populateddl()
        End If
    End Sub
    Sub populateddl()
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility

        ddlCoFrom.DataSource = objAcessdata.funcGetSQLDataReader("select ClientIndivID ,FName + ' ' + LName + ' - ' + EmailID as ClientName  from CORIntClientIndivMaster where TariffType='S' order by ClientName")
        ddlCoFrom.DataValueField = "ClientIndivID"
        ddlCoFrom.DataTextField = "ClientName"
        ddlCoFrom.DataBind()
        ddlCoFrom.Items.Insert(0, New ListItem("", ""))

        ddlCoTo.DataSource = objAcessdata.funcGetSQLDataReader("select ClientIndivID ,FName + ' ' + LName + ' - ' + EmailID as ClientName  from CORIntClientIndivMaster where TariffType='S' order by ClientName")
        ddlCoTo.DataValueField = "ClientIndivID"
        ddlCoTo.DataTextField = "ClientName"
        ddlCoTo.DataBind()
        ddlCoTo.Items.Insert(0, New ListItem("", ""))

    End Sub

    Private Sub btnsubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        Dim MyConnection As SqlConnection
        Dim MyConnection2 As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        MyConnection2 = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))

        Dim cmd As SqlCommand
        cmd = New SqlCommand("delete from CORIntChaufSplPkgsClientLink where ClientType = 'I' and ClientID = " & ddlCoTo.SelectedItem.Value, MyConnection)
        MyConnection.Open()
        cmd.ExecuteNonQuery()
        MyConnection.Close()

         myConnection.Open()

        Dim cmdFrom As SqlCommand
        cmdFrom = New SqlCommand("select PkgID from CORIntChaufSplPkgsClientLink where ClientType = 'I' and ClientID = " & ddlCoFrom.SelectedItem.Value, MyConnection)

         ' Create a DataReader to ferry information back from the database
         Dim myReader as SqlDataReader
         myReader = cmdFrom.ExecuteReader()
         'Iterate through the results
          While myReader.Read()
              Dim cmdTo As SqlCommand
              cmdTo = New SqlCommand("insert into CORIntChaufSplPkgsClientLink values ("&myReader("PkgID")&", 'I', "& ddlCoTo.SelectedItem.Value &")", MyConnection2)
              MyConnection2.Open()
              cmdTo.ExecuteNonQuery()
              myConnection2.Close()
          End While

         myConnection.Close()

        Dim cmdSD As SqlCommand
        cmdSD = New SqlCommand("delete from CORIntSDSplPkgsClientLink where ClientType = 'I' and ClientID = " & ddlCoTo.SelectedItem.Value, MyConnection)
        MyConnection.Open()
        cmdSD.ExecuteNonQuery()
        MyConnection.Close()

         myConnection.Open()

        Dim cmdFromSD As SqlCommand
        cmdFromSD = New SqlCommand("select PkgID from CORIntSDSplPkgsClientLink where ClientType = 'I' and ClientID = " & ddlCoFrom.SelectedItem.Value, MyConnection)

         ' Create a DataReader to ferry information back from the database
         Dim myReaderSD as SqlDataReader
         myReaderSD = cmdFromSD.ExecuteReader()
         'Iterate through the results
          While myReaderSD.Read()
              Dim cmdToSD As SqlCommand
              cmdToSD = New SqlCommand("insert into CORIntSDSplPkgsClientLink values ("&myReaderSD("PkgID")&", 'I', "& ddlCoTo.SelectedItem.Value &")", MyConnection2)
              MyConnection2.Open()
              cmdToSD.ExecuteNonQuery()
              myConnection2.Close()
          End While

         myConnection.Close()

        pnlmainform.Visible = False
        pnlconfirmation.Visible = True

        lblMessage.Text = "You have Assigned the special packages successfully."
        hyplnkretry.Text = "Copy special packages from one Individual to another"
        hyplnkretry.NavigateUrl = "PkgsIndv2IndvAssignSearch.aspx"
       
    End Sub

    Private Sub btnreset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnreset.Click
        Response.Redirect("PkgsIndv2IndvAssignSearch.aspx")
    End Sub
End Class
