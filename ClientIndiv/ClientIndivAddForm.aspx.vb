Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class ClientIndivAddForm
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtfname As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtmname As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtlname As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtdesig As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtadd As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtSTDCode As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtph1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtph As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtalternatefname As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtalternatemname As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtalternatelname As System.Web.UI.WebControls.TextBox
    Protected WithEvents tctalternatecphone As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtfax As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtemail As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtDOB As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtAnniv As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtEmailID2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtResAdd As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtarearemarks As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkActive As System.Web.UI.WebControls.CheckBox
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnReset As System.Web.UI.WebControls.Button
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents hyplnkretry2 As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    Protected WithEvents txtcompname As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddldiscount As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtcclimit As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddTariffType As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtContractDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkFuelSurchargeYN As System.Web.UI.WebControls.CheckBox
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label



    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btnSubmit.Attributes("OnClick") = "return validate();"
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility
        objAcessdata.funcpopulatenumddw(100, 0, ddldiscount)
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("ClientIndivAddForm.aspx")
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        Dim intuniqcheck As SqlParameter
        Dim intuniqvalue as int32
        Dim intclientindividParam As SqlParameter
        Dim intclientindivid as int32

        cmd = New SqlCommand("procAddindvclientMaster", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("@fname", txtfname.Text)
        cmd.Parameters.Add("@mname", txtmname.Text)
        cmd.Parameters.Add("@lname", txtlname.Text)
        cmd.Parameters.Add("@companyname", txtcompname.Text)
        cmd.Parameters.Add("@designation", txtdesig.Text)
        cmd.Parameters.Add("@address", txtadd.Text)
        cmd.Parameters.Add("@STDCode", txtSTDCode.Text)
        cmd.Parameters.Add("@phone1", txtph1.Text)
        cmd.Parameters.Add("@phone2", txtph.Text)
        cmd.Parameters.Add("@altfname", txtalternatefname.Text)
        cmd.Parameters.Add("@altmname", txtalternatemname.Text)
        cmd.Parameters.Add("@altlname", txtalternatelname.Text)
        cmd.Parameters.Add("@altphone", tctalternatecphone.Text)
        cmd.Parameters.Add("@fax", txtfax.Text)
        cmd.Parameters.Add("@emailid", txtemail.Text)
        cmd.Parameters.Add("@DOB", txtDOB.Text)
        cmd.Parameters.Add("@Anniv", txtAnniv.Text)
        cmd.Parameters.Add("@EmailID2", txtEmailID2.Text)
        cmd.Parameters.Add("@discountpc", ddldiscount.SelectedItem.Value)
if not txtcclimit.text="" then
        cmd.Parameters.Add("@creditlimit", txtcclimit.Text)
end if
        cmd.Parameters.Add("@TariffType", ddTariffType.SelectedItem.Value)
        cmd.Parameters.Add("@ContractDate", txtContractDate.Text)
        cmd.Parameters.Add("@FuelSurchargeYN", get_YNvalue(chkFuelSurchargeYN))
        cmd.Parameters.Add("@remarks", txtarearemarks.Text)
        cmd.Parameters.Add("@active", get_YNvalue(chkActive))
        cmd.Parameters.Add("@createdby", Session("loggedin_user"))
        cmd.Parameters.Add("@ResAdd", txtResAdd.Text)
       intuniqcheck = cmd.Parameters.Add("@uniqcheckval", SqlDbType.Int)
       intuniqcheck.Direction = ParameterDirection.Output
       intclientindividParam = cmd.Parameters.Add("@clientindivid", SqlDbType.Int)
       intclientindividParam.Direction = ParameterDirection.Output


        '  Try
        MyConnection.Open()
        cmd.ExecuteNonQuery()
           
        intuniqvalue=cmd.Parameters("@uniqcheckval").Value
        intclientindivid=cmd.Parameters("@clientindivid").Value
        MyConnection.Close()
           if not intuniqvalue = 0 then
                lblErrorMsg.visible=true
                lblErrorMsg.text="Email already exist."
                exit sub
            else
		        lblErrorMsg.visible=false
                pnlmainform.Visible = False

                pnlconfirmation.Visible = True

                lblMessage.Text = "You have added the Individual Client successfully"
                hyplnkretry.Text = "Add another Individual Client"
                hyplnkretry.NavigateUrl = "ClientIndivAddForm.aspx"
                if ddTariffType.SelectedItem.Value = "S" then
                   hyplnkretry2.Text = "<b>PLEASE SELECT THE SPECIAL TARIFFS FOR THIS CLIENT</b>"
                   hyplnkretry2.NavigateUrl = "PkgsAssignForm.aspx?ID="& intclientindivid
                end if
        end if
        '   Catch
        '   End Try
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function


End Class
