<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Src="ClientIndivEditForm.aspx.vb" Inherits="ClientIndivEditForm"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CarzonRent :: Internal software</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="JavaScript" src="../utilityfunction.js"></script>
		<script language="JavaScript" src="../JScripts/Datefunc.js"></script>
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
		<script language="javascript">
		function validate()
		{
				if (isNaN(document.forms[0].txtSTDCode.value))
				{
					alert("The STD code should be numeric only");
					return false;
				}
				if (isNaN(document.forms[0].txtph1.value))
				{
					alert("The Mobile # should be numeric only");
					return false;
				}
				if (document.forms[0].txtph1.value.length != 10)
				{
					alert("The Mobile # must have 10 digits");
					return false;
				}
			if(document.forms[0].txtemail.value!="")
				{
					
					var theStr=document.forms[0].txtemail.value;
					var atIndex = theStr.indexOf('@'); 
					var dotIndex = theStr.indexOf('.', atIndex); 
					theSub = theStr.substring(0, dotIndex+1) 

				if ((atIndex < 1)||(atIndex != theStr.lastIndexOf('@'))||(dotIndex < atIndex + 2)||(theStr.length <= theSub.length)) 
				{ 
					alert("Email ID is not a valid Email ID");
					return false;
				}
				} 	
				
			if(document.forms[0].txtEmailID2.value!="")
				{
					
					var theStr=document.forms[0].txtEmailID2.value;
					var atIndex = theStr.indexOf('@'); 
					var dotIndex = theStr.indexOf('.', atIndex); 
					theSub = theStr.substring(0, dotIndex+1) 

				if ((atIndex < 1)||(atIndex != theStr.lastIndexOf('@'))||(dotIndex < atIndex + 2)||(theStr.length <= theSub.length)) 
				{ 
					alert("The personal Email ID is not a valid Email ID");
					return false;
				}
				}
				if (isNaN(document.forms[0].txtcclimit.value))
				{
				alert("Credit Limit should be numeric only")
				return false;
				}
			var strvalues
			strvalues=('txtfname,txtlname,txtContractDate,txtadd,txtSTDCode,txtph1,txtemail,ddldiscount')
			return checkmandatory(strvalues);
			}
			
					function dateReg(obj)
        {
            if(obj.value!="")
            {
                // alert(obj.value);
                var reg = /((0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[0-1])-(201[0-9]))/
                if(reg.test(obj.value))
                {
                    //alert('valid');
                }
                else
                {
                    alert('notvalid');
                    obj.value="";
                }
            }
        }


		</script>
	</HEAD>
	<body onload="OnLoadshowLength(document.forms[0].txtarearemarks.value,shwMessage)">
		<form id="Form1" method="post" runat="server">
			<uc1:Headerctrl id="Headerctrl1" runat="server"></uc1:Headerctrl>
			<table align="center" id="Table1">
				<asp:panel id="pnlmainform" Runat="server">
					<TBODY>
						<TR>
							<TD align="center" colSpan="2"><B><U>Edit an Individual Client</U></B>
								<BR>
								<BR>
							</TD>
						</TR>
						<tr><td colspan="2" align="center"><asp:Label id="lblErrorMsg" cssclass="subRedHead" visible="false" runat="server"></asp:Label></td></tr>
						<TR>
						<TR>
							<TD>* First Name</TD>
							<TD>
								<asp:textbox id="txtfname" runat="server" MaxLength="50" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>Middle Namel</TD>
							<TD>
								<asp:textbox id="txtmname" runat="server" MaxLength="50" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>* Last Name</TD>
							<TD>
								<asp:textbox id="txtlname" runat="server" MaxLength="50" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>* Contract date
							</TD>
							<TD>
								<asp:textbox id="txtContractDate" runat="server" MaxLength="12" CssClass="input" onblur = "dateReg(this);" size="12"></asp:textbox>
                        <A onmouseover="window.status='Date Picker';return true;" onmouseout="window.status='';return true;" href="javascript:show_calendar('Form1.txtContractDate');"><IMG height="21" src="../images/show-calendar.gif" width="24" border="0"></A></FONT></TD>
						</TR>
						<TR>
							<TD>Company Name</TD>
							<TD>
								<asp:textbox id="txtcompname" runat="server" MaxLength="100" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>Designation</TD>
							<TD>
								<asp:textbox id="txtdesig" runat="server" MaxLength="50" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>* Address</TD>
							<TD>
								<asp:textbox id="txtadd" runat="server" MaxLength="250" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>* STD Code
							</TD>
							<TD>
								<asp:textbox id="txtSTDCode" size="10" runat="server" MaxLength="10" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>* Mobile #</TD>
							<TD>
								<asp:textbox id="txtph1" runat="server" MaxLength="10" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>Phone #</TD>
							<TD>
								<asp:textbox id="txtph" runat="server" MaxLength="20" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>Residence address</TD>
							<TD>
								<asp:textbox id="txtResAdd" runat="server" MaxLength="250" CssClass="input" size="70"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>Alternate Contact First Name</TD>
							<TD>
								<asp:textbox id="txtalternatefname" runat="server" MaxLength="50" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>Alternate Contact Middle Name</TD>
							<TD>
								<asp:textbox id="txtalternatemname" runat="server" MaxLength="50" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>Alternate Contact Last Name</TD>
							<TD>
								<asp:textbox id="txtalternatelname" runat="server" MaxLength="50" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>Alternate Contact Phone</TD>
							<TD>
								<asp:textbox id="tctalternatecphone" runat="server" MaxLength="20" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>Fax</TD>
							<TD>
								<asp:textbox id="txtfax" runat="server" MaxLength="20" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>* Email ID</TD>
							<TD>
								<asp:textbox id="txtemail" runat="server" MaxLength="100" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>Personal Email ID</TD>
							<TD>
								<asp:textbox id="txtEmailID2" runat="server" MaxLength="100" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>Date of Birth</TD>
							<TD>
								<asp:textbox id="txtDOB" runat="server" MaxLength="12" CssClass="input" onblur = "dateReg(this);" size="12"></asp:textbox><A onmouseover="window.status='Date Picker';return true;" onmouseout="window.status='';return true;" href="javascript:show_calendar('Form1.txtDOB');"><IMG height="21" src="../images/show-calendar.gif" width="24" border="0"></A></FONT> <a href="#" onClick='javascript:document.forms[0].txtDOB.value="";'>Clear</a></TD>
						</TR>
						<TR>
							<TD>Wedding Anniversary</TD>
							<TD>
								<asp:textbox id="txtAnniv" runat="server" MaxLength="12" CssClass="input" onblur = "dateReg(this);" size="12"></asp:textbox><A onmouseover="window.status='Date Picker';return true;" onmouseout="window.status='';return true;" href="javascript:show_calendar('Form1.txtAnniv');"><IMG height="21" src="../images/show-calendar.gif" width="24" border="0"></A></FONT> <a href="#" onClick='javascript:document.forms[0].txtAnniv.value="";'>Clear</a></TD>
						</TR>
						<TR>
							<TD>* Discount %</TD>
							<TD>
								<asp:DropDownList id="ddldiscount" runat="server" cssclass="input"></asp:DropDownList></TD>
						</TR>
						<TR>
							<TD>Credit Limit</TD>
							<TD>
								<asp:textbox id="txtcclimit" runat="server" CssClass="input" ></asp:textbox></TD>
						</TR>
						<TR>
							<TD>Tariff Type</TD>
							<TD>
							<asp:DropDownList id="ddTariffType" runat="server" cssclass="input">
								<asp:listitem Text="National" Value="N" />
								<asp:listitem Text="Special" Value="S" />
							</asp:DropDownList></TD></TR>
               	<TR>
							<TD>Apply fuel impact</TD>
							<TD>
								<asp:checkbox id="chkFuelSurchargeYN" runat="server" CssClass="input" Checked="False"></asp:checkbox></TD>
						</TR>
						<TR>
							<TD vAlign="top">Remarks(<SPAN class="shwText" id="shwMessage">0</SPAN>/2000 chars)</TD>
							<TD>
								<asp:textbox id="txtarearemarks" onkeydown="showLength(this.form.txtarearemarks,'document.all.shwMessage',2000)"
									onkeyup="showLength(this.form.txtarearemarks,'document.all.shwMessage',2000)" runat="server"
									MaxLength="2000" CssClass="input" Rows="3" Columns="50" TextMode="MultiLine"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>Active</TD>
							<TD>
								<asp:checkbox id="chkActive" runat="server" CssClass="input" Checked="True"></asp:checkbox></TD>
						</TR>
						<TR>
							<TD align="center" colSpan="2">
								<asp:button id="btnSubmit" runat="server" CssClass="input" Text="Submit"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;
								<asp:button id="btnReset" runat="server" CssClass="input" Text="Reset"></asp:button></TD>
						</TR>
				</asp:panel><asp:panel id="pnlconfirmation" Runat="server" Visible="False">
					<TR align="center">
						<TD colSpan="2">
							<asp:Label id="lblMessage" runat="server"></asp:Label></TD>
					</TR>
					<TR align="center">
						<TD colSpan="2">
							<asp:HyperLink id="hyplnkretry2" runat="server"></asp:HyperLink><br><br></TD>
					</TR>
					<TR align="center">
						<TD colSpan="2">
							<asp:HyperLink id="hyplnkretry" runat="server"></asp:HyperLink></TD>
					</TR>
				</asp:panel></TBODY></table>
		</form>
	</body>
</HTML>
