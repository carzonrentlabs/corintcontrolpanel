﻿<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="~/usercontrol/Headerctrl.ascx" %>

<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ClientBulkBatch.aspx.cs" Inherits="ClientBulkBatch" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CarzonRent :: Internal software control panel</title>
    <link href="../HertzInt.css" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css" />
    <script src="//code.jquery.com/jquery-1.10.2.js" type="text/jscript"></script>
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js" type="text/jscript"></script>
    <link rel="stylesheet" href="/resources/demos/style.css" />
    <script type="text/javascript">
        function ValidateControl() {
            if (document.getElementById("<%=ddlClientCoName.ClientID%>").selectedIndex == 0) {
                alert("Select client name");
                document.getElementById("<%=ddlClientCoName.ClientID%>").focus();
                return false;
            }
        }

        $(document).ready(function () {
            $("#<%=txtEffectiveDate.ClientID %>").datepicker(
                {
                    beforeShowDay: available
                    //,minDate: 0
                });
        });

        function validate_EffectiveDate() {
            if (document.getElementById("<%=txtEffectiveDate.ClientID%>").value == "") {
                alert("Select Package Effective Date From.");
                document.getElementById("<%=txtEffectiveDate.ClientID %>").focus();
                return false;
            }
        }

        function available(date) {
            var day = date.getDate();//,
            //last = (new Date(date.getFullYear(), date.getMonth() + 1, 0, 23, 59, 59)).getDate();
            if (day == 1) {
                return [true, "", "Available"];
            } else {
                return [false, "", "unAvailable"];
            }
        }

    </script>
</head>
<body>
    <form id="from" method="post" runat="server">
        <uc1:Headerctrl ID="Headerctrl1" runat="server"></uc1:Headerctrl>
        <center>
            <table border="0">
                <tr>
                    <td colspan="2" style="text-align: center;"><b>Client GST Mapping</b></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label></td>
                </tr>
                <tr>
                    <td><b>* Client Name</b></td>
                    <td>
                        <asp:DropDownList ID="ddlClientCoName" runat="server"></asp:DropDownList>&nbsp;
                    <asp:RequiredFieldValidator ID="rfValidateClientName" runat="server" ControlToValidate="ddlClientCoName" ErrorMessage="*" InitialValue="0" Display="Dynamic" ForeColor="Red" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>* Effective date</b>
                    </td>
                    <td>
                        <asp:TextBox ID="txtEffectiveDate" runat="server" CssClass="input"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td><b>* Client Type</b> </td>
                    <td>
                        <asp:DropDownList ID="ddlClientType" runat="server">
                            <asp:ListItem Text="SELECT" Value="0"></asp:ListItem>
                            <asp:ListItem Text="WEEKLY" Value="WEEKLY"></asp:ListItem>
                            <asp:ListItem Text="QUARTERLY" Value="QUARTERLY"></asp:ListItem>
                        </asp:DropDownList>
                    </td>

                </tr>
                <tr>
                    <td><b>* Active</b></td>
                    <td>
                        <asp:CheckBox ID="chkActive" runat="server" Checked="true" /></td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                    </td>
                </tr>
            </table>
        </center>
    </form>
</body>
</html>
