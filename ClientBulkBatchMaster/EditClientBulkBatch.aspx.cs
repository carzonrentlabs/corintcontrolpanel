﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class EditClientBulkBatch : System.Web.UI.Page
{
    string connectionString = System.Configuration.ConfigurationManager.AppSettings["corConnectString"];
    SqlConnection myConnection = null;
    SqlCommand cmd = null;
    SqlDataAdapter adp = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            lblMessage.Visible = false;
            if (!Page.IsPostBack)
            {
                BindClient();
                //btnSubmit.Attributes.Add("onclick", "ValidateControl()");
            }
            //btnSubmit.Attributes.Add("onclick", "ValidateControl()");
        }
    }
    private void BindClient()
    {
        DataSet dsClient = new DataSet();
        using (myConnection = new SqlConnection(connectionString))
        {
            adp = new SqlDataAdapter("select ClientCoName, ClientCoID from CorIntClientCoMaster where active=1 order by ClientCoName ", myConnection);
            adp.Fill(dsClient);
            ddlClientCoName.DataSource = dsClient.Tables[0];
            ddlClientCoName.DataTextField = "ClientCoName";
            ddlClientCoName.DataValueField = "ClientCoID";
            ddlClientCoName.DataBind();
            ddlClientCoName.Items.Insert(0, new ListItem("--All--", "0"));
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {

        DataSet dsUser = new DataSet();
        try
        {
            using (myConnection = new SqlConnection(connectionString))
            {
                myConnection.Open();
                cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "Prc_GetClientBulkBatchMasterDetails";
                cmd.Parameters.AddWithValue("@ClientCoId", ddlClientCoName.SelectedValue);
                cmd.Connection = myConnection;
                adp = new SqlDataAdapter(cmd);
                adp.Fill(dsUser);
                if (dsUser.Tables[0].Rows.Count > 0)
                {
                    grvUseDetails.DataSource = dsUser.Tables[0];
                    grvUseDetails.DataBind();
                }

            }

        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.Text = Ex.Message;
            lblMessage.ForeColor = System.Drawing.Color.Red;
        }

    }

    protected void grvUseDetails_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            int index = e.RowIndex;
            int Id = (int)grvUseDetails.DataKeys[index].Values[0];
            lblMessage.Visible = true;
            lblMessage.Text = Id.ToString();
            lblMessage.ForeColor = System.Drawing.Color.Red;
        }
        catch (Exception Ex)
        {

            lblMessage.Visible = true;
            lblMessage.Text = Ex.Message;
            lblMessage.ForeColor = System.Drawing.Color.Red;
        }
        
    }
    protected void grvUseDetails_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "EditDetails")
        {
            int index = Convert.ToInt16(e.CommandArgument);
            int Id = (int)grvUseDetails.DataKeys[index].Values[0];
            Response.Redirect("ClientBulkBatch.aspx?ID=" + Id, false);
        }
    }
}