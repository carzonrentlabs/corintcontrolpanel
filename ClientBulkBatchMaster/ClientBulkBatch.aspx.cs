﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

public partial class ClientBulkBatch : System.Web.UI.Page
{
    string connectionString = System.Configuration.ConfigurationManager.AppSettings["corConnectString"];
    SqlConnection myConnection = null;
    SqlCommand cmd = null;
    SqlDataAdapter adp = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        lblMessage.Visible = false;
        if (!Page.IsPostBack)
        {
            BindClient();
            btnSubmit.Attributes.Add("onclick", "ValidateControl()");
            if (Request.QueryString.HasKeys())
            {
                DataSet ds = new DataSet();
                string id = Request.QueryString["ID"];
                using (myConnection = new SqlConnection(connectionString))
                {
                    adp = new SqlDataAdapter("select ClientCoID,EffectiveDate,ClientType,Active from dbo.CorIntClientBulkBatchMaster where ID=" + id, myConnection);
                    adp.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        ddlClientCoName.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["ClientCoID"]);
                        ddlClientCoName.Enabled = false;
                        ddlClientType.Enabled = false;
                        txtEffectiveDate.Enabled = false;
                        ddlClientType.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["ClientType"]);
                        txtEffectiveDate.Text = Convert.ToDateTime(ds.Tables[0].Rows[0]["EffectiveDate"]).ToString("MM/dd/yyyy");
                        chkActive.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0]["Active"]);
                    }
                }
            }
            else
            {
                ddlClientCoName.Enabled = true;
                ddlClientType.Enabled = true;
                txtEffectiveDate.Enabled = true;
            }
        }
        btnSubmit.Attributes.Add("onclick", "ValidateControl()");
    }

    private void BindClient()
    {
        DataSet dsClient = new DataSet();
        using (myConnection = new SqlConnection(connectionString))
        {
            adp = new SqlDataAdapter("select ClientCoName, ClientCoID from dbo.CorIntClientCoMaster where active=1 order by ClientCoName ", myConnection);
            adp.Fill(dsClient);
            ddlClientCoName.DataSource = dsClient.Tables[0];
            ddlClientCoName.DataTextField = "ClientCoName";
            ddlClientCoName.DataValueField = "ClientCoID";
            ddlClientCoName.DataBind();
            ddlClientCoName.Items.Insert(0, new ListItem("--Select--", "0"));
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (ddlClientCoName.SelectedIndex == 0)
        {
            lblMessage.Visible = true;
            lblMessage.Text = "Select client name";
            lblMessage.ForeColor = System.Drawing.Color.Red;
        }
        else if (ddlClientType.SelectedIndex == 0)
        {
            lblMessage.Visible = true;
            lblMessage.Text = "Select Client Type";
            lblMessage.ForeColor = System.Drawing.Color.Red;
        }
        else if (string.IsNullOrEmpty(txtEffectiveDate.Text))
        {
            lblMessage.Visible = true;
            lblMessage.Text = "Select Effective Date";
            lblMessage.ForeColor = System.Drawing.Color.Red;
        }
        else
        {
            try
            {
                SqlParameter regIdentity;
                int status;
                using (myConnection = new SqlConnection(connectionString))
                {
                    myConnection.Open();
                    cmd = new SqlCommand();
                    cmd.CommandText = "Prc_SaveEditClientBulkBatchMaster";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ClientCoId", ddlClientCoName.SelectedValue);
                    cmd.Parameters.AddWithValue("@UserID", Session["loggedin_user"]);
                    cmd.Parameters.AddWithValue("@EffectiveDate", txtEffectiveDate.Text);
                    cmd.Parameters.AddWithValue("@ClientType", ddlClientType.SelectedValue);
                    cmd.Parameters.AddWithValue("@Active", chkActive.Checked);
                    regIdentity = cmd.Parameters.AddWithValue("@ID", SqlDbType.Int);
                    regIdentity.Direction = ParameterDirection.Output;
                    cmd.Connection = myConnection;
                    cmd.ExecuteNonQuery();
                    status = Convert.ToInt32(cmd.Parameters["@ID"].Value);
                    if (status > 1)
                    {
                        lblMessage.Visible = true;
                        lblMessage.Text = "Added successfully";
                        lblMessage.ForeColor = System.Drawing.Color.Red;
                        ddlClientCoName.SelectedIndex = 0;
                        ddlClientType.SelectedIndex = 0;
                    }
                    else
                    {
                        lblMessage.Visible = true;
                        lblMessage.Text = "update successfully.";
                        lblMessage.ForeColor = System.Drawing.Color.Red;
                        ddlClientCoName.SelectedIndex = 0;
                        ddlClientType.SelectedIndex = 0;
                    }
                }
            }
            catch (Exception Ex)
            {
                lblMessage.Visible = true;
                lblMessage.Text = Ex.Message;
                lblMessage.ForeColor = System.Drawing.Color.Red;
            }
        }
    }
}