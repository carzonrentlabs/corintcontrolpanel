Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Imports System.Drawing

Partial Class Client_Mapping_Client_Mapping
    Inherits System.Web.UI.Page
    Private Const indexId As Integer = 0

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    End Sub

    Protected WithEvents lblusername As System.Web.UI.WebControls.Label
    Protected WithEvents btnreset As System.Web.UI.WebControls.Button
    'Protected WithEvents hdnSelectedlst As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btnSubmit.Attributes("onClick") = "return postForm();"
        If Not Page.IsPostBack Then
            BindClient()
        End If

    End Sub


    Sub BindClient()
        'Dim dtrreader As SqlDataReader
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility
        ddlParentClient.DataSource = objAcessdata.funcGetSQLDataReader("select clientcoid,clientconame from CORIntClientCoMaster  where Active=1 order by ClientCoName")
        ddlParentClient.DataValueField = "clientcoid"
        ddlParentClient.DataTextField = "clientconame"
        ddlParentClient.DataBind()
        ddlParentClient.Items.Insert(0, New ListItem("", ""))
        ddClientCoName.DataSource = objAcessdata.funcGetSQLDataReader("select clientcoid,clientconame from CORIntClientCoMaster  where Active=1  order by ClientCoName")
        ddClientCoName.DataValueField = "clientcoid"
        ddClientCoName.DataTextField = "clientconame"
        ddClientCoName.DataBind()
        ddClientCoName.Items.Insert(0, New ListItem("", ""))
        objAcessdata.Dispose()
    End Sub
    

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        Dim statusCheck As SqlParameter
        Dim arrModuleid As Array
        If Request.Form("hdnSelectedlst") = "" Then
            lblErrorMsg.Visible = True
            lblErrorMsg.Text = "Select client to mapped."
            lblErrorMsg.ForeColor = Color.Red
            Exit Sub
        End If
        arrModuleid = Request.Form("hdnSelectedlst").Split(",")
        Dim status As Int32
        Dim Items As Int32
        Items = ddlMappedClient.Items.Count
        If Items = 0 Then
            lblErrorMsg.Visible = True
            lblErrorMsg.Text = "Please select mapped Client."
            lblErrorMsg.ForeColor = Color.Red
            Exit Sub
        End If
        Dim index As Int32
        MyConnection.Open()
        For index = 0 To UBound(arrModuleid)
            'Arraylst.Add(ddlMappedClient.Items(index).Value)
            cmd = New SqlCommand("Prc_ClientCompanyMapping", MyConnection)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@ClientCoId", ddlParentClient.SelectedItem.Value)
            'cmd.Parameters.AddWithValue("@MppedId", ddlMappedClient.Items(index).Value)
            cmd.Parameters.AddWithValue("@MppedId", arrModuleid(index))
            cmd.Parameters.AddWithValue("@CreatedBy", Session("loggedin_user"))
            statusCheck = cmd.Parameters.Add("@Status", SqlDbType.Int)
            statusCheck.Direction = ParameterDirection.Output
            cmd.ExecuteNonQuery()
        Next
        ' Response.End()
        'cmd = New SqlCommand("Prc_ClientCompanyMapping", MyConnection)
        'cmd.CommandType = CommandType.StoredProcedure
        'cmd.Parameters.AddWithValue("@ClientCoId", ddClientCoName.SelectedItem.Value)
        'cmd.Parameters.AddWithValue("@MppedId", ddlMappedClient.SelectedItem.Value)
        'cmd.Parameters.AddWithValue("@CreatedBy", Session("loggedin_user"))
        'statusCheck = cmd.Parameters.Add("@Status", SqlDbType.Int)
        'statusCheck.Direction = ParameterDirection.Output
        'MyConnection.Open()
        'cmd.ExecuteNonQuery()
        status = cmd.Parameters("@Status").Value
        MyConnection.Close()
        If status = -11 Then
            lblErrorMsg.Visible = True
            lblErrorMsg.Text = "Client Mapping already exist."
            lblErrorMsg.ForeColor = Color.Red
            Exit Sub
        Else
            lblErrorMsg.Visible = True
            lblErrorMsg.Text = "You have added the client Mapping successfully"
            lblErrorMsg.ForeColor = Color.Red
        End If
        'DisplayClientMapping()
    End Sub
    Protected Sub ddlParentClient_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlParentClient.SelectedIndexChanged
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility
        ddlMappedClient.DataSource = objAcessdata.funcGetSQLDataReader("select ClientCoID,ClientCoName from CORIntClientCoMaster where ClientCoID in(select MappedId from CorIntClientCoMapping where ClientCoID='" + ddlParentClient.SelectedValue + "' and active = 1) and active = 1 order by ClientCoName")
        ddlMappedClient.DataValueField = "ClientCoID"
        ddlMappedClient.DataTextField = "ClientCoName"
        ddlMappedClient.DataBind()
        ddlMappedClient.Items.Insert(0, New ListItem("", ""))
        objAcessdata.Dispose()
        BindClientCoName()
    End Sub
    Private Sub BindClientCoName()
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility
        ddClientCoName.DataSource = objAcessdata.funcGetSQLDataReader("select ClientCoID,ClientCoName from CORIntClientCoMaster where ClientCoID not in(select MappedId from CorIntClientCoMapping where ClientCoID='" + ddlParentClient.SelectedValue + "' and active = 1) and active =1 order by ClientCoName")
        ddClientCoName.DataValueField = "ClientCoID"
        ddClientCoName.DataTextField = "ClientCoName"
        ddClientCoName.DataBind()
        'ddClientCoName.Items.Insert(0, New ListItem("", ""))
        objAcessdata.Dispose()

    End Sub

End Class
