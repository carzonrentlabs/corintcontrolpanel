Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Imports System.Drawing
Partial Class Client_Mapping_Client_MappingActive
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            BindClient()
            bntSave.Visible = False
        End If
    End Sub
    Sub BindClient()
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility
        ddParent.DataSource = objAcessdata.funcGetSQLDataReader("select clientcoid,clientconame from CORIntClientCoMaster  where Active=1 order by ClientCoName")
        ddParent.DataValueField = "clientcoid"
        ddParent.DataTextField = "clientconame"
        ddParent.DataBind()
        ddParent.Items.Insert(0, New ListItem("", ""))
        objAcessdata.Dispose()
    End Sub

    Protected Sub ddParent_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddParent.SelectedIndexChanged
        Dim mydataset As DataSet
        Dim myAddapter As SqlDataAdapter
        Dim MyConnection As SqlConnection
        lblErrorMsg.Visible = False
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        cmd = New SqlCommand("Prc_GetMappedClientCompany", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@ClientCoId", ddParent.SelectedValue)
        MyConnection.Open()
        mydataset = New DataSet
        myAddapter = New SqlDataAdapter
        myAddapter.SelectCommand = cmd
        myAddapter.Fill(mydataset, "ClientMapped")
        If (mydataset.Tables("ClientMapped").Rows.Count > 0) Then
            gvClientMapped.DataSource = mydataset.Tables("ClientMapped")
            gvClientMapped.DataBind()
            bntSave.Visible = True
        Else
            gvClientMapped.DataSource = mydataset.Tables("Test")
            gvClientMapped.DataBind()
            lblErrorMsg.Visible = True
            lblErrorMsg.Text = "Record not available."
            lblErrorMsg.ForeColor = Color.Red
            bntSave.Visible = False
        End If
        MyConnection.Close()
    End Sub

    Protected Sub gvClientMapped_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvClientMapped.RowDataBound
        Dim ServiceId As String
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim active As CheckBox = CType(e.Row.FindControl("chMapped"), CheckBox)
            ServiceId = gvClientMapped.DataKeys(e.Row.RowIndex).Values(1).ToString()
            If (ServiceId = True) Then
                active.Checked = True
            End If
        End If
    End Sub

    Protected Sub bntSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles bntSave.Click

        Dim mappingid, mappedId As Integer
        Dim i, active As Integer
        Dim status As Integer
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        For i = 0 To gvClientMapped.Rows.Count - 1
            mappingid = gvClientMapped.DataKeys(i).Values(0)
            Dim chBox As CheckBox = CType(gvClientMapped.Rows(i).FindControl("chMapped"), CheckBox)
            mappedId = gvClientMapped.DataKeys(i).Values(2)
            If (chBox.Checked = True) Then
                active = 1
            Else
                active = 0
            End If
            cmd = New SqlCommand("Prc_UpdateClientCoMapping_new", MyConnection)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@MappingId", mappingid)
            cmd.Parameters.AddWithValue("@MappedId", mappedId)
            cmd.Parameters.AddWithValue("@UserId", Session("loggedin_user"))
            cmd.Parameters.AddWithValue("@Active", active)
            MyConnection.Open()
            status = cmd.ExecuteNonQuery()
            MyConnection.Close()
        Next
        If status > 0 Then
            lblErrorMsg.Visible = True
            lblErrorMsg.Text = "Record updated successfully"
            lblErrorMsg.ForeColor = Color.Red
        End If
    End Sub
End Class
