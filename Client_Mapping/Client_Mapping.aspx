<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Client_Mapping.aspx.vb"
    Inherits="Client_Mapping_Client_Mapping" %>

<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>CarzonRent :: Internal software Control Panel</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="../HertzInt.css" type="text/css" rel="stylesheet">

    <script language="JavaScript" src="../utilityfunction.js"></script>

    <script language="JavaScript">
    function validate_input()
    {
    
	    if(document.getElementById("<%=ddlParentClient.ClientID%>").selectedIndex==0)
	    {
		    alert("Please Select Parent Client.")
		    document.getElementById("<%=ddlParentClient.ClientID%>").focus();
		    return false;
	    }
        //else if(document.getElementById ("<%=ddlMappedClient.ClientID%>").items ==0)
        else if(document.forms[0].ddlMappedClient.length==1)
	    {
		    alert("Please Select Mapped Client.")
		    document.getElementById ("<%=ddlMappedClient.ClientID%>").focus();
		    return false;
	    }
    }
    
    function AddClient()
	{
	if (parseInt(document.forms[0].ddClientCoName.selectedIndex,10) >= 0 )
	  {

		for(i = document.forms[0].ddClientCoName.length - 1; i >= 0;i--)
		{
			if (document.forms[0].ddClientCoName[i].selected)
			  {
				var oOption = document.createElement("OPTION");
				document.forms[0].ddlMappedClient.options.add(oOption);
				oOption.innerText = document.forms[0].ddClientCoName.options[document.forms[0].ddClientCoName.selectedIndex].text;
				oOption.value = document.forms[0].ddClientCoName.options[document.forms[0].ddClientCoName.selectedIndex].value;
				document.forms[0].ddClientCoName.options[document.forms[0].ddClientCoName.selectedIndex] = null ;
			}

		}
	  }
	else
	 { 
	 alert("Please select Client");
	 }
	}
	function RemoveClient()
		{
		if (parseInt(document.forms[0].ddlMappedClient.selectedIndex,10) >= 0 )
		    {

			for(i = document.forms[0].ddlMappedClient.length - 1; i >= 0;i--)
			{
				if (document.forms[0].ddlMappedClient[i].selected)
				  {
					var oOption = document.createElement("OPTION");
					document.forms[0].ddClientCoName.options.add(oOption);
					oOption.innerText = document.forms[0].ddlMappedClient.options[document.forms[0].ddlMappedClient.selectedIndex].text;
					oOption.value = document.forms[0].ddlMappedClient.options[document.forms[0].ddlMappedClient.selectedIndex].value;
					document.forms[0].ddlMappedClient.options[document.forms[0].ddlMappedClient.selectedIndex] = null ;
				}
			}
		  }
		else { alert("Please select Client to Remove");}
		}
		
	function postForm()
	{
	
	var strTmpValues = new String();
	var bFlag  = false;
	for(i = 1; i != document.forms[0].ddlMappedClient.length;i++)
		{
		    strTmpValues += document.forms[0].ddlMappedClient.options[i].value ;
			if (i + 1 !=  document.forms[0].ddlMappedClient.length)
				{
				strTmpValues += ",";
				}
		}
	    document.forms[0].hdnSelectedlst.value  = strTmpValues;		
	}
		
    </script>

</head>
<body>
    <form id="Form1" method="post" runat="server">
        <uc1:Headerctrl ID="Headerctrl1" runat="server"></uc1:Headerctrl>
        <table border="0" cellpadding="0" cellspacing="0" align="center" width="80%">
            <tr align="center" style="padding-top: 10px;">
                <td colspan="2">
                    <asp:Label ID="lblErrorMsg" CssClass="subRedHead" Visible="false" runat="server">
                    </asp:Label>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2" style="padding-top: 10px;">
                    <b><u>Add Client Mapping</u></b></td>
            </tr>
            <asp:Panel ID="pnlmainform" runat="server">
                <tr>
                    <td style="padding-top: 10px; padding-bottom: 15px; text-align: center" colspan="2">
                        * Parent Client &nbsp; &nbsp;&nbsp;
                        <asp:DropDownList ID="ddlParentClient" runat="server" AutoPostBack="true">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RrquiredField" runat="server" ErrorMessage="Plesae select Company!"
                            ControlToValidate="ddlParentClient" InitialValue=" "></asp:RequiredFieldValidator>
                            <input id="hdnSelectedlst" type="hidden" runat="server" name="hdnSelectedlst">
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: center;">
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="vertical-align: middle;">
                                    * Mapped Client &nbsp; &nbsp;&nbsp;</td>
                                <td>
                                    <asp:ListBox ID="ddClientCoName" runat="server" Rows="10" Width="250" Height="400"></asp:ListBox>
                                </td>
                                <td style="text-align: center; width: 150px">
                                    <%--<asp:Button ID="bntLeft" runat="server" Text=">> Add"  OnClientClick ="AddClient()"/>--%>
                                    <input type="button" id="bntLeft" value=" >> Add " onclick="javascript:AddClient();" />
                                    <br />
                                    <br />
                                    <br />
                                    <%--<asp:Button ID="bntRight" runat="server" Text="<< Remove" />--%>
                                    <input type="button" id="bntRight" value=" << Remove " onclick="javascript:RemoveClient();" />
                                </td>
                                <td>
                                    <asp:ListBox ID="ddlMappedClient" runat="server" Rows="10" Width="250" Height="400"
                                        SelectionMode="Multiple"></asp:ListBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <asp:Button ID="btnSubmit" runat="server" CssClass="input" Text="Submit" OnClientClick="validate_input();">
                        </asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="Reset" name="Reset" value="Reset" /></td>
                </tr>
            </asp:Panel>
        </table>
    </form>
</body>
</html>
