<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Client_MappingActive.aspx.vb"
    Inherits="Client_Mapping_Client_MappingActive" %>

<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CarzonRent :: Internal software Control Panel</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="../HertzInt.css" type="text/css" rel="stylesheet">

    <script language="JavaScript" src="../utilityfunction.js"></script>

</head>
<body>
    <form id="form1" runat="server">
        <uc1:Headerctrl ID="Headerctrl1" runat="server"></uc1:Headerctrl>
        <div>
            <table align="center">
                <tbody>
                    <tr>
                        <td align="center" colspan="2">
                            <b><u>Edit Client Mapping</u></b></td>
                    </tr>
                    <asp:Panel ID="pnlconfirmation" runat="server">
                        <tr>
                            <td>
                                * Parent Client
                            </td>
                            <td>
                                <asp:DropDownList ID="ddParent" runat="server" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr><td colspan ="2">&nbsp;</td></tr>
                        <tr align="center">
                            <td style="text-align: center;" colspan="2">
                                <asp:Label ID="lblErrorMsg" CssClass="subRedHead" Visible="false" runat="server">
                                </asp:Label>
                            </td>
                        </tr>
                        <tr align="center">
                            <td colspan="2">
                                <asp:GridView ID="gvClientMapped" runat="server" AutoGenerateColumns="false" DataKeyNames="mappingid,Active,MappedId"
                                    HeaderStyle-BorderStyle="Ridge"  HeaderStyle-BackColor ="#BDBDBD" BorderColor ="black" AlternatingRowStyle-BorderColor ="black"  >
                                    <Columns>
                                        <asp:TemplateField HeaderText="S.No." ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSNo" runat="server" Text="<%#Container.DisplayIndex + 1%>">"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="Client Name" DataField="ParentName" />
                                        <asp:BoundField HeaderText="Mapping Client Name" DataField="ClientCoName" />
                                        <asp:TemplateField HeaderText="Mapped" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chMapped" runat="server" value='<%#Eval("MappedId")%>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                        
                        <tr>
                            <td style="text-align: center;" colspan="2">
                                <asp:Button ID="bntSave" Text="Update" runat="Server" Visible="false" /></td>
                        </tr>
                    </asp:Panel>
                </tbody>
            </table>
        </div>
    </form>
</body>
</html>
