Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class MappingAdd
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ddlCompID As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents ddlfields As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlMisc1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlMisc2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlMisc3 As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlMisc4 As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlMisc5 As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlMisc6 As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlMisc7 As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnsubmit As System.Web.UI.WebControls.Button
    'Protected WithEvents chkMandatory As System.Web.UI.WebControls.CheckBox

    Protected WithEvents chkMand1 As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkMand2 As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkMand3 As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkMand4 As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkMand5 As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkMand6 As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkMand7 As System.Web.UI.WebControls.CheckBox

    Protected WithEvents chkactive1 As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkactive2 As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkactive3 As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkactive4 As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkactive5 As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkactive6 As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkactive7 As System.Web.UI.WebControls.CheckBox

    'Protected WithEvents chkActive As System.Web.UI.WebControls.CheckBox
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        btnsubmit.Attributes("onClick") = "return validation();"
        If Not Page.IsPostBack() Then
            populateddl()
        End If

    End Sub
    Sub populateddl()

        Dim objAcessdata As clsutility
        objAcessdata = New clsutility
        ddlCompID.DataSource = objAcessdata.funcGetSQLDataReader("select ClientCoName  ,ClientCoID   from CORIntClientCoMaster    where active=1  and  providerId=" & Session("provider_Id") & " order by ClientCoName ")
        ddlCompID.DataValueField = "ClientCoID"
        ddlCompID.DataTextField = "ClientCoName"
        ddlCompID.DataBind()
    End Sub

    Private Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        Response.Redirect("MappingAddList.aspx?CompID=" & ddlCompID.SelectedItem.Value)
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function
End Class