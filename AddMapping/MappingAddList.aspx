<%@ Page Language="vb" AutoEventWireup="false" Src="MappingAddList.aspx.vb" Codebehind="MappingAddList.aspx.vb"
    Inherits="MappingAddList" %>

<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>CarzonRent :: Internal software Control panel</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />

    <script language="JavaScript" src="../utilityfunction.js" type="text/javascript"></script>

    <script language="JavaScript" src="../JScripts/Datefunc.js" type="text/javascript"></script>

    <link href="../HertzInt.css" type="text/css" rel="stylesheet" />

    <script language="javascript" type="text/javascript">
		function validation()
		{

		}		
    </script>

</head>
<body ms_positioning="GridLayout">
    <form id="Form1" method="post" runat="server">
        <uc1:Headerctrl id="Headerctrl1" runat="server">
        </uc1:Headerctrl>
        <table id="Table1" align="center">
            <asp:Panel ID="pnlmainform" runat="server">
                <tbody>
                    <tr>
                        <td align="center" colspan="2">
                            <strong><u>Add a Mapping</u></strong></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            &nbsp;&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            * Company</td>
                        <td>
                            <asp:Label ID="ddlCompID" runat="server" CssClass="input"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                            * Miscelleneous1</td>
                        <td>
                            <asp:TextBox ID="ddlMisc1" runat="server" CssClass="input"></asp:TextBox>
                            <asp:CheckBox ID="chkMand1" runat="server" CssClass="input" Checked="True"></asp:CheckBox>
                            : MandatoryYN
                            <asp:CheckBox ID="chkactive1" runat="server" CssClass="input" Checked="True"></asp:CheckBox>
                            : ActiveYN
                        </td>
                        <td>
                            <asp:CheckBoxList ID="CheckBoxList1" runat="server">
                                <asp:ListItem Text="Text Box" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Drop Downlist" Value="1"></asp:ListItem>
                            </asp:CheckBoxList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            * Miscelleneous2</td>
                        <td>
                            <asp:TextBox ID="ddlMisc2" runat="server" CssClass="input"></asp:TextBox>
                            <asp:CheckBox ID="chkMand2" runat="server" CssClass="input" Checked="True"></asp:CheckBox>
                            : MandatoryYN
                            <asp:CheckBox ID="chkactive2" runat="server" CssClass="input" Checked="True"></asp:CheckBox>
                            : ActiveYN
                        </td>
                        <td>
                            <asp:CheckBoxList ID="CheckBoxList2" runat="server">
                                <asp:ListItem Text="Text Box" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Drop Downlist" Value="1"></asp:ListItem>
                            </asp:CheckBoxList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            * Miscelleneous3</td>
                        <td>
                            <asp:TextBox ID="ddlMisc3" runat="server" CssClass="input"></asp:TextBox>
                            <asp:CheckBox ID="chkMand3" runat="server" CssClass="input" Checked="True"></asp:CheckBox>
                            : MandatoryYN
                            <asp:CheckBox ID="chkactive3" runat="server" CssClass="input" Checked="True"></asp:CheckBox>
                            : ActiveYN
                        </td>
                        <td>
                            <asp:CheckBoxList ID="CheckBoxList3" runat="server">
                                <asp:ListItem Text="Text Box" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Drop Downlist" Value="1"></asp:ListItem>
                            </asp:CheckBoxList></td>
                    </tr>
                    <tr>
                        <td>
                            * Miscelleneous4</td>
                        <td>
                            <asp:TextBox ID="ddlMisc4" runat="server" CssClass="input"></asp:TextBox>
                            <asp:CheckBox ID="chkMand4" runat="server" CssClass="input" Checked="True"></asp:CheckBox>
                            : MandatoryYN
                            <asp:CheckBox ID="chkactive4" runat="server" CssClass="input" Checked="True"></asp:CheckBox>
                            : ActiveYN
                        </td>
                        <td>
                            <asp:CheckBoxList ID="CheckBoxList4" runat="server">
                                <asp:ListItem Text="Text Box" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Drop Downlist" Value="1"></asp:ListItem>
                            </asp:CheckBoxList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            * Miscelleneous5</td>
                        <td>
                            <asp:TextBox ID="ddlMisc5" runat="server" CssClass="input"></asp:TextBox>
                            <asp:CheckBox ID="chkMand5" runat="server" CssClass="input" Checked="True"></asp:CheckBox>
                            : MandatoryYN
                            <asp:CheckBox ID="chkactive5" runat="server" CssClass="input" Checked="True"></asp:CheckBox>
                            : ActiveYN
                        </td>
                        <td>
                            <asp:CheckBoxList ID="CheckBoxList5" runat="server">
                                <asp:ListItem Text="Text Box" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Drop Downlist" Value="1"></asp:ListItem>
                            </asp:CheckBoxList></td>
                    </tr>
                    <tr>
                        <td>
                            * Miscelleneous6</td>
                        <td>
                            <asp:TextBox ID="ddlMisc6" runat="server" CssClass="input"></asp:TextBox>
                            <asp:CheckBox ID="chkMand6" runat="server" CssClass="input" Checked="True"></asp:CheckBox>
                            : MandatoryYN
                            <asp:CheckBox ID="chkactive6" runat="server" CssClass="input" Checked="True"></asp:CheckBox>
                            : ActiveYN
                        </td>
                        <td>
                            <asp:CheckBoxList ID="CheckBoxList6" runat="server">
                                <asp:ListItem Text="Text Box" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Drop Downlist" Value="1"></asp:ListItem>
                            </asp:CheckBoxList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            * Miscelleneous7</td>
                        <td>
                            <asp:TextBox ID="ddlMisc7" runat="server" CssClass="input"></asp:TextBox>
                            <asp:CheckBox ID="chkMand7" runat="server" CssClass="input" Checked="True"></asp:CheckBox>
                            : MandatoryYN
                            <asp:CheckBox ID="chkactive7" runat="server" CssClass="input" Checked="True"></asp:CheckBox>
                            : ActiveYN
                        </td>
                        <td>
                            <asp:CheckBoxList ID="CheckBoxList7" runat="server">
                                <asp:ListItem Text="Text Box" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Drop Downlist" Value="1"></asp:ListItem>
                            </asp:CheckBoxList></td>
                    </tr>
                    <tr>
                        <td>
                            * Miscelleneous8</td>
                        <td>
                            <asp:TextBox ID="ddlMisc8" runat="server" CssClass="input"></asp:TextBox>
                            <asp:CheckBox ID="chkMand8" runat="server" CssClass="input" Checked="True"></asp:CheckBox>
                            : MandatoryYN
                            <asp:CheckBox ID="chkactive8" runat="server" CssClass="input" Checked="True"></asp:CheckBox>
                            : ActiveYN
                        </td>
                        <td>
                            <asp:CheckBoxList ID="CheckBoxList8" runat="server">
                                <asp:ListItem Text="Text Box" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Drop Downlist" Value="1"></asp:ListItem>
                            </asp:CheckBoxList></td>
                    </tr>
                    <tr>
                        <td>
                            * Miscelleneous9</td>
                        <td>
                            <asp:TextBox ID="ddlMisc9" runat="server" CssClass="input"></asp:TextBox>
                            <asp:CheckBox ID="chkMand9" runat="server" CssClass="input" Checked="True"></asp:CheckBox>
                            : MandatoryYN
                            <asp:CheckBox ID="chkactive9" runat="server" CssClass="input" Checked="True"></asp:CheckBox>
                            : ActiveYN
                        </td>
                        <td>
                            <asp:CheckBoxList ID="CheckBoxList9" runat="server">
                                <asp:ListItem Text="Text Box" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Drop Downlist" Value="1"></asp:ListItem>
                            </asp:CheckBoxList></td>
                    </tr>
                    <tr>
                        <td>
                            * Miscelleneous10</td>
                        <td>
                            <asp:TextBox ID="ddlMisc10" runat="server" CssClass="input"></asp:TextBox>
                            <asp:CheckBox ID="chkMand10" runat="server" CssClass="input" Checked="True"></asp:CheckBox>
                            : MandatoryYN
                            <asp:CheckBox ID="chkactive10" runat="server" CssClass="input" Checked="True"></asp:CheckBox>
                            : ActiveYN
                        </td>
                        <td>
                            <asp:CheckBoxList ID="CheckBoxList10" runat="server">
                                <asp:ListItem Text="Text Box" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Drop Downlist" Value="1"></asp:ListItem>
                            </asp:CheckBoxList></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <asp:Button ID="btnsubmit" runat="server" CssClass="button" Text="Submit"></asp:Button>&nbsp;&nbsp;
                            <asp:Button ID="btnreset" runat="server" CssClass="button" Text="Reset" CausesValidation="False">
                            </asp:Button></td>
                    </tr>
            </asp:Panel>
            <asp:Panel ID="pnlconfirmation" runat="server" Visible="False">
                <tr align="center">
                    <td colspan="2">
                        <asp:Label ID="lblErrorMsg" runat="server"></asp:Label></td>
                </tr>
                <tr align="center">
                    <td colspan="2">
                        <asp:HyperLink ID="hyplnkretry" runat="server"></asp:HyperLink></td>
                </tr>
            </asp:Panel>
            </TBODY></table>
    </form>
</body>
</html>
