<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AddMisMappingDropDownValues.aspx.vb"
    Inherits="AddMapping_AddMisMappingDropDownValues" %>

<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>CarzonRent :: Internal software Control panel</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <script language="JavaScript" src="../utilityfunction.js"></script>
    <link href="../HertzInt.css" type="text/css" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server">
    <uc1:Headerctrl ID="Headerctrl1" runat="server"></uc1:Headerctrl>
    <div>
        <table align="center">
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblErrorMsg" CssClass="subRedHead" Visible="false" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <b>Client Name : </b>
                    <asp:DropDownList ID="ddlClient" runat="server" AutoPostBack="true">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="grv_MisValues" runat="server" AutoGenerateColumns="False" BackColor="#DEBA84"
                        BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" CellPadding="3" CellSpacing="2"
                        ShowFooter="true" AllowPaging="true" PageSize="100">
                        <Columns>
                            <asp:TemplateField HeaderText="Map field ID">
                                <ItemTemplate>
                                    <asp:Label ID="lblMapfieldID" runat="server" Text='<%# Eval("MisFieldid")%>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblMapfieldID_Edit" runat="server" Text='<%# Eval("MisFieldid")%>'></asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Client ID">
                                <ItemTemplate>
                                    <asp:Label ID="lblClientID" runat="server" Text='<%# Eval("ClientCoId")%>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblClientID_Edit" runat="server" Text='<%# Eval("ClientCoId")%>'></asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DisPlay ID">
                                <ItemTemplate>
                                    <asp:Label ID="lblDisplayID" runat="server" Text='<%# Eval("ID")%>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblDisplayID_Edit" runat="server" Text='<%# Eval("ID")%>'></asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Map field Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblMapFieldName" runat="server" Text='<%# Eval("MapFieldName")%>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtMapFieldName_Edit" runat="server" Text='<%# Eval("MapFieldName")%>'></asp:TextBox>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtMapFieldName_Footer" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rvf_txtMapFieldName_Edit" ErrorMessage="*" runat="server"
                                        ControlToValidate="txtMapFieldName_Footer" ValidationGroup="drodownlist"></asp:RequiredFieldValidator>
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Client Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblClientName" runat="server" Text='<%# Eval("ClientCoName")%>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:DropDownList ID="ddlClientName_Edit" runat="server">
                                    </asp:DropDownList>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:DropDownList ID="ddlClientName_Footer" runat="server">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="rvf_ddlClientName" ErrorMessage="*" runat="server"
                                        ControlToValidate="ddlClientName_Footer" InitialValue="0" ValidationGroup="drodownlist"></asp:RequiredFieldValidator>
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Mis Field Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblMisFieldName" runat="server" Text='<%# Eval("DisplayName")%>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:DropDownList ID="ddlMisFieldName_Edit" runat="server">
                                    </asp:DropDownList>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:DropDownList ID="ddlMisFieldName_Footer" runat="server">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="rvf_ddlMisFieldName" ErrorMessage="*" runat="server"
                                        ControlToValidate="ddlMisFieldName_Footer" InitialValue="0" ValidationGroup="drodownlist"></asp:RequiredFieldValidator>
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Active">
                                <ItemTemplate>
                                    <asp:Label ID="lblActive" runat="server" Text='<%# Eval("Active")%>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:CheckBox ID="chkActive_Edit" runat="server" Checked="true" />
                                    <asp:Label ID="lblActive_Edit" runat="server" Text='<%# Eval("Active")%>'></asp:Label>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:CheckBox ID="chkActive_Footer" runat="server" />
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnedit" runat="server" CommandName="Edit" Text="Edit" CausesValidation="false"></asp:LinkButton>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:LinkButton ID="btnupdate" runat="server" CommandName="Update" Text="Update"
                                        CausesValidation="false"></asp:LinkButton>
                                    <asp:LinkButton ID="btncancel" runat="server" CommandName="Cancel" Text="Cancel"
                                        CausesValidation="false"></asp:LinkButton>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:Button ID="btnAdd" runat="server" Text="Add" ValidationGroup="drodownlist" OnClick="AddMisFieldValues" />
                                </FooterTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                        <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                        <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                        <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
