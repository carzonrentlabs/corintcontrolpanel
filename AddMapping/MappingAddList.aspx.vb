Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class MappingAddList
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ddlCompID As System.Web.UI.WebControls.Label
    'Protected WithEvents ddlfields As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlMisc1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlMisc2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlMisc3 As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlMisc4 As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlMisc5 As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlMisc6 As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlMisc7 As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlMisc8 As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlMisc9 As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlMisc10 As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnsubmit As System.Web.UI.WebControls.Button
    'Protected WithEvents chkMandatory As System.Web.UI.WebControls.CheckBox

    Protected WithEvents chkMand1 As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkMand2 As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkMand3 As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkMand4 As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkMand5 As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkMand6 As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkMand7 As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkMand8 As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkMand9 As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkMand10 As System.Web.UI.WebControls.CheckBox

    Protected WithEvents chkactive1 As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkactive2 As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkactive3 As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkactive4 As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkactive5 As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkactive6 As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkactive7 As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkactive8 As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkactive9 As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkactive10 As System.Web.UI.WebControls.CheckBox

    Protected WithEvents CheckBoxList1 As System.Web.UI.WebControls.CheckBoxList
    Protected WithEvents CheckBoxList2 As System.Web.UI.WebControls.CheckBoxList
    Protected WithEvents CheckBoxList3 As System.Web.UI.WebControls.CheckBoxList
    Protected WithEvents CheckBoxList4 As System.Web.UI.WebControls.CheckBoxList
    Protected WithEvents CheckBoxList5 As System.Web.UI.WebControls.CheckBoxList
    Protected WithEvents CheckBoxList6 As System.Web.UI.WebControls.CheckBoxList
    Protected WithEvents CheckBoxList7 As System.Web.UI.WebControls.CheckBoxList
    Protected WithEvents CheckBoxList8 As System.Web.UI.WebControls.CheckBoxList
    Protected WithEvents CheckBoxList9 As System.Web.UI.WebControls.CheckBoxList
    Protected WithEvents CheckBoxList10 As System.Web.UI.WebControls.CheckBoxList

    'Protected WithEvents chkActive As System.Web.UI.WebControls.CheckBox
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        btnsubmit.Attributes("onClick") = "return validation();"

        If Trim("" & Request.QueryString("CompID")) <> "" Then
            Dim intID As Integer
            intID = Request.QueryString("CompID")
            If Not IsPostBack() Then
                populateddl(intID)
            End If
        Else
            Response.Write("Bad request.")
            Response.End()
        End If
    End Sub
    Sub populateddl(ByVal intID As Integer)
        Dim strquery As String
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility
        Dim dtrreader As SqlDataReader

        strquery = New String("select isnull(a.FieldName,'') as FieldName,isnull(a.DisplayName,'') as DisplayName,a.MandatoryYN,a.Active ,b.ClientCoName, isnull(a.controltype,0) as controltype from CorIntMapReportFields as a (nolock) full outer join corintclientcomaster as b (nolock) on a.clientcoid=b.clientcoid where a.active=1 and b.ClientCoID = " & intID)
        'Response.Write(strquery)
        dtrreader = objAcessdata.funcGetSQLDataReader(strquery.ToString)
        Do While dtrreader.Read
            ddlCompID.Text = dtrreader("ClientCoName")

            If (dtrreader("FieldName") = "Misc1") Then
                ddlMisc1.Text = dtrreader("DisplayName")
                If (String.IsNullOrEmpty(dtrreader("DisplayName"))) Then
                    ddlMisc1.Enabled = True
                Else
                    ddlMisc1.Enabled = False
                End If


                If dtrreader("MandatoryYN") Then
                    chkMand1.Checked = True
                Else
                    chkMand1.Checked = False
                End If
                If dtrreader("Active") Then
                    chkactive1.Checked = True
                Else
                    chkactive1.Checked = False
                End If

                If dtrreader("controltype") = 0 Then
                    CheckBoxList1.Items(0).Selected = True
                Else
                    CheckBoxList1.Items(1).Selected = True
                End If

            End If


            If (dtrreader("FieldName") = "Misc2") Then
                ddlMisc2.Text = dtrreader("DisplayName")
                If (String.IsNullOrEmpty(dtrreader("DisplayName"))) Then
                    ddlMisc2.Enabled = True
                Else
                    ddlMisc2.Enabled = False
                End If


                If dtrreader("MandatoryYN") Then
                    chkMand2.Checked = True
                Else
                    chkMand2.Checked = False
                End If
                If dtrreader("Active") Then
                    chkactive2.Checked = True
                Else
                    chkactive2.Checked = False
                End If

                If dtrreader("controlType") = 0 Then
                    CheckBoxList2.Items(0).Selected = True

                Else
                    CheckBoxList2.Items(1).Selected = True
                End If

            End If

            If (dtrreader("FieldName") = "Misc3") Then
                ddlMisc3.Text = dtrreader("DisplayName")
                If (String.IsNullOrEmpty(dtrreader("DisplayName"))) Then
                    ddlMisc3.Enabled = True
                Else
                    ddlMisc3.Enabled = False
                End If


                If dtrreader("MandatoryYN") Then
                    chkMand3.Checked = True
                Else
                    chkMand3.Checked = False
                End If
                If dtrreader("Active") Then
                    chkactive3.Checked = True
                Else
                    chkactive3.Checked = False
                End If

                If dtrreader("controlType") = 0 Then
                    CheckBoxList3.Items(0).Selected = True

                Else
                    CheckBoxList3.Items(1).Selected = True
                End If
            End If

            If (dtrreader("FieldName") = "Misc4") Then
                ddlMisc4.Text = dtrreader("DisplayName")
                If (String.IsNullOrEmpty(dtrreader("DisplayName"))) Then
                    ddlMisc4.Enabled = True
                Else
                    ddlMisc4.Enabled = False
                End If


                If dtrreader("MandatoryYN") Then
                    chkMand4.Checked = True
                Else
                    chkMand4.Checked = False
                End If
                If dtrreader("Active") Then
                    chkactive4.Checked = True
                Else
                    chkactive4.Checked = False
                End If


                If dtrreader("controlType") = 0 Then
                    CheckBoxList4.Items(0).Selected = True

                Else
                    CheckBoxList4.Items(1).Selected = True
                End If
            End If

            If (dtrreader("FieldName") = "Misc5") Then
                ddlMisc5.Text = dtrreader("DisplayName")
                If (String.IsNullOrEmpty(dtrreader("DisplayName"))) Then
                    ddlMisc5.Enabled = True
                Else
                    ddlMisc5.Enabled = False
                End If


                If dtrreader("MandatoryYN") Then
                    chkMand5.Checked = True
                Else
                    chkMand5.Checked = False
                End If
                If dtrreader("Active") Then
                    chkactive5.Checked = True
                Else
                    chkactive5.Checked = False
                End If

                If dtrreader("controlType") = 0 Then
                    CheckBoxList5.Items(0).Selected = True

                Else
                    CheckBoxList5.Items(1).Selected = True
                End If
            End If

            If (dtrreader("FieldName") = "Misc6") Then
                ddlMisc6.Text = dtrreader("DisplayName")
                If (String.IsNullOrEmpty(dtrreader("DisplayName"))) Then
                    ddlMisc6.Enabled = True
                Else
                    ddlMisc6.Enabled = False
                End If


                If dtrreader("MandatoryYN") Then
                    chkMand6.Checked = True
                Else
                    chkMand6.Checked = False
                End If
                If dtrreader("Active") Then
                    chkactive6.Checked = True
                Else
                    chkactive6.Checked = False
                End If

                If dtrreader("controlType") = 0 Then
                    CheckBoxList6.Items(0).Selected = True

                Else
                    CheckBoxList6.Items(1).Selected = True
                End If
            End If

            If (dtrreader("FieldName") = "Misc7") Then
                ddlMisc7.Text = dtrreader("DisplayName")
                If (String.IsNullOrEmpty(dtrreader("DisplayName"))) Then
                    ddlMisc7.Enabled = True
                Else
                    ddlMisc7.Enabled = False
                End If


                If dtrreader("MandatoryYN") Then
                    chkMand7.Checked = True
                Else
                    chkMand7.Checked = False
                End If
                If dtrreader("Active") Then
                    chkactive7.Checked = True
                Else
                    chkactive7.Checked = False
                End If

                If dtrreader("controlType") = 0 Then
                    CheckBoxList7.Items(0).Selected = True

                Else
                    CheckBoxList7.Items(1).Selected = True
                End If
            End If


            If (dtrreader("FieldName") = "Misc8") Then
                ddlMisc8.Text = dtrreader("DisplayName")
                If (String.IsNullOrEmpty(dtrreader("DisplayName"))) Then
                    ddlMisc8.Enabled = True
                Else
                    ddlMisc8.Enabled = False
                End If


                If dtrreader("MandatoryYN") Then
                    chkMand8.Checked = True
                Else
                    chkMand8.Checked = False
                End If
                If dtrreader("Active") Then
                    chkactive8.Checked = True
                Else
                    chkactive8.Checked = False
                End If

                If dtrreader("controlType") = 0 Then
                    CheckBoxList8.Items(0).Selected = True

                Else
                    CheckBoxList8.Items(1).Selected = True
                End If
            End If

            If (dtrreader("FieldName") = "Misc9") Then
                ddlMisc9.Text = dtrreader("DisplayName")
                If (String.IsNullOrEmpty(dtrreader("DisplayName"))) Then
                    ddlMisc9.Enabled = True
                Else
                    ddlMisc9.Enabled = False
                End If


                If dtrreader("MandatoryYN") Then
                    chkMand9.Checked = True
                Else
                    chkMand9.Checked = False
                End If
                If dtrreader("Active") Then
                    chkactive9.Checked = True
                Else
                    chkactive9.Checked = False
                End If

                If dtrreader("controlType") = 0 Then
                    CheckBoxList9.Items(0).Selected = True

                Else
                    CheckBoxList9.Items(1).Selected = True
                End If
            End If

            If (dtrreader("FieldName") = "Misc10") Then
                ddlMisc10.Text = dtrreader("DisplayName")
                If (String.IsNullOrEmpty(dtrreader("DisplayName"))) Then
                    ddlMisc10.Enabled = True
                Else
                    ddlMisc10.Enabled = False
                End If


                If dtrreader("MandatoryYN") Then
                    chkMand10.Checked = True
                Else
                    chkMand10.Checked = False
                End If
                If dtrreader("Active") Then
                    chkactive10.Checked = True
                Else
                    chkactive10.Checked = False
                End If

                If dtrreader("controlType") = 0 Then
                    CheckBoxList10.Items(0).Selected = True

                Else
                    CheckBoxList10.Items(1).Selected = True
                End If
            End If

        Loop
    End Sub

    Private Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        Dim intFlagCheck As SqlParameter
        Dim intFlag As Int32

        Dim intFGRParam As SqlParameter
        Dim intFGRid As Int32
        Dim txtHr As String
        Dim count As Int32
        Dim strArray(10) As String
        Dim strArrayName(10) As String
        Dim strgetmandatory(10) As CheckBox
        Dim strgetactive(10) As CheckBox
        Dim strgetControlType(10) As String
        count = 0

        If (trim(ddlMisc1.Text) <> "") Then
            count = count + 1
            strArray(count) = ddlMisc1.Text
            strArrayName(count) = "Misc1"
            strgetmandatory(count) = chkMand1
            strgetactive(count) = chkactive1
            strgetControlType(count) = CheckBoxList1.SelectedValue.ToString()

        End If

        If (trim(ddlMisc2.Text) <> "") Then
            count = count + 1
            strArray(count) = ddlMisc2.Text
            strArrayName(count) = "Misc2"
            strgetmandatory(count) = chkMand2
            strgetactive(count) = chkactive2
            strgetControlType(count) = CheckBoxList2.SelectedValue.ToString()
        End If

        If (trim(ddlMisc3.Text) <> "") Then
            count = count + 1
            strArray(count) = ddlMisc3.Text
            strArrayName(count) = "Misc3"
            strgetmandatory(count) = chkMand3
            strgetactive(count) = chkactive3
            strgetControlType(count) = CheckBoxList3.SelectedValue.ToString()
        End If

        If (trim(ddlMisc4.Text) <> "") Then
            count = count + 1
            strArray(count) = ddlMisc4.Text
            strArrayName(count) = "Misc4"
            strgetmandatory(count) = chkMand4
            strgetactive(count) = chkactive4
            strgetControlType(count) = CheckBoxList4.SelectedValue.ToString()
        End If

        If (trim(ddlMisc5.Text) <> "") Then
            count = count + 1
            strArray(count) = ddlMisc5.Text
            strArrayName(count) = "Misc5"
            strgetmandatory(count) = chkMand5
            strgetactive(count) = chkactive5
            strgetControlType(count) = CheckBoxList5.SelectedValue.ToString()
        End If

        If (trim(ddlMisc6.Text) <> "") Then
            count = count + 1
            strArray(count) = ddlMisc6.Text
            strArrayName(count) = "Misc6"
            strgetmandatory(count) = chkMand6
            strgetactive(count) = chkactive6
            strgetControlType(count) = CheckBoxList6.SelectedValue.ToString()
        End If

        If (trim(ddlMisc7.Text) <> "") Then
            count = count + 1
            strArray(count) = ddlMisc7.Text
            strArrayName(count) = "Misc7"
            strgetmandatory(count) = chkMand7
            strgetactive(count) = chkactive7
            strgetControlType(count) = CheckBoxList7.SelectedValue.ToString()
        End If


        If (trim(ddlMisc8.Text) <> "") Then
            count = count + 1
            strArray(count) = ddlMisc8.Text
            strArrayName(count) = "Misc8"
            strgetmandatory(count) = chkMand8
            strgetactive(count) = chkactive8
            strgetControlType(count) = CheckBoxList8.SelectedValue.ToString()
        End If

        If (trim(ddlMisc9.Text) <> "") Then
            count = count + 1
            strArray(count) = ddlMisc9.Text
            strArrayName(count) = "Misc9"
            strgetmandatory(count) = chkMand9
            strgetactive(count) = chkactive9
            strgetControlType(count) = CheckBoxList9.SelectedValue.ToString()
        End If

        If (trim(ddlMisc10.Text) <> "") Then
            count = count + 1
            strArray(count) = ddlMisc10.Text
            strArrayName(count) = "Misc10"
            strgetmandatory(count) = chkMand10
            strgetactive(count) = chkactive10
            strgetControlType(count) = CheckBoxList10.SelectedValue.ToString()
        End If

        'Do While Not count <= 0
        'response.write(count)
        'response.write("<br>")
        'response.write(strArray(count))
        'response.write("<br>")
        'response.write(strArrayName(count))
        'response.write("<br>")
        'If count = 1 Then
        '    response.end()
        'End If
        'count = count - 1
        'Loop


        'response.write("You Are Here...")
        'response.end()
        'response.write(ddlfields.SelectedValue)
        'response.write("<br>")
        'response.write(ddlfields.SelectedItem)
        'response.write("<br>")
        'response.write(ddlCompID.SelectedValue)
        'response.write("<br>")
        'response.write(get_YNvalue(chkMandatory))
        'response.write("<br>")
        'response.write(get_YNvalue(chkActive))
        'response.write("<br>")
        'response.write(Session("loggedin_user"))
        'response.end()

        Do While Not count <= 0
            cmd = New SqlCommand("UspAddMapReportFields", MyConnection)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@FieldName", strArrayName(count))
            cmd.Parameters.Add("@DisplayName", strArray(count))
            cmd.Parameters.Add("@ClientCoID", Request.QueryString("CompID"))
            cmd.Parameters.Add("@CreatedBy", Session("loggedin_user"))
            cmd.Parameters.Add("@MandatoryYN", Get_YNvalue(strgetmandatory(count)))
            cmd.Parameters.Add("@Active", get_YNvalue(strgetactive(count)))
            cmd.Parameters.Add("@ControlType", get_YNvalue1(Convert.ToString(strgetControlType(count))))

            intFlagCheck = cmd.Parameters.Add("@StatusFlag", SqlDbType.Int)
            intFlagCheck.Direction = ParameterDirection.Output

            intFGRParam = cmd.Parameters.Add("@ID", SqlDbType.Int)
            intFGRParam.Direction = ParameterDirection.Output
            If (MyConnection.State = ConnectionState.Closed) Then
                MyConnection.Close()
                MyConnection.Open()
            End If
            'response.write(cmd)
            'response.end()
            cmd.ExecuteNonQuery()

            intFlag = cmd.Parameters("@StatusFlag").Value
            intFGRid = cmd.Parameters("@ID").Value

            count = count - 1

        Loop
        MyConnection.Close()

        lblErrorMsg.Visible = True

        'If Trim("" & intFlag) = "2" Then

        pnlmainform.Visible = False
        pnlconfirmation.Visible = True
        If count <> 0 Then
            '       lblErrorMsg.Text = "This Mapping already exists!<br> The Mapping ID is <b>" & intFGRid & "</b>"
            '      hyplnkretry.Text = "Add another Mapping"
            '     hyplnkretry.NavigateUrl = "MappingAdd.aspx"
            '    Else

            '   pnlmainform.Visible = False
            '  pnlconfirmation.Visible = True

            lblErrorMsg.Text = "You have added the Mapping <br>" ' The Mapping ID is <b>" & intFGRid & "</b>"
            hyplnkretry.Text = "Add another Mapping"
            hyplnkretry.NavigateUrl = "MappingAdd.aspx"
        Else
            lblErrorMsg.Text = "All fields already added.<br>" ' The Mapping ID is <b>" & intFGRid & "</b>"
            hyplnkretry.Text = "Add another Mapping"
            hyplnkretry.NavigateUrl = "MappingAdd.aspx"
        End If
        ' End If
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function
    Function get_YNvalue1(ByVal Value As String) As Int32
        Dim returnval As Int32
        If Value = "0" Then
            returnval = 0
        Else
            returnval = 1
        End If
        Return returnval
    End Function
End Class