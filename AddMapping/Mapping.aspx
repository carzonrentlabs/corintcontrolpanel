<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Src="Mapping.aspx.vb" Codebehind="Mapping.aspx.vb" Inherits="Mapping"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CarzonRent :: Internal software Control panel</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="JavaScript" src="../utilityfunction.js"></script>
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<uc1:Headerctrl id="Headerctrl1" runat="server"></uc1:Headerctrl>
			<TABLE id="Table1" align="center">
				<TBODY>
					<TR>
						<TD align="center" colSpan="2"><STRONG><U> Mapping</U></STRONG></TD>
					</TR>
					<TR>
						<TD align="center" colSpan="2">&nbsp;&nbsp;</TD>
					</TR>
					<TR>
						<TD align="center" colSpan="2"><a href="MappingAdd.aspx">Add a Mapping</a></TD>
					</TR>
					<TR>
						<TD align="center" colSpan="2"><a href="MappingSearch.aspx">Edit a Mapping</a></TD>
					</TR>
				</TBODY>
			</TABLE>
		</form>
	</body>
</HTML>
