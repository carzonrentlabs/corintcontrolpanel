Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Partial Class AddMapping_AddMisMappingDropDownValues
    Inherits System.Web.UI.Page

  

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        lblErrorMsg.Visible = False
        If Not Page.IsPostBack Then
            populateddl()
        End If

    End Sub

    Sub populateddl()
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility
        ddlClient.DataSource = objAcessdata.funcGetSQLDataReader("select ClientCoID,ClientCoName from CORIntClientCoMaster where active=1 order by ClientCoName ")
        ddlClient.DataValueField = "ClientCoID"
        ddlClient.DataTextField = "ClientCoName"
        ddlClient.DataBind()
        ddlClient.Items.Insert(0, New ListItem("", ""))
        objAcessdata.Dispose()

    End Sub

    Protected Sub ddlClient_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlClient.SelectedIndexChanged
        BindData()
    End Sub


    Private Sub BindData()
        Dim SqlConnection As SqlConnection

        Dim SqlDataAdapter As SqlDataAdapter = New SqlDataAdapter()
        Dim DataSet As DataSet = New DataSet()
        SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("corConnectString"))
        SqlConnection.Open()
        Dim SqlCommand As SqlCommand = New SqlCommand("select BrandMstr.ID as MisFieldid,BrandMstr.MapFieldName,BrandMstr.ClientCoid,BrandMstr.Active,ClientMstr.ClientCoName,MisField.ID,MisField.DisplayName  from CorIntBrandMaster as BrandMstr inner join CORIntClientCoMaster as ClientMstr on BrandMstr.ClientCoId= ClientMstr.ClientCoID inner join CorIntMapReportFields as MisField on MisField.ID=BrandMstr.DisplayID where BrandMstr.Active=1 and ClientMstr.Active=1 and MisField.Active=1 and ClientMstr.Clientcoid=" & ddlClient.SelectedValue.ToString() & "")

        SqlCommand.Connection = SqlConnection
        SqlDataAdapter = New SqlDataAdapter(SqlCommand)
        SqlDataAdapter.Fill(DataSet)
        If DataSet.Tables(0).Rows.Count >= 1 Then
            grv_MisValues.DataSource = DataSet
            grv_MisValues.DataBind()
        Else
            If DataSet.Tables(0).Rows.Count = 0 Then
                DataSet.Tables(0).Rows.Add(DataSet.Tables(0).NewRow())
                grv_MisValues.DataSource = DataSet
                grv_MisValues.DataBind()
                Dim columnCount As Integer = grv_MisValues.Rows(0).Cells.Count
                grv_MisValues.Rows(0).Cells.Clear()
                grv_MisValues.Rows(0).Cells.Add(New TableCell)
                grv_MisValues.Rows(0).Cells(0).ColumnSpan = columnCount
                grv_MisValues.Rows(0).Cells(0).Text = "No Records Found."

            End If
        End If
        SqlConnection.Close()
    End Sub

    Protected Sub grv_MisValues_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles grv_MisValues.RowCancelingEdit
        grv_MisValues.EditIndex = -1
        BindData()
    End Sub

    Protected Sub grv_MisValues_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles grv_MisValues.RowUpdating
        Try
            Dim SqlConnection As SqlConnection
            Dim SqlDataAdapter As SqlDataAdapter = New SqlDataAdapter()
            Dim DataSet As DataSet = New DataSet()
            SqlConnection = New SqlConnection(ConfigurationManager.AppSettings("corConnectString"))
            Dim lblMapfieldID As Label = DirectCast(grv_MisValues.Rows(e.RowIndex).FindControl("lblMapfieldID"), Label)
            Dim txtMapFieldName_Edit As TextBox = DirectCast(grv_MisValues.Rows(e.RowIndex).FindControl("txtMapFieldName_Edit"), TextBox)
            Dim ddlMisFieldName_Edit As DropDownList = DirectCast(grv_MisValues.Rows(e.RowIndex).FindControl("ddlMisFieldName_Edit"), DropDownList)
            Dim ddlClientName_Edit As DropDownList = DirectCast(grv_MisValues.Rows(e.RowIndex).FindControl("ddlClientName_Edit"), DropDownList)
            Dim chkActive As CheckBox = DirectCast(grv_MisValues.Rows(e.RowIndex).FindControl("chkActive_Footer"), CheckBox)
            Dim Active As Int32
            If chkActive.Checked = True Then
                Active = 1
            Else
                Active = 0
            End If
            Dim SqlCommand As SqlCommand = New SqlCommand("Update CorintsublocationMaster set MapFieldName='" & txtMapFieldName_Edit.Text & "',ClientCoId='" & ddlClientName_Edit.SelectedValue.ToString() & "',DisplayID='" & ddlMisFieldName_Edit.SelectedValue.ToString() & "',Active=" & Active & ",ModifyBy = " & Session("loggedin_user") & ",modifyDate ='" & DateTime.Now() & "' where ID=" & lblMapfieldID.Text & "")
            SqlCommand.Connection = SqlConnection
            SqlCommand.Connection.Open()
            SqlCommand.ExecuteNonQuery()
            grv_MisValues.EditIndex = -1
            BindData()
            SqlConnection.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        
    End Sub

    Protected Sub grv_MisValues_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grv_MisValues.RowEditing
        grv_MisValues.EditIndex = e.NewEditIndex
        BindData()
    End Sub

    Protected Sub grv_MisValues_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles grv_MisValues.SelectedIndexChanging

    End Sub

    Protected Sub grv_MisValues_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grv_MisValues.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim ddlClientName_Edit As DropDownList = e.Row.FindControl("ddlClientName_Edit") 'CType(e.Row.FindControl("ddlCityName"), DropDownList)
                Dim ddlMisFieldName_Edit As DropDownList = e.Row.FindControl("ddlMisFieldName_Edit")
                Dim chkActive As CheckBox = e.Row.FindControl("chkActive_Edit")
                Dim lblClientID As Label = CType(e.Row.FindControl("lblClientID_Edit"), Label)
                Dim lblDisplayID As Label = CType(e.Row.FindControl("lblDisplayID_Edit"), Label)
                Dim lblChkActive As Label = CType(e.Row.FindControl("lblActive_Edit"), Label)


                Dim SqlConnection As SqlConnection

                Dim SqlDataAdapter As SqlDataAdapter = New SqlDataAdapter()
                Dim DataSet As DataSet = New DataSet()
                Dim DataSet_DisplayName As DataSet = New DataSet()
                SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("corConnectString"))
                SqlConnection.Open()
                Dim SqlCommand As SqlCommand = New SqlCommand("select ClientCoID,ClientCoName from CORIntClientCoMaster   where active=1 order by ClientCoName")
                SqlCommand.Connection = SqlConnection
                SqlDataAdapter = New SqlDataAdapter(SqlCommand)
                SqlDataAdapter.Fill(DataSet)

                If ddlClientName_Edit IsNot Nothing Then
                    ddlClientName_Edit.DataSource = DataSet
                    ddlClientName_Edit.DataTextField = "ClientCoName"
                    ddlClientName_Edit.DataValueField = "ClientCoID"
                    ddlClientName_Edit.DataBind()
                    ddlClientName_Edit.Items.FindByValue(lblClientID.Text).Selected = True
                End If

                SqlCommand = New SqlCommand("select id as MisFieldid, DisplayName from CorIntMapReportFields where Active=1")
                SqlCommand.Connection = SqlConnection
                SqlDataAdapter = New SqlDataAdapter(SqlCommand)
                SqlDataAdapter.Fill(DataSet_DisplayName)

                If ddlMisFieldName_Edit IsNot Nothing Then
                    ddlMisFieldName_Edit.DataSource = DataSet_DisplayName
                    ddlMisFieldName_Edit.DataTextField = "DisplayName"
                    ddlMisFieldName_Edit.DataValueField = "MisFieldid"
                    ddlMisFieldName_Edit.DataBind()
                    ddlMisFieldName_Edit.Items.FindByValue(lblDisplayID.Text).Selected = True
                End If

                If chkActive IsNot Nothing Then
                    If lblChkActive.Text = "True" Then
                        chkActive.Checked = True
                    Else
                        chkActive.Checked = False
                    End If
                End If

            End If

            If e.Row.RowType = DataControlRowType.Footer Then
                Dim SqlConnection As SqlConnection

                Dim SqlDataAdapter As SqlDataAdapter = New SqlDataAdapter()
                Dim DataSet As DataSet = New DataSet()
                SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("corConnectString"))
                SqlConnection.Open()
                Dim SqlCommand As SqlCommand = New SqlCommand("select ClientCoID,ClientCoName from CORIntClientCoMaster where active=1 order by ClientCoName")

                SqlCommand.Connection = SqlConnection
                SqlDataAdapter = New SqlDataAdapter(SqlCommand)
                SqlDataAdapter.Fill(DataSet)
                Dim ddlClientName_Footer As DropDownList = e.Row.FindControl("ddlClientName_Footer")
                If ddlClientName_Footer IsNot Nothing Then
                    ddlClientName_Footer.DataSource = DataSet
                    ddlClientName_Footer.DataTextField = "ClientCoName"
                    ddlClientName_Footer.DataValueField = "ClientCoID"
                    ddlClientName_Footer.DataBind()
                    ddlClientName_Footer.Items.Insert(0, "Select Client")
                End If

                SqlCommand = New SqlCommand("select id as MisFieldid,DisplayName from CorIntMapReportFields where Active=1 and ClientCoID=" & ddlClient.SelectedValue.ToString() & "")

                SqlCommand.Connection = SqlConnection
                Dim DataSetnew As DataSet = New DataSet()
                SqlDataAdapter = New SqlDataAdapter(SqlCommand)
                SqlDataAdapter.Fill(DataSetnew)
                Dim ddlMisFieldName_Footer As DropDownList = e.Row.FindControl("ddlMisFieldName_Footer")
                If ddlMisFieldName_Footer IsNot Nothing Then
                    ddlMisFieldName_Footer.DataSource = DataSetnew
                    ddlMisFieldName_Footer.DataTextField = "DisplayName"
                    ddlMisFieldName_Footer.DataValueField = "MisFieldid"
                    ddlMisFieldName_Footer.DataBind()
                    ddlMisFieldName_Footer.Items.Insert(0, "Select Mis field")
                End If

            End If
        Catch ex As Exception
            Response.Write(ex.Message())
        End Try
    End Sub

    Protected Sub grv_MisValues_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grv_MisValues.PageIndexChanging
        grv_MisValues.PageIndex = e.NewPageIndex
        BindData()
    End Sub

    Protected Sub AddMisFieldValues(ByVal sender As Object, ByVal e As EventArgs)
        Dim SqlConnection As SqlConnection
        Try
            Dim mapFieldName As String = DirectCast(grv_MisValues.FooterRow.FindControl("txtMapFieldName_Footer"), TextBox).Text
            Dim clientID As String = DirectCast(grv_MisValues.FooterRow.FindControl("ddlClientName_Footer"), DropDownList).SelectedValue.ToString()
            Dim DisplayID As String = DirectCast(grv_MisValues.FooterRow.FindControl("ddlMisFieldName_Footer"), DropDownList).SelectedValue.ToString()
            Dim chkActive As CheckBox = DirectCast(grv_MisValues.FooterRow.FindControl("chkActive_Footer"), CheckBox)
            Dim Active As Integer
            Active = 0
            If chkActive.Checked = True Then
                Active = 1
            Else
                Active = 0
            End If

            SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("corConnectString"))
            Dim cmd As New SqlCommand()
            cmd.CommandType = CommandType.Text
            cmd.CommandText = " insert into CorIntBrandMaster (MapFieldName,ClientCoId,CreatedBy ,CreateDate,DisplayID,Active) values(@mapFieldName, @ClientCoId, @CreatedBy,@CreateDate,@DisplayID,@Active)"
            cmd.Connection = SqlConnection

            cmd.Parameters.Add("@mapFieldName", SqlDbType.VarChar).Value = mapFieldName
            cmd.Parameters.Add("@ClientCoId", SqlDbType.Int).Value = Convert.ToInt32(clientID)
            cmd.Parameters.Add("@CreatedBy", SqlDbType.Int).Value = Session("loggedin_user")
            cmd.Parameters.Add("@CreateDate", SqlDbType.DateTime).Value = DateTime.Now
            cmd.Parameters.Add("@DisplayID", SqlDbType.Int).Value = Convert.ToInt32(DisplayID)
            cmd.Parameters.Add("@Active", SqlDbType.Int).Value = Active
            cmd.Connection.Open()
            Dim status As Integer = cmd.ExecuteNonQuery()
            If status = 1 Then
                lblErrorMsg.Text = "Sublocation inserted successfully."
            End If
            BindData()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

    End Sub
End Class
