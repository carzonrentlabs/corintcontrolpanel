Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class VendorChaufPkgsCityAddForm
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ddlcarcat As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlpkgkm As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtrate As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtExtraKM As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtRemarks As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkactive As System.Web.UI.WebControls.CheckBox

    Protected WithEvents btnsubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnreset As System.Web.UI.WebControls.Button
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel

    'Protected WithEvents chkairport As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkservice As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlcityname As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlpackagehr As System.Web.UI.WebControls.DropDownList
    Protected WithEvents chkoutstation As System.Web.UI.WebControls.CheckBox
    Protected WithEvents txtratehr As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtratekm As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtxtraHr As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtoutallowance As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtnighstay As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlHr As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlMin As System.Web.UI.WebControls.DropDownList

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btnsubmit.Attributes("onClick") = "return validation();"
        If Not Page.IsPostBack Then
            populateddl()
        End If
    End Sub

    Sub populateddl()
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility


        ddlcarcat.DataSource = objAcessdata.funcGetSQLDataReader("select CarCatName ,CarCatID  from CORIntCarCatMaster   where active=1 order by CarCatName")
        ddlcarcat.DataValueField = "CarCatID"
        ddlcarcat.DataTextField = "CarCatName"
        ddlcarcat.DataBind()
        ddlcarcat.Items.Insert(0, New ListItem("", ""))

        ddlcityname.DataSource = objAcessdata.funcGetSQLDataReader("select CityName  ,CityID   from CORIntCityMaster   where active=1 order by CityName")
        ddlcityname.DataValueField = "CityID"
        ddlcityname.DataTextField = "CityName"
        ddlcityname.DataBind()
        ddlcityname.Items.Insert(0, New ListItem("", ""))


        objAcessdata.funcpopulatenumddw(1000, 0, ddlpackagehr)
        objAcessdata.funcpopulatenumddw(5000, 0, ddlpkgkm)
        objAcessdata.Dispose()

    End Sub

    Private Sub btnsubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        Dim intuniqcheck As SqlParameter
        Dim intPkgidParam As SqlParameter
        Dim intuniqvalue As int32
        Dim intPkgid As int32

        Dim txtHr As String
        cmd = New SqlCommand("procAddVendorChaufCityPkgsMaster_1", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("@carcatid", ddlcarcat.SelectedItem.Value)
        'cmd.Parameters.Add("@apttransyn", get_YNvalue(chkairport))
        cmd.Parameters.Add("@service", chkservice.SelectedItem.Value)
        cmd.Parameters.Add("@cityid", ddlcityname.SelectedItem.Value)
        If Not ddlpackagehr.SelectedItem.Value = "" Then
            cmd.Parameters.Add("@pkghrs", ddlpackagehr.SelectedItem.Value)
        End If
        If Not ddlpkgkm.SelectedItem.Value = "" Then
            cmd.Parameters.Add("@pkgkms", ddlpkgkm.SelectedItem.Value)
        End If
        cmd.Parameters.Add("@outstationyn", get_YNvalue(chkoutstation))
        cmd.Parameters.Add("@pkgrate", txtrate.Text)
        If Not txtratehr.text = "" Then
            cmd.Parameters.Add("@extrahrrate", txtratehr.Text)
        End If
        If Not txtratekm.text = "" Then
            cmd.Parameters.Add("@extrakmrate", txtratekm.Text)
        End If
        txtHr = ddlHr.SelectedItem.Value + "." + ddlMin.SelectedItem.Value
        cmd.Parameters.Add("@thresholdextrahr", txtHr)
        If Not txtExtraKM.text = "" Then
            cmd.Parameters.Add("@thresholdextrakm", txtExtraKM.Text)
        End If
        If Not txtoutallowance.text = "" Then
            cmd.Parameters.Add("@outstationallowance", txtoutallowance.Text)
        End If
        If Not txtnighstay.text = "" Then
            cmd.Parameters.Add("@nightstayallowance", txtnighstay.Text)
        End If
        cmd.Parameters.Add("@remarks", txtRemarks.Text)
        cmd.Parameters.Add("@active", get_YNvalue(chkactive))

        cmd.Parameters.Add("@createdby", Session("loggedin_user"))

        intuniqcheck = cmd.Parameters.Add("@uniqcheckval", SqlDbType.Int)
        intuniqcheck.Direction = ParameterDirection.Output
        intPkgidParam = cmd.Parameters.Add("@Pkgid", SqlDbType.Int)
        intPkgidParam.Direction = ParameterDirection.Output

        MyConnection.Open()
        cmd.ExecuteNonQuery()
        intuniqvalue = cmd.Parameters("@uniqcheckval").Value
        intPkgid = cmd.Parameters("@Pkgid").Value
        MyConnection.Close()
        pnlmainform.Visible = False
        pnlconfirmation.Visible = True


        If Not intuniqvalue = 0 Then
            pnlmainform.Visible = False
            pnlconfirmation.Visible = True

            lblMessage.Text = "This particular package already exists!<br> The Package ID is <b>" & intPkgid & "</b>"
            hyplnkretry.Text = "Add another Vendor City Package"
            hyplnkretry.NavigateUrl = "VendorChaufPkgsCityAddForm.aspx"
        Else

            pnlmainform.Visible = False
            pnlconfirmation.Visible = True

            lblMessage.Text = "You have added the Chauffeur Package successfully <br> The Package ID is <b>" & intPkgid & "</b>"
            hyplnkretry.Text = "Add another Vendor City Package"
            hyplnkretry.NavigateUrl = "VendorChaufPkgsCityAddForm.aspx"
        End If
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function

End Class
