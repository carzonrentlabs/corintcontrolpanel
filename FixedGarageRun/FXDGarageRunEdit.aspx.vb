Imports commonutility
Imports System.Data
Imports System.text
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class FXDGarageRunEdit
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object
    Protected WithEvents lblCompID As System.Web.UI.WebControls.Label
    Protected WithEvents lblService As System.Web.UI.WebControls.Label
    Protected WithEvents lblCityID As System.Web.UI.WebControls.Label
    Protected WithEvents lblCarCatID As System.Web.UI.WebControls.Label
    Protected WithEvents Txt_Amt As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtEffectiveDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents chk_Active As System.Web.UI.WebControls.CheckBox
    Protected WithEvents btnsubmit As System.Web.UI.WebControls.Button
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink

    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel



    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        ' btnsubmit.Attributes("onClick") = "return validation();"
        If Trim("" & Request.QueryString("ID")) <> "" Then
            Dim intID As Integer
            intID = Request.QueryString("ID")
            If Not IsPostBack() Then
                getvalue(intID)
            End If
        Else
            Response.Write("Bad request.")
            Response.End()
        End If
    End Sub
    Sub getvalue(ByVal intID As Integer)
        Dim strquery As String
        'Dim StrUrl As StringBuilder
        Dim str As StringBuilder
        Dim dtrreader As SqlDataReader
        Dim accessdata As clsutility
        Dim createby As String = ""
        Dim ModifiedBy As String = ""

        accessdata = New clsutility

        strquery = "Exec Proc_CP_SelFXDGarageRun '" & intID & "',2"

        dtrreader = accessdata.funcGetSQLDataReader(strquery.ToString)
        'If dtrreader.HasRows Then
        While dtrreader.Read
            lblCompID.Text = dtrreader("ClientCoName")
            lblService.Text = dtrreader("Service")

            If Trim("" & dtrreader("CarCatName")) <> "" Then
                lblCarCatID.text = dtrreader("CarCatName")
            Else
                lblCarCatID.text = "Any"
            End If


            If IsDBNull(dtrreader("CityName")) Then
                lblCityID.Text = "Any"
            Else
                lblCityID.Text = dtrreader("CityName")
            End If

            Txt_Amt.Text = dtrreader("Amount")
            If dtrreader("Active") Then
                chk_Active.Checked = True
            Else
                chk_Active.Checked = False
            End If

            'txtEffectiveDate.Text = dtrreader("EffectiveDateFrom")
        End While
        'End If
    End Sub

    Private Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        Dim Flag As Integer
        Dim intAmount As Integer

        intAmount = Txt_Amt.Text
        'Response.Write("Txt_Amount==" & intAmount)

        cmd = New SqlCommand("Proc_CP_EditFXDGarageRun", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@ID", Request.QueryString("ID"))
        cmd.Parameters.AddWithValue("@Amount", intAmount)
        cmd.Parameters.AddWithValue("@Active", get_YNvalue(chk_Active))
        cmd.Parameters.AddWithValue("@CreatedBy", Session("loggedin_user"))
        'cmd.Parameters.AddWithValue("@EffectiveToDate", Convert.ToDateTime(txtEffectiveDate.Text.ToString()))

        MyConnection.Open()
        Flag = cmd.ExecuteNonQuery()

        MyConnection.Close()
        lblErrorMsg.Visible = True
        'Response.Write("Flag=" & Flag)

        If Trim("" & Flag) = "0" Then

            pnlmainform.Visible = False
            pnlconfirmation.Visible = True

            lblErrorMsg.Text = "Unable to edit Fixed Garage Run Package!<br> The Fixed Garage Run Package ID is <b>" & Request.QueryString("id") & "</b>"
            hyplnkretry.Text = "Edit another Fixed Garage Run Package"
            hyplnkretry.NavigateUrl = "FXDGarageRunSearch.aspx"
        Else

            pnlmainform.Visible = False
            pnlconfirmation.Visible = True

            lblErrorMsg.Text = "You have edited the Fixed Garage Run Package <br> The Fixed Garage Run Package ID is <b>" & Request.QueryString("id") & "</b>"
            hyplnkretry.Text = "Edit another Fixed Garage Run Package"
            hyplnkretry.NavigateUrl = "FXDGarageRunSearch.aspx"
        End If
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function
End Class
