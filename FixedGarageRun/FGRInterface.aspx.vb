Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Imports System.Data.Sql
Imports System.Data.OleDb
Imports System.IO
Partial Class FixedGarageRun_FGRInterface
    Inherits System.Web.UI.Page

    Protected Sub btn_UploadFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_UploadFile.Click
        Dim strFile As String
        Dim filename As String
        Dim filepath As String
        Dim Dt As DataTable = New DataTable()
        Dim DtExistsFgrPkg As DataTable = New DataTable()
        Dim Adap As SqlDataAdapter
        'Dim AdapExistsFgrPkg As SqlDataAdapter
        'filepath = "F:\CorInt\FGRExcelUpload\"
        filepath = "D:\CorInt\FGRExcelUpload\"

        Dim di As DirectoryInfo = New DirectoryInfo(filepath)
        If di.Exists Then
            'MsgBox("That path exists already.")
        Else
            di.Create()
        End If
        Dim Newfilename As String

        If UploadFGR.PostedFile.FileName <> "" And UploadFGR.PostedFile.ContentLength > 0 Then
            strFile = Path.GetFileName(UploadFGR.FileName)
            'End If

            Newfilename = System.DateTime.Now.Year.ToString() + "_" + System.DateTime.Now.Month.ToString() + "_" + System.DateTime.Now.Day.ToString() + "_" + System.DateTime.Now.Hour.ToString() + "_" + System.DateTime.Now.Minute.ToString() + "_" + System.DateTime.Now.Second.ToString() + ".xlsx"



            filename = Path.GetFileName(UploadFGR.FileName)
            If File.Exists(filepath + filename) Then
                File.Delete(filepath + filename)
            End If

            If strFile <> "" Then

                If filename = "FGRPackage.xlsx" Then

                    'UploadFGR.SaveAs("C:\MyDir\" + filename)
                    UploadFGR.SaveAs(filepath + Newfilename)

                    'End If
                    Dim connStringExcel As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filepath + Newfilename + ";Extended Properties=""Excel 12.0;HDR=YES;"""
                    Dim excelConn As New OleDbConnection(connStringExcel)
                    Dim excelCmd As New OleDbCommand("Select * From [Sheet1$]", excelConn)
                    'Dim sqlCommand1 As SqlCommand
                    'Dim sqlCommand2 As SqlCommand
                    'Dim sqlCommand3 As SqlCommand
                    Try
                        excelConn.Open()
                        Dim excelReader As OleDbDataReader = excelCmd.ExecuteReader()
                        Dim Pkglist As New ArrayList
                        Dim Citylist As New ArrayList
                        Dim CarCatlist As New ArrayList
                        Dim intPkgidParam As SqlParameter

                        Dim MyConnection As SqlConnection
                        MyConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("corConnectString"))
                        Try
                            MyConnection.Open()
                            Dim num As Integer
                            While (excelReader.Read())
                                If IsNothing(excelReader("CarCatName")) Or IsDBNull(excelReader("CarCatName")) Then
                                Else
                                    Dim Str As New StringBuilder
                                    Dim cmd As SqlCommand

                                    cmd = New SqlCommand("ExcelFGRUpload", MyConnection)
                                    cmd.CommandType = CommandType.StoredProcedure
                                    cmd.CommandTimeout = 6000
                                    cmd.Parameters.AddWithValue("@CityName", excelReader("CityName").ToString())
                                    cmd.Parameters.AddWithValue("@Service", excelReader("service").ToString())
                                    cmd.Parameters.AddWithValue("@ClientCoID", Convert.ToInt32(ddlCompany.SelectedValue))
                                    cmd.Parameters.AddWithValue("@Amount", Convert.ToDecimal(excelReader("Amount")))
                                    cmd.Parameters.AddWithValue("@CarCatName", excelReader("CarCatName").ToString())
                                    cmd.Parameters.AddWithValue("@UserID", Convert.ToInt32(Session("loggedin_user")))
                                    cmd.Parameters.AddWithValue("@EffectiveToDate", Convert.ToDateTime(txtEffectiveDate.Text.ToString()))
                                    intPkgidParam = cmd.Parameters.Add("@ID", SqlDbType.Int)
                                    intPkgidParam.Direction = ParameterDirection.Output
                                    cmd.ExecuteNonQuery()
                                    'Dim Str1 As New StringBuilder

                                    'Str1 = Str1.Append("select ID as pkgid from CorIntFixedGarageRun Where active=1 and CityID = ")
                                    'Str1 = Str1.Append("(select CityID from corintcitymaster where active=1 and Cityname='" + excelReader("CityName") + "')")
                                    'Str1 = Str1.Append("and Service = '" + excelReader("service").ToString() + "' and CompanyID = " + Convert.ToString(ddlCompany.SelectedValue.ToString()) + " ")
                                    'Str1 = Str1.Append(" AND CarCategoryID = (select carcatId from CORIntCarCatMaster where carcatname='" + excelReader("CarCatName") + "' and active=1)")
                                    'sqlCommand2 = New SqlCommand(Str1.ToString(), MyConnection)
                                    'AdapExistsFgrPkg = New SqlDataAdapter(sqlCommand2)
                                    'AdapExistsFgrPkg.Fill(DtExistsFgrPkg)
                                    'If DtExistsFgrPkg.Rows.Count = 0 Then
                                    'Dim Str As New StringBuilder
                                    'Str = Str.Append("insert into CorIntFixedGarageRun(CityID,Service,CompanyID,Amount,Active ,CreatedBy,CreateDate,CarCategoryID)")
                                    'Str = Str.Append("values((select CityID from corintcitymaster where Cityname='" + excelReader("CityName") + "'  and active=1),")
                                    'Str = Str.Append("'" + excelReader("service").ToString() + "'," + Convert.ToString(ddlCompany.SelectedValue.ToString()) + "," + excelReader("Amount").ToString() + "")
                                    'Str = Str.Append("," + Convert.ToString(excelReader("Active")) + "," + Convert.ToString(Session("loggedin_user")) + ", Getdate()" + "")
                                    'Str = Str.Append(",(select carcatId from CORIntCarCatMaster where carcatname='" + excelReader("CarCatName") + "' and active=1))")


                                    'sqlCommand1 = New SqlCommand(Str.ToString(), MyConnection)
                                    'sqlCommand1.ExecuteNonQuery()
                                    'sqlCommand3 = New SqlCommand("select max(ID)as pkgid from CorIntFixedGarageRun", MyConnection)
                                    'Adap = New SqlDataAdapter(sqlCommand3)
                                    'Adap.Fill(Dt)
                                    'End If
                                    If (lblMessage.Text.Trim() = "") Then
                                        If (Convert.ToInt32(cmd.Parameters("@ID").Value) = -2) Then
                                            lblMessage.Text = "Incorrect City Name,"
                                        ElseIf (Convert.ToInt32(cmd.Parameters("@ID").Value) = -3) Then
                                            lblMessage.Text = "Incorrect Car Category Name,"
                                        Else
                                            lblMessage.Text = "FGR Pkg Id's =" + Convert.ToString(cmd.Parameters("@ID").Value) + ","
                                        End If
                                    Else
                                        If (Convert.ToInt32(cmd.Parameters("@ID").Value) = -2) Then
                                            lblMessage.Text += "Incorrect City Name,"
                                        ElseIf (Convert.ToInt32(cmd.Parameters("@ID").Value) = -3) Then
                                            lblMessage.Text += "Incorrect Car Category Name,"
                                        Else
                                            lblMessage.Text += Convert.ToString(cmd.Parameters("@ID").Value) + ","
                                        End If
                                    End If
                                End If
                            End While
                            excelConn.Close()
                            excelConn.Dispose()
                            'lblMessage.Text = "FGR Package Id's ="
                            'For num = 0 To Dt.Rows.Count - 1
                            'lblMessage.Text += Convert.ToString(Dt.Rows(num)("pkgid")) + ","
                            'Next
                            'If DtExistsFgrPkg.Rows.Count > 0 Then
                            '    lblMessage.Text += "Already Inserted"
                            '    For num = 0 To DtExistsFgrPkg.Rows.Count - 1
                            '    lblMessage.Text += Convert.ToString(DtExistsFgrPkg.Rows(num)("pkgid")) + ","
                            '    Next
                            'End If

                            'If File.Exists("C:\MyDir\" + filename) Then
                            'File.Delete("C:\MyDir\" + filename)
                            'End If

                        Catch exs As Exception
                            lblMessage.Text = exs.Message
                            'If File.Exists("C:\MyDir\" + filename) Then
                            'File.Delete("C:\MyDir\" + filename)
                            'End If
                        Finally
                            MyConnection.Close()
                        End Try
                    Catch exo As Exception
                        lblMessage.Text = exo.Message
                        'If File.Exists("C:\MyDir\" + filename) Then
                        'File.Delete("C:\MyDir\" + filename)
                        'End If
                    Finally

                    End Try
                Else
                    lblMessage.Text = "Please select package file!"
                End If
            End If

        Else
            lblMessage.Text = "Please Select File!"
        End If

    End Sub

    Protected Sub btn_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Reset.Click

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        btn_UploadFile.Attributes("onClick") = "return validation();"
        If Not Page.IsPostBack Then
            Dim dtrreader As SqlDataReader
            Dim accessdata As clsutility
            accessdata = New clsutility
            dtrreader = accessdata.funcGetSQLDataReader("select ClientCoID ,ClientCoName  from dbo.CORIntClientCoMaster where TariffType='S' and active=1 order by ClientCoName")
            ddlCompany.DataSource = dtrreader
            ddlCompany.DataValueField = "ClientCoID"
            ddlCompany.DataTextField = "ClientCoName"
            ddlCompany.DataBind()
            ddlCompany.Items.Insert(0, New ListItem("--Select--", 0))
            dtrreader.Close()
        End If
    End Sub
End Class