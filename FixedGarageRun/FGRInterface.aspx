<%@ Page Language="VB" AutoEventWireup="false" CodeFile="FGRInterface.aspx.vb" Inherits="FixedGarageRun_FGRInterface" %>

<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Fixed Garage Run Package</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <script language="JavaScript" src="../utilityfunction.js" type="text/jscript"></script>
    <link href="../HertzInt.css" type="text/css" rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <link rel="stylesheet" href="/resources/demos/style.css">

    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            $("#<%=txtEffectiveDate.ClientID %>").datepicker();
        });

        function validation() {
            if (document.forms[0].ddlCompany.value == "0") {
                alert("Please Select Company Name.")
                document.forms[0].ddlCompany.focus();
                return false;
            }
            else {
                return true;
            }
        }
    </script>
</head>
<body ms_positioning="GridLayout">
    <form id="form1" runat="server">
        <uc1:Headerctrl ID="Headerctrl1" runat="server"></uc1:Headerctrl>
        <div>
            <table align="center">
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <strong><u>Fixed Garage Run Package</u></strong>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:Label ID="lblMessage" runat="server" ForeColor="red"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>* Company</td>
                    <td>
                        <asp:DropDownList ID="ddlCompany" runat="server" CssClass="input">
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3">Package Effective Date &nbsp;&nbsp;
                    <asp:TextBox ID="txtEffectiveDate" runat="server" CssClass="input"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:FileUpload ID="UploadFGR" runat="server" />&nbsp;
                        <asp:Button ID="btn_UploadFile" runat="server" Text="Upload FGR Rate" />&nbsp;
                        <asp:Button ID="btn_Reset" runat="server" Text="Reset" /></td>
                </tr>
                <tr>
                    <td colspan="2" align="left">
                        <b><u>Below is sample Excel File Format and file name should be (FGRPackage.xlsx) and sheet name should be (Sheet1)</u></b>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" align="center" style="width: 100%">
                        <table border="1">
                            <tr style="background-color: Yellow;">
                                <td>CityName</td>
                                <td>service</td>
                                <td>Amount</td>
                                <td>CarCatName</td>
                            </tr>
                            <tr>
                                <td>Delhi</td>
                                <td>C</td>
                                <td>800</td>
                                <td>Economy</td>
                            </tr>
                            <tr>
                                <td>Delhi</td>
                                <td>OST</td>
                                <td>1200</td>
                                <td>Economy</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
