Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class FXDGarageRunAdd
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ddlCompID As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlService As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlCarCatID As System.Web.UI.WebControls.DropDownList

    Protected WithEvents ddlCityID As System.Web.UI.WebControls.DropDownList
    Protected WithEvents TxtAmount As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtEffectiveDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnsubmit As System.Web.UI.WebControls.Button
    Protected WithEvents chkActive As System.Web.UI.WebControls.CheckBox
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink

    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        btnsubmit.Attributes("onClick") = "return validation();"
        If Not Page.IsPostBack() Then
            populateddl()
        End If

    End Sub
    Sub populateddl()
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility
        ddlCityID.DataSource = objAcessdata.funcGetSQLDataReader("select cityid,cityname from corintcitymaster where active=1 order by cityname ")
        'ddlCityID.DataSource = dtrreader
        
        ddlCityID.DataValueField = "cityid"
        ddlCityID.DataTextField = "cityname"
        ddlCityID.DataBind()
        ddlCityID.Items.Insert(0, New ListItem("Any", 0))
        'dtrreader.Close()

        objAcessdata = New clsutility
        ddlCompID.DataSource = objAcessdata.funcGetSQLDataReader("select ClientCoName  ,ClientCoID   from CORIntClientCoMaster    where active=1 order by ClientCoName ")
        ddlCompID.DataValueField = "ClientCoID"
        ddlCompID.DataTextField = "ClientCoName"
        ddlCompID.DataBind()

        'Dim dtrreader As SqlDataReader
        'Dim accessdata As clsutility

        objAcessdata = New clsutility
        ddlCarCatID.DataSource = objAcessdata.funcGetSQLDataReader("Select CarcatID, CarCatName from CORIntCarCatMaster Where Active = 1 order by CarCatName")
        ddlCarCatID.DataValueField = "CarcatID"
        ddlCarCatID.DataTextField = "CarCatName"
        ddlCarCatID.DataBind()
        ddlCarCatID.Items.Insert(0, New ListItem("Any", 0))


    End Sub

    Private Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        Dim intFlagCheck As SqlParameter
        Dim intFlag As Int32

        Dim intFGRParam As SqlParameter
        Dim intFGRid As Int32
        Dim txtHr As String

        cmd = New SqlCommand("Proc_CP_AddFxdGarageRun", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.AddWithValue("@CityID", ddlCityID.SelectedValue)
        cmd.Parameters.AddWithValue("@Service", ddlService.SelectedValue)
        cmd.Parameters.AddWithValue("@CompanyID", ddlCompID.SelectedValue)
        cmd.Parameters.AddWithValue("@Amount", TxtAmount.Text)
        cmd.Parameters.AddWithValue("@Active", get_YNvalue(chkActive))
        cmd.Parameters.AddWithValue("@CreatedBy", Session("loggedin_user"))
        cmd.Parameters.AddWithValue("@CarCatID", ddlCarCatID.SelectedValue)
        cmd.Parameters.AddWithValue("@EffectiveToDate", Convert.ToDateTime(txtEffectiveDate.Text.ToString()))

        intFlagCheck = cmd.Parameters.AddWithValue("@StatusFlag", SqlDbType.Int)
        intFlagCheck.Direction = ParameterDirection.Output

        intFGRParam = cmd.Parameters.Add("@ID", SqlDbType.Int)
        intFGRParam.Direction = ParameterDirection.Output

        MyConnection.Open()
        cmd.ExecuteNonQuery()
        intFlag = cmd.Parameters("@StatusFlag").Value
        intFGRid = cmd.Parameters("@ID").Value

        MyConnection.Close()
        lblErrorMsg.Visible = True

        If Trim("" & intFlag) = "2" Then

            pnlmainform.Visible = False
            pnlconfirmation.Visible = True

            lblErrorMsg.Text = "This Fixed Garage Run Package already exists!<br> The Fixed Garage Run Package ID is <b>" & intFGRid & "</b>"
            hyplnkretry.Text = "Add another Fixed Garage Run Package"
            hyplnkretry.NavigateUrl = "FXDGarageRunAdd.aspx"
        Else

            pnlmainform.Visible = False
            pnlconfirmation.Visible = True

            lblErrorMsg.Text = "You have added the Fixed Garage Run Package <br> The Fixed Garage Run Package ID is <b>" & intFGRid & "</b>"
            hyplnkretry.Text = "Add another Fixed Garage Run Package"
            hyplnkretry.NavigateUrl = "FXDGarageRunAdd.aspx"
        End If
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function
End Class
