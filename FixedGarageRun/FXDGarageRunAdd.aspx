<%@ Page Language="vb" AutoEventWireup="false" Src="FXDGarageRunAdd.aspx.vb" CodeBehind="FXDGarageRunAdd.aspx.vb" Inherits="FXDGarageRunAdd" %>

<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Fixed Garage Run Package</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <script language="JavaScript" src="../utilityfunction.js"></script>
    <link href="../HertzInt.css" type="text/css" rel="stylesheet">
    <script language="javascript">
        function validation() {
            //alert(isNaN(document.forms[0].TxtAmount.value));
            if (isNaN(document.forms[0].TxtAmount.value)) {
                //alert("Rate should be numeric and upto two decimal digits only.")
                alert("Rate should be numeric only.")
                return false;
            }
                /*else
					{
					alert(parseFloat(document.forms[0].TxtAmount.value.indexOf(".")));
					if (parseFloat(document.forms[0].TxtAmount.value.indexOf(".")) > 0)
						{
							var FlotVal
							FlotVal=document.forms[0].TxtAmount.value.substr(parseFloat(document.forms[0].TxtAmount.value.indexOf(".")),document.forms[0].TxtAmount.value.length)
							alert(FlotVal)
							if(parseFloat((FlotVal.length)-1)>2)
							{
							alert("2. Rate should be numeric and upto two decimal digits only")
							return false;
							}
						}
					alert("3. Rate should be numeric and upto two decimal digits only.")
					return false;	
					}*/
            else {
                return true;
            }
        }
    </script>
</head>
<body ms_positioning="GridLayout">
    <form id="Form1" method="post" runat="server">
        <uc1:Headerctrl ID="Headerctrl1" runat="server"></uc1:Headerctrl>
        <table id="Table1" align="center">
            <asp:Panel ID="pnlmainform" runat="server">
                <tbody>
                    <tr>
                        <td align="center" colspan="2"><strong><u>Add a Fixed Garage Run Package</u></strong>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">&nbsp;&nbsp;</td>
                    </tr>
                    <tr>
                        <td>* City
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlCityID" runat="server" CssClass="input"></asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td>* Service</td>
                        <td>
                            <asp:DropDownList ID="ddlService" runat="server" CssClass="input">
                                <asp:ListItem Value="A">Airport</asp:ListItem>
                                <asp:ListItem Value="C">Chauffeur Drive</asp:ListItem>
                                <asp:ListItem Value="CT">City Transfer</asp:ListItem>
                                <asp:ListItem Value="H">Hotel</asp:ListItem>
                                <asp:ListItem Value="L">Local</asp:ListItem>
                                <asp:ListItem Value="OST">Outstation</asp:ListItem>
                                <asp:ListItem Value="S">Self-drive</asp:ListItem>
                            </asp:DropDownList></td>
                    </tr>

                    <tr>
                        <td>* Company</td>
                        <td>
                            <asp:DropDownList ID="ddlCompID" runat="server" CssClass="input"></asp:DropDownList></td>
                    </tr>

                    <tr>
                        <td>* Car Category</td>
                        <td>
                            <asp:DropDownList ID="ddlCarCatID" runat="server" CssClass="input"></asp:DropDownList></td>
                    </tr>

                    <tr>
                        <td>* Amount
                        </td>
                        <td>
                            <asp:TextBox ID="TxtAmount" runat="server" CssClass="input" MaxLength="50"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>Effective Date </td>
                        <td>
                            <asp:TextBox ID="txtEffectiveDate" runat="server" CssClass="input"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Active</td>
                        <td>
                            <asp:CheckBox ID="chkactive" runat="server" CssClass="input" Checked="True"></asp:CheckBox></td>
                    </tr>


                    <tr>
                        <td align="center" colspan="2">
                            <asp:Button ID="btnsubmit" runat="server" CssClass="button" Text="Submit"></asp:Button>&nbsp;&nbsp;
								<asp:Button ID="btnreset" runat="server" CssClass="button" Text="Reset" CausesValidation="False"></asp:Button></td>
                    </tr>
            </asp:Panel>
            <asp:Panel ID="pnlconfirmation" runat="server" Visible="False">
                <tr align="center">
                    <td colspan="2">
                        <asp:Label ID="lblErrorMsg" runat="server"></asp:Label></td>
                </tr>
                <tr align="center">
                    <td colspan="2">
                        <asp:HyperLink ID="hyplnkretry" runat="server"></asp:HyperLink></td>
                </tr>

            </asp:Panel>
            </TBODY>
        </table>

    </form>
</body>
</html>
