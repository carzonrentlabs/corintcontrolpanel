Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System

Public Class CarCompEditSearch
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnReset As System.Web.UI.WebControls.Button
    Protected WithEvents ddcompName As System.Web.UI.WebControls.DropDownList

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not Page.IsPostBack Then
            Dim dtrreader As SqlDataReader
            Dim accessdata As clsutility
            accessdata = New clsutility
            If Session("provider_Id") <> 1 Then
                dtrreader = accessdata.funcGetSQLDataReader("select carcompid,carcompname from CORIntCarCompMaster where carcompname <>'(Radio Taxi)' order by carcompname")
            Else
                dtrreader = accessdata.funcGetSQLDataReader("select carcompid,carcompname from CORIntCarCompMaster order by carcompname")
            End If
            ddcompName.DataSource = dtrreader
            ddcompName.DataValueField = "carcompid"
            ddcompName.DataTextField = "carcompname"
            ddcompName.DataBind()
            ddcompName.Items.Insert(0, New ListItem("Any", -1))
            dtrreader.Close()
            accessdata.Dispose()

        End If
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Response.Redirect("CarCompEditResult.aspx?id=" & ddcompName.SelectedItem.Value)
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("carcompeditsearch.aspx")
    End Sub
End Class
