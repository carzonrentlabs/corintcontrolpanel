Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class SubCategoryEditSearch
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ddCategoryName As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddSubCategoryName As System.Web.UI.WebControls.DropDownList
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnReset As System.Web.UI.WebControls.Button

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btnSubmit.Attributes("onClick") = "return validation();"
        If Not Page.IsPostBack Then
            Dim dtrreader As SqlDataReader
            Dim accessdata As clsutility
            accessdata = New clsutility
            dtrreader = accessdata.funcGetSQLDataReader("select CategoryID ,CategoryName from CORIntCategory  order by CategoryName")
            ddCategoryName.DataSource = dtrreader
            ddCategoryName.DataValueField = "CategoryID"
            ddCategoryName.DataTextField = "CategoryName"
            ddCategoryName.DataBind()
            ddCategoryName.Items.Insert(0, New ListItem("-Select Category-", "0"))
            dtrreader.Close()
        End If
    End Sub
    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Response.Redirect("SubCategoryResult.aspx?cid=" & ddCategoryName.SelectedItem.Value & "&scid=" & ddSubCategoryName.SelectedItem.Value)
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("SubCategoryEditSearch.aspx")
    End Sub

    Private Sub ddCategoryName_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddCategoryName.SelectedIndexChanged
        Dim dtrreader As SqlDataReader
        Dim accessdata As clsutility
        accessdata = New clsutility
        dtrreader = accessdata.funcGetSQLDataReader("select SubCatID ,SubCategoryName from CORIntSubCategory where CategoryID = " + ddCategoryName.SelectedValue + " order by SubCategoryName")
        ddSubCategoryName.DataSource = dtrreader
        ddSubCategoryName.DataValueField = "SubCatID"
        ddSubCategoryName.DataTextField = "SubCategoryName"
        ddSubCategoryName.DataBind()
        ddSubCategoryName.Items.Insert(0, New ListItem("Any", "-1"))
        dtrreader.Close()
    End Sub
End Class
