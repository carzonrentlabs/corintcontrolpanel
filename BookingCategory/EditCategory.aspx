<%@ Page Language="vb" AutoEventWireup="false" src="EditCategory.aspx.vb" Inherits="EditCategory"%>
<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CarzonRent :: Internal software</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="JavaScript" src="../utilityfunction.js"></script>
		<script language="JavaScript" src="../JScripts/Datefunc.js"></script>
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
		<script language="JavaScript">
		function validation()
		{
			if(document.forms[0].txtCategoryName.value=="")
			{
				alert("Enter a category name.");
				return false;
			}
		}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<uc1:headerctrl id="Headerctrl1" runat="server"></uc1:headerctrl>
			<table align="center" style="WIDTH: 410px; HEIGHT: 112px">
				&nbsp;
				<asp:panel id="pnlmainform" Runat="server">
					<TBODY>
						<TR>
							<TD align="center" colSpan="2"><B><U>Add a Category</U></B></TD>
						</TR>
						<TR>
							<TD align="center" colSpan="2">
								<asp:Label id="lblErrorMsg" runat="server" visible="false" cssclass="subRedHead"></asp:Label></TD>
						</TR>
						<TR>
							<TD style="WIDTH: 180px; HEIGHT: 36px">*Category Name:</TD>
							<TD style="HEIGHT: 36px">
								<asp:TextBox id="txtCategoryName" runat="server" MaxLength="50" Width="152px"></asp:TextBox></TD>
						</TR>
						<TR>
							<TD style="WIDTH: 180px; HEIGHT: 36px">Active:</TD>
							<TD style="HEIGHT: 36px">
								<asp:checkbox id="chkActive" runat="server" Width="96px"></asp:checkbox></TD>
						</TR>
						<TR>
							<TD align="center" colSpan="2">
								<asp:Label id="hidCategoryId" runat="server" Visible="False"></asp:Label>
								<asp:button id="btnSubmit" runat="server" CssClass="input" Text="Submit"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;
								<asp:button id="btnReset" runat="server" CssClass="input" Text="Reset"></asp:button></TD>
						</TR>
				</asp:panel>
				<asp:panel id="pnlconfirmation" Runat="server" Visible="False">
					<TR>
						<TD style="HEIGHT: 28px" align="center" colSpan="2">&nbsp;&nbsp;&nbsp;
							<asp:Label id="lblMessage" runat="server"></asp:Label></TD>
					</TR>
					<TR>
						<TD align="center" colSpan="2">
							<asp:HyperLink id="hyplnkretry" runat="server"></asp:HyperLink></TD>
					</TR>
				</asp:panel></TBODY>
			</table>
		</form>
	</body>
</HTML>
