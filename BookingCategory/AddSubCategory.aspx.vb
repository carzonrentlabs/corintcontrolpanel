Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Mail
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class AddSubCategory
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnReset As System.Web.UI.WebControls.Button
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    Protected WithEvents ddlCategory As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtSubCategoryName As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtDescription As System.Web.UI.WebControls.TextBox
    Protected WithEvents RadioButton1 As System.Web.UI.WebControls.RadioButton
    Protected WithEvents RadioButton2 As System.Web.UI.WebControls.RadioButton
    Protected WithEvents RadioButton3 As System.Web.UI.WebControls.RadioButton
    Protected WithEvents rdStatus1 As System.Web.UI.WebControls.RadioButton
    Protected WithEvents rdStatus2 As System.Web.UI.WebControls.RadioButton
    Protected WithEvents rdStatus3 As System.Web.UI.WebControls.RadioButton

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btnSubmit.Attributes("onClick") = "return validation();"
        If Not Page.IsPostBack Then
            populateddl()
        End If
    End Sub
    Sub populateddl()
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility

        ddlCategory.DataSource = objAcessdata.funcGetSQLDataReader("select categoryid,categoryname from CORIntcategory  where active=1 order by CategoryName")
        ddlCategory.DataValueField = "categoryid"
        ddlCategory.DataTextField = "categoryname"
        ddlCategory.DataBind()
        ddlCategory.Items.Insert(0, New ListItem("-select category-", "0"))
        objAcessdata.Dispose()
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        Dim intuniqcheck As SqlParameter
        Dim intuniqvalue As Int32
        'execueting the stored procedure for checking unique category and for inserting category.
        cmd = New SqlCommand("procAddSubCategory", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@categoryid", Trim(ddlCategory.SelectedValue))
        cmd.Parameters.Add("@subCategoryName", Trim(txtSubCategoryName.Text))
        cmd.Parameters.Add("@description", Trim(txtDescription.Text))
        If rdStatus1.checked Then
            cmd.Parameters.Add("@status", rdStatus1.Text)
        ElseIf rdStatus2.checked Then
            cmd.Parameters.Add("@status", rdStatus2.Text)
        Else
            cmd.Parameters.Add("@status", rdStatus3.Text)
        End If
        intuniqcheck = cmd.Parameters.Add("@uniqcheckval", SqlDbType.Int)
        intuniqcheck.Direction = ParameterDirection.Output
        MyConnection.Open()
        cmd.ExecuteNonQuery()
        'getting the value to know that whethre the category is unique or not
        intuniqvalue = cmd.Parameters("@uniqcheckval").Value
        MyConnection.Close()

        If Not intuniqvalue = 0 Then
            lblErrorMsg.Visible = True
            lblErrorMsg.Text = "SubCategory already exist."
            Exit Sub
        Else
            lblMessage.Text = "You have added the Sub Category successfully"
            hyplnkretry.Text = "Add another Sub Category"
            hyplnkretry.NavigateUrl = "AddSubCategory.aspx"
            pnlmainform.Visible = False
            pnlconfirmation.Visible = True
        End If
    End Sub
End Class
