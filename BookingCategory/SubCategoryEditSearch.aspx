<%@ Page Language="vb" AutoEventWireup="false" src="SubCategoryEditSearch.aspx.vb" Inherits="SubCategoryEditSearch"%>
<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CarzonRent :: Internal software Control panel</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
		<script language="JavaScript">
		function validation()
		{
			if(document.forms[0].ddCategoryName.value=="0")
			{
				alert("Select a category name.");
				return false;
			}		
			if(document.forms[0].ddSubCategoryName.value=="0")
			{
				alert("Select a subcategory name.");
				return false;
			}
		}
		</script>		
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<uc1:Headerctrl id="Headerctrl1" runat="server"></uc1:Headerctrl>
			<br>
			<br>
			<TABLE align="center">
				<TR>
					<TD colspan="2" align="center"><STRONG><U>Edit a SubCategory where</U></STRONG>
						<br>
						<br>
					</TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 31px">Category Name</TD>
					<TD style="HEIGHT: 31px">
						<asp:DropDownList id="ddCategoryName" runat="server" CssClass="input" AutoPostBack="True"></asp:DropDownList></TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 16px">SubCategory Name</TD>
					<TD style="HEIGHT: 16px">
						<asp:DropDownList id="ddSubCategoryName" runat="server" CssClass="input"></asp:DropDownList></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD colspan="2" align="center">
						<asp:Button id="btnSubmit" runat="server" Text="Submit" CssClass="button"></asp:Button>
						<asp:Button id="btnReset" runat="server" Text="Reset" CssClass="button"></asp:Button></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
