Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class BookingCategory
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    Protected WithEvents chkActive As System.Web.UI.WebControls.CheckBox
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnReset As System.Web.UI.WebControls.Button
    Protected WithEvents txtCategoryName As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btnSubmit.Attributes("onClick") = "return validation();"
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        Dim intuniqcheck As SqlParameter
        Dim intuniqvalue As Int32
        'execueting the stored procedure for checking unique category and for inserting category.
        cmd = New SqlCommand("procAddCategory", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@categoryname", Trim(txtCategoryName.Text))
        cmd.Parameters.Add("@active", get_YNvalue(chkActive))
        intuniqcheck = cmd.Parameters.Add("@uniqcheckval", SqlDbType.Int)
        intuniqcheck.Direction = ParameterDirection.Output
        MyConnection.Open()
        cmd.ExecuteNonQuery()
        'getting the value to know that whethre the category is unique or not
        intuniqvalue = cmd.Parameters("@uniqcheckval").Value
        MyConnection.Close()

        If Not intuniqvalue = 0 Then
            lblErrorMsg.Visible = True
            lblErrorMsg.Text = "Category already exist."
            Exit Sub
        Else
            lblMessage.Text = "You have added the Category successfully"
            hyplnkretry.Text = "Add another Category"
            hyplnkretry.NavigateUrl = "AddCategory.aspx"
            pnlmainform.Visible = False
            pnlconfirmation.Visible = True
        End If
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function
End Class