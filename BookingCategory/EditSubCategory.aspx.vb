Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Mail
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class EditSubCategory
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    Protected WithEvents ddlCategory As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtSubCategoryName As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtDescription As System.Web.UI.WebControls.TextBox
    Protected WithEvents rdStatus1 As System.Web.UI.WebControls.RadioButton
    Protected WithEvents rdStatus2 As System.Web.UI.WebControls.RadioButton
    Protected WithEvents rdStatus3 As System.Web.UI.WebControls.RadioButton
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnReset As System.Web.UI.WebControls.Button
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    Protected WithEvents hidSubCategoryId As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here

        btnSubmit.Attributes("onClick") = "return validation();"

        If Not Page.IsPostBack Then
            populateddl()
            Dim dtrreader As SqlDataReader
            Dim accessdata As clsutility
            accessdata = New clsutility
            dtrreader = accessdata.funcGetSQLDataReader("select CategoryID, SubCategoryName, Description, Status from CORIntSubCategory where SubCatID=" & Request.QueryString("id") & " ")
            If dtrreader.Read() Then
                hidSubCategoryId.Text = Request.QueryString("id")
                ddlCategory.Items.FindByValue(dtrreader("CategoryID")).Selected = True
                txtSubCategoryName.Text = dtrreader("SubCategoryName") & ""
                txtDescription.Text = dtrreader("Description") & ""
                If dtrreader("Status") = "Active" Then
                    rdStatus1.Checked = True
                ElseIf dtrreader("Status") = "Inactive" Then
                    rdStatus2.Checked = True
                Else
                    rdStatus3.Checked = True
                End If
            End If
            dtrreader.Close()
            accessdata.Dispose()
        End If
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        Dim intuniqcheck As SqlParameter
        Dim intuniqvalue As Int32
        cmd = New SqlCommand("procEditSubCategory", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@SubCategoryID", hidSubCategoryId.Text)
        cmd.Parameters.Add("@CategoryID", ddlCategory.SelectedValue)
        cmd.Parameters.Add("@SubCategoryName ", txtSubCategoryName.Text)
        cmd.Parameters.Add("@description", txtDescription.Text)
        If rdStatus1.Checked Then
            cmd.Parameters.Add("@status", rdStatus1.Text)
        ElseIf rdStatus2.Checked Then
            cmd.Parameters.Add("@status", rdStatus2.Text)
        Else
            cmd.Parameters.Add("@status", rdStatus3.Text)
        End If
        intuniqcheck = cmd.Parameters.Add("@uniqcheckval", SqlDbType.Int)
        intuniqcheck.Direction = ParameterDirection.Output
        MyConnection.Open()
        cmd.ExecuteNonQuery()
        intuniqvalue = cmd.Parameters("@uniqcheckval").Value
        MyConnection.Close()
        If Not intuniqvalue = 0 Then
            lblErrorMsg.Visible = True
            lblErrorMsg.Text = "SubCategory already exist."
            Exit Sub
        Else
            lblErrorMsg.Visible = False
            pnlmainform.Visible = False
            pnlconfirmation.Visible = True

            lblMessage.Text = "You have updated the SubCategory successfully"
            hyplnkretry.Text = "Edit another SubCategory"
            hyplnkretry.NavigateUrl = "SubCategoryEditSearch.aspx"
        End If
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("EditSubCategory.aspx?id=" & Request.QueryString("id"))
    End Sub
    Sub populateddl()
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility

        ddlCategory.DataSource = objAcessdata.funcGetSQLDataReader("select categoryid,categoryname from CORIntcategory  where active=1 order by CategoryName")
        ddlCategory.DataValueField = "categoryid"
        ddlCategory.DataTextField = "categoryname"
        ddlCategory.DataBind()
        ddlCategory.Items.Insert(0, New ListItem("-select category-", "0"))
        objAcessdata.Dispose()
    End Sub
End Class

