<%@ Page Language="vb" AutoEventWireup="false" src="EditSubCategory.aspx.vb" Inherits="EditSubCategory"%>
<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CarzonRent :: Internal software</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="JavaScript" src="../utilityfunction.js"></script>
		<script language="JavaScript" src="../JScripts/Datefunc.js"></script>
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
		<script language="JavaScript">
		function validation()
		{
			if(document.forms[0].ddlCategory.value=="0")
			{
				alert("Select a category name.");
				return false;
			}		
			if(document.forms[0].txtSubCategoryName.value=="")
			{
				alert("Enter a sub category name.");
				return false;
			}
		}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<uc1:headerctrl id="Headerctrl1" runat="server"></uc1:headerctrl>
			<table style="WIDTH: 548px; HEIGHT: 232px" align="center">
				&nbsp;
				<asp:panel id="pnlmainform" Runat="server">
					<TBODY>
						<TR>
							<TD align="center" colSpan="2"><B><U>Edit Sub Category</U></B></TD>
						</TR>
						<TR>
							<TD align="center" colSpan="2">
								<asp:Label id="lblErrorMsg" runat="server" visible="false" cssclass="subRedHead"></asp:Label></TD>
						</TR>
						<TR>
							<TD style="WIDTH: 180px; HEIGHT: 38px">*Category Name:</TD>
							<TD style="HEIGHT: 38px">
								<asp:DropDownList id="ddlCategory" runat="server"></asp:DropDownList></TD>
						</TR>
						<TR>
							<TD style="WIDTH: 180px; HEIGHT: 36px">*Sub Category Name:</TD>
							<TD style="HEIGHT: 36px">
								<asp:TextBox id="txtSubCategoryName" runat="server" MaxLength="50" Width="152px"></asp:TextBox></TD>
						</TR>
						<TR>
							<TD style="WIDTH: 180px; HEIGHT: 36px">Description:</TD>
							<TD style="HEIGHT: 36px">
								<asp:TextBox id="txtDescription" runat="server" MaxLength="1000" Width="176px" TextMode="MultiLine"
									Height="64px"></asp:TextBox></TD>
						</TR>
						<TR>
							<TD style="WIDTH: 180px; HEIGHT: 36px">Status:</TD>
							<TD style="HEIGHT: 36px">
								<asp:RadioButton id="rdStatus1" runat="server" GroupName="Status" Text="Active"></asp:RadioButton>
								<asp:RadioButton id="rdStatus2" runat="server" GroupName="Status" Text="Inactive"></asp:RadioButton>
								<asp:RadioButton id="rdStatus3" runat="server" GroupName="Status" Text="Reference"></asp:RadioButton></TD>
						</TR>
						<TR>
							<TD align="center" colSpan="2">
								<asp:Label id="hidSubCategoryId" runat="server" Visible="False"></asp:Label>
								<asp:button id="btnSubmit" runat="server" Text="Submit" CssClass="input"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;
								<asp:button id="btnReset" runat="server" Text="Reset" CssClass="input"></asp:button></TD>
						</TR>
				</asp:panel>
				<asp:panel id="pnlconfirmation" Runat="server" Visible="False">
					<TR>
						<TD style="HEIGHT: 28px" align="center" colSpan="2">&nbsp;&nbsp;&nbsp;
							<asp:Label id="lblMessage" runat="server"></asp:Label></TD>
					</TR>
					<TR>
						<TD align="center" colSpan="2">
							<asp:HyperLink id="hyplnkretry" runat="server"></asp:HyperLink></TD>
					</TR>
				</asp:panel></TBODY></table>
		</form>
	</body>
</HTML>
