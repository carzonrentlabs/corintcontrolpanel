﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Text;
using System.IO;

public partial class ImageViewer : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // byte[] bytes;        
        //idImage.Attributes["src"] = "D:\\Upload\\DutySlips\\8357779.pdf";
        //FileStream obj = new FileStream("D:\\Upload\\DutySlips\\8357779.pdf",FileMode.Open);
        //StreamReader obj = new StreamReader("D:\\Upload\\DutySlips\\8357779.pdf");
        
        try
        {
            //Response.Write(Request.QueryString["_fileType"]);
            //Response.End();            

            //if (Request.QueryString.HasKeys() && !string.IsNullOrEmpty(Convert.ToString(Session["loggedin_user"])))
            if (Request.QueryString.HasKeys())
            {
                //Response.Write(Request.QueryString["_fileType"]);
               // Response.End();

                if (!string.IsNullOrEmpty(Request.QueryString["fileName"]))
                {
                    string path = string.Empty; 
                    if (Request.QueryString["_fileType"] == "A")
                    {
                         path="ClientContract\\" + Request.QueryString["fileName"];
                    }
                    else if (Request.QueryString["_fileType"] == "C")
                    {
                        path="ClientBilling_CAM\\" + Request.QueryString["fileName"];
                    }
                    else if (Request.QueryString["_fileType"] == "S")
                    {

                        path="ClientBilling_SOP\\" + Request.QueryString["fileName"];
                    }
                    //else
                    //{
                    //    bytes = System.IO.File.ReadAllBytes(@"F:\\Upload\\Mails7132064.pdf");
                    //}

                    byte[] bytes = System.IO.File.ReadAllBytes(@"F:\\Upload\\" + path);

                    Response.Buffer = true;
                    Response.Charset = "";
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.ContentType = "application";
                    Response.BinaryWrite(bytes);
                    Response.Flush();
                    Response.End();
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message);
        }
    }

}