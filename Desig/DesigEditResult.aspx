<%@ Page Language="vb" AutoEventWireup="false" Src="DesigEditResult.aspx.vb" Inherits="DesigEditResult"%>
<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CarzonRent :: Internal software Control panel</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<uc1:headerctrl id="Headerctrl1" runat="server"></uc1:headerctrl>
			<table align="center">
				<tr>
					<td align="center"><STRONG><U>Edit a Designation where</U></STRONG></td>
				</tr>
				<tr>
					<td valign="top">
						<asp:table ID="tblRecDetail" HorizontalAlign=Center Runat="server" BorderColor=#cccc99 BorderStyle="Solid" GridLines=both CellSpacing="0" CellPadding="0" >
						
							<asp:tablerow>
								<asp:tableheadercell>
								&nbsp;&nbsp;<asp:linkbutton runat="server" Text="Designation Name" OnClick="SortGird" ID="Linkbutton1"></asp:linkbutton>&nbsp;&nbsp;
								</asp:tableheadercell>
								<asp:tableheadercell>
									&nbsp;&nbsp;<asp:linkbutton runat="server" Text="Active" OnClick="SortGird" ID="Linkbutton2"></asp:linkbutton>&nbsp;&nbsp;
								</asp:tableheadercell>
								<asp:tableheadercell>
									&nbsp;&nbsp;
								</asp:tableheadercell>
							</asp:tablerow>
						</asp:table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
