Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class MainAdEditForm
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents Validationsummary1 As System.Web.UI.WebControls.ValidationSummary
    Protected WithEvents Requiredfieldvalidator1 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator2 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnReset As System.Web.UI.WebControls.Button
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    Protected WithEvents txtaddress1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtaddress2 As System.Web.UI.WebControls.TextBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not Page.IsPostBack Then

            Dim objAcessdata As clsutility
            objAcessdata = New clsutility
            Dim dtrreader As SqlDataReader
            dtrreader = objAcessdata.funcGetSQLDataReader("select mainad from CORIntMainAdMaster where HeaderFooter=1 and ProviderId=" & Session("Provider_Id") & "")
            dtrreader.Read()
            If (dtrreader.HasRows) Then
                txtaddress1.Text = dtrreader("mainad")
            End If
            dtrreader.Close()
            dtrreader = objAcessdata.funcGetSQLDataReader("select mainad from CORIntMainAdMaster where HeaderFooter=2 and ProviderId=" & Session("Provider_Id") & "")

            dtrreader.Read()
            If (dtrreader.HasRows) Then
                txtaddress2.Text = dtrreader("mainad")
            End If
            dtrreader.Close()
            objAcessdata.Dispose()
        End If

    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("MainAdEditForm.aspx")
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        cmd = New SqlCommand("procEditAdd", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("@addres1", txtaddress1.Text)
        cmd.Parameters.Add("@addres2", txtaddress2.Text)
        cmd.Parameters.Add("@modifiedby", Session("loggedin_user"))
        cmd.Parameters.Add("@ProviderId", Session("provider_Id"))

        Try
            MyConnection.Open()
            cmd.ExecuteNonQuery()
            MyConnection.Close()
            pnlmainform.Visible = False
            pnlconfirmation.Visible = True

            lblMessage.Text = "You have updated the Addresses successfully"
            hyplnkretry.Text = "Edit Address again"
            hyplnkretry.NavigateUrl = "MainAdEditForm.aspx"
        Catch
      
        End Try
    End Sub
End Class
