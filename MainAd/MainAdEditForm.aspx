<%@ Page Language="vb" AutoEventWireup="false" Src="MainAdEditForm.aspx.vb" Inherits="MainAdEditForm"%>
<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CarzonRent :: Internal software Control panel</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" href="../HertzInt.css" type="text/css">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<uc1:Headerctrl id="Headerctrl1" runat="server"></uc1:Headerctrl>
			<p><asp:validationsummary id="Validationsummary1" runat="server" ShowSummary="false" HeaderText="Please make sure all the fields marked with * are filled in."
					ShowMessageBox="true"></asp:validationsummary></p>
			<table align="center">
				<asp:Panel ID="pnlmainform" Runat="server">
					<TBODY>
						<TR>
							<TD align="center" colSpan="2"><B><U>Edit Address</U></B></TD>
						</TR>
						<TR>
							<TD>* Top address
							</TD>
							<TD>
								<asp:textbox id="txtaddress1" runat="server" CssClass="input" size="100" MaxLength="250"></asp:textbox><br>(Entering a | will place a line break in its place when displayed on the invoice / duty slip)
								<asp:requiredfieldvalidator id="Requiredfieldvalidator1" runat="server" errormessage="" Display="None" Controltovalidate="txtaddress1"></asp:requiredfieldvalidator></TD>
						</TR>
						<TR>
							<TD>&nbsp;<br><br>
							</TD>
							<TD>&nbsp;<br><br>
						</TR>
						<TR>
							<TD>* Bottom address
							</TD>
							<TD>
								<asp:textbox id="txtaddress2" runat="server" CssClass="input" size="100" MaxLength="250"></asp:textbox>
								<asp:requiredfieldvalidator id="Requiredfieldvalidator2" runat="server" errormessage="" Display="None" Controltovalidate="txtaddress2"></asp:requiredfieldvalidator></TD>
						</TR>
						<TR>
							<TD align="center" colSpan="2">
								<asp:button id="btnSubmit" runat="server" CssClass="input" Text="Submit"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;
								<asp:button id="btnReset" runat="server" CssClass="input" Text="Reset" CausesValidation="False"></asp:button></TD>
						</TR>
				</asp:Panel>
				<asp:Panel ID="pnlconfirmation" Visible="False" Runat="server">
					<TR align="center">
						<TD colSpan="2">
							<asp:Label id="lblMessage" runat="server"></asp:Label></TD>
					</TR>
					<TR align="center">
						<TD colSpan="2">
							<asp:HyperLink id="hyplnkretry" runat="server"></asp:HyperLink></TD>
					</TR>
				</asp:Panel></TBODY>
			</table>
		</form>
	</body>
</HTML>
