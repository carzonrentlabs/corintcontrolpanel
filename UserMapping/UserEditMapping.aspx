<%@ Page Language="vb" AutoEventWireup="false"  Inherits="UserEditMapping" src="UserEditMapping.aspx.vb" %>
<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CarzonRent :: Internal software</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../utilityfunction.js"></script>
		<script language="JavaScript">
		function validate_input()
		{
		
		}
		
		</script>
	</HEAD>
	<body onLoad="OnLoadshowLength(document.forms[0].txtarearemarks.value,shwMessage)">
		<form id="Form1" method="post" runat="server">
			<uc1:Headerctrl id="Headerctrl1" runat="server"></uc1:Headerctrl>
			<table align="center">
				<TBODY>
					<TR>
						<TD align="center" colSpan="2"><B><U>Edit a User Mapping </U></B></TD>
					</TR>
					<tr>
						<td colspan="2" align="center"><asp:Label id="lblErrorMsg" cssclass="subRedHead" visible="false" runat="server"></asp:Label></td>
					</tr>
					<asp:panel id="pnlmainform" Runat="server">

						<TR>
							<TD>* User Name </TD>
							<TD><asp:DropDownList ID="ddlUserName" Enabled="false" runat="server" CssClass="input"></asp:DropDownList></TD>
						</TR>
						<TR>
							<TD>* Mappped User </TD>
						    <TD><asp:DropDownList ID="ddMappedUser" runat="server" CssClass="input"></asp:DropDownList></TD>
						</TR>
						<TR>
							<TD></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD vAlign="top">Remarks(<SPAN class="shwText" id="shwMessage">0</SPAN>/2000 chars)</TD>
							<TD>
								<asp:textbox id="txtarearemarks" onkeydown="showLength(this.form.txtarearemarks,'document.all.shwMessage',500)"
									onkeyup="showLength(this.form.txtarearemarks,'document.all.shwMessage',500)" runat="server"
									CssClass="input" MaxLength="500" TextMode="MultiLine" Rows="3" Columns="50"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>Active</TD>
							<TD>
								<asp:checkbox id="chkActive" runat="server" CssClass="input" Checked="True"></asp:checkbox></TD>
						</TR>

						<TR>
							<TD align="center" colSpan="2">
								<asp:button id="btnSubmit" runat="server" CssClass="input" Text="Submit"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;
								<INPUT type="reset" value="Reset" name="Reset" CssClass="button"></TD>
						</TR>
					</asp:panel>
					<asp:Panel ID="pnlconfirmation" Visible="False" Runat="server">
						<BR>
						<BR>
						<BR>
						<TR align="center">
							<TD colSpan="2"><INPUT type="hidden" name="txtarearemarks"> <SPAN class="shwText" id="shwMessage">
								</SPAN>
								<asp:Label id="lblMessage" runat="server"></asp:Label></TD>
						</TR>
						<TR align="center">
							<TD colSpan="2">
								<asp:HyperLink id="hyplnkretry" runat="server"></asp:HyperLink></TD>
						</TR>
					</asp:Panel></TBODY></table>
		</form>
	</body>
</HTML>
