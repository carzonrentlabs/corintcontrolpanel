Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class UserAddMapping
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected WithEvents ddlUserName As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddMappedUser As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtarearemarks As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkActive As System.Web.UI.WebControls.CheckBox
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnReset As System.Web.UI.WebControls.Button
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btnSubmit.Attributes("onClick") = "return validate_input();"
        If Not Page.IsPostBack Then
            populateddl()
        End If
    End Sub

    Sub populateddl()
        Dim dtrreader As SqlDataReader
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility

        ddlUserName.DataSource = objAcessdata.funcGetSQLDataReader("select SysUserID,isnull(fname,'')+' '+isnull(mname,'')+' '+isnull(lname,'') as username from CORIntSysUsersMaster where active=1 order by fname+'-'+mname+'-'+lname")
        ddlUserName.DataValueField = "SysUserID"
        ddlUserName.DataTextField = "username"
        ddlUserName.DataBind()
        ddlUserName.Items.Insert(0, New ListItem("", ""))

        ddMappedUser.DataSource = objAcessdata.funcGetSQLDataReader("select SysUserID,isnull(fname,'')+' '+isnull(mname,'')+' '+isnull(lname,'') as username from CORIntSysUsersMaster where active=1 order by fname+'-'+mname+'-'+lname")
        ddMappedUser.DataValueField = "SysUserID"
        ddMappedUser.DataTextField = "username"
        ddMappedUser.DataBind()
        ddMappedUser.Items.Insert(0, New ListItem("", ""))

        objAcessdata.Dispose()

    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        Dim intFlagCheck As SqlParameter
        Dim intFlag As Int32

        Dim intParam As SqlParameter
        Dim intid As Int32

        cmd = New SqlCommand("procMappUser", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("@UserID ", ddlUserName.SelectedItem.Value)
        cmd.Parameters.Add("@MappUserID", ddMappedUser.SelectedItem.Value)
        cmd.Parameters.Add("@remarks ", txtarearemarks.Text)
        cmd.Parameters.Add("@active", get_YNvalue(chkActive))
        cmd.Parameters.Add("@createdby ", Session("loggedin_user"))
        intFlagCheck = cmd.Parameters.Add("@StatusFlag", SqlDbType.Int)
        intFlagCheck.Direction = ParameterDirection.Output

        intParam = cmd.Parameters.Add("@ID", SqlDbType.Int)
        intParam.Direction = ParameterDirection.Output

        MyConnection.Open()
        cmd.ExecuteNonQuery()
        intFlag = cmd.Parameters("@StatusFlag").Value
        intid = cmd.Parameters("@ID").Value

        MyConnection.Close()
        lblMessage.Visible = True

        If Trim("" & intFlag) = "2" Then

            pnlmainform.Visible = False
            pnlconfirmation.Visible = True
            lblMessage.Text = "Mapping for user already exists!<br> The ID is <b>" & intid & "</b>"
            hyplnkretry.Text = "Add another User Mapping"
            hyplnkretry.NavigateUrl = "UserAddMapping.aspx"
        ElseIf Trim("" & intFlag) = "3" Then

            pnlmainform.Visible = False
            pnlconfirmation.Visible = True
            lblMessage.Text = "Please do not select same user!"
            hyplnkretry.Text = "Add another User Mapping"
            hyplnkretry.NavigateUrl = "UserAddMapping.aspx"

        Else

            pnlmainform.Visible = False
            pnlconfirmation.Visible = True

            lblMessage.Text = "You have added the user mapping <br> The ID is <b>" & intid & "</b>"
            hyplnkretry.Text = "Add another User Mapping"
            hyplnkretry.NavigateUrl = "UserAddMapping.aspx"
        End If
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("UserAddMapping.aspx")
    End Sub
End Class
