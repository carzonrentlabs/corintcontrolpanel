Imports commonutility
Imports System.text
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Imports System.Web.UI
Public Class UserEditResult
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tblRecDetail As System.Web.UI.WebControls.Table

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not Page.IsPostBack Then
            getvalue("username")
        End If
    End Sub
    Sub getvalue(ByVal strorderby As String)
        Dim strquery As StringBuilder
        Dim dtrreader As SqlDataReader
        Dim accessdata As clsutility
        accessdata = New clsutility
        strquery = New StringBuilder("select SMa.ID, S.fname+' '+isnull(S.mname,'')+' '+S.lname as username,S.sysuserid, ")
        strquery.Append(" case S.active when '1' then 'Active' when '0' then 'Not Active' end as active, U.UnitName,SMa.UnitUserID,  ")
        strquery.Append("(select SysUM.fname+' '+isnull(SysUM.mname,'')+' '+SysUM.lname from CORIntSysUsersMaster as SysUM (nolock) where SysUM.Active=1 and SysUM.SysUserID=SMa.UnitUserID) as MappedUser , ")
        strquery.Append("(select Unit.UnitName from corintunitmaster as Unit (nolock) where Unit.Active=1 and Unit.unitid=SMa.unitid) as MappedUnit ")
        strquery.Append(" from CORIntSysUsersMaster S (nolock) inner join CorIntSysUserMapping SMa (nolock) on SMa.SysUserID=S.SysUserID inner join ")
        strquery.Append(" corintunitmaster U (nolock) on u.unitid=S.UnitID ")
        strquery.Append(" where SMa.Active=1 and S.Active=1 ")

        If Request.QueryString("id") <> "-1" Then
            strquery.Append(" and S.sysuserid=" & Request.QueryString("id") & " ")
        End If
        strquery.Append(" order by " & strorderby & "")
        'Response.Write(strquery.ToString)
        'Response.end()
        dtrreader = accessdata.funcGetSQLDataReader(strquery.ToString)
        While dtrreader.Read
            Dim Temprow As New TableRow
            Temprow.HorizontalAlign = HorizontalAlign.Center

            Dim Tempcell As New TableCell
            Tempcell.Controls.Add(New LiteralControl(dtrreader("username") & ""))
            Temprow.Cells.Add(Tempcell)

            Dim Tempcel2 As New TableCell
            Tempcel2.Controls.Add(New LiteralControl(dtrreader("MappedUser") & ""))
            Temprow.Cells.Add(Tempcel2)

            Dim Tempcel3 As New TableCell
            Tempcel3.Controls.Add(New LiteralControl(dtrreader("MappedUnit") & ""))
            Temprow.Cells.Add(Tempcel3)

            Dim Tempcel4 As New TableCell
            Tempcel4.Controls.Add(New LiteralControl(dtrreader("active") & ""))
            Temprow.Cells.Add(Tempcel4)

            Dim Tempcel5 As New TableCell
            Tempcel5.Controls.Add(New LiteralControl("<a href=UserEditMapping.aspx?ID=" & dtrreader("ID") & " >Edit</a>"))
            Temprow.Cells.Add(Tempcel5)
            'adding the Tables rows to the table
            tblRecDetail.Rows.Add(Temprow)

        End While
        dtrreader.Close()
    End Sub
    Sub SortGird(ByVal sender As Object, ByVal e As System.EventArgs)
        Select Case sender.id
            Case "Linkbutton1"
                getvalue("username")
            Case "Linkbutton2"
                getvalue("SMa.UnitUserID")
            Case "Linkbutton3"
                getvalue("U.unitname")
            Case "Linkbutton4"
                getvalue("U.active")
        End Select

    End Sub
End Class
