Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class UserEditSearch
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnReset As System.Web.UI.WebControls.Button
    Protected WithEvents ddUserName As System.Web.UI.WebControls.DropDownList

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not Page.IsPostBack Then
            Dim dtrreader As SqlDataReader
            Dim accessdata As clsutility
            accessdata = New clsutility
            dtrreader = accessdata.funcGetSQLDataReader("select distinct SysUser.fname+' '+ isnull(SysUser.mname,'')+' '+isnull(SysUser.lname,'') as username,SysUser.sysuserid from CORIntSysUsersMaster as SysUser (nolock) inner join CorIntSysUserMapping as SMp (nolock) on SMp.SysUserID = SysUser.SysUserID where SysUser.Active=1 and SMp.Active=1  order by SysUser.fname+' '+isnull(SysUser.mname,'')+' '+isnull(SysUser.lname,'') ")
            ddUserName.DataSource = dtrreader
            ddUserName.DataValueField = "sysuserid"
            ddUserName.DataTextField = "username"
            ddUserName.DataBind()
            ddUserName.Items.Insert(0, New ListItem("Any", -1))
            dtrreader.Close()
        End If

    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Response.Redirect("UserEditResult.aspx?id=" & ddUserName.SelectedItem.Value)
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("UserEditsearch.aspx")
    End Sub
End Class
