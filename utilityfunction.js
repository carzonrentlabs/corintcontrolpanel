function showLength(field, countfield, maxLimit) {
    //setting the limit of the textarea to 500
    if (field.value.length > maxLimit) // if too long...trim it!
        field.value = field.value.substring(0, maxLimit);
    // otherwise, update 'characters left' counter
    else
        if (document.all)
        { eval(countfield).innerText = field.value.length; }
        else {
            var strCtrlName = new String();
            var strtmp = new String();
            strCtrlName = countfield;
            strtmp = strCtrlName.substr(strCtrlName.lastIndexOf(".") + 1, strCtrlName.length - strCtrlName.lastIndexOf("."));
            document.getElementById(strtmp).innerHTML = field.value.length;
        }

}

function OnLoadshowLength(field1, field2) {
    var str = new String();
    str = field1;
    if (str.length != 0) {
        eval(field2).innerText = str.length;
    }
}


function checkmandatory(ctrntrlnames) {
    var strvalues
    strvalues = ctrntrlnames
    var arrvalue
    arrvalue = strvalues.split(",")
    for (i = 0; i < arrvalue.length; i++) {
        var cntrlName = eval('document.forms[0].' + arrvalue[i])
        if (cntrlName.value == "") {
            alert("Please make sure all the fields marked with * are filled in")
            document.getElementById(arrvalue[i]).focus();      
            return false;
        }
    }

}

// Code added by BSL 0n 03-April-07
function smartOptionFinder(oSelect, oEvent) {
    var sKeyCode = oEvent.keyCode;
    var sToChar = String.fromCharCode(sKeyCode);
    if (sKeyCode > 47 && sKeyCode < 91) {
        var sNow = new Date().getTime();
        if (oSelect.getAttribute("finder") == null) {
            oSelect.setAttribute("finder", sToChar.toUpperCase())
            oSelect.setAttribute("timer", sNow)
        }
        else if (sNow > parseInt(oSelect.getAttribute("timer")) + 2000) { //Rest all;
            oSelect.setAttribute("finder", sToChar.toUpperCase())
            oSelect.setAttribute("timer", sNow) //reset timer;
        } else {
            oSelect.setAttribute("finder", oSelect.getAttribute("finder") + sToChar.toUpperCase())
            oSelect.setAttribute("timer", sNow); //update timer;
        }
        var sFinder = oSelect.getAttribute("finder");
        var arrOpt = oSelect.options
        var iLen = arrOpt.length
        for (var i = 0; i < iLen; i++) {
            sTest = arrOpt[i].text;
            if (sTest.toUpperCase().indexOf(sFinder) == 0) {
                arrOpt[i].selected = true;
                break;
            }
        }
        event.returnValue = false;
    }
    else {
        //Not a digit;
    }
    return true;
}

// Code added by BSL 0n 03-April-07
function smartOptionFinderWithSubmit(oSelect, oEvent) {
    var sKeyCode = oEvent.keyCode;
    var sToChar = String.fromCharCode(sKeyCode);
    if (sKeyCode > 47 && sKeyCode < 91) {
        var sNow = new Date().getTime();
        if (oSelect.getAttribute("finder") == null) {
            oSelect.setAttribute("finder", sToChar.toUpperCase())
            oSelect.setAttribute("timer", sNow)
        }
        else if (sNow > parseInt(oSelect.getAttribute("timer")) + 2000) { //Rest all;
            oSelect.setAttribute("finder", sToChar.toUpperCase())
            oSelect.setAttribute("timer", sNow) //reset timer;
        } else {
            oSelect.setAttribute("finder", oSelect.getAttribute("finder") + sToChar.toUpperCase())
            oSelect.setAttribute("timer", sNow); //update timer;
        }
        var sFinder = oSelect.getAttribute("finder");
        var arrOpt = oSelect.options
        var iLen = arrOpt.length
        for (var i = 0; i < iLen; i++) {
            sTest = arrOpt[i].text;
            if (sTest.toUpperCase().indexOf(sFinder) == 0) {
                arrOpt[i].selected = true;
                break;
            }
        }
        event.returnValue = false;
    }
    else {
        //Not a digit;
    }
    if (sKeyCode == 13 || sKeyCode == 9) {
        document.Form1.submit();
    }
    return true;
}