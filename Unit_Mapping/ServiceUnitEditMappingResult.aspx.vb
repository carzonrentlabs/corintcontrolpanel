Imports commonutility
Imports System.text
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Imports System.Web.UI
Partial Class Unit_Mapping_ServiceUnitEditMappingResult
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    ' Protected WithEvents tblRecDetail As System.Web.UI.WebControls.Table

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not Page.IsPostBack Then
            getvalue("UM.Unitname")
        End If
    End Sub

    Sub getvalue(ByVal strorderby As String)
        Dim strquery As StringBuilder
        Dim dtrreader As SqlDataReader
        Dim accessdata As clsutility
        accessdata = New clsutility
        strquery = New StringBuilder("select UMap.UID, UMap.UnitID, UMap.ReferencedUnitID, UM.UnitName, UM1.UnitName as MapUnitName from CORIntServiceUnitMapping as UMap (nolock) inner join CorIntUnitMaster as UM (nolock) on UM.UnitID = UMap.UnitID left outer join CorIntUnitMaster as UM1 (nolock) on UM1.UnitID = UMap.ReferencedUnitID")
        If Request.QueryString("id") <> "-1" Then
            strquery.Append(" where UMap.unitid=" & Request.QueryString("id") & " ")
        End If
        strquery.Append(" order by " & strorderby & "")

        dtrreader = accessdata.funcGetSQLDataReader(strquery.ToString)
        While dtrreader.Read
            Dim Temprow As New TableRow
            Temprow.HorizontalAlign = HorizontalAlign.Center

            Dim Tempcel1 As New TableCell
            Tempcel1.Controls.Add(New LiteralControl(dtrreader("ReferencedUnitID") & ""))
            Temprow.Cells.Add(Tempcel1)

            Dim Tempcel2 As New TableCell
            Tempcel2.Controls.Add(New LiteralControl(dtrreader("MapUnitName") & ""))
            Temprow.Cells.Add(Tempcel2)

            Dim Tempcel3 As New TableCell
            Tempcel3.Controls.Add(New LiteralControl(dtrreader("UnitID") & ""))
            Temprow.Cells.Add(Tempcel3)

            Dim Tempcel4 As New TableCell
            Tempcel4.Controls.Add(New LiteralControl(dtrreader("UnitName") & ""))
            Temprow.Cells.Add(Tempcel4)

            Dim Tempcel5 As New TableCell
            Tempcel5.Controls.Add(New LiteralControl("<a href=ServiceUnitEditMapping.aspx?ID=" & dtrreader("UID") & " >Edit</a>"))
            Temprow.Cells.Add(Tempcel5)
            'adding the Tables rows to the table
            tblRecDetail.Rows.Add(Temprow)

        End While
        dtrreader.Close()
    End Sub
    Sub SortGird(ByVal sender As Object, ByVal e As System.EventArgs)
        Select Case sender.id
            Case "Linkbutton1"
                getvalue("ReferencedUnitID")
            Case "Linkbutton2"
                getvalue("MapUnitName")
            Case "Linkbutton3"
                getvalue("UMap.UnitID")
            Case "Linkbutton4"
                getvalue("UM.UnitName")
        End Select

    End Sub
End Class
