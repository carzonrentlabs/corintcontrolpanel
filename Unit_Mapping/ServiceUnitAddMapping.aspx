<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ServiceUnitAddMapping.aspx.vb" Inherits="Unit_Mapping_ServiceUnitAddMapping" %>
<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<HTML>
<HEAD>
<title>CarzonRent :: Internal software Control Panel</title>
<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
<meta content="JavaScript" name="vs_defaultClientScript">
<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
<script language="JavaScript" src="../utilityfunction.js"></script>
<script language="JavaScript" src="../JScripts/Datefunc.js"></script>
<script language="JavaScript">
function validate_input()
{
	if(document.forms[0].ddlUnit.value == "")
	{
		alert("Please Select Unit Name.")
		document.forms[0].ddlUnit.focus();
		return false;
	}

	if(document.forms[0].ddlMapUnit.value == "")
	{
		alert("Please Select Mapped Unit Name.")
		document.forms[0].ddlMapUnit.focus();
		return false;
	}
}
</script>
</HEAD>
<body>
<form id="Form1" method="post" runat="server">
	<uc1:headerctrl id="Headerctrl1" runat="server"></uc1:headerctrl>
	<table align="center">
		<TBODY>
			<TR>
				<TD align="center" colSpan="2"><B><U>Add a Service Unit Mapping</U></B></TD>
			</TR>
			<tr>
			<TR colspan="2" align="center">
				<asp:Label id="lblErrorMsg" cssclass="subRedHead" visible="false" runat="server">
				</asp:Label>
				</td>
			</TR>
			<asp:Panel ID="pnlmainform" Runat="server">
			<TR>
				<TD>* Mapped Unit Name
				</TD>
				<TD>
					<asp:dropdownlist id="ddlMapUnit" runat="server"></asp:dropdownlist>
				</TD>
			</TR>
			<TR>
				<TD>* Unit Name
				</TD>
				<TD>
					<asp:dropdownlist id="ddlUnit" runat="server"></asp:dropdownlist>
				</TD>
			</TR>
			
			<TR>
				<TD align="center" colSpan="2">
					<asp:button id="btnSubmit" runat="server" CssClass="input" Text="Submit">
					</asp:button>&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="Reset" CssClass="button" name="Reset" value="Reset" /></TD>
			</TR>
			</asp:Panel>
			<asp:Panel ID="pnlconfirmation" Visible="False" Runat="server">
			<TR align="center">
				<TD colSpan="2">
				<br>	<br>	<br>	
					<asp:Label id="lblMessage" runat="server"></asp:Label></TD>
			</TR>
			<TR align="center">
				<TD colSpan="2">
					<asp:HyperLink id="hyplnkretry" runat="server"></asp:HyperLink></TD>
			</TR>
			</asp:Panel>
		</TBODY>
	</table>
</form>
</body>
</HTML>
