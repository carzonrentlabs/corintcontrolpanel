Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class UnitEditMapping
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ddlUnit As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlMapUnit As System.Web.UI.WebControls.DropDownList
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btnSubmit.Attributes("onClick") = "return validate_input();"
        If Not Page.IsPostBack Then
            populateddl()

            Dim dtrreader As SqlDataReader
            Dim accessdata As clsutility
            accessdata = New clsutility
            dtrreader = accessdata.funcGetSQLDataReader("select * from CORIntUnitMapping where UID=" & Request.QueryString("id") & " ")
            dtrreader.Read()

            ddlUnit.Items.FindByValue(dtrreader("ReferencedUnitID")).Selected = True
            ddlMapUnit.Items.FindByValue(dtrreader("UnitID")).Selected = True
            dtrreader.Close()
            accessdata.Dispose()

        End If
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        Dim intuniqcheck As SqlParameter
        Dim intuniqvalue As int32

        cmd = New SqlCommand("prc_EditUnitMapping", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@UID", Request.QueryString("id"))
        cmd.Parameters.Add("@UnitID", ddlMapUnit.SelectedItem.Value)
        cmd.Parameters.Add("@ReferencedUnitID", ddlUnit.SelectedItem.Value)
        cmd.Parameters.Add("@modifiedby", Session("loggedin_user"))
        intuniqcheck = cmd.Parameters.Add("@uniqcheckval", SqlDbType.Int)
        intuniqcheck.Direction = ParameterDirection.Output
        '  Try
        MyConnection.Open()
        cmd.ExecuteNonQuery()
        intuniqvalue = cmd.Parameters("@uniqcheckval").Value
        MyConnection.Close()
        If Not intuniqvalue = 0 Then
            lblErrorMsg.visible = True
            lblErrorMsg.text = "Unit Mapping already exist."
            Exit Sub
        Else
            lblErrorMsg.visible = False
            pnlmainform.Visible = False
            pnlconfirmation.Visible = True

            lblMessage.Text = "You have updated the Unit Mapping successfully"
            hyplnkretry.Text = "Edit another Unit Mapping"
            hyplnkretry.NavigateUrl = "UnitEditMappingSearch.aspx"
        End If
    End Sub

    Sub populateddl()
        Dim dtrreader As SqlDataReader
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility

        ddlUnit.DataSource = objAcessdata.funcGetSQLDataReader("select UnitID, Unitname from CorIntUnitMaster order by Unitname")
        ddlUnit.DataValueField = "UnitID"
        ddlUnit.DataTextField = "Unitname"
        ddlUnit.DataBind()
        ddlUnit.Items.Insert(0, New ListItem("", ""))

        ddlMapUnit.DataSource = objAcessdata.funcGetSQLDataReader("select UnitID, Unitname from CorIntUnitMaster order by Unitname")
        ddlMapUnit.DataValueField = "UnitID"
        ddlMapUnit.DataTextField = "Unitname"
        ddlMapUnit.DataBind()
        ddlMapUnit.Items.Insert(0, New ListItem("", ""))

        objAcessdata.Dispose()

    End Sub

End Class
