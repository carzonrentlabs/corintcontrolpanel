Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class SDPkgsAddForm
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents ddlcarcat As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlCarModel As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddTariffType As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlcity As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlpkgdays As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlpkgkm As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtrate As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtrateextra As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtextrakm As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtextraday As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtthreextrakm As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtdiscount As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtRemarks As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkactive As System.Web.UI.WebControls.CheckBox
    Protected WithEvents btnsubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnreset As System.Web.UI.WebControls.Button
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents lblErrorMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    Protected WithEvents txtVae As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtTp As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtPac As System.Web.UI.WebControls.TextBox
    Protected WithEvents ChkSpecial As System.Web.UI.WebControls.CheckBox
    Protected WithEvents ddlPkgType As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtEffectiveFromDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtEffectiveToDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtMaxHour As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtPkgLimit As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtrateWeekEnd As System.Web.UI.WebControls.TextBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btnsubmit.Attributes("onClick") = "return validation();"
        If Not Page.IsPostBack Then
            populateddl()
    
        End If
    End Sub
    Sub populateddl()
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility

        ddlcity.DataSource = objAcessdata.funcGetSQLDataReader("select CityName ,CityID  from CORIntCityMaster   where active=1 order by CityName")
        ddlcity.DataValueField = "CityID"
        ddlcity.DataTextField = "CityName"
        ddlcity.DataBind()
        ddlcity.Items.Insert(0, New ListItem("Select City", ""))

        'ddlcarcat.DataSource = objAcessdata.funcGetSQLDataReader("select CarCatName ,CarCatID  from CORIntCarCatMaster   where active=1 order by CarCatName")
        'ddlcarcat.DataValueField = "CarCatID"
        'ddlcarcat.DataTextField = "CarCatName"
        'ddlcarcat.DataBind()
        'ddlcarcat.Items.Insert(0, New ListItem("Select Car Caategory", ""))

        ddlCarModel.DataSource = objAcessdata.funcGetSQLDataReader("select CarModelID,CarModelName  from CORIntCarModelMaster where active=1 order by CarModelName")
        ddlCarModel.DataValueField = "CarModelID"
        ddlCarModel.DataTextField = "CarModelName"
        ddlCarModel.DataBind()
        ddlCarModel.Items.Insert(0, New ListItem("Select Model", ""))

        objAcessdata.funcpopulatenumddw(365, 0, ddlpkgdays)
        objAcessdata.funcpopulatenumddw(5000, 0, ddlpkgkm)
        objAcessdata.Dispose()

    End Sub

    Private Sub btnsubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsubmit.Click

        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        Dim intuniqcheck As SqlParameter
        Dim intuniqvalue as int32
        Dim intPkgidParam As SqlParameter
        Dim intPkgid As Int32
        Dim specialPkgYN As Boolean

        cmd = New SqlCommand("procAddSDPkgsMaster", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure

        'cmd.Parameters.Add("@carcatid", ddlcarcat.SelectedItem.Value)
        cmd.Parameters.AddWithValue("@CarModelID", ddlCarModel.SelectedItem.Value)
        cmd.Parameters.AddWithValue("@TariffType", ddTariffType.SelectedItem.Value)
        cmd.Parameters.AddWithValue("@CityID", ddlcity.SelectedItem.Value)
        cmd.Parameters.AddWithValue("@pkgdays", ddlpkgdays.SelectedItem.Value)
        cmd.Parameters.AddWithValue("@pkgkms", ddlpkgkm.SelectedItem.Value)
        cmd.Parameters.AddWithValue("@pkgrate", txtrate.Text)
        cmd.Parameters.AddWithValue("@pkgrateWeekEnd", txtrateWeekEnd.Text)

        If Not txtrateextra.Text = "" Then
            cmd.Parameters.AddWithValue("@extradayrate", txtrateextra.Text)
        Else
            lblErrorMessage.Text = "Please select the Extra Day rate"

        End If
        If Not txtextrakm.Text = "" Then
            cmd.Parameters.AddWithValue("@extrakmrate", txtextrakm.Text)
        Else
            lblErrorMessage.Text = "Please select the Extra Km rate"

        End If
        If Not txtextraday.Text = "" Then
            cmd.Parameters.AddWithValue("@thresholdextraday", txtextraday.Text)
        Else
            lblErrorMessage.Text = "Please select the thresholdextraday"

        End If
        If Not txtextrakm.Text = "" Then
            cmd.Parameters.AddWithValue("@thresholdextrakm", txtextrakm.Text)
        Else
            lblErrorMessage.Text = "Please select the thresholdextrakm"

        End If
        If Not txtdiscount.Text = "" Then
            cmd.Parameters.AddWithValue("@depositamt", txtdiscount.Text)
        Else
            lblErrorMessage.Text = "Please select the depositamt"

        End If
        If Not ddlPkgType.SelectedValue = "0" Then
            cmd.Parameters.AddWithValue("@pkgType", ddlPkgType.SelectedItem.Text)
        Else
            lblErrorMessage.Text = "Please select the Package Type"

        End If
        If Not txtPkgLimit.Text = "" Then
            cmd.Parameters.AddWithValue("@pkgLimit", txtPkgLimit.Text)
        Else
            lblErrorMessage.Text = "Please select the PkgLimt"

        End If
        If Not txtEffectiveFromDate.Text = "" Then
            cmd.Parameters.AddWithValue("@EffectiveFromDate", txtEffectiveFromDate.Text)
        Else
            lblErrorMessage.Text = "Please select the EffectiveFromDate"

        End If
        If Not txtEffectiveToDate.Text = "" Then
            cmd.Parameters.AddWithValue("@EffectiveToDate", txtEffectiveToDate.Text)
        Else
            cmd.Parameters.AddWithValue("@EffectiveToDate", txtEffectiveToDate.Text)
        End If
        If Not txtMaxHour.Text = "" Then
            cmd.Parameters.AddWithValue("@pkgMaxHour", txtMaxHour.Text)
        Else
            lblErrorMessage.Text = "Please select the max pkg hours"

        End If
        If ChkSpecial.Checked = True Then
            If txtEffectiveToDate.Text = "" Then
                lblErrorMessage.Text = "Please select the EffectiveToDate"

            End If
            specialPkgYN = True
            cmd.Parameters.AddWithValue("@SpecialPkg", Convert.ToInt32(specialPkgYN))
        Else
            specialPkgYN = False
            cmd.Parameters.AddWithValue("@SpecialPkg", Convert.ToInt32(specialPkgYN))
        End If
        cmd.Parameters.AddWithValue("@remarks", txtRemarks.Text)
        cmd.Parameters.AddWithValue("@active", get_YNvalue(chkactive))
        cmd.Parameters.AddWithValue("@createdby", Session("loggedin_user"))

        If Not txtVae.Text = "" Then
            cmd.Parameters.AddWithValue("@vae", txtVae.Text)
        End If
        If Not txtTp.Text = "" Then
            cmd.Parameters.AddWithValue("@tp", txtTp.Text)
        End If
        If Not txtPac.Text = "" Then
            cmd.Parameters.AddWithValue("@pac", txtPac.Text)
        End If

        intuniqcheck = cmd.Parameters.AddWithValue("@uniqcheckval", SqlDbType.Int)
        intuniqcheck.Direction = ParameterDirection.Output

        intPkgidParam = cmd.Parameters.AddWithValue("@Pkgid", SqlDbType.Int)
        intPkgidParam.Direction = ParameterDirection.Output


        Try
            MyConnection.Open()
            cmd.ExecuteNonQuery()
            intuniqvalue = cmd.Parameters("@uniqcheckval").Value
            intPkgid = cmd.Parameters("@Pkgid").Value
            MyConnection.Close()
            If Not intuniqvalue = 0 Then
                pnlmainform.Visible = False
                pnlconfirmation.Visible = True

                lblMessage.Text = "This particular package already exists! <br> The Package ID is <b>" & intPkgid & "</b>"
                hyplnkretry.Text = "Add another Self Drive Package"
                hyplnkretry.NavigateUrl = "SDPkgsAddForm.aspx"
            Else
                pnlmainform.Visible = False
                pnlconfirmation.Visible = True

                lblMessage.Text = "You have added the Self Drive Package successfully <br> The Package ID is <b>" & intPkgid & "</b>"
                hyplnkretry.Text = "Add another Self Drive Package"
                hyplnkretry.NavigateUrl = "SDPkgsAddForm.aspx"
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function

    Private Sub btnreset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnreset.Click
        Response.Redirect("SDPkgsAddForm.aspx")
    End Sub
End Class