<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AssignAndRemoveSDPackage.aspx.vb"
    Inherits="SDPkgs_AssignAndRemoveSDPackage" %>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CarzonRent :: Internal software Control panel</title>

		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="JavaScript" src="../utilityfunction.js"></script>
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
    <script language="JavaScript">
        function validate_input()
        {
            if(document.getElementById("<%=ddlcompanyname.ClientID%>").selectedIndex == 0)
            {
                alert("Please Select Client.")
		        document.getElementById("<%=ddlcompanyname.ClientID%>").focus();
		        return false;
            
            }
	        else if(document.getElementById("<%=PkgFromSD.ClientID%>").value=="")
	        {
		        alert("Please Package ID.")
		        document.getElementById("<%=PkgFromSD.ClientID%>").focus();
		        return false;
	        }
            else if(document.getElementById("<%=PkgToSD.ClientID%>").value =="")
	        {
		        alert("Please Package ID.")
		        document.getElementById ("<%=PkgToSD.ClientID%>").focus();
		        return false;
	        }
        }
        function validate_input_Remove()
        {

        
           if(document.getElementById("<%=ddlcompanyname.ClientID%>").selectedIndex==0)
            {
                alert("Please Select Client.")
		        document.getElementById("<%=ddlcompanyname.ClientID%>").focus();
		        return false;
            
            }
            else if(document.getElementById("<%=PkgFromSD.ClientID%>").value=="")
	        {
		        alert("Please Package ID.")
		        document.getElementById("<%=PkgFromSD.ClientID%>").focus();
		        return false;
	        }
            else if(document.getElementById("<%=PkgToSD.ClientID%>").value =="")
	        {
		        alert("Please Package ID.")
		        document.getElementById ("<%=PkgToSD.ClientID%>").focus();
		        return false;
	        }
	     }

    </script>
</head>
<body>
    <form id="form1" runat="server">
      <uc1:Headerctrl ID="Headerctrl1" runat="server"></uc1:Headerctrl>
        <div>
            <table align="center">
              <tr><td colspan="3"><asp:Label id="lblMessage" runat="server" ForeColsor="red"></asp:Label><br />
                <tr>
                    <td>
                        Company Name</td>
                    <td>
                        <asp:DropDownList ID="ddlcompanyname" runat="server" CssClass="input" AutoPostBack="true">
                        </asp:DropDownList></td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        Package IDs</td>
                    <td>
                        From:
                        <asp:TextBox ID="PkgFromSD" size="5" runat="server" CssClass="input"></asp:TextBox>
                        To:
                        <asp:TextBox ID="PkgToSD" size="5" runat="server" CssClass="input"></asp:TextBox>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="text-align:center;">
                        <asp:Button ID="AssignPackage" runat="server" Text="Assign Package"  OnClientClick="return validate_input();" />
                        <asp:Button ID="RemovePackage" runat="server" Text="Remove Package" OnClientClick="return validate_input_Remove();"/>
                    </td>
                   
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
