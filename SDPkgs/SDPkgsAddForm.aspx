<%@ Page Language="vb" AutoEventWireup="false" Src="SDPkgsAddForm.aspx.vb" Inherits="SDPkgsAddForm" %>

<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>CarzonRent :: Internal software Control panel</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">

    <script language="JavaScript" src="../utilityfunction.js"></script>

    <link href="../HertzInt.css" type="text/css" rel="stylesheet">

    <script language="JavaScript" src="../JScripts/Datefunc.js"></script>

    <script language="javascript">
   function dateReg(obj)
            {
                if(obj.value!="")
                {
                    // alert(obj.value);
                    var reg = /((0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[0-1])-(201[0-9]))/
                    if(reg.test(obj.value))
                    {
                        //alert('valid');
                    }
                    else
                    {
                        alert('notvalid');
                        obj.value="";
                    }
                }
            }
    
    function validation()
    {

		    if(isNaN(document.forms[0].txtrate.value))
		    {
			    alert("Rate should be numeric and upto two decimal digits only.")
			    return false;	
		    }
		    else
		    {
		        if (parseFloat(document.forms[0].txtrate.value.indexOf("."))>0)
			    {
				    var FlotVal
				    FlotVal=document.forms[0].txtrate.value.substr(parseFloat(document.forms[0].txtrate.value.indexOf(".")),document.forms[0].txtrate.value.length)
				    if(parseFloat((FlotVal.length)-1)>2)
				    {
				        alert("Rate should be numeric and upto two decimal digits only")
				        return false;
				    }
			    }	
		    }
		    
		    if(isNaN(document.forms[0].txtrateWeekEnd.value))
		    {
			    alert("WeekEnd Rate should be numeric and upto two decimal digits only.")
			    return false;	
		    }
		    else
		    {
		        if (parseFloat(document.forms[0].txtrateWeekEnd.value.indexOf("."))>0)
			    {
				    var FlotVal
				    FlotVal=document.forms[0].txtrateWeekEnd.value.substr(parseFloat(document.forms[0].txtrateWeekEnd.value.indexOf(".")),document.forms[0].txtrateWeekEnd.value.length)
				    if(parseFloat((FlotVal.length)-1)>2)
				    {
				        alert("WeekEnd Rate should be numeric and upto two decimal digits only")
				        return false;
				    }
			    }	
		    }
		    if(isNaN(document.forms[0].txtrateextra.value))
            {
                alert("Rate / Extra Day should be numeric and upto two decimal digits only.")
                return false;	
            }
            else
            {
                if (parseFloat(document.forms[0].txtrateextra.value.indexOf("."))>0)
                {
                    var FlotVal
                    FlotVal=document.forms[0].txtrateextra.value.substr(parseFloat(document.forms[0].txtrateextra.value.indexOf(".")),document.forms[0].txtrateextra.value.length)
                    if(parseFloat((FlotVal.length)-1)>2)
                    {
                        alert("Rate / Extra Day should be numeric and upto two decimal digits only")
                        return false;
                    }
                }	
            }
            if(isNaN(document.forms[0].txtextrakm.value))
            {
                alert("Rate / Extra KM should be numeric and upto two decimal digits only.")
                return false;	
            }
            else
            {
                if (parseFloat(document.forms[0].txtextrakm.value.indexOf("."))>0)
                {
                    var FlotVal
                    FlotVal=document.forms[0].txtextrakm.value.substr(parseFloat(document.forms[0].txtextrakm.value.indexOf(".")),document.forms[0].txtextrakm.value.length)
                    if(parseFloat((FlotVal.length)-1)>2)
                    {
                        alert("Rate / Extra KM should be numeric and upto two decimal digits only")
                        return false;
                    }
                }	
            }    			
            if(isNaN(document.forms[0].txtPkgLimit.value))
            {
                alert("Please select the pkg limit in numeric.")
                return false;	
            }					

            if(isNaN(document.forms[0].txtMaxHour.value))
            {
                alert("Please select the max pkg hours.")
                return false;	
            }	
            
            if(isNaN(document.forms[0].txtextraday.value))
            {
                alert("Threshold Extra Day (Crosses to next package if reached) should be numeric only.")
                return false;	
            }	
            
            if(isNaN(document.forms[0].txtthreextrakm.value))
            {
                alert("Threshold Extra KM (Crosses to next package if reached) should be numeric only.")
                return false;	
            }	
            var ChkSpecial = document.getElementById('<%=ChkSpecial.ClientID%>');
            var strValue =ChkSpecial.id             
            if(strValue.checked == true)
            {
        
                var cntrlName = document.getElementById('<%=txtEffectiveToDate.ClientID%>').value
			    if (cntrlName.value=="")
			    {
				    alert("Please select effective To date in case of special package")
				    return false;
			    }
            }

            if(isNaN(document.forms[0].txtdiscount.value))
            {
                alert("Deposit Amount should be numeric and upto two decimal digits only.")
                return false;	
            }
            else
            {
                if (parseFloat(document.forms[0].txtdiscount.value.indexOf("."))>0)
                {
                    var FlotVal
                    FlotVal=document.forms[0].txtdiscount.value.substr(parseFloat(document.forms[0].txtdiscount.value.indexOf(".")),document.forms[0].txtdiscount.value.length)
                    if(parseFloat((FlotVal.length)-1)>2)
                    {
                        alert("Deposit Amount should be numeric and upto two decimal digits only")
                        return false;
                    }
                }	
            }						
            var strvalues
            strvalues=('ddlpkgdays,ddlpkgkm,txtrate,ddlPkgType,txtPkgLimit,,txtEffectiveFromDate,txtMaxHour')            	
            return checkmandatory(strvalues)		
    }		
	function checkmandatory(ctrntrlnames)
	{
		var strvalues
		strvalues=ctrntrlnames
		var arrvalue
		arrvalue=strvalues.split(",")
		for (i=0;i<arrvalue.length;i++)
		{
			var cntrlName = eval('document.forms[0].' + arrvalue[i])
			if (cntrlName.value=="")
			{
				alert("Please make sure all the fields marked with * are filled in")
				return false;
			}
		}		
	}	
    </script>

</head>
<body>
    <form id="Form1" method="post" runat="server">
        <uc1:Headerctrl ID="Headerctrl1" runat="server"></uc1:Headerctrl>
        <table id="Table1" align="center">
            <asp:Panel ID="pnlmainform" runat="server">
                <%--  <tbody>--%>
                <tr>
                    <td align="center" colspan="2">
                        <strong><u>Add a Self Drive Package</u></strong>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        &nbsp;&nbsp;</td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <asp:Label ID="lblErrorMessage" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        * Car Model
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlCarModel" runat="server" CssClass="input">
                        </asp:DropDownList></td>
                </tr>
                <%--<tr>
                    <td>
                        * Car Category
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlcarcat" runat="server" CssClass="input">
                        </asp:DropDownList></td>
                </tr>--%>
                <tr>
                    <td>
                        * Tariff Type</td>
                    <td>
                        <asp:DropDownList ID="ddTariffType" runat="server" CssClass="input">
                            <asp:ListItem Text="Special" Value="S" />
                            <asp:ListItem Text="National" Value="N" />
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td>
                        * Pick-up City Name
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlcity" runat="server" CssClass="input">
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td>
                        * Package Days/Hour</td>
                    <td>
                        <asp:DropDownList ID="ddlpkgdays" runat="server" CssClass="input">
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td>
                        * Package KMs
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlpkgkm" runat="server" CssClass="input">
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td>
                        * Rate (WeekDay)
                    </td>
                    <td>
                        <asp:TextBox ID="txtrate" runat="server" CssClass="input" MaxLength="50"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        * Rate (WeekEnd)
                    </td>
                    <td>
                        <asp:TextBox ID="txtrateWeekEnd" runat="server" CssClass="input" MaxLength="50"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        * VAE</td>
                    <td>
                        <asp:TextBox ID="txtVae" runat="server" CssClass="input" MaxLength="50" Enabled="false"
                            Text="0"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        * TP</td>
                    <td>
                        <asp:TextBox ID="txtTp" runat="server" CssClass="input" MaxLength="50" Enabled="false"
                            Text="0"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        * PAC</td>
                    <td>
                        <asp:TextBox ID="txtPac" runat="server" CssClass="input" MaxLength="50" Enabled="false"
                            Text="0"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        * Rate / Extra Day
                    </td>
                    <td>
                        <asp:TextBox ID="txtrateextra" runat="server" CssClass="input" MaxLength="50"></asp:TextBox>%
                        * Extra per day of Package Rate.</td>
                </tr>
                <tr>
                    <td>
                        * Rate / Extra KM</td>
                    <td>
                        <asp:TextBox ID="txtextrakm" runat="server" CssClass="input" MaxLength="50"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        * Threshold Extra Day (Crosses to next package if reached)
                    </td>
                    <td>
                        <asp:TextBox ID="txtextraday" runat="server" CssClass="input" MaxLength="50"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        * Threshold Extra KM (Crosses to next package if reached)</td>
                    <td>
                        <asp:TextBox ID="txtthreextrakm" runat="server" CssClass="input" MaxLength="50"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        * Deposit Amount
                    </td>
                    <td>
                        <asp:TextBox ID="txtdiscount" runat="server" CssClass="input" MaxLength="50"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        * Pakage Type:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlPkgType" runat="server">
                            <asp:ListItem Value="0" Text="Select package Type"></asp:ListItem>
                            <asp:ListItem Value="1" Text="Hourly"></asp:ListItem>
                            <asp:ListItem Value="2" Text="Daily"></asp:ListItem>
                            <asp:ListItem Value="3" Text="Weekly"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Monthly"></asp:ListItem>
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td>
                        * Package Km Limit:
                    </td>
                    <td>
                        <asp:TextBox ID="txtPkgLimit" runat="server" CssClass="input" MaxLength="50" Text="250"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        * Effective FromDate:
                    </td>
                    <td>
                        <asp:TextBox ID="txtEffectiveFromDate" runat="server" CssClass="input" MaxLength="12"
                            onblur="dateReg(this);"></asp:TextBox>
                        <font><a onmouseover="window.status='Date Picker';return true;" onmouseout="window.status='';return true;"
                            href="javascript:show_calendar('Form1.txtEffectiveFromDate');">
                            <img height="21" src="../images/show-calendar.gif" width="24" border="0"></a></font></td>
                </tr>
                <tr>
                    <td>
                        * Effective ToDate:
                    </td>
                    <td>
                        <asp:TextBox ID="txtEffectiveToDate" runat="server" CssClass="input" MaxLength="12"
                            onblur="dateReg(this);"></asp:TextBox>
                        <font><a onmouseover="window.status='Date Picker';return true;" onmouseout="window.status='';return true;"
                            href="javascript:show_calendar('Form1.txtEffectiveToDate');">
                            <img height="21" src="../images/show-calendar.gif" width="24" border="0"></a></font></td>
                </tr>
                <tr>
                    <td>
                        * Pkg Max Hour:
                    </td>
                    <td>
                        <asp:TextBox ID="txtMaxHour" runat="server" CssClass="input" MaxLength="50" Text="24"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        * Special Packge YN:
                    </td>
                    <td>
                        <asp:CheckBox ID="ChkSpecial" runat="server" /></td>
                </tr>
                <tr>
                    <td valign="top">
                        Remarks(<span class="shwText" id="shwMessage">0</span>/2000 chars)</td>
                    <td>
                        <asp:TextBox ID="txtRemarks" onkeydown="showLength(this.form.txtRemarks,'document.all.shwMessage',2000)"
                            onkeyup="showLength(this.form.txtRemarks,'document.all.shwMessage',2000)" runat="server"
                            CssClass="input" MaxLength="2000" Rows="3" Columns="50" TextMode="MultiLine"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        * Active</td>
                    <td>
                        <asp:CheckBox ID="chkactive" runat="server" CssClass="input" Checked="True"></asp:CheckBox></td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <asp:Button ID="btnsubmit" runat="server" CssClass="button" Text="Submit"></asp:Button>&nbsp;&nbsp;
                        <asp:Button ID="btnreset" runat="server" CssClass="button" Text="Reset" CausesValidation="False">
                        </asp:Button></td>
                </tr>
            </asp:Panel>
            <asp:Panel ID="pnlconfirmation" runat="server" Visible="False">
                <tr align="center">
                    <td colspan="2">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label></td>
                </tr>
                <tr align="center">
                    <td colspan="2">
                        <asp:HyperLink ID="hyplnkretry" runat="server"></asp:HyperLink></td>
                </tr>
            </asp:Panel>
            <%-- </tbody>--%>
        </table>
    </form>
</body>
</html>
