Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Imports System.Data.Sql
Imports System.Data.OleDb
Imports System.IO
Partial Class SDPkgs_AssignAndRemoveSDPackage
    Inherits System.Web.UI.Page

    Protected Sub AssignPackage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles AssignPackage.Click
        Try

            Dim MyConnection As SqlConnection
            Dim SqlCmd As SqlCommand
            'Dim InsertSqlCmd As SqlCommand
            Dim InsertSqlCmd1 As SqlCommand
            Dim Dt As DataTable = New DataTable()
            Dim Adap As SqlDataAdapter
            Dim num As Integer

            AssignPackage.Attributes.Add("OnClick", " return validate_input()")

            MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
            SqlCmd = New SqlCommand("select Pkgid from CORIntSDPkgsMaster where pkgid between " + PkgFromSD.Text + " and " + PkgToSD.Text + "", MyConnection)
            MyConnection.Open()
            Adap = New SqlDataAdapter(SqlCmd)
            Adap.Fill(Dt)
            Dim Str As New StringBuilder

            MyConnection.Close()
            For num = 0 To Dt.Rows.Count - 1

                InsertSqlCmd1 = New SqlCommand("procAssignedPkgsSelfDrive", MyConnection)
                InsertSqlCmd1.CommandType = CommandType.StoredProcedure
                InsertSqlCmd1.Parameters.AddWithValue("@PkgID", Convert.ToString(Dt.Rows(num)("Pkgid")))
                InsertSqlCmd1.Parameters.AddWithValue("@ClientID", ddlcompanyname.SelectedItem.Value)
                MyConnection.Open()
                Try
                    InsertSqlCmd1.ExecuteNonQuery()
                Catch exp As SqlException
                End Try
                MyConnection.Close()
            Next
        Catch ex As Exception
            Response.Write(ex.Message.ToString())
        Finally
            lblMessage.Text = "Record Added successfully"
            PkgFromSD.Text = ""
            PkgToSD.Text = ""
        End Try
    End Sub

    Protected Sub RemovePackage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles RemovePackage.Click
        Dim MyConnection As SqlConnection
        Dim SqlCmd As SqlCommand
        Dim InsertSqlCmd As SqlCommand
        If ddlcompanyname.SelectedItem.Value <> -1 Then
            MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
            InsertSqlCmd = New SqlCommand("prc_DeleteAssignPkgSelfDrive", MyConnection)
            InsertSqlCmd.CommandType = CommandType.StoredProcedure
            InsertSqlCmd.Parameters.AddWithValue("@ClientType", "C")
            InsertSqlCmd.Parameters.AddWithValue("@ClientID", ddlcompanyname.SelectedItem.Value)
            InsertSqlCmd.Parameters.AddWithValue("@pkgIDFrom", Convert.ToInt32(PkgFromSD.Text))
            InsertSqlCmd.Parameters.AddWithValue("@pkgIDTo", Convert.ToInt32(PkgToSD.Text))
            MyConnection.Open()
            Try
                InsertSqlCmd.ExecuteNonQuery()
            Catch exp As SqlException
            End Try
            MyConnection.Close()

            lblMessage.Text = "Package Remove!"
            PkgFromSD.Text = ""
            PkgToSD.Text = ""
        Else
            lblMessage.Text = "Please Select Client!"

        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim DtRreader As SqlDataReader
            Dim AccessData As clsutility
            AccessData = New clsutility
            DtRreader = AccessData.funcGetSQLDataReader("select ClientCoID ,ClientCoName  from CORIntClientCoMaster where TariffType='S' and active=1 order by ClientCoName")
            ddlcompanyname.DataSource = DtRreader
            ddlcompanyname.DataValueField = "ClientCoID"
            ddlcompanyname.DataTextField = "ClientCoName"
            ddlcompanyname.DataBind()
            ddlcompanyname.Items.Insert(0, New ListItem("Any", -1))
            DtRreader.Close()
        End If
    End Sub
End Class
