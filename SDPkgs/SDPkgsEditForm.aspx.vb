Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class SDPkgsEditForm
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents ddlcarcat As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlCarModel As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddTariffType As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlcity As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlpkgdays As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlpkgkm As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtrate As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtrateWeekEnd As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtrateextra As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtextrakm As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtextraday As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtthreextrakm As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtdiscount As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtRemarks As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkactive As System.Web.UI.WebControls.CheckBox
    Protected WithEvents btnsubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnreset As System.Web.UI.WebControls.Button
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    Protected WithEvents txtVae As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtTp As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtPac As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlPkgType As System.Web.UI.WebControls.DropDownList

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btnsubmit.Attributes("onClick") = "return validation();"
        If Not Page.IsPostBack Then
            populateddl()
            Dim dtrreader As SqlDataReader
            Dim accessdata As clsutility
            accessdata = New clsutility
            dtrreader = accessdata.funcGetSQLDataReader("select * from CORIntSDPkgsMaster where  pkgid=" & Request.QueryString("id") & " ")
            dtrreader.Read()

            autoselec_ddl(ddlCarModel, dtrreader("carmodelid"))
            autoselec_ddl(ddlcity, dtrreader("CityID"))
            autoselec_ddl_pkgdays(ddlpkgdays, dtrreader("pkgdays"))
            autoselec_ddl_pkgdays(ddlpkgkm, dtrreader("pkgkms"))
            txtrate.Text = dtrreader("pkgrate") & ""
            txtrateWeekEnd.Text = dtrreader("PkgRate_WeekEnd") & ""

            txtVae.Text = dtrreader("vae") & ""
            txtTp.Text = dtrreader("tp") & ""
            txtPac.Text = dtrreader("pac") & ""

            txtrateextra.Text = dtrreader("extradayrate") & ""
            txtextrakm.Text = dtrreader("extrakmrate") & ""
            txtextraday.Text = dtrreader("thresholdextraday") & ""
            txtextrakm.Text = dtrreader("extrakmrate") & ""
            txtdiscount.Text = dtrreader("depositamt") & ""
            txtthreextrakm.Text = dtrreader("thresholdextrakm") & ""
            ddTariffType.Items.FindByValue(dtrreader("TariffType")).Selected = True
            txtRemarks.Text = dtrreader("remarks") & ""
            chkactive.Checked = dtrreader("active")

            If dtrreader("pkgType") = "Hourly" Then
                autoselec_ddl(ddlPkgType, "1")
                'End If
            ElseIf dtrreader("pkgType") = "Daily" Then
                autoselec_ddl(ddlPkgType, "2")
            'End If
            ElseIf dtrreader("pkgType") = "Weekly" Then
                autoselec_ddl(ddlPkgType, "3")
                'End If
            ElseIf dtrreader("pkgType") = "Monthly" Then
                autoselec_ddl(ddlPkgType, "4")
            Else
                autoselec_ddl(ddlPkgType, "0")
            End If

            dtrreader.Close()
            accessdata.Dispose()
            End If
    End Sub
    Sub populateddl()
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility

        'ddlcarcat.DataSource = objAcessdata.funcGetSQLDataReader("select CarCatName ,CarCatID  from CORIntCarCatMaster   where active=1 order by CarCatName")
        'ddlcarcat.DataValueField = "CarCatID"
        'ddlcarcat.DataTextField = "CarCatName"
        'ddlcarcat.DataBind()
        'ddlcarcat.Items.Insert(0, New ListItem("", ""))

        ddlCarModel.DataSource = objAcessdata.funcGetSQLDataReader("select CarModelID,CarModelName  from CORIntCarModelMaster where active=1 order by CarModelName")
        ddlCarModel.DataValueField = "CarModelID"
        ddlCarModel.DataTextField = "CarModelName"
        ddlCarModel.DataBind()
        ddlCarModel.Items.Insert(0, New ListItem("Select Model", ""))

        ddlcity.DataSource = objAcessdata.funcGetSQLDataReader("select CityName ,CityID  from CORIntCityMaster   where active=1 order by CityName")
        ddlcity.DataValueField = "CityID"
        ddlcity.DataTextField = "CityName"
        ddlcity.DataBind()
        ddlcity.Items.Insert(0, New ListItem("", ""))

        objAcessdata.funcpopulatenumddw(365, 0, ddlpkgdays)
        objAcessdata.funcpopulatenumddw(5000, 0, ddlpkgkm)
        objAcessdata.Dispose()

    End Sub
    Private Sub btnsubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        cmd = New SqlCommand("procEditSDPkgsMaster", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.AddWithValue("@rowid", Request.QueryString("id"))
        cmd.Parameters.AddWithValue("@carmodelid", ddlCarModel.SelectedItem.Value)
        cmd.Parameters.AddWithValue("@TariffType", ddTariffType.SelectedItem.Value)
        cmd.Parameters.AddWithValue("@CityID", ddlcity.SelectedItem.Value)
        cmd.Parameters.AddWithValue("@pkgdays", ddlpkgdays.SelectedItem.Value)
        cmd.Parameters.AddWithValue("@pkgkms", ddlpkgkm.SelectedItem.Value)
        cmd.Parameters.AddWithValue("@pkgrate", txtrate.Text)
        cmd.Parameters.AddWithValue("@pkgrateWeekEnd", txtrateWeekEnd.Text)


        If Not txtVae.Text = "" Then
            cmd.Parameters.AddWithValue("@Vae", txtVae.Text)
        End If
        If Not txtTp.Text = "" Then
            cmd.Parameters.AddWithValue("@tp", txtTp.Text)
        End If
        If Not txtPac.Text = "" Then
            cmd.Parameters.AddWithValue("@pac", txtPac.Text)
        End If


        If Not txtrateextra.Text = "" Then
            cmd.Parameters.AddWithValue("@extradayrate", txtrateextra.Text)
        End If
        If Not txtextrakm.Text = "" Then
            cmd.Parameters.AddWithValue("@extrakmrate", txtextrakm.Text)
        End If
        If Not txtextraday.Text = "" Then
            cmd.Parameters.AddWithValue("@thresholdextraday", txtextraday.Text)
        End If
        If Not txtextrakm.Text = "" Then
            cmd.Parameters.AddWithValue("@thresholdextrakm", txtextrakm.Text)
        End If
        If Not txtdiscount.Text = "" Then
            cmd.Parameters.AddWithValue("@depositamt", txtdiscount.Text)
        End If
        cmd.Parameters.AddWithValue("@remarks", txtRemarks.Text)
        cmd.Parameters.AddWithValue("@active", get_YNvalue(chkactive))
        cmd.Parameters.AddWithValue("@modifiedby", Session("loggedin_user"))
        cmd.Parameters.AddWithValue("@pkgType", ddlPkgType.SelectedItem.Text)
        

        '  Try
        MyConnection.Open()
        cmd.ExecuteNonQuery()
        MyConnection.Close()
        pnlmainform.Visible = False
        pnlconfirmation.Visible = True

        lblMessage.Text = "You have updated the Self Drive Package successfully"
        hyplnkretry.Text = "Edit another Self Drive Package"
        hyplnkretry.NavigateUrl = "SDPkgsEditSearch.aspx"
        '   Catch
        '   End Try
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function


    Function autoselec_ddl(ByVal ddlname As DropDownList, ByVal selectvalue As Int32)
        If Not selectvalue = "0" Then
            ddlname.Items.FindByValue(selectvalue).Selected = True
        End If
    End Function

    Function autoselec_ddl_pkgdays(ByVal ddlname As DropDownList, ByVal selectvalue As Int32)
        ddlname.Items.FindByValue(selectvalue).Selected = True
    End Function

    Private Sub btnreset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnreset.Click
        Response.Redirect("SDPkgsEditForm.aspx?id=" & Request.QueryString("id"))
    End Sub
End Class
