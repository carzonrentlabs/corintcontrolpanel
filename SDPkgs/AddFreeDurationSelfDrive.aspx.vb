Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Partial Class SDPkgs_AddFreeDurationSelfDrive
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' lblErrorMsg.Visible = False
        If Not Page.IsPostBack Then
            BindData()
        End If

    End Sub

    Private Sub BindData()
        Dim SqlConnection As SqlConnection

        Dim SqlDataAdapter As SqlDataAdapter = New SqlDataAdapter()
        Dim DataSet As DataSet = New DataSet()
        SqlConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        SqlConnection.Open()
        Dim SqlCommand As SqlCommand = New SqlCommand("select ID,type as packagetype,Duration as totalDuration,FreeDuration ,active from CORIntSDPkgsExtra order by ID")

        SqlCommand.Connection = SqlConnection
        SqlDataAdapter = New SqlDataAdapter(SqlCommand)
        SqlDataAdapter.Fill(DataSet)
        If DataSet.Tables(0).Rows.Count > 1 Then
            grv_FreeDuration.DataSource = DataSet
            grv_FreeDuration.DataBind()
        Else
            If DataSet.Tables(0).Rows.Count = 0 Then
                DataSet.Tables(0).Rows.Add(DataSet.Tables(0).NewRow())
                grv_FreeDuration.DataSource = DataSet
                grv_FreeDuration.DataBind()
                Dim columnCount As Integer = grv_FreeDuration.Rows(0).Cells.Count
                grv_FreeDuration.Rows(0).Cells.Clear()
                grv_FreeDuration.Rows(0).Cells.Add(New TableCell)
                grv_FreeDuration.Rows(0).Cells(0).ColumnSpan = columnCount
                grv_FreeDuration.Rows(0).Cells(0).Text = "No Records Found."

            End If
        End If
        SqlConnection.Close()
    End Sub

    Protected Sub grv_FreeDuration_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grv_FreeDuration.PageIndexChanging
        grv_FreeDuration.PageIndex = e.NewPageIndex
        BindData()
    End Sub

    Protected Sub grv_FreeDuration_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles grv_FreeDuration.RowUpdating
        Try
            Dim SqlConnection As SqlConnection
            Dim SqlDataAdapter As SqlDataAdapter = New SqlDataAdapter()
            Dim DataSet As DataSet = New DataSet()
            SqlConnection = New SqlConnection(ConfigurationManager.AppSettings("corConnectString"))
            Dim lblID_Edit As Label = DirectCast(grv_FreeDuration.Rows(e.RowIndex).FindControl("lblID_Edit"), Label)
            Dim txtTotalDuration_Edit As TextBox = DirectCast(grv_FreeDuration.Rows(e.RowIndex).FindControl("txtTotalDuration_Edit"), TextBox)
            Dim ddlPackageType_Edit As DropDownList = DirectCast(grv_FreeDuration.Rows(e.RowIndex).FindControl("ddlPackageType_Edit"), DropDownList)
            Dim txtFreeDuration_Edit As TextBox = DirectCast(grv_FreeDuration.Rows(e.RowIndex).FindControl("txtFreeDuration_Edit"), TextBox)
            Dim chkActive As CheckBox = DirectCast(grv_FreeDuration.Rows(e.RowIndex).FindControl("chkActive_Edit"), CheckBox)
            Dim Active As Int32
            If chkActive.Checked = True Then
                Active = 1
            Else
                Active = 0
            End If
            Dim SqlCommand As SqlCommand = New SqlCommand("Update CORIntSDPkgsExtra set type='" & ddlPackageType_Edit.SelectedItem.Text & "',Duration='" & txtTotalDuration_Edit.Text & "',FreeDuration='" & txtFreeDuration_Edit.Text & "',Active=" & Active & ",ModifiedBy = " & Session("loggedin_user") & ",modifyDate ='" & DateTime.Now() & "' where ID=" & lblID_Edit.Text & "")
            SqlCommand.Connection = SqlConnection
            SqlCommand.Connection.Open()
            SqlCommand.ExecuteNonQuery()
            grv_FreeDuration.EditIndex = -1
            BindData()
            SqlConnection.Close()
        Catch ex As Exception
            lblErrorMsg.Text = ex.Message + "- Free and Total Should be numeric."
        End Try

    End Sub

    Protected Sub grv_FreeDuration_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grv_FreeDuration.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim chkActive As CheckBox = e.Row.FindControl("chkActive_Edit")
                Dim lblChkActive As Label = CType(e.Row.FindControl("lblActive_Edit"), Label)
                Dim ddlPackageType_Edit As DropDownList = CType(e.Row.FindControl("ddlPackageType_Edit"), DropDownList)
                Dim lblPackageType As Label = CType(e.Row.FindControl("lblPackageType_Edit"), Label)
                If ddlPackageType_Edit IsNot Nothing Then
                    ddlPackageType_Edit.Items.FindByText(lblPackageType.Text).Selected = True
                End If
                If chkActive IsNot Nothing Then
                    If lblChkActive.Text = "True" Then
                        chkActive.Checked = True
                    Else
                        chkActive.Checked = False
                    End If
                End If
            End If
        Catch ex As Exception
            Response.Write(ex.Message())
        End Try
    End Sub

    Protected Sub grv_FreeDuration_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grv_FreeDuration.RowEditing
        grv_FreeDuration.EditIndex = e.NewEditIndex
        BindData()
    End Sub

    Protected Sub grv_FreeDuration_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles grv_FreeDuration.RowCancelingEdit
        grv_FreeDuration.EditIndex = -1
        BindData()
    End Sub


    Protected Sub AddFreeDuration(ByVal sender As Object, ByVal e As EventArgs)
        Dim SqlConnection As SqlConnection
        Try
            Dim txtTotalDuration_Footer As String = DirectCast(grv_FreeDuration.FooterRow.FindControl("txtTotalDuration_Footer"), TextBox).Text
            Dim ddlPackageType_Footer As String = DirectCast(grv_FreeDuration.FooterRow.FindControl("ddlPackageType_Footer"), DropDownList).SelectedItem.Text.ToString()
            Dim txtFreeDuration_Footer As String = DirectCast(grv_FreeDuration.FooterRow.FindControl("txtFreeDuration_Footer"), TextBox).Text
            Dim chkActive_Footer As CheckBox = DirectCast(grv_FreeDuration.FooterRow.FindControl("chkActive_Footer"), CheckBox)
            Dim Active As Integer
            Active = 0
            If chkActive_Footer.Checked = True Then
                Active = 1
            Else
                Active = 0
            End If
            SqlConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
            Dim cmd As New SqlCommand()
            cmd.CommandType = CommandType.Text
            cmd.CommandText = "insert into CORIntSDPkgsExtra (pkgid,Type,Duration,FreeDuration,CreatedBy,CreateDate,Active) values(2,@Type, @TotalDuration,@Freeduration, @CreatedBy,@CreateDate,@Active)"
            cmd.Connection = SqlConnection
            cmd.Parameters.Add("@Type", SqlDbType.VarChar).Value = ddlPackageType_Footer
            cmd.Parameters.Add("@TotalDuration", SqlDbType.Int).Value = Convert.ToInt32(txtTotalDuration_Footer)
            cmd.Parameters.Add("@Freeduration", SqlDbType.Int).Value = Convert.ToInt32(txtFreeDuration_Footer)
            cmd.Parameters.Add("@CreatedBy", SqlDbType.Int).Value = Session("loggedin_user")
            cmd.Parameters.Add("@CreateDate", SqlDbType.DateTime).Value = DateTime.Now
            cmd.Parameters.Add("@Active", SqlDbType.Int).Value = Active
            cmd.Connection.Open()
            Dim status As Integer = cmd.ExecuteNonQuery()
            If status = 1 Then
                lblErrorMsg.Text = "Sublocation inserted successfully."
            End If
            BindData()
        Catch ex As Exception
            lblErrorMsg.Text = ex.Message + "- Free and Total Should be numeric."
        End Try

    End Sub
End Class
