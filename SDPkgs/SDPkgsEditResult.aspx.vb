Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.text
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Imports System.Web.UI
Public Class SDPkgsEditResult
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tblRecDetail As System.Web.UI.WebControls.Table

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not Page.IsPostBack Then
            getvalue("C.carcatname")
        End If
    End Sub
    Sub getvalue(ByVal strorderby As String)
        Dim strquery As StringBuilder
        Dim dtrreader As SqlDataReader
        Dim accessdata As clsutility
        accessdata = New clsutility
        strquery = New StringBuilder("select M.pkgid, C.carcatname, A.CityName, convert(varchar(10),M.pkgdays)+'/'+convert(varchar(10),M.pkgkms) as package, M.ExtraDayRate, M.ExtraKMRate, case M.TariffType when 'N' then 'National' when 'S' then 'Special' end as TariffType, case M.active when '1' then 'Active' when '0' then 'Not Active' end as active from CORIntSDPkgsMaster M left outer join CORIntCityMaster A on A.cityid=M.cityid, CORIntCarCatMaster C where M.carcatid=C.carcatid ")
        If Request.QueryString("PkgID") <> "" Then
            strquery.Append(" and M.pkgid=" & Request.QueryString("pkgid") & " ")
        End If
        If Request.QueryString("CarCat") <> "-1" Then
            strquery.Append(" and M.carcatid=" & Request.QueryString("CarCat") & " ")
        End If
        If Request.QueryString("Tariff") <> "" Then
            strquery.Append(" and M.TariffType='" & Request.QueryString("Tariff") & "' ")
        End If
        If Request.QueryString("City") <> "-1" Then
            strquery.Append(" and M.CityID=" & Request.QueryString("City") & " ")
        End If
        If Request.QueryString("PkgDays") <> "" Then
            strquery.Append(" and M.pkgdays=" & Request.QueryString("PkgDays") & " ")
        End If
        If Request.QueryString("PkgKMs") <> "" Then
            strquery.Append(" and M.pkgkms=" & Request.QueryString("PkgKMs") & " ")
        End If
        If Request.QueryString("Active") <> "" Then
            strquery.Append(" and M.Active=" & Request.QueryString("Active") & " ")
        End If
        strquery.Append(" order by " & strorderby & "")
        dtrreader = accessdata.funcGetSQLDataReader(strquery.ToString)
        While dtrreader.Read
            Dim Temprow As New TableRow
            Temprow.HorizontalAlign = HorizontalAlign.Center

            Dim Tempcel10 As New TableCell
            Tempcel10.Controls.Add(New LiteralControl(dtrreader("PkgID") & ""))
            Temprow.Cells.Add(Tempcel10)

            Dim Tempcell As New TableCell
            Tempcell.Controls.Add(New LiteralControl(dtrreader("carcatname") & ""))
            Temprow.Cells.Add(Tempcell)

            Dim Tempcel2 As New TableCell
            Tempcel2.Controls.Add(New LiteralControl(dtrreader("package") & ""))
            Temprow.Cells.Add(Tempcel2)

            Dim Tempcel12 As New TableCell
            Tempcel12.Controls.Add(New LiteralControl(dtrreader("ExtraDayRate") & ""))
            Temprow.Cells.Add(Tempcel12)

            Dim Tempcel13 As New TableCell
            Tempcel13.Controls.Add(New LiteralControl(dtrreader("ExtraKMRate") & ""))
            Temprow.Cells.Add(Tempcel13)

            Dim Tempcel5 As New TableCell
            Tempcel5.Controls.Add(New LiteralControl(dtrreader("TariffType") & ""))
            Temprow.Cells.Add(Tempcel5)

            Dim Tempcel6 As New TableCell
            Tempcel6.Controls.Add(New LiteralControl(dtrreader("CityName") & ""))
            Temprow.Cells.Add(Tempcel6)

            Dim Tempcel3 As New TableCell
            Tempcel3.Controls.Add(New LiteralControl(dtrreader("active") & ""))
            Temprow.Cells.Add(Tempcel3)

            Dim Tempcel4 As New TableCell
            Tempcel4.Controls.Add(New LiteralControl("<a href=SDPkgsEditForm.aspx?ID=" & dtrreader("pkgid") & " >Edit</a>"))
            Temprow.Cells.Add(Tempcel4)
            'adding the Tables rows to the table
            tblRecDetail.Rows.Add(Temprow)

        End While
        dtrreader.Close()
        accessdata.Dispose()


    End Sub
    Sub SortGird(ByVal sender As Object, ByVal e As System.EventArgs)
        Select Case sender.id
            Case "Linkbutton6"
                getvalue("PkgID")
            Case "Linkbutton1"
                getvalue("carcatname")
            Case "Linkbutton2"
                getvalue("package")
            Case "Linkbutton12"
                getvalue("ExtraDayRate")
            Case "Linkbutton13"
                getvalue("ExtraKMRate")
            Case "Linkbutton4"
                getvalue("TariffType")
            Case "Linkbutton5"
                getvalue("CityName")
            Case "Linkbutton3"
                getvalue("active")
        End Select

    End Sub

End Class
