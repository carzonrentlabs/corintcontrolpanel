<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" Src="SDPkgsEditForm.aspx.vb" Inherits="SDPkgsEditForm" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>CarzonRent :: Internal software Control panel</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <script language="JavaScript" src="../utilityfunction.js"></script>
    <link href="../HertzInt.css" type="text/css" rel="stylesheet">
    <script language="javascript" type="text/javascript">
        function validation() {

            if (isNaN(document.forms[0].txtrate.value)) {
                alert("Rate should be numeric and upto two decimal digits only.")
                return false;
            }
            else {
                if (parseFloat(document.forms[0].txtrate.value.indexOf(".")) > 0) {
                    var FlotVal
                    FlotVal = document.forms[0].txtrate.value.substr(parseFloat(document.forms[0].txtrate.value.indexOf(".")), document.forms[0].txtrate.value.length)
                    if (parseFloat((FlotVal.length) - 1) > 2) {
                        alert("Rate should be numeric and upto two decimal digits only")
                        return false;
                    }

                }
            }

            if (isNaN(document.forms[0].txtrateWeekEnd.value)) {
                alert("WeekEnd Rate should be numeric and upto two decimal digits only.")
                return false;
            }
            else {
                if (parseFloat(document.forms[0].txtrateWeekEnd.value.indexOf(".")) > 0) {
                    var FlotVal
                    FlotVal = document.forms[0].txtrateWeekEnd.value.substr(parseFloat(document.forms[0].txtrateWeekEnd.value.indexOf(".")), document.forms[0].txtrateWeekEnd.value.length)
                    if (parseFloat((FlotVal.length) - 1) > 2) {
                        alert("WeekEnd Rate should be numeric and upto two decimal digits only")
                        return false;
                    }

                }
            }
            if (isNaN(document.forms[0].txtrateextra.value)) {
                alert("Rate / Extra Day should be numeric and upto two decimal digits only.")
                return false;
            }
            else {
                if (parseFloat(document.forms[0].txtrateextra.value.indexOf(".")) > 0) {
                    var FlotVal
                    FlotVal = document.forms[0].txtrateextra.value.substr(parseFloat(document.forms[0].txtrateextra.value.indexOf(".")), document.forms[0].txtrateextra.value.length)
                    if (parseFloat((FlotVal.length) - 1) > 2) {
                        alert("Rate / Extra Day should be numeric and upto two decimal digits only")
                        return false;
                    }

                }
            }

            if (isNaN(document.forms[0].txtextrakm.value)) {
                alert("Rate / Extra KM should be numeric and upto two decimal digits only.")
                return false;
            }
            else {
                if (parseFloat(document.forms[0].txtextrakm.value.indexOf(".")) > 0) {
                    var FlotVal
                    FlotVal = document.forms[0].txtextrakm.value.substr(parseFloat(document.forms[0].txtextrakm.value.indexOf(".")), document.forms[0].txtextrakm.value.length)
                    if (parseFloat((FlotVal.length) - 1) > 2) {
                        alert("Rate / Extra KM should be numeric and upto two decimal digits only")
                        return false;
                    }

                }
            }

            if (isNaN(document.forms[0].txtextraday.value)) {
                alert("Threshold Extra Day (Crosses to next package if reached) should be numeric only.")
                return false;
            }

            if (isNaN(document.forms[0].txtthreextrakm.value)) {
                alert("Threshold Extra KM (Crosses to next package if reached) should be numeric only.")
                return false;
            }

            if (isNaN(document.forms[0].txtdiscount.value)) {
                alert("Deposit Amount should be numeric and upto two decimal digits only.")
                return false;
            }
            else {
                if (parseFloat(document.forms[0].txtdiscount.value.indexOf(".")) > 0) {
                    var FlotVal
                    FlotVal = document.forms[0].txtdiscount.value.substr(parseFloat(document.forms[0].txtdiscount.value.indexOf(".")), document.forms[0].txtdiscount.value.length)
                    if (parseFloat((FlotVal.length) - 1) > 2) {
                        alert("Deposit Amount should be numeric and upto two decimal digits only")
                        return false;
                    }

                }
            }

            var strvalues
            strvalues = ('ddlCarModel,ddlpkgdays,ddlpkgkm,txtrate')
            return checkmandatory(strvalues);
        }		
		

    </script>
</head>
<body ms_positioning="GridLayout">
    <form id="Form1" method="post" runat="server">
    <uc1:Headerctrl ID="Headerctrl1" runat="server"></uc1:Headerctrl>
    <table id="Table1" align="center">
        <asp:Panel ID="pnlmainform" runat="server">
            <tbody>
                <tr>
                    <td align="center" colspan="2">
                        <strong><u>Edit a Self Drive Package</u></strong>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        &nbsp;&nbsp;
                    </td>
                </tr>
                <%--<tr>
                    <td>
                        * Car Category
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlcarcat" runat="server" CssClass="input">
                        </asp:DropDownList>
                    </td>
                </tr>--%>
                 <tr>
                    <td>
                        * Car Model
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlCarModel" runat="server" CssClass="input">
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td>
                        Tariff Type
                    </td>
                    <td>
                        <asp:DropDownList ID="ddTariffType" runat="server" CssClass="input">
                            <asp:ListItem Text="National" Value="N" />
                            <asp:ListItem Text="Special" Value="S" />
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        Pick-up City Name
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlcity" runat="server" CssClass="input">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        * Package Days
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlpkgdays" runat="server" CssClass="input">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        * Package KMs
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlpkgkm" runat="server" CssClass="input">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        * Rate (WeekDay)
                    </td>
                    <td>
                        <asp:TextBox ID="txtrate" runat="server" CssClass="input" MaxLength="50"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        * Rate (WeekEnd)
                    </td>
                    <td>
                        <asp:TextBox ID="txtrateWeekEnd" runat="server" CssClass="input" MaxLength="50"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        VAE
                    </td>
                    <td>
                        <asp:TextBox ID="txtVae" runat="server" CssClass="input" MaxLength="50"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        TP
                    </td>
                    <td>
                        <asp:TextBox ID="txtTp" runat="server" CssClass="input" MaxLength="50"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        PAC
                    </td>
                    <td>
                        <asp:TextBox ID="txtPac" runat="server" CssClass="input" MaxLength="50"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Rate / Extra Day
                    </td>
                    <td>
                        <asp:TextBox ID="txtrateextra" runat="server" CssClass="input" MaxLength="50"></asp:TextBox>%
                        Extra per day of Package Rate.
                    </td>
                </tr>
                <tr>
                    <td>
                        Rate / Extra KM
                    </td>
                    <td>
                        <asp:TextBox ID="txtextrakm" runat="server" CssClass="input" MaxLength="50"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Threshold Extra Day (Crosses to next package if reached)
                    </td>
                    <td>
                        <asp:TextBox ID="txtextraday" runat="server" CssClass="input" MaxLength="50"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Threshold Extra KM (Crosses to next package if reached)
                    </td>
                    <td>
                        <asp:TextBox ID="txtthreextrakm" runat="server" CssClass="input" MaxLength="50"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Deposit Amount
                    </td>
                    <td>
                        <asp:TextBox ID="txtdiscount" runat="server" CssClass="input" MaxLength="50"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        * Pakage Type:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlPkgType" runat="server">
                            <asp:ListItem Value=" " Text="Select package Type"></asp:ListItem>
                            <asp:ListItem Value="1" Text="Hourly"></asp:ListItem>
                            <asp:ListItem Value="2" Text="Daily"></asp:ListItem>
                            <asp:ListItem Value="3" Text="Weekly"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Monthly"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        Remarks(<span class="shwText" id="shwMessage">0</span>/2000 chars)
                    </td>
                    <td>
                        <asp:TextBox ID="txtRemarks" onkeydown="showLength(this.form.txtRemarks,'document.all.shwMessage',2000)"
                            onkeyup="showLength(this.form.txtRemarks,'document.all.shwMessage',2000)" runat="server"
                            CssClass="input" MaxLength="2000" TextMode="MultiLine" Columns="50" Rows="3"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Active
                    </td>
                    <td>
                        <asp:CheckBox ID="chkactive" runat="server" CssClass="input" Checked="True"></asp:CheckBox>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <asp:Button ID="btnsubmit" runat="server" CssClass="button" Text="Submit"></asp:Button>&nbsp;&nbsp;
                        <asp:Button ID="btnreset" runat="server" CssClass="button" Text="Reset" CausesValidation="False">
                        </asp:Button>
                    </td>
                </tr>
        </asp:Panel>
        <asp:Panel ID="pnlconfirmation" runat="server" Visible="False">
            <tr align="center">
                <td colspan="2">
                    <input type="hidden" name="txtRemarks">
                    <span class="shwText" id="shwMessage"></span>
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </td>
            </tr>
            <tr align="center">
                <td colspan="2">
                    <asp:HyperLink ID="hyplnkretry" runat="server"></asp:HyperLink>
                </td>
            </tr>
        </asp:Panel>
        </TBODY></table>
    </form>
</body>
</html>
