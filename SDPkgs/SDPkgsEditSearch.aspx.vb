Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class SDPkgsEditSearch
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtPkgID As System.Web.UI.WebControls.Textbox
    Protected WithEvents ddCatName As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddTariffType As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlcity As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlpkgdays As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlpkgkm As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddActive As System.Web.UI.WebControls.DropDownList
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnReset As System.Web.UI.WebControls.Button

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not Page.IsPostBack Then
            Dim dtrreader As SqlDataReader
            Dim accessdata As clsutility
            accessdata = New clsutility
            dtrreader = accessdata.funcGetSQLDataReader("select CarCatID ,CarCatName  from CORIntCarCatMaster  order by CarCatName")
            ddCatName.DataSource = dtrreader
            ddCatName.DataValueField = "CarCatID"
            ddCatName.DataTextField = "CarCatName"
            ddCatName.DataBind()
            ddCatName.Items.Insert(0, New ListItem("Any", -1))

            dtrreader = accessdata.funcGetSQLDataReader("select CityName ,CityID  from CORIntCityMaster where active=1 order by CityName")
            ddlcity.DataSource = dtrreader
            ddlcity.DataValueField = "CityID"
            ddlcity.DataTextField = "CityName"
            ddlcity.DataBind()
            ddlcity.Items.Insert(0, New ListItem("Any", -1))

            accessdata.funcpopulatenumddw(365, 0, ddlpkgdays)
            accessdata.funcpopulatenumddw(5000, 0, ddlpkgkm)
            dtrreader.Close()
        End If
    End Sub
    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Response.Redirect("SDPkgsEditResult.aspx?PkgID=" & txtPkgID.Text & "&CarCat=" & ddCatName.SelectedItem.Value & "&Tariff=" & ddTariffType.SelectedItem.Value & "&City=" & ddlcity.SelectedItem.Value & "&PkgDays=" & ddlpkgdays.SelectedItem.Value & "&PkgKMs=" & ddlpkgkm.SelectedItem.Value & "&Active=" & ddActive.SelectedItem.Value)
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("SDPkgsEditSearch.aspx")
    End Sub

End Class
