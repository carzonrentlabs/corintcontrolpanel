<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AddFreeDurationSelfDrive.aspx.vb"
    Inherits="SDPkgs_AddFreeDurationSelfDrive" %>

<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>CarzonRent :: Internal software Control panel</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">

    <script language="JavaScript" src="../utilityfunction.js"></script>

    <link href="../HertzInt.css" type="text/css" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server">
        <uc1:Headerctrl ID="Headerctrl1" runat="server"></uc1:Headerctrl>
        <div>
            <table align="center" >
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="text-align:center;">
                        <asp:Label ID="lblErrorMsg" runat="server" ForeColor="red"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:GridView ID="grv_FreeDuration" runat="server" AutoGenerateColumns="False" BackColor="#DEBA84"
                            BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" CellPadding="3" CellSpacing="2"
                            ShowFooter="true" AllowPaging="true" PageSize="100">
                            <Columns>
                                <asp:TemplateField HeaderText="ID">
                                    <ItemTemplate>
                                        <asp:Label ID="lblID" runat="server" Text='<%# Eval("ID")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:Label ID="lblID_Edit" runat="server" Text='<%# Eval("ID")%>'></asp:Label>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Package Type">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPackageType" runat="server" Text='<%# Eval("packagetype")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                    <asp:Label ID="lblPackageType_Edit" runat="server" Text='<%# Eval("packagetype")%>' Visible="false"></asp:Label>
                                        <asp:DropDownList ID="ddlPackageType_Edit" runat="server">
                                            <asp:ListItem Value="0" Text="Select Package type"></asp:ListItem>
                                            <asp:ListItem Value="1" Text="Hourly"></asp:ListItem>
                                            <asp:ListItem Value="2" Text="Daily"></asp:ListItem>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:DropDownList ID="ddlPackageType_Footer" runat="server">
                                            <asp:ListItem Value="0" Text="Select Package type"></asp:ListItem>
                                            <asp:ListItem Value="1" Text="Hourly"></asp:ListItem>
                                            <asp:ListItem Value="2" Text="Daily"></asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="rvf_ddlPackageType_Footer" ErrorMessage="*" runat="server"
                                            ControlToValidate="ddlPackageType_Footer" InitialValue="0" ValidationGroup="drodownlist"></asp:RequiredFieldValidator>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Total Duration">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTotalDuration" runat="server" Text='<%# Eval("totalDuration")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtTotalDuration_Edit" runat="server" Text='<%# Eval("totalDuration")%>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="txtTotalDuration_Footer" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rvf_txtTotalDuration_Footer" ErrorMessage="*" runat="server"
                                            ControlToValidate="txtTotalDuration_Footer" InitialValue="0" ValidationGroup="drodownlist"></asp:RequiredFieldValidator>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Free Duration">
                                    <ItemTemplate>
                                        <asp:Label ID="lblFreeDuration" runat="server" Text='<%# Eval("FreeDuration")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtFreeDuration_Edit" runat="server" Text='<%# Eval("FreeDuration")%>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="txtFreeDuration_Footer" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rvf_txtFreeDuration_Footer" ErrorMessage="*" runat="server"
                                            ControlToValidate="txtFreeDuration_Footer" ValidationGroup="drodownlist"></asp:RequiredFieldValidator>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Active">
                                    <ItemTemplate>
                                        <asp:Label ID="lblActive" runat="server" Text='<%# Eval("Active")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:CheckBox ID="chkActive_Edit" runat="server" />
                                        <asp:Label ID="lblActive_Edit" runat="server" Text='<%# Eval("Active")%>' Visible="false"></asp:Label>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:CheckBox ID="chkActive_Footer" runat="server" Checked="true" />
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnedit" runat="server" CommandName="Edit" Text="Edit" CausesValidation="false"></asp:LinkButton>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:LinkButton ID="btnupdate" runat="server" CommandName="Update" Text="Update"
                                            CausesValidation="false"></asp:LinkButton>
                                        <asp:LinkButton ID="btncancel" runat="server" CommandName="Cancel" Text="Cancel"
                                            CausesValidation="false"></asp:LinkButton>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:Button ID="btnAdd" runat="server" Text="Add" ValidationGroup="drodownlist" OnClick="AddFreeDuration"/>
                                    </FooterTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                            <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                            <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                            <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
