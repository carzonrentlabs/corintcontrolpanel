<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>

<%@ Page Language="VB" AutoEventWireup="false" CodeFile="MylesInterFace.aspx.vb"
    Inherits="MylesInterFace" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>CarzonRent :: Internal software Control panel</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../HertzInt.css" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css" />
    <script src="//code.jquery.com/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js" type="text/javascript"></script>
    <link rel="stylesheet" href="/resources/demos/style.css" />
    <script type="text/javascript" language="javascript">

        $(document).ready(function () {
            $("#<%=txtEffectiveDate.ClientID %>").datepicker();
        });

        function validate_EffectiveDate() {
            if (document.getElementById("<%=txtEffectiveDate.ClientID%>").value == "") {
                alert("Select Package Effective Date From.");
                document.getElementById("<%=txtEffectiveDate.ClientID %>").focus();
                return false;

            }
        }

        $(document).ready(function () {
            $("#<%=txtEffectiveDateTo.ClientID %>").datepicker();
        });

        function validate_EffectiveDate() {
            if (document.getElementById("<%=txtEffectiveDateTo.ClientID%>").value == "") {
                alert("Select Package Effective Date To.");
                document.getElementById("<%=txtEffectiveDateTo.ClientID %>").focus();
                return false;
            }
        }
    </script>
</head>
<body>
    <form id="Form1" method="post" runat="server">
    <uc1:Headerctrl ID="Headerctrl1" runat="server"></uc1:Headerctrl>
    <table align="center">
        <tbody>
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblMessage" runat="server" ForeColor="red"></asp:Label><br />
                    <asp:Label ID="lblPackageID" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="width: 100%;" align="left">
                    Effective From &nbsp;<asp:TextBox ID="txtEffectiveDate" runat="server" CssClass="input"></asp:TextBox>
                    &nbsp; Effective To &nbsp;<asp:TextBox ID="txtEffectiveDateTo" runat="server" CssClass="input"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="width: 100%;" align="left">
                    <asp:FileUpload ID="UploadPackage" runat="server" />
                    <asp:Button ID="btnUploadPkg" runat="server" Text="Upload Myles Rates" OnClientClick="return validate_EffectiveDate();" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center" style="width: 100%">
                    Below is sample Excel File Format and file name should be (MylesPackage.xlsx)
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center" style="width: 100%">
                    <table border="1">
                        <tr style="background-color: Yellow;">
                            <td>
                                CarModel
                            </td>
                            <td>
                                CityName
                            </td>
                            <td>
                                PackageKMs
                            </td>
                            <td>
                                Rate_Regular_days
                            </td>
                            <td>
                                Rate_Premium_days
                            </td>
                            <td>
                                Extra_Day_Rate_percentage
                            </td>
                            <td>
                                Extra_KM_Rate
                            </td>
                            <td>
                                Deposit_Amt
                            </td>
                            <td>
                                Pakage_Type
                            </td>
                            <td>
                                Package_Km_Limit_For_Hourly
                            </td>
                            <td>
                                Pkg_Max_Hour
                            </td>
                            <td>
                                Special_Packge_YN
                            </td>
                            <td>
                                Remarks
                            </td>
                            <td>
                                Active
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Beat
                            </td>
                            <td>
                                Delhi
                            </td>
                            <td>
                                10
                            </td>
                            <td>
                                1200
                            </td>
                            <td>
                                1500
                            </td>
                            <td>
                                30
                            </td>
                            <td>
                                10
                            </td>
                            <td>
                                50000
                            </td>
                            <td>
                                Daily
                            </td>
                            <td>
                                250
                            </td>
                            <td>
                                24
                            </td>
                            <td>
                                false
                            </td>
                            <td>
                                test package entered
                            </td>
                            <td>
                                true
                            </td>
                        </tr>
                        <tr>
                            <td>
                                City
                            </td>
                            <td>
                                Delhi
                            </td>
                            <td>
                                10
                            </td>
                            <td>
                                1200
                            </td>
                            <td>
                                1500
                            </td>
                            <td>
                                30
                            </td>
                            <td>
                                10
                            </td>
                            <td>
                                50000
                            </td>
                            <td>
                                Daily
                            </td>
                            <td>
                                250
                            </td>
                            <td>
                                24
                            </td>
                            <td>
                                false
                            </td>
                            <td>
                                test package entered
                            </td>
                            <td>
                                true
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    </form>
</body>
</html>
