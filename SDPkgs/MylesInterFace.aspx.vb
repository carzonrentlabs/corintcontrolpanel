Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Imports System.Data.Sql
Imports System.Data.OleDb
Imports System.IO
Partial Class MylesInterFace
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    End Sub

    Protected Sub btnUploadPkg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUploadPkg.Click
        Dim str As String
        str = Session("loggedin_user")
        Dim strFile As String
        Dim filename As String
        Dim Newfilename As String
        Dim Dt As DataTable = New DataTable()

        Dim di As DirectoryInfo = New DirectoryInfo("C:\MyDir")
        If di.Exists Then
            'MsgBox("That path exists already.")
        Else
            di.Create()
        End If

        If UploadPackage.PostedFile.FileName <> "" And UploadPackage.PostedFile.ContentLength > 0 Then
            strFile = Path.GetFileName(UploadPackage.FileName)
            filename = Path.GetFileName(UploadPackage.FileName)
            If File.Exists("C:\MyDir\" + filename) Then
                File.Delete("C:\MyDir\" + filename)
            End If
            If strFile <> "" And txtEffectiveDate.Text <> "" Then
                If filename = "MylesPackage.xlsx" Then
                    Newfilename = System.DateTime.Now.Year.ToString() + "_" + System.DateTime.Now.Month.ToString() + "_" + System.DateTime.Now.Day.ToString() + "_" + System.DateTime.Now.Hour.ToString() + "_" + System.DateTime.Now.Minute.ToString() + "_" + System.DateTime.Now.Second.ToString() + ".xlsx"

                    UploadPackage.SaveAs("C:\MyDir\" + Newfilename)

                    Dim connStringExcel As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\MyDir\" + Newfilename + ";Extended Properties=""Excel 12.0;HDR=YES;"""
                    Dim excelConn As New OleDbConnection(connStringExcel)
                    Dim excelCmd As New OleDbCommand("Select * From [MylesPackage$]", excelConn)

                    Try
                        excelConn.Open()
                        Dim excelReader As OleDbDataReader = excelCmd.ExecuteReader()
                        Dim Pkglist As New ArrayList
                        Dim Citylist As New ArrayList
                        Dim CarCatlist As New ArrayList
                        Dim intPkgidParam As SqlParameter
                        lblPackageID.Text = ""
                        Dim MyConnection As SqlConnection
                        MyConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("corConnectString"))
                        Try
                            MyConnection.Open()
                            While (excelReader.Read())
                                If IsNothing(excelReader("CityName")) Or IsDBNull(excelReader("CarModel")) Then
                                    lblMessage.Text = "Please check the Excel Sheet Data."
                                Else
                                    Dim cmd As SqlCommand
                                    Dim intPkgID As Int32

                                    cmd = New SqlCommand("Prc_Upload_MylesPkg", MyConnection)
                                    cmd.CommandType = CommandType.StoredProcedure
                                    cmd.CommandTimeout = 6000
                                    cmd.Parameters.AddWithValue("@CarModelName", excelReader("CarModel").ToString())
                                    cmd.Parameters.AddWithValue("@CityName", excelReader("CityName").ToString())
                                    cmd.Parameters.AddWithValue("@pkgkms", Convert.ToInt32(excelReader("PackageKMs")))
                                    cmd.Parameters.AddWithValue("@pkgrate", Convert.ToDouble(excelReader("Rate_Regular_days")))
                                    cmd.Parameters.AddWithValue("@pkgrateWeekEnd", Convert.ToDouble(excelReader("Rate_Premium_days")))
                                    cmd.Parameters.AddWithValue("@extradayrate", Convert.ToInt32(excelReader("Extra_Day_Rate_percentage")))
                                    cmd.Parameters.AddWithValue("@extrakmrate", Convert.ToInt32(excelReader("Extra_KM_Rate")))
                                    cmd.Parameters.AddWithValue("@depositamt", Convert.ToInt32(excelReader("Deposit_Amt")))
                                    cmd.Parameters.AddWithValue("@remarks", excelReader("Remarks").ToString())
                                    cmd.Parameters.AddWithValue("@active", Convert.ToBoolean(excelReader("Active")))
                                    cmd.Parameters.AddWithValue("@createdby", Convert.ToInt32(Session("loggedin_user")))
                                    cmd.Parameters.AddWithValue("@pkgType", excelReader("Pakage_Type").ToString())
                                    cmd.Parameters.AddWithValue("@pkgLimit", Convert.ToInt32(excelReader("Package_Km_Limit_For_Hourly")))
                                    cmd.Parameters.AddWithValue("@EffectiveFromDate", Convert.ToDateTime(txtEffectiveDate.Text.ToString()))
                                    cmd.Parameters.AddWithValue("@EffectiveToDate", Convert.ToDateTime(txtEffectiveDateTo.Text.ToString()))
                                    cmd.Parameters.AddWithValue("@pkgMaxHour", Convert.ToInt32(excelReader("Pkg_Max_Hour")))
                                    cmd.Parameters.AddWithValue("@SpecialPkg", Convert.ToBoolean(excelReader("Special_Packge_YN")))

                                    intPkgidParam = cmd.Parameters.Add("@Pkgid", SqlDbType.Int)
                                    intPkgidParam.Direction = ParameterDirection.Output
                                    cmd.ExecuteNonQuery()

                                    intPkgID = cmd.Parameters("@Pkgid").Value

                                    If (Convert.ToInt32(intPkgID) <> 0) Then
                                        If (lblPackageID.Text = "") Then
                                            lblPackageID.Text = "Package Id's ="
                                        End If
                                        If (Convert.ToInt32(intPkgID) = -1) Then
                                            lblPackageID.Text += "Incorrect Car Model Name,"
                                        End If
                                        If (Convert.ToInt32(intPkgID) = -2) Then
                                            lblPackageID.Text += "Incorrect City Name,"
                                        End If
                                        If (Convert.ToInt32(intPkgID) > 0) Then
                                            lblPackageID.Text += Convert.ToString(intPkgID) + ","
                                        End If
                                    End If
                                End If
                            End While
                            excelConn.Close()
                            excelConn.Dispose()

                            If File.Exists("C:\MyDir\" + Newfilename) Then
                                File.Delete("C:\MyDir\" + Newfilename)
                            End If
                            lblMessage.Text = "Packages Insert successfully!"
                        Catch exs As Exception
                            lblMessage.Text = exs.Message
                        Finally
                            MyConnection.Close()
                        End Try
                    Catch exo As Exception
                        lblMessage.Text = exo.Message
                        If File.Exists("C:\MyDir\" + Newfilename) Then
                            File.Delete("C:\MyDir\" + Newfilename)
                        End If
                    Finally

                    End Try
                Else
                    lblMessage.Text = "Please select package file!"
                End If
            End If

        Else
            lblMessage.Text = "Please Select File!"
        End If

    End Sub
End Class