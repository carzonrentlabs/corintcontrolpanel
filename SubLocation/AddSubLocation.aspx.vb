Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Partial Class SubLocation_AddSubLocation
    Inherits System.Web.UI.Page

    Protected Sub grv_Sublocation_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grv_Sublocation.PageIndexChanging
        grv_Sublocation.PageIndex = e.NewPageIndex
        BindData()
    End Sub

    Protected Sub grv_Sublocation_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles grv_Sublocation.RowCancelingEdit
        grv_Sublocation.EditIndex = -1
        BindData()
    End Sub

    Protected Sub grv_Sublocation_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles grv_Sublocation.RowUpdating
        Dim SqlConnection As SqlConnection
        Dim SqlDataAdapter As SqlDataAdapter = New SqlDataAdapter()
        Dim DataSet As DataSet = New DataSet()
        SqlConnection = New SqlConnection(ConfigurationManager.AppSettings("corConnectString"))
        Dim lblSubLocationID_Edit As Label = DirectCast(grv_Sublocation.Rows(e.RowIndex).FindControl("lblSubLocationID_Edit"), Label)
        Dim txtSublocationName As TextBox = DirectCast(grv_Sublocation.Rows(e.RowIndex).FindControl("txtSublocationName"), TextBox)
        Dim ddlCityName As DropDownList = DirectCast(grv_Sublocation.Rows(e.RowIndex).FindControl("ddlCityName"), DropDownList)
        Dim txtSublocationAddress As TextBox = DirectCast(grv_Sublocation.Rows(e.RowIndex).FindControl("txtSublocationAddress"), TextBox)
        Dim txtCostPercen As TextBox = DirectCast(grv_Sublocation.Rows(e.RowIndex).FindControl("txtCostPercen"), TextBox)
        Dim chkActive As CheckBox = DirectCast(grv_Sublocation.Rows(e.RowIndex).FindControl("chkActive"), CheckBox)
        Dim Active As Int32
        If chkActive.Checked = True Then
            Active = 1
        Else
            Active = 0
        End If
        Dim ddlTimingFromHr_Edit As DropDownList = DirectCast(grv_Sublocation.Rows(e.RowIndex).FindControl("ddlTimingFromHr"), DropDownList)
        Dim ddlTimingFromMin_Edit As DropDownList = DirectCast(grv_Sublocation.Rows(e.RowIndex).FindControl("ddlTimingFromMin"), DropDownList)
        Dim ddlTimingToHr_Edit As DropDownList = DirectCast(grv_Sublocation.Rows(e.RowIndex).FindControl("ddlTimingToHr"), DropDownList)
        Dim ddlTimingToMim_Edit As DropDownList = DirectCast(grv_Sublocation.Rows(e.RowIndex).FindControl("ddlTimingToMin"), DropDownList)
        Dim txtLat As TextBox = DirectCast(grv_Sublocation.Rows(e.RowIndex).FindControl("txtLat"), TextBox)
        Dim txtLon As TextBox = DirectCast(grv_Sublocation.Rows(e.RowIndex).FindControl("txtLon"), TextBox)
        Dim txtSharingPer As TextBox = DirectCast(grv_Sublocation.Rows(e.RowIndex).FindControl("txtSharingPer"), TextBox)
        Dim txtAssociateName As TextBox = DirectCast(grv_Sublocation.Rows(e.RowIndex).FindControl("txtAssociateName"), TextBox)
        Dim txtPanNo As TextBox = DirectCast(grv_Sublocation.Rows(e.RowIndex).FindControl("txtPanNo"), TextBox)
        Dim txtBankAccountNo As TextBox = DirectCast(grv_Sublocation.Rows(e.RowIndex).FindControl("txtBankAccountNo"), TextBox)
        Dim chkIsUpgradeYN As CheckBox = DirectCast(grv_Sublocation.Rows(e.RowIndex).FindControl("chkIsUpgradeYN"), CheckBox)
        Dim ChkUpgradeYN As Int32
        If chkIsUpgradeYN.Checked = True Then
            ChkUpgradeYN = 1
        Else
            ChkUpgradeYN = 0
        End If
        Dim SqlCommand As SqlCommand = New SqlCommand("Update CorintsublocationMaster set  SubLocationName='" & txtSublocationName.Text & "',CityID='" & ddlCityName.SelectedValue.ToString() & "',SubLocationAddress='" & txtSublocationAddress.Text & "',Active=" & Active & ",ModifyBy = " & Session("loggedin_user") & ",CostPercentage = " & txtCostPercen.Text & ",modifyDate ='" & DateTime.Now() & "', TimingFrom ='" & ddlTimingFromHr_Edit.SelectedValue.ToString() & ":" & ddlTimingFromMin_Edit.SelectedValue.ToString() & "',TimingTo = '" & ddlTimingToHr_Edit.SelectedValue.ToString() & ":" & ddlTimingToMim_Edit.SelectedValue.ToString() & "' , Lat='" & txtLat.Text & "',Lon='" & txtLon.Text & "',SharingPercentage=" & Convert.ToInt32(txtSharingPer.Text) & ",AssociatesName='" & txtAssociateName.Text & "' ,PANNO ='" & txtPanNo.Text & "' ,BankAccountNumber ='" & txtBankAccountNo.Text & "',IsUpgradeAllowYN=" & ChkUpgradeYN & " where SublocationId=" & lblSubLocationID_Edit.Text & "")
        SqlCommand.Connection = SqlConnection
        SqlCommand.Connection.Open()
        SqlCommand.ExecuteNonQuery()
        grv_Sublocation.EditIndex = -1
        BindData()
        SqlConnection.Close()
    End Sub

    Protected Sub grv_Sublocation_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grv_Sublocation.RowEditing
        grv_Sublocation.EditIndex = e.NewEditIndex
        BindData()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindData()
        End If

    End Sub

    Private Sub BindData()
        Dim SqlConnection As SqlConnection

        Dim SqlDataAdapter As SqlDataAdapter = New SqlDataAdapter()
        Dim DataSet As DataSet = New DataSet()
        SqlConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        SqlConnection.Open()
        Dim SqlCommand As SqlCommand = New SqlCommand("select Subloc.CityID,Subloc.SubLocationID ,Subloc.SubLocationName, CityMastr.CityName ,subloc.Active,Subloc.SubLocationAddress,CostPercentage ,Subloc.TimingFrom,Subloc.TimingTo,left(subloc.TimingFrom,2) as TimingFromHr,right(subloc.TimingFrom,2) as TimingFromMin ,left(subloc.TimingTo,2) as TimingToHr,right(subloc.TimingTo,2) as TimingToMin, Isnull(Subloc.Lat,0) as Lat ,Isnull(Subloc.Lon,0) as Lon,isnull(Subloc.SharingPercentage,0) as SharingPer,isnull(Subloc.AssociatesName,'') as AssociateName,isnull(Subloc.PANNO,'') as PanNo,isnull(Subloc.BankAccountNumber,'')as BankAccountNo,Subloc.IsUpgradeAllowYn as IsUpgradeYN from CorIntSubLocationMaster  Subloc inner join CORIntCityMaster as CityMastr on  Subloc.CityID=CityMastr.CityID where Subloc.Active=1 order by  CityMastr.CityName")

        SqlCommand.Connection = SqlConnection
        SqlDataAdapter = New SqlDataAdapter(SqlCommand)
        SqlDataAdapter.Fill(DataSet)

        grv_Sublocation.DataSource = DataSet
        grv_Sublocation.DataBind()
        SqlConnection.Close()
    End Sub


    Protected Sub grv_Sublocation_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grv_Sublocation.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                'If e.Row.RowState = DataControlRowState.Edit Then
                Dim ddlCity As DropDownList = e.Row.FindControl("ddlCityName") 'CType(e.Row.FindControl("ddlCityName"), DropDownList)

                Dim chkActive As CheckBox = e.Row.FindControl("chkActive")
                Dim CityID As Label = CType(e.Row.FindControl("lblCityID_Edit"), Label)
                Dim lblChkActive As Label = CType(e.Row.FindControl("lblActive_Edit"), Label)
                Dim chkIsUpgradeYN As CheckBox = e.Row.FindControl("chkIsUpgradeYN")
                Dim lblIsUpgradeYN As Label = CType(e.Row.FindControl("lblIsUpgradeYN_Edit"), Label)

                Dim ddlTimingFromHr As DropDownList = CType(e.Row.FindControl("ddlTimingFromHr"), DropDownList)
                Dim ddlTimingFromMin As DropDownList = CType(e.Row.FindControl("ddlTimingFromMin"), DropDownList)
                Dim ddlTimingToHr As DropDownList = CType(e.Row.FindControl("ddlTimingToHr"), DropDownList)
                Dim ddlTimingToMin As DropDownList = CType(e.Row.FindControl("ddlTimingToMin"), DropDownList)
                Dim lblTimingFromHR As Label = CType(e.Row.FindControl("lblTimingFromHR"), Label)
                Dim lblTimingFromMin As Label = CType(e.Row.FindControl("lblTimingFromMin"), Label)
                Dim lblTimingToHR As Label = CType(e.Row.FindControl("lblTimingToHR"), Label)
                Dim lblTimingToMin As Label = CType(e.Row.FindControl("lblTimingToMin"), Label)
                Dim SqlConnection As SqlConnection

                Dim SqlDataAdapter As SqlDataAdapter = New SqlDataAdapter()
                Dim DataSet As DataSet = New DataSet()
                SqlConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
                SqlConnection.Open()
                Dim SqlCommand As SqlCommand = New SqlCommand("select CityID,CityName from CorIntCityMaster where active=1")

                SqlCommand.Connection = SqlConnection
                SqlDataAdapter = New SqlDataAdapter(SqlCommand)
                SqlDataAdapter.Fill(DataSet)

                If ddlCity IsNot Nothing Then
                    ddlCity.DataSource = DataSet
                    ddlCity.DataTextField = "CityName"
                    ddlCity.DataValueField = "CityID"
                    ddlCity.DataBind()
                    ddlCity.Items.FindByValue(CityID.Text).Selected = True
                End If
                If chkActive IsNot Nothing Then
                    If lblChkActive.Text = "1" Then
                        chkActive.Checked = True
                    Else
                        chkActive.Checked = False
                    End If
                End If
                If chkIsUpgradeYN IsNot Nothing Then
                    If lblIsUpgradeYN.Text = True Then
                        chkIsUpgradeYN.Checked = True
                    Else
                        chkIsUpgradeYN.Checked = False
                    End If
                End If
                If ddlTimingFromHr IsNot Nothing Then
                    FillHr(ddlTimingFromHr, lblTimingFromHR.Text)
                End If
                If ddlTimingFromMin IsNot Nothing Then
                    FillMin(ddlTimingFromMin, lblTimingFromMin.Text)
                End If
                If ddlTimingFromHr IsNot Nothing Then
                    FillHr(ddlTimingToHr, lblTimingToHR.Text)
                End If
                If ddlTimingFromMin IsNot Nothing Then
                    FillMin(ddlTimingToMin,lblTimingToMin.Text)
                End If
            End If
            If e.Row.RowType = DataControlRowType.Footer Then
                Dim SqlConnection As SqlConnection

                Dim SqlDataAdapter As SqlDataAdapter = New SqlDataAdapter()
                Dim DataSet As DataSet = New DataSet()

                SqlConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
                SqlConnection.Open()
                Dim SqlCommand As SqlCommand = New SqlCommand("select CityID,CityName from CorIntCityMaster where active=1")

                SqlCommand.Connection = SqlConnection
                SqlDataAdapter = New SqlDataAdapter(SqlCommand)
                SqlDataAdapter.Fill(DataSet)
                Dim ddlCity_Edit As DropDownList = e.Row.FindControl("ddlCityName_Edit")
                Dim ddlTimingFromHr As DropDownList = CType(e.Row.FindControl("ddlTimingFromHr_Edit"), DropDownList)
                Dim ddlTimingFromMin As DropDownList = CType(e.Row.FindControl("ddlTimingFromMin_Edit"), DropDownList)
                Dim ddlTimingToHr As DropDownList = CType(e.Row.FindControl("ddlTimingToHr_Edit"), DropDownList)
                Dim ddlTimingToMin As DropDownList = CType(e.Row.FindControl("ddlTimingToMin_Edit"), DropDownList)
                If ddlCity_Edit IsNot Nothing Then
                    ddlCity_Edit.DataSource = DataSet
                    ddlCity_Edit.DataTextField = "CityName"
                    ddlCity_Edit.DataValueField = "CityID"
                    ddlCity_Edit.DataBind()
                    ddlCity_Edit.Items.Insert(0, "Select City")
                End If
                If ddlTimingFromHr IsNot Nothing Then
                    FillHr(ddlTimingFromHr, "00")
                End If
                If ddlTimingFromMin IsNot Nothing Then
                    FillMin(ddlTimingFromMin, "00")
                End If

                If ddlTimingToHr IsNot Nothing Then
                    FillHr(ddlTimingToHr, "00")
                End If
                If ddlTimingToMin IsNot Nothing Then
                    FillMin(ddlTimingToMin, "00")
                End If
            End If
        Catch ex As Exception
            Response.Write(ex.Message())
        End Try
    End Sub

    Protected Sub AddNewSublocation(ByVal sender As Object, ByVal e As EventArgs)
        Dim SqlConnection As SqlConnection
        Dim SublocationName As String = DirectCast(grv_Sublocation.FooterRow.FindControl("txtSublocationName_Edit"), TextBox).Text
        Dim CityID As String = DirectCast(grv_Sublocation.FooterRow.FindControl("ddlCityName_Edit"), DropDownList).SelectedValue.ToString()
        Dim SublocationAdderss As String = DirectCast(grv_Sublocation.FooterRow.FindControl("txtSublocationAddress_Edit"), TextBox).Text
        Dim costPerCentage As String = DirectCast(grv_Sublocation.FooterRow.FindControl("txtCostPercen_Edit"), TextBox).Text
        Dim chkActive As CheckBox = DirectCast(grv_Sublocation.FooterRow.FindControl("chkActive_Edit"), CheckBox)
        Dim ddlTimingFromHr_Edit As DropDownList = DirectCast(grv_Sublocation.FooterRow.FindControl("ddlTimingFromHr_Edit"), DropDownList)
        Dim ddlTimingFromMin_Edit As DropDownList = DirectCast(grv_Sublocation.FooterRow.FindControl("ddlTimingFromMin_Edit"), DropDownList)
        Dim ddlTimingToHr_Edit As DropDownList = DirectCast(grv_Sublocation.FooterRow.FindControl("ddlTimingToHr_Edit"), DropDownList)
        Dim ddlTimingToMin_Edit As DropDownList = DirectCast(grv_Sublocation.FooterRow.FindControl("ddlTimingToMin_Edit"), DropDownList)
        Dim txtLat_Edit As String = DirectCast(grv_Sublocation.FooterRow.FindControl("txtLat_Edit"), TextBox).Text
        Dim txtLon_Edit As String = DirectCast(grv_Sublocation.FooterRow.FindControl("txtLon_Edit"), TextBox).Text
        Dim chkIsUpgradeYN_Edit As CheckBox = DirectCast(grv_Sublocation.FooterRow.FindControl("chkIsUpgradeYN_Edit"), CheckBox)
        Dim txtSharingPer_Edit As String = DirectCast(grv_Sublocation.FooterRow.FindControl("txtSharingPer_Edit"), TextBox).Text
        Dim txtAssociateName_Edit As String = DirectCast(grv_Sublocation.FooterRow.FindControl("txtAssociateName_Edit"), TextBox).Text
        Dim txtPanNo_Edit As String = DirectCast(grv_Sublocation.FooterRow.FindControl("txtPanNo_Edit"), TextBox).Text
        Dim txtBankAccountNo_Edit As String = DirectCast(grv_Sublocation.FooterRow.FindControl("txtBankAccountNo_Edit"), TextBox).Text
        Dim Active As Integer
        Active = 0
        Dim IsUpgrade As Integer
        Active = 0
        If chkActive.Checked = True Then
            Active = 1
        Else
            Active = 0
        End If
        If chkIsUpgradeYN_Edit.Checked = True Then
            IsUpgrade = 1
        Else
            IsUpgrade = 0
        End If

        If SublocationName = "" Then
            Response.Write("Please select sublocation name.")
            Response.End()
        ElseIf CityID = "Select City" Then
            Response.Write("Please select city name.")
            Response.End()
        ElseIf SublocationAdderss = "" Then
            Response.Write("Please enter sublocation Address.")
            Response.End()
        ElseIf costPerCentage = "" Then
            Response.Write("Please enter sublocation cost.")
            Response.End()
        Else
        End If
        If IsNumeric(costPerCentage) = False Then
            Response.Write("Please enter only numeric value in sublocation cost.")
            Response.End()
        End If
        SqlConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As New SqlCommand()
        cmd.CommandType = CommandType.Text
        cmd.CommandText = "insert into corintsubLocationmaster(CityID, SubLocationName, SublocationAddress,createdBy,createDate,CostPercentage,Active,TimingFrom,TimingTo,Lat,Lon,IsUpgradeAllowYN,SharingPercentage,AssociatesName,PANNO,BankAccountNumber) values(@CityID, @SubLocationName, @SublocationAddress,@CreatedBy,@CreateDate,@CostPercentage,@Active,@TimingFrom,@TimingTo,@Lat,@Lon,@IsUpgradeYN,@SharingPer,@AssociatesName,@PanNo,@BankAccountNum)"
        cmd.Connection = SqlConnection
        cmd.Parameters.Add("@CityID", SqlDbType.Int).Value = Convert.ToInt32(CityID)
        cmd.Parameters.Add("@SubLocationName", SqlDbType.VarChar).Value = SublocationName
        cmd.Parameters.Add("@SublocationAddress", SqlDbType.VarChar).Value = SublocationAdderss
        cmd.Parameters.Add("@CreatedBy", SqlDbType.Int).Value = Session("loggedin_user")
        cmd.Parameters.Add("@CreateDate", SqlDbType.DateTime).Value = DateTime.Now
        cmd.Parameters.Add("@CostPercentage", SqlDbType.Int).Value = Convert.ToDouble(costPerCentage)
        cmd.Parameters.Add("@Active", SqlDbType.Int).Value = Active
        cmd.Parameters.Add("@TimingFrom", SqlDbType.VarChar).Value = ddlTimingFromHr_Edit.SelectedValue.ToString() & ":" & ddlTimingFromMin_Edit.SelectedValue.ToString()
        cmd.Parameters.Add("@TimingTo", SqlDbType.VarChar).Value = ddlTimingToHr_Edit.SelectedValue.ToString() & ":" & ddlTimingToMin_Edit.SelectedValue.ToString()
        cmd.Parameters.Add("@Lat", SqlDbType.Float).Value = Convert.ToDouble(txtLat_Edit)
        cmd.Parameters.Add("@Lon", SqlDbType.Float).Value = Convert.ToDouble(txtLon_Edit)
        cmd.Parameters.Add("@IsUpgradeYN", SqlDbType.Bit).Value = IsUpgrade
        cmd.Parameters.Add("@SharingPer", SqlDbType.Int).Value = Convert.ToInt32(txtSharingPer_Edit)
        cmd.Parameters.Add("@AssociatesName", SqlDbType.VarChar).Value = txtAssociateName_Edit
        cmd.Parameters.Add("@PanNo", SqlDbType.VarChar).Value = txtPanNo_Edit
        cmd.Parameters.Add("@BankAccountNum", SqlDbType.VarChar).Value = txtBankAccountNo_Edit
        cmd.Connection.Open()
        Dim status As Integer = cmd.ExecuteNonQuery()
        If status = 1 Then
            lblMessage.Text = "Sublocation inserted successfully."
        End If
        BindData()
    End Sub


    Private Function GetData(ByVal cmd As SqlCommand) As DataTable
        Dim strConnString As String = New String(ConfigurationManager.ConnectionStrings("corConnectString").ConnectionString)
        Using con As New SqlConnection(strConnString)
            Using sda As New SqlDataAdapter()
                cmd.Connection = con
                sda.SelectCommand = cmd
                Using dt As New DataTable()
                    sda.Fill(dt)
                    Return dt
                End Using
            End Using
        End Using
    End Function

    Private Sub FillHr(ByVal ddlTiming As DropDownList, ByVal Hr As String)
        Try
            Dim DTHr As DataTable = New DataTable("Hours")
            DTHr.Columns.Add(New DataColumn("HourVal", GetType(String)))
            DTHr.Columns.Add(New DataColumn("Hour", GetType(String)))
            DTHr.Rows.Add("00", "00")
            DTHr.Rows.Add("01", "01")
            DTHr.Rows.Add("02", "02")
            DTHr.Rows.Add("03", "03")
            DTHr.Rows.Add("04", "04")
            DTHr.Rows.Add("05", "05")
            DTHr.Rows.Add("06", "06")
            DTHr.Rows.Add("07", "07")
            DTHr.Rows.Add("08", "08")
            DTHr.Rows.Add("09", "09")
            DTHr.Rows.Add("10", "10")
            DTHr.Rows.Add("11", "11")
            DTHr.Rows.Add("12", "12")
            DTHr.Rows.Add("13", "13")
            DTHr.Rows.Add("14", "14")
            DTHr.Rows.Add("15", "15")
            DTHr.Rows.Add("16", "16")
            DTHr.Rows.Add("17", "17")
            DTHr.Rows.Add("18", "18")
            DTHr.Rows.Add("19", "19")
            DTHr.Rows.Add("20", "20")
            DTHr.Rows.Add("21", "21")
            DTHr.Rows.Add("22", "22")
            DTHr.Rows.Add("23", "23")
            ddlTiming.DataSource = DTHr
            ddlTiming.DataTextField = "Hour"
            ddlTiming.DataValueField = "HourVal"
            ddlTiming.DataBind()
            ddlTiming.Items.FindByValue(Hr).Selected = True
            Return
        Catch ex As Exception
            Return
            Throw
        End Try
    End Sub

    Private Sub FillMin(ByVal ddlTiming As DropDownList, ByVal Min As String)
        Try
          
            Dim timeHours As String
            For value As Integer = 0 To 59
                If value <= 9 Then
                    timeHours = "0" & value.ToString()
                Else
                    timeHours = value.ToString()
                End If
                ddlTiming.Items.Add(New ListItem(timeHours.ToString(), timeHours.ToString()))
            Next
            ddlTiming.Items.FindByValue(Min).Selected = True
            Return
        Catch ex As Exception
            Return
            Throw
        End Try
    End Sub

End Class
