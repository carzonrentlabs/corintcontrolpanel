<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AddSubLocation.aspx.vb"
    Inherits="SubLocation_AddSubLocation" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>CarzonRent :: Internal software Control Panel</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />

    <script language="JavaScript" src="../utilityfunction.js"></script>

    <link href="../HertzInt.css" type="text/css" rel="stylesheet" />

    <script language="JavaScript" type="text/javascript">
    
   
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <uc1:Headerctrl ID="Headerctrl1" runat="server"></uc1:Headerctrl>
        <div>
            <table align="center">
                <tr>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblMessage" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" HeaderText="Following error occurs:"
                        ShowMessageBox="false" DisplayMode="BulletList" ShowSummary="true" />
                    <td>
                        <asp:GridView ID="grv_Sublocation" runat="server" AutoGenerateColumns="False" BackColor="#DEBA84"
                            BorderColor="#DEBA84" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" CellSpacing="2"
                            ShowFooter="true" AllowPaging="true" PageSize="100">
                            <Columns>
                                <asp:TemplateField HeaderText="Sublocation ID" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSubLocationID" runat="server" Text='<%# Eval("SubLocationID")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:Label ID="lblSubLocationID_Edit" runat="server" Text='<%# Eval("SubLocationID")%>'></asp:Label>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="City ID" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCityID" runat="server" Text='<%# Eval("CityID")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:Label ID="lblCityID_Edit" runat="server" Text='<%# Eval("CityID")%>'></asp:Label>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Sublocation Name">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSublocationName" runat="server" Text='<%# Eval("SubLocationName")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtSublocationName" runat="server" Text='<%# Eval("SubLocationName")%>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="txtSublocationName_Edit" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rvf_txtSublocationName" ErrorMessage="*" runat="server"
                                            ControlToValidate="txtSublocationName_Edit" ValidationGroup="drodownlist"></asp:RequiredFieldValidator>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="City Name">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCityName" runat="server" Text='<%# Eval("CityName")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="ddlCityName" runat="server" Enabled="false">
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:DropDownList ID="ddlCityName_Edit" runat="server">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="rvf_ddlCityName" ErrorMessage="*" runat="server"
                                            ControlToValidate="ddlCityName_Edit" InitialValue="0" ValidationGroup="drodownlist"></asp:RequiredFieldValidator>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Address">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSublocationAddress" runat="server" Text='<%# Eval("SubLocationAddress")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtSublocationAddress" runat="server" Text='<%# Eval("SubLocationAddress")%>'
                                            Width="400"></asp:TextBox>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="txtSublocationAddress_Edit" runat="server" Width="400"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rvf_txtSublocationAddress" ErrorMessage="*" runat="server"
                                            ControlToValidate="txtSublocationAddress_Edit" ValidationGroup="drodownlist"></asp:RequiredFieldValidator>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Cost%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCostPercent" runat="server" Text='<%# Eval("Costpercentage")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtCostPercen" runat="server" Text='<%# Eval("Costpercentage")%>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="txtCostPercen_Edit" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rvf_txtCostPercent" ErrorMessage="*" runat="server"
                                            ControlToValidate="txtCostPercen_Edit" ValidationGroup="drodownlist"></asp:RequiredFieldValidator>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Active">
                                    <ItemTemplate>
                                        <asp:Label ID="lblActive" runat="server" Text='<%# Eval("Active")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:CheckBox ID="chkActive" runat="server" />
                                        <asp:Label ID="lblActive_Edit" runat="server" Text='<%# Eval("Active")%>'></asp:Label>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:CheckBox ID="chkActive_Edit" runat="server" />
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Timing From">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTimingFrom" runat="server" Text='<%# Eval("TimingFrom")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="ddlTimingFromHr" runat="server">
                                        </asp:DropDownList>
                                        <asp:DropDownList ID="ddlTimingFromMin" runat="server">
                                        </asp:DropDownList>
                                        <asp:Label ID="lblTimingFromHR" runat="server" Text='<%# Eval("TimingFromHr")%>' Visible="false" ></asp:Label>
                                        <asp:Label ID="lblTimingFromMin" runat="server" Text='<%# Eval("TimingFromMin")%>' Visible="false" ></asp:Label>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:DropDownList ID="ddlTimingFromHr_Edit" runat="server">
                                        </asp:DropDownList>
                                        <asp:DropDownList ID="ddlTimingFromMin_Edit" runat="server">
                                        </asp:DropDownList>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Timing To">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTimingTo" runat="server" Text='<%# Eval("TimingTo")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="ddlTimingToHr" runat="server">
                                        </asp:DropDownList>
                                        <asp:DropDownList ID="ddlTimingToMin" runat="server">
                                        </asp:DropDownList>
                                        <asp:Label ID="lblTimingToHR" runat="server" Text='<%# Eval("TimingToHr")%>' Visible="false"></asp:Label>
                                        <asp:Label ID="lblTimingToMin" runat="server" Text='<%# Eval("TimingToMin")%>' Visible="false"></asp:Label>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:DropDownList ID="ddlTimingToHr_Edit" runat="server">
                                        </asp:DropDownList>
                                        <asp:DropDownList ID="ddlTimingToMin_Edit" runat="server">
                                        </asp:DropDownList>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                
                                
                                 <asp:TemplateField HeaderText="Lat">
                                    <ItemTemplate>
                                        <asp:Label ID="lblLat" runat="server" Text='<%# Eval("Lat")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtLat" runat="server" Text='<%# Eval("Lat")%>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="txtLat_Edit" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rvf_txtLat" ErrorMessage="*" runat="server"
                                            ControlToValidate="txtLat_Edit" ValidationGroup="drodownlist"></asp:RequiredFieldValidator>
                                    </FooterTemplate>
                                </asp:TemplateField> 
                                <asp:TemplateField HeaderText="Lon">
                                    <ItemTemplate>
                                        <asp:Label ID="lblLon" runat="server" Text='<%# Eval("Lon")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtLon" runat="server" Text='<%# Eval("Lon")%>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="txtLon_Edit" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rvf_txtLon" ErrorMessage="*" runat="server"
                                            ControlToValidate="txtLon_Edit" ValidationGroup="drodownlist"></asp:RequiredFieldValidator>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                
                                <asp:TemplateField HeaderText="Sharing Percentage">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSharingPer" runat="server" Text='<%# Eval("SharingPer")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtSharingPer" runat="server" Text='<%# Eval("SharingPer")%>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="txtSharingPer_Edit" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rvf_txtSharingPer" ErrorMessage="*" runat="server"
                                            ControlToValidate="txtSharingPer_Edit" ValidationGroup="drodownlist"></asp:RequiredFieldValidator>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                
                                <asp:TemplateField HeaderText="Associates Name">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAssociateName" runat="server" Text='<%# Eval("AssociateName")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtAssociateName" runat="server" Text='<%# Eval("AssociateName")%>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="txtAssociateName_Edit" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rvf_txtAssociateName" ErrorMessage="*" runat="server"
                                            ControlToValidate="txtAssociateName_Edit" ValidationGroup="drodownlist"></asp:RequiredFieldValidator>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                
                                
                                <asp:TemplateField HeaderText="Pan No">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPanNo" runat="server" Text='<%# Eval("PanNo")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtPanNo" runat="server" Text='<%# Eval("PanNo")%>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="txtPanNo_Edit" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rvf_txtPanNo" ErrorMessage="*" runat="server"
                                            ControlToValidate="txtPanNo_Edit" ValidationGroup="drodownlist"></asp:RequiredFieldValidator>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                
                                
                                <asp:TemplateField HeaderText="Bank Account Number">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBankAccountNo" runat="server" Text='<%# Eval("BankAccountNo")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtBankAccountNo" runat="server" Text='<%# Eval("BankAccountNo")%>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="txtBankAccountNo_Edit" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rvf_txtBankAccountNo" ErrorMessage="*" runat="server"
                                            ControlToValidate="txtBankAccountNo_Edit" ValidationGroup="drodownlist"></asp:RequiredFieldValidator>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                
                                  <asp:TemplateField HeaderText="Is Upgrade AllowedYN">
                                    <ItemTemplate>
                                        <asp:Label ID="lblIsUpgradeYN" runat="server" Text='<%# Eval("IsUpgradeYN")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:CheckBox ID="chkIsUpgradeYN" runat="server" />
                                        <asp:Label ID="lblIsUpgradeYN_Edit" runat="server" Text='<%# Eval("IsUpgradeYN")%>'></asp:Label>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:CheckBox ID="chkIsUpgradeYN_Edit" runat="server" />
                                    </FooterTemplate>
                                </asp:TemplateField>
                                
                                
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnedit" runat="server" CommandName="Edit" Text="Edit" CausesValidation="false"></asp:LinkButton>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:LinkButton ID="btnupdate" runat="server" CommandName="Update" Text="Update"
                                            CausesValidation="false"></asp:LinkButton>
                                        <asp:LinkButton ID="btncancel" runat="server" CommandName="Cancel" Text="Cancel"
                                            CausesValidation="false"></asp:LinkButton>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:Button ID="btnAdd" runat="server" Text="Add" OnClick="AddNewSublocation" ValidationGroup="drodownlist" />
                                    </FooterTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                            <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                            <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                            <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
