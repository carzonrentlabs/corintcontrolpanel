Imports commonutility
Imports System.Data
Imports System.text
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class ServiceMasterEdit
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object
    Protected WithEvents lblServiceID As System.Web.UI.WebControls.Label
    Protected WithEvents desc As System.Web.UI.WebControls.TextBox
    Protected WithEvents chk_Active As System.Web.UI.WebControls.CheckBox
    Protected WithEvents btnsubmit As System.Web.UI.WebControls.Button
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel



    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btnsubmit.Attributes("onClick") = "return validation();"
        If Trim("" & Request.QueryString("ID")) <> "" Then
            Dim intID As Integer
            intID = Request.QueryString("ID")
            If Not IsPostBack() Then
                getvalue(intID)
            End If
        Else
            Response.Write("Bad request.")
            Response.End()
        End If
    End Sub
    Sub getvalue(ByVal intID As Integer)
        Dim strquery As String
        Dim str As StringBuilder
        Dim dtrreader As SqlDataReader
        Dim accessdata As clsutility
        
        accessdata = New clsutility

        strquery = "Exec Proc_serviceMaster '" & intID & "',2"

        dtrreader = accessdata.funcGetSQLDataReader(strquery.ToString)
        'If dtrreader.HasRows Then
        While dtrreader.Read

            lblServiceID.Text = dtrreader("ServiceID")
            desc.Text = dtrreader("Description")
            If dtrreader("Active") Then
                chk_Active.Checked = True
            Else
                chk_Active.Checked = False
            End If
        End While
        'End If
    End Sub

    Private Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        Dim Flag As Integer

        'If Trim("" & descp.Text) = "" Then
        'lblErrorMsg.Visible = True
        'hyplnkretry.Text = "Please enter the description"
        'response.write("Please enter the description")
        'Response.End()
        'End If

        cmd = New SqlCommand("Proc_EditServiceMaster", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@ID", Request.QueryString("ID"))
        cmd.Parameters.Add("@Description", desc.Text)
        cmd.Parameters.Add("@Active", get_YNvalue(chk_Active))
        cmd.Parameters.Add("@CreatedBy", Session("loggedin_user"))

        MyConnection.Open()
        Flag = cmd.ExecuteNonQuery()

        MyConnection.Close()
        lblErrorMsg.Visible = True

        If Trim("" & Flag) = "0" Then

            pnlmainform.Visible = False
            pnlconfirmation.Visible = True

            lblErrorMsg.Text = "Unable to edit Service Master!<br> The ID is <b>" & Request.QueryString("ID") & "</b>"
            hyplnkretry.Text = "Edit another Service Master"
            hyplnkretry.NavigateUrl = "ServiceMasterSearch.aspx"
        Else

            pnlmainform.Visible = False
            pnlconfirmation.Visible = True

            lblErrorMsg.Text = "You have edited the Service Master<br> The ID is <b>" & Request.QueryString("ID") & "</b>"
            hyplnkretry.Text = "Edit another Service Master"
            hyplnkretry.NavigateUrl = "ServiceMasterSearch.aspx"
        End If
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function
End Class
