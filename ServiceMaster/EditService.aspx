<%@ Page Language="VB" AutoEventWireup="false" CodeFile="EditService.aspx.vb" Inherits="ServiceMaster_EditService" %>

<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<HTML>
	<HEAD>
		<title>CarzonRent :: Internal software Control panel</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="JavaScript" src="../utilityfunction.js"></script>
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
		<script language="javascript">
		function validation()
		{
	
			var strvalues
			strvalues=('TxtDescription,ddlCharges,ddlMandatory')
			return checkmandatory(strvalues);		
		}		
		

		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<uc1:Headerctrl id="Headerctrl1" runat="server"></uc1:Headerctrl>
			<TABLE id="Table1" align="center">
				<asp:panel id="pnlmainform" Runat="server">
					<TBODY>
						<TR>
							<TD align="center" colSpan="3"><STRONG><U>Edit Self Drive Services </U></STRONG>
							</TD>
						</TR>
							<TR>
							<TD align="center" colSpan="3">&nbsp;&nbsp;</TD>
						</TR>
						<TR>
						<TD>* Service Id</TD>
							<td style="width:30px;"></td>
							<TD>
								<asp:DropDownList id="ddlServiceId" runat="server" cssclass="input">
								</asp:DropDownList>
								<asp:button id="btnSearch" runat="server" CssClass="button" Text="Search"></asp:button>
							</TD>
						</TR>
						<TR>
							<TD align="center" colSpan="3">&nbsp;&nbsp;</TD>
						</TR>
						<TR>
							<TD>* Description
							</TD>
							<td style="width:30px;"></td>
							<TD>
								<asp:textbox id="TxtDescription" runat="server" CssClass="input" MaxLength="50"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>* MandatoryYN</TD>
							<td style="width:30px;"></td>
							<TD>
								<asp:DropDownList id="ddlMandatory" runat="server" cssclass="input">
									<asp:listitem Text="0" Value="0" />
									<asp:listitem Text="1" Value="1" />
								</asp:DropDownList></TD>
						</TR>
						<TR>
							<TD>* Charges Type
							</TD>
							<td style="width:30px;"></td>
							<TD>
								<asp:DropDownList id="ddlCharges" runat="server" CssClass="input">
									<asp:listitem Text="Daily" Value="Daily" />
									<asp:listitem Text="One Time" Value="One Time" />
									<asp:listitem Text="Monthly" Value="Monthly" />
									<asp:listitem Text="Weekly" Value="Weekly" />
								</asp:DropDownList></TD>
						</TR>
						
						<TR>
							<TD>* Active</TD>
							<td style="width:30px;"></td>
							<TD>
								<asp:checkbox id="chkactive" runat="server" CssClass="input" Checked="True"></asp:checkbox></TD>
						</TR>
						<TR>
							<TD align="center" colSpan="3">
								<asp:button id="btnsubmit" runat="server" CssClass="button" Text="Submit"></asp:button>&nbsp;&nbsp;
								<asp:button id="btnreset" runat="server" CssClass="button" Text="Reset" CausesValidation="False"></asp:button></TD>
						</TR>
				</asp:panel><asp:panel id="pnlconfirmation" Runat="server" Visible="False">
					<TR align="center">
						<TD colSpan="3">
							<asp:Label id="lblMessage" runat="server"></asp:Label></TD>
					</TR>
					<TR align="center">
						<TD colSpan="3">
							<asp:HyperLink id="hyplnkretry" runat="server"></asp:HyperLink></TD>
					</TR>
				</asp:panel></TBODY></TABLE>
		</form>
	</body>
</HTML>