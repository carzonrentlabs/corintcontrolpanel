Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Partial Class ServiceMaster_EditService
    Inherits System.Web.UI.Page
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        btnsubmit.Attributes("onClick") = "return validation();"
        If Not Page.IsPostBack Then
            Dim dtrreader As SqlDataReader
            Dim accessdata As clsutility
            accessdata = New clsutility
            dtrreader = accessdata.funcGetSQLDataReader("select ServiceID,Description from CorIntServicesMaster order by description")
            ddlServiceId.DataSource = dtrreader
            ddlServiceId.DataValueField = "ServiceID"
            ddlServiceId.DataTextField = "Description"
            ddlServiceId.DataBind()
            ddlServiceId.Items.Insert(0, New ListItem("Any", -1))
            dtrreader.Close()
        End If
    End Sub
    Private Sub btnsubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        cmd = New SqlCommand("prc_EditServiceMasterSelfDrive", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("@Desccription", TxtDescription.Text)
        cmd.Parameters.Add("@Active", get_YNvalue(chkactive))
        cmd.Parameters.Add("@Required", ddlMandatory.SelectedItem.Value)
        cmd.Parameters.Add("@Charges", ddlCharges.SelectedItem.Value)
        cmd.Parameters.Add("@modifyBy", Session("loggedin_user"))
        cmd.Parameters.Add("@ServiceId", ddlServiceId.SelectedItem.Value)

        MyConnection.Open()
        cmd.ExecuteNonQuery()

        '  Try
        'MyConnection.Open()
        cmd.ExecuteNonQuery()
        MyConnection.Close()
        pnlmainform.Visible = False
        pnlconfirmation.Visible = True

        lblMessage.Text = "You have updated the Self Drive Services successfully"
        hyplnkretry.Text = "Edit another Self Drive Service"
        hyplnkretry.NavigateUrl = "EditService.aspx"

    End Sub

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim dtrreader As SqlDataReader
        Dim accessdata As clsutility
        accessdata = New clsutility
        dtrreader = accessdata.funcGetSQLDataReader("select * from CorIntServicesMaster where serviceID=" + ddlServiceId.SelectedValue.ToString() + " order by description")
        dtrreader.Read()

        TxtDescription.Text = dtrreader("Description") & ""
        'ddlMandatory.Items.FindByValue(dtrreader("Required")).Selected = True
        'ddlCharges.Items.FindByValue(dtrreader("Charges")).Selected = True
        ddlMandatory.SelectedValue = dtrreader("Required") & ""
        ddlCharges.SelectedValue = dtrreader("charges") & ""
        chkactive.Checked = dtrreader("active")
        dtrreader.Close()
        accessdata.Dispose()


    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function
End Class
