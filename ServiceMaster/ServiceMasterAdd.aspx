<%@ Page Language="vb" AutoEventWireup="false" Src="ServiceMasterAdd.aspx.vb" Codebehind="ServiceMasterAdd.aspx.vb" Inherits="ServiceMasterAdd"%>
<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//Dtd HTML 4.0 transitional//EN">

<script runat="server">

Protected Sub Headerctrl1_Load(ByVal sender As Object, ByVal e As System.EventArgs)

End Sub

Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)

End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>CarzonRent :: Internal software Control panel</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="JavaScript" src="../utilityfunction.js"></script>
		<script language="JavaScript" src="../JScripts/Datefunc.js"></script>
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
		<script language="javascript">
		function validation()
		{
			var strdesc = document.getElementById('desc').value;
			strdesc = strdesc.replace(/^\s+/,""); 	//Removes Left Blank Spaces
			strdesc = strdesc.replace(/\s+$/,""); 	//Removes Right Blank Spaces
			if (strdesc == "")
			{
				alert("Please enter the Description");
				document.getElementById('desc').focus();
				return false;	
			}
			return true;
		}
		</script>
	</head>
	 <body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<uc1:Headerctrl id="Headerctrl1" runat="server" OnLoad="Headerctrl1_Load"></uc1:Headerctrl>
			<table id="Table1" align="center">
				<asp:panel id="pnlmainform" Runat="server">
					<tbody>
						<tr>
							<td align="center" colspan="2"><strong><u>Add a Service Master</u></strong></td>
						</tr>
						
						<tr>
							<td align="center" colspan="2">&nbsp;&nbsp;</td>
						</tr>

				

						<tr>
							<td>* Description </td>
							<td><asp:textbox id="desc" runat="server" CssClass="input" MaxLength="200" TextMode="SingleLine" ></asp:textbox></td>
						</tr>
						
						<tr>
							<td>* Active</td>
							<td><asp:checkbox id="chkactive" runat="server" CssClass="input" Checked="true"></asp:checkbox></td>
						</tr>
						<TR>
							<TD>* MandatoryYN</TD>
							
							<TD>
								<asp:DropDownList id="ddlMandatory" runat="server" cssclass="input">
									<asp:listitem Text="0" Value="0" />
									<asp:listitem Text="1" Value="1" />
								</asp:DropDownList></TD>
						</TR>
						<TR>
							<TD>* Charges Type
							</TD>
				
							<TD>
								<asp:DropDownList id="ddlCharges" runat="server" CssClass="input">
									<asp:listitem Text="Daily" Value="Daily" />
									<asp:listitem Text="One Time" Value="One Time" />
									<asp:listitem Text="Monthly" Value="Monthly" />
									<asp:listitem Text="Weekly" Value="Weekly" />
								</asp:DropDownList></TD>
						</TR>

						<tr>
							<td align="center" colspan="2">
				<asp:button id="btnsubmit" runat="server" CssClass="button" Text="Submit"></asp:button>&nbsp;&nbsp;
				<asp:button id="btnreset" runat="server" CssClass="button" Text="Reset" CausesValidation="False"></asp:button></td>
						</tr>
						
				</tbody></asp:panel>
				<asp:panel id="pnlconfirmation" Runat="server" Visible="False">
					<tr align="center">
					<td colspan="2"><asp:Label id="lblErrorMsg" runat="server"></asp:Label></td>
					</tr>
					<tr align="center">
					<td colspan="2"><asp:HyperLink id="hyplnkretry" runat="server"></asp:HyperLink></td>
					</tr>
		
				</asp:panel></table>
		</form>
	</body>
</html>