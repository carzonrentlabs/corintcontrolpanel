<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ServiceMasterEdit.aspx.vb" Inherits="ServiceMasterEdit" Src="ServiceMasterEdit.aspx.vb" %>
<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>CarzonRent :: Internal software Control panel</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="JavaScript" src="../utilityfunction.js"></script>
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
		<script language="JavaScript">
		function validation()
		{
			//if(trim(document.getElementById('desc').value) == "" )
			//if(document.form1.desc.value=="")
			//if (document.form[0].desc.value == "" ) 				
			//var strSearchGuest = document.form1.searchGuest.value;
			var strdesc = document.getElementById('desc').value;
			strdesc = strdesc.replace(/^\s+/,""); //Removes Left Blank Spaces
			strdesc = strdesc.replace(/\s+$/,""); //Removes Right Blank Spaces
			//if (document.getElementById('descp').value == "")
			if (strdesc == "")
			{
				alert("Please enter the Description");
				document.getElementById('desc').focus();
				return false;	
			}
			return true;	
		}		
		</script>
	</head>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<uc1:Headerctrl id="Headerctrl1" runat="server"></uc1:Headerctrl>
			<table id="Table1" align="center">
				<asp:panel id="pnlmainform" Runat="server">
					<tbody>
						<tr>
							<td align="center" colSpan="2"><strong><u>Edit a Service Master</u></strong></td>
						</tr>
						<tr>
							<td align="center" colSpan="2">&nbsp;&nbsp;</td>
						</tr>
						
						<tr>
							<td>Service ID </td>
							<td><asp:Label ID="lblServiceID" Runat="server"></asp:Label></td>
						</tr>
					
						
						<tr>
							<td>* Description</td>
							<td><asp:textbox id="desc"  runat="server" value=""></asp:textbox></td>
						</tr>
						
						<tr>
							<td>Active</td>
							<td><asp:checkbox id="chk_active" runat="server" CssClass="input" Checked="True"></asp:checkbox></td>
						</tr>


						<tr>
							<td align="center" colSpan="2">
			<asp:button id="btnsubmit" runat="server"  CssClass="button" Text="Submit"></asp:button>&nbsp;&nbsp;
			<asp:button id="btnreset" runat="server"  CssClass="button" Text="Reset" CausesValidation="False"></asp:button></td>
						</tr>
				</tbody></asp:panel>
				<asp:panel id="pnlconfirmation" Runat="server" Visible="False">
					<tr align="center">
						<td colspan="2">
							<asp:Label id="lblErrorMsg" runat="server"></asp:Label></td>
					</tr>
					<tr align="center">
						<td  colspan="2">
							<asp:HyperLink id="hyplnkretry" runat="server"></asp:HyperLink></td>
					</tr>

				</asp:panel></table>

		</form>
	</body>
</html>
