/******************************/
/*Library of functions
/*File: cfunc.js
/******************************/

  Date.prototype.getActualMonth = getActualMonth;

  Date.prototype.getActualDay = getActualDay;

  Date.prototype.getCalendarDay = getCalendarDay;

  Date.prototype.getCalendarMonth = getCalendarMonth;

// Rajesh function /////////
// Removes leading whitespaces
function LTrim( value ) {
        
        var re = /\s*((\S+\s*)*)/;
        return value.replace(re, "$1");
        
}
 
// Removes ending whitespaces
function RTrim( value ) {
        
        var re = /((\s*\S+)*)\s*/;
        return value.replace(re, "$1");
        
}
 
// Removes leading and ending whitespaces
function trim( value ) {
        
        return LTrim(RTrim(value));
        
}


function echeck(str) {

		var at="@"
		var dot="."
		var lat=str.indexOf(at)
		var lstr=str.length
		var ldot=str.indexOf(dot)
		if (str.indexOf(at)==-1){
		   alert("Invalid E-mail ID")
		   return false
		}

		if (str.indexOf(at)==-1 || str.indexOf(at)==0 || str.indexOf(at)==lstr){
		   alert("Invalid E-mail ID")
		   return false
		}

		if (str.indexOf(dot)==-1 || str.indexOf(dot)==0 || str.indexOf(dot)==lstr){
		    alert("Invalid E-mail ID")
		    return false
		}

		 if (str.indexOf(at,(lat+1))!=-1){
		    alert("Invalid E-mail ID")
		    return false
		 }

		 if (str.substring(lat-1,lat)==dot || str.substring(lat+1,lat+2)==dot){
		    alert("Invalid E-mail ID")
		    return false
		 }

		 if (str.indexOf(dot,(lat+2))==-1){
		    alert("Invalid E-mail ID")
		    return false
		 }
		
		 if (str.indexOf(" ")!=-1){
		    alert("Invalid E-mail ID")
		    return false
		 }

 		 return true					
	}

//////////////////////////////

  function getActualMonth() {

    var n = this.getMonth();

    n += 1;

    return n;

  }



  function getActualDay() {

    var n = this.getDay();

    n += 1;

    return n;

  }



  function getCalendarDay() {

    var n = this.getDay();

    var dow = new Array(7);

    dow[0] = "Sunday";

    dow[1] = "Monday";

    dow[2] = "Tuesday";

    dow[3] = "Wednesday";

    dow[4] = "Thursday";

    dow[5] = "Friday";

    dow[6] = "Saturday";

    return dow[n];

  }



  function getCalendarMonth() {

    var n = this.getMonth();

    var moy = new Array(12);

    moy[0] = "January";

    moy[1] = "February";

    moy[2] = "March";

    moy[3] = "April";

    moy[4] = "May";

    moy[5] = "June";

    moy[6] = "July";

    moy[7] = "August";

    moy[8] = "September";

    moy[9] = "October";

    moy[10] = "November";

    moy[11] = "December";

    return moy[n];

  }


function numToString(number)
{
	//**********************************************************************
	//Language: JavaScript
	//Written by: unknown
	//Date: Unknown
	//----------
	//Purpose:
	//Function to convert number to string
	//----------
	//Arguments:
	//1) number (number) - number to be converted to string
	//Return type: string object
	//************************************************************************
	number += "";
	return number;
}
	

function lTrim(objString)
{
	//**********************************************************************
	//Language: JavaScript
	//Written by: unknown
	//Date: Unknown
	//----------
	//Purpose:
	//Function to trim left spaces
	//----------
	//Arguments:
	//1) objString (string object) - string whose left spaces have to be trimmed
	//Return type: string object
	//************************************************************************

	var temp="";
	temp=objString;
	var len=objString.length;
	if(len>0)
	{
		for(i=0;i<len;i++)
		{
			if(temp.charAt(i)!=" ")
				break;
		}
		
		tempStr=""
		k=0;
		for(j=i;j<len;j++)
		{
			tempStr=tempStr+temp.charAt(j);
			k=k+1;
		}
		return tempStr;
	}
	else
		return ("");
}


function isNumber(obj,displayName)
{
	//**********************************************************************
	//Language: JavaScript
	//Written by: unknown
	//Date: Unknown
	//----------
	//Purpose:
	//Function to check whether object has only digits in its value
	//----------
	//Arguments:
	//1) obj (object) - the object whose value for digits has to be tested
	//2) displayName (String) - Part of the alert message to be displayed
	//Return type: boolean
	//Returns: 	true: if the value is all numeric
	//			false: if the value is not all numeric, displays a message
	//************************************************************************

	var str=lTrim(obj.value);
	for(i=0;i<str.length;i++)
	{
		if(str.charAt(i)<'0'||str.charAt(i)>'9')
		{	
			if((str.charAt(0))=="-")
			{
				i=i+1;
					continue;
			}
			alert(displayName+" should be numeric.");
			obj.focus();
			return false;
		}
	}
	return true;
} 


function isNonNumber(obj,displayName)
{
	//**********************************************************************
	//Language: JavaScript
	//Written by: unknown
	//Date: Unknown
	//----------
	//Purpose:
	//Function to check whether object does not have only digits in its value
	//----------
	//Arguments:
	//1) obj (object) - the object whose value for non-digits has to be tested
	//2) displayName (String) - Part of the alert message to be displayed
	//Return type: boolean
	//Returns: 	true: if the value is not all numeric
	//			false: if the value is all numeric, displays a message
	//************************************************************************

	var str=lTrim(obj.value);
	var flag=false;
	if(str.length>0)
	{
		for(i=0;i<str.length;i++)
		{	
			if((str.charAt(0))=="-")
  			{
  				i=i+1;
  				continue;
  			}
  			if(str.charAt(i)>'0'||str.charAt(i)>'9')
  			{
  				flag=true;	
  			}
  		}
  		if(flag)
  			return true;
  		else
  		{
  			alert(displayName+" should be non-numeric.");
  			obj.focus();
  			return false;
  		}
	}
	else
		return true;
	return flag;
}


function isEmail (obj,displayName) 
{ 
	//**********************************************************************
	//Language: JavaScript
	//Written by: unknown
	//Date: Unknown
	//----------
	//Purpose:
	//Function to check whether object contains the valid email address
	//----------
	//Arguments:
	//1) obj (object) - the object whose value for validity of email to be tested
	//2) displayName (String) - Part of the alert message to be displayed
	//Return type: boolean
	//Returns: 	true: if the value contains valid email address
	//			false: if the value does not contain valid email addreee, displays a message
	//************************************************************************

	var theStr=obj.value;
	var atIndex = theStr.indexOf('@'); 
	var dotIndex = theStr.indexOf('.', atIndex); 
	var flag = true; 
	theSub = theStr.substring(0, dotIndex+1) 

	if ((atIndex < 1)||(atIndex != theStr.lastIndexOf('@'))||(dotIndex < atIndex + 2)||(theStr.length <= theSub.length)) 
	{ 
		alert(displayName+" is not a valid Email ID");
		obj.focus();
		flag = false; 
	} 
	else { flag = true; } 

	return(flag); 
}


function isRecEmail (obj,displayName) 
{ 
	//**********************************************************************
	//Language: JavaScript
	//Written by: Shafqat Bashir
	//Date: 29-Sep-00
	//----------
	//Purpose:
	//Function to check whether object contains the valid recruiter's email address
	//----------
	//Arguments:
	//1) obj (object) - the object whose value for validity of recruiter's email to be tested
	//2) displayName (String) - Part of the alert message to be displayed
	//Return type: boolean
	//Returns: 	true: if the value contains valid recruiter's email address
	//			false: if the value does not contain valid recruiter's email addreee, displays a message
	//************************************************************************

	var theStr=obj.value;
	var atIndex = theStr.indexOf('@'); 
	var dotIndex = theStr.indexOf('.', atIndex); 
	var flag = true; 
	if (theStr=="") 
		return true;
	theSub = theStr.substring(0, dotIndex+1) 
	acrosub = theStr.substring(atIndex+1,theStr.length);

	if ((atIndex < 1)||(acrosub!="acrocorp.com")||(atIndex != theStr.lastIndexOf('@'))||(dotIndex < atIndex + 2)||(theStr.length <= theSub.length)) 
	{ 
		alert(displayName+" is not a valid Recruiter's Email ID");
		obj.focus();
		flag = false; 
	} 
	else { flag = true; } 

	return(flag); 
}


function isBlank(obj,displayName)
{
	//**********************************************************************
	//Language: JavaScript
	//Written by: unknown
	//Date: Unknown
	//----------
	//Purpose:
	//Function to check whether object value is blank
	//----------
	//Arguments:
	//1) obj (object) - the object whose value for blank is to be tested
	//2) displayName (String) - Part of the alert message to be displayed
	//Return type: boolean
	//Returns: 	true: if the object value is blank
	//			false: if the value is not blank, displays a message
	//************************************************************************

	var str="";
	str=obj.value;
	var len=str.length;
	var i;
	for(i=0;i<len;++i)
	{
		if(str.charAt(i)!=" ")
		{
			return false;
		}
	}
//	alert(displayName+" cannot be left blank");
	obj.focus();
	return true;
}


function hasNoSplChars(obj,displayName)
{
	//**********************************************************************
	//Language: JavaScript
	//Written by: unknown
	//Date: Unknown
	//----------
	//Purpose:
	//Function to check whether object has special characters
	//   the special characters to check: @,#,$,%,^,&,*
	//----------
	//Arguments:
	//1) obj (object) - the object whose value for special characters is to be tested
	//2) displayName (String) - Part of the alert message to be displayed
	//Return type: boolean
	//Returns: 	true: if the object value has no special characters
	//			false: if the object value has special characters, displays a message
	//************************************************************************

	var s="";
	s=lTrim(obj.value);
	splChars=new Array("@","#","$","%","^","&","*");
	for(i=0;i>s.length;i++)
		for(j=0;j<splChars.length;j++)
		{
			if(s.charAt(i)==splChars[j])
			{
				alert(displayName+" cannot have special characters like @,#,$,% etc.");
				obj.focus();
				return false;
			}
		}
	return true;
}


function checkBlankForm(objFormToCheck, objFieldArrayToIgnore)
{
	//**********************************************************************
	//Language: JavaScript
	//Written by: Sanjeev Majalikar
	//Date: 29-Sep-00
	//----------
	//Purpose:
	//Function to check whether the HTML form has all blank fields
	//----------
	//Arguments:
	//1) objFormToCheck (object) - the form whose fields have to be tested for blank
	//2) objFieldArrayToIgnore (object) - the array object containing the form-fields to be ignored during testing
	//Return type: boolean
	//Returns: 	true: if the form has all blank fields
	//			false: if the form does not have all blank fields (i.e. form has atleast one filled field)
	//************************************************************************

	var numFields = objFormToCheck.elements.length;
	var numFieldsToIgnore = objFieldArrayToIgnore.length;
	var validFlag = true;
	//iterate through the form fields
	for (var i=0; i<numFields; i++)
	{
		//iterate through the array objFieldArrayToIgnore to check
		//whether the field is to be ignored
		proceedFlag = true;
		for (var j=0; j<numFieldsToIgnore; j++)
		{
			if (objFormToCheck.elements[i] == objFieldArrayToIgnore[j])
			{
				proceedFlag = false;
				break;
			}
		}

		if ( (proceedFlag == true) && 
			 ( (objFormToCheck.elements[i].value != null && objFormToCheck.elements[i].value != "" ) && 
			   (objFormToCheck.elements[i].type != 'submit' && objFormToCheck.elements[i].type != 'reset' && 
			      objFormToCheck.elements[i].type != 'button')
			  )
		    )
		{
			validFlag = false;
			//alert ("The form has a filled field");
			break;
		}
	}
	//return numFieldsToIgnore;
	return validFlag;
}



function checkFillForm(objFormToCheck, objFieldArrayToIgnore)
{
	//**********************************************************************
	//Language: JavaScript
	//Written by: Sanjeev Majalikar
	//Date: 29-Sep-00
	//----------
	//Purpose:
	//Function to check whether the HTML form has atleast one blank field
	//----------
	//Arguments:
	//1) objFormToCheck (object) - the form whose fields have to be tested for blank
	//2) objFieldArrayToIgnore (object) - the array object containing the form-fields to be ignored during testing
	//Return type: boolean
	//Returns: 	true: if the form has all filled fields
	//			false: if the form does not have all filled fields (i.e. form has atleast one blank field)
	//************************************************************************


	var numFields = objFormToCheck.elements.length;
	var numFieldsToIgnore = objFieldArrayToIgnore.length;
	var validFlag = true;
  
	//iterate through the form fields
	for (var i=0; i<numFields; i++)
	{
		//iterate through the array objFieldArrayToIgnore to check
		//whether the field is to be ignored
		proceedFlag = true;
		for (var j=0; j<numFieldsToIgnore; j++)
		{
	 		if (objFormToCheck.elements[i] == objFieldArrayToIgnore[j])
			{
				proceedFlag = false;
				break;
			}
		} 
	 
		if ( (proceedFlag == true) && 
			 ( (objFormToCheck.elements[i].value == null || objFormToCheck.elements[i].value == "" ) && 
		       (objFormToCheck.elements[i].type != 'submit' || objFormToCheck.elements[i].type != 'reset' || objFormToCheck.elements[i].type != 'hidden')
	       	 ) 
	       )
      	{
			validFlag = false;
			//alert ("The form has a blank field");
			break;
		}
	}
	return validFlag;
}


function goHome() 
{
	//**********************************************************************
	//Language: JavaScript
	//Written by: Shafqat Bashir
	//Date: 10-Oct-00
	//----------
	//Purpose:
	//Function to go to "http://www.acrocorp.com"
	//----------
	//Arguments:
	//None
	//Return type: na
	//Returns: 	none
	//************************************************************************
	top.close();
	newWindow = window.open("http://www.acrocorp.com");
}


function IsBlankArray(objFieldsToCheck)
{
	//**********************************************************************
	//Language: JavaScript
	//Written by: Shafqat Bashir
	//Date: 12-Oct-00
	//----------
	//Purpose:
	//Function to check whether the fields passed to this function are blank 
	//----------
	//Arguments:
	//1) objFieldsToCheck (object) - the form whose fields have to be tested for blank
	//Return type: boolean
	//Returns: 	true: if the all the fields are blank
	//			false: if one or more fields are not null
	//************************************************************************

	var numFields = objFieldsToCheck.length;
	var validFlag = true;
	//iterate through the form fields
	for (var i=0; i<numFields; i++)
	{

		if ( (objFieldsToCheck[i].value != null && objFieldsToCheck[i].value != "" ) )
		{
			validFlag = false;
			//alert ("Atleast one of the mandatory fields is empty");
			objFieldsToCheck[i].focus();
			break;
		}
	}
	return validFlag;
}
function setNull(objFormObjArray)
{
	//**********************************************************************
	//Language: JavaScript
	//Written by: Sanjeev Majalikar
	//Date: 10-Oct-00
	//----------
	//Purpose:
	//Function to set the value of array elements to null
	//----------
	//Arguments:
	//objFormObjArray (object) - Array of form-objects whose value is to be set to null
	//Return type: int
	//Returns: 	-1 if the array length is 0 or less
	//			0 on successful completion
	//************************************************************************
	arrLength = objFormObjArray.length;
	nullvar = null;
	if (arrLength <= 0)
		return -1;
	for (i=0;i<arrLength;i++) 		
		objFormObjArray[i].value = "";
						
	return 0;
}

function isDDMMYYYYDate(objFormField)
{
	//**********************************************************************
	//Language: JavaScript
	//Written by: Sanjeev Majalikar
	//Date: 10-Nov-00
	//----------
	//Purpose:
	//Function to validate the value of form field is in the date format dd/mm/yyyy
	//----------
	//Arguments:
	//objFormField (object) - form field object whose value is to be validated
	//Return type: boolean
	//Returns: 	true if the date is in the correct format
	//			false if the date is not in the correct format
	//************************************************************************

	var lsFieldVal = objFormField.value;
	var lsFieldLength = lsFieldVal.length;
	var thirdchar = lsFieldVal.substring(2,3);
	var sixthchar = lsFieldVal.substring(5,6);
	var separator = '/';
	if 	(
		(lsFieldLength == 10) &&
		(thirdchar == separator) &&
		(sixthchar == separator)
		)
		{
		//split the chars
		var liDayPart
		var liMonthPart
		var liYearPart
		if (lsFieldVal.substr(0,1) == 0)
			liDayPart = parseInt(lsFieldVal.substr(1,2));
		else
			liDayPart = parseInt(lsFieldVal.substr(0,2));

		if (lsFieldVal.substring(3,4) == 0)
			liMonthPart = parseInt(lsFieldVal.substring(4,5));
		else
			liMonthPart = parseInt(lsFieldVal.substring(3,5));

		liYearPart = parseInt(lsFieldVal.substring(6,10));

		//check overall validity
		if (
			(liDayPart >= 1) &&
			(liDayPart <= 31) &&
			(liMonthPart >= 1) &&
			(liMonthPart <= 12) &&
			(liYearPart >= 1999) &&
			(liYearPart <= 2050)
			)
			{
			//check for 30-day months
			if (
				(liMonthPart==4) ||
				(liMonthPart==6) ||
				(liMonthPart==9) ||
				(liMonthPart==11) 
				)
				{
				if ( liDayPart <= 30)
					{
					retval = true;
					}
				else
					{
					retval = false;
					}
				}
			else	//not a 30-day month
				{
				//if february
				if (liMonthPart == 2)
					{
					//check for leap year
					if ( (liYearPart % 4) != 0) 
						{
						liFebDays = 28;
						}
					else
						{
						if ((liYearPart % 400) == 0)
							{
							liFebDays = 29;
							}
						else
							{
							if ((liYearPart % 100) ==0)
								{
								liFebDays = 28;
								}
							else
								{
								liFebDays = 29;
								}
							}
						}

					if (liDayPart <= liFebDays)
						{
						retval = true;
						}
					else
						{
						retval = false;
						}
					}
				else	//31-day month
					{
					retval = true;
					}
				}
			}
		else
			{
				retval = false;
			}
		}
	else
		{
		retval = false;
		}

	return retval;
}


function isMMDDYYYYDate(objFormField)
{
	//**********************************************************************
	//Language: JavaScript
	//Written by: Sanjeev Majalikar
	//Date: 10-Nov-00
	//----------
	//Purpose:
	//Function to validate the value of form field is in the date format mm/dd/yyyy
	//----------
	//Arguments:
	//objFormField (object) - form field object whose value is to be validated
	//Return type: boolean
	//Returns: 	true if the date is in the correct format
	//			false if the date is not in the correct format
	//************************************************************************

	var lsFieldVal = objFormField.value;
	var lsFieldLength = lsFieldVal.length;
	var thirdchar = lsFieldVal.substring(2,3);
	var sixthchar = lsFieldVal.substring(5,6);
	var separator = '/';
	if 	(
		(lsFieldLength == 10) &&
		(thirdchar == separator) &&
		(sixthchar == separator)
		)
		{
		//split the chars
		var liDayPart
		var liMonthPart
		var liYearPart
		if (lsFieldVal.substr(0,1) == 0)
			liMonthPart = parseInt(lsFieldVal.substr(1,2));
		else
			liMonthPart = parseInt(lsFieldVal.substr(0,2));

		if (lsFieldVal.substring(3,4) == 0)
			liDayPart = parseInt(lsFieldVal.substring(4,5));
		else
			liDayPart = parseInt(lsFieldVal.substring(3,5));

		liYearPart = parseInt(lsFieldVal.substring(6,10));

		//check overall validity
		if (
			(liDayPart >= 1) &&
			(liDayPart <= 31) &&
			(liMonthPart >= 1) &&
			(liMonthPart <= 12) &&
			(liYearPart >= 1980) &&
			(liYearPart <= 2050)
			)
			{
			//check for 30-day months
			if (
				(liMonthPart==4) ||
				(liMonthPart==6) ||
				(liMonthPart==9) ||
				(liMonthPart==11) 
				)
				{
				if ( liDayPart <= 30)
					{
					retval = true;
					}
				else
					{
					retval = false;
					}
				}
			else	//not a 30-day month
				{
				//if february
				if (liMonthPart == 2)
					{
					//check for leap year
					if ( (liYearPart % 4) != 0) 
						{
						liFebDays = 28;
						}
					else
						{
						if ((liYearPart % 400) == 0)
							{
							liFebDays = 29;
							}
						else
							{
							if ((liYearPart % 100) ==0)
								{
								liFebDays = 28;
								}
							else
								{
								liFebDays = 29;
								}
							}
						}

					if (liDayPart <= liFebDays)
						{
						retval = true;
						}
					else
						{
						retval = false;
						}
					}
				else	//31-day month
					{
					retval = true;
					}
				}
			}
		else
			{
				retval = false;
			}
		}
	else
		{
		retval = false;
		}

	return retval;
}

function getRadioValue(radioObject) 
{

	var value = null

	for (var i=0; i<radioObject.length; i++) {

		if (radioObject[i].checked) {

			value = radioObject[i].value

			break					}

											}

	return value

}


function IsFilledArray(objFieldsToCheck)
{
	//**********************************************************************
	//Language: JavaScript
	//Written by: Rachit Khanna
	//Date: 05-Dec-00
	//----------
	//Purpose:
	//Function to check whether the elements passed to this function are filled 
	//----------
	//Arguments:
	//1) objFieldsToCheck (object) - the Array whose fields have to be tested for filled
	//Return type: boolean
	//Returns: 	true: if the all the fields are filled
	//			false: if one or more fields are  null
	//*******************************************\*****************************

	var numFields = objFieldsToCheck.length;
	var validFlag = true;
	//iterate through the form fields
	for (var i=0; i<numFields; i++)
	{

		if ( (objFieldsToCheck[i].value == null || objFieldsToCheck[i].value == "" ) )
		{
			validFlag = false;
			//alert ("Atleast one of the mandatory fields is empty");
			objFieldsToCheck[i].focus();
			break;
		}
	}
	return validFlag;
}

function formatDateDDMMYYYY(objFormTextField)
{
	var retval = "";
	var liDayPart;
	var liMonthPart;
	var lsFieldValue = objFormTextField.value;
	
	var lsDayPart = lsFieldValue.substr(0,2);
	if (lsFieldValue.substr(0,1) == 0)
		liDayPart = parseInt(lsFieldValue.substr(1,2));
	else
		liDayPart = parseInt(lsFieldValue.substr(0,2));

//	var liMonthPart = parseInt(lsFieldValue.substring(3,5));
	if (lsFieldValue.substring(3,4) == 0)
		liMonthPart = parseInt(lsFieldValue.substring(4,5));
	else
		liMonthPart = parseInt(lsFieldValue.substring(3,5));

	var lsYearPart = lsFieldValue.substring(6,10);
	var liYearPart = parseInt(lsYearPart);
	
	var ldDate = new Date(liYearPart, liMonthPart-1, liDayPart);
	
	var lsMonthName = ldDate.getCalendarMonth();
	lsMonthName = lsMonthName.substr(0,3);
	lsMonthName = lsMonthName.toUpperCase();

	retval = lsDayPart + "-" + lsMonthName + "-" + lsYearPart ;

	return (retval);
}

function formatDateMMDDYYYY(objFormTextField)
{
	var retval = "";
	var liDayPart;
	var liMonthPart;
	var lsFieldValue = objFormTextField.value;
	
	var lsMonthPart = lsFieldValue.substr(0,2);
	if (lsFieldValue.substr(0,1) == 0)
		liMonthPart = parseInt(lsFieldValue.substr(1,2));
	else
		liMonthPart = parseInt(lsFieldValue.substr(0,2));

	var lsDayPart = lsFieldValue.substring(3,5);
	if (lsFieldValue.substring(3,4) == 0)
		liDayPart = parseInt(lsFieldValue.substring(4,5));
	else
		liDayPart = parseInt(lsFieldValue.substring(3,5));

	var lsYearPart = lsFieldValue.substring(6,10);
	var liYearPart = parseInt(lsYearPart);
	
	var ldDate = new Date(liYearPart, liMonthPart-1, liDayPart);
	
	var lsMonthName = ldDate.getCalendarMonth();
	lsMonthName = lsMonthName.substr(0,3);
	lsMonthName = lsMonthName.toUpperCase();

	retval = lsDayPart + "-" + lsMonthName + "-" + lsYearPart ;

	return (retval);
}

function formatDate()
{
	var ldToday = new Date();
	var lsDay = ldToday.getDate();
	var lsMonth = ldToday.getCalendarMonth();
	lsMonth = lsMonth.substr(0,3);
	lsMonth = lsMonth.toUpperCase();
	lsYear = ldToday.getFullYear() + "";
	lsDate = lsDay + "-" + lsMonth + "-" + lsYear;
	return (lsDate);
}

function ConvertToAlphaKeyPad(argStr)
{
	//function to convert a string into equivalent numeric format as represented
	// on an aphanumeric keypad pf a telephone
	//3-Nov-01 - Sanjeev
	//return type - string

	//get length
	var numLength = argStr.length;
	var retStr = "";

	for (var i=0; i<numLength; i++)
	{
		switch (argStr.substr(i,1))
		{
			case 'A':
				strNumValue = "2";
				break;
			case 'B':
				strNumValue = "2";
				break;
			case 'C':
				strNumValue = "2";
				break;
			case 'D':
				strNumValue = "3";
				break;
			case 'E':
				strNumValue = "3";
				break;
			case 'F':
				strNumValue = "3";
				break;
			case 'G':
				strNumValue = "4";
				break;
			case 'H':
				strNumValue = "4";
				break;
			case 'I':
				strNumValue = "4";
				break;
			case 'J':
				strNumValue = "5";
				break;
			case 'K':
				strNumValue = "5";
				break;
			case 'L':
				strNumValue = "5";
				break;
			case 'M':
				strNumValue = "6";
				break;
			case 'N':
				strNumValue = "6";
				break;
			case 'O':
				strNumValue = "6";
				break;
			case 'P':
				strNumValue = "7";
				break;
			case 'Q':
				strNumValue = "7";
				break;
			case 'R':
				strNumValue = "7";
				break;
			case 'S':
				strNumValue = "7";
				break;
			case 'T':
				strNumValue = "8";
				break;
			case 'U':
				strNumValue = "8";
				break;
			case 'V':
				strNumValue = "8";
				break;
			case 'W':
				strNumValue = "9";
				break;
			case 'X':
				strNumValue = "9";
				break;
			case 'Y':
				strNumValue = "9";
				break;
			case 'Z':
				strNumValue = "9";
				break;
			default:
				strNumValue = argStr.substr(i,1);
		}
		retStr = retStr + strNumValue;
	}
	return (retStr);

}

function MisReportMapFlds()
{
	for(i=1;i<=7;i++)
	{
		//hdnObj="hdnMisc"+i
		if (document.getElementById("hdnMisc"+i))
		{
			if(document.getElementById("hdnMisc"+i).value=="True")
			{
				document.getElementById("Misc"+i).value = trim(document.getElementById("Misc"+i).value);
				if(document.getElementById("Misc"+i).value=="")
				{
					alert("Please enter value for "+document.getElementById("dspMisc"+i).value);
					document.getElementById("Misc"+i).focus();
					return false;
				}
			}
		}
	}
return true;
}
