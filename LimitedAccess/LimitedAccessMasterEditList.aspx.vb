Imports commonutility
Imports System.Data
Imports System.text
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System

Public Class LimitedAccessMasterEditList
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tbl1 As System.Web.UI.WebControls.Table
    Protected WithEvents MainDiv As System.Web.UI.HtmlControls.HtmlGenericControl

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not Page.IsPostBack Then
            getvalue()
        End If
    End Sub
    Sub getvalue()
        Dim strquery As StringBuilder
        Dim str As StringBuilder
        Dim dtrreader As SqlDataReader
        Dim accessdata As clsutility
        Dim createby As String = ""
        Dim ModifiedBy As String = ""
        Dim ModifiedDate As String = ""
        Dim Service As String = ""

        accessdata = New clsutility
        strquery = New StringBuilder("Exec Proc_LimitedAccessMaster '" & Request.QueryString("ID") & "',0")
        'response.write(strquery)
        'response.end()
        dtrreader = accessdata.funcGetSQLDataReader(strquery.ToString)
        If dtrreader.HasRows Then
            str = New StringBuilder("<table ID=""tbl1"" HorizontalAlign=""Center"" BorderColor=""#cccc99"" BorderStyle=""Solid"" GridLines=""both"" border=""1"" CellSpacing=""0"" CellPadding=""0"">")
            str.Append("<tr>")
            str.Append("<td Class=""btnstyle"">&nbsp;&nbsp;<b>ID</b>&nbsp;&nbsp;</td>")
            str.Append("<td Class=""btnstyle"">&nbsp;&nbsp;<b>User Name</b>&nbsp;&nbsp;</td>")
            str.Append("<td Class=""btnstyle"">&nbsp;&nbsp;<b>Page Name</b>&nbsp;&nbsp;</td>")
            str.Append("<td Class=""btnstyle"">&nbsp;&nbsp;<b>Created By</b>&nbsp;&nbsp;</td>")
            str.Append("<td Class=""btnstyle"">&nbsp;&nbsp;<b>Create Date</b>&nbsp;&nbsp;</td>")
            str.Append("<td Class=""btnstyle"">&nbsp;&nbsp;<b>Modified By</b>&nbsp;&nbsp;</td>")
            str.Append("<td Class=""btnstyle"">&nbsp;&nbsp;<b>Modify Date</b>&nbsp;&nbsp;</td>")
            str.Append("<td Class=""btnstyle"">&nbsp;&nbsp;<b>Active</b>&nbsp;&nbsp;</td>")
            str.Append("<td Class=""btnstyle"">&nbsp;&nbsp;<b>Edit</b>&nbsp;&nbsp;</td>")
            str.Append("</tr>")
            While dtrreader.Read

                str.Append("<tr>")
                str.Append("<td>" & dtrreader("ID") & "</td>")
                str.Append("<td>" & dtrreader("UserName") & "</td>")
                str.Append("<td>" & dtrreader("PageName") & "</td>")
                str.Append("<td>" & dtrreader("CreatedBy") & "</td>")
                str.Append("<td>" & dtrreader("CreateDate") & "</td>")
                str.Append("<td>" & dtrreader("ModifiedBy") & "</td>")
                str.Append("<td>" & dtrreader("ModifiedDate") & "</td>")
                If dtrreader("Active") Then
                    str.Append("<td>Active</td>")
                Else
                    str.Append("<td>Not Active</td>")
                End If

                str.Append("<td>&nbsp;&nbsp;<a href=LimitedAccessMasterEdit.aspx?ID=" & dtrreader("ID") & " >Edit</a>&nbsp;&nbsp;</td>")
                str.Append("</tr>")

            End While
            str.Append("</table>")
            MainDiv.InnerHtml = str.ToString
        Else
            str = New StringBuilder("<table ID=""tblCDP1"" HorizontalAlign=""Center"" BorderColor=""#cccc99"" BorderStyle=""Solid"" GridLines=""both"" border=""1"" CellSpacing=""0"" CellPadding=""0"">")
            str.Append("<tr>")
            str.Append("<td Class=""btnstyle"">&nbsp;&nbsp;<b>No record exists.</b>&nbsp;&nbsp;</td>")
            str.Append("</tr>")
            str.Append("</table>")
            MainDiv.InnerHtml = str.ToString
        End If
        dtrreader.Close()
        accessdata.Dispose()
    End Sub
End Class
