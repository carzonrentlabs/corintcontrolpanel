<%@ Page Language="vb" AutoEventWireup="false" Src="LimitedAccessMasterEditList.aspx.vb"
    CodeBehind="LimitedAccessMasterEditList.aspx.vb" Inherits="LimitedAccessMasterEditList" %>

<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>CarzonRent :: Internal software Control panel</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <script language="JavaScript" src="../utilityfunction.js" type="text/jscript"></script>
    <script language="JavaScript" src="../JScripts/Datefunc.js" type="text/jscript"></script>
    <link href="../HertzInt.css" type="text/css" rel="stylesheet" />
    <style type="text/css">
        .btnstyle
        {
            color: #aa0000;
            text-decoration: none;
        }
    </style>
    <script language="javascript">
        function validation() {

        }		
    </script>
</head>
<body ms_positioning="GridLayout">
    <form id="Form1" method="post" runat="server">
    <uc1:Headerctrl ID="Headerctrl2" runat="server"></uc1:Headerctrl>
    <br/>
    <br/>
    <table align="center">
        <tr>
            <td colspan="2" align="center">
                <strong><u>Edit Booker Master</u></strong>
                <br/>
                <br/>
            </td>
        </tr>
        <tr>
            <td colspan="2" valign="top">
                <div id="MainDiv" runat="server" style="width: 100%; height: 100%">
                </div>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
