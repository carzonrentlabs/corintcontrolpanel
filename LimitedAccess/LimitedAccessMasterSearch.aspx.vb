Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class LimitedAccessMasterSearch
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents ddUserName As System.Web.UI.WebControls.DropDownList

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not Page.IsPostBack() Then
            populateddl()
        End If
    End Sub
    Sub populateddl()
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility
        ddUserName.DataSource = objAcessdata.funcGetSQLDataReader("select a.fname+' '+a.mname+' '+a.lname +' '+ '('+a.loginid + ')' as username,a.sysuserid from CORIntSysUsersMaster as a inner join CorIntLimitedAccess as b on a.sysuserid = b.Userid order by a.fname+' '+a.mname+' '+a.lname  ")
        ddUserName.DataValueField = "sysuserid"
        ddUserName.DataTextField = "username"
        ddUserName.DataBind()
        ddUserName.Items.Insert(0, New ListItem("Any", 0))
        'dtrreader.Close()
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Response.Redirect("LimitedAccessMasterEditList.aspx?ID=" & ddUserName.SelectedValue)
    End Sub
End Class
