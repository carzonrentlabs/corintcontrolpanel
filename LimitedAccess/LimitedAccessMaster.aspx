<%@ Page Language="vb" AutoEventWireup="false" Src="LimitedAccessMaster.aspx.vb" CodeBehind="LimitedAccessMaster.aspx.vb" Inherits="LimitedAccessMaster" %>
<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>CarzonRent :: Internal software Control panel</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <script language="JavaScript" src="../utilityfunction.js" type="text/jscript"></script>
    <link href="../HertzInt.css" type="text/css" rel="stylesheet" />
</head>
<body ms_positioning="GridLayout">
    <form id="Form1" method="post" runat="server">
    <uc1:Headerctrl ID="Headerctrl1" runat="server"></uc1:Headerctrl>
    <table id="Table1" align="center">
        <tbody>
            <tr>
                <td align="center" colspan="2">
                    <strong><u>Limited Access Master</u></strong>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2">
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2">
                    <a href="LimitedAccessMasterAdd.aspx">Add a Limited Access Master</a>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2">
                    <a href="LimitedAccessMasterSearch.aspx">Edit a Limited Access Master</a>
                </td>
            </tr>
        </tbody>
    </table>
    </form>
</body>
</html>
