<%@ Page Language="vb" AutoEventWireup="false" Src="LimitedAccessMasterSearch.aspx.vb"
    CodeBehind="LimitedAccessMasterSearch.aspx.vb" Inherits="LimitedAccessMasterSearch" %>

<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>CarzonRent :: Internal software Control panel</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <script language="JavaScript" src="../utilityfunction.js" type="text/jscript"></script>
    <link href="../HertzInt.css" type="text/css" rel="stylesheet" />
</head>
<body ms_positioning="GridLayout">
    <form id="Form1" method="post" runat="server">
    <uc1:Headerctrl ID="Headerctrl1" runat="server"></uc1:Headerctrl>
    <table id="Table1" align="center">
        <tbody>
            <tr>
                <td align="center" colspan="2">
                    <strong><u>Edit a Limited Access</u></strong>
                </td>
            </tr>
            <tr>
                <td>
                    * User Name
                </td>
                <td>
                    <asp:DropDownList ID="ddUserName" runat="server" CssClass="input">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2">
                    <asp:Button ID="btnSubmit" Text="Serach" runat="server"></asp:Button>
                </td>
            </tr>
        </tbody>
    </table>
    </form>
</body>
</html>
