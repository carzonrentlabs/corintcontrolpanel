<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="LimitedAccessMasterEdit.aspx.vb"
    Inherits="LimitedAccessMasterEdit" Src="LimitedAccessMasterEdit.aspx.vb" %>

<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>CarzonRent :: Internal software Control panel</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <script language="JavaScript" src="../utilityfunction.js" type="text/jscript"></script>
    <link href="../HertzInt.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript">
        function validation() {
            var strLimitedAccess = document.getElementById('txtPagename').value;
            strLimitedAccess = strLimitedAccess.replace(/^\s+/, ""); //Removes Left Blank Spaces
            strLimitedAccess = strLimitedAccess.replace(/\s+$/, ""); //Removes Right Blank Spaces

            if (strLimitedAccess == "") {
                alert("Please enter the Page Name")
                document.getElementById('txtPagename').focus();
                return false;
            }
            return true;
        }		
    </script>
</head>
<body ms_positioning="GridLayout">
    <form id="Form1" method="post" runat="server">
    <uc1:Headerctrl ID="Headerctrl1" runat="server"></uc1:Headerctrl>
    <table id="Table1" align="center">
        <asp:Panel ID="pnlmainform" runat="server">
            <tbody>
                <tr>
                    <td align="center" colspan="2">
                        <strong><u>Edit a Booker Master</u></strong>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        &nbsp;&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        Booker ID
                    </td>
                    <td>
                        <asp:Label ID="lblID" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        * Booker Name
                    </td>
                    <td>
                        <asp:TextBox ID="txtPagename" runat="server" value=""></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        * Agency ID
                    </td>
                    <td>
                        <asp:DropDownList ID="ddUserName" runat="server" value="">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        Active
                    </td>
                    <td>
                        <asp:CheckBox ID="chk_active" runat="server" CssClass="input" Checked="True"></asp:CheckBox>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <asp:Button ID="btnsubmit" runat="server" CssClass="button" Text="Submit"></asp:Button>&nbsp;&nbsp;
                        <asp:Button ID="btnreset" runat="server" CssClass="button" Text="Reset" CausesValidation="False">
                        </asp:Button>
                    </td>
                </tr>
            </tbody>
        </asp:Panel>
        <asp:Panel ID="pnlconfirmation" runat="server" Visible="False">
            <tr align="center">
                <td colspan="2">
                    <asp:Label ID="lblErrorMsg" runat="server"></asp:Label>
                </td>
            </tr>
            <tr align="center">
                <td colspan="2">
                    <asp:HyperLink ID="hyplnkretry" runat="server"></asp:HyperLink>
                </td>
            </tr>
        </asp:Panel>
    </table>
    </form>
</body>
</html>