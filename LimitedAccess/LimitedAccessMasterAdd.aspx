<%@ Page Language="vb" AutoEventWireup="false" Src="LimitedAccessMasterAdd.aspx.vb"
    CodeBehind="LimitedAccessMasterAdd.aspx.vb" Inherits="LimitedAccessMasterAdd" %>

<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//Dtd HTML 4.0 transitional//EN">
<script runat="server">

    Protected Sub Headerctrl1_Load(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>CarzonRent :: Internal software Control panel</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <script language="JavaScript" src="../utilityfunction.js" type="text/jscript"></script>
    <script language="JavaScript" src="../JScripts/Datefunc.js" type="text/jscript"></script>
    <link href="../HertzInt.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript">
        function validation() {

            var strAccessRights = document.getElementById('txtPageName').value;
            strAccessRights = strAccessRights.replace(/^\s+/, ""); //Removes Left Blank Spaces
            strAccessRights = strAccessRights.replace(/\s+$/, ""); //Removes Right Blank Spaces

            if (strAccessRights == "") {
                alert("Please enter the Page Name.");
                document.getElementById('txtPageName').focus();
                return false;
            }

            if (document.getElementById("ddUserName").value == "0" || document.getElementById("ddUserName").value == "1") {
                alert("Please select User Name");
                document.getElementById("ddUserName").focus();
                return false;
            }

            return true;
        }
    </script>
</head>
<body ms_positioning="GridLayout">
    <form id="Form1" method="post" runat="server">
    <uc1:Headerctrl ID="Headerctrl1" runat="server" OnLoad="Headerctrl1_Load"></uc1:Headerctrl>
    <table id="Table1" align="center">
        <asp:Panel ID="pnlmainform" runat="server">
            <tbody>
                <tr>
                    <td align="center" colspan="2">
                        <strong><u>Add a Edit Access Rights</u></strong>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        &nbsp;&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        User Name
                    </td>
                    <td>
                        <asp:DropDownList ID="ddUserName" runat="server" CssClass="input">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        * Page Name
                    </td>
                    <td>
                        <asp:TextBox ID="txtPageName" runat="server" CssClass="input" MaxLength="200" TextMode="SingleLine"
                            value="BookingCoEditForm.asp"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Active
                    </td>
                    <td>
                        <asp:CheckBox ID="chkactive" runat="server" CssClass="input" Checked="true"></asp:CheckBox>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <asp:Button ID="btnsubmit" runat="server" CssClass="button" Text="Submit"></asp:Button>&nbsp;&nbsp;
                        <asp:Button ID="btnreset" runat="server" CssClass="button" Text="Reset" CausesValidation="False">
                        </asp:Button>
                    </td>
                </tr>
            </tbody>
        </asp:Panel>
        <asp:Panel ID="pnlconfirmation" runat="server" Visible="False">
            <tr align="center">
                <td colspan="2">
                    <asp:Label ID="lblErrorMsg" runat="server"></asp:Label>
                </td>
            </tr>
            <tr align="center">
                <td colspan="2">
                    <asp:HyperLink ID="hyplnkretry" runat="server"></asp:HyperLink>
                </td>
            </tr>
        </asp:Panel>
    </table>
    </form>
</body>
</html>
