Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class LimitedAccessMasterAdd
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtPageName As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnsubmit As System.Web.UI.WebControls.Button
    Protected WithEvents chkActive As System.Web.UI.WebControls.CheckBox
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    Protected WithEvents ddUserName As System.Web.UI.WebControls.DropDownList

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        btnsubmit.Attributes("onClick") = "return validation();"
        If Not Page.IsPostBack() Then
            Dim dtrreader As SqlDataReader
            Dim accessdata As clsutility
            accessdata = New clsutility
            dtrreader = accessdata.funcGetSQLDataReader("select fname+' '+mname+' '+lname +' '+ '('+loginid + ')' as username,sysuserid from CORIntSysUsersMaster where active=1 order by fname+' '+mname+' '+lname ")
            ddUserName.DataSource = dtrreader
            ddUserName.DataValueField = "sysuserid"
            ddUserName.DataTextField = "username"
            ddUserName.DataBind()
            ddUserName.Items.Insert(0, New ListItem("Select", 0))
            dtrreader.Close()
        End If
    End Sub

    Private Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        Dim intFlagCheck As SqlParameter
        Dim intFlag As Int32
        Dim intSDParam As SqlParameter
        Dim intSDid As Int32

        cmd = New SqlCommand("Proc_AddLimitedAccessMaster", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.AddWithValue("@PageName", txtPageName.Text)
        cmd.Parameters.AddWithValue("@UserID", Convert.ToInt32(ddUserName.SelectedItem.Value))
        cmd.Parameters.AddWithValue("@Active", get_YNvalue(chkActive))
        cmd.Parameters.AddWithValue("@CreatedBy", Session("loggedin_user"))
        intFlagCheck = cmd.Parameters.Add("@StatusFlag", SqlDbType.Int)
        intFlagCheck.Direction = ParameterDirection.Output
        intSDParam = cmd.Parameters.Add("@ID", SqlDbType.Int)
        intSDParam.Direction = ParameterDirection.Output

        MyConnection.Open()

        cmd.ExecuteNonQuery()
        intFlag = cmd.Parameters("@StatusFlag").Value
        intSDid = cmd.Parameters("@ID").Value

        MyConnection.Close()
        lblErrorMsg.Visible = True

        If Trim("" & intFlag) = "2" Then
            pnlmainform.Visible = False
            pnlconfirmation.Visible = True
            lblErrorMsg.Text = "This Edit Access to the user is already exists!<br/> The ID is <b>" & intSDid & "</b>"
            hyplnkretry.Text = "Assign another User Edit Access"
            hyplnkretry.NavigateUrl = "LimitedAccessMasterAdd.aspx"
        Else
            pnlmainform.Visible = False
            pnlconfirmation.Visible = True
            lblErrorMsg.Text = "You have added the Edit Access rights to the user <br/> The ID is <b>" & intSDid & "</b>"
            hyplnkretry.Text = "Add another Edit Access Rights to the User."
            hyplnkretry.NavigateUrl = "LimitedAccessMasterAdd.aspx"
        End If
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function
End Class