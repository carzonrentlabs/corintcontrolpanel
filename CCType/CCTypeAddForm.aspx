<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Src="CCTypeAddForm.aspx.vb" Inherits="CCTypeAddForm"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CarzonRent :: Internal software</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../utilityfunction.js"></script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<uc1:Headerctrl id="Headerctrl1" runat="server"></uc1:Headerctrl>
			<p><asp:validationsummary id="Validationsummary1" runat="server" ShowSummary="false" HeaderText="Please make sure all the fields marked with * are filled in."
					ShowMessageBox="true"></asp:validationsummary></p>
			<table align="center">
				<asp:Panel ID="pnlmainform" Runat="server">
					<TBODY>
						<TR>
							<TD align="center" colSpan="2"><B><U>Add a Credit Card Type</U></B></TD>
						</TR>
						<tr><td colspan="2" align="center"><asp:Label id="lblErrorMsg" cssclass="subRedHead" visible="false" runat="server"></asp:Label></td></tr>
						<TR>
							<TD>* Credit Card Type
							</TD>
							<TD>
								<asp:textbox id="txtcctype" runat="server" MaxLength="100" CssClass="input"></asp:textbox>
								<asp:requiredfieldvalidator id="Requiredfieldvalidator1" runat="server" Controltovalidate="txtcctype" Display="None"
									errormessage=""></asp:requiredfieldvalidator></TD>
						</TR>
						<TR>
							<TD vAlign="top">Remarks(<SPAN class="shwText" id="shwMessage">0</SPAN>/2000 chars)</TD>
							<TD>
								<asp:textbox id="txtarearemarks" onkeydown="showLength(this.form.txtarearemarks,'document.all.shwMessage',2000)"
									onkeyup="showLength(this.form.txtarearemarks,'document.all.shwMessage',2000)" runat="server"
									MaxLength="2000" CssClass="input" TextMode="MultiLine" Columns="50" Rows="3"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>Active</TD>
							<TD>
								<asp:checkbox id="chkActive" runat="server" CssClass="input" Checked="True"></asp:checkbox></TD>
						</TR>
						<TR>
							<TD align="center" colSpan="2">
								<asp:button id="btnSubmit" runat="server" CssClass="input" Text="Submit"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="Reset" CssClass="button" name="Reset" value="Reset" /></TD>
						</TR>
				</asp:Panel>
				<asp:Panel ID="pnlconfirmation" Visible="False" Runat="server">
					<TR align="center">
						<TD colSpan="2">
							<asp:Label id="lblMessage" runat="server"></asp:Label></TD>
					</TR>
					<TR align="center">
						<TD colSpan="2">
							<asp:HyperLink id="hyplnkretry" runat="server"></asp:HyperLink></TD>
					</TR>
				</asp:Panel></TBODY>
			</table>
		</form>
	</body>
</HTML>
