Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports commonutility
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Web.UI


Public Class Headerctrl
    Inherits System.Web.UI.UserControl

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tblRecDetail As System.Web.UI.WebControls.Table

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        'Put user code to initialize the page here
        '   If Not Page.IsPostBack Then

        If IsDBNull(Session("loggedin_user")) Then
            Session.Abandon()
            ViewState.Clear()
            Response.Redirect("https://insta.carzonrent.com/CorintControlPanel/CP_Logout.aspx")
        End If

        Dim dtrreader As SqlDataReader

        Dim selectdata As clsutility
        selectdata = New clsutility
        dtrreader = selectdata.funcGetSQLDataReader("exec Prc_ControlPanelMaster")
        Dim Temprow As New TableRow
        Dim Tempcell As New TableCell

        While dtrreader.Read
            Tempcell.Controls.Add(New LiteralControl("<a href=../index/CP_index.aspx?ID=" & dtrreader("moduleid") & " >" & dtrreader("modulename") & "</a>|"))
            Temprow.Cells.Add(Tempcell)
            tblRecDetail.Rows.Add(Temprow)
        End While
        dtrreader.Close()
        selectdata.Dispose()

        '      End If
    End Sub
End Class
