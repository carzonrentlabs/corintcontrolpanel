Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Mail
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Net.Mail.MailMessage
Imports System

Public Class ClientCoIndivAddForm
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtfname As System.Web.UI.WebControls.TextBox

    Protected WithEvents txtlname As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlcompany As System.Web.UI.WebControls.DropDownList
   
    Protected WithEvents txtph1 As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtph As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtalternatefname As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtalternatemname As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtalternatelname As System.Web.UI.WebControls.TextBox
    'Protected WithEvents tctalternatecphone As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtfax As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtemail As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtDOB As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtAnniv As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtEmailID2 As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtResAdd As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtResPhone As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtEmpCode As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtCCLocCode As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtarearemarks As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkActive As System.Web.UI.WebControls.CheckBox
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnReset As System.Web.UI.WebControls.Button
    Protected WithEvents pnl2ndform As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    Protected WithEvents hdndcoval As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ddldiscount As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddldiscountdecimal As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlpayment As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    Protected WithEvents btnproceed As System.Web.UI.WebControls.Button
    Protected WithEvents btnr As System.Web.UI.WebControls.Button
    'Protected WithEvents txtCCN1 As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtCCN3 As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtCCN4 As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txExpDate As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtCCN2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents DropDownList1 As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents ddlCCardType As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents ddCCardType As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents txtCCN As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtExpDate As System.Web.UI.WebControls.TextBox
    'Protected WithEvents pnlCc As System.Web.UI.WebControls.Panel
    Protected WithEvents chkIsVIP As System.Web.UI.WebControls.CheckBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' Code commented by Binary semantics on 28-March-07
        'btnproceed.Attributes("onClick") = "return firstpage();"
        'btnSubmit.Attributes("onClick") = "return secondpage();"

        ' Code changed by Binary semantics on 28-March-07
        btnSubmit.Attributes("onClick") = "return firstpage();"
        ''Form1.Attributes("onLoad") = "dispCCPanel(this);"

        ' Code added by BSL on 03-April-07, define event on company combo
        ddlcompany.Attributes("onkeydown") = "smartOptionFinderWithSubmit(this,event);"
        'ddlcompany.Attributes("onkeydown") = "smartOptionFinderWithSubmit(this,event);" 'Duplicate code Commented By Rahul on 06-Apr-2010


        If Not Page.IsPostBack Then
            populateddl()
        End If
    End Sub
    Sub populateddl()
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility
        ddlcompany.DataSource = objAcessdata.funcGetSQLDataReader("select ClientCoName  ,ClientCoID   from CORIntClientCoMaster    where active=1 order by ClientCoName ")
        ddlcompany.DataValueField = "ClientCoID"
        ddlcompany.DataTextField = "ClientCoName"
        ddlcompany.DataBind()
        ddlcompany.Items.Insert(0, New ListItem("", ""))
        objAcessdata.funcpopulatenumddw(100, 0, ddldiscount)
        objAcessdata.funcpopulatenumddw(9, 0, ddldiscountdecimal)

        'ddCCardType.DataSource = objAcessdata.funcGetSQLDataReader("Select CCTypeID, CCTypeName from CORIntCCTypeMaster where Active = 1 Order by CCTypeName")
        'ddCCardType.DataValueField = "CCTypeID"
        'ddCCardType.DataTextField = "CCTypeName"
        'ddCCardType.DataBind()
        'ddCCardType.Items.Insert(0, New ListItem("", ""))
        'ddCCardType.Dispose()
    End Sub


    Function autoselec_ddl(ByVal ddlname As DropDownList, ByVal selectvalue As Int32)

        If Not IsDBNull(selectvalue) Then
            If Not selectvalue = 0 Then
                ddlname.Items.FindByValue(selectvalue).Selected = True
            Else
                ddlname.SelectedValue = 0
            End If
        End If
    End Function

    Private Sub btnr_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Response.Redirect("ClientCoIndivAddForm.aspx")
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("ClientCoIndivAddForm.aspx")
    End Sub

    ' Code Added by Binary Semantics on 29-March-07
    Function IsEmailExist(ByVal email As String) As Boolean
        Dim emailCheck As Boolean
        emailCheck = False
        Dim dtrreader As SqlDataReader
        Dim accessdata As clsutility
        accessdata = New clsutility
        dtrreader = accessdata.funcGetSQLDataReader("SELECT Count(EmailID) as EMail FROM CORIntClientCoIndivMaster WHERE EmailID =  '" & email & "' ")
        dtrreader.Read()

        If dtrreader("EMail") > 0 Then
            emailCheck = True
        End If

        dtrreader.Close()
        accessdata.Dispose()

        Return emailCheck
    End Function
    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        ' Code added by Binary Semantics on 29-March-07
        ' Call function to check for email exist or not
        Dim checkEmail As Boolean
        checkEmail = IsEmailExist(txtemail.Text)

        If Not checkEmail Then
            ' Email not exist, insert the record

            Dim MyConnection As SqlConnection
            MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
            Dim cmd As SqlCommand

            Dim intuniqcheck As SqlParameter
            Dim intuniqvalue As Int32
            'Dim intInc As Int32
            Dim disdiscount As String
            'Dim strCardNo As String
            'Dim ChrMod As String

            'If txtCCN.Text <> "" Then
            '    For intInc = 1 To Len(txtCCN.Text)
            '        ChrMod = Chr(CInt(Asc(Mid(txtCCN.Text, intInc, 1))) + 9)
            '        strCardNo = strCardNo & ChrMod
            '    Next
            'End If

            disdiscount = ddldiscount.SelectedItem.Value + "." + ddldiscountdecimal.SelectedItem.Value
            'disdiscount = ddldiscount.SelectedItem.Value

            Dim intClientCoIndivIDParam As SqlParameter
            Dim intClientCoIndivID As Int32
            cmd = New SqlCommand("procAddindvmaster_New", MyConnection)
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.Add("@fname", txtfname.Text)
            cmd.Parameters.Add("@lname", txtlname.Text)
            cmd.Parameters.Add("@clientcoid", ddlcompany.SelectedItem.Value)
            cmd.Parameters.Add("@phone1", txtph1.Text)
            
            cmd.Parameters.Add("@emailid", txtemail.Text)
            
            cmd.Parameters.Add("@discountpc", disdiscount)

            cmd.Parameters.Add("@paymentterms", ddlpayment.SelectedItem.Value)

            cmd.Parameters.Add("@remarks", txtarearemarks.Text)

            cmd.Parameters.Add("@active", get_YNvalue(chkActive))
            cmd.Parameters.Add("@IsVIP", get_YNvalue(chkIsVIP))
            cmd.Parameters.Add("@createdby", Session("loggedin_user"))






            intuniqcheck = cmd.Parameters.Add("@uniqcheckval", SqlDbType.Int)
            intuniqcheck.Direction = ParameterDirection.Output
            intClientCoIndivIDParam = cmd.Parameters.Add("@ClientCoIndivID", SqlDbType.Int)
            intClientCoIndivIDParam.Direction = ParameterDirection.Output


            '  Try
            MyConnection.Open()
            cmd.ExecuteNonQuery()

            intuniqvalue = cmd.Parameters("@uniqcheckval").Value

            If intuniqvalue = 0 Then
                intClientCoIndivID = cmd.Parameters("@ClientCoIndivID").Value
            End If


            MyConnection.Close()

            If Not intuniqvalue = 0 Then
                lblErrorMsg.Visible = True
                lblErrorMsg.Text = "Client company individual already exists."
                Exit Sub
            Else
                lblErrorMsg.Visible = False

                ' Code commented by Binary Semantics on 28-March-07
                pnlmainform.Visible = False
                'pnl2ndform.Visible = False

                pnlconfirmation.Visible = True
                Dim sremailmessage As System.Text.StringBuilder
                sremailmessage = New System.Text.StringBuilder("Dear " & txtfname.Text & " " & txtlname.Text & ",<br> You have been assigned a unique ID as an individual, associated with your company at Carzonrent (I) Pvt Ltd.<br> ")
                sremailmessage.Append("Kindly give this unique ID to the Carzonrent customer service representative while requesting a booking.<br> ")
                sremailmessage.Append("Your unique ID is: " & intClientCoIndivID & " <br> ")
                sremailmessage.Append("You can also make your booking with CarzonRent online, using the following link:<br>https://www.carzonrent.com/CorporateSite/ClientLogin.asp?CoID=" & ddlcompany.SelectedItem.Value & " <br> ")
                sremailmessage.Append("Warm Regards, <br>")
                sremailmessage.Append("Carzonrent (I) Pvt Ltd")


                'procSendMail(sremailmessage.ToString, txtemail.Text)

                lblMessage.Text = "You have added the Client Company Individual successfully"
                hyplnkretry.Text = "Add another Client Company Individual"
                hyplnkretry.NavigateUrl = "ClientCoIndivAddForm.aspx"
                '   Catch
                '   End Try
            End If
        Else
            ' Email exist, display message
            lblErrorMsg.Visible = True
            lblErrorMsg.Text = "EMail ID already exists."
        End If


    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function

    Function procSendMail(ByVal strMessage As String, ByVal EMailTO As String) As Boolean

        Dim stresult As Boolean


        Dim StrMailBody As String
        StrMailBody = strMessage
        Dim objMail As MailMessage
        objMail = New MailMessage
        objMail.From = "reserve@carzonrent.com"
        objMail.To = EMailTO

        objMail.Subject = "Your unique individual ID at Carzonrent"

        objMail.Body = StrMailBody.ToString
        objMail.BodyFormat = MailFormat.Html
        System.Web.Mail.SmtpMail.SmtpServer = System.Configuration.ConfigurationSettings.AppSettings("SmtpServerName")
        Try
            System.Web.Mail.SmtpMail.Send(objMail)
            stresult = True

        Catch
            stresult = True
        End Try

        Return stresult

    End Function

    ' Code Added by Binary Semantics on 28-March-07
    Private Sub ddlcompany_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlcompany.SelectedIndexChanged
        'Dim dtrreader1 As SqlDataReader
        'Dim accessdata1 As clsutility
        'accessdata1 = New clsutility
        'dtrreader1 = accessdata1.funcGetSQLDataReader("select top 1 ClientCoAddID, Counter, ClientCoAddress from CORIntClientCoAddMaster where ClientCoID = " & ddlcompany.SelectedItem.Value & " order by Counter")
        'dtrreader1.Read()
        'txtadd.Text = Replace(dtrreader1("ClientCoAddress"), "'", "")
        'hdnClientCoAddID.Value = dtrreader1("ClientCoAddID")
        'dtrreader1.Close()
        'accessdata1.Dispose()

        ' Set the Payment combo value on changing of Client Company
        Dim dtrreader As SqlDataReader
        Dim accessdata As clsutility
        accessdata = New clsutility
        dtrreader = accessdata.funcGetSQLDataReader("select discountpc,paymentterms,ClientCoId  from CORIntClientCoMaster where ClientCoID = " & ddlcompany.SelectedItem.Value & " ")
        dtrreader.Read()

        Dim valDisc As String
        Dim valDiscDec As String
        Dim arrDisc As Array
        arrDisc = CStr(dtrreader("discountpc")).Split(".")

        If arrDisc.Length = 2 Then
            valDisc = arrDisc(0)
            valDiscDec = arrDisc(1)
            If valDisc = "" Then valDisc = "0"
            If valDiscDec = "00" Or valDiscDec = "" Then valDiscDec = "0"
            valDiscDec = Left(valDiscDec, 1)
        Else
            valDisc = arrDisc(0)
            valDiscDec = "0"
        End If

        autoselec_ddl(ddldiscount, dtrreader("discountpc"))
        ddldiscount.SelectedValue = valDisc
        ddldiscountdecimal.SelectedValue = valDiscDec
        If (dtrreader("paymentterms") = "CC" And dtrreader("ClientCoId") <> 686) Then
            ddlpayment.SelectedValue = dtrreader("paymentterms")
            ddlpayment.enabled = False
        Else
            ddlpayment.SelectedValue = dtrreader("paymentterms")
            ddlpayment.Enabled = True

            'ddlpayment.enabled = True
        End If

        dtrreader.Close()
        accessdata.Dispose()
    End Sub

End Class
