<%@ Page Language="vb" AutoEventWireup="false" Inherits="ClientCoIndivEditForm" src="ClientCoIndivEditForm.aspx.vb" %>
<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CarzonRent :: Internal software</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="JavaScript" src="../utilityfunction.js"></script>
		<script language="JavaScript" src="../JScripts/Datefunc.js"></script>
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
		<script language="javascript">

		function firstpage()
		{
				
				
				
				if (isNaN(document.forms[0].txtph1.value))
				{
					alert("The Mobile # should be numeric only");
					return false;
				}
				if (document.forms[0].txtph1.value.length != 10)
				{
					alert("The Mobile # must have 10 digits");
					return false;
				}
				if(document.forms[0].txtemail.value!="")
				{
					
					var theStr=document.forms[0].txtemail.value;
					var atIndex = theStr.indexOf('@'); 
					var dotIndex = theStr.indexOf('.', atIndex); 
					theSub = theStr.substring(0, dotIndex+1) 

					if ((atIndex < 1)||(atIndex != theStr.lastIndexOf('@'))||(dotIndex < atIndex + 2)||(theStr.length <= theSub.length)) 
					{ 
						alert("Email ID is not a valid Email ID");
						return false;
					}
				}
		
						
				var strvalues
				//strvalues = ('txtfname,txtlname,ddlcompany,txtadd,txtSTDCode,txtph1,txtph,txtCCLocCode,txtemail')
				
				// Code changed by Binary Semantics on 28-March-07
				strvalues = ('txtfname,txtlname,ddlcompany,txtph1,txtemail,')
				return checkmandatory(strvalues);
			}
			/* Code Commented by Binary Semantics on 28-March-07
			function secondpage()
			{
			var strvalues
			strvalues=('ddldiscount,ddlpayment')
			return checkmandatory(strvalues);
			}*/
			


		
		</script>
	    <style type="text/css">
<!--
.style1 {color: #aa0000}
-->
        </style>
</HEAD>
	<body >
		<form id="Form1" method="post" runat="server">
			<uc1:headerctrl id="Headerctrl1" runat="server"></uc1:headerctrl>
			<table align="center">
				<asp:panel id="pnlmainform" Runat="server">
					<TBODY>
						<TR>
							<TD align="center" colSpan="2"><B><U>Edit a Client Company Individual</U></B>
								<BR>
								<BR>							</TD>
						</TR>
						<TR>
							<TD align="center" colSpan="2">
								<asp:Label id="lblErrorMsg" runat="server" cssclass="subRedHead" visible="false"></asp:Label></TD>
						</TR>
						<TR>
							<TD>* First Name</TD>
							<TD>
								<asp:textbox id="txtfname" runat="server" MaxLength="50" CssClass="input" ReadOnly="true"></asp:textbox></TD>
						</TR>
						
						<TR>
							<TD>* Last Name</TD>
							<TD>
								<asp:textbox id="txtlname" runat="server" MaxLength="50" CssClass="input" ReadOnly="true"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>* Client Company</TD>
						  <TD><asp:DropDownList ID="ddlcompany" runat="server" CssClass="input" AutoPostBack="True" Enabled="false"></asp:DropDownList></TD>
						</TR>
						
						<TR>
							<TD>* Mobile #</TD>
							<TD>
								<asp:textbox id="txtph1" runat="server" MaxLength="10" CssClass="input"></asp:textbox></TD>
						</TR>
						
					
						<TR>
							<TD>* Email ID</TD>
							<TD>
								<asp:textbox id="txtemail" runat="server" MaxLength="100" CssClass="input"></asp:textbox></TD>
						</TR>
						
						
						<TR>
							<TD vAlign="top">Remarks(<SPAN class="shwText" id="shwMessage">0</SPAN>/2000 chars)</TD>
							<TD>
								<asp:textbox id="txtarearemarks" onkeydown="showLength(this.form.txtarearemarks,'document.all.shwMessage',2000)"
									onkeyup="showLength(this.form.txtarearemarks,'document.all.shwMessage',2000)" runat="server"
									MaxLength="2000" CssClass="input" Rows="3" Columns="50" TextMode="MultiLine"></asp:textbox></TD>
						</TR>
						
						<TR>
							<TD>Is V.I.P</TD>
							<TD>
								<asp:checkbox id="chkIsVIP" onclick="enableEsc();" runat="server" CssClass="input" Checked="False"></asp:checkbox></TD>
						</TR>
						<TR>
							<TD>Active</TD>
							<TD>
								<asp:checkbox id="chkActive" runat="server" CssClass="input" Checked="True"></asp:checkbox></TD>
						</TR> <!-- Change start by Binary on 28-March-07 -->
					
						<TR>
							<TD style="HEIGHT: 36px">* Discount %</TD>
							<TD style="HEIGHT: 36px">
								<asp:DropDownList id="ddldiscount" runat="server" cssclass="input"></asp:DropDownList>
								<asp:DropDownList id="ddldiscountdecimal" runat="server" cssclass="input"></asp:DropDownList></TD>
						</TR>
						<TR>
							<TD style="HEIGHT: 16px">* Payment Terms</TD>
							<TD style="HEIGHT: 16px">
								<asp:DropDownList id="ddlpayment" runat="server" cssclass="input">
									<asp:ListItem></asp:ListItem>
									<asp:ListItem Value="Cr">Credit</asp:ListItem>
									<asp:ListItem Value="CC">Credit Card</asp:ListItem>
								</asp:DropDownList></TD>
						</TR>
						
						<TR>
							<TD align="center" colSpan="2">
								<asp:button id="btnSubmit" runat="server" CssClass="input" Text="Submit"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;
								<asp:button id="btnReset1" runat="server" CssClass="input" Text="Reset"></asp:button></TD>
						</TR>
				</asp:panel>
				<!-- Change end by Binary on 28-March-07 --><asp:panel id="pnlconfirmation" Runat="server" Visible="False">
					<TR align="center">
						<TD colSpan="2">
							<asp:Label id="lblMessage" runat="server"></asp:Label></TD>
					</TR>
					<TR align="center">
						<TD colSpan="2">
							<asp:HyperLink id="hyplnkretry" runat="server"></asp:HyperLink></TD>
					</TR>
				</asp:panel></TBODY></table>
		</form>
	</body>
</HTML>
