<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Src="FacilitatorsList.aspx.vb" Inherits="FacilitatorsList"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CarzonRent :: Internal software Control panel</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
		<script language="JavaScript">
		   function SendFac(FName, MName, LName, Ph)
		   {
		      var FName = FName;
		      var MName = MName;
		      var LName = LName;
		      var Ph = Ph;
		      window.opener.document.Form1.txtalternatefname.value = FName;
		      window.opener.document.Form1.txtalternatemname.value = MName;
		      window.opener.document.Form1.txtalternatelname.value = LName;
		      window.opener.document.Form1.tctalternatecphone.value = Ph;
		      window.close();
		   }
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<table align="center">
				<tr>
					<td align="center"><STRONG><U>Select a facilitator as an alternate contact</U></STRONG></td>
				</tr>
				<tr>
					<td valign="top">
						<asp:table ID="tblRecDetail" HorizontalAlign="Center" Runat="server" BorderColor="#cccc99"
							BorderStyle="Solid" GridLines="both" CellSpacing="0" CellPadding="0">
							<asp:tablerow>
								<asp:tableheadercell>
									&nbsp;&nbsp;
<asp:linkbutton runat="server" Text="First Name" OnClick="SortGird" ID="Linkbutton1"></asp:linkbutton>&nbsp;&nbsp;
								</asp:tableheadercell>
								<asp:tableheadercell>
									&nbsp;&nbsp;
<asp:linkbutton runat="server" Text="Middle Name" OnClick="SortGird" ID="Linkbutton2"></asp:linkbutton>&nbsp;&nbsp;
								</asp:tableheadercell>
								<asp:tableheadercell>
									&nbsp;&nbsp;
<asp:linkbutton runat="server" Text="Last Name" OnClick="SortGird" ID="Linkbutton3"></asp:linkbutton>&nbsp;&nbsp;
								</asp:tableheadercell>
								<asp:tableheadercell>
									&nbsp;&nbsp;
<asp:linkbutton runat="server" Text="Phone" OnClick="SortGird" ID="Linkbutton4"></asp:linkbutton>&nbsp;&nbsp;
								</asp:tableheadercell>
								<asp:tableheadercell>
									&nbsp;&nbsp;
								</asp:tableheadercell>
							</asp:tablerow>
						</asp:table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>