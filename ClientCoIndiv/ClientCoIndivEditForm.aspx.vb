Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class ClientCoIndivEditForm
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtfname As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtmname As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtlname As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlcompany As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents txtdesig As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtadd As System.Web.UI.WebControls.TextBox
    'Protected WithEvents hdnClientCoAddID As System.Web.UI.HtmlControls.HtmlInputHidden
    'Protected WithEvents txtSTDCode As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtph1 As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtph As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtalternatefname As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtalternatemname As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtalternatelname As System.Web.UI.WebControls.TextBox
    'Protected WithEvents tctalternatecphone As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtfax As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtemail As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtDOB As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtAnniv As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtEmailID2 As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtResAdd As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtResPhone As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtEmpCode As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtCCLocCode As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtarearemarks As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkActive As System.Web.UI.WebControls.CheckBox
    Protected WithEvents btnproceed As System.Web.UI.WebControls.Button
    Protected WithEvents btnr As System.Web.UI.WebControls.Button
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents ddldiscount As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddldiscountdecimal As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlpayment As System.Web.UI.WebControls.DropDownList
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnReset As System.Web.UI.WebControls.Button
    Protected WithEvents pnl2ndform As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    'Protected WithEvents ddCCardType As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents txtCCN As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtExpDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents Panel2 As System.Web.UI.WebControls.Panel
    Protected WithEvents pnlCc As System.Web.UI.WebControls.Panel
    Protected WithEvents btnReset1 As System.Web.UI.WebControls.Button
    Protected WithEvents hidCCN As System.Web.UI.WebControls.TextBox

    'Protected WithEvents txtEscalation As System.Web.UI.WebControls.TextBox

    'Protected WithEvents txtCCNTemp As System.Web.UI.WebControls.TextBox
    Protected WithEvents Checkbox1 As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkIsVIP As System.Web.UI.WebControls.CheckBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Dim CCNoNew As String
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' Code commented by Binary semantics on 28-March-07
        'btnproceed.Attributes("onClick") = "return firstpage();"
        'btnSubmit.Attributes("onClick") = "return secondpage();"

        ' Code changed by Binary semantics on 28-March-07
        btnSubmit.Attributes("onClick") = "return firstpage();"

        ' Code added by BSL on 03-April-07, define event on company combo
        ddlcompany.Attributes("onkeydown") = "smartOptionFinderWithSubmit(this,event);"


        If Not Page.IsPostBack Then
            populateddl()
            Dim dtrreader As SqlDataReader
            Dim accessdata As clsutility
            accessdata = New clsutility
            Dim valDisc As String
            Dim valDiscDec As String
            Dim arrDisc As Array

            dtrreader = accessdata.funcGetSQLDataReader("select a.*, b.ClientCoAddress, c.CCtypeID, c. CCNo, c.ExpiryDate, isnull(a.discountpc,0) as discountpc1 from CORIntClientCoIndivMaster as a inner join CORIntClientCoAddMaster as b on a.ClientCoAddID = b.ClientCoAddID  left join CORIntCCClientCoIndiv as c on a.clientcoindivid = c.clientcoindivid  where a.clientcoindivid =" & Request.QueryString("id") & " ")
            dtrreader.Read()



            txtfname.Text = dtrreader("fname") & ""
            txtlname.Text = dtrreader("lname") & ""
            autoselec_ddl(ddlcompany, dtrreader("clientcoid"))
            txtph1.Text = dtrreader("phone1") & ""
            txtemail.Text = dtrreader("emailid") & ""
            arrDisc = CStr(dtrreader("discountpc1")).Split(".")

            If arrDisc.Length = 2 Then
                valDisc = arrDisc(0)
                valDiscDec = arrDisc(1)
                If valDisc = "" Then valDisc = "0"
                If valDiscDec = "00" Or valDiscDec = "" Then valDiscDec = "0"
                valDiscDec = Left(valDiscDec, 1)
            Else
                valDisc = arrDisc(0)
                valDiscDec = "0"
            End If

            autoselec_ddl(ddldiscount, valDisc)
            autoselec_ddl(ddldiscountdecimal, valDiscDec)


            ddlpayment.Items.FindByValue(dtrreader("paymentterms")).Selected = True
            If (dtrreader("paymentterms") = "CC" And dtrreader("ClientCoId") <> 686) Then
                ddlpayment.Enabled = False
            End If

            txtarearemarks.Text = dtrreader("remarks") & ""
            ' Code commented  on 09-jan-2012
            'If Not IsDBNull(dtrreader("CCTypeID")) Then
            '    ddCCardType.Items.FindByValue(dtrreader("CCTypeID")).Selected = True

            '    If Not IsDBNull(dtrreader("CCNo")) Then
            '        Dim i As Int32
            '        Dim ChrModNew As String

            '        For i = 1 To Len(dtrreader("CCNo"))
            '            ChrModNew = Chr(CInt(Asc(Mid(dtrreader("CCNo"), i, 1))) - 9)
            '            CCNoNew = CCNoNew & ChrModNew
            '        Next

            '        txtCCN.Text = "XXXXXXXXXXXX" & Right(CCNoNew, 4)
            '        txtCCNTemp.Text = dtrreader("CCNo")
            '        txtExpDate.Text = dtrreader("ExpiryDate") & ""
            '    End If
            'End If
            chkActive.Checked = dtrreader("active")
            chkIsVIP.Checked = dtrreader("IsVIP")

            dtrreader.Close()
            accessdata.Dispose()
        End If
    End Sub
    Sub populateddl()
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility
        ddlcompany.DataSource = objAcessdata.funcGetSQLDataReader("select ClientCoName ,ClientCoID   from CORIntClientCoMaster    where active=1 order by ClientCoName ")
        ddlcompany.DataValueField = "ClientCoID"
        ddlcompany.DataTextField = "ClientCoName"
        ddlcompany.DataBind()
        ddlcompany.Items.Insert(0, New ListItem("", ""))
        objAcessdata.funcpopulatenumddw(100, 0, ddldiscount)
        objAcessdata.funcpopulatenumddw(9, 0, ddldiscountdecimal)

        'ddCCardType.DataSource = objAcessdata.funcGetSQLDataReader("Select CCTypeID, CCTypeName from CORIntCCTypeMaster where Active = 1 Order by CCTypeName")
        'ddCCardType.DataValueField = "CCTypeID"
        'ddCCardType.DataTextField = "CCTypeName"
        'ddCCardType.DataBind()
        'ddCCardType.Items.Insert(0, New ListItem("", ""))
        'objAcessdata.Dispose()
    End Sub
   
    Function autoselec_ddl(ByVal ddlname As DropDownList, ByVal selectvalue As Int32)
        If Not IsDBNull(selectvalue) Then
            If Not selectvalue = 0 Then
                ddlname.Items.FindByValue(selectvalue).Selected = True
            Else
                ddlname.SelectedValue = 0
            End If
        End If
    End Function

    ' Code Added by Binary Semantics on 29-March-07
    Function IsEmailExist(ByVal email As String) As Boolean
        Dim emailCheck As Boolean
        emailCheck = False
        Dim dtrreader As SqlDataReader
        Dim accessdata As clsutility
        accessdata = New clsutility
        dtrreader = accessdata.funcGetSQLDataReader("SELECT Count(EmailID) as EMail FROM CORIntClientCoIndivMaster WHERE EmailID =  '" & email & "' and clientcoindivid=" & Request.QueryString("id"))
        dtrreader.Read()

        If dtrreader("EMail") > 0 Then
            emailCheck = True
        End If

        dtrreader.Close()
        accessdata.Dispose()

        Return emailCheck
    End Function

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        ' Code added by Binary Semantics on 29-March-07
        ' Call function to check for email exist or not
        Dim checkEmail As Boolean
        checkEmail = IsEmailExist(txtemail.Text)

        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand

        Dim intuniqcheck As SqlParameter
        Dim intuniqvalue As Int32
        'Dim intInc As Int32
        Dim disdiscount As String
        'Dim strCardNo As String
        'Dim ChrMod As String

        'If txtCCN.Text <> "" Then
        '    If Left(txtCCN.Text, 1) = "X" Then
        '        strCardNo = txtCCNTemp.Text
        '    Else
        '        For intInc = 1 To Len(txtCCN.Text)
        '            ChrMod = Chr(CInt(Asc(Mid(txtCCN.Text, intInc, 1))) + 9)
        '            strCardNo = strCardNo & ChrMod
        '        Next
        '    End If
        'End If

        disdiscount = ddldiscount.SelectedItem.Value + "." + ddldiscountdecimal.SelectedItem.Value


        cmd = New SqlCommand("procEditindvmaster_New", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("@rowid", Request.QueryString("id"))
        cmd.Parameters.Add("@fname", txtfname.Text)

        cmd.Parameters.Add("@lname", txtlname.Text)
        cmd.Parameters.Add("@clientcoid", ddlcompany.SelectedItem.Value)
        
        cmd.Parameters.Add("@phone1", txtph1.Text)

       
        cmd.Parameters.Add("@emailid", txtemail.Text)
        cmd.Parameters.Add("@discountpc", disdiscount)
        cmd.Parameters.Add("@paymentterms", ddlpayment.SelectedItem.Value)
       
        cmd.Parameters.Add("@remarks", txtarearemarks.Text)
        cmd.Parameters.Add("@IsVIP", get_YNvalue(chkIsVIP))
        cmd.Parameters.Add("@active", get_YNvalue(chkActive))
       
        cmd.Parameters.Add("@modifyby", Session("loggedin_user"))
       
        intuniqcheck = cmd.Parameters.Add("@uniqcheckval", SqlDbType.Int)
        intuniqcheck.Direction = ParameterDirection.Output

        'Try
        MyConnection.Open()
        cmd.ExecuteNonQuery()
        intuniqvalue = cmd.Parameters("@uniqcheckval").Value
        MyConnection.Close()
        If Not intuniqvalue = 0 Then
            lblErrorMsg.Visible = True
            lblErrorMsg.Text = "Client company individual already exist."
            Exit Sub
        Else

            lblErrorMsg.Visible = False

            ' Code commented by Binary Semantics on 28-March-07
            pnlmainform.Visible = False
            'pnl2ndform.Visible = False

            pnlconfirmation.Visible = True

            lblMessage.Text = "You have Updated the Client Company Individual successfully"
            hyplnkretry.Text = "Edit another Client Company Individual"
            hyplnkretry.NavigateUrl = "ClientCoIndivEditSearch.aspx"
            '   Catch
            '   End Try
        End If
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function

    ' Code Added by Binary Semantics on 28-March-07
    Private Sub ddlcompany_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlcompany.SelectedIndexChanged
        ' Set the Payment combo value on changing of Client Company
        Dim dtrreader As SqlDataReader
        Dim accessdata As clsutility
        accessdata = New clsutility
        dtrreader = accessdata.funcGetSQLDataReader("select discountpc,paymentterms  from CORIntClientCoMaster where clientcoid=" & ddlcompany.SelectedItem.Value & " ")
        dtrreader.Read()

        Dim valDisc As String
        Dim valDiscDec As String
        Dim arrDisc As Array
        arrDisc = CStr(dtrreader("discountpc")).Split(".")

        If arrDisc.Length = 1 Then
            valDisc = arrDisc(0)
            valDiscDec = arrDisc(1)
            If valDisc = "" Then valDisc = "0"
            If valDiscDec = "00" Or valDiscDec = "" Then valDiscDec = "0"
            valDiscDec = Left(valDiscDec, 1)
        Else
            valDisc = arrDisc(0)
            valDiscDec = "0"
        End If

        ddldiscount.SelectedValue = valDisc
        ddldiscountdecimal.SelectedValue = valDiscDec

        autoselec_ddl(ddldiscount, dtrreader("discountpc"))
        If dtrreader("paymentterms") = "CC" Then
            ddlpayment.SelectedValue = dtrreader("paymentterms")
            ddlpayment.Enabled = False
        Else
            ddlpayment.SelectedValue = dtrreader("paymentterms")
            ddlpayment.Enabled = True
        End If
        dtrreader.Close()
        accessdata.Dispose()
    End Sub
End Class
