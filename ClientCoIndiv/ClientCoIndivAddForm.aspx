<%@ Page Language="vb" AutoEventWireup="false" Inherits="ClientCoIndivAddForm" Src="ClientCoIndivAddForm.aspx.vb" %>

<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>CarzonRent :: Internal software</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">

    <script language="JavaScript" src="../utilityfunction.js"></script>

    <script language="JavaScript" src="../JScripts/Datefunc.js"></script>

    <link href="../HertzInt.css" type="text/css" rel="stylesheet">

    <script language="javascript">
		function firstpage()
		{
				 //Added By Rahul on 06-Apr-2010 Start
				var vtxtfname = document.forms[0].txtfname.value; 
				var vtxtlname = document.forms[0].txtlname.value;
				vtxtfname = vtxtfname.replace(/^\s+/,""); 	//Removes Left Blank Spaces
				vtxtfname = vtxtfname.replace(/\s+$/,""); 	//Removes Right Blank Spaces
				
				vtxtlname = vtxtlname.replace(/^\s+/,""); 	//Removes Left Blank Spaces
				vtxtlname = vtxtlname.replace(/\s+$/,""); 	//Removes Right Blank Spaces
		
				if (vtxtfname == "")
				{
					alert("Please Enter first name");
					document.forms[0].txtfname.focus();
					return false;
				}
				if (vtxtlname == "")
				{
					alert("Please Enter Last name");
					document.forms[0].txtlname.focus();					
					return false;
				}
			 //Added By Rahul on 06-Apr-2010 end
				
				
				if (isNaN(document.forms[0].txtph1.value))
				{
					alert("The Mobile # should be numeric only");
					return false;
				}
				if (document.forms[0].txtph1.value.length != 10)
				{
					alert("The Mobile # must have 10 digits");
					return false;
				}
				if(document.forms[0].txtemail.value!="")
				{
					var theStr=document.forms[0].txtemail.value;
					var atIndex = theStr.indexOf('@'); 
					var dotIndex = theStr.indexOf('.', atIndex); 
					theSub = theStr.substring(0, dotIndex+1) 

					if ((atIndex < 1)||(atIndex != theStr.lastIndexOf('@'))||(dotIndex < atIndex + 2)||(theStr.length <= theSub.length)) 
					{ 
						alert("Email ID is not a valid Email ID");
						return false;
					}
				}
//			
			 //Added By Rahul on 06-Apr-2010 start
				if (document.forms[0].ddlpayment.value =="") 
				{
					alert("Please select payment Terms");
					document.forms[0].ddlpayment.focus();
					return false;
				}	

				var strvalues
				
				strvalues = ('ddlcompany,txtph1,txtemail,ddldiscount,ddlpayment')
				return checkmandatory(strvalues);
			}
			
	
    </script>

</head>
<body >
    <form id="Form1" method="post" runat="server">
        <uc1:Headerctrl ID="Headerctrl1" runat="server"></uc1:Headerctrl>
        <table align="center">
            <tbody>
                <asp:Panel ID="pnlmainform" runat="server">
                    <tr>
                        <td align="center" colspan="2">
                            <b><u>Add a Client Company Individual</u></b>
                            <br>
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <asp:Label ID="lblErrorMsg" runat="server" Visible="false" CssClass="subRedHead"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                            * First Name</td>
                        <td>
                            <asp:TextBox ID="txtfname" runat="server" MaxLength="50" CssClass="input"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>
                            * Last Name</td>
                        <td>
                            <asp:TextBox ID="txtlname" runat="server" MaxLength="50" CssClass="input"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>
                            * Client Company</td>
                        <td>
                            <asp:DropDownList ID="ddlcompany" runat="server" CssClass="input" AutoPostBack="True">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td>
                            * Mobile #</td>
                        <td>
                            <asp:TextBox ID="txtph1" runat="server" MaxLength="10" CssClass="input"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>
                            * Email ID</td>
                        <td>
                            <asp:TextBox ID="txtemail" runat="server" MaxLength="100" CssClass="input"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td valign="top">
                            Remarks(<span class="shwText" id="shwMessage">0</span>/2000 chars)</td>
                        <td>
                            <asp:TextBox ID="txtarearemarks" onkeydown="showLength(this.form.txtarearemarks,'document.all.shwMessage',2000)"
                                onkeyup="showLength(this.form.txtarearemarks,'document.all.shwMessage',2000)"
                                runat="server" MaxLength="2000" CssClass="input" Rows="3" Columns="50" TextMode="MultiLine"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>
                            Is V.I.P</td>
                        <td>
                            <asp:CheckBox ID="chkIsVIP" runat="server" CssClass="input" Checked="False"></asp:CheckBox></td>
                    </tr>
                    <tr>
                        <td>
                            Active</td>
                        <td>
                            <asp:CheckBox ID="chkActive" runat="server" CssClass="input" Checked="True"></asp:CheckBox></td>
                    </tr>
                    <tr>
                        <td>
                            * Discount %</td>
                        <td>
                            <asp:DropDownList ID="ddldiscount" runat="server" CssClass="input">
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddldiscountdecimal" runat="server" CssClass="input">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td style="height: 42px">
                            * Payment Terms</td>
                        <td style="height: 42px">
                            <asp:DropDownList ID="ddlpayment" runat="server" CssClass="input">
                                <asp:ListItem Text="" Value="" />
                                <asp:ListItem Text="Credit" Value="Cr" />
                                <asp:ListItem Text="Credit Card" Value="CC" />
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <asp:Button ID="btnSubmit" runat="server" CssClass="input" Text="Submit"></asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Button ID="btnReset" runat="server" CssClass="input" Text="Reset"></asp:Button></td>
                    </tr>
                </asp:Panel>
                <asp:Panel ID="pnlconfirmation" runat="server" Visible="False">
                    <tr align="center">
                        <td colspan="2">
                            <asp:Label ID="lblMessage" runat="server"></asp:Label></td>
                    </tr>
                    <tr align="center">
                        <td colspan="2">
                            <asp:HyperLink ID="hyplnkretry" runat="server"></asp:HyperLink></td>
                    </tr>
                </asp:Panel>
            </tbody>
        </table>
    </form>
</body>
</html>
