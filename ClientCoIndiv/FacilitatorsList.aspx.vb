Imports commonutility
Imports System.text
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Imports System.Web.UI
Public Class FacilitatorsList
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tblRecDetail As System.Web.UI.WebControls.Table

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not Page.IsPostBack Then
            getvalue("indvname")
        End If
    End Sub
    Sub getvalue(ByVal strorderby As String)
        Dim strquery As StringBuilder
        Dim dtrreader As SqlDataReader
        Dim accessdata As clsutility
        accessdata = New clsutility
        strquery = New StringBuilder("select FName, MName, LName, Phone1 from CORIntFacilitatorMaster where Active=1 and ClientCoID = '"&Request.QueryString("CoID")&"' order by FName, LName")
        dtrreader = accessdata.funcGetSQLDataReader(strquery.ToString)
        While dtrreader.Read
            Dim Temprow As New TableRow
            Temprow.HorizontalAlign = HorizontalAlign.Center

            Dim Tempcell As New TableCell
            Tempcell.Controls.Add(New LiteralControl(dtrreader("FName") & ""))
            Temprow.Cells.Add(Tempcell)
            Dim strFName As String = Replace(dtrreader("FName"), " ", "&nbsp;")

            Dim Tempcel2 As New TableCell
            Tempcel2.Controls.Add(New LiteralControl(dtrreader("MName") & ""))
            Temprow.Cells.Add(Tempcel2)
            Dim strMName As String = Replace(dtrreader("MName"), " ", "&nbsp;")

            Dim Tempcel3 As New TableCell
            Tempcel3.Controls.Add(New LiteralControl(dtrreader("LName") & ""))
            Temprow.Cells.Add(Tempcel3)
            Dim strLName As String = Replace(dtrreader("LName"), " ", "&nbsp;")

            Dim Tempcel4 As New TableCell
            Tempcel4.Controls.Add(New LiteralControl(dtrreader("Phone1") & ""))
            Temprow.Cells.Add(Tempcel4)
            Dim strPh As String = Replace(dtrreader("Phone1"), " ", "&nbsp;")

            Dim Tempcel5 As New TableCell
            Tempcel5.Controls.Add(New LiteralControl("<a href='#' onClick=SendFac('"+ strFName +"','"+ strMName +"','"+ strLName +"','"+ strPh +"')>Select</a>"))
            Temprow.Cells.Add(Tempcel5)
            'adding the Tables rows to the table
            tblRecDetail.Rows.Add(Temprow)

        End While
        dtrreader.Close()
    End Sub
    Sub SortGird(ByVal sender As Object, ByVal e As System.EventArgs)
        Select Case sender.id
            Case "Linkbutton1"
                getvalue("FName")
            Case "Linkbutton2"
                getvalue("MName")
            Case "Linkbutton3"
                getvalue("LName")
            Case "Linkbutton4"
                getvalue("Phone1")
        End Select

    End Sub
End Class
