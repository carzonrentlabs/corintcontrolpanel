Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class CarCatEditForm
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtcatname As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtCorAmt As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddCatGrp As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtduty As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtdco As System.Web.UI.WebControls.TextBox
    Protected WithEvents txthotdco As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtdirpay As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtarearemarks As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkActive As System.Web.UI.WebControls.CheckBox
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnReset As System.Web.UI.WebControls.Button
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here

        btnSubmit.Attributes("onClick") = "return validate();"
        If Not Page.IsPostBack Then
            Dim dtrreader As SqlDataReader
            Dim accessdata As clsutility
            accessdata = New clsutility
            dtrreader = accessdata.funcGetSQLDataReader("select * from CORIntCarCatMaster where  carcatid=" & Request.QueryString("id") & " ")
            dtrreader.Read()
            txtcatname.Text = dtrreader("carcatname") & ""
            txtduty.Text = dtrreader("corporatedcopercent") & ""
            txtdco.Text = dtrreader("selfdrivedcopercent") & ""
            txthotdco.Text = dtrreader("hoteldcopercent") & ""
            txtdirpay.Text = dtrreader("ccdcopercent") & ""
            txtarearemarks.Text = dtrreader("remarks") & ""
            txtCorAmt.Text = dtrreader("cortax") & ""
            chkActive.Checked = dtrreader("active")
            ddCatGrp.Items.FindByValue(dtrreader("carcatgroup")).Selected = True
            dtrreader.Close()
            accessdata.Dispose()
        End If
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        Dim intuniqcheck As SqlParameter
        Dim intuniqvalue as int32
        cmd = New SqlCommand("procEditCarCat", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@rowid ", Request.QueryString("id"))
        cmd.Parameters.Add("@carcat ", txtcatname.Text)
        cmd.Parameters.Add("@carcatname ", ddCatGrp.SelectedItem.Value)
        cmd.Parameters.Add("@corporatedcopercent", txtduty.Text)
        cmd.Parameters.Add("@selfdrivedcopercent", txtdco.Text)
        cmd.Parameters.Add("@hoteldcopercent", txthotdco.Text)
        cmd.Parameters.Add("@ccdcopercent", txtdirpay.Text)
        cmd.Parameters.Add("@cortax", txtCorAmt.Text)
        cmd.Parameters.Add("@remarks ", txtarearemarks.Text)
        cmd.Parameters.Add("@active", get_YNvalue(chkActive))
        cmd.Parameters.Add("@modifiedby ", Session("loggedin_user"))

       intuniqcheck = cmd.Parameters.Add("@uniqcheckval", SqlDbType.Int)
       intuniqcheck.Direction = ParameterDirection.Output


        '  Try
        MyConnection.Open()
        cmd.ExecuteNonQuery()
        intuniqvalue=cmd.Parameters("@uniqcheckval").Value
        MyConnection.Close()
           if not intuniqvalue = 0 then
                lblErrorMsg.visible=true
                lblErrorMsg.text="Car category already exist."
                exit sub
            else
		        lblErrorMsg.visible=false
                pnlmainform.Visible = False
                pnlconfirmation.Visible = True

                lblMessage.Text = "You have updated the Car Category successfully"
                hyplnkretry.Text = "Edit another Car Category"
                hyplnkretry.NavigateUrl = "CarCatEditSearch.aspx"
            end if
        '   Catch
        '   End Try
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("carcateditform.aspx?id=" & Request.QueryString("id"))
    End Sub

End Class
