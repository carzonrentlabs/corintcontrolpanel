<%@ Page Language="vb" AutoEventWireup="false" Src="CarCatEditForm.aspx.vb" Inherits="CarCatEditForm"%>
<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CarzonRent :: Internal software</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="JavaScript" src="../utilityfunction.js"></script>
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
		<script language="javascript">
		function validate()
		{
			if(document.forms[0].txtcatname.value=="")
			{
				alert("Please make sure all the fields marked with * are filled in.")
				return false;
			}
			
			if(document.forms[0].ddCatGrp.value=="")
			{
				alert("Please make sure all the fields marked with * are filled in.")
				return false;
			}

			if(document.forms[0].txtduty.value=="")
			{
				alert("Please make sure all the fields marked with * are filled in.")
				return false;
			}
			
			if(isNaN(document.forms[0].txtduty.value))
					{
						alert("Corporate duty (DCO) revenue sharing % default should be numeric and upto two decimal digits only.")
						return false;	
					}
					else
					{
						if (parseFloat(document.forms[0].txtduty.value.indexOf("."))>0)
						{
							var FlotVal
							FlotVal=document.forms[0].txtduty.value.substr(parseFloat(document.forms[0].txtduty.value.indexOf(".")),document.forms[0].txtduty.value.length)
							if(parseFloat((FlotVal.length)-1)>2)
						{
						alert("Corporate duty (DCO) revenue sharing % default should be numeric and upto two decimal digits only")
						return false;
						}

						}
							
					}

			if(document.forms[0].txtdco.value=="")
				{
						alert("Please make sure all the fields marked with * are filled in.")
						return false;
				}
			if(isNaN(document.forms[0].txtdco.value))
					{
						alert("Self Drive (DCO) revenue sharing % default should be numeric and upto two decimal digits only.")
						return false;	
					}
					else
					{
						if (parseFloat(document.forms[0].txtdco.value.indexOf("."))>0)
						{
							var FlotVal
							FlotVal=document.forms[0].txtdco.value.substr(parseFloat(document.forms[0].txtdco.value.indexOf(".")),document.forms[0].txtdco.value.length)
							if(parseFloat((FlotVal.length)-1)>2)
						{
						alert("Self Drive (DCO) revenue sharing % default should be numeric and upto two decimal digits only")
						return false;
						}

						}
							
					}

			
			if(document.forms[0].txthotdco.value=="")
				{
						alert("Please make sure all the fields marked with * are filled in.")
						return false;
				}
				
			if(isNaN(document.forms[0].txthotdco.value))
					{
						alert("Hotel duty (DCO) revenue sharing % default should be numeric and upto two decimal digits only.")
						return false;	
					}
					else
					{
						if (parseFloat(document.forms[0].txthotdco.value.indexOf("."))>0)
						{
							var FlotVal
							FlotVal=document.forms[0].txthotdco.value.substr(parseFloat(document.forms[0].txthotdco.value.indexOf(".")),document.forms[0].txthotdco.value.length)
							if(parseFloat((FlotVal.length)-1)>2)
						{
						alert("Hotel duty (DCO) revenue sharing % default should be numeric and upto two decimal digits only")
						return false;
						}

						}
							
					}

			if(document.forms[0].txtdirpay.value=="")
					{
						alert("Please make sure all the fields marked with * are filled in.")
						return false;
					}
			if(isNaN(document.forms[0].txtdirpay.value))
					{
						alert("Direct Payment (Credit Card) (DCO) revenue sharing % default should be numeric and upto two decimal digits only.")
						return false;	
					}
					else
					{
						if (parseFloat(document.forms[0].txtdirpay.value.indexOf("."))>0)
						{
							var FlotVal
							FlotVal=document.forms[0].txtdirpay.value.substr(parseFloat(document.forms[0].txtdirpay.value.indexOf(".")),document.forms[0].txtdirpay.value.length)
							if(parseFloat((FlotVal.length)-1)>2)
						{
						alert("Direct Payment (Credit Card) (DCO) revenue sharing % default should be numeric and upto two decimal digits only")
						return false;
						}

						}
							
					}
		
		}
		</script>
	</HEAD>
	<body onload="OnLoadshowLength(document.forms[0].txtarearemarks.value,shwMessage)">
		<form id="Form1" method="post" runat="server">
			<uc1:Headerctrl id="Headerctrl1" runat="server"></uc1:Headerctrl>
			<table align="center">
				<asp:Panel ID="pnlmainform" Runat="server">
					<TBODY>
						<TR>
							<TD align="center" colSpan="2"><B><U>Edit a Car Category</U></B></TD>
						</TR>
						<tr><td colspan="2" align="center"><asp:Label id="lblErrorMsg" cssclass="subRedHead" visible="false" runat="server"></asp:Label></td></tr>
						<TR>
							<TD>* Category Name
							</TD>
							<TD>
								<asp:textbox id="txtcatname" runat="server" MaxLength="50" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>* Category Group</TD>
							<TD>
								<asp:DropDownList id="ddCatGrp" runat="server" CssClass="input">
									<asp:ListItem value=""></asp:ListItem>
									<asp:ListItem value="A">A</asp:ListItem>
									<asp:ListItem value="B">B</asp:ListItem>
									<asp:ListItem value="C">C</asp:ListItem>
									<asp:ListItem value="D">D</asp:ListItem>
									<asp:ListItem value="E">E</asp:ListItem>
									<asp:ListItem value="F">F</asp:ListItem>
									<asp:ListItem value="G">G</asp:ListItem>
									<asp:ListItem value="H">H</asp:ListItem>
									<asp:ListItem value="I">I</asp:ListItem>
									<asp:ListItem value="J">J</asp:ListItem>
									<asp:ListItem value="K">K</asp:ListItem>
									<asp:ListItem value="L">L</asp:ListItem>
									<asp:ListItem value="M">M</asp:ListItem>
									<asp:ListItem value="N">N</asp:ListItem>
									<asp:ListItem value="O">O</asp:ListItem>
									<asp:ListItem value="P">P</asp:ListItem>
									<asp:ListItem value="Q">Q</asp:ListItem>
									<asp:ListItem value="R">R</asp:ListItem>
									<asp:ListItem value="S">S</asp:ListItem>
									<asp:ListItem value="T">T</asp:ListItem>
									<asp:ListItem value="U">U</asp:ListItem>
									<asp:ListItem value="V">V</asp:ListItem>
									<asp:ListItem value="W">W</asp:ListItem>
									<asp:ListItem value="X">X</asp:ListItem>
									<asp:ListItem value="Y">Y</asp:ListItem>
									<asp:ListItem value="Z">Z</asp:ListItem>
								</asp:DropDownList></TD>
						</TR>
						<TR>
							<TD>* Corporate duty (DCO) revenue sharing % default</TD>
							<TD>
								<asp:textbox id="txtduty" runat="server" MaxLength="50" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>* Self Drive (DCO) revenue sharing % default*</TD>
							<TD>
								<asp:textbox id="txtdco" runat="server" MaxLength="50" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>* Hotel duty (DCO) revenue sharing % default</TD>
							<TD>
								<asp:textbox id="txthotdco" runat="server" MaxLength="50" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>* Direct Payment (Credit Card) (DCO) revenue sharing % default</TD>
							<TD>
								<asp:textbox id="txtdirpay" runat="server" MaxLength="50" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>* Cor Tax Amount</TD>
							<TD>
								<asp:textbox id="txtCorAmt" runat="server" MaxLength="50" CssClass="input"></asp:textbox></TD>
						</TR>
						
						<TR>
							<TD vAlign="top">Remarks(<SPAN class="shwText" id="shwMessage">0</SPAN>/2000 chars)</TD>
							<TD>
								<asp:textbox id="txtarearemarks" onkeydown="showLength(this.form.txtarearemarks,'document.all.shwMessage',2000)"
									onkeyup="showLength(this.form.txtarearemarks,'document.all.shwMessage',2000)" runat="server"
									MaxLength="2000" CssClass="input" TextMode="MultiLine" Columns="50" Rows="3"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>Active</TD>
							<TD>
								<asp:checkbox id="chkActive" runat="server" CssClass="input" Checked="True"></asp:checkbox></TD>
						</TR>
						<TR>
							<TD align="center" colSpan="2">
								<asp:button id="btnSubmit" runat="server" CssClass="input" Text="Submit"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;
								<asp:button id="btnReset" runat="server" CssClass="input" Text="Reset"></asp:button></TD>
						</TR>
				</asp:Panel>
				<asp:Panel ID="pnlconfirmation" Visible="False" Runat="server">
					<TR align="center">
						<TD colSpan="2"><BR>
							<BR>
							<BR>
							<BR>
							<INPUT type="hidden" name="txtarearemarks">
					<SPAN class="shwText" id="shwMessage"></SPAN>
							<asp:Label id="lblMessage" runat="server"></asp:Label></TD>
					</TR>
					<TR align="center">
						<TD colSpan="2">
							<asp:HyperLink id="hyplnkretry" runat="server"></asp:HyperLink></TD>
					</TR>
				</asp:Panel></TBODY>
			</table>
		</form>
	</body>
</HTML>
