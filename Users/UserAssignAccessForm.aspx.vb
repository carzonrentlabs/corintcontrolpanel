Imports commonutility
Imports System.text
Imports System.data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class UserAssignAccessForm
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblusername As System.Web.UI.WebControls.Label
    Protected WithEvents lstavailablemodule As System.Web.UI.WebControls.ListBox
    Protected WithEvents lstAssignedModules As System.Web.UI.WebControls.ListBox
    Protected WithEvents btnsubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnreset As System.Web.UI.WebControls.Button
    Protected WithEvents hdnSelectedlst As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        btnsubmit.Attributes("onClick") = "return postForm();"
        'Put user code to initialize the page here
        If Not Page.IsPostBack Then
            Dim dtrreader As SqlDataReader
            Dim accessdata As clsutility
            Dim Str As String
            accessdata = New clsutility
            'dtrreader = accessdata.funcGetSQLDataReader("select S.functionid,M.modulename+' - '+functionname as modulename from corintmodulemaster M, CORIntModuleFunctionMaster S where S.moduleid=M.moduleid and s.functionid not in(select modfuncid from CORIntSysUsersModAccessMaster where sysuserid=" & Request.QueryString("id") & ")")
            'Str = "select functionid, CORIntModuleMaster.modulename+' - '+CORIntModuleFunctionMaster.functionname as modulename from CORIntModuleFunctionMaster inner join CORIntModuleMaster on CORIntModuleMaster.Moduleid=CORIntModuleFunctionMaster.Moduleid   where  CORIntModuleFunctionMaster.functionid not in(select modfuncid from CORIntSysUsersModAccessMaster where sysuserid=" & Request.QueryString("id") & ") union  select functionid,functionname from CORIntModuleFunctionMaster where moduleid between 55 and 61  and  functionid not in(select modfuncid from CORIntSysUsersModAccessMaster where sysuserid=" & Request.QueryString("id") & " ) order by CORIntModuleMaster.modulename+' - '+CORIntModuleFunctionMaster.functionname "
            'Str = "select functionid, b.modulename+' - '+a.functionname as modulename from CORIntModuleFunctionMaster as a inner join CORIntModuleMaster as b on b.Moduleid=a.Moduleid where a.active = 1 and a.functionid not in(select modfuncid from CORIntSysUsersModAccessMaster as a  inner join corintsysUsersMaster as b  on a.sysUserId=b.sysUserId  where a.sysuserid= " & Request.QueryString("id") & " and ProviderId=" & Session("provider_Id") & ") and SupportsMultipleProvidersYN =case " & Session("provider_Id") & " when 1 then  SupportsMultipleProvidersYN else 1 end   order by b.modulename+' - '+a.functionname "
            Str = "select functionid, b.modulename+' - '+a.functionname as modulename from CORIntModuleFunctionMaster as a inner join CORIntModuleMaster as b on b.Moduleid=a.Moduleid where a.active = 1 and a.functionid not in(select modfuncid from CORIntSysUsersModAccessMaster as a  inner join corintsysUsersMaster as b  on a.sysUserId=b.sysUserId  where a.sysuserid= " & Request.QueryString("id") & ") and SupportsMultipleProvidersYN =case " & Session("provider_Id") & " when 1 then  SupportsMultipleProvidersYN else 1 end   order by b.modulename+' - '+a.functionname "
            dtrreader = accessdata.funcGetSQLDataReader(Str)
            lstavailablemodule.DataSource = dtrreader
            lstavailablemodule.DataValueField = "functionid"
            lstavailablemodule.DataTextField = "modulename"
            lstavailablemodule.DataBind()
            dtrreader.Close()

            dtrreader = accessdata.funcGetSQLDataReader("select S.functionid,M.modulename+' - '+functionname as modulename from corintmodulemaster M, CORIntModuleFunctionMaster S ,CORIntSysUsersModAccessMaster C,corintsysUsersMaster as D  where S.moduleid=M.moduleid and C.modfuncid=S.functionid and S.active = 1 and d.sysUserid=c.sysUserId and d.providerId=" & Session("provider_Id") & " and C.sysuserid=" & Request.QueryString("id") & " and SupportsMultipleProvidersYN =case " & Session("provider_Id") & " when 1 then  SupportsMultipleProvidersYN else 1 end  order by M.modulename+' - '+functionname ")
            lstAssignedModules.DataSource = dtrreader
            lstAssignedModules.DataValueField = "functionid"
            lstAssignedModules.DataTextField = "modulename"
            lstAssignedModules.DataBind()
            dtrreader.Close()


            dtrreader = accessdata.funcGetSQLDataReader("select fname+' '+mname+' '+lname as username from CORIntSysUsersMaster where providerid=" & Session("provider_Id") & " sysuserid=" & Request.QueryString("id") & "")
            If Not dtrreader Is Nothing Then
                dtrreader.Read()
                lblusername.Text = dtrreader("username")
                dtrreader.Close()
            End If


            accessdata.Dispose()
        End If
    End Sub

    Private Sub btnsubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsubmit.Click

        Dim arrModuleid As Array
        Dim intcounter As Int32
        arrModuleid = Request.Form("hdnSelectedlst").Split(",")

        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand

        cmd = New SqlCommand("delete from CORIntSysUsersModAccessMaster where sysuserid=" & Request.QueryString("id"), MyConnection)
        MyConnection.Open()
        cmd.ExecuteNonQuery()
        MyConnection.Close()


        For intcounter = 0 To UBound(arrModuleid)
            cmd = New SqlCommand("procAssignedRightd", MyConnection)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@sysuserid", Request.QueryString("id"))
            cmd.Parameters.Add("@modufuncid", arrModuleid(intcounter))
            MyConnection.Open()
            Try
                cmd.ExecuteNonQuery()
            Catch exp As SqlException
            End Try
            MyConnection.Close()

        Next

        pnlmainform.Visible = False
        pnlconfirmation.Visible = True

        lblMessage.Text = "You have Assigned the Rights successfully"
        hyplnkretry.Text = "Assign multiple module rights to another user"
        hyplnkretry.NavigateUrl = "UserAssignAccessSearch.aspx"
       
    End Sub

    Private Sub btnreset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnreset.Click
        Response.Redirect("userassignaccessform.aspx?id=" & Request.QueryString("id") & "")
    End Sub
End Class
