Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class ModuleAssignAccessSearch
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ddrightName As System.Web.UI.WebControls.DropDownList
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnReset As System.Web.UI.WebControls.Button

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Put user code to initialize the page here
        If Not Page.IsPostBack Then
            Dim dtrreader As SqlDataReader
            Dim accessdata As clsutility
            accessdata = New clsutility
            dtrreader = accessdata.funcGetSQLDataReader("select S.functionid,M.modulename+' - '+S.functionname as modulename from corintmodulemaster M inner join CORIntModuleFunctionMaster S on S.moduleid=M.moduleid inner join CORIntSysUsersModAccessMaster E  on  E.ModFuncID=S.functionID inner join CorintSysUsersMaster D on E.SysuserId=D.SysUserId where d.SysUserId = " & Session("loggedin_user") & " And d.ProviderId = " & Session("provider_Id") & " and SupportsMultipleProvidersYN =case " & Session("provider_Id") & " when 1 then  SupportsMultipleProvidersYN else 1 end order by modulename ")
            ddrightName.DataSource = dtrreader
            ddrightName.DataValueField = "functionid"
            ddrightName.DataTextField = "modulename"
            ddrightName.DataBind()
            ddrightName.Items.Insert(0, New ListItem("Any", -1))
            dtrreader.Close()
        End If
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Response.Redirect("ModuleAssignAccessResult.aspx?id=" & ddrightName.SelectedItem.Value)
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("ModuleAssignAccessSearch.aspx")
    End Sub
End Class
