Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Imports commonutility
Imports System.text
Imports System.Data.SqlClient
Imports System.Web.UI
Public Class ModuleAssignAccessResult
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tblRecDetail As System.Web.UI.WebControls.Table

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Put user code to initialize the page here
        If Not Page.IsPostBack Then
            getvalue("modulename")
        End If
    End Sub

    Sub getvalue(ByVal strorderby As String)
        Dim strquery As StringBuilder
        Dim dtrreader As SqlDataReader
        Dim accessdata As clsutility
        accessdata = New clsutility
        strquery = New StringBuilder("select S.functionid,M.modulename+' - '+S.functionname as modulename from corintmodulemaster M, CORIntModuleFunctionMaster S where S.moduleid=M.moduleid ")

        If Request.QueryString("id") <> "-1" Then
            strquery.Append(" and SupportsMultipleProvidersYN =case " & Session("provider_Id") & " when 1 then  SupportsMultipleProvidersYN else 1 end and S.functionid=" & Request.QueryString("id") & " ")
        Else
            strquery.Append(" and SupportsMultipleProvidersYN =case " & Session("provider_Id") & " when 1 then  SupportsMultipleProvidersYN else 1 end  ")
        End If
        strquery.Append(" order by " & strorderby & "")

        dtrreader = accessdata.funcGetSQLDataReader(strquery.ToString)
        While dtrreader.Read
            Dim Temprow As New TableRow
            Temprow.CssClass = "tabletext"

            Dim Tempcell As New TableCell
            Tempcell.Controls.Add(New LiteralControl(dtrreader("modulename") & ""))
            Temprow.Cells.Add(Tempcell)

            
            Dim Tempcel2 As New TableCell
            Tempcel2.Controls.Add(New LiteralControl("<a href=ModuleAssignAccessForm.aspx?ID=" & dtrreader("functionid") & " >Access Rights</a>"))
            Temprow.Cells.Add(Tempcel2)
            'adding the Tables rows to the table
            tblRecDetail.Rows.Add(Temprow)

        End While
        dtrreader.Close()
    End Sub
    Sub SortGird(ByVal sender As Object, ByVal e As System.EventArgs)
        Select Case sender.id
            Case "Linkbutton1"
                getvalue("modulename")
        End Select

    End Sub

End Class
