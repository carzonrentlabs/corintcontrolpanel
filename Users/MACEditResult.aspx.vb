Imports commonutility
Imports System.text
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Imports System.Web.UI
Public Class MACEditResult
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tblRecDetail As System.Web.UI.WebControls.Table

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not Page.IsPostBack Then
            getvalue("MACIDAddress")
        End If
    End Sub
    Sub getvalue(ByVal strorderby As String)
        Dim strquery As StringBuilder
        Dim dtrreader As SqlDataReader
        Dim accessdata As clsutility
        accessdata = New clsutility
        strquery = New StringBuilder("select MACIDAddress, AddressType, mid, ")
        strquery.Append(" case active when '1' then 'Active' when '0' then 'Not Active' end as active ")
        strquery.Append(" from CORIntMACAccess where 1=1 ")
        
        If Request.QueryString("id") <> "-1" Then
            strquery.Append(" and mid=" & Request.QueryString("id") & " ")
        End If
        strquery.Append(" order by " & strorderby & "")
        'Response.Write(strquery.ToString)
        dtrreader = accessdata.funcGetSQLDataReader(strquery.ToString)
        While dtrreader.Read
            Dim Temprow As New TableRow
            Temprow.HorizontalAlign = HorizontalAlign.Center

            Dim Tempcell As New TableCell
            Tempcell.Controls.Add(New LiteralControl(dtrreader("MACIDAddress") & ""))
            Temprow.Cells.Add(Tempcell)

            Dim Tempcell2 As New TableCell
            Tempcell2.Controls.Add(New LiteralControl(dtrreader("AddressType") & ""))
            Temprow.Cells.Add(Tempcell2)


            Dim Tempcell3 As New TableCell
            Tempcell3.Controls.Add(New LiteralControl(dtrreader("active") & ""))
            Temprow.Cells.Add(Tempcell3)

            Dim Tempcell4 As New TableCell
            Tempcell4.Controls.Add(New LiteralControl("<a href=EditMAC.aspx?ID=" & dtrreader("mid") & " >Edit</a>"))
            Temprow.Cells.Add(Tempcell4)
            'adding the Tables rows to the table
            tblRecDetail.Rows.Add(Temprow)

        End While
        dtrreader.Close()
    End Sub
    Sub SortGird(ByVal sender As Object, ByVal e As System.EventArgs)
        Select Case sender.id
            Case "Linkbutton1"
                getvalue("MACIDAddress")
            Case "Linkbutton2"
                getvalue("AddressType")
            Case "Linkbutton3"
                getvalue("active")
        End Select
    End Sub
End Class
