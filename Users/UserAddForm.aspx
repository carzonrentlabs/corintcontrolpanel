<%@ Page Language="vb" AutoEventWireup="false" Inherits="UserAddForm" Src="UserAddForm.aspx.vb" %>

<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>CarzonRent :: Internal software Control panel</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="../HertzInt.css" type="text/css" rel="stylesheet">
    <script language="JavaScript" src="../utilityfunction.js"></script>
    <script src="../JScripts/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script language="JavaScript">
        function validate_input() {

            if (document.forms[0].txtloginid.value == "") {
                alert("Please make sure all the fields marked with * are filled in.")
                document.forms[0].txtloginid.focus();
                return false;
            }
            else {
                if (document.forms[0].txtloginid.value.length < 5) {
                    alert("Login id must be five digits long")
                    return false;
                }
            }
            //if(document.forms[0].txtpwd.value=="")
            //	{
            //		alert("Please make sure all the fields marked with * are filled in.")
            //		return false;
            //	}
            //				else
            //	{
            //	if (document.forms[0].txtpwd.value.length<5)
            //	{
            //		alert("Password must be five digits long")
            //		return false;
            //	}
            //	}
            if (document.forms[0].txtfname.value == "") {
                alert("Please make sure all the fields marked with * are filled in.")
                document.forms[0].txtfname.focus();
                return false;
            }
            if (document.forms[0].txtlname.value == "") {
                alert("Please make sure all the fields marked with * are filled in.")
                document.forms[0].txtlname.focus();
                return false;
            }

            if (document.forms[0].txtph1.value == "") {
                alert("Please make sure all the fields marked with * are filled in.")
                document.forms[0].txtph1.focus();
                return false;
            }

            if (document.forms[0].txtemail.value == "") {
                alert("Please make sure all the fields marked with * are filled in.")
                document.forms[0].txtemail.focus();
                return false;
            }

            if (document.forms[0].txtemail.value != "") {

                var theStr = document.forms[0].txtemail.value;
                var atIndex = theStr.indexOf('@');
                var dotIndex = theStr.indexOf('.', atIndex);
                theSub = theStr.substring(0, dotIndex + 1)
                if ((atIndex < 1) || (atIndex != theStr.lastIndexOf('@')) || (dotIndex < atIndex + 2) || (theStr.length <= theSub.length)) {
                    alert("Unit Email ID is not a valid Email ID");
                    document.forms[0].txtemail.focus();
                    return false;
                }
            }
            if (document.forms[0].ddlDesig.value == "") {
                alert("Please make sure all the fields marked with * are filled in.")
                document.forms[0].ddlDesig.focus();
                return false;
            }
            if (document.forms[0].ddreport.value == "") {
                alert("Please make sure all the fields marked with * are filled in.")
                document.forms[0].ddreport.focus();
                return false;
            }

            if (document.forms[0].chkoutsider.checked != true) {
                if (document.forms[0].ddunit.value == "") {
                    alert("Please select the unit Reports to user belongs to")
                    document.forms[0].chkoutsider.focus();
                    return false;
                }

            }
            if (document.forms[0].ddaccess.value == "") {
                alert("Please make sure all the fields marked with * are filled in.")
                document.forms[0].ddaccess.focus();
                return false;
            }


        }
    </script>
    <script type="text/javascript">
        $(function () {

            $("#<%=txtph1.ClientID%>,#<%=txtphone2.ClientID%>").keyup(function () {
                var controlId = this.id;
                var jsControlId = "#" + controlId;
                var strPass = $(jsControlId).val();
                var strLength = strPass.length;
                var lchar = strPass.charAt((strLength) - 1);
                var cCode = CalcKeyCode(lchar);
                if (cCode < 46 || cCode > 57 || cCode == 47) {
                    var myNumber = strPass.substring(0, (strLength) - 1);
                    $(jsControlId).val(myNumber);
                    alert("Enter only numeric value.")
                }
            });

            // $("#<%=txtph1.ClientID%>,#<%=txtphone2.ClientID%>").focusout(function () {
            $("#<%=btnSubmit.ClientID%>").click(function () {
                if ($("#<%=txtph1.ClientID%>").val() != "" && $("#<%=txtph1.ClientID%>").val().length < 10) {
                    alert("Mobile number should not be less than 10 digit.");
                    $("#<%=txtph1.ClientID%>").focus();
                    return false;
                }
                else if ($("#<%=txtph1.ClientID%>").val() != "" && isNaN($("#<%=txtph1.ClientID%>").val())) {
                    alert("Mobile number should be numeric");
                    $("#<%=txtph1.ClientID%>").focus();
                    return false;
                }
            });
        });
    function CalcKeyCode(aChar) {
        var character = aChar.substring(0, 1);
        var code = aChar.charCodeAt(0);
        return code;
    }
    </script>
</head>
<body>
    <form id="Form1" method="post" runat="server">
        <uc1:Headerctrl ID="Headerctrl1" runat="server"></uc1:Headerctrl>
        <table align="center">
            <asp:Panel ID="pnlmainform" runat="server">
                <tbody>
                    <tr>
                        <td align="center" colspan="2">
                            <b><u>Add a User</u></b>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <asp:Label ID="lblerrmsg" runat="server" CssClass="subRedHead" Visible="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>* Login ID
                        </td>
                        <td>
                            <asp:TextBox ID="txtloginid" runat="server" CssClass="input" MaxLength="20"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>* Password
                        </td>
                        <td>
                            <asp:TextBox ID="txtpwd" runat="server" CssClass="input" MaxLength="20" TextMode="Password"
                                Enabled="False"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>* First Name
                        </td>
                        <td>
                            <asp:TextBox ID="txtfname" runat="server" CssClass="input" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Middle Name
                        </td>
                        <td>
                            <asp:TextBox ID="txtmidname" runat="server" CssClass="input" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>* Last Name
                        </td>
                        <td>
                            <asp:TextBox ID="txtlname" runat="server" CssClass="input" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Address
                        </td>
                        <td>
                            <asp:TextBox ID="txtaddress" runat="server" CssClass="input" MaxLength="250"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>* Phone 1
                        </td>
                        <td>
                            <asp:TextBox ID="txtph1" runat="server" CssClass="input" MaxLength="20"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Phone 2
                        </td>
                        <td>
                            <asp:TextBox ID="txtphone2" runat="server" CssClass="input" MaxLength="20"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>* Email ID
                        </td>
                        <td>
                            <asp:TextBox ID="txtemail" runat="server" CssClass="input" MaxLength="100"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>* Employee Code
                        </td>
                        <td>
                            <asp:TextBox ID="txtEmpCode" runat="server" CssClass="input" MaxLength="100"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>* Designation
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlDesig" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>* Reports to
                        </td>
                        <td>
                            <asp:DropDownList ID="ddreport" runat="server" CssClass="input">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>Unit
                        </td>
                        <td>
                            <asp:DropDownList ID="ddunit" runat="server" CssClass="input">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Outsider (Is the user a direct ,<br>
                            COR employee?<br>
                            Eg. if the user is a call centre exec,<br>
                            please put a tick in the box)
                        </td>
                        <td valign="top">
                            <asp:CheckBox ID="chkoutsider" runat="server" CssClass="input"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">Access Type*
                        </td>
                        <td>
                            <asp:DropDownList ID="ddaccess" runat="server" CssClass="input">
                                <asp:ListItem Text="HQ" Value='HQ' />
                                <asp:ListItem Text="Region" Value='RN' />
                                <asp:ListItem Text="City" Value='CT' />
                                <asp:ListItem Text="Service Unit" Value='SU' />
                                <asp:ListItem Text="Call Centre" Value='CC' />
                                <asp:ListItem Text="Regional Manager" Value='RM' />
                                <asp:ListItem Text="Branch Credit Controler" Value='BCC' />
                                <asp:ListItem Text="Head Credit Controler" Value='HCC' />
                                <asp:ListItem Text="Sales Person" Value='SP' />
                                <asp:ListItem Text="Branch Accountant" Value='BA' />
                                <asp:ListItem Text="Tely Caller" Value='TC' />
                                <asp:ListItem Text="Branch Manager" Value='BM' />
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">RelationShip Type
                        </td>
                        <td>
                            <asp:DropDownList ID="ddrelation" runat="server" CssClass="input">
                                <asp:ListItem Text="Relation Ship Manager" Value='R' />
                                <asp:ListItem Text="National Account Manager" Value='N' />
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">Remarks(<span class="shwText" id="shwMessage">0</span>/2000 chars)
                        </td>
                        <td>
                            <asp:TextBox ID="txtarearemarks" onkeydown="showLength(this.form.txtarearemarks,'document.all.shwMessage',2000)"
                                onkeyup="showLength(this.form.txtarearemarks,'document.all.shwMessage',2000)"
                                runat="server" CssClass="input" MaxLength="2000" TextMode="MultiLine" Rows="3"
                                Columns="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Active
                        </td>
                        <td>
                            <asp:CheckBox ID="chkActive" runat="server" CssClass="input" Checked="True"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td>By Pass
                        </td>
                        <td>
                            <asp:CheckBox ID="chkbypass" runat="server" CssClass="input" Checked="false"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <asp:Button ID="btnSubmit" runat="server" CssClass="input" Text="Submit"></asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Button ID="btnReset" runat="server" CssClass="input" Text="Reset"></asp:Button>
                        </td>
                    </tr>
            </asp:Panel>
            <asp:Panel ID="pnlconfirmation" Visible="False" runat="server">
                <tr align="center">
                    <td colspan="2">
                        <br>
                        <br>
                        <br>
                        <br>
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr align="center">
                    <td colspan="2">
                        <asp:HyperLink ID="hyplnkretry" runat="server"></asp:HyperLink>
                    </td>
                </tr>
            </asp:Panel>
            </TBODY>
        </table>
    </form>
</body>
</html>
