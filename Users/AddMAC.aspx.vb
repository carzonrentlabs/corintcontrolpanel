Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class AddMAC
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    Protected WithEvents txtAccessMACIPs As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkActive As System.Web.UI.WebControls.CheckBox
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnReset As System.Web.UI.WebControls.Button
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents ddaddress As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddAddressType As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblerrmsg As System.Web.UI.WebControls.Label
    Protected WithEvents txtDesc As System.Web.UI.WebControls.TextBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btnSubmit.Attributes("onClick") = "return validate_input();"
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        cmd = New SqlCommand("procInsertMACAddress", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("@MACIDAddress", txtAccessMACIPs.Text)
        cmd.Parameters.Add("@AddressType", ddAddressType.SelectedItem.Value)
        cmd.Parameters.Add("@txtDesc", txtDesc.Text)
        cmd.Parameters.Add("@active", get_YNvalue(chkActive))
        cmd.Parameters.Add("@createdby ", Session("loggedin_user"))

        Try
            MyConnection.Open()
            cmd.ExecuteNonQuery()
            MyConnection.Close()
            pnlmainform.Visible = False
            pnlconfirmation.Visible = True

            lblMessage.Text = "You have added the IP or MAC Address successfully"
            hyplnkretry.Text = "Add another IP or MAC Address"
            hyplnkretry.NavigateUrl = "AddMAC.aspx"
        Catch
            lblerrmsg.Visible = True
            lblerrmsg.Text = "IP or MAC already exist"
        End Try
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("AddMAC.aspx")
    End Sub

End Class
