<%@ Page Language="vb" AutoEventWireup="false" Src="UserRightsCopySearch.aspx.vb" Inherits="UserRightsCopySearch"%>
<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CarzonRent :: Internal software Control panel</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="JavaScript" src="../utilityfunction.js"></script>
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
		<script language="javascript">
		function validation()
		{
			if(document.forms[0].ddlUserFrom.value==document.forms[0].ddlUserTo.value)
			{
				alert("From and To users should be different from each other.")
				return false;
			}
			var strvalues;
			strvalues=('ddlUserFrom,ddlUserTo');
			return checkmandatory(strvalues);
		}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<uc1:headerctrl id="Headerctrl1" runat="server"></uc1:headerctrl>
			<TABLE id="Table1" align="center">
				<asp:panel id="pnlmainform" Runat="server">
					<TBODY>
						<TR>
							<TD align="center" colSpan="2"><STRONG><U>Assign user rights</U></STRONG>
							</TD>
						</TR>
						<tr><td colspan="2" align="center"><asp:Label id="lblErrorMsg" cssclass="subRedHead" visible="false" runat="server"></asp:Label></td></tr>
						<TR>
							<TD align="center" colSpan="2">
								<asp:Label id="lblerr" CssClass="subRedHead" runat="server" Visible="False"></asp:Label></TD>
						</TR>
						<TR>
							<TD><b>FROM</b>
							</TD>
							<TD>
								<asp:DropDownList id="ddlUserFrom" runat="server" CssClass="input"></asp:DropDownList></TD>
						</TR>
						<TR>
							<TD>&nbsp;
							</TD>
							<TD>
                     </TD>
						</TR>
						<TR>
							<TD><b>TO</b>
							</TD>
							<TD>
								<asp:DropDownList id="ddlUserTo" runat="server" CssClass="input"></asp:DropDownList></TD>
						</TR>
						<TR>
							<TD>&nbsp;
							</TD>
							<TD>
                     </TD>
						</TR>
						<TR>
							<TD align="center" colSpan="2">
								<asp:button id="btnsubmit" runat="server" CssClass="button" Text="Submit"></asp:button>&nbsp;&nbsp;
								<asp:button id="btnreset" runat="server" CssClass="button" Text="Reset" CausesValidation="False"></asp:button></TD>
						</TR>
				</asp:panel><asp:panel id="pnlconfirmation" Runat="server" Visible="False">
					<TR align="center">
						<TD colSpan="2">
							<asp:Label id="lblMessage" runat="server"></asp:Label></TD>
					</TR>
					<TR align="center">
						<TD colSpan="2">
							<asp:HyperLink id="hyplnkretry2" runat="server"></asp:HyperLink><br><br></TD>
					</TR>
					<TR align="center">
						<TD colSpan="2">
							<asp:HyperLink id="hyplnkretry" runat="server"></asp:HyperLink></TD>
					</TR>
				</asp:panel></TBODY></TABLE>
		</form>
	</body>
</HTML>
