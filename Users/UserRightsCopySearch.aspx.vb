Imports commonutility
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
'Imports System.EnterpriseServices
Imports System.Configuration
Imports System
Public Class UserRightsCopySearch
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents hyplnkretry2 As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    Protected WithEvents ddlUserFrom As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlUserTo As System.Web.UI.WebControls.DropDownList
    Protected WithEvents btnsubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnreset As System.Web.UI.WebControls.Button

   Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    Protected WithEvents lblerr As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btnsubmit.Attributes("onClick") = "return validation();"
        If Not Page.IsPostBack Then
            populateddl()
        End If
    End Sub
    Sub populateddl()
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility

        ddlUserFrom.DataSource = objAcessdata.funcGetSQLDataReader("select SysUserID, FName + ' ' + LName + ' - ' + LoginID as UserName from CORIntSysUsersMaster where providerId=" & Session("provider_Id") & " and SysUserID <> 1 order by FName, LName, LoginID")
        ddlUserFrom.DataValueField = "SysUserID"
        ddlUserFrom.DataTextField = "UserName"
        ddlUserFrom.DataBind()
        ddlUserFrom.Items.Insert(0, New ListItem("", ""))

        ddlUserTo.DataSource = objAcessdata.funcGetSQLDataReader("select SysUserID, FName + ' ' + LName + ' - ' + LoginID as UserName from CORIntSysUsersMaster where providerId=" & Session("provider_Id") & " and Active = 1 and SysUserID <> 1 order by FName, LName, LoginID")
        ddlUserTo.DataValueField = "SysUserID"
        ddlUserTo.DataTextField = "UserName"
        ddlUserTo.DataBind()
        ddlUserTo.Items.Insert(0, New ListItem("", ""))

    End Sub

    Private Sub btnsubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        Dim MyConnection As SqlConnection
        Dim MyConnection2 As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("corConnectString"))
        MyConnection2 = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("corConnectString"))

        Dim cmd As SqlCommand
        'Dim intMaxID
        cmd = New SqlCommand("delete from CORIntSysUsersModAccessMaster where SysUserID = " & ddlUserTo.SelectedItem.Value, MyConnection)
        MyConnection.Open()
        cmd.ExecuteNonQuery()
        MyConnection.Close()

        myConnection.Open()

        'Dim cmdMaxID As SqlCommand
        'cmdMaxID = New SqlCommand("select Max(SysUserModAccessID) as SysUserModAccessID from CORIntSysUsersModAccessMaster", MyConnection)

        'Dim myMaxIDReader as SqlDataReader
        'myMaxIDReader = cmdMaxID.ExecuteReader()

        'myMaxIDReader.Read()
        'intMaxID = myMaxIDReader("SysUserModAccessID")
        'MyConnection.Close()

        'myConnection.Open()

        'Dim cmdFrom As SqlCommand
        'cmdFrom = New SqlCommand("select ModFuncID from CORIntSysUsersModAccessMaster where SysUserID = " & ddlUserFrom.SelectedItem.Value, MyConnection)

         ' Create a DataReader to ferry information back from the database
        'Dim myReader as SqlDataReader
        'myReader = cmdFrom.ExecuteReader()
         'Iterate through the results
        'While myReader.Read()
        'intMaxID = intMaxID + 1
        Dim cmdTo As SqlCommand
        'cmdTo = New SqlCommand("insert into CORIntSysUsersModAccessMaster (SysUserModAccessID, SysUserID, ModFuncID) values ('"& intMaxID &"', '"& ddlUserTo.SelectedItem.Value &"', '"& myReader("ModFuncID") &"')", MyConnection2)
        'cmdTo = New SqlCommand("insert into CORIntSysUsersModAccessMaster (SysUserID, ModFuncID) values ('" & ddlUserTo.SelectedItem.Value & "', '" & myReader("ModFuncID") & "')", MyConnection2)
        cmdTo = New SqlCommand("insert into CORIntSysUsersModAccessMaster (SysUserID, ModFuncID) select " & ddlUserTo.SelectedItem.Value & ", ModFuncID from CORIntSysUsersModAccessMaster where SysUserID = " & ddlUserFrom.SelectedItem.Value, MyConnection2)
        MyConnection2.Open()
        cmdTo.ExecuteNonQuery()
        MyConnection2.Close()
        'End While

        MyConnection.Close()

        pnlmainform.Visible = False
        pnlconfirmation.Visible = True

        lblMessage.Text = "You have duplicated the user rights successfully."
        hyplnkretry.Text = "Duplicate user rights from one user to another"
        hyplnkretry.NavigateUrl = "UserRightsCopySearch.aspx"

    End Sub

    Private Sub btnreset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnreset.Click
        Response.Redirect("UserRightsCopySearch.aspx")
    End Sub
End Class