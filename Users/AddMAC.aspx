<%@ Page Language="vb" AutoEventWireup="false" src="AddMAC.aspx.vb" Inherits="AddMAC"%>
<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CarzonRent :: Internal software Control panel</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../utilityfunction.js"></script>
		<script language="JavaScript">
		function validate_input()
		{
				if(document.forms[0].txtAccessMACIPs.value=="")
				{
					alert("Please make sure all the fields marked with * are filled in.")
					return false;
				}
		}
		
		</script>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<uc1:headerctrl id="Headerctrl1" runat="server"></uc1:headerctrl>
			<table align="center" style="WIDTH: 422px; HEIGHT: 243px">
				
				<asp:panel id="pnlmainform" Runat="server">
					<TBODY>
						<TR>
							<TD align="center" colSpan="2"><B><U>Add a MAC or IP Address</U></B></TD>
						</TR>
						<TR>
							<TD></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD style="HEIGHT: 25px" align="center" colSpan="2">
								<asp:Label id="lblerrmsg" runat="server" Visible="False" CssClass="subRedHead"></asp:Label></TD>
						</TR>
						<TR>
							<TD vAlign="top">Access IPs&nbsp;or MAC Address</TD>
							<TD style="HEIGHT: 25px">
								<P>
									<asp:textbox id="txtAccessMACIPs" runat="server" CssClass="input" Width="224px" MaxLength="50"></asp:textbox><BR>
								</P>
							</TD>
						</TR>
						<TR>
							<TD vAlign="top">Address Type*</TD>
							<TD style="HEIGHT: 25px">
								<asp:dropdownlist id="ddAddressType" runat="server" CssClass="input">
									<asp:listitem Text="MAC" Value='MAC' />
									<asp:listitem Text="IP" Value='IP' />
								</asp:dropdownlist></TD>
						</TR>
						<TR>
							<TD vAlign="top">Description</TD>
							<TD>
								<asp:TextBox id="txtDesc" runat="server" Width="224px" TextMode="MultiLine" Rows="6" Columns="15"
									Height="72px"></asp:TextBox></TD>
						</TR>
						<TR>
							<TD style="HEIGHT: 22px">Active</TD>
							<TD style="HEIGHT: 22px">
								<asp:checkbox id="chkActive" runat="server" CssClass="input" Checked="True"></asp:checkbox></TD>
						</TR>
						<TR>
							<TD align="center" colSpan="2">
								<asp:button id="btnSubmit" runat="server" CssClass="input" Text="Submit"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;
								<asp:button id="btnReset" runat="server" CssClass="input" Text="Reset"></asp:button></TD>
						</TR>
				</asp:panel>
				
				<asp:Panel ID="pnlconfirmation" Visible="False" Runat="server">
					<TR align="center">
						<TD colSpan="2">
							<asp:Label id="lblMessage" runat="server"></asp:Label></TD>
					</TR>
					<TR align="center">
						<TD colSpan="2">
							<asp:HyperLink id="hyplnkretry" runat="server"></asp:HyperLink></TD>
					</TR>
				</asp:Panel>
				
				</TBODY>
				</table>
		</form>
	</body>
</HTML>
