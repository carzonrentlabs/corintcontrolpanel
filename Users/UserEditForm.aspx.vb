Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
imports system.dbnull
Imports System
Public Class UserEditForm
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtloginid As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtpwd As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtfname As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtmidname As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtlname As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtaddress As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtph1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtphone2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtemail As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlDesig As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddreport As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddunit As System.Web.UI.WebControls.DropDownList
    Protected WithEvents chkoutsider As System.Web.UI.WebControls.CheckBox
    Protected WithEvents ddaccess As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtarearemarks As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkActive As System.Web.UI.WebControls.CheckBox
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    'Dim intRTID
    Protected WithEvents CheckBox1 As System.Web.UI.WebControls.CheckBox
    Protected WithEvents ddrelation As System.Web.UI.WebControls.DropDownList
    Protected WithEvents chkbypass As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkforcedoffroad As System.Web.UI.WebControls.CheckBox
    Protected WithEvents txtEmpCode As System.Web.UI.WebControls.TextBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Put user code to initialize the page here
        btnSubmit.Attributes("onClick") = "return validate_input();"
        If Not Page.IsPostBack Then
            Dim dtrreaderRT As SqlDataReader
            Dim accessdataRT As  commonutility.clsutility
            accessdataRT = New commonutility.clsutility
            dtrreaderRT = accessdataRT.funcGetSQLDataReader("select Active from CORIntSysUsersMaster where ProviderId=" & Session("provider_Id") & " and sysuserid IN (select ReportsToID from CORIntSysUsersMaster where sysuserid=" & Request.QueryString("id") & " )")
            dtrreaderRT.Read()

            populateddl()
            Dim dtrreader As SqlDataReader
            Dim accessdata As commonutility.clsutility
            accessdata = New commonutility.clsutility
            dtrreader = accessdata.funcGetSQLDataReader("select * from CORIntSysUsersMaster where ProviderId=" & Session("provider_Id") & " and sysuserid=" & Request.QueryString("id") & " ")
            dtrreader.Read()
            If Not dtrreaderRT Is Nothing And dtrreaderRT.HasRows Then

                txtloginid.Text = dtrreader("loginid") & ""
                'txtpwd.Text = dtrreader("userpwd") & ""
                txtfname.Text = dtrreader("fname") & ""
                txtmidname.Text = dtrreader("mname") & ""
                txtlname.Text = dtrreader("lname") & ""
                ddlDesig.Items.FindByValue(dtrreader("desigid")).Selected = True
                txtaddress.Text = dtrreader("homeaddress") & ""
                txtph1.Text = dtrreader("phone1") & ""
                txtphone2.Text = dtrreader("phone2") & ""
                txtemail.Text = dtrreader("emailid") & ""
                txtEmpCode.Text = dtrreader("Emp_Code") & ""
                'intRTID = dtrreader("reportstoid")
                If Not dtrreaderRT("Active") = False Then
                    Try
                        ddreport.Items.FindByValue(dtrreader("reportstoid")).Selected = True
                    Catch ex As Exception

                    End Try

                End If
                If Not IsDBNull(dtrreader("unitid")) Then
                    Try
                        ddunit.Items.FindByValue(dtrreader("unitid")).Selected = True
                    Catch ex As Exception
                    End Try

                End If

                chkoutsider.Checked = dtrreader("outsideryn")
                ddaccess.Items.FindByValue(Trim(dtrreader("accesstype"))).Selected = True
                ddrelation.Items.FindByValue(Trim(dtrreader("RelationShipType"))).Selected = True

                txtarearemarks.Text = dtrreader("remarks") & ""
                chkActive.Checked = dtrreader("active")
                chkbypass.Checked = dtrreader("ByPassYN")
                chkforcedoffroad.Checked = dtrreader("forcedoffroadaccessyn")
                dtrreader.Close()
                accessdata.Dispose()
            Else
                Response.Redirect("UserEditResult.aspx?N=NoRecord&id=" & Request.QueryString("id"))
            End If

        End If
    End Sub
    Sub populateddl()
        Dim objAcessdata As commonutility.clsutility
        objAcessdata = New commonutility.clsutility

        ddlDesig.DataSource = objAcessdata.funcGetSQLDataReader("select designame,desigid from CORIntDesigMaster where active=1  order by designame")
        ddlDesig.DataValueField = "desigid"
        ddlDesig.DataTextField = "designame"
        ddlDesig.DataBind()
        ddlDesig.Items.Insert(0, New ListItem("", ""))

        ddreport.DataSource = objAcessdata.funcGetSQLDataReader("select SysUserID,isnull(fname,'')+' '+isnull(mname,'')+' '+isnull(lname,'') as username from CORIntSysUsersMaster where active=1 and ProviderId=" & Session("provider_Id") & "  order by fname+'-'+mname+'-'+lname")
        ddreport.DataValueField = "SysUserID"
        ddreport.DataTextField = "username"
        ddreport.DataBind()
        ddreport.Items.Insert(0, New ListItem("", ""))

        ddaccess.Items.Insert(0, New ListItem("", ""))
        ddrelation.Items.Insert(0, New ListItem("", ""))

        ddunit.DataSource = objAcessdata.funcGetSQLDataReader("select UnitID,UnitName from CORIntUnitMaster where ProviderId=" & Session("provider_Id") & " and active=1 order by UnitName")
        ddunit.DataValueField = "UnitID"
        ddunit.DataTextField = "UnitName"
        ddunit.DataBind()
        ddunit.Items.Insert(0, New ListItem("", ""))

        objAcessdata.Dispose()

    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        Dim MyConnection As SqlConnection
        Dim changepwd As Int16
        MyConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        cmd = New SqlCommand("procUpdateSysuserMaster", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("@rowid", Request.QueryString("id"))
        cmd.Parameters.Add("@loginid", txtloginid.Text)
        If (CheckBox1.Checked = True) Then
            changepwd = 1
        Else
            changepwd = 0
        End If
        cmd.Parameters.Add("@userpwd", changepwd)
        cmd.Parameters.Add("@fname", txtfname.Text)
        cmd.Parameters.Add("@lname", txtlname.Text)
        cmd.Parameters.Add("@mname", txtmidname.Text)
        cmd.Parameters.Add("@homeaddress", txtaddress.Text)
        cmd.Parameters.Add("@phone1", txtph1.Text)
        cmd.Parameters.Add("@phone2 ", txtphone2.Text)
        cmd.Parameters.Add("@emailid ", txtemail.Text)
        cmd.Parameters.Add("@EmpCode", txtEmpCode.Text)
        cmd.Parameters.Add("@desigid ", ddlDesig.SelectedItem.Value)
        cmd.Parameters.Add("@reportstoid", ddreport.SelectedItem.Value)
        cmd.Parameters.Add("@unitid ", ddunit.SelectedItem.Value)
        cmd.Parameters.Add("@outsideryn ", get_YNvalue(chkoutsider))
        cmd.Parameters.Add("@accesstype", ddaccess.SelectedItem.Value)
        cmd.Parameters.Add("@remarks ", txtarearemarks.Text)
        cmd.Parameters.Add("@active", get_YNvalue(chkActive))
        cmd.Parameters.Add("@modifieddby ", Session("loggedin_user"))
        cmd.Parameters.Add("@RelationShipType", ddrelation.SelectedItem.Value)
        cmd.Parameters.Add("@ByPass", get_YNvalue(chkbypass))
        cmd.Parameters.Add("@ForcedoffroadAccessyn", get_YNvalue(chkforcedoffroad))
        Try
            MyConnection.Open()
            cmd.ExecuteNonQuery()
            MyConnection.Close()
            pnlmainform.Visible = False
            pnlconfirmation.Visible = True

            lblMessage.Text = "You have updated the User successfully"
            hyplnkretry.Text = "Edit another User"
            hyplnkretry.NavigateUrl = "UserEditSearch.aspx"
        Catch
        End Try
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function


End Class
