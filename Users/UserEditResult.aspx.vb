Imports commonutility
Imports System.text
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Imports System.Web.UI
Public Class UserEditResult
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tblRecDetail As System.Web.UI.WebControls.Table
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        lblMessage.Text = String.Empty
        If Not Page.IsPostBack Then
            getvalue("username")
        End If
    End Sub
    Sub getvalue(ByVal strorderby As String)
        Dim strquery As StringBuilder
        Dim dtrreader As SqlDataReader
        Dim accessdata As clsutility
        accessdata = New clsutility
        lblMessage.Text = String.Empty
        strquery = New StringBuilder("select S.fname+' '+isnull(S.mname,'')+' '+S.lname as username,S.sysuserid,S.loginid,S.unitid,Case S.accesstype when 'HQ' then 'HQ' when 'RN' then 'Region' when 'CT' then 'City' when 'SU' then 'Service Unit' when 'CC' then 'Call Center' when 'RM' then 'Regional Manager' when 'BCC' then 'Branch Credit Controler' when 'HCC' then 'Head Credit Controler' when 'SP' then 'Sales Person' when 'BA' then 'Branch Accountant' when 'TC' then 'Tely Caller' when 'BM' then 'Branch Manager' end as accesstype,")
        strquery.Append(" case S.active when '1' then 'Active' when '0' then 'Not Active' end as active,D.designame,U.unitname, R.fname+' '+isnull(R.mname,'')+' '+r.lname as reportsto  ")
        strquery.Append(" from CORIntSysUsersMaster S left outer join corintunitmaster U on u.unitid=S.unitid , corintdesigmaster D, CORIntSysUsersMaster R ")
        strquery.Append(" where(D.desigid = S.desigid And R.sysuserid = S.reportstoid) and s.ProviderId=" & Session("provider_Id") & " ")

        If Request.QueryString("id") <> "-1" Then
            strquery.Append(" and S.sysuserid=" & Request.QueryString("id") & " ")
        End If
        strquery.Append(" order by " & strorderby & "")
        'Response.Write(strquery.ToString)
        If Request.QueryString.HasKeys Then
            If Request.QueryString("N") = "NoRecord" Then
                lblMessage.Text = "Some value missing to edit."
                lblMessage.ForeColor = Drawing.Color.Red
            End If
        End If
        dtrreader = accessdata.funcGetSQLDataReader(strquery.ToString)
        While dtrreader.Read
            Dim Temprow As New TableRow
            Temprow.HorizontalAlign = HorizontalAlign.Center

            Dim Tempcell As New TableCell
            Tempcell.Controls.Add(New LiteralControl(dtrreader("username") & ""))
            Temprow.Cells.Add(Tempcell)

            Dim Tempcel2 As New TableCell
            Tempcel2.Controls.Add(New LiteralControl(dtrreader("loginid") & ""))
            Temprow.Cells.Add(Tempcel2)

            Dim Tempcel3 As New TableCell
            Tempcel3.Controls.Add(New LiteralControl(dtrreader("designame") & ""))
            Temprow.Cells.Add(Tempcel3)

            Dim Tempcel4 As New TableCell
            Tempcel4.Controls.Add(New LiteralControl(dtrreader("reportsto") & ""))
            Temprow.Cells.Add(Tempcel4)

            Dim Tempcel5 As New TableCell
            Tempcel5.Controls.Add(New LiteralControl(dtrreader("unitname") & ""))
            Temprow.Cells.Add(Tempcel5)

            Dim Tempcel6 As New TableCell
            Tempcel6.Controls.Add(New LiteralControl(dtrreader("accesstype") & ""))
            Temprow.Cells.Add(Tempcel6)


            Dim Tempcel7 As New TableCell
            Tempcel7.Controls.Add(New LiteralControl(dtrreader("active") & ""))
            Temprow.Cells.Add(Tempcel7)

            Dim Tempcel8 As New TableCell
            Tempcel8.Controls.Add(New LiteralControl("<a href=UserEditForm.aspx?ID=" & dtrreader("sysuserid") & " >Edit</a>"))
            Temprow.Cells.Add(Tempcel8)
            'adding the Tables rows to the table
            tblRecDetail.Rows.Add(Temprow)

        End While
        dtrreader.Close()
    End Sub
    Sub SortGird(ByVal sender As Object, ByVal e As System.EventArgs)
        Select Case sender.id
            Case "Linkbutton1"
                getvalue("username")
            Case "Linkbutton2"
                getvalue("S.loginid")
            Case "Linkbutton3"
                getvalue("D.designame")
            Case "Linkbutton4"
                getvalue("reportsto")
            Case "Linkbutton5"
                getvalue("U.unitname")
            Case "Linkbutton6"
                getvalue("accesstype")
            Case "Linkbutton7"
                getvalue("U.active")

        End Select

    End Sub
End Class
