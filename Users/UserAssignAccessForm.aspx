<%@ Page Language="vb" AutoEventWireup="false" Src="UserAssignAccessForm.aspx.vb"
    Inherits="UserAssignAccessForm" EnableEventValidation="false" %>

<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>CarzonRent :: Internal software Control panel</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="../HertzInt.css" type="text/css" rel="stylesheet">
    <script language="javascript">
        function assignmodules() {
            if (parseInt(document.forms[0].lstavailablemodule.selectedIndex, 10) >= 0) {

                for (i = document.forms[0].lstavailablemodule.length - 1; i >= 0; i--) {
                    if (document.forms[0].lstavailablemodule[i].selected) {
                        var oOption = document.createElement("OPTION");
                        document.forms[0].lstAssignedModules.options.add(oOption);
                        oOption.innerText = document.forms[0].lstavailablemodule.options[document.forms[0].lstavailablemodule.selectedIndex].text;
                        oOption.value = document.forms[0].lstavailablemodule.options[document.forms[0].lstavailablemodule.selectedIndex].value;
                        document.forms[0].lstavailablemodule.options[document.forms[0].lstavailablemodule.selectedIndex] = null;
                    }

                }
            }
            else { alert("Please select Module"); }
        }
        function removemodules() {
            if (parseInt(document.forms[0].lstAssignedModules.selectedIndex, 10) >= 0) {

                for (i = document.forms[0].lstAssignedModules.length - 1; i >= 0; i--) {
                    if (document.forms[0].lstAssignedModules[i].selected) {
                        var oOption = document.createElement("OPTION");
                        document.forms[0].lstavailablemodule.options.add(oOption);
                        oOption.innerText = document.forms[0].lstAssignedModules.options[document.forms[0].lstAssignedModules.selectedIndex].text;
                        oOption.value = document.forms[0].lstAssignedModules.options[document.forms[0].lstAssignedModules.selectedIndex].value;
                        document.forms[0].lstAssignedModules.options[document.forms[0].lstAssignedModules.selectedIndex] = null;
                    }
                }
            }
            else { alert("Please select Module"); }
        }

        function postForm() {
            var strTmpValues = new String();
            var bFlag = false;
            for (i = 0; i != document.forms[0].lstAssignedModules.length; i++) {
                strTmpValues += document.forms[0].lstAssignedModules.options[i].value;
                if (i + 1 != document.forms[0].lstAssignedModules.length) {
                    strTmpValues += ",";
                }
            }

            document.forms[0].hdnSelectedlst.value = strTmpValues;

            //alert(document.forms[0].hdnSelectedlst.value)
            //	if (strTmpValues == "")
            //		{
            //			alert("Please select Module")
            //			return false;
            ////		}
        }
    </script>
</head>
<body>
    <form id="Form1" method="post" runat="server">
    <uc1:Headerctrl ID="Headerctrl1" runat="server"></uc1:Headerctrl>
    <table align="center">
        <tbody>
            <asp:Panel ID="pnlmainform" runat="server">
                <tr>
                    <td align="center" colspan="3">
                        <strong><u>Assign multiple module rights to &nbsp;
                            <asp:Label ID="lblusername" runat="server" CssClass="input"></asp:Label></u></strong>
                        <input id="hdnSelectedlst" type="hidden" runat="server" name="hdnSelectedlst">
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        List of Available Rights<br>
                        <asp:ListBox ID="lstavailablemodule" runat="server" CssClass="input" Rows="10"></asp:ListBox>
                    </td>
                    <td valign="middle" align="center" colspan="1" rowspan="1">
                        <input class="formButton" id="btnPut" onclick="assignmodules()" type="button" value="  >>  Add       ">
                        <br>
                        <input class="formButton" id="btnRemove" onclick="removemodules()" type="button"
                            value="  <<  Remove">
                    </td>
                    <td align="center" colspan="1" rowspan="1">
                        List of Assigned Rights<br>
                        <asp:ListBox ID="lstAssignedModules" runat="server" CssClass="input" Rows="10"></asp:ListBox>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="3">
                        <asp:Button ID="btnsubmit" runat="server" CssClass="input" Text="Submit"></asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Button ID="btnreset" runat="server" CssClass="input" Text="Reset"></asp:Button>
                    </td>
                </tr>
            </asp:Panel>
            <asp:Panel ID="pnlconfirmation" Visible="False" runat="server">
                <tr align="center">
                    <td colspan="3">
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr align="center">
                    <td colspan="3">
                        <asp:HyperLink ID="hyplnkretry" runat="server"></asp:HyperLink>
                    </td>
                </tr>
            </asp:Panel>
        </tbody>
    </table>
    </form>
</body>
</html>
