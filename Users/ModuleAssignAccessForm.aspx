<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Src="ModuleAssignAccessForm.aspx.vb" Inherits="ModuleAssignAccessForm"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CarzonRent :: Internal software Control panel</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
				<script language="javascript">
	function assignmodules()
		{
		if (parseInt(document.forms[0].lstavailablemodule.selectedIndex,10) >= 0 )
		    {

			for(i = document.forms[0].lstavailablemodule.length - 1; i >= 0;i--)
			{
				if (document.forms[0].lstavailablemodule[i].selected)
				  {
					var oOption = document.createElement("OPTION");
					document.forms[0].lstAssignedModules.options.add(oOption);
					oOption.innerText = document.forms[0].lstavailablemodule.options[document.forms[0].lstavailablemodule.selectedIndex].text;
					oOption.value = document.forms[0].lstavailablemodule.options[document.forms[0].lstavailablemodule.selectedIndex].value;
					document.forms[0].lstavailablemodule.options[document.forms[0].lstavailablemodule.selectedIndex] = null ;
				}

			}
		  }
		else { alert("Please select Module");}
		}
	function removemodules()
		{
		if (parseInt(document.forms[0].lstAssignedModules.selectedIndex,10) >= 0 )
		    {

			for(i = document.forms[0].lstAssignedModules.length - 1; i >= 0;i--)
			{
				if (document.forms[0].lstAssignedModules[i].selected)
				  {
					var oOption = document.createElement("OPTION");
					document.forms[0].lstavailablemodule.options.add(oOption);
					oOption.innerText = document.forms[0].lstAssignedModules.options[document.forms[0].lstAssignedModules.selectedIndex].text;
					oOption.value = document.forms[0].lstAssignedModules.options[document.forms[0].lstAssignedModules.selectedIndex].value;
					document.forms[0].lstAssignedModules.options[document.forms[0].lstAssignedModules.selectedIndex] = null ;
				}
			}
		  }
		else { alert("Please select Module");}
		}
		
	function postForm()
	{
	var strTmpValues = new String();
	var bFlag  = false;
	for(i = 0; i != document.forms[0].lstAssignedModules.length;i++)
		{
			strTmpValues += document.forms[0].lstAssignedModules.options[i].value ;
			if (i + 1 !=  document.forms[0].lstAssignedModules.length)
				{
				strTmpValues += ",";
				}
		}

	document.forms[0].hdnSelectedlst.value  = strTmpValues;
	
	//alert(document.forms[0].hdnSelectedlst.value)
//	if (strTmpValues == "")
//		{
//			alert("Please select Module")
//			return false;
//		}
		}
		</script>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<uc1:Headerctrl id="Headerctrl1" runat="server"></uc1:Headerctrl>
			<table align="center">
				<TBODY>
				<asp:Panel ID="pnlmainform" Runat="server">
					<tr>
						<td align="center" colSpan="3"><STRONG><U>Assign rights for &nbsp;
									<asp:label id="lblusername" runat="server" CssClass="input"></asp:label></U></STRONG>
							<input id="hdnSelectedlst" type="hidden" runat="server" NAME="hdnSelectedlst"></td>
					</tr>
					
						<TR>
							<TD align="left">List of All System Users<BR>
								<asp:listbox id="lstavailablemodule" runat="server" CssClass="input" Rows="10"></asp:listbox></TD>
							<TD vAlign="middle" align="center" colSpan="1" rowSpan="1"><INPUT class="formButton" id="btnPut" onclick="assignmodules()" type="button" value="  >>  Add       ">
								<BR>
								<INPUT class="formButton" id="btnRemove" onclick="removemodules()" type="button" value="  <<  Remove">
							</TD>
							<TD align="center" colSpan="1" rowSpan="1">List of System User Assigned this Right<BR>
								<asp:listbox id="lstAssignedModules" runat="server" CssClass="input" Rows="10"></asp:listbox></TD>
						</TR>
						<TR>
							<TD align="center" colSpan="3">
								<asp:Button id="btnsubmit" runat="server" CssClass="input" Text="Submit"></asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;
								<asp:Button id="btnreset" runat="server" CssClass="input" Text="Reset"></asp:Button></TD>
						</TR>
					</asp:Panel>
					<asp:Panel ID="pnlconfirmation" Visible="False" Runat="server">
						<TR align="center">
							<TD colSpan="3"><BR>
								<BR>
								<BR>
								<BR>
								<BR>
								<asp:Label id="lblMessage" runat="server"></asp:Label></TD>
						</TR>
						<TR align="center">
							<TD colSpan="3">
								<asp:HyperLink id="hyplnkretry" runat="server"></asp:HyperLink></TD>
						</TR>
					</asp:Panel>
				</TBODY>
			</table>
		</form>
	</body>
</HTML>
