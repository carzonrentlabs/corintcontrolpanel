Imports commonutility
Imports System.text
Imports System.data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class ModuleAssignAccessForm
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblusername As System.Web.UI.WebControls.Label
    Protected WithEvents lstavailablemodule As System.Web.UI.WebControls.ListBox
    Protected WithEvents lstAssignedModules As System.Web.UI.WebControls.ListBox
    Protected WithEvents btnsubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnreset As System.Web.UI.WebControls.Button
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    Protected WithEvents hdnSelectedlst As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btnsubmit.Attributes("onClick") = "return postForm();"
        'Put user code to initialize the page here
        If Not Page.IsPostBack Then
            Dim dtrreader As SqlDataReader
            Dim accessdata As clsutility
            accessdata = New clsutility


            dtrreader = accessdata.funcGetSQLDataReader("select fname+' '+isnull(mname,'')+' '+lname as username,sysuserid from CORIntSysUsersMaster where ProviderId=" & Session("provider_Id") & " and CORIntSysUsersMaster.active=1 and loginid<>'admin' and sysuserid not in(select sysuserid from CORIntSysUsersModAccessMaster where modfuncid=" & Request.QueryString("id") & ") order by username")
            lstavailablemodule.DataSource = dtrreader
            lstavailablemodule.DataValueField = "sysuserid"
            lstavailablemodule.DataTextField = "username"
            lstavailablemodule.DataBind()
            dtrreader.Close()

            dtrreader = accessdata.funcGetSQLDataReader("select fname+' '+isnull(mname,'')+' '+lname as username,sysuserid from CORIntSysUsersMaster where CORIntSysUsersMaster.active=1 and ProviderId=" & Session("provider_Id") & " and loginid<>'admin' and sysuserid in(select sysuserid from CORIntSysUsersModAccessMaster where modfuncid=" & Request.QueryString("id") & ") order by username")
            lstAssignedModules.DataSource = dtrreader
            lstAssignedModules.DataValueField = "sysuserid"
            lstAssignedModules.DataTextField = "username"
            lstAssignedModules.DataBind()
            dtrreader.Close()

            dtrreader = accessdata.funcGetSQLDataReader("select M.modulename+' - '+functionname as modulename from corintmodulemaster M inner join  CORIntModuleFunctionMaster S on S.moduleid=M.moduleid inner join CORIntSysUsersModAccessMaster P on P.ModFuncID =S.FunctionID inner join CORIntSysUsersMaster as D on d.SysUserID =P.SysUserID where  d.SysUserId = " & Session("loggedin_user") & " And d.ProviderId = " & Session("provider_Id") & " and S.functionid =" & Request.QueryString("id") & "")
            If Not dtrreader Is Nothing Then
                dtrreader.Read()
                lblusername.Text = dtrreader("modulename")
                dtrreader.Close()
            End If
            accessdata.Dispose()
        End If
    End Sub
    Private Sub btnsubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsubmit.Click

        Dim arruserid As Array
        Dim intcounter As Int32
        arruserid = Request.Form("hdnSelectedlst").Split(",")

        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("corConnectString"))
        Dim cmd As SqlCommand

        cmd = New SqlCommand("delete from CORIntSysUsersModAccessMaster where modfuncid=" & Request.QueryString("id"), MyConnection)
        MyConnection.Open()
        cmd.ExecuteNonQuery()
        MyConnection.Close()


            For intcounter = 0 To UBound(arruserid)
                cmd = New SqlCommand("procAssignedRightsToUser", MyConnection)
                cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@sysuserid", arruserid(intcounter))
            cmd.Parameters.AddWithValue("@modufuncid", Request.QueryString("id"))
                MyConnection.Open()
                Try
                    cmd.ExecuteNonQuery()
                Catch exp As SqlException
                End Try
                MyConnection.Close()

            Next

            pnlmainform.Visible = False
            pnlconfirmation.Visible = True

            lblMessage.Text = "You have Assigned the Right successfully"
            hyplnkretry.Text = "Assign more single module rights to multiple users"
        hyplnkretry.NavigateUrl = "ModuleAssignAccessSearch.aspx"

    End Sub

    Private Sub btnreset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnreset.Click
        Response.Redirect("ModuleAssignAccessForm.aspx?id=" & Request.QueryString("id") & "")
    End Sub

End Class
