Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class UserAddForm
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtfname As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtmidname As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtlname As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlDesig As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtaddress As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtph1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtphone2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtemail As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtarearemarks As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkActive As System.Web.UI.WebControls.CheckBox
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnReset As System.Web.UI.WebControls.Button
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    Protected WithEvents txtloginid As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtpwd As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkoutsider As System.Web.UI.WebControls.CheckBox
    Protected WithEvents ddaccess As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddreport As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddunit As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblerrmsg As System.Web.UI.WebControls.Label
    Protected WithEvents ddrelation As System.Web.UI.WebControls.DropDownList

    Protected WithEvents chkbypass As System.Web.UI.WebControls.CheckBox
    Protected WithEvents txtEmpCode As System.Web.UI.WebControls.TextBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btnSubmit.Attributes("onClick") = "return validate_input();"
        If Not Page.IsPostBack Then
            populateddl()
        End If
    End Sub

    Sub populateddl()
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility

        ddlDesig.DataSource = objAcessdata.funcGetSQLDataReader("select designame,desigid from CORIntDesigMaster where active=1  order by designame")
        ddlDesig.DataValueField = "desigid"
        ddlDesig.DataTextField = "designame"
        ddlDesig.DataBind()
        ddlDesig.Items.Insert(0, New ListItem("", ""))

        ddreport.DataSource = objAcessdata.funcGetSQLDataReader("select SysUserID,isnull(fname,'')+' '+isnull(mname,'')+' '+isnull(lname,'') as username from CORIntSysUsersMaster where active=1 and providerId=" & Session("Provider_Id") & " order by fname+'-'+mname+'-'+lname")
        ddreport.DataValueField = "SysUserID"
        ddreport.DataTextField = "username"
        ddreport.DataBind()
        ddreport.Items.Insert(0, New ListItem("", ""))
        ddaccess.Items.Insert(0, New ListItem("", ""))
        ddrelation.Items.Insert(0, New ListItem("", ""))

        ddunit.DataSource = objAcessdata.funcGetSQLDataReader("select UnitID,UnitName from CORIntUnitMaster where active=1 and providerId=" & Session("Provider_Id") & " order by UnitName")
        ddunit.DataValueField = "UnitID"
        ddunit.DataTextField = "UnitName"
        ddunit.DataBind()
        ddunit.Items.Insert(0, New ListItem("", ""))

        objAcessdata.Dispose()

    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        cmd = New SqlCommand("procInsertSysuserMaster", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("@loginid", txtloginid.Text)
        'cmd.Parameters.Add("@userpwd", txtpwd.Text)
        cmd.Parameters.Add("@fname", txtfname.Text)
        cmd.Parameters.Add("@lname", txtlname.Text)
        cmd.Parameters.Add("@mname", txtmidname.Text)
        cmd.Parameters.Add("@homeaddress", txtaddress.Text)
        cmd.Parameters.Add("@phone1", txtph1.Text)
        cmd.Parameters.Add("@phone2 ", txtphone2.Text)
        cmd.Parameters.Add("@emailid ", txtemail.Text)
        cmd.Parameters.Add("@desigid ", ddlDesig.SelectedItem.Value)
        cmd.Parameters.Add("@reportstoid", ddreport.SelectedItem.Value)
        cmd.Parameters.Add("@unitid ", ddunit.SelectedItem.Value)
        cmd.Parameters.Add("@outsideryn ", get_YNvalue(chkoutsider))
        cmd.Parameters.Add("@accesstype", ddaccess.SelectedItem.Value)
        cmd.Parameters.Add("@remarks ", txtarearemarks.Text)
        cmd.Parameters.Add("@active", get_YNvalue(chkActive))
        cmd.Parameters.Add("@createdby ", Session("loggedin_user"))
        cmd.Parameters.Add("@RelationShipType", ddrelation.SelectedItem.Value)
        cmd.Parameters.Add("@ByPass", get_YNvalue(chkbypass))
        cmd.Parameters.Add("@EmpCode ", txtEmpCode.Text)
        cmd.Parameters.Add("@ProviderId", Session("provider_Id"))
        Try
            MyConnection.Open()
            cmd.ExecuteNonQuery()
            MyConnection.Close()
            pnlmainform.Visible = False
            pnlconfirmation.Visible = True

            lblMessage.Text = "You have added the User successfully"
            hyplnkretry.Text = "Add another User"
            hyplnkretry.NavigateUrl = "UserAddForm.aspx"
        Catch
            lblerrmsg.Visible = True
            lblerrmsg.Text = "Userid already exist"
        End Try
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("UserAddForm.aspx")
    End Sub
End Class
