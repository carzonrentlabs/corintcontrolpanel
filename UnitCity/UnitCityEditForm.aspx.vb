Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System


Public Class UnitCityEditForm
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents Validationsummary1 As System.Web.UI.WebControls.ValidationSummary
    Protected WithEvents txtCityName As System.Web.UI.WebControls.TextBox
    Protected WithEvents Requiredfieldvalidator1 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents txtStateName As System.Web.UI.WebControls.TextBox
    Protected WithEvents Requiredfieldvalidator2 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents txtRemarks As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddRegion As System.Web.UI.WebControls.DropDownList
    Protected WithEvents chkactive As System.Web.UI.WebControls.CheckBox
    Protected WithEvents btnsubmit As System.Web.UI.WebControls.Button
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    'Protected WithEvents ddlMainUnitCty As System.Web.UI.WebControls.DropDownList


    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not Page.IsPostBack Then
            'populateddl()

            Dim dtrreader As SqlDataReader
            Dim accessdata As clsutility
            accessdata = New clsutility
            dtrreader = accessdata.funcGetSQLDataReader("select cityname,statename,remarks,active,Region,NewUnitID from  CORIntUnitCityMaster  where unitcityid=" & Request.QueryString("id") & " ")




            dtrreader.Read()
            txtCityName.Text = dtrreader("cityname") & ""
            txtStateName.Text = dtrreader("statename") & ""
            txtRemarks.Text = dtrreader("remarks") & ""
            chkactive.Checked = dtrreader("active")
            ddRegion.Items.FindByValue(dtrreader("Region")).Selected = True
            'ddlMainUnitCty.Items.FindByValue(dtrreader("NewUnitID")).Selected = True

            dtrreader.Close()
        End If
    End Sub

    Private Sub btnsubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        Dim intactive As Int32
        If chkactive.Checked = True Then
            intactive = 1
        Else
            intactive = 0
        End If

        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        Dim intuniqcheck As SqlParameter
        Dim intuniqvalue as int32

        'execueting the stored procedure for checking the user login validity by using the output parameter
        cmd = New SqlCommand("procUpdateUnitcityMaster", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@unitid ", Request.QueryString("id"))
        cmd.Parameters.Add("@cityname", txtCityName.Text.ToString)
        cmd.Parameters.Add("@statename", txtStateName.Text.ToString)
        cmd.Parameters.Add("@remarks", txtRemarks.Text.ToString)
        cmd.Parameters.Add("@active", intactive)
        cmd.Parameters.Add("@updatedby", Session("loggedin_user"))
        cmd.Parameters.Add("@Region", ddRegion.SelectedItem.Value)
        'cmd.Parameters.Add("@NewUnitcityid", ddlMainUnitCty.SelectedItem.Value)
        intuniqcheck = cmd.Parameters.Add("@uniqcheckval", SqlDbType.Int)
        intuniqcheck.Direction = ParameterDirection.Output
        '  Try
        MyConnection.Open()
        cmd.ExecuteNonQuery()
        intuniqvalue=cmd.Parameters("@uniqcheckval").Value
        MyConnection.Close()
         if not intuniqvalue = 0 then
                lblErrorMsg.visible=true
                lblErrorMsg.text="City already exists."
                exit sub
        else
                lblErrorMsg.visible=false
                pnlmainform.Visible = False
                pnlconfirmation.Visible = True

                lblMessage.Text = "You have updated the Unit City successfully"
                hyplnkretry.Text = "Edit another Unit City"
                hyplnkretry.NavigateUrl = "UnitCityEditsearch.aspx"
        end if
        '   Catch
        '   End Try

    End Sub

    'Sub populateddl()
    'Dim dtrreader As SqlDataReader
    'Dim objAcessdata As clsutility
    '   objAcessdata = New clsutility

    'ddlMainUnitCty.DataSource = objAcessdata.funcGetSQLDataReader("select NewUnitID ,Unitname from CorIntMainUnitMaster order by Unitname")
    'ddlMainUnitCty.DataValueField = "NewUnitID"
    'ddlMainUnitCty.DataTextField = "Unitname"
    'ddlMainUnitCty.DataBind()
    'ddlMainUnitCty.Items.Insert(0, New ListItem("", ""))
    '  objAcessdata.Dispose()

    'End Sub

End Class
