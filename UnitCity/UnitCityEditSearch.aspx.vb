Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System


Public Class UnitCityEditSearch
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents ddCityName As System.Web.UI.WebControls.DropDownList

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not Page.IsPostBack Then
            Dim dtrreader As SqlDataReader
            Dim accessdata As clsutility
            accessdata = New clsutility
            dtrreader = accessdata.funcGetSQLDataReader("select unitcityid,cityname from corintunitcitymaster order by cityname")
            ddCityName.DataSource = dtrreader
            ddCityName.DataValueField = "unitcityid"
            ddCityName.DataTextField = "cityname"
            ddCityName.DataBind()
            ddCityName.Items.Insert(0, New ListItem("Any", -1))
            dtrreader.Close()
        End If

    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Response.Redirect("UnitCityEditresult.aspx?id=" & ddCityName.SelectedItem.Value)
    End Sub


End Class
