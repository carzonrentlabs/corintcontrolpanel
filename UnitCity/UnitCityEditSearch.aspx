<%@ Page Language="vb" AutoEventWireup="false" Src="UnitCityEditSearch.aspx.vb" Inherits="UnitCityEditSearch"%>
<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CarzonRent :: Internal software Control panel</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<uc1:Headerctrl id="Headerctrl1" runat="server"></uc1:Headerctrl>
			<br>
			<br>
			<TABLE align="center">
				<TR>
					<TD colspan="2" align="center"><STRONG><U>Edit a Unit City where</U></STRONG></TD>
				</TR>
				<TR>
					<TD>City name</TD>
					<TD>
						<asp:DropDownList id="ddCityName" runat="server" CssClass="input"></asp:DropDownList></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD colspan="2" align="center">
						<asp:Button id="btnSubmit" runat="server" Text="Submit" CssClass="button"></asp:Button>
						<input type="Reset" CssClass="button" name="Reset" value="Reset" /></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
