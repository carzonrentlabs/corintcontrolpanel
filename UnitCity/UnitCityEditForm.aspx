<%@ Page Language="vb" AutoEventWireup="false" Src="UnitCityEditForm.aspx.vb" Inherits="UnitCityEditForm"%>
<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CarzonRent :: Internal software Control Panel</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../utilityfunction.js"></script>
	</HEAD>
	<body onLoad="OnLoadshowLength(document.forms[0].txtRemarks.value,shwMessage)">
		<form id="Form1" method="post" runat="server">
			<uc1:Headerctrl id="Headerctrl1" runat="server"></uc1:Headerctrl>
			<P></P>
			<p><asp:validationsummary id="Validationsummary1" runat="server" ShowSummary="false" HeaderText="Please make sure all the fields marked with * are filled in."
					ShowMessageBox="true"></asp:validationsummary></p>
			<TABLE align="center">
				
					<TBODY>
						<TR>
							<TD align="center" colSpan="2"><STRONG><U>Edit a Unit City</U></STRONG>
							</TD>
						</TR>
						<tr><td colspan="2" align="center"><asp:Label id="lblErrorMsg" cssclass="subRedHead" visible="false" runat="server"></asp:Label></td>

						<asp:Panel ID="pnlmainform" Runat="server">
						<TR>
							<TD>* City Name</TD>
							<TD>
								<asp:textbox id="txtCityName" runat="server" MaxLength="100" CssClass="input"></asp:textbox>
								<asp:requiredfieldvalidator id="Requiredfieldvalidator1" runat="server" Controltovalidate="txtCityName" Display="None"
									errormessage=""></asp:requiredfieldvalidator></TD>
						</TR>
						<TR>
							<TD>* State Name</TD>
							<TD>
								<asp:textbox id="txtStateName" runat="server" MaxLength="100" CssClass="input"></asp:textbox>
								<asp:requiredfieldvalidator id="Requiredfieldvalidator2" runat="server" Controltovalidate="txtStateName" Display="None"
									errormessage=""></asp:requiredfieldvalidator></TD>
						</TR>
						<TR>
							<TD vAlign="top">* Region</TD>
							<TD>
								<asp:dropdownlist id="ddRegion" runat="server" CssClass="input">
									<asp:listitem Text="Central" Value='C' />
									<asp:listitem Text="Eastern" Value='E' />
									<asp:listitem Text="Northern" Value='N' />
									<asp:listitem Text="Southern" Value='S' />
									<asp:listitem Text="Western" Value='W' />
									<asp:listitem Text="Airport" Value='A' />
								</asp:dropdownlist></TD>
						</TR>
						<!--<TR>
							<TD>* Main Unit City</TD>
							<TD>
								<asp:dropdownlist id="ddlMainUnitCty" runat="server"></asp:dropdownlist></TD>
						</TR>-->
						<TR>
							<TD vAlign="top">Remarks(<SPAN id="shwMessage">0</SPAN>/2000 chars)</TD>
							<TD>
								<asp:textbox id="txtRemarks" onkeydown="showLength(this.form.txtRemarks,'document.all.shwMessage',2000)"
									onkeyup="showLength(this.form.txtRemarks,'document.all.shwMessage',2000)" runat="server"
									MaxLength="2000" CssClass="input" TextMode="MultiLine" Columns="50" Rows="3"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>Active</TD>
							<TD>
								<asp:checkbox id="chkactive" runat="server" CssClass="input" Checked="True"></asp:checkbox></TD>
						</TR>
						<TR>
							<TD align="center" colSpan="2">
								<asp:button id="btnsubmit" runat="server" CssClass="button" Text="Submit"></asp:button>&nbsp;&nbsp;
								<input type="Reset" CssClass="button" name="Reset" value="Reset" /></TD>
						</TR>
				</asp:Panel>
				<asp:Panel ID="pnlconfirmation" Visible="False" Runat="server">
					<TR align="center">
						<TD colSpan="2">
						<INPUT type="hidden" name="txtRemarks">
						<SPAN class="shwText" id="shwMessage"></SPAN>
							<asp:Label id="lblMessage" runat="server"></asp:Label></TD>
					</TR>
					<TR align="center">
						<TD colSpan="2">
							<asp:HyperLink id="hyplnkretry" runat="server"></asp:HyperLink></TD>
					</TR>
				</asp:Panel></TBODY>
			</TABLE>
		</form>
	</body>
</HTML>
