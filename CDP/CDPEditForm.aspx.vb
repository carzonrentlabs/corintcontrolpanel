Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Mail
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class CDPEditForm
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    Protected WithEvents lblerr As System.Web.UI.WebControls.Label
    Protected WithEvents txtCDPNo As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddldiscount As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddldiscountdecimal As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtDescription As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkActive As System.Web.UI.WebControls.CheckBox
    Protected WithEvents btnsubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnreset As System.Web.UI.WebControls.Button
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    Protected WithEvents txtFromDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtExpDate As System.Web.UI.WebControls.TextBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btnsubmit.Attributes.Add("onClick", "return validation();")
        If Not Page.IsPostBack Then
            If Convert.ToString(Request.QueryString("id")) = "" Then
                lblErrorMsg.Visible = True
                lblErrorMsg.Text = "CDP No Not exist."
                Response.End()
            End If

            populateddl()

            Dim valDisc As String
            Dim valDiscDec As String
            Dim arrDisc As Array

            Dim dtrreader As SqlDataReader
            Dim accessdata As clsutility
            accessdata = New clsutility
            'dtrreader = accessdata.funcGetSQLDataReader("select CDPNo, CDPDescription, Discount, Active from CORIntCDP where  CDPid=" & Request.QueryString("id") & " ")
            'dtrreader = accessdata.funcGetSQLDataReader("SP_CDPEditDetails " & Request.QueryString("id"))
            dtrreader = accessdata.funcGetSQLDataReader("SP_CDPEditDetails " & Convert.ToString(Request.QueryString("id")))

            If dtrreader.HasRows() Then
                dtrreader.Read()

                txtCDPNo.Text = dtrreader("CDPNo")
                arrDisc = Convert.ToString(dtrreader("Discount")).Split(".")
                If arrDisc.Length = 2 Then
                    valDisc = arrDisc(0)
                    valDiscDec = arrDisc(1)
                    If valDisc = "" Then valDisc = "0"
                    If valDiscDec = "00" Or valDiscDec = "" Then valDiscDec = "0"
                    valDiscDec = Left(valDiscDec, 1)
                Else
                    valDisc = arrDisc(0)
                    valDiscDec = "0"
                End If

                autoselec_ddl(ddldiscount, valDisc)
                autoselec_ddl(ddldiscountdecimal, valDiscDec)

                txtDescription.Text = dtrreader("CDPDescription") & ""
                txtFromDate.Text = dtrreader("StartDate") & ""
                txtExpDate.Text = dtrreader("ExpiredDate") & ""
                chkActive.Checked = dtrreader("Active")
                dtrreader.Close()
            Else
                lblErrorMsg.Visible = True
                lblErrorMsg.Text = "CDP No Not exist."
                Response.End()
            End If
            accessdata.Dispose()

        End If
    End Sub
    Sub populateddl()
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility

        objAcessdata.funcpopulatenumddw(100, 0, ddldiscount)
        objAcessdata.funcpopulatenumddw(9, 0, ddldiscountdecimal)

        objAcessdata.Dispose()

    End Sub
    ' Code Added by Binary Semantics on 24-April-07
    Function IsCDPNoExist(ByVal cdpno As String) As Boolean
        Dim cdpNoCheck As Boolean
        cdpNoCheck = False
        Dim dtrreader As SqlDataReader
        Dim accessdata As clsutility
        accessdata = New clsutility
        dtrreader = accessdata.funcGetSQLDataReader("SELECT Count(CDPNO) as CDPNO FROM CORIntCDP WHERE CDPNo =  '" & cdpno & "' ")
        dtrreader.Read()

        If dtrreader("CDPNO") > 0 Then
            cdpNoCheck = True
        End If

        dtrreader.Close()
        accessdata.Dispose()
        Return cdpNoCheck
    End Function
    Private Sub btnsubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        ' Code added by Binary Semantics on 24-April-07
        ' Call function to check for email exist or not
        'Dim checkUserName As Boolean
        'checkUserName = IsCDPNoExist(txtCDPNo.Text)

        'If Not checkUserName Then
        ' UserName not exist, update the record

        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        Dim cmd2 As SqlCommand
        Dim intuniqcheck As SqlParameter
        Dim intuniqvalue As Int32
        Dim intclientcoidParam As SqlParameter
        Dim intclientcoid As Int32


        Dim disdiscount As String

        disdiscount = ddldiscount.SelectedItem.Value + "." + ddldiscountdecimal.SelectedItem.Value

        cmd = New SqlCommand("procUpdateCDPMaster", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("@cdpno", txtCDPNo.Text)
        cmd.Parameters.Add("@cdpid", Convert.ToString(Request.QueryString("id")))
        cmd.Parameters.Add("@discount", disdiscount)

        cmd.Parameters.Add("@CDPdescription", txtDescription.Text)
        cmd.Parameters.Add("@active", get_YNvalue(chkActive))
        cmd.Parameters.Add("@Modifyby", Convert.ToString(Session("loggedin_user")))
        cmd.Parameters.Add("@ExpDate", Convert.ToString(txtExpDate.Text))

        'intuniqcheck = cmd.Parameters.Add("@uniqcheckval", SqlDbType.Int)

        'intuniqcheck.Direction = ParameterDirection.Output

        'Try
        MyConnection.Open()
        intuniqvalue = cmd.ExecuteNonQuery()
        'intuniqvalue = cmd.Parameters("@uniqcheckval").Value

        MyConnection.Close()

        If intuniqvalue = 0 Then
            lblErrorMsg.Visible = True
            lblErrorMsg.Text = "CDP No already exist."
            Exit Sub
        Else
            lblErrorMsg.Visible = False

            pnlmainform.Visible = False
            pnlconfirmation.Visible = True

            lblMessage.Text = "You have edited the CDP No successfully"
            hyplnkretry.Text = "Edit another CDP No"
            hyplnkretry.NavigateUrl = "CDPEditFormList.aspx"
        End If

        'Else
        '    ' UserName exist, display message
        '    lblErrorMsg.Visible = True
        '    lblErrorMsg.Text = "CDP NO already exists."
        'End If
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function

    Private Sub btnreset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnreset.Click
        Response.Redirect("cdpAddform.aspx")
    End Sub
    Function autoselec_ddl(ByVal ddlname As DropDownList, ByVal selectvalue As Int32)
        If Not IsDBNull(selectvalue) Then
            ' If Not selectvalue = 0 Then
            ddlname.Items.FindByValue(selectvalue).Selected = True
            'End If
        End If
    End Function
End Class

