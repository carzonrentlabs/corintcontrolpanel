<%@ Page Language="vb" AutoEventWireup="false" src="CDPAddForm.aspx.vb" Inherits="CDPAddForm"%>
<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CarzonRent :: Internal software Control panel</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="JavaScript" src="../utilityfunction.js"></script>
		<script language="JavaScript" src="../JScripts/Datefunc.js"></script>
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
		<script language="javascript">
		function validation()
		{
			if(document.forms[0].txtCDPNo.value=="")
				{
					alert("Please fill CDP No");
					return false;
				}
			if ((document.forms[0].ddldiscount.value=="") || ((document.forms[0].ddldiscountdecimal.value=="")))
				{
					alert("Please fill Discount%");
					document.forms[0].ddldiscount.focus();
					return false;
				}				
			if(document.forms[0].txtFromDate.value=="")
				{
					alert("Please fill Start date");
					return false;
				}
			if(document.forms[0].txtExpDate.value=="")
				{
					alert("Please fill Expire date");
					return false;
				}
			
			else
			{
				
				today = document.forms[0].txttodaydates.value;
				//var today =new Date();
				userDate = document.forms[0].txtFromDate.value;
				userexpDate = document.forms[0].txtExpDate.value;
				
				var date1, date2, date3;
				var month1, month2, month3;
				var year1, year2, year3;
				var d = new Date();
				month1 =d.getMonth(); 
				date1 = d.getDate(); 
				year1 = d.getYear();

				month2 = userDate.substring (0, userDate.indexOf ("/"));
				date2 = userDate.substring (userDate.indexOf ("/")+1, userDate.lastIndexOf ("/"));
				year2 = userDate.substring (userDate.lastIndexOf ("/")+1, userDate.length);

				month3 = userexpDate.substring (0, userexpDate.indexOf ("/"));
				date3 = userexpDate.substring (userexpDate.indexOf ("/")+1, userexpDate.lastIndexOf ("/"));
				year3 = userexpDate.substring (userexpDate.lastIndexOf ("/")+1, userexpDate.length);
				
				month1 = parseInt(month1);
				date1 = parseInt(date1);
				year1 = parseInt(year1);
				month2 = parseInt(month2);
				date2 = parseInt(date2);
				year2 = parseInt(year2);
				month3 = parseInt(month3);
				date3 = parseInt(date3);
				year3 = parseInt(year3);
				
				
				//Start date validation
				if (year2 < year1)
				{
					alert('Start date should be equal to or greater then current date.');
					return false;
				}
				else
				{
					if (month2 < month1)
					{
						alert('Start date should be equal to or greater then current date.');
						return false;
					}
					else
					{
						if (date2 < date1) 
						{
							alert('Start date should be equal to or greater then current date.');
							return false;
						}
					}
				}
				
				//Expire date validation
				if (year3 < year1)
				{
					alert('Expire date should be equal or greater then current date.');
					return false;
				}
				else
				{
					if (month3 < month1)
					{
						alert('Expire date should be equal or greater then current date.');
						return false;
					}
					else
					{
						if (date3 < date1) 
						{
							alert('Expire date should be equal or greater then current date.');
							return false;
						}
					}
				}
				//start and exp validation
				if (year3 < year2)
				{
					alert('Expire date should be equal to or greater then start date.');
					return false;
				}
				else
				{
					if (month3 < month2)
					{
						alert('Expire date should be equal to or greater then start date.');
						return false;
					}
					else
					{
						if (date3 < date2) 
						{
							alert('Expire date should be equal to or greater then start date.');
							return false;
						}
					}
				}
			}
   		}	
   		
		function dateReg(obj)
        {
            if(obj.value!="")
            {
                // alert(obj.value);
                var reg = /((0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[0-1])-(201[0-9]))/
                if(reg.test(obj.value))
                {
                    //alert('valid');
                }
                else
                {
                    alert('notvalid');
                    obj.value="";
                }
            }
        }
   			
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<uc1:headerctrl id="Headerctrl2" runat="server"></uc1:headerctrl>
			<TABLE id="Table1" align="center">
				<asp:panel id="pnlmainform" Runat="server">
					<TBODY>
						<TR>
							<TD align="center" colSpan="2"><STRONG><U>Add a CDP</U></STRONG>
							</TD>
						</TR>
						<TR>
							<TD align="center" colSpan="2">
								<asp:Label id="lblErrorMsg" runat="server" visible="false" cssclass="subRedHead"></asp:Label></TD>
						</TR>
						<TR>
							<TD align="center" colSpan="2">
								<asp:Label id="lblerr" runat="server" CssClass="subRedHead" Visible="False"></asp:Label></TD>
						</TR>
						<TR>
							<TD>CDP No<FONT color="red">*</FONT></TD>
							<TD>
								<asp:textbox id="txtCDPNo" runat="server" CssClass="input" MaxLength="100"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>Discount%<FONT color="red">*</FONT></TD>
							<TD>
								<asp:DropDownList id="ddldiscount" runat="server" CssClass="input"></asp:DropDownList>
								<asp:DropDownList id="ddldiscountdecimal" runat="server" CssClass="input"></asp:DropDownList></TD>
						</TR>
						<TR>
							<TD vAlign="top">Description</TD>
							<TD>
								<asp:textbox id="txtDescription" runat="server"	CssClass="input" MaxLength="1000" TextMode="MultiLine" Columns="50" Rows="3"></asp:textbox></TD>
						</TR>
						<TR>
							<TD vAlign="top">Start Date<FONT color="red">*</FONT></TD>
							<TD>
								<asp:textbox id="txtFromDate" runat="server" CssClass="input" MaxLength="12" onblur = "dateReg(this);"
									size="12"></asp:textbox><A onmouseover="window.status='Date Picker';return true;" onmouseout="window.status='';return true;"
									href="javascript:show_calendar('Form1.txtFromDate');"><IMG height="21" src="../images/show-calendar.gif" width="24" border="0"></A></FONT> 
								</SELECT></TD>
						</TR>
						<TR>
							<TD vAlign="top">Expire Date<FONT color="red">*</FONT></TD>
							<TD><INPUT id=txttodaydates type=hidden value="<%=Today()%>">
								<asp:textbox id="txtExpDate" runat="server" CssClass="input" MaxLength="12" onblur = "dateReg(this);" size="12"></asp:textbox><A onmouseover="window.status='Date Picker';return true;" onmouseout="window.status='';return true;"
									href="javascript:show_calendar('Form1.txtExpDate');"><IMG height="21" src="../images/show-calendar.gif" width="24" border="0"></A></FONT> 
								</SELECT></TD>
						</TR>
						<TR>
							<TD>Active</TD>
							<TD>
								<asp:checkbox id="chkActive" runat="server" CssClass="input" Checked="True"></asp:checkbox></TD>
						</TR>
						<TR>
							<TD align="center" colSpan="2">
								<asp:button id="btnsubmit" runat="server" CssClass="button" Text="Submit"></asp:button>&nbsp;&nbsp;
								<asp:button id="btnreset" runat="server" CssClass="button" Text="Reset" CausesValidation="False"></asp:button></TD>
						</TR>
				</asp:panel><asp:panel id="pnlconfirmation" Runat="server" Visible="False">
					<TR align="center">
						<TD colSpan="2">
							<asp:Label id="lblMessage" runat="server"></asp:Label></TD>
					</TR>
					<TR align="center">
						<TD colSpan="2">
							<asp:HyperLink id="hyplnkretry" runat="server"></asp:HyperLink></TD>
					</TR>
				</asp:panel></TBODY></TABLE>
		</form>
	</body>
</HTML>
