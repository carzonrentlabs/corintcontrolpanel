Imports commonutility
Imports System.Data
Imports System.text
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class CDPEditFormList
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents CDPNo As System.Web.UI.WebControls.DropDownList
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnReset As System.Web.UI.WebControls.Button
    Protected WithEvents tblCDP As System.Web.UI.WebControls.Table
    Protected WithEvents MainDiv As System.Web.UI.HtmlControls.HtmlGenericControl

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not Page.IsPostBack Then
            getvalue("CDPNO")
        End If
    End Sub
    Function getuserName(ByVal userid As Integer) As String
        Dim strquery As StringBuilder
        Dim oConnection As SqlConnection
        Dim strConnection As String = System.Configuration.ConfigurationSettings.AppSettings("corConnectString")

        oConnection = New SqlConnection(strConnection)
        strquery = New StringBuilder("select top 1 Fname + ' ' + Mname + ' ' + lname as UName from CORIntSysUsersMaster where sysuserid=" & userid)

        Dim Command = New SqlCommand(strquery.ToString, oConnection)
        oConnection.Open()
        Dim strreturnvalue As String = ""
        strreturnvalue = Convert.ToString(Command.ExecuteScalar())
        If oConnection.State = ConnectionState.Open Then
            oConnection.Close()
        End If
        Return strreturnvalue
    End Function
    Sub getvalue(ByVal strorderby As String)
        Dim strquery As StringBuilder
        Dim str As StringBuilder
        Dim dtrreader As SqlDataReader
        Dim accessdata As clsutility
        Dim createby As String = ""
        Dim Createon As String = ""
        accessdata = New clsutility
        strquery = New StringBuilder("select CDPID, CDPNO, CDPDescription, Discount,")
        strquery.Append(" Active, CreateBy, convert(varchar,CreateDate,101) as CreateDate, ModifyBy, convert(varchar,ModifyDate,101) as ModifyDate, convert(varchar,StartDate,101) as StartDate, convert(varchar,ExpiredDate,101) as ExpiredDate from CORIntCDP order by cdpid")
        dtrreader = accessdata.funcGetSQLDataReader(strquery.ToString)
        If dtrreader.HasRows Then
            str = New StringBuilder("<table ID=""tblCDP1"" HorizontalAlign=""Center"" BorderColor=""#cccc99"" BorderStyle=""Solid"" GridLines=""both"" border=""1"" CellSpacing=""0"" CellPadding=""0"">")
            str.Append("<tr>")
            str.Append("<td Class=""btnstyle"">&nbsp;&nbsp;<b>CDP No</b>&nbsp;&nbsp;</td>")
            str.Append("<td Class=""btnstyle"">&nbsp;&nbsp;<b>CDP Description</b>&nbsp;&nbsp;</td>")
            str.Append("<td Class=""btnstyle"">&nbsp;&nbsp;<b>Discount%</b>&nbsp;&nbsp;</td>")
            str.Append("<td Class=""btnstyle"">&nbsp;&nbsp;<b>Active</b>&nbsp;&nbsp;</td>")
            str.Append("<td Class=""btnstyle"">&nbsp;&nbsp;<b>Create By</b>&nbsp;&nbsp;</td>")
            str.Append("<td Class=""btnstyle"">&nbsp;&nbsp;<b>Modify By</b>&nbsp;&nbsp;</td>")
            str.Append("<td Class=""btnstyle"">&nbsp;&nbsp;<b>Create Date</b>&nbsp;&nbsp;</td>")
            str.Append("<td Class=""btnstyle"">&nbsp;&nbsp;<b>Modify Date</b>&nbsp;&nbsp;</td>")
            str.Append("<td Class=""btnstyle"">&nbsp;&nbsp;<b>Start Date</b>&nbsp;&nbsp;</td>")
            str.Append("<td Class=""btnstyle"">&nbsp;&nbsp;<b>Expire Date</b>&nbsp;&nbsp;</td>")
            str.Append("<td Class=""btnstyle"">&nbsp;&nbsp;<b>Edit</b>&nbsp;&nbsp;</td>")
            str.Append("</tr>")
            While dtrreader.Read

                str.Append("<tr>")
                str.Append("<td>&nbsp;&nbsp;" & dtrreader("CDPNO") & "&nbsp;&nbsp;</td>")
                str.Append("<td>&nbsp;&nbsp;" & dtrreader("CDPDescription") & "&nbsp;&nbsp;</td>")
                str.Append("<td>&nbsp;&nbsp;" & dtrreader("Discount") & "&nbsp;&nbsp;</td>")
                If dtrreader("Active") Then
                    str.Append("<td>&nbsp;&nbsp;Active&nbsp;&nbsp;</td>")
                Else
                    str.Append("<td>&nbsp;&nbsp;Non Active&nbsp;&nbsp;</td>")
                End If

                createby = getuserName(Convert.ToString(dtrreader("CreateBy")))
                str.Append("<td>&nbsp;&nbsp;" & createby & "&nbsp;&nbsp;</td>")

                If Convert.ToString(dtrreader("ModifyBy")) <> "" Then
                    createby = getuserName(Convert.ToString(dtrreader("ModifyBy") & ""))
                End If
                str.Append("<td>&nbsp;&nbsp;" & createby & "&nbsp;&nbsp;</td>")

                Createon = Convert.ToString(dtrreader("CreateDate"))
                str.Append("<td>&nbsp;&nbsp;" & Createon & "&nbsp;&nbsp;</td>")

                If Convert.ToString(dtrreader("ModifyDate")) <> "" Then
                    Createon = Convert.ToString(dtrreader("ModifyDate"))
                End If
                str.Append("<td>&nbsp;&nbsp;" & Createon & "&nbsp;&nbsp;</td>")
                str.Append("<td>&nbsp;&nbsp;" & dtrreader("StartDate") & "&nbsp;&nbsp;</td>")
                str.Append("<td>&nbsp;&nbsp;" & dtrreader("ExpiredDate") & "&nbsp;&nbsp;</td>")
                str.Append("<td>&nbsp;&nbsp;<a href=CDPEditForm.aspx?ID=" & dtrreader("CDPID") & " >Edit</a>&nbsp;&nbsp;</td>")
                str.Append("</tr>")

            End While
            str.Append("</table>")
            MainDiv.innerHTML = str.ToString
        Else
            str = New StringBuilder("<table ID=""tblCDP1"" HorizontalAlign=""Center"" BorderColor=""#cccc99"" BorderStyle=""Solid"" GridLines=""both"" border=""1"" CellSpacing=""0"" CellPadding=""0"">")
            str.Append("<tr>")
            str.Append("<td Class=""btnstyle"">&nbsp;&nbsp;<b>No record exists.</b>&nbsp;&nbsp;</td>")
            str.Append("</tr>")
            str.Append("</table>")
        End If
        dtrreader.Close()
        accessdata.Dispose()
    End Sub
    'Sub SortGird(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Select Case sender.id
    '        Case "Linkbutton1"
    '            getvalue("CDPNO")
    '        Case "Linkbutton2"
    '            getvalue("CDPDescription")
    '        Case "Linkbutton3"
    '            getvalue("Discount")
    '        Case "Linkbutton4"
    '            getvalue("Active")
    '        Case "Linkbutton5"
    '            getvalue("CreateDate")
    '    End Select

    'End Sub
End Class
