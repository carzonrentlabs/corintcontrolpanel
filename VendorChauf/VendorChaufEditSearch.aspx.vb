Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class VendorChaufEditSearch
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ddlvendorname As System.Web.UI.WebControls.DropDownList
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnReset As System.Web.UI.WebControls.Button

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not Page.IsPostBack Then
            Dim dtrreader As SqlDataReader
            Dim accessdata As clsutility
            accessdata = New clsutility


            'Dim objAcessdataNw As clsutility
            'objAcessdataNw = New clsutility
            'Dim strquery As String
            'Dim dtrreaderNew As SqlDataReader
            'Dim strqueryvendor As String

            'Dim objAcessdataMapping As clsutility 'Changes
            'objAcessdataMapping = New clsutility 'Changes
            'Dim strqueryMapping As String 'Changes
            'Dim dtrreaderMapping As SqlDataReader 'Changes
            'Dim UnitID As String 'Changes
            'Dim strquerycity As String

            'strquery = New String("SELECT a.AccessType, a.UnitID, b.UnitCityID, c.Region FROM CORIntSysUsersMaster as a, CORIntUnitMaster as b, CORIntUnitCityMaster as c WHERE a.UnitID = b.UnitID and b.UnitCityID = c.UnitCityID and a.SysUserID =" & Session("loggedin_user"))
            'dtrreaderNew = objAcessdataNw.funcGetSQLDataReader(strquery.ToString)

            'Do While dtrreaderNew.Read

            '    'Changes
            '    If dtrreaderNew("AccessType") = "SU" Or dtrreaderNew("AccessType") = "RN" Then
            '        strqueryMapping = New String("select UM.UnitID, UM.referencedUnitID from corintunitmapping as UM, CORIntUnitMaster as b, CORIntUnitCityMaster as c WHERE UM.referencedUnitID = b.UnitID and b.UnitCityID = c.UnitCityID and UM.UnitID = " & dtrreaderNew("UnitID"))
            '    ElseIf dtrreaderNew("AccessType") = "CT" Then
            '        strqueryMapping = New String("select UM.UnitID, c.UnitCityID as referencedUnitID from corintunitmapping as UM, CORIntUnitMaster as b, CORIntUnitCityMaster as c WHERE UM.referencedUnitID = b.UnitID and b.UnitCityID = c.UnitCityID and UM.UnitID = " & dtrreaderNew("UnitID"))
            '    Else
            '        UnitID = dtrreaderNew("UnitID")
            '    End If

            '    If dtrreaderNew("AccessType") = "SU" Or dtrreaderNew("AccessType") = "CT" Or dtrreaderNew("AccessType") = "RN" Then
            '        dtrreaderMapping = objAcessdataMapping.funcGetSQLDataReader(strqueryMapping.ToString)

            '        Do While dtrreaderMapping.Read
            '            UnitID = dtrreaderMapping("referencedUnitID")
            '        Loop

            '        dtrreaderMapping.Close()

            '    End If


            '    objAcessdataMapping.Dispose()
            '    'Changes

            '    If UnitID = "" Then
            '        UnitID = dtrreaderNew("UnitID")
            '    End If

            '    If dtrreaderNew("AccessType") = "SU" Or dtrreaderNew("AccessType") = "RN" Then
            '        strqueryvendor = New String("select distinct VCM.CarVendorID  ,VCM.CarVendorName  from CORIntCarVendorMaster as VCM, CORIntCityMaster as a, CORIntUnitCityMaster as b, CORIntUnitMaster as c WHERE VCM.CarVendorCityID = a.CityID and a.nearestUnitCityId=b.UnitCityId and a.Active = 1 and a.CityName <> '' and c.UnitCityID = b.UnitCityID and isnull(ApproveYN,0) = 1 and c.UnitID = '" & UnitID & "' order by CarVendorName")
            '    Else
            '        strqueryvendor = New String("select distinct VCM.CarVendorID  ,VCM.CarVendorName  from CORIntCarVendorMaster as VCM, CORIntCityMaster as a, CORIntUnitCityMaster as b, CORIntUnitMaster as c WHERE VCM.CarVendorCityID = a.CityID and a.nearestUnitCityId=b.UnitCityId and a.Active = 1 and a.CityName <> '' and c.UnitCityID = b.UnitCityID and isnull(ApproveYN,0) = 1 and c.UnitCityID = '" & UnitID & "' order by CarVendorName")
            '    End If
            'Loop

            'dtrreaderNew.Close()
            'objAcessdataNw.Dispose()
            'dtrreader = accessdata.funcGetSQLDataReader(strqueryvendor)

            dtrreader = accessdata.funcGetSQLDataReader("select CarVendorID  ,CarVendorName  from CORIntCarVendorMaster    order by CarVendorName")
            ddlvendorname.DataSource = dtrreader
            ddlvendorname.DataValueField = "CarVendorID"
            ddlvendorname.DataTextField = "CarVendorName"
            ddlvendorname.DataBind()
            ddlvendorname.Items.Insert(0, New ListItem("Any", -1))
            dtrreader.Close()
            accessdata.Dispose()

        End If
    End Sub
    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Response.Redirect("VendorChaufEditResult.aspx?id=" & ddlvendorname.SelectedItem.Value)
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("VendorChaufEditSearch.aspx")
    End Sub


End Class
