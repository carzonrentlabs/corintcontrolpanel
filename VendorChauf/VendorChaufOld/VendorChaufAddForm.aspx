<%@ Page Language="vb" AutoEventWireup="false" Src="VendorChaufAddForm.aspx.vb" Inherits="VendorChaufAddForm"%>
<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CarzonRent :: Internal software control panel</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="JavaScript" src="../../utilityfunction.js"></script>
		<script language="JavaScript" src="../../JScripts/Datefunc.js"></script>
		<LINK href="../../HertzInt.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<uc1:Headerctrl id="Headerctrl1" runat="server"></uc1:Headerctrl>
			<p><asp:validationsummary id="Validationsummary1" runat="server" ShowSummary="false" HeaderText="Please make sure all the fields marked with * are filled in."
					ShowMessageBox="true"></asp:validationsummary></p>
			<TABLE id="Table1" align="center">
				<asp:panel id="pnlmainform" Runat="server">
					<TBODY>
						<TR>
							<TD align="center" colSpan="2"><STRONG><U>Add a Vendor Chauffeurr</U></STRONG>
							</TD>
						</TR>
						<TR>
							<TD align="center" colSpan="2">&nbsp;&nbsp;</TD>
						</TR>
						<TR>
							<TD>* Vendor Name
							</TD>
							<TD>
								<asp:DropDownList id="ddlvendorname" runat="server" CssClass="input"></asp:DropDownList>
								<asp:requiredfieldvalidator id="Requiredfieldvalidator1" runat="server" errormessage="" Display="None" Controltovalidate="ddlvendorname"></asp:requiredfieldvalidator></TD>
						</TR>
						<TR>
							<TD>* First Name</TD>
						  <TD>
						  <asp:TextBox ID="txtfname" runat="server" CssClass="input" MaxLength="50"></asp:TextBox>
						  <asp:requiredfieldvalidator id="Requiredfieldvalidator2" runat="server" errormessage="" Display="None" Controltovalidate="txtfname"></asp:requiredfieldvalidator></TD>
						</TR>
						<TR>
							<TD>Middle Name
							</TD>
							<TD>
								<asp:textbox id="txtmname" runat="server" CssClass="input" MaxLength="50"></asp:textbox>
						  </TD>
						</TR>
						<TR>
							<TD>* Last Name</TD>
							<TD>
								<asp:textbox id="txtlname" runat="server" CssClass="input" MaxLength="50"></asp:textbox>
								<asp:requiredfieldvalidator id="Requiredfieldvalidator7" runat="server" errormessage="" Display="None" Controltovalidate="txtlname"></asp:requiredfieldvalidator></TD>
						</TR>
						<TR>
							<TD>* Address</TD>
							<TD>
								<asp:textbox id="txtaddress" runat="server" CssClass="input" MaxLength="250"></asp:textbox>
								<asp:requiredfieldvalidator id="Requiredfieldvalidator8" runat="server" errormessage="" Display="None" Controltovalidate="txtaddress"></asp:requiredfieldvalidator></TD>
						</TR>
						<TR>
							<TD>* Phone</TD>
							<TD>
								<asp:textbox id="txtphone" runat="server" CssClass="input" MaxLength="20"></asp:textbox>
								<asp:requiredfieldvalidator id="Requiredfieldvalidator9" runat="server" errormessage="" Display="None" Controltovalidate="txtphone"></asp:requiredfieldvalidator></TD>
						</TR>
						<TR>
							<TD>Mobile</TD>
							<TD>
								<asp:textbox id="txtmobile" runat="server" CssClass="input" MaxLength="20"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>Blood Group
							</TD>
							<TD>
								<asp:DropDownList id="ddlblood" runat="server" CssClass="input">
									<asp:listitem Text="" Selected="True" Value="" />
									<asp:listitem Text="A+" Value="A+" />
									<asp:listitem Text="A-" Value="A-" />
									<asp:listitem Text="B+" Value="b+" />
									<asp:listitem Text="B-" Value="B-" />
									<asp:listitem Text="AB+" Value="AB+" />
									<asp:listitem Text="AB-" Value="AB-" />
									<asp:listitem Text="O+" Value="O+" />
									<asp:listitem Text="O-" Value="O-" />
								</asp:DropDownList></TD>
						</TR>
						<TR>
							<TD> Date of Birth
							</TD>
							<TD>
								<asp:textbox id="txtdob" runat="server" CssClass="input" ReadOnly=True size=12 MaxLength="12"></asp:textbox>
								<A onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;"
								href="javascript:show_calendar('Form1.txtdob');"><IMG height="21" src="../../images/show-calendar.gif" width="24" border="0"></A></FONT> 
							</SELECT>
						  </TD>
						</TR>
						<TR>
							<TD vAlign="top">Remarks(<SPAN class="shwText" id="shwMessage">0</SPAN>/2000 chars)</TD>
							<TD>
								<asp:textbox id="txtRemarks" onkeydown="showLength(this.form.txtRemarks,'document.all.shwMessage',2000)"
									onkeyup="showLength(this.form.txtRemarks,'document.all.shwMessage',2000)" runat="server"
									CssClass="input" MaxLength="2000" TextMode="MultiLine" Columns="50" Rows="3"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>Active</TD>
							<TD>
								<asp:checkbox id="chkactive" runat="server" CssClass="input" Checked="True"></asp:checkbox></TD>
						</TR>
						<TR>
							<TD align="center" colSpan="2">
								<asp:button id="btnsubmit" runat="server" CssClass="button" Text="Submit"></asp:button>&nbsp;&nbsp;
								<asp:button id="btnreset" runat="server" CssClass="button" Text="Reset" CausesValidation="False"></asp:button></TD>
						</TR>
				</asp:panel><asp:panel id="pnlconfirmation" Runat="server" Visible="False">
					<TR align="center">
						<TD colSpan="2">
							<asp:Label id="lblMessage" runat="server"></asp:Label></TD>
					</TR>
					<TR align="center">
						<TD colSpan="2">
							<asp:HyperLink id="hyplnkretry" runat="server"></asp:HyperLink></TD>
					</TR>
				</asp:panel></TBODY></TABLE>
		    
		</form>
	</body>
</HTML>
