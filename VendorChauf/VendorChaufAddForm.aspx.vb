Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class VendorChaufAddForm
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents Validationsummary1 As System.Web.UI.WebControls.ValidationSummary
    Protected WithEvents ddlvendorname As System.Web.UI.WebControls.DropDownList
    Protected WithEvents Requiredfieldvalidator1 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents txtfname As System.Web.UI.WebControls.TextBox
    Protected WithEvents Requiredfieldvalidator2 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents txtmname As System.Web.UI.WebControls.TextBox
    Protected WithEvents Requiredfieldvalidator3 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents txtlname As System.Web.UI.WebControls.TextBox
    Protected WithEvents Requiredfieldvalidator7 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents txtaddress As System.Web.UI.WebControls.TextBox
    Protected WithEvents Requiredfieldvalidator8 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents txtphone As System.Web.UI.WebControls.TextBox
    Protected WithEvents Requiredfieldvalidator9 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents txtmobile As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtdob As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtRemarks As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkactive As System.Web.UI.WebControls.CheckBox
    Protected WithEvents btnsubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnreset As System.Web.UI.WebControls.Button
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    Protected WithEvents ddlblood As System.Web.UI.WebControls.DropDownList
    Protected WithEvents Requiredfieldvalidator4 As System.Web.UI.WebControls.RequiredFieldValidator

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not Page.IsPostBack Then
            populateddl()
        End If
    End Sub
    Sub populateddl()
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility

        'Dim objAcessdataNw As clsutility
        'objAcessdataNw = New clsutility
        'Dim strquery As String
        'Dim dtrreaderNew As SqlDataReader
        'Dim strqueryvendor As String

        'Dim dtrreader As SqlDataReader
        'Dim objAcessdataMapping As clsutility 'Changes
        'objAcessdataMapping = New clsutility 'Changes
        'Dim strqueryMapping As String 'Changes
        'Dim dtrreaderMapping As SqlDataReader 'Changes
        'Dim UnitID As String 'Changes

        'strquery = New String("SELECT a.AccessType, a.UnitID, b.UnitCityID, c.Region FROM CORIntSysUsersMaster as a, CORIntUnitMaster as b, CORIntUnitCityMaster as c WHERE a.UnitID = b.UnitID and b.UnitCityID = c.UnitCityID and a.SysUserID =" & Session("loggedin_user"))
        'dtrreaderNew = objAcessdataNw.funcGetSQLDataReader(strquery.ToString)

        'Do While dtrreaderNew.Read

        '    'Changes
        '    If dtrreaderNew("AccessType") = "SU" Or dtrreaderNew("AccessType") = "RN" Then
        '        strqueryMapping = New String("select UM.UnitID, UM.referencedUnitID from corintunitmapping as UM, CORIntUnitMaster as b, CORIntUnitCityMaster as c WHERE UM.referencedUnitID = b.UnitID and b.UnitCityID = c.UnitCityID and UM.UnitID = " & dtrreaderNew("UnitID"))
        '    ElseIf dtrreaderNew("AccessType") = "CT" Then
        '        strqueryMapping = New String("select UM.UnitID, c.UnitCityID as referencedUnitID from corintunitmapping as UM, CORIntUnitMaster as b, CORIntUnitCityMaster as c WHERE UM.referencedUnitID = b.UnitID and b.UnitCityID = c.UnitCityID and UM.UnitID = " & dtrreaderNew("UnitID"))
        '    Else
        '        UnitID = dtrreaderNew("UnitID")
        '    End If

        '    If dtrreaderNew("AccessType") = "SU" Or dtrreaderNew("AccessType") = "CT" Or dtrreaderNew("AccessType") = "RN" Then
        '        dtrreaderMapping = objAcessdataMapping.funcGetSQLDataReader(strqueryMapping.ToString)

        '        Do While dtrreaderMapping.Read
        '            UnitID = dtrreaderMapping("referencedUnitID")
        '        Loop

        '        dtrreaderMapping.Close()

        '    End If


        '    objAcessdataMapping.Dispose()
        '    'Changes

        '    If UnitID = "" Then
        '        UnitID = dtrreaderNew("UnitID")
        '    End If

        '    If dtrreaderNew("AccessType") = "SU" Or dtrreaderNew("AccessType") = "RN" Then
        '        strqueryvendor = New String("select distinct VCM.CarVendorID  ,VCM.CarVendorName  from CORIntCarVendorMaster as VCM, CORIntCityMaster as a, CORIntUnitCityMaster as b, CORIntUnitMaster as c WHERE VCM.CarVendorCityID = a.CityID and a.nearestUnitCityId=b.UnitCityId and a.Active = 1 and a.CityName <> '' and c.UnitCityID = b.UnitCityID and isnull(ApproveYN,0) = 1 and c.UnitID = '" & UnitID & "' order by CarVendorName")
        '    Else
        '        strqueryvendor = New String("select distinct VCM.CarVendorID  ,VCM.CarVendorName  from CORIntCarVendorMaster as VCM, CORIntCityMaster as a, CORIntUnitCityMaster as b, CORIntUnitMaster as c WHERE VCM.CarVendorCityID = a.CityID and a.nearestUnitCityId=b.UnitCityId and a.Active = 1 and a.CityName <> '' and c.UnitCityID = b.UnitCityID and isnull(ApproveYN,0) = 1 and c.UnitCityID = '" & UnitID & "' order by CarVendorName")
        '    End If
        'Loop

        'dtrreaderNew.Close()
        'objAcessdataNw.Dispose()
        'ddlvendorname.DataSource = objAcessdata.funcGetSQLDataReader(strqueryvendor)


        ddlvendorname.DataSource = objAcessdata.funcGetSQLDataReader("select CarVendorName ,carvendorid  from CORIntCarVendorMaster   where active=1 order by CarVendorName")
        ddlvendorname.DataValueField = "carvendorid"
        ddlvendorname.DataTextField = "CarVendorName"
        ddlvendorname.DataBind()
        ddlvendorname.Items.Insert(0, New ListItem("", ""))

        objAcessdata.Dispose()

    End Sub

    Private Sub btnsubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsubmit.Click

        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        cmd = New SqlCommand("procAddVendorChaufMaster", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("@carvendorid", ddlvendorname.SelectedItem.Value)

        cmd.Parameters.Add("@fname", txtfname.Text)
        cmd.Parameters.Add("@mname", txtmname.Text)
        cmd.Parameters.Add("@lname", txtlname.Text)
        cmd.Parameters.Add("@address", txtaddress.Text)
        cmd.Parameters.Add("@phone", txtphone.Text)
        cmd.Parameters.Add("@mobile", txtmobile.Text)
        cmd.Parameters.Add("@bloodgroup", ddlblood.SelectedItem.Value)
        cmd.Parameters.Add("@dob", txtdob.Text)
        cmd.Parameters.Add("@remarks", txtRemarks.Text)
        cmd.Parameters.Add("@active", get_YNvalue(chkactive))
        cmd.Parameters.Add("@createdby", Session("loggedin_user"))


        '  Try
        MyConnection.Open()
        cmd.ExecuteNonQuery()
        MyConnection.Close()
        pnlmainform.Visible = False

        pnlconfirmation.Visible = True

        lblMessage.Text = "You have added the Vendor Chauffeur successfully"
        hyplnkretry.Text = "Add another Vendor Chauffeur"
        hyplnkretry.NavigateUrl = "VendorChaufAddForm.aspx"
        '   Catch
        '   End Try
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function

End Class
