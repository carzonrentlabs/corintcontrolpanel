<%@ Page Language="vb" AutoEventWireup="false" Src="CarAddForm.aspx.vb" Inherits="CarAddForm" %>

<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>CarzonRent :: Internal software</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">

    <script language="JavaScript" src="../utilityfunction.js"></script>

    <script language="JavaScript" src="../JScripts/Datefunc.js"></script>

    <link href="../HertzInt.css" type="text/css" rel="stylesheet">

    <script language="JavaScript">
		

		
			function checkbeforenext()
			{
				if (isNaN(document.forms[0].txtlastkm.value))
				{
					alert("Last KM Reading should be numeric only")
					return false
				}
			
	            if (isNaN(document.forms[0].txtIMEINumber.value)) {
	                alert("IMEI should be numeric only")
	                return false
	            }

	            if (isNaN(document.forms[0].txtSimMobileNumber.value)) {
	                alert("Sim Mobile Number should be numeric only")
	                return false
	            }

	            if ((document.forms[0].txtSimMobileNumber.value != "") && (document.forms[0].txtSimMobileNumber.value.length < 10 || document.forms[0].txtSimMobileNumber.value.length > 10))
                {
                    alert("Sim Mobile Number should be 10 digit")
                    return false
                }

				var strvalues
				strvalues=('ddlunit,ddlmodel,txtregsitration,txtengineno,txtchasis,txtregistrationdate,ddlregiplace,ddlyear,txtpurdate,ddlfuel,txtlastkm')
				return checkmandatory(strvalues);				
			}


			function checkvalue()
			{
	
	
	
				if (isNaN(document.forms[0].txttaxes.value))
				{
					alert("Total Taxes and Permit Cost should be numeric and upto two decimal digits only")
					return false;
				}
				else
				{

					if (parseFloat(document.forms[0].txttaxes.value.indexOf("."))>0)
					{
						var FlotVal
						FlotVal=document.forms[0].txttaxes.value.substr(parseFloat(document.forms[0].txttaxes.value.indexOf(".")),document.forms[0].txttaxes.value.length)
						if(parseFloat((FlotVal.length)-1)>2)
							{
								alert("Total Taxes and Permit Cost should be numeric and upto two decimal digits only")
								return false;
							}

					}
				}

				if (isNaN(document.forms[0].txtinsuamt.value))
				{
					alert("Insurance Amount should be numeric and upto two decimal digits only")
					return false;
				}
				else
				{

					if (parseFloat(document.forms[0].txtinsuamt.value.indexOf("."))>0)
					{
						var FlotVal
						FlotVal=document.forms[0].txtinsuamt.value.substr(parseFloat(document.forms[0].txtinsuamt.value.indexOf(".")),document.forms[0].txtinsuamt.value.length)
						if(parseFloat((FlotVal.length)-1)>2)
							{
								alert("Insurance Amount should be numeric and upto two decimal digits only")
								return false;
							}

					}
				}		
				
				if (isNaN(document.forms[0].txtlastknservice.value))
				{
					alert("Last Serviced KM should be numeric and upto two decimal digits only")
					return false;
				}	
				
				if (isNaN(document.forms[0].txtnextservice.value))
				{
					alert("Next Service KM should be numeric and upto two decimal digits only")
					return false;
				}		
				
				if (isNaN(document.forms[0].txtlastemiamt.value))
				{
					alert("EMI Amount should be numeric and upto two decimal digits only")
					return false;
				}
				else
				{

					if (parseFloat(document.forms[0].txtlastemiamt.value.indexOf("."))>0)
					{
						var FlotVal
						FlotVal=document.forms[0].txtlastemiamt.value.substr(parseFloat(document.forms[0].txtlastemiamt.value.indexOf(".")),document.forms[0].txtlastemiamt.value.length)
						if(parseFloat((FlotVal.length)-1)>2)
							{
								alert("EMI Amount should be numeric and upto two decimal digits only")
								return false;
							}

					}
				}
				
				if (isNaN(document.forms[0].txtrevenuesharing.value))
				{
					alert("Corporate duty (DCO) revenue sharing % should be numeric and upto two decimal digits only")
					return false;
				}
				else
				{

					if (parseFloat(document.forms[0].txtrevenuesharing.value.indexOf("."))>0)
					{
						var FlotVal
						FlotVal=document.forms[0].txtrevenuesharing.value.substr(parseFloat(document.forms[0].txtrevenuesharing.value.indexOf(".")),document.forms[0].txtrevenuesharing.value.length)
						if(parseFloat((FlotVal.length)-1)>2)
							{
								alert("Corporate duty (DCO) revenue sharing % should be numeric and upto two decimal digits only")
								return false;
							}

					}
				}



				if (isNaN(document.forms[0].txtselfdriverev.value))
				{
					alert("Self Drive (DCO) revenue sharing % should be numeric and upto two decimal digits only")
					return false;
				}
				else
				{

					if (parseFloat(document.forms[0].txtselfdriverev.value.indexOf("."))>0)
					{
						var FlotVal
						FlotVal=document.forms[0].txtselfdriverev.value.substr(parseFloat(document.forms[0].txtselfdriverev.value.indexOf(".")),document.forms[0].txtselfdriverev.value.length)
						if(parseFloat((FlotVal.length)-1)>2)
							{
								alert("Self Drive (DCO) revenue sharing % should be numeric and upto two decimal digits only")
								return false;
							}

					}
				}
				
				if (isNaN(document.forms[0].txthotel.value))
				{
					alert("Self Drive (DCO) revenue sharing % should be numeric and upto two decimal digits only")
					return false;
				}
				else
				{

					if (parseFloat(document.forms[0].txthotel.value.indexOf("."))>0)
					{
						var FlotVal
						FlotVal=document.forms[0].txthotel.value.substr(parseFloat(document.forms[0].txthotel.value.indexOf(".")),document.forms[0].txthotel.value.length)
						if(parseFloat((FlotVal.length)-1)>2)
							{
								alert("Hotel duty (DCO) revenue sharing % should be numeric and upto two decimal digits only")
								return false;
							}

					}
				}


				if (isNaN(document.forms[0].txtdirpaysharing.value))
				{
					alert("Direct Payment (Credit Card) (DCO) revenue sharing % should be numeric and upto two decimal digits only")
					return false;
				}
				else
				{

					if (parseFloat(document.forms[0].txtdirpaysharing.value.indexOf("."))>0)
					{
						var FlotVal
						FlotVal=document.forms[0].txtdirpaysharing.value.substr(parseFloat(document.forms[0].txtdirpaysharing.value.indexOf(".")),document.forms[0].txtdirpaysharing.value.length)
						if(parseFloat((FlotVal.length)-1)>2)
							{
								alert("Direct Payment (Credit Card) (DCO) revenue sharing % should be numeric and upto two decimal digits only")
								return false;
							}

					}
				}				
				
		if(document.forms[0].hdndcoval.value=="1")
			{
				if(document.forms[0].ddldcochaufer.value=="")
				{
				alert("Please select DCO Chauffeur")
				return false;
				}
			
			}
						
			var strvalues
			strvalues=('txttaxes,txtinsuamt,txtlastknservice,txtnextservice,txtlastemiamt')
			return checkmandatory(strvalues);
			}

			function check_dco()
				{
					if(document.forms[0].hdndcoval.value==0)
					{
						document.forms[0].ddldcochaufer.disabled=true
						document.forms[0].txtrevenuesharing.disabled=true
						document.forms[0].txtselfdriverev.disabled=true
						document.forms[0].txthotel.disabled=true
						document.forms[0].txtdirpaysharing.disabled=true
						
					}
				}

			function func_dco()
			{
				if(document.forms[0].chkdco.checked==true)
				{
					document.forms[0].hdndcoval.value=1
				}
				else
				{
				document.forms[0].hdndcoval.value=0
				}
			}
		
		    function dateReg(obj)
            {
                if(obj.value!="")
                {
                    // alert(obj.value);
                    var reg = /((0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[0-1])-(201[0-9]))/
                    if(reg.test(obj.value))
                    {
                        //alert('valid');
                    }
                    else
                    {
                        alert('notvalid');
                        obj.value="";
                    }
                }
            }
    </script>

</head>
<body onload="check_dco()" ms_positioning="GridLayout">
    <form id="Form1" method="post" runat="server">
        <uc1:Headerctrl ID="Headerctrl1" runat="server"></uc1:Headerctrl>
        <table align="center">
            <input id="hdndcoval" type="hidden" name="hdndcoval" value="1" runat="server">
            <asp:Panel ID="pnlmainform" runat="server">
                <tbody>
                    <tr>
                        <td align="center" colspan="2">
                            <b><u>Add a Car � Page 1</u></b>
                            <br>
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <asp:Label ID="lblErrorMsg" CssClass="subRedHead" Visible="false" runat="server"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                            *Unit</td>
                        <td>
                            <asp:DropDownList ID="ddlunit" runat="server" CssClass="input">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td>
                            *Model</td>
                        <td>
                            <asp:DropDownList ID="ddlmodel" runat="server" CssClass="input">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td>
                            * Registration #</td>
                        <td>
                            <asp:TextBox ID="txtregsitration" runat="server" MaxLength="250" CssClass="input"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>
                            AC?</td>
                        <td>
                            <asp:CheckBox ID="chkac" runat="server" CssClass="input" Checked="True"></asp:CheckBox></td>
                    </tr>
                    <tr>
                        <td>
                            * Engine #</td>
                        <td>
                            <asp:TextBox ID="txtengineno" runat="server" MaxLength="20" CssClass="input"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>
                            * Chassis #</td>
                        <td>
                            <asp:TextBox ID="txtchasis" runat="server" MaxLength="20" CssClass="input"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>
                            * Registration Date</td>
                        <td>
                            <asp:TextBox ID="txtregistrationdate" runat="server" MaxLength="12" CssClass="input"
                                onblur="dateReg(this);" size="12"></asp:TextBox><a onmouseover="window.status='Date Picker';return true;"
                                    onmouseout="window.status='';return true;" href="javascript:show_calendar('Form1.txtregistrationdate');"><img
                                        height="21" src="../images/show-calendar.gif" width="24" border="0"></a></FONT>
                            </SELECT></td>
                    </tr>
                    <tr>
                        <td>
                            * Place of Registration</td>
                        <td>
                            <asp:DropDownList ID="ddlregiplace" runat="server" CssClass="input">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td>
                            * Year of Manufacture</td>
                        <td>
                            <asp:DropDownList ID="ddlyear" runat="server" CssClass="input">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td>
                            * Purchase Date</td>
                        <td>
                            <asp:TextBox ID="txtpurdate" runat="server" MaxLength="12" CssClass="input" onblur="dateReg(this);"
                                size="12"></asp:TextBox><a onmouseover="window.status='Date Picker';return true;"
                                    onmouseout="window.status='';return true;" href="javascript:show_calendar('Form1.txtpurdate');"><img
                                        height="21" src="../images/show-calendar.gif" width="24" border="0"></a></FONT>
                            </SELECT></td>
                    </tr>
                    <tr>
                        <td>
                            * Fuel Type</td>
                        <td>
                            <asp:DropDownList ID="ddlfuel" runat="server" CssClass="input">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td>
                            * Last KM Reading</td>
                        <td>
                            <asp:TextBox ID="txtlastkm" runat="server" MaxLength="10" CssClass="input"></asp:TextBox></td>
                    </tr>
                    <%--<tr>
                        <td>
                            Available for Self Drive</td>
                        <td>
                            <asp:CheckBox ID="SelfDriveAvailableYN" runat="server" CssClass="input"></asp:CheckBox></td>
                    </tr>--%>
                    <tr>
                        <td>
                            Under DCO Scheme</td>
                        <td>
                            <asp:CheckBox ID="chkdco" onclick="func_dco()" runat="server" CssClass="input" Checked="True">
                            </asp:CheckBox></td>
                    </tr>
                    <tr>
                        <td valign="top">
                            Remarks(<span class="shwText" id="shwMessage">0</span>/2000 chars)</td>
                        <td>
                            <asp:TextBox ID="txtarearemarks" onkeydown="showLength(this.form.txtarearemarks,'document.all.shwMessage',2000)"
                                onkeyup="showLength(this.form.txtarearemarks,'document.all.shwMessage',2000)"
                                runat="server" MaxLength="2000" CssClass="input" Rows="3" Columns="50" TextMode="MultiLine"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>
                            Active</td>
                        <td>
                            <asp:CheckBox ID="chkActive" runat="server" CssClass="input" Checked="True"></asp:CheckBox></td>
                    </tr>
                    <tr>
                        <td>
                            Special Type </td>
                        <td>
                            <asp:CheckBox ID="chkSpcialType" runat="server" CssClass="input" /></td>
                    </tr>
                    <%--<tr>
                        <td>
                            Sub Location </td>
                        <td>
                            <asp:DropDownList ID="ddlSubLocation" runat="server" CssClass="input">
                            </asp:DropDownList> </td>
                    </tr>--%>
                    <tr>
                        <td>
                            IMEI Number </td>
                        <td>
                            <asp:TextBox ID="txtIMEINumber" runat ="server"  MaxLength ="16" ></asp:TextBox> </td>
                    </tr>
                     <tr>
                        <td>
                            Sim Mobile Number </td>
                        <td>
                            <asp:TextBox ID="txtSimMobileNumber" runat ="server" MaxLength ="12" ></asp:TextBox> </td>
                    </tr>
                    
                    <tr>
                        <td align="center" colspan="2">
                            <asp:Button ID="btnproceed" runat="server" CssClass="input" Text="Next > >"></asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
            </asp:Panel>
            <asp:Panel ID="pnl2ndform" runat="server" Visible="False">
                <tr>
                    <td align="center" colspan="2">
                        <b><u>Add a Car � Page 2</u></b>
                        <br>
                        <br>
                    </td>
                </tr>
                <tr>
                    <td>
                        Permit #</td>
                    <td>
                        <asp:TextBox ID="txtpermit" runat="server" MaxLength="20" CssClass="input"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        Permit Valid From</td>
                    <td>
                        <asp:TextBox ID="txtpermitfrom" runat="server" MaxLength="12" CssClass="input" onblur="dateReg(this);"
                            size="12"></asp:TextBox><a onmouseover="window.status='Date Picker';return true;"
                                onmouseout="window.status='';return true;" href="javascript:show_calendar('Form1.txtpermitfrom');"><img
                                    height="21" src="../images/show-calendar.gif" width="24" border="0"></a></FONT>
                        </SELECT></td>
                </tr>
                <tr>
                    <td>
                        Permit Valid To</td>
                    <td>
                        <asp:TextBox ID="txtpermitto" runat="server" MaxLength="12" CssClass="input" onblur="dateReg(this);"
                            size="12"></asp:TextBox><a onmouseover="window.status='Date Picker';return true;"
                                onmouseout="window.status='';return true;" href="javascript:show_calendar('Form1.txtpermitto');"><img
                                    height="21" src="../images/show-calendar.gif" width="24" border="0"></a></FONT>
                        </SELECT></td>
                </tr>
                <tr>
                    <td>
                        Permit Issued From</td>
                    <td>
                        <asp:DropDownList ID="ddlpermitissue" runat="server" CssClass="input">
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td>
                        * Total Taxes and Permit Cost</td>
                    <td>
                        <asp:TextBox ID="txttaxes" runat="server" MaxLength="10" CssClass="input"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        Authorisation #</td>
                    <td>
                        <asp:TextBox ID="txtauthorization" runat="server" MaxLength="20" CssClass="input"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        Authorisation Valid From</td>
                    <td>
                        <asp:TextBox ID="txtauthfrom" runat="server" MaxLength="12" CssClass="input" onblur="dateReg(this);"
                            size="12"></asp:TextBox><a onmouseover="window.status='Date Picker';return true;"
                                onmouseout="window.status='';return true;" href="javascript:show_calendar('Form1.txtauthfrom');"><img
                                    height="21" src="../images/show-calendar.gif" width="24" border="0"></a></FONT>
                        </SELECT></td>
                </tr>
                <tr>
                    <td>
                        Authorisation Valid To</td>
                    <td>
                        <asp:TextBox ID="txtauthvalidto" runat="server" MaxLength="12" CssClass="input" onblur="dateReg(this);"
                            size="12"></asp:TextBox><a onmouseover="window.status='Date Picker';return true;"
                                onmouseout="window.status='';return true;" href="javascript:show_calendar('Form1.txtauthvalidto');"><img
                                    height="21" src="../images/show-calendar.gif" width="24" border="0"></a></FONT>
                        </SELECT></td>
                </tr>
                <tr>
                    <td>
                        Authorisation Issued From</td>
                    <td>
                        <asp:DropDownList ID="ddlauthissuefrom" runat="server" CssClass="input">
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td>
                        Insurance #</td>
                    <td>
                        <asp:TextBox ID="txtinsu" runat="server" MaxLength="20" CssClass="input"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        Insurance Valid To</td>
                    <td>
                        <asp:TextBox ID="txtinsuto" runat="server" MaxLength="12" CssClass="input" onblur="dateReg(this);"
                            size="12"></asp:TextBox><a onmouseover="window.status='Date Picker';return true;"
                                onmouseout="window.status='';return true;" href="javascript:show_calendar('Form1.txtinsuto');"><img
                                    height="21" src="../images/show-calendar.gif" width="24" border="0"></a></FONT>
                        </SELECT></td>
                </tr>
                <tr>
                    <td>
                        Insurance Valid From</td>
                    <td>
                        <asp:TextBox ID="txtinsufrom" runat="server" MaxLength="12" CssClass="input" onblur="dateReg(this);"
                            size="12"></asp:TextBox><a onmouseover="window.status='Date Picker';return true;"
                                onmouseout="window.status='';return true;" href="javascript:show_calendar('Form1.txtinsufrom');"><img
                                    height="21" src="../images/show-calendar.gif" width="24" border="0"></a></FONT>
                        </SELECT></td>
                </tr>
                <tr>
                    <td>
                        Insurance Issued From</td>
                    <td>
                        <asp:DropDownList ID="ddlinsissued" runat="server" CssClass="input">
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td>
                        * Insurance Amount</td>
                    <td>
                        <asp:TextBox ID="txtinsuamt" runat="server" MaxLength="10" CssClass="input"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        Insurance Company</td>
                    <td>
                        <asp:TextBox ID="txtinscomp" runat="server" MaxLength="100" CssClass="input"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        Sales Tax Valid From</td>
                    <td>
                        <asp:TextBox ID="txtsalesfrom" runat="server" MaxLength="12" CssClass="input" onblur="dateReg(this);"
                            size="12"></asp:TextBox><a onmouseover="window.status='Date Picker';return true;"
                                onmouseout="window.status='';return true;" href="javascript:show_calendar('Form1.txtsalesfrom');"><img
                                    height="21" src="../images/show-calendar.gif" width="24" border="0"></a></FONT>
                        </SELECT></td>
                </tr>
                <tr>
                    <td>
                        Sales Tax Valid To</td>
                    <td>
                        <asp:TextBox ID="txtsalesto" runat="server" MaxLength="12" CssClass="input" onblur="dateReg(this);"
                            size="12"></asp:TextBox><a onmouseover="window.status='Date Picker';return true;"
                                onmouseout="window.status='';return true;" href="javascript:show_calendar('Form1.txtsalesto');"><img
                                    height="21" src="../images/show-calendar.gif" width="24" border="0"></a></FONT>
                        </SELECT></td>
                </tr>
                <tr>
                    <td>
                        Fitness Valid From</td>
                    <td>
                        <asp:TextBox ID="txtfitnessfrom" runat="server" MaxLength="12" CssClass="input" onblur="dateReg(this);"
                            size="12"></asp:TextBox><a onmouseover="window.status='Date Picker';return true;"
                                onmouseout="window.status='';return true;" href="javascript:show_calendar('Form1.txtfitnessfrom');"><img
                                    height="21" src="../images/show-calendar.gif" width="24" border="0"></a></FONT>
                        </SELECT></td>
                </tr>
                <tr>
                    <td>
                        Fitness Valid To</td>
                    <td>
                        <asp:TextBox ID="txtfitnessto" runat="server" MaxLength="12" CssClass="input" onblur="dateReg(this);"
                            size="12"></asp:TextBox><a onmouseover="window.status='Date Picker';return true;"
                                onmouseout="window.status='';return true;" href="javascript:show_calendar('Form1.txtfitnessto');"><img
                                    height="21" src="../images/show-calendar.gif" width="24" border="0"></a></FONT>
                        </SELECT></td>
                </tr>
                <tr>
                    <td>
                        Fitness Issued From</td>
                    <td>
                        <asp:DropDownList ID="ddlfitissuedfrom" runat="server" CssClass="input">
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td>
                        * Last Serviced KM</td>
                    <td>
                        <asp:TextBox ID="txtlastknservice" runat="server" MaxLength="10" CssClass="input"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        * Next Service KM</td>
                    <td>
                        <asp:TextBox ID="txtnextservice" runat="server" MaxLength="10" CssClass="input"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        State Road Valid From</td>
                    <td>
                        <asp:TextBox ID="txtstroadfrom" runat="server" MaxLength="12" CssClass="input" onblur="dateReg(this);"
                            size="12"></asp:TextBox><a onmouseover="window.status='Date Picker';return true;"
                                onmouseout="window.status='';return true;" href="javascript:show_calendar('Form1.txtstroadfrom');"><img
                                    height="21" src="../images/show-calendar.gif" width="24" border="0"></a></FONT>
                        </SELECT></td>
                </tr>
                <tr>
                    <td>
                        State Road Valid To</td>
                    <td>
                        <asp:TextBox ID="txtstrdvldto" runat="server" MaxLength="12" CssClass="input" onblur="dateReg(this);"
                            size="12"></asp:TextBox><a onmouseover="window.status='Date Picker';return true;"
                                onmouseout="window.status='';return true;" href="javascript:show_calendar('Form1.txtstrdvldto');"><img
                                    height="21" src="../images/show-calendar.gif" width="24" border="0"></a></FONT>
                        </SELECT></td>
                </tr>
                <tr>
                    <td>
                        State Road Issued From</td>
                    <td>
                        <asp:DropDownList ID="ddlstissfrom" runat="server" CssClass="input">
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td>
                        Pollution Check Valid From</td>
                    <td>
                        <asp:TextBox ID="txtpolfrom" runat="server" MaxLength="12" CssClass="input" onblur="dateReg(this);"
                            size="12"></asp:TextBox><a onmouseover="window.status='Date Picker';return true;"
                                onmouseout="window.status='';return true;" href="javascript:show_calendar('Form1.txtpolfrom');"><img
                                    height="21" src="../images/show-calendar.gif" width="24" border="0"></a></FONT>
                        </SELECT></td>
                </tr>
                <tr>
                    <td>
                        Pollution Check Valid To</td>
                    <td>
                        <asp:TextBox ID="txtpolvaldto" runat="server" MaxLength="12" CssClass="input" onblur="dateReg(this);"
                            size="12"></asp:TextBox><a onmouseover="window.status='Date Picker';return true;"
                                onmouseout="window.status='';return true;" href="javascript:show_calendar('Form1.txtpolvaldto');"><img
                                    height="21" src="../images/show-calendar.gif" width="24" border="0"></a></FONT>
                        </SELECT></td>
                </tr>
                <tr>
                    <td>
                        Pollution Check Issued From</td>
                    <td>
                        <asp:DropDownList ID="ddlpolisfrom" runat="server" CssClass="input">
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td>
                        Financed By</td>
                    <td>
                        <asp:TextBox ID="txtfinby" runat="server" MaxLength="100" CssClass="input"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        First EMI Date</td>
                    <td>
                        <asp:TextBox ID="txtemi" runat="server" MaxLength="12" CssClass="input" onblur="dateReg(this);"
                            size="12"></asp:TextBox><a onmouseover="window.status='Date Picker';return true;"
                                onmouseout="window.status='';return true;" href="javascript:show_calendar('Form1.txtemi');"><img
                                    height="21" src="../images/show-calendar.gif" width="24" border="0"></a></FONT>
                        </SELECT></td>
                </tr>
                <tr>
                    <td>
                        Last EMI Date</td>
                    <td>
                        <asp:TextBox ID="txtlastemi" runat="server" MaxLength="12" CssClass="input" onblur="dateReg(this);"
                            size="12"></asp:TextBox><a onmouseover="window.status='Date Picker';return true;"
                                onmouseout="window.status='';return true;" href="javascript:show_calendar('Form1.txtlastemi');"><img
                                    height="21" src="../images/show-calendar.gif" width="24" border="0"></a></FONT>
                        </SELECT></td>
                </tr>
                <tr>
                    <td>
                        * EMI Amount</td>
                    <td>
                        <asp:TextBox ID="txtlastemiamt" runat="server" MaxLength="100" CssClass="input"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        DCO Chauffeur</td>
                    <td>
                        <asp:DropDownList ID="ddldcochaufer" runat="server" CssClass="input">
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td>
                        Corporate duty (DCO) revenue sharing %</td>
                    <td>
                        <asp:TextBox ID="txtrevenuesharing" runat="server" MaxLength="10" CssClass="input"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        Self Drive (DCO) revenue sharing %</td>
                    <td>
                        <asp:TextBox ID="txtselfdriverev" runat="server" MaxLength="10" CssClass="input"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        Hotel duty (DCO) revenue sharing %</td>
                    <td>
                        <asp:TextBox ID="txthotel" runat="server" MaxLength="10" CssClass="input"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        Direct Payment (Credit Card) (DCO) revenue sharing %</td>
                    <td>
                        <asp:TextBox ID="txtdirpaysharing" runat="server" MaxLength="10" CssClass="input"></asp:TextBox></td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <asp:Button ID="btnSubmit" runat="server" CssClass="input" Text="Submit"></asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Button ID="btnReset" runat="server" CssClass="input" Text="Reset"></asp:Button></td>
                </tr>
            </asp:Panel>
            <asp:Panel ID="pnlconfirmation" runat="server" Visible="False">
                <tr align="center">
                    <td colspan="2">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label></td>
                </tr>
                <tr align="center">
                    <td colspan="2">
                        <asp:HyperLink ID="hyplnkretry" runat="server"></asp:HyperLink>
                        <input type="hidden" name="ddldcochaufer" />
                        <input type="hidden" name="txtrevenuesharing" />
                        <input type="hidden" name="txtselfdriverev" />
                        <input type="hidden" name="txthotel" />
                        <input type="hidden" name="txtdirpaysharing" />
                    </td>
                </tr>
            </asp:Panel>
            </TBODY></table>
    </form>
</body>
</html>
