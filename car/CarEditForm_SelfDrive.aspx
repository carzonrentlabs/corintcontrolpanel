<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CarEditForm_SelfDrive.aspx.vb"
    Inherits="car_CarEditForm_SelfDrive" %>

<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>CarzonRent :: Internal software</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">

    <script language="JavaScript" src="../utilityfunction.js"></script>

    <script language="JavaScript" src="../JScripts/Datefunc.js"></script>

    <link href="../HertzInt.css" type="text/css" rel="stylesheet">
</head>

<script language="JavaScript">
		
    function checkbeforenext()
    {

        var strvalues
        strvalues=('ddlunit,ddlmodel,txtregsitration,txtengineno,txtchasis,txtregistrationdate,ddlregiplace,ddlyear,txtpurdate,txtlastkm,ddlSubLocation,ddlpermitissue,txtAvailableFrom')
        return checkmandatory(strvalues);

    }
    function checkmandatory(ctrntrlnames)
    {
        var strvalues
        strvalues=ctrntrlnames
        var arrvalue
        arrvalue=strvalues.split(",")
        for (i=0;i<arrvalue.length;i++)
        {
            var cntrlName = eval('document.forms[0].' + arrvalue[i])
            if (cntrlName.value=="")
            {
                alert("Please make sure all the fields marked with * are filled in")
                return false;
            }
        }		
    }
    function dateReg(obj)
    {
        if(obj.value!="")
        {
            // alert(obj.value);
            var reg = /((0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[0-1])-(201[0-9]))/
            if(reg.test(obj.value))
            {
                   //alert('valid');
            }
            else
            {
                    alert('notvalid');
                    obj.value="";
            }
        }
    }
</script>

<body>
    <form id="form1" runat="server">
        <uc1:Headerctrl ID="Headerctrl1" runat="server"></uc1:Headerctrl>
        <div>
            <table align="center">
                <%--   <input id="hdndcoval" type="hidden" name="hdndcoval" value="1" runat="server">--%>
                <%--<asp:Panel ID="pnlmainform" runat="server">--%>
                    <tr>
                        <td align="center" colspan="2">
                            <b><u>Edit a Car</u></b>
                            <br>
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <asp:Label ID="lblErrorMsg" CssClass="subRedHead" Visible="false" runat="server"></asp:Label></td>
                    </tr>
                    <tr>
                    <tr>
                        <td>
                            *Unit</td>
                        <td>
                            <asp:DropDownList ID="ddlunit" runat="server" CssClass="input" AutoPostBack ="true" >
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td>
                            *Model</td>
                        <td>
                            <asp:DropDownList ID="ddlmodel" runat="server" CssClass="input">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td>
                            * Registration #</td>
                        <td>
                            <asp:TextBox ID="txtregsitration" runat="server" MaxLength="250" CssClass="input"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>
                            AC?</td>
                        <td>
                            <asp:CheckBox ID="chkac" runat="server" CssClass="input" Checked="True"></asp:CheckBox></td>
                    </tr>
                    <tr>
                        <td>
                            * Engine #</td>
                        <td>
                            <asp:TextBox ID="txtengineno" runat="server" MaxLength="20" CssClass="input"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>
                            * Chassis #</td>
                        <td>
                            <asp:TextBox ID="txtchasis" runat="server" MaxLength="20" CssClass="input"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>
                            * Registration Date</td>
                        <td>
                            <asp:TextBox ID="txtregistrationdate" runat="server" MaxLength="12" CssClass="input"
                                onblur="dateReg(this);" size="12"></asp:TextBox><a onmouseover="window.status='Date Picker';return true;"
                                    onmouseout="window.status='';return true;" href="javascript:show_calendar('Form1.txtregistrationdate');"><img
                                        height="21" src="../images/show-calendar.gif" width="24" border="0"></a></td>
                    </tr>
                    <tr>
                        <td>
                            * Place of Registration</td>
                        <td>
                            <asp:DropDownList ID="ddlregiplace" runat="server" CssClass="input">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td>
                            * Year of Manufacture</td>
                        <td>
                            <asp:DropDownList ID="ddlyear" runat="server" CssClass="input">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td>
                            * Purchase Date</td>
                        <td>
                            <asp:TextBox ID="txtpurdate" runat="server" MaxLength="12" CssClass="input" onblur="dateReg(this);"
                                size="12"></asp:TextBox><a onmouseover="window.status='Date Picker';return true;"
                                    onmouseout="window.status='';return true;" href="javascript:show_calendar('Form1.txtpurdate');"><img
                                        height="21" src="../images/show-calendar.gif" width="24" border="0"></a>
                        </td>
                    </tr>
                    <%--  <tr>
                    <td>
                        * Fuel Type</td>
                    <td>
                        <asp:DropDownList ID="ddlfuel" runat="server" CssClass="input">
                        </asp:DropDownList></td>
                </tr>--%>
                    <%--  <tr>
                    <td>
                        * Last KM Reading</td>
                    <td>
                        <asp:TextBox ID="txtlastkm" runat="server" MaxLength="10" CssClass="input"></asp:TextBox></td>
                </tr>--%>
                    <tr>
                        <td>
                            Available for Self Drive</td>
                        <td>
                            <asp:CheckBox ID="SelfDriveAvailableYN" runat="server" CssClass="input" Checked="True">
                            </asp:CheckBox></td>
                    </tr>
                    <tr>
                        <td valign="top">
                            Remarks(<span class="shwText" id="shwMessage">0</span>/2000 chars)</td>
                        <td>
                            <asp:TextBox ID="txtarearemarks" onkeydown="showLength(this.form.txtarearemarks,'document.all.shwMessage',2000)"
                                onkeyup="showLength(this.form.txtarearemarks,'document.all.shwMessage',2000)"
                                runat="server" MaxLength="2000" CssClass="input" Rows="3" Columns="50" TextMode="MultiLine"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>
                            Active</td>
                        <td>
                            <asp:CheckBox ID="chkActive" runat="server" CssClass="input" Checked="True"></asp:CheckBox></td>
                    </tr>
                    <tr>
                        <td>
                            * Sub Location
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlSubLocation" runat="server" CssClass="input">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <%--<asp:Panel ID="pnl2ndform" runat="server" Visible="False">--%>
                    <tr>
                        <td>
                            Permit Valid From</td>
                        <td>
                            <asp:TextBox ID="txtpermitfrom" runat="server" MaxLength="12" CssClass="input" onblur="dateReg(this);"
                                size="12"></asp:TextBox><a onmouseover="window.status='Date Picker';return true;"
                                    onmouseout="window.status='';return true;" href="javascript:show_calendar('Form1.txtpermitfrom');"><img
                                        height="21" src="../images/show-calendar.gif" width="24" border="0"></a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Permit Valid To</td>
                        <td>
                            <asp:TextBox ID="txtpermitto" runat="server" MaxLength="12" CssClass="input" onblur="dateReg(this);"
                                size="12"></asp:TextBox><a onmouseover="window.status='Date Picker';return true;"
                                    onmouseout="window.status='';return true;" href="javascript:show_calendar('Form1.txtpermitto');"><img
                                        height="21" src="../images/show-calendar.gif" width="24" border="0"></a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            * Permit Issued From</td>
                        <td>
                            <asp:DropDownList ID="ddlpermitissue" runat="server" CssClass="input">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td>
                            * Available From</td>
                        <td>
                            <asp:TextBox ID="txtAvailableFrom" runat="server" MaxLength="12" CssClass="input"
                                onblur="dateReg(this);" size="12"></asp:TextBox><a onmouseover="window.status='Date Picker';return true;"
                                    onmouseout="window.status='';return true;" href="javascript:show_calendar('Form1.txtAvailableFrom');"><img
                                        height="21" src="../images/show-calendar.gif" width="24" border="0"></a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            No of Suitcases</td>
                        <td>
                            <asp:TextBox ID="txtTotalSuitCases" runat="server" MaxLength="12" CssClass="input"
                                size="12"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Auto Manual</td>
                        <td>
                            <asp:DropDownList ID="ddlAutoManual" runat="server">
                                <asp:ListItem Value="0" Text="0"></asp:ListItem>
                                <asp:ListItem Value="1" Text="1"></asp:ListItem>
                                <asp:ListItem Value="2" Text="2"></asp:ListItem>
                                <asp:ListItem Value="3" Text="3"></asp:ListItem>
                                <asp:ListItem Value="4" Text="4"></asp:ListItem>
                                <asp:ListItem Value="5" Text="5"></asp:ListItem>
                                <asp:ListItem Value="6" Text="5"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <asp:Button ID="btnSubmit" runat="server" CssClass="input" Text="Submit"></asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Button ID="btnReset" runat="server" CssClass="input" Text="Reset"></asp:Button></td>
                    </tr>
                <%--</asp:Panel>--%>
                <asp:Panel ID="pnlconfirmation" runat="server" Visible="False">
                    <tr align="center">
                        <td colspan="2">
                            <asp:Label ID="lblMessage" runat="server"></asp:Label></td>
                    </tr>
                    <tr align="center">
                        <td colspan="2">
                            <asp:HyperLink ID="hyplnkretry" runat="server"></asp:HyperLink>
                            <input type="hidden" name="ddldcochaufer" />
                            <input type="hidden" name="txtrevenuesharing" />
                            <input type="hidden" name="txtselfdriverev" />
                            <input type="hidden" name="txthotel" />
                            <input type="hidden" name="txtdirpaysharing" />
                        </td>
                    </tr>
                </asp:Panel>
            </table>
        </div>
    </form>
</body>
</html>
