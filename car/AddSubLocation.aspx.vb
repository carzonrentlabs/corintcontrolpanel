Imports commonutility

Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Partial Class car_AddSubLocation
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            populateddl()
        End If
    End Sub
    Sub populateddl()
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility
        ddlCityName.DataSource = objAcessdata.funcGetSQLDataReader("select CityName ,CityID  from CORIntCityMaster  where active='1'  order by CityName")
        ddlCityName.DataValueField = "CityID"
        ddlCityName.DataTextField = "CityName"
        ddlCityName.DataBind()
        ddlCityName.Items.Insert(0, New ListItem("", ""))
        objAcessdata.Dispose()

    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            Dim MyConnection As SqlConnection
            MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
            Dim cmd As SqlCommand
            cmd = New SqlCommand("prc_AddSubLocation", MyConnection)
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.Add("@CityID", ddlCityName.SelectedItem.Value)
            cmd.Parameters.Add("@SubLocationName", txtSubLacotionName.Text)
            cmd.Parameters.Add("@SubLocationAddress", txtSubLocationAddress.Text)
            cmd.Parameters.Add("@Active", get_YNvalue(chkActive))
            MyConnection.Open()
            Dim status As Integer
            status = cmd.ExecuteNonQuery()
            MyConnection.Close()
           
            If status = 1 Then
                lblMessage.Text = "You have added the Sub Location successfully"
                HyplnkRetry.Text = "Add another Location"
                HyplnkRetry.NavigateUrl = "AddSubLocation.aspx"
                pnlmainform.Visible = False
                pnlconfirmation.Visible = True
            Else
                lblMessage.Text = "Location Not Added"
                HyplnkRetry.Text = "Add another Location"
                HyplnkRetry.NavigateUrl = "AddSubLocation.aspx"
                pnlmainform.Visible = True
                pnlconfirmation.Visible = True
            End If
            
        Catch ex As Exception

        End Try
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function
End Class
