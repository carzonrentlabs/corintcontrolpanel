Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.DBNull
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Partial Class car_CarEditForm_SelfDrive
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        btnSubmit.Attributes("onClick") = "return checkbeforenext();"
        If Not Page.IsPostBack Then
            populateddl()
            Dim dtrreader As SqlDataReader
            Dim accessdata As clsutility
            accessdata = New clsutility
            dtrreader = accessdata.funcGetSQLDataReader("select * from CORIntCarMaster where  carid=" & Request.QueryString("id") & " ")
            dtrreader.Read()
            chkac.Checked = dtrreader("acyn")
            txtregistrationdate.Text = dtrreader("regndate") & ""
            txtpurdate.Text = dtrreader("purchasedate") & ""
            'txtpermit.Text = dtrreader("permitno") & ""
            txtpermitfrom.Text = dtrreader("permitfrom") & ""
            txtpermitto.Text = dtrreader("permitto") & ""
            autoselec_ddl(ddlunit, dtrreader("unitid"))
            autoselec_ddl(ddlmodel, dtrreader("carmodelid"))
            FillSublocation(dtrreader("UnitID"))
            If IsDBNull(dtrreader("permitcityid")) = True Then
                ddlpermitissue.SelectedValue = ""
            Else
                autoselec_ddl(ddlpermitissue, dtrreader("permitcityid"))
            End If

            If IsDBNull(dtrreader("regncityid")) = True Then
                ddlregiplace.SelectedValue = ""
            Else
                autoselec_ddl(ddlregiplace, dtrreader("regncityid"))
            End If

            If IsDBNull(dtrreader("subLocationID")) = True Then
                ddlSubLocation.SelectedValue = ""
            Else
                autoselec_ddl(ddlSubLocation, dtrreader("subLocationID"))
            End If
            autoselec_ddl(ddlyear, dtrreader("manufactureyr"))
            txtregsitration.Text = dtrreader("regnno") & ""
            txtengineno.Text = dtrreader("engineno") & ""
            txtTotalSuitCases.Text = dtrreader("Total_suitcases") & ""
            txtchasis.Text = dtrreader("chassisno") & ""
            txtarearemarks.Text = dtrreader("remarks") & ""
            chkActive.Checked = dtrreader("active")
            dtrreader.Close()
            accessdata.Dispose()
        Else
        End If
    End Sub

    Function autoselec_ddl(ByVal ddlname As DropDownList, ByVal selectvalue As Int32)
        If Not IsDBNull(selectvalue) Then
            If Not selectvalue = 0 Then
                Try
                    '' ddlname.Items.FindByValue(selectvalue).Selected = True
                    ddlname.SelectedValue = selectvalue
                Catch ex As Exception
                End Try

            End If
        End If
    End Function
    Function FillSublocation(ByVal unitid As Integer)
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("corConnectString"))
        Dim cmd As SqlCommand

        Dim adpSubLoc As SqlDataAdapter
        Dim dsSubLoc As DataSet
        cmd = New SqlCommand("Prc_GetSelfDriveSublocation_UnitBasis", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@UserId", Session("loggedin_user"))
        cmd.Parameters.AddWithValue("@SelectUnitId", unitid)
        adpSubLoc = New SqlDataAdapter(cmd)
        dsSubLoc = New DataSet()
        adpSubLoc.Fill(dsSubLoc)
        If dsSubLoc.Tables(0).Rows.Count > 0 Then
            ddlSubLocation.DataSource = dsSubLoc.Tables(0)
            ddlSubLocation.DataTextField = "SubLocationName"
            ddlSubLocation.DataValueField = "SubLocationID"
            ddlSubLocation.DataBind()
            ddlSubLocation.Items.Insert(0, New ListItem("-select-", 0))
        Else
            ddlSubLocation.Items.Insert(0, "-select-")
        End If
        MyConnection.Dispose()
    End Function

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        Dim intuniqcheck As SqlParameter
        Dim intuniqvalue As Int32

        cmd = New SqlCommand("prc_EditCarMaster_SelfDrive", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@rowid", Request.QueryString("id"))
        cmd.Parameters.Add("@unitid", ddlunit.SelectedItem.Value)
        cmd.Parameters.Add("@carmodelid", ddlmodel.SelectedItem.Value)
        cmd.Parameters.Add("@regnno", txtregsitration.Text)
        cmd.Parameters.Add("@acyn", get_YNvalue(chkac))
        cmd.Parameters.Add("@engineno", txtengineno.Text)
        cmd.Parameters.Add("@chassisno", txtchasis.Text)
        cmd.Parameters.Add("@regndate", txtregistrationdate.Text)
        cmd.Parameters.Add("@regncityid", ddlregiplace.SelectedItem.Value)
        cmd.Parameters.Add("@manufactureyr", ddlyear.SelectedItem.Value)
        cmd.Parameters.Add("@purchasedate", txtpurdate.Text)

        If Not txtpermitfrom.Text = "" Then
            cmd.Parameters.Add("@permitfrom", txtpermitfrom.Text)
        End If
        If Not txtpermitto.Text = "" Then
            cmd.Parameters.Add("@permitto", txtpermitto.Text)
        End If
        cmd.Parameters.Add("@permitcityid", ddlpermitissue.SelectedItem.Value)
        cmd.Parameters.Add("@SelfDriveAvailableYN", get_YNvalue(SelfDriveAvailableYN))
        cmd.Parameters.Add("@remarks", txtarearemarks.Text)
        cmd.Parameters.Add("@active", get_YNvalue(chkActive))
        cmd.Parameters.Add("@modifiedby", Session("loggedin_user"))
        cmd.Parameters.Add("@subLocationID", ddlSubLocation.SelectedItem.Value)
        cmd.Parameters.Add("@total_suitcases", txtTotalSuitCases.Text)
        intuniqcheck = cmd.Parameters.Add("@uniqcheckval", SqlDbType.Int)
        intuniqcheck.Direction = ParameterDirection.Output
        '  Try
        MyConnection.Open()
        cmd.ExecuteNonQuery()
        intuniqvalue = cmd.Parameters("@uniqcheckval").Value
        MyConnection.Close()
        If intuniqvalue = 1 Then
            lblErrorMsg.Visible = True
            lblErrorMsg.Text = "Car No already exist."
            Exit Sub
        Else
            pnlconfirmation.Visible = True
            lblMessage.Text = "You have updated the Car successfully"
            hyplnkretry.Text = "Edit another Car"
            hyplnkretry.NavigateUrl = "CarEditSearch.aspx"
        End If
        '   Catch
        '   End Try
    End Sub

    Sub populateddl()
        Dim objAcessdata As clsutility
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("corConnectString"))
        objAcessdata = New clsutility
        Dim cmd As SqlCommand
        'ddlunit.DataSource = objAcessdata.funcGetSQLDataReader("select unitname,unitid from CORIntUnitMaster where active=1 order by unitname")
        'ddlunit.DataValueField = "unitid"
        'ddlunit.DataTextField = "unitname"
        'ddlunit.DataBind()
        'ddlunit.Items.Insert(0, New ListItem("", ""))
        Dim adpUnit As SqlDataAdapter
        Dim dsUnit As DataSet
        cmd = New SqlCommand("Prc_GetSelfDriveAccessUnit", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@UserId", Session("loggedin_user"))
        adpUnit = New SqlDataAdapter(cmd)
        dsUnit = New DataSet()
        adpUnit.Fill(dsUnit)
        If dsUnit.Tables(0).Rows.Count > 0 Then
            ddlunit.DataSource = dsUnit.Tables(0)
            ddlunit.DataTextField = "unitName"
            ddlunit.DataValueField = "UnitID"
            ddlunit.DataBind()
            ddlunit.Items.Insert(0, New ListItem("-select-", 0))
        Else
            ddlunit.Items.Insert(0, "-select-")
        End If


        ddlmodel.DataSource = objAcessdata.funcGetSQLDataReader("select m.carmodelid, C.carcompname+' '+M.carmodelname as carname from CORIntCarCompMaster C, CORIntCarModelMaster M where C.carcompid=M.carcompid and C.active='1' and M.active='1' order by carname ")
        ddlmodel.DataValueField = "carmodelid"
        ddlmodel.DataTextField = "carname"
        ddlmodel.DataBind()
        ddlmodel.Items.Insert(0, New ListItem("", ""))
        ddlregiplace.DataSource = objAcessdata.funcGetSQLDataReader("select CityName ,CityID  from CORIntCityMaster  where active='1'  order by CityName")
        ddlregiplace.DataValueField = "CityID"
        ddlregiplace.DataTextField = "CityName"
        ddlregiplace.DataBind()
        ddlregiplace.Items.Insert(0, New ListItem("", ""))
        objAcessdata.funcpopulatenumddw(Year(Now), 1950, ddlyear)
        ddlpermitissue.DataSource = objAcessdata.funcGetSQLDataReader("select CityName ,CityID  from CORIntCityMaster  where active='1'  order by CityName")
        ddlpermitissue.DataValueField = "CityID"
        ddlpermitissue.DataTextField = "CityName"
        ddlpermitissue.DataBind()
        ddlpermitissue.Items.Insert(0, New ListItem("", ""))
        ddlpermitissue.DataSource = objAcessdata.funcGetSQLDataReader("select CityName ,CityID  from CORIntCityMaster  where active='1'  order by CityName")
        ddlpermitissue.DataValueField = "CityID"
        ddlpermitissue.DataTextField = "CityName"
        ddlpermitissue.DataBind()
        ddlpermitissue.Items.Insert(0, New ListItem("", ""))
        'ddlSubLocation.DataSource = objAcessdata.funcGetSQLDataReader("select SubLocationID,SubLocationName from CorIntSubLocationMaster where Active=1 order by SubLocationName")
        'ddlSubLocation.DataValueField = "SubLocationID"
        'ddlSubLocation.DataTextField = "SubLocationName"
        'ddlSubLocation.DataBind()
        'ddlSubLocation.Items.Insert(0, New ListItem("", ""))        

        objAcessdata.Dispose()
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("CarEditForm_SelfDrive.aspx?id=" & Request.QueryString("id"))
    End Sub

    Protected Sub ddlunit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlunit.SelectedIndexChanged
        Dim objAcessdata As clsutility
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("corConnectString"))
        objAcessdata = New clsutility
        Dim cmd As SqlCommand

        Dim adpSubLoc As SqlDataAdapter
        Dim dsSubLoc As DataSet
        Dim selectUnit As Integer
        selectUnit = ddlunit.SelectedValue

        cmd = New SqlCommand("Prc_GetSelfDriveSublocation_UnitBasis", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@UserId", Session("loggedin_user"))
        cmd.Parameters.AddWithValue("@SelectUnitId", selectUnit)
        adpSubLoc = New SqlDataAdapter(cmd)
        dsSubLoc = New DataSet()
        adpSubLoc.Fill(dsSubLoc)
        If dsSubLoc.Tables(0).Rows.Count > 0 Then
            ddlSubLocation.DataSource = dsSubLoc.Tables(0)
            ddlSubLocation.DataTextField = "SubLocationName"
            ddlSubLocation.DataValueField = "SubLocationID"
            ddlSubLocation.DataBind()
            ddlSubLocation.Items.Insert(0, New ListItem("-select-", 0))
        Else
            ddlSubLocation.DataSource = ""
            ddlSubLocation.DataBind()
            ddlSubLocation.Items.Insert(0, "-select-")
        End If

        Dim accessdata As clsutility
        accessdata = New clsutility
        Dim dtrreader As SqlDataReader
        dtrreader = accessdata.funcGetSQLDataReader("select * from CORIntCarMaster where  carid=" & Request.QueryString("id") & " ")
        dtrreader.Read()
        If IsDBNull(dtrreader("subLocationID")) = True Then
            ddlSubLocation.SelectedValue = ""
        Else
            autoselec_ddl(ddlSubLocation, dtrreader("subLocationID"))
        End If

        MyConnection.Dispose()
        objAcessdata.Dispose()
        accessdata.Dispose()
    End Sub
End Class
