﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SelfDriveCarSublocationShift.aspx.vb"
    Inherits="car_SelfDriveCarSublocationShift" %>

<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CarzonRent :: Internal software</title>
    <link href="../HertzInt.css" type="text/css" rel="stylesheet">
    <script type="text/javascript">
        function checkValidation() {

            //            if (document.getElementById("<%'=txtCarNo.ClientId%>").value == "") {
            //                alert("Enter car number.")
            //                document.getElementById("<%'=txtCarNo.ClientId%>").focus();
            //                return false;
            //            }
            //            else 
            if (document.getElementById("<%=ddlCarNo.ClientId%>").selectedIndex == 0) {
                alert("Select nubmer");
                document.getElementById("<%=ddlCarNo.ClientId%>").focus();
                return false;
            }
            else if (document.getElementById("<%=ddlSubLocation.ClientId%>").selectedIndex == 0) {
                alert("Select sublocation");
                document.getElementById("<%=ddlSubLocation.ClientId%>").focus();
                return false;
            }
        }
    
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <uc1:Headerctrl ID="Headerctrl1" runat="server"></uc1:Headerctrl>
    <div>
        <br />
        <br />
        <table cellpadding="0" cellspacing="0" border="0" align="center">
            <caption>
                <b>Change Sub Location</b></caption>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
                </td>
            </tr>
            <%--<tr>
                <td>
                    Enter Car Number&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox ID="txtCarNo" runat="server" CssClass="input"></asp:TextBox>
                </td>
            </tr>--%>
            <tr>
                <td>
                    Select Car&nbsp;&nbsp;
                </td>
                <td>
                    <asp:DropDownList ID="ddlCarNo" runat="server" AutoPostBack ="true">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    Present Location&nbsp;&nbsp;
                </td>
                <td>
                    <asp:Label ID="lblPresentLocation" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    Sub Location&nbsp;&nbsp;
                </td>
                <td>
                    <asp:DropDownList ID="ddlSubLocation" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:Button ID="bntSave" Text="Save" runat="server" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
