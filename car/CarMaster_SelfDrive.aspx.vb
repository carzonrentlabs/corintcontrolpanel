Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Partial Class car_CarMaster_SelfDrive
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        btnSubmit.Attributes("onClick") = "return checkmandatory();"
        btnSubmit.Attributes("onClick") = "return checkbeforenext();"
        If Not Page.IsPostBack Then
            populateddl()
        End If
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            Dim MyConnection As SqlConnection
            MyConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("corConnectString"))
            Dim cmd As SqlCommand
            Dim intuniqcheck As SqlParameter
            Dim intuniqvalue As Int32

            cmd = New SqlCommand("prc_AddCarMaster_SelfDrive", MyConnection)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@unitid", ddlunit.SelectedItem.Value)
            cmd.Parameters.Add("@carmodelid", ddlmodel.SelectedItem.Value)
            cmd.Parameters.Add("@regnno", txtregsitration.Text)
            cmd.Parameters.Add("@acyn", get_YNvalue(chkac))
            cmd.Parameters.Add("@engineno", txtengineno.Text)
            cmd.Parameters.Add("@chassisno", txtchasis.Text)
            ' cmd.Parameters.Add("@lastkm", txtlastkm.Text)
            cmd.Parameters.Add("@regndate", txtregistrationdate.Text)
            cmd.Parameters.Add("@regncityid", ddlregiplace.SelectedItem.Value)
            cmd.Parameters.Add("@manufactureyr", ddlyear.SelectedItem.Value)
            cmd.Parameters.Add("@purchasedate", txtpurdate.Text)
            cmd.Parameters.Add("@permitcityid", ddlpermitissue.SelectedItem.Value)
            'cmd.Parameters.Add("@fueltype", ddlfuel.SelectedItem.Value)
            cmd.Parameters.Add("@SelfDriveAvailableYN", get_YNvalue(SelfDriveAvailableYN))
            cmd.Parameters.Add("@remarks", txtarearemarks.Text)
            cmd.Parameters.Add("@active", get_YNvalue(chkActive))
            cmd.Parameters.Add("@createdby", Session("loggedin_user"))
            cmd.Parameters.Add("@SubLocationId", ddlSubLocation.SelectedItem.Value)
            cmd.Parameters.Add("@TotalNoOfSuitCase", txtTotalSuitCases.Text)
            cmd.Parameters.Add("@AutoManual", ddlAutoManual.SelectedItem.Value)
            'cmd.Parameters.Add("@permitno", txtpermit.Text)
            If Not txtpermitfrom.Text = "" Then
                cmd.Parameters.Add("@permitfrom", txtpermitfrom.Text)
            End If
            If Not txtpermitto.Text = "" Then
                cmd.Parameters.Add("@permitto", txtpermitto.Text)
            End If

            If Not txtAvailableFrom.Text = "" Then
                cmd.Parameters.Add("@AvailableFrom", txtAvailableFrom.Text)
            End If
            intuniqcheck = cmd.Parameters.Add("@uniqcheckval", SqlDbType.Int)
            intuniqcheck.Direction = ParameterDirection.Output

            MyConnection.Open()
            cmd.ExecuteNonQuery()
            intuniqvalue = cmd.Parameters("@uniqcheckval").Value
            MyConnection.Close()

            If intuniqvalue = 1 Then
                lblErrorMsg.Visible = True
                lblErrorMsg.Text = "Car No already exist."
                Exit Sub
            Else
                pnlmainform.Visible = False
                'pnl2ndform.Visible = False
                pnlconfirmation.Visible = True
                lblMessage.Text = "You have added the Car successfully"
                hyplnkretry.Text = "Add another Car"
                hyplnkretry.NavigateUrl = "CarAddForm.aspx"
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

    End Sub

    Sub populateddl()
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility

        'ddlunit.DataSource = objAcessdata.funcGetSQLDataReader("select unitname,unitid from CORIntUnitMaster  where active=1  order by unitname")
        'ddlunit.DataValueField = "unitid"
        'ddlunit.DataTextField = "unitname"
        'ddlunit.DataBind()
        'ddlunit.Items.Insert(0, New ListItem("", ""))

        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("corConnectString"))
        Dim cmd As SqlCommand

        'Below line commented by BK Sharma on 21 May 2014 as per discussed with Rahul sir
        '' '' ''Dim adpSubLoc As SqlDataAdapter
        '' '' ''Dim dsSubLoc As DataSet
        ' '' '' '' cmd = New SqlCommand("Prc_GetSelfDriveSublocation", MyConnection)
        '' '' ''cmd = New SqlCommand("Prc_GetSelfDriveSublocation_UnitBasis", MyConnection)
        '' '' ''cmd.CommandType = CommandType.StoredProcedure
        '' '' ''cmd.Parameters.AddWithValue("@UserId", Session("loggedin_user"))
        '' '' ''adpSubLoc = New SqlDataAdapter(cmd)
        '' '' ''dsSubLoc = New DataSet()
        '' '' ''adpSubLoc.Fill(dsSubLoc)
        '' '' ''If dsSubLoc.Tables(0).Rows.Count > 0 Then

        '' '' ''    ddlSubLocation.DataSource = dsSubLoc.Tables(0)
        '' '' ''    ddlSubLocation.DataTextField = "SubLocationName"
        '' '' ''    ddlSubLocation.DataValueField = "SubLocationID"
        '' '' ''    ddlSubLocation.DataBind()
        '' '' ''    ddlSubLocation.Items.Insert(0, New ListItem("-select-", 0))
        '' '' ''Else
        '' '' ''    ddlSubLocation.Items.Insert(0, "-select-")
        '' '' ''End If
        'End of Above commented 


        Dim adpUnit As SqlDataAdapter
        Dim dsUnit As DataSet
        cmd = New SqlCommand("Prc_GetSelfDriveAccessUnit", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@UserId", Session("loggedin_user"))
        adpUnit = New SqlDataAdapter(cmd)
        dsUnit = New DataSet()
        adpUnit.Fill(dsUnit)
        If dsUnit.Tables(0).Rows.Count > 0 Then

            ddlunit.DataSource = dsUnit.Tables(0)
            ddlunit.DataTextField = "unitName"
            ddlunit.DataValueField = "UnitID"
            ddlunit.DataBind()
            ddlunit.Items.Insert(0, New ListItem("-select-", 0))
        Else
            ddlunit.Items.Insert(0, "-select-")
        End If

        ddlmodel.DataSource = objAcessdata.funcGetSQLDataReader("select m.carmodelid, C.carcompname+' '+M.carmodelname as carname from CORIntCarCompMaster C, CORIntCarModelMaster M where C.carcompid=M.carcompid and C.active='1' and M.active='1' order by carname ")
        ddlmodel.DataValueField = "carmodelid"
        ddlmodel.DataTextField = "carname"
        ddlmodel.DataBind()
        ddlmodel.Items.Insert(0, New ListItem("", ""))

        ddlregiplace.DataSource = objAcessdata.funcGetSQLDataReader("select CityName ,CityID  from CORIntCityMaster  where active='1'  order by CityName")
        ddlregiplace.DataValueField = "CityID"
        ddlregiplace.DataTextField = "CityName"
        ddlregiplace.DataBind()
        ddlregiplace.Items.Insert(0, New ListItem("", ""))

        'ddlfuel.DataSource = objAcessdata.funcGetSQLDataReader("select FuelTypeID, FuelTypeName from CORIntFuelTypeMaster  where active=1 order by FuelTypeName")
        'ddlfuel.DataValueField = "FuelTypeID"
        'ddlfuel.DataTextField = "FuelTypeName"
        'ddlfuel.DataBind()
        'ddlfuel.Items.Insert(0, New ListItem("", ""))

        objAcessdata.funcpopulatenumddw(Year(Now), 1950, ddlyear)

        ddlpermitissue.DataSource = objAcessdata.funcGetSQLDataReader("select CityName ,CityID  from CORIntCityMaster  where active='1'  order by CityName")
        ddlpermitissue.DataValueField = "CityID"
        ddlpermitissue.DataTextField = "CityName"
        ddlpermitissue.DataBind()
        ddlpermitissue.Items.Insert(0, New ListItem("", ""))

        ddlpermitissue.DataSource = objAcessdata.funcGetSQLDataReader("select CityName ,CityID  from CORIntCityMaster  where active='1'  order by CityName")
        ddlpermitissue.DataValueField = "CityID"
        ddlpermitissue.DataTextField = "CityName"
        ddlpermitissue.DataBind()
        ddlpermitissue.Items.Insert(0, New ListItem("", ""))

        'ddlSubLocation.DataSource = objAcessdata.funcGetSQLDataReader("select SubLocationID,SubLocationName from CorIntSubLocationMaster where Active=1 order by SubLocationName")
        'ddlSubLocation.DataValueField = "SubLocationID"
        'ddlSubLocation.DataTextField = "SubLocationName"
        'ddlSubLocation.DataBind()
        'ddlSubLocation.Items.Insert(0, New ListItem("", ""))

        objAcessdata.Dispose()

    End Sub

    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function

    Protected Sub ddlunit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlunit.SelectedIndexChanged

        Dim objAcessdata As clsutility
        objAcessdata = New clsutility
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("corConnectString"))
        Dim selectUnit As Integer
        selectUnit = ddlunit.SelectedValue
        If ddlunit.SelectedIndex > 0 Then
            Dim cmd As SqlCommand
            Dim adpSubLoc As SqlDataAdapter
            Dim dsSubLoc As DataSet
            cmd = New SqlCommand("Prc_GetSelfDriveSublocation_UnitBasis", MyConnection)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@UserId", Session("loggedin_user"))
            cmd.Parameters.AddWithValue("@SelectUnitId", selectUnit)
            adpSubLoc = New SqlDataAdapter(cmd)
            dsSubLoc = New DataSet()
            adpSubLoc.Fill(dsSubLoc)
            If dsSubLoc.Tables(0).Rows.Count > 0 Then
                ddlSubLocation.DataSource = dsSubLoc.Tables(0)
                ddlSubLocation.DataTextField = "SubLocationName"
                ddlSubLocation.DataValueField = "SubLocationID"
                ddlSubLocation.DataBind()
                ddlSubLocation.Items.Insert(0, New ListItem("-select-", 0))
            Else
                ddlSubLocation.DataSource = ""
                ddlSubLocation.DataBind()
                ddlSubLocation.Items.Insert(0, "-select-")
            End If
        Else
            ddlSubLocation.DataSource = ""
            ddlSubLocation.DataBind()
            ddlSubLocation.Items.Insert(0, "-select-")
        End If
    End Sub
End Class
