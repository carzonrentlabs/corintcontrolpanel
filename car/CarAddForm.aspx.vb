Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class CarAddForm
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ddlunit As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtarearemarks As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkActive As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkSpcialType As System.Web.UI.WebControls.CheckBox
    Protected WithEvents btnproceed As System.Web.UI.WebControls.Button
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnReset As System.Web.UI.WebControls.Button
    Protected WithEvents pnl2ndform As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    Protected WithEvents txtpermitto As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlmodel As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtregsitration As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkac As System.Web.UI.WebControls.CheckBox
    Protected WithEvents txtengineno As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtchasis As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtregistrationdate As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlregiplace As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlyear As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtpurdate As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlfuel As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtlastkm As System.Web.UI.WebControls.TextBox
    'Protected WithEvents SelfDriveAvailableYN As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkdco As System.Web.UI.WebControls.CheckBox
    Protected WithEvents txtpermit As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtpermitfrom As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlpermitissue As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txttaxes As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtauthorization As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtauthfrom As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtauthvalidto As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlauthissuefrom As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtinsu As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtinsuto As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtinsufrom As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtinsuamt As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtinscomp As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtsalesfrom As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtsalesto As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtfitnessfrom As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtfitnessto As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlfitissuedfrom As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtlastknservice As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtnextservice As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtstroadfrom As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtstrdvldto As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlstissfrom As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtpolfrom As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtpolvaldto As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlpolisfrom As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtfinby As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtemi As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtlastemi As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtlastemiamt As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddldcochaufer As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtrevenuesharing As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtselfdriverev As System.Web.UI.WebControls.TextBox
    Protected WithEvents txthotel As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtdirpaysharing As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlinsissued As System.Web.UI.WebControls.DropDownList
    Protected WithEvents hdndcoval As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    'Protected WithEvents ddlSubLocation As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtSimMobileNumber As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtIMEINumber As System.Web.UI.WebControls.TextBox
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btnproceed.Attributes("onClick") = "return checkbeforenext();"
        btnSubmit.Attributes("onClick") = "return checkvalue();"
        If Not Page.IsPostBack Then
            populateddl()
        End If
    End Sub

    Private Sub btnproceed_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnproceed.Click
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        Dim intuniqcheck As SqlParameter
        Dim intuniqvalue As Int32
        'execueting the stored procedure for checking the user login validity by using the output parameter
        cmd = New SqlCommand("procCheckCarRegn", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@active", get_YNvalue(chkActive))
        cmd.Parameters.Add("@regnno", txtregsitration.Text)
        If txtSimMobileNumber.Text <> "" Then
            cmd.Parameters.AddWithValue("@SimMobileNumber", txtSimMobileNumber.Text)
        End If
        If txtIMEINumber.Text <> "" Then
            cmd.Parameters.AddWithValue("@IMIENumber", txtIMEINumber.Text)
        End If

        intuniqcheck = cmd.Parameters.Add("@uniqcheckval", SqlDbType.Int)
        intuniqcheck.Direction = ParameterDirection.Output

        MyConnection.Open()
        cmd.ExecuteNonQuery()
        'getting the value to know that whethre the user is vallid user for login or not
        intuniqvalue = cmd.Parameters("@uniqcheckval").Value
        MyConnection.Close()

        If intuniqvalue = 4 Then
            lblErrorMsg.Visible = True
            lblErrorMsg.Text = "Sim number already exist."
            Exit Sub
        ElseIf intuniqvalue = 5 Then
            lblErrorMsg.Visible = True
            lblErrorMsg.Text = "IMIE already exist."
            Exit Sub
        ElseIf Not intuniqvalue = 0 Then
            lblErrorMsg.Visible = True
            lblErrorMsg.Text = "Car RegnNo. already exist."
            Exit Sub
        Else
            lblErrorMsg.Visible = False
            pnlmainform.Visible = False
            pnl2ndform.Visible = True
        End If

    End Sub
    Sub populateddl()
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility

        ddlunit.DataSource = objAcessdata.funcGetSQLDataReader("select unitname,unitid from CORIntUnitMaster  where active=1 order by unitname")
        ddlunit.DataValueField = "unitid"
        ddlunit.DataTextField = "unitname"
        ddlunit.DataBind()
        ddlunit.Items.Insert(0, New ListItem("", ""))

        ddlmodel.DataSource = objAcessdata.funcGetSQLDataReader("select m.carmodelid, C.carcompname+' '+M.carmodelname as carname from CORIntCarCompMaster C, CORIntCarModelMaster M where C.carcompid=M.carcompid and C.active='1' and M.active='1' order by carname ")
        ddlmodel.DataValueField = "carmodelid"
        ddlmodel.DataTextField = "carname"
        ddlmodel.DataBind()
        ddlmodel.Items.Insert(0, New ListItem("", ""))

        ddlregiplace.DataSource = objAcessdata.funcGetSQLDataReader("select CityName ,CityID  from CORIntCityMaster  where active='1'  order by CityName")
        ddlregiplace.DataValueField = "CityID"
        ddlregiplace.DataTextField = "CityName"
        ddlregiplace.DataBind()
        ddlregiplace.Items.Insert(0, New ListItem("", ""))

        ddlfuel.DataSource = objAcessdata.funcGetSQLDataReader("select FuelTypeID, FuelTypeName from CORIntFuelTypeMaster  where active=1 order by FuelTypeName")
        ddlfuel.DataValueField = "FuelTypeID"
        ddlfuel.DataTextField = "FuelTypeName"
        ddlfuel.DataBind()
        ddlfuel.Items.Insert(0, New ListItem("", ""))

        objAcessdata.funcpopulatenumddw(Year(Now), 1950, ddlyear)

        ddlpermitissue.DataSource = objAcessdata.funcGetSQLDataReader("select CityName ,CityID  from CORIntCityMaster  where active='1'  order by CityName")
        ddlpermitissue.DataValueField = "CityID"
        ddlpermitissue.DataTextField = "CityName"
        ddlpermitissue.DataBind()
        ddlpermitissue.Items.Insert(0, New ListItem("", ""))


        ddlpermitissue.DataSource = objAcessdata.funcGetSQLDataReader("select CityName ,CityID  from CORIntCityMaster  where active='1'  order by CityName")
        ddlpermitissue.DataValueField = "CityID"
        ddlpermitissue.DataTextField = "CityName"
        ddlpermitissue.DataBind()
        ddlpermitissue.Items.Insert(0, New ListItem("", ""))

        ddlauthissuefrom.DataSource = objAcessdata.funcGetSQLDataReader("select CityName ,CityID  from CORIntCityMaster  where active='1'  order by CityName")
        ddlauthissuefrom.DataValueField = "CityID"
        ddlauthissuefrom.DataTextField = "CityName"
        ddlauthissuefrom.DataBind()
        ddlauthissuefrom.Items.Insert(0, New ListItem("", ""))

        ddlinsissued.DataSource = objAcessdata.funcGetSQLDataReader("select CityName ,CityID  from CORIntCityMaster  where active='1'  order by CityName")
        ddlinsissued.DataValueField = "CityID"
        ddlinsissued.DataTextField = "CityName"
        ddlinsissued.DataBind()
        ddlinsissued.Items.Insert(0, New ListItem("", ""))

        ddlfitissuedfrom.DataSource = objAcessdata.funcGetSQLDataReader("select CityName ,CityID  from CORIntCityMaster  where active='1'  order by CityName")
        ddlfitissuedfrom.DataValueField = "CityID"
        ddlfitissuedfrom.DataTextField = "CityName"
        ddlfitissuedfrom.DataBind()
        ddlfitissuedfrom.Items.Insert(0, New ListItem("", ""))

        ddlstissfrom.DataSource = objAcessdata.funcGetSQLDataReader("select CityName ,CityID  from CORIntCityMaster  where active='1'  order by CityName")
        ddlstissfrom.DataValueField = "CityID"
        ddlstissfrom.DataTextField = "CityName"
        ddlstissfrom.DataBind()
        ddlstissfrom.Items.Insert(0, New ListItem("", ""))

        ddlpolisfrom.DataSource = objAcessdata.funcGetSQLDataReader("select CityName ,CityID  from CORIntCityMaster  where active='1'  order by CityName")
        ddlpolisfrom.DataValueField = "CityID"
        ddlpolisfrom.DataTextField = "CityName"
        ddlpolisfrom.DataBind()
        ddlpolisfrom.Items.Insert(0, New ListItem("", ""))

        ddldcochaufer.DataSource = objAcessdata.funcGetSQLDataReader("select Fname+' '+isnull(mname,'')+' '+lname as chafeurname ,ChauffeurID   from CORIntChauffeurMaster   where active='1'  order by chafeurname")
        ddldcochaufer.DataValueField = "ChauffeurID"
        ddldcochaufer.DataTextField = "chafeurname"
        ddldcochaufer.DataBind()
        ddldcochaufer.Items.Insert(0, New ListItem("", ""))

        'ddlSubLocation.DataSource = objAcessdata.funcGetSQLDataReader("select SubLocationID,SubLocationName from CorIntSubLocationMaster where Active=1 order by SubLocationName")
        'ddlSubLocation.DataValueField = "SubLocationID"
        'ddlSubLocation.DataTextField = "SubLocationName"
        'ddlSubLocation.DataBind()
        'ddlSubLocation.Items.Insert(0, New ListItem("", ""))

        objAcessdata.Dispose()

    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        Dim intuniqcheck As SqlParameter
        Dim intuniqvalue As Int32
        cmd = New SqlCommand("procAsscarmaster", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("@unitid", ddlunit.SelectedItem.Value)
        cmd.Parameters.Add("@carmodelid", ddlmodel.SelectedItem.Value)
        cmd.Parameters.Add("@regnno", txtregsitration.Text)
        cmd.Parameters.Add("@acyn", get_YNvalue(chkac))
        cmd.Parameters.Add("@engineno", txtengineno.Text)

        cmd.Parameters.Add("@chassisno", txtchasis.Text)
        cmd.Parameters.Add("@lastkm", txtlastkm.Text)
        cmd.Parameters.Add("@regndate", txtregistrationdate.Text)
        cmd.Parameters.Add("@regncityid", ddlregiplace.SelectedItem.Value)

        cmd.Parameters.Add("@manufactureyr", ddlyear.SelectedItem.Value)
        cmd.Parameters.Add("@purchasedate", txtpurdate.Text)
        cmd.Parameters.Add("@permitno", txtpermit.Text)
        If Not txtpermitfrom.Text = "" Then
            cmd.Parameters.Add("@permitfrom", txtpermitfrom.Text)
        End If
        If Not txtpermitto.Text = "" Then
            cmd.Parameters.Add("@permitto", txtpermitto.Text)
        End If
        cmd.Parameters.Add("@permitcityid", ddlpermitissue.SelectedItem.Value)
        cmd.Parameters.Add("@taxpermitcost", txttaxes.Text)
        cmd.Parameters.Add("@fueltype", ddlfuel.SelectedItem.Value)

        cmd.Parameters.Add("@authrisncityid", ddlauthissuefrom.SelectedItem.Value)
        cmd.Parameters.Add("@authrisnno", txtauthorization.Text)
        If Not txtauthfrom.Text = "" Then
            cmd.Parameters.Add("@authrisnfrom", txtauthfrom.Text)
        End If
        If Not txtauthvalidto.Text = "" Then
            cmd.Parameters.Add("@authrisnto", txtauthvalidto.Text)
        End If
        cmd.Parameters.Add("@insurepolno", txtinsu.Text)

        cmd.Parameters.Add("@insurepolcityid", ddlinsissued.SelectedItem.Value)
        If Not txtinsufrom.Text = "" Then
            cmd.Parameters.Add("@insurepolfrom", txtinsufrom.Text)
        End If
        If Not txtinsuto.Text = "" Then
            cmd.Parameters.Add("@insurepolto", txtinsuto.Text)
        End If
        cmd.Parameters.Add("@insurepolco", txtinscomp.Text)
        cmd.Parameters.Add("@insurepolamt", txtinsuamt.Text)

        If Not txtsalesfrom.Text = "" Then
            cmd.Parameters.Add("@salestaxfrom", txtsalesfrom.Text)
        End If
        If Not txtsalesto.Text = "" Then
            cmd.Parameters.Add("@salestaxto", txtsalesto.Text)
        End If
        If Not txtfitnessfrom.Text = "" Then
            cmd.Parameters.Add("@fitnessfrom", txtfitnessfrom.Text)
        End If
        If Not txtfitnessto.Text = "" Then
            cmd.Parameters.Add("@fitnessto", txtfitnessto.Text)
        End If
        cmd.Parameters.Add("@fitnesscityid", ddlfitissuedfrom.SelectedItem.Value)

        cmd.Parameters.Add("@lastservicekm", txtlastknservice.Text)
        cmd.Parameters.Add("@nextservicekm", txtnextservice.Text)
        cmd.Parameters.Add("@stateroadissuecityid", ddlstissfrom.SelectedItem.Value)
        If Not txtstroadfrom.Text = "" Then
            cmd.Parameters.Add("@stateroadissuefrom", txtstroadfrom.Text)
        End If
        If Not txtstrdvldto.Text = "" Then
            cmd.Parameters.Add("@stateroadissueto", txtstrdvldto.Text)
        End If


        cmd.Parameters.Add("@pollutioncityid", ddlpolisfrom.SelectedItem.Value)
        cmd.Parameters.Add("@pollutionfrom", txtpolfrom.Text)
        cmd.Parameters.Add("@pollutionto", txtpolvaldto.Text)
        cmd.Parameters.Add("@financedby", txtfinby.Text)
        If Not txtemi.Text = "" Then
            cmd.Parameters.Add("@emifrom", txtemi.Text)
        End If
        If Not txtlastemi.Text = "" Then
            cmd.Parameters.Add("@emito", txtlastemi.Text)
        End If
        cmd.Parameters.Add("@emiamt", txtlastemiamt.Text)
        cmd.Parameters.Add("@SelfDriveAvailableYN", "0") 'get_YNvalue(SelfDriveAvailableYN))
        cmd.Parameters.Add("@dcoyn", get_YNvalue(chkdco))
        cmd.Parameters.Add("@dcochauffeurid", ddldcochaufer.SelectedItem.Value)
        If Not txtrevenuesharing.Text = "" Then
            cmd.Parameters.Add("@corporatedcopercent", txtrevenuesharing.Text)
        End If
        If Not txtselfdriverev.Text = "" Then
            cmd.Parameters.Add("@selfdrivedcopercent", txtselfdriverev.Text)
        End If
        If Not txthotel.Text = "" Then
            cmd.Parameters.Add("@hoteldcopercent", txthotel.Text)
        End If
        If Not txtdirpaysharing.Text = "" Then
            cmd.Parameters.Add("@ccdcopercent", txtdirpaysharing.Text)
        End If
        cmd.Parameters.Add("@remarks", txtarearemarks.Text)
        cmd.Parameters.Add("@active", get_YNvalue(chkActive))
        cmd.Parameters.Add("@createdby", Session("loggedin_user"))
        cmd.Parameters.Add("@specialType", get_YNvalue(chkSpcialType))
        cmd.Parameters.Add("@SubLocationId", "0") 'ddlSubLocation.SelectedItem.Value)

        If Not txtSimMobileNumber.Text = "" Then
            cmd.Parameters.Add("@SimMobileNo", txtSimMobileNumber.Text)
        End If
        If Not txtIMEINumber.Text = "" Then
            cmd.Parameters.Add("@IEMI", txtIMEINumber.Text)
        End If
        intuniqcheck = cmd.Parameters.Add("@uniqcheckval", SqlDbType.Int)
        intuniqcheck.Direction = ParameterDirection.Output
        '  Response.Write(txtlastkm.Text)
        '  Response.End()
        ' Try
        MyConnection.Open()
        cmd.ExecuteNonQuery()
        intuniqvalue = cmd.Parameters("@uniqcheckval").Value

        MyConnection.Close()
        
        If intuniqvalue = 1 Then
            lblErrorMsg.Visible = True
            lblErrorMsg.Text = "Car No already exist."
            Exit Sub
        Else
            pnlmainform.Visible = False
            pnl2ndform.Visible = False
            pnlconfirmation.Visible = True
            lblMessage.Text = "You have added the Car successfully"
            hyplnkretry.Text = "Add another Car"
            hyplnkretry.NavigateUrl = "CarAddForm.aspx"
        End If
        '   Catch
        '   End Try
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("carAddForm.aspx")
    End Sub
End Class
