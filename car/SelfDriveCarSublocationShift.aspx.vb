﻿Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Partial Class car_SelfDriveCarSublocationShift
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Page.IsPostBack) Then
            bntSave.Attributes.Add("onclick", "return checkValidation();")
            BindCar()
            BindSubLocation()
        End If
        bntSave.Attributes.Add("onclick", "return checkValidation();")
    End Sub

  
    Private Sub BindCar()
        Try
            Dim MyConnection As SqlConnection
            MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
            Dim cmd As SqlCommand
            Dim adpCar As SqlDataAdapter
            Dim dsCar As DataSet
            cmd = New SqlCommand("Prc_GetSelfDriveCar", MyConnection)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@UserId", Session("loggedin_user"))
            adpCar = New SqlDataAdapter(cmd)
            dsCar = New DataSet()
            adpCar.Fill(dsCar)
            If dsCar.Tables(0).Rows.Count > 0 Then

                ddlCarNo.DataSource = dsCar.Tables(0)
                ddlCarNo.DataTextField = "RegnNo"
                ddlCarNo.DataValueField = "CarID"
                ddlCarNo.DataBind()
                ddlCarNo.Items.Insert(0, New ListItem("-select-", 0))
            Else
                ddlCarNo.Items.Insert(0, "-select-")
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub BindSubLocation()
        Try
            Dim MyConnection As SqlConnection
            MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
            Dim cmd As SqlCommand
            Dim adpSubLoc As SqlDataAdapter
            Dim dsSubLoc As DataSet
            cmd = New SqlCommand("Prc_GetSelfDriveSublocation", MyConnection)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@UserId", Session("loggedin_user"))
            adpSubLoc = New SqlDataAdapter(cmd)
            dsSubLoc = New DataSet()
            adpSubLoc.Fill(dsSubLoc)
            If dsSubLoc.Tables(0).Rows.Count > 0 Then

                ddlSubLocation.DataSource = dsSubLoc.Tables(0)
                ddlSubLocation.DataTextField = "SubLocationName"
                ddlSubLocation.DataValueField = "SubLocationID"
                ddlSubLocation.DataBind()
                ddlSubLocation.Items.Insert(0, New ListItem("-select-", 0))
            Else
                ddlSubLocation.Items.Insert(0, "-select-")
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Protected Sub bntSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles bntSave.Click
        If ddlCarNo.SelectedIndex > 0 And ddlSubLocation.SelectedIndex > 0 Then
            Try
                Dim MyConnection As SqlConnection
                Dim status As Integer
                MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
                Dim cmd As SqlCommand
                cmd = New SqlCommand("Prc_UpdateSelfDriveSublocation", MyConnection)
                cmd.CommandType = CommandType.StoredProcedure
                MyConnection.Open()
                cmd.Parameters.AddWithValue("@UserId", Session("loggedin_user"))
                cmd.Parameters.AddWithValue("@CarId", ddlCarNo.SelectedValue)
                cmd.Parameters.AddWithValue("@SubLocation", ddlSubLocation.SelectedValue)
                status = cmd.ExecuteNonQuery()
                If status > 0 Then
                    lblMessage.Visible = True
                    lblMessage.Text = "Record updated sucsessfully"
                    lblMessage.ForeColor = Drawing.Color.Red
                    ddlSubLocation.SelectedIndex = 0
                Else
                    lblMessage.Visible = False
                End If
                MyConnection.Dispose()
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        Else
            MsgBox("Select correct Car and sub Location")

        End If

    End Sub

    Protected Sub ddlCarNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCarNo.SelectedIndexChanged
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        Dim adpSubLocationName As SqlDataAdapter
        Dim dsSubLoc As DataSet
        cmd = New SqlCommand("Prc_SelfDriveGetSubLocation", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@CarID", ddlCarNo.SelectedValue)
        adpSubLocationName = New SqlDataAdapter(cmd)
        dsSubLoc = New DataSet()
        adpSubLocationName.Fill(dsSubLoc)
        If dsSubLoc.Tables(0).Rows.Count > 0 Then
            lblPresentLocation.Text = ""
            lblPresentLocation.Text = dsSubLoc.Tables(0).Rows(0)("SubLocationName")
            lblPresentLocation.ForeColor = Drawing.Color.DarkRed
        Else
            lblPresentLocation.Text = ""
        End If
    End Sub
End Class
