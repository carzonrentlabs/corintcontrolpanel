<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AddSubLocation.aspx.vb" Inherits="car_AddSubLocation" %>
<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    
    <title>CarzonRent :: Internal software Control panel</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="JavaScript" src="../utilityfunction.js"></script>
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
		<script type="text/javascript">
		function validation()
		{
		    var strvalues
			strvalues=('ddlCityName,txtSubLacotionName,txtSubLocationAddress')
			return checkmandatory(strvalues);
		}
		</script>
		
</head>
<body>
    <form id="form1" runat="server">
 
    <uc1:Headerctrl id="Headerctrl1" runat="server"></uc1:Headerctrl>
     
     <table align="center">
     <asp:panel id="pnlmainform" Runat="server">
     <tr><td>&nbsp;</td></tr>
      <tr><td>&nbsp;</td></tr>
      
        <tr><td>&nbsp;</td></tr>
        <tr>
            <td><b>*City Name</b></td>
            <td><b>:</b></td>
            <td><asp:DropDownList ID ="ddlCityName" runat="server"></asp:DropDownList></td>
        </tr>
        <tr><td>&nbsp;</td></tr>
        <tr>
            <td><b>*Sub Location Name</b></td>
            <td><b>:</b></td>
            <td><asp:TextBox ID ="txtSubLacotionName" runat="server"></asp:TextBox></td>
        </tr>
        <tr><td>&nbsp;</td></tr>
        <tr> <td><b>*Sub Location Address</b></td>
            <td><b>:</b></td>
            <td><asp:TextBox ID ="txtSubLocationAddress" runat="server"></asp:TextBox></td></tr>
        
        <tr><td>&nbsp;</td></tr>
        <tr>
         <td><b>*Active</b></td>
            <td><b>:</b></td>
            <td> <asp:CheckBox ID="chkActive" runat="server" CssClass="input" Checked="True"></asp:CheckBox></td>
        </tr>
        <tr></tr>
        <tr><td style="text-align:center;" colspan="3">
        <asp:Button ID="btnSubmit" runat="server" Text="Add Sublocation" OnClientClick="validation()"/>
       </td>
        </tr>
     </asp:panel>
     <asp:panel id="pnlconfirmation" Runat="server" Visible="False">
					<TR align="center">
						<TD colSpan="2">
							<asp:Label id="lblMessage" runat="server"></asp:Label></TD>
					</TR>
					<TR align="center">
						<TD colSpan="2">
							<asp:HyperLink id="HyplnkRetry" runat="server"></asp:HyperLink></TD>
					</TR>
				</asp:panel>
        </table>
  
    </form>
</body>
</html>
