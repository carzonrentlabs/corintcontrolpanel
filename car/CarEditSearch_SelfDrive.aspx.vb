Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Partial Class car_CarEditSearch_SelfDrive
    Inherits System.Web.UI.Page

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Response.Redirect("CarEditResult_SelfDrive.aspx?id=" & ddcarinfo.SelectedItem.Value)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If Not Page.IsPostBack Then
            'Dim dtrreader As SqlDataReader
            'Dim accessdata As clsutility
            'accessdata = New clsutility
            'dtrreader = accessdata.funcGetSQLDataReader("select CarID,RegnNo  from CORIntCarMaster where SelfDriveAvailableYN=1 and Active=1 order by RegnNo ")
            'ddcarinfo.DataSource = dtrreader
            'ddcarinfo.DataValueField = "CarID"
            'ddcarinfo.DataTextField = "RegnNo"
            'ddcarinfo.DataBind()
            'ddcarinfo.Items.Insert(0, New ListItem("Any", -1))
            'dtrreader.Close()
            'accessdata.Dispose()
            Try
                Dim MyConnection As SqlConnection
                MyConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("corConnectString"))
                Dim cmd As SqlCommand
                Dim adpCar As SqlDataAdapter
                Dim dsCar As DataSet
                cmd = New SqlCommand("Prc_GetSelfDriveCar", MyConnection)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@UserId", Session("loggedin_user"))
                adpCar = New SqlDataAdapter(cmd)
                dsCar = New DataSet()
                adpCar.Fill(dsCar)
                If dsCar.Tables(0).Rows.Count > 0 Then

                    ddcarinfo.DataSource = dsCar.Tables(0)
                    ddcarinfo.DataTextField = "RegnNo"
                    ddcarinfo.DataValueField = "CarID"
                    ddcarinfo.DataBind()
                    ddcarinfo.Items.Insert(0, New ListItem("-select-", -1))
                Else
                    ddcarinfo.Items.Insert(0, "-select-")
                End If
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try

        End If
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("CarEditSearch_SelfDrive.aspx")
    End Sub
End Class
