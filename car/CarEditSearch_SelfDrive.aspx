<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CarEditSearch_SelfDrive.aspx.vb"
    Inherits="car_CarEditSearch_SelfDrive" %>

<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>CarzonRent :: Internal software</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">

    <script language="JavaScript" src="../utilityfunction.js"></script>

    <script language="JavaScript" src="../JScripts/Datefunc.js"></script>

    <link href="../HertzInt.css" type="text/css" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <uc1:Headerctrl ID="Headerctrl1" runat="server"></uc1:Headerctrl>
            <table align="center">
                <tr>
                    <td colspan="2" align="center">
                        <strong><u>Edit a Car where</u></strong>
                        <br>
                        <br>
                    </td>
                </tr>
                <tr>
                    <td>
                        Car info</td>
                    <td>
                        <asp:DropDownList ID="ddcarinfo" runat="server" CssClass="input">
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="button"></asp:Button>
                        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="button"></asp:Button></td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
