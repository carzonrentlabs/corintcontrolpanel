Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.text
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Imports System.Web.UI
Partial Class car_CarEditResult_SelfDrive
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            getvalue("carname")
        End If
    End Sub
    Sub getvalue(ByVal strorderby As String)
        Dim strquery As StringBuilder
        Dim dtrreader As SqlDataReader
        Dim accessdata As clsutility
        accessdata = New clsutility
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("corConnectString"))

        Dim cmd As SqlCommand
        Dim adpCar As SqlDataAdapter
        Dim dsCar As DataSet
        cmd = New SqlCommand("Prc_GetUserAccessType", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@UserId", Session("loggedin_user"))
        adpCar = New SqlDataAdapter(cmd)
        dsCar = New DataSet()
        adpCar.Fill(dsCar)


        strquery = New StringBuilder("select CarMaster.carid, UnitMaster.unitname,CarMaster.regnno,case CarMaster.ACyn when '1' then 'Yes' else 'No' end as ac,")
        strquery = strquery.Append("CarMaster.manufactureyr,FuelMaster.fueltypeName,case CarMaster.active when '1' then 'Active' else 'Non Active' end as active,")
        strquery = strquery.Append("CarCompMaster.carcompname+' '+ModelMaster.carmodelname as carname from CORIntCarMaster CarMaster, CORIntUnitMaster UnitMaster, CORIntCarCompMaster CarCompMaster,")
        strquery = strquery.Append("CORIntCarModelMaster ModelMaster, CORIntFuelTypeMaster FuelMaster")
        strquery = strquery.Append(" where UnitMaster.unitid = CarMaster.unitid and CarCompMaster.carcompid=ModelMaster.carcompid and ")
        strquery = strquery.Append("ModelMaster.carmodelid = CarMaster.carmodelid And CarMaster.FuelTypeID = FuelMaster.FuelTypeID and CarMaster.SelfDriveAvailableYN=1 ") ' and CarMaster.Active=1")
    
        If dsCar.Tables(0).Rows(0)("AccessType") = "SU" Then
            strquery = strquery.Append("  and UnitMaster.UnitID = " & dsCar.Tables(0).Rows(0)("UnitID"))
        End If
        If dsCar.Tables(0).Rows(0)("AccessType") = "CT" Then
            strquery = strquery.Append(" and UnitMaster.UnitCityID = " & dsCar.Tables(0).Rows(0)("UnitCityId"))
        End If
        If dsCar.Tables(0).Rows(0)("AccessType") = "RN" Then
            strquery = strquery.Append("  and UnitCityMaster.Region = " & dsCar.Tables(0).Rows(0)("Region"))
        End If

        If Request.QueryString("id") <> "-1" Then
            strquery.Append(" and CarMaster.carid=" & Request.QueryString("id") & " ")
        End If
        strquery.Append(" order by " & strorderby & "")
        'Response.Write(strquery)
        'Response.End()

        dtrreader = accessdata.funcGetSQLDataReader(strquery.ToString)
        While dtrreader.Read
            Dim Temprow As New TableRow
            Temprow.HorizontalAlign = HorizontalAlign.Center


            Dim Tempcell As New TableCell
            Tempcell.Controls.Add(New LiteralControl(dtrreader("unitname") & ""))
            Temprow.Cells.Add(Tempcell)

            Dim Tempcel2 As New TableCell
            Tempcel2.Controls.Add(New LiteralControl(dtrreader("carname") & ""))
            Temprow.Cells.Add(Tempcel2)

            Dim Tempcel4 As New TableCell
            Tempcel4.Controls.Add(New LiteralControl(dtrreader("regnno") & ""))
            Temprow.Cells.Add(Tempcel4)

            Dim Tempcel5 As New TableCell
            Tempcel5.Controls.Add(New LiteralControl(dtrreader("ac") & ""))
            Temprow.Cells.Add(Tempcel5)

            Dim Tempcel6 As New TableCell
            Tempcel6.Controls.Add(New LiteralControl(dtrreader("manufactureyr") & ""))
            Temprow.Cells.Add(Tempcel6)

            Dim Tempcel7 As New TableCell
            Tempcel7.Controls.Add(New LiteralControl(dtrreader("FuelTypeName") & ""))
            Temprow.Cells.Add(Tempcel7)

            Dim Tempcel8 As New TableCell
            Tempcel8.Controls.Add(New LiteralControl(dtrreader("active") & ""))
            Temprow.Cells.Add(Tempcel8)


            Dim Tempcel9 As New TableCell
            Tempcel9.Controls.Add(New LiteralControl("<a href=CarEditForm_SelfDrive.aspx?ID=" & dtrreader("carid") & " >Edit</a>"))
            Temprow.Cells.Add(Tempcel9)
            tblRecDetail.Rows.Add(Temprow)

        End While
        dtrreader.Close()
        accessdata.Dispose()


    End Sub
    Sub SortGird(ByVal sender As Object, ByVal e As System.EventArgs)
        Select Case sender.id
            Case "Linkbutton1"
                getvalue("U.unitname")
            Case "Linkbutton2"
                getvalue("carname")
            Case "Linkbutton3"
                getvalue("M.regnno")
            Case "Linkbutton4"
                getvalue("ac")
            Case "Linkbutton5"
                getvalue("M.manufactureyr")
            Case "Linkbutton6"
                getvalue("M.fueltype")
            Case "Linkbutton7"
                getvalue("active")

        End Select

    End Sub
End Class
