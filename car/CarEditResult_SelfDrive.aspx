<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CarEditResult_SelfDrive.aspx.vb"
    Inherits="car_CarEditResult_SelfDrive" %>

<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>CarzonRent :: Internal software</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">

    <script language="JavaScript" src="../utilityfunction.js"></script>

    <script language="JavaScript" src="../JScripts/Datefunc.js"></script>

    <link href="../HertzInt.css" type="text/css" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <uc1:Headerctrl ID="Headerctrl1" runat="server"></uc1:Headerctrl>
            <table align="center">
                <tr>
                    <td align="center">
                        <strong><u>Edit a Car where</u></strong>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <asp:Table ID="tblRecDetail" HorizontalAlign="Center" runat="server" BorderColor="#cccc99"
                            BorderStyle="Solid" GridLines="both" CellSpacing="0" CellPadding="0">
                            <asp:TableRow HorizontalAlign="Center">
                                <asp:TableHeaderCell>
                                    &nbsp;&nbsp;
                                    <asp:LinkButton runat="server" Text="Unit Name" OnClick="SortGird" ID="Linkbutton1"></asp:LinkButton>&nbsp;&nbsp;
                                </asp:TableHeaderCell>
                                <asp:TableHeaderCell>
                                    &nbsp;&nbsp;
                                    <asp:LinkButton runat="server" Text="Company - Model" OnClick="SortGird" ID="Linkbutton2"></asp:LinkButton>&nbsp;&nbsp;
                                </asp:TableHeaderCell>
                                <asp:TableHeaderCell>
                                    &nbsp;&nbsp;
                                    <asp:LinkButton runat="server" Text="Registration #" OnClick="SortGird" ID="Linkbutton3"></asp:LinkButton>&nbsp;&nbsp;
                                </asp:TableHeaderCell>
                                <asp:TableHeaderCell>
                                    &nbsp;&nbsp;
                                    <asp:LinkButton runat="server" Text="AC?" OnClick="SortGird" ID="Linkbutton4"></asp:LinkButton>&nbsp;&nbsp;
                                </asp:TableHeaderCell>
                                <asp:TableHeaderCell>
                                    &nbsp;&nbsp;
                                    <asp:LinkButton runat="server" Text="Year of Manufacture" OnClick="SortGird" ID="Linkbutton5"></asp:LinkButton>&nbsp;&nbsp;
                                </asp:TableHeaderCell>
                                <asp:TableHeaderCell>
                                    &nbsp;&nbsp;
                                    <asp:LinkButton runat="server" Text="Fuel Type" OnClick="SortGird" ID="Linkbutton6"></asp:LinkButton>&nbsp;&nbsp;
                                </asp:TableHeaderCell>
                                <asp:TableHeaderCell>
                                    &nbsp;&nbsp;
                                    <asp:LinkButton runat="server" Text="Active" OnClick="SortGird" ID="Linkbutton7"></asp:LinkButton>&nbsp;&nbsp;
                                </asp:TableHeaderCell>
                                <asp:TableHeaderCell>
									&nbsp;&nbsp;
                                </asp:TableHeaderCell>
                            </asp:TableRow>
                        </asp:Table>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
