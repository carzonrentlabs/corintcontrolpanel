Imports commonutility
Imports System.Data
Imports System.text
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class VendorShareEdit
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object
    Protected WithEvents lblCompID As System.Web.UI.WebControls.Label
    Protected WithEvents ddlVendID As System.Web.UI.WebControls.Label
    Protected WithEvents lblService As System.Web.UI.WebControls.Label

    Protected WithEvents txtEffecDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblCityID As System.Web.UI.WebControls.Label
    Protected WithEvents ddlCarCatID As System.Web.UI.WebControls.Label
    Protected WithEvents TxtAmount As System.Web.UI.WebControls.TextBox
    Protected WithEvents chk_Active As System.Web.UI.WebControls.CheckBox
    Protected WithEvents btnsubmit As System.Web.UI.WebControls.Button
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents chkptg As System.Web.UI.WebControls.CheckBox
    Protected WithEvents Txtpercent As System.Web.UI.WebControls.TextBox

    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel



    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        ' btnsubmit.Attributes("onClick") = "return validation();"
        If Trim("" & Request.QueryString("ID")) <> "" Then
            Dim intID As Integer
            intID = Request.QueryString("ID")
            If Not IsPostBack() Then
                getvalue(intID)
            End If
        Else
            Response.Write("Bad request.")
            Response.End()
        End If
    End Sub
    Sub getvalue(ByVal intID As Integer)
        Dim strquery As String
        Dim StrUrl As StringBuilder
        Dim str As StringBuilder
        Dim dtrreader As SqlDataReader
        Dim accessdata As clsutility
        Dim createby As String = ""
        Dim ModifiedBy As String = ""

        accessdata = New clsutility

        strquery = "Exec Proc_CP_ClientVendorRun_1 " & intID

        dtrreader = accessdata.funcGetSQLDataReader(strquery.ToString)
        'If dtrreader.HasRows Then
        While dtrreader.Read
            lblCompID.Text = dtrreader("ClientCoName")
            lblService.Text = dtrreader("Service")

            If Trim("" & dtrreader("CarVendorName")) <> "" Then
                ddlVendID.Text = dtrreader("CarVendorName")
            Else
                ddlVendID.Text = "ALL"
            End If

            TxtAmount.Text = dtrreader("Revenue_Sharing")

            If Trim("" & dtrreader("CarCatName")) <> "" Then
                ddlCarCatID.text = dtrreader("CarCatName")
            Else
                ddlCarCatID.text = "ALL"
            End If


            If IsDBNull(dtrreader("CityName")) Then
                lblCityID.Text = "ALL"
            Else
                lblCityID.Text = dtrreader("CityName")
            End If
            txtEffecDate.Text = dtrreader("Effective_date")

            If dtrreader("Active") Then
                chk_Active.Checked = True
            Else
                chk_Active.Checked = False
            End If

            If dtrreader("Active") Then
                chkptg.Checked = True
            Else
                chkptg.Checked = False
            End If
            Txtpercent.Text = dtrreader("Effective_date")

        End While
        'End If
    End Sub

    Private Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        Dim Flag As Integer
        Dim intAmount As Integer

        'intAmount = Txt_Amt.Text
        'Response.Write("Txt_Amount==" & intAmount)
        If (get_YNvalue(chkptg) = 1) Then
            If (TxtAmount.Text = "") Then
                TxtAmount.Text = 0
            End If
        End If

        cmd = New SqlCommand("Proc_CP_EditVendorSharing", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@ID", Request.QueryString("ID"))
        cmd.Parameters.Add("@RevenueSharePC", TxtAmount.Text)
        cmd.Parameters.Add("@TargetDate", txtEffecDate.Text)
        cmd.Parameters.Add("@Active", get_YNvalue(chk_Active))
        cmd.Parameters.Add("@CreatedBy", Session("loggedin_user"))
        cmd.Parameters.Add("@PerCentIncreate", get_YNvalue(chkptg))
        cmd.Parameters.Add("@AdditionalRevenue", Txtpercent.Text)

        'response.write(Request.QueryString("ID"))
        'response.write("<br>")
        'response.write(TxtAmount.Text)
        'response.write("<br>")
        'response.write(txtEffecDate.Text)
        'response.write("<br>")
        'response.write(get_YNvalue(chk_Active))
        'response.write("<br>")
        'response.write(Session("loggedin_user"))
        'response.write("<br>")
        'response.write(get_YNvalue(chkptg))
        'response.write("<br>")
        'response.write(Txtpercent.Text)
        'response.write("<br>")
        'response.end()

        MyConnection.Open()
        cmd.ExecuteNonQuery()
        'Flag = cmd.ExecuteNonQuery()

        MyConnection.Close()
        lblErrorMsg.Visible = True
        'Response.Write("Flag=" & Flag)

        'If Trim("" & Flag) = "0" Then

        'pnlmainform.Visible = False
        'pnlconfirmation.Visible = True

        'lblErrorMsg.Text = "Unable to edit Vendor Share Package!<br> The Vendor Share Package ID is <b>" & Request.QueryString("id") & "</b>"
        'hyplnkretry.Text = "Edit another Vendor Share Package"
        'hyplnkretry.NavigateUrl = "VendorShareSearch.aspx"
        'Else

        pnlmainform.Visible = False
        pnlconfirmation.Visible = True

        lblErrorMsg.Text = "You have edited the Vendor Share Package <br> The Vendor Share Package ID is <b>" & Request.QueryString("id") & "</b>"
        hyplnkretry.Text = "Edit another Vendor Share Package"
        hyplnkretry.NavigateUrl = "VendorShareSearch.aspx"
        'End If
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function
End Class
