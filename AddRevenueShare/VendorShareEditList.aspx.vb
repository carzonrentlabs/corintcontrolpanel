Imports commonutility
Imports System.Data
Imports System.text
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System

Public Class VendorShareEditList
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tbl1 As System.Web.UI.WebControls.Table
    Protected WithEvents MainDiv As System.Web.UI.HtmlControls.HtmlGenericControl

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not Page.IsPostBack Then
            getvalue()
        End If
    End Sub
    Sub getvalue()
        Dim strquery As StringBuilder
        'Dim StrUrl As StringBuilder
        Dim str As StringBuilder
        Dim dtrreader As SqlDataReader
        Dim accessdata As clsutility
        Dim createby As String = ""
        Dim ModifiedBy As String = ""
         

        accessdata = New clsutility
        
        strquery = New StringBuilder("Exec Proc_CP_ClientVendorRun "  & Request.QueryString("CompID"))
        'strquery = New StringBuilder("Exec Proc_CP_ClientVendorRun " & Request.QueryString("ddlCompID"))

        dtrreader = accessdata.funcGetSQLDataReader(strquery.ToString)
        If dtrreader.HasRows Then
            str = New StringBuilder("<table ID=""tbl1"" HorizontalAlign=""Center"" BorderColor=""#cccc99"" BorderStyle=""Solid"" GridLines=""both"" border=""1"" CellSpacing=""0"" CellPadding=""0"">")
            str.Append("<tr>")
            str.Append("<td Class=""btnstyle"">&nbsp;&nbsp;<b>Company</b>&nbsp;&nbsp;</td>")
            str.Append("<td Class=""btnstyle"">&nbsp;&nbsp;<b>City</b>&nbsp;&nbsp;</td>")
            str.Append("<td Class=""btnstyle"">&nbsp;&nbsp;<b>Service</b>&nbsp;&nbsp;</td>")
            str.Append("<td Class=""btnstyle"">&nbsp;&nbsp;<b>Vendor</b>&nbsp;&nbsp;</td>")
            str.Append("<td Class=""btnstyle"">&nbsp;&nbsp;<b>Revenue Sharing</b>&nbsp;&nbsp;</td>")
            str.Append("<td Class=""btnstyle"">&nbsp;&nbsp;<b>Additional Revenue</b>&nbsp;&nbsp;</td>")
            str.Append("<td Class=""btnstyle"">&nbsp;&nbsp;<b>Car Category</b>&nbsp;&nbsp;</td>")
            str.Append("<td Class=""btnstyle"">&nbsp;&nbsp;<b>Created By</b>&nbsp;&nbsp;</td>")
            str.Append("<td Class=""btnstyle"">&nbsp;&nbsp;<b>Modified By</b>&nbsp;&nbsp;</td>")
            str.Append("<td Class=""btnstyle"">&nbsp;&nbsp;<b>Effective Date</b>&nbsp;&nbsp;</td>")
            str.Append("<td Class=""btnstyle"">&nbsp;&nbsp;<b>Modify Date</b>&nbsp;&nbsp;</td>")
            str.Append("<td Class=""btnstyle"">&nbsp;&nbsp;<b>Active</b>&nbsp;&nbsp;</td>")
            str.Append("<td Class=""btnstyle"">&nbsp;&nbsp;<b>Edit</b>&nbsp;&nbsp;</td>")
            str.Append("</tr>")
            While dtrreader.Read

                str.Append("<tr>")
                'str.Append("<td>&nbsp;&nbsp;" & dtrreader("ClientCoName") & "&nbsp;&nbsp;</td>")
                str.Append("<td>"&dtrreader("ClientCoName")&"</td>")
                'str.Append("<td>&nbsp;&nbsp;" & dtrreader("CityName") & "&nbsp;&nbsp;</td>")
                str.Append("<td>" & dtrreader("CityName") & "</td>")
                'str.Append("<td>&nbsp;&nbsp;" & dtrreader("service") & " &nbsp;&nbsp;</td>")
                str.Append("<td>" & dtrreader("service") & "</td>")
                'str.Append("<td>&nbsp;&nbsp;" & dtrreader("CarVendorName") & " &nbsp;&nbsp;</td>")

                If Trim("" & dtrreader("CarVendorName")) = "" Then
                    str.Append("<td>&nbsp;&nbsp;Any&nbsp;&nbsp;</td>")
                Else
                    str.Append("<td>" & dtrreader("CarVendorName") & "</td>")
                End If

                'str.Append("<td>&nbsp;&nbsp;" & dtrreader("Revenue_Sharing") & "&nbsp;&nbsp;</td>")
                str.Append("<td>" & dtrreader("Revenue_Sharing") & "</td>")
                str.Append("<td>" & dtrreader("Revenue_Additional") & "&nbsp;</td>")
                If Trim("" & dtrreader("CarCatName")) <> "" Then
                    'str.Append("<td>&nbsp;&nbsp;" & dtrreader("CarCatName") & "&nbsp;&nbsp;</td>")
                    str.Append("<td>" & dtrreader("CarCatName") & "</td>")
                Else
                    str.Append("<td>&nbsp;&nbsp;Any&nbsp;&nbsp;</td>")
                End If


                If Trim("" & dtrreader("Created_By")) <> "" Then
                    createby = getuserName(Convert.ToString(dtrreader("Created_By")))
                Else
                    createby = "-"
                End If
                'str.Append("<td>&nbsp;&nbsp;" & createby & "&nbsp;&nbsp;</td>")
                str.Append("<td>" & createby & "</td>")

                If IsDBNull(dtrreader("Modified_By")) Then
                    ModifiedBy = "-"
                Else
                    ModifiedBy = getuserName(Convert.ToString(dtrreader("Modified_By")))
                End If
                'str.Append("<td>&nbsp;&nbsp;" & ModifiedBy & "&nbsp;&nbsp;</td>")
                str.Append("<td>" & ModifiedBy & "</td>")

                'str.Append("<td>&nbsp;&nbsp;" & dtrreader("Create_Date") & "&nbsp;&nbsp;</td>")
                str.Append("<td>" & dtrreader("Effective_date") & "</td>")
                'str.Append("<td>&nbsp;&nbsp;" & dtrreader("Modified_Date") & "&nbsp;&nbsp;</td>")

                'getuserName(Convert.ToString(dtrreader("Modified_Date")))
                If IsDBNull(dtrreader("Modified_By")) Then
                    str.Append("<td>-</td>")
                Else
                    str.Append("<td>" & dtrreader("Modified_Date") & "</td>")
                End If
                'str.Append("<td>&nbsp;&nbsp;" & dtrreader("Active") & "&nbsp;&nbsp;</td>")
                If dtrreader("Active") Then
                    'str.Append("<td>&nbsp;&nbsp;Active&nbsp;&nbsp;</td>")
                    str.Append("<td>Active</td>")
                Else
                    'str.Append("<td>&nbsp;&nbsp;Not Active&nbsp;&nbsp;</td>")
                    str.Append("<td>Not Active</td>")
                End If

                'str.Append("<td>&nbsp;&nbsp;" & createby & "&nbsp;&nbsp;</td>")
                'str.Append("<td>&nbsp;&nbsp;" & ModifiedBy & "&nbsp;&nbsp;</td>")

                'str.Append("<td>&nbsp;&nbsp;<a href=PromotionalSchemsEdit.aspx?ID=" & dtrreader("PromotionID") & " >Edit</a>&nbsp;&nbsp;</td>")
                'Response.Write(StrUrl)
                str.Append("<td>&nbsp;&nbsp;<a href=VendorShareEdit.aspx?ID=" & dtrreader("ID") & " >Edit</a>&nbsp;&nbsp;</td>")
                str.Append("</tr>")

            End While
            str.Append("</table>")
            MainDiv.InnerHtml = str.ToString
        Else
            str = New StringBuilder("<table ID=""tblCDP1"" HorizontalAlign=""Center"" BorderColor=""#cccc99"" BorderStyle=""Solid"" GridLines=""both"" border=""1"" CellSpacing=""0"" CellPadding=""0"">")
            str.Append("<tr>")
            str.Append("<td Class=""btnstyle"">&nbsp;&nbsp;<b>No record exists.</b>&nbsp;&nbsp;</td>")
            str.Append("</tr>")
            str.Append("</table>")
            MainDiv.InnerHtml = str.ToString
        End If
        dtrreader.Close()
        accessdata.Dispose()
    End Sub
    Function getuserName(ByVal userid As Integer) As String
        Dim strquery As StringBuilder
        Dim oConnection As SqlConnection
        Dim strConnection As String = System.Configuration.ConfigurationSettings.AppSettings("corConnectString")

        oConnection = New SqlConnection(strConnection)
        'strquery = New StringBuilder("select top 1 Fname + ' ' + Mname + ' ' + lname as UName from CORIntSysUsersMaster where sysuserid=" & userid)
        strquery = New StringBuilder("select top 1 Fname + ' ' + ISNull(Mname,'') + ' ' + IsNull(lname,'') as UName from CORIntSysUsersMaster where sysuserid=" & userid)

        Dim Command = New SqlCommand(strquery.ToString, oConnection)
        oConnection.Open()
        Dim strreturnvalue As String = ""
        strreturnvalue = Convert.ToString(Command.ExecuteScalar())
        If oConnection.State = ConnectionState.Open Then
            oConnection.Close()
        End If
        Return strreturnvalue
    End Function
End Class
