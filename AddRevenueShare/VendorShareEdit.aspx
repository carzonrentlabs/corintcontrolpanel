<%@ Page Language="vb" AutoEventWireup="false" Codebehind="VendorShareEdit.aspx.vb" Inherits="VendorShareEdit" Src="VendorShareEdit.aspx.vb" %>
<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CarzonRent :: Internal software Control panel</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="JavaScript" src="../utilityfunction.js"></script>
		<script language="JavaScript" src="../JScripts/Datefunc.js"></script>
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
		<script language="javascript">
		function validation()
		{
			if(document.forms[0].chkptg.checked==true)
			{
				if(document.forms[0].Txtpercent.value="")
				{
					alert("Please Enter Percentage Increace.");
					return false;
				}
			}

			if(isNaN(document.forms[0].Txt_Amt.value))
				{
					alert("Rate should be numeric only.");
					return false;	
				}
			else
			{
				return true;	
			}
		}
		function percentage()
		{
			if(document.forms[0].chkptg.checked==true)
			{
				document.forms[0].TxtAmount.disabled=true;
				document.forms[0].TxtAmount.value=0;				
				document.forms[0].Txtpercent.value="";
				document.getElementById('show').style.display="block";
			}
			else
			{
				//document.forms[0].TxtAmount.value="";
				document.forms[0].TxtAmount.disabled=false;
				document.forms[0].Txtpercent.value=0;
				document.getElementById('show').style.display="none";
			}	
		}
		
	    function dateReg(obj)
        {
            if(obj.value!="")
            {
                // alert(obj.value);
                var reg = /((0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[0-1])-(201[0-9]))/
                if(reg.test(obj.value))
                {
                    //alert('valid');
                }
                else
                {
                    alert('notvalid');
                    obj.value="";
                }
            }
        }			
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout" onLoad="percentage();">
		<blockquote>
		  <form id="Form1" method="post" runat="server">
		    <uc1:Headerctrl id="Headerctrl1" runat="server"></uc1:Headerctrl>
		    <TABLE id="Table1" align="center">
		      <asp:panel id="pnlmainform" Runat="server">
		        <TBODY>
		          <TR>
		            <TD align="center" colSpan="2"><STRONG><U>Edit a Vendor Share Package</U></STRONG>		              </TD>
				    </TR>
		          <TR>
		            <TD align="center" colSpan="2">&nbsp;&nbsp;</TD>
				    </TR>
		          <TR>
		            <TD>Company</TD>
					  <TD><asp:Label ID="lblCompID" Runat="server"></asp:Label></td>
				    </TR>
		          <TR>
		            <TD>Vendor Name</TD>
					  <TD><asp:Label ID="ddlVendID" Runat="server"></asp:Label></td>
				    </TR>
		          <TR>
		            <TD>Vendor Share</TD>
					  <TD><asp:textbox id="TxtAmount" runat="server" value=""></asp:textbox>				      </TD>
				    </TR>
		          <TR>
		            <TD>Car Category</TD>
					  <TD><asp:Label ID="ddlCarCatID" Runat="server"></asp:Label></TD>
				    </TR>
		          <TR>
		            <TD>City</TD>
					  <TD><asp:Label ID="lblCityID" Runat="server"></asp:Label></TD>
				    </TR>
					<TR>
							<TD>Service</TD>
							<TD><asp:Label ID="lblService" Runat="server"></asp:Label></td>
					</TR>
		          <TR>
		            <TD>Effective Date</TD>
						  <TD>
			<asp:textbox id="txtEffecDate" runat="server" CssClass="input" MaxLength="12" onblur = "dateReg(this);" size="12">
			</asp:textbox><A onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;"
								href="javascript:show_calendar('Form1.txtEffecDate');"><IMG height="21" src="../images/show-calendar.gif" width="24" border="0"></A>
									</TD>
		          <TR>
		            <TD>Active</TD>
					  <TD>
					      <asp:checkbox id="chk_active" runat="server" Checked="True"></asp:checkbox></TD>
				    </TR>
		          
				  						<TR>
							<TD>%age increase YN </TD>
							<TD>
								<asp:checkbox id="chkptg" onclick=percentage(); runat="server" CssClass="input" Checked="False"></asp:checkbox></TD>
						</TR>
						
						<TR id="show">
							<TD>* Percentage#
							</TD>
							<TD>
							<asp:textbox id="Txtpercent" Text="0" runat="server" CssClass="input" MaxLength="50"></asp:textbox></TD>
						</TR>
		          
		          <TR>
		            <TD align="center" colSpan="2">
		              <asp:button id="btnsubmit" runat="server"  Text="Submit"></asp:button>&nbsp;&nbsp;
		              <asp:button id="btnreset" runat="server"  Text="Reset" CausesValidation="False"></asp:button></TD>
				    </TR>
	            </asp:panel>
		      <asp:panel id="pnlconfirmation" Runat="server" Visible="False">
		        <TR align="center">
		          <TD colSpan="2">
		            <asp:Label id="lblErrorMsg" runat="server"></asp:Label></TD>
				  </TR>
		        <TR align="center">
		          <TD colSpan="2">
		            <asp:HyperLink id="hyplnkretry" runat="server"></asp:HyperLink></TD>
				  </TR>
		        
		        </asp:panel></TBODY></TABLE>
		</form>
    </blockquote>
	</body>
</HTML>
