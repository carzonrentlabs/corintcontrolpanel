Imports System
Imports System.Data.SqlClient
Imports System.Text
Imports commonutility
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Web.UI
Imports System.Data

Public Class CP_Index
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tblRecDetail As System.Web.UI.WebControls.Table
    Protected WithEvents lblmessage As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        'checking whether the user is coming first time to this page or coming via clicking on the header menu item
        If Not Request.QueryString("id") Is Nothing Then
            Dim dtrreader As SqlDataReader
            Dim strquery As StringBuilder
            Dim dtrFetchlink As clsutility
            dtrFetchlink = New clsutility

            strquery = New StringBuilder("select M.functionname,M.link from CORIntModuleFunctionMaster M ")


            'checking whether the logged in user is admin or not if 1 then it is admin else a user
            If Not Session("loggedin_user") = "1" Then
                strquery.Append(",CORIntSysUsersModAccessMaster S ")
            End If
            strquery.Append("where ")
            'on the basis of the logged in user it wil check fire the query
            If Not Session("loggedin_user") = "1" Then
                strquery.Append("S.sysuserid=" & Session("loggedin_user") & " ")
                strquery.Append(" and M.functionid=S.modfuncid and M.moduleid=" & Request.QueryString("id") & " and M.active=1 ")
            Else
                strquery.Append("M.moduleid=" & Request.QueryString("id") & " and M.active=1 ")
            End If

            '   Response.Write(strquery.ToString())
            dtrreader = dtrFetchlink.funcGetSQLDataReader(strquery.ToString())
            Dim intcounter As Int32 = 0
            While dtrreader.Read
                intcounter = 1
                Dim Temprow As New TableRow
                'assigning the values to the asp tables columns
                Dim Tempcell As New TableCell
                Tempcell.Controls.Add(New LiteralControl("<a href=" & dtrreader("link") & " >" & dtrreader("functionname") & "</a>"))
                Temprow.Cells.Add(Tempcell)
                'adding the Tables rows to the table
                tblRecDetail.Rows.Add(Temprow)
            End While
            'if logged in user doesn't contain any nrights then displaying the error message
            If Not (intcounter > 0) Then
                lblmessage.Visible = True
                lblmessage.Text = "Sorry, you don't have the right to perform any action in this module"
            End If

        End If


    End Sub


    'Protected Sub SaveLoginLogs()
    '    Dim SqlConnection As SqlConnection
    '    Try

    '        SqlConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
    '        Dim cmd As New SqlCommand()
    '        cmd.CommandType = CommandType.Text
    '        cmd.CommandText = "insert into CorIntLogInDetail (username, logintime, ipaddress) values(@username, getdate(), @ipaddress)"
    '        cmd.Connection = SqlConnection
    '        cmd.Parameters.Add("@Type", SqlDbType.VarChar).Value = ddlPackageType_Footer
    '        cmd.Parameters.Add("@TotalDuration", SqlDbType.Int).Value = Convert.ToInt32(txtTotalDuration_Footer)

    '        cmd.Connection.Open()
    '        Dim status As Integer = cmd.ExecuteNonQuery()

    '    Catch ex As Exception

    '    End Try

    'End Sub

End Class
