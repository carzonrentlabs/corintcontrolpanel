<%@ Page Language="vb" AutoEventWireup="false" Inherits="PreAuthEditForm" src="PreAuthEditForm.aspx.vb" %>
<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CarzonRent :: Internal software</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="JavaScript" src="../utilityfunction.js"></script>
		<script language="JavaScript" src="../JScripts/Datefunc.js"></script>
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
		<script language="javascript">
		function firstpage()
		{
				var strvalues
				strvalues = ('txtfname,txtlname,ddlcompany')
				return checkmandatory(strvalues);
			}
		 
		
		</script>
	</HEAD>
	<body onLoad="showRows();">
		<form id="Form1" method="post" runat="server">
			<uc1:headerctrl id="Headerctrl1" runat="server"></uc1:headerctrl>
			<table align="center">
				<asp:panel id="pnlmainform" Runat="server">
					<td width="176"><TBODY>
						<TR>
							<TD align="center" colSpan="2"><B><U>Edit the PreAuth for Company Individual</U></B>
								<BR>
								<BR>
							</TD>
						</TR>
						<TR>
							<TD align="center" colSpan="2">
								<asp:Label id="lblErrorMsg" runat="server" cssclass="subRedHead" visible="false"></asp:Label></TD>
						</TR>
						<TR>
							<TD>* First Name</TD>
							<TD width="106">
						  <asp:textbox id="txtfname"  Enabled="false" runat="server" MaxLength="50" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>Middle Namel</TD>
							<TD>
								<asp:textbox id="txtmname"  Enabled="false" runat="server" MaxLength="50" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>* Last Name</TD>
							<TD>
								<asp:textbox id="txtlname"  Enabled="false" runat="server" MaxLength="50" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>* Client Company</TD>
							<TD>
								<asp:DropDownList id="ddlcompany" runat="server" cssclass="input" AutoPostBack="True" Enabled="false"></asp:DropDownList></TD>
						</TR>
						
						<TR>
							<TD>Active</TD>
							<TD>
								<asp:checkbox id="chkActive" runat="server"  Enabled="false" CssClass="input" Checked="True"></asp:checkbox></TD>
								
						</TR> 
				<TR>
							<TD>Pre Auth Required(Yes/No)</TD>
							<TD>
								<asp:checkbox id="chkPreAuthorize" runat="server" CssClass="input" Checked="True"></asp:checkbox></TD>
						</TR> 
					
						<TR>
							<TD align="center" colSpan="2">
								<asp:button id="btnSubmit" runat="server" CssClass="input" Text="Submit"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;
								<asp:button id="btnReset1" runat="server" CssClass="input" Text="Reset"></asp:button></TD>
						</TR>
				</asp:panel>
				<asp:panel id="pnlconfirmation" Runat="server" Visible="False">
					<TR align="center">
						<TD colSpan="2">
							<asp:Label id="lblMessage" runat="server"></asp:Label></TD>
					</TR>
					<TR align="center">
						<TD colSpan="2">
							<asp:HyperLink id="hyplnkretry" runat="server"></asp:HyperLink></TD>
					</TR>
				</asp:panel></TBODY></table>
		</form>
	</body>
</HTML>
