Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class PreAuthEditForm
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtfname As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtmname As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtlname As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlcompany As System.Web.UI.WebControls.DropDownList
    Protected WithEvents chkActive As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkPreAuthorize As System.Web.UI.WebControls.CheckBox
    Protected WithEvents btnproceed As System.Web.UI.WebControls.Button
    Protected WithEvents btnr As System.Web.UI.WebControls.Button
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    'Protected WithEvents ddldiscount As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents ddldiscountdecimal As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents ddlpayment As System.Web.UI.WebControls.DropDownList
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnReset As System.Web.UI.WebControls.Button
    Protected WithEvents pnl2ndform As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    Protected WithEvents ddCCardType As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents txtCCN As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtExpDate As System.Web.UI.WebControls.TextBox
    'Protected WithEvents Panel2 As System.Web.UI.WebControls.Panel
    'Protected WithEvents pnlCc As System.Web.UI.WebControls.Panel
    Protected WithEvents btnReset1 As System.Web.UI.WebControls.Button
    'Protected WithEvents hidCCN As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtCCNTemp As System.Web.UI.WebControls.TextBox
    Protected WithEvents Checkbox1 As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkIsVIP As System.Web.UI.WebControls.CheckBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
         InitializeComponent()
    End Sub

#End Region
    Dim CCNoNew As String
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        btnSubmit.Attributes("onClick") = "return firstpage();"
        ddlcompany.Attributes("onkeydown") = "smartOptionFinderWithSubmit(this,event);"

        If Not Page.IsPostBack Then
            populateddl()
            Dim dtrreader As SqlDataReader
            Dim accessdata As clsutility
            accessdata = New clsutility
            Dim valDisc As String
            Dim valDiscDec As String
            Dim arrDisc As Array

            dtrreader = accessdata.funcGetSQLDataReader("select fname,mname,lname,clientcoid,active,isnull(PreAuthNotRequire,'0')as PreAuthNotRequire  from CORIntClientCoIndivMaster where clientcoindivid =" & Request.QueryString("id") & " ")
            dtrreader.Read()

            txtfname.Text = dtrreader("fname") & ""
            txtmname.Text = dtrreader("mname") & ""
            txtlname.Text = dtrreader("lname") & ""
            autoselec_ddl(ddlcompany, dtrreader("clientcoid"))
            chkActive.Checked = dtrreader("active")
            chkPreAuthorize.Checked = dtrreader("PreAuthNotRequire")
            If dtrreader("PreAuthNotRequire") =0 Then
              chkPreAuthorize.checked=true
            Else
              chkPreAuthorize.checked = false
            End If

            dtrreader.Close()
            accessdata.Dispose()
        End If
    End Sub
    Sub populateddl()
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility
        ddlcompany.DataSource = objAcessdata.funcGetSQLDataReader("select ClientCoName ,ClientCoID   from CORIntClientCoMaster    where active=1 order by ClientCoName ")
        ddlcompany.DataValueField = "ClientCoID"
        ddlcompany.DataTextField = "ClientCoName"
        ddlcompany.DataBind()
        ddlcompany.Items.Insert(0, New ListItem("", ""))
        objAcessdata.Dispose()
    End Sub

    Function autoselec_ddl(ByVal ddlname As DropDownList, ByVal selectvalue As Int32)
        If Not IsDBNull(selectvalue) Then
           If Not selectvalue = 0 Then
                ddlname.Items.FindByValue(selectvalue).Selected = True
            Else
                ddlname.SelectedValue = 0
            End If
        End If
    End Function

    

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        Dim intuniqcheck As SqlParameter
        Dim intuniqvalue As Int32
        Dim intInc As Int32

        cmd = New SqlCommand("procEditindvmaster_Pre", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("@rowid", Request.QueryString("id"))
        cmd.Parameters.Add("@fname", txtfname.Text)
        cmd.Parameters.Add("@mname", txtmname.Text)
        cmd.Parameters.Add("@lname", txtlname.Text)
        cmd.Parameters.Add("@clientcoid", ddlcompany.SelectedItem.Value)
        cmd.Parameters.Add("@active", get_YNvalue(chkActive))
        cmd.Parameters.Add("@preauthnotrequire", get_YNvalue1(chkPreAuthorize))
        cmd.Parameters.Add("@modifyby", Session("loggedin_user"))
        intuniqcheck = cmd.Parameters.Add("@uniqcheckval", SqlDbType.Int)
        intuniqcheck.Direction = ParameterDirection.Output

        MyConnection.Open()
        cmd.ExecuteNonQuery()
        intuniqvalue = cmd.Parameters("@uniqcheckval").Value
        MyConnection.Close()
            lblErrorMsg.Visible = False
            pnlmainform.Visible = False
            pnlconfirmation.Visible = True

            lblMessage.Text = "You have Updated the PreAuth for Company Individual successfully"
            hyplnkretry.Text = "Edit another PreAuth for Client Company Individual"
            hyplnkretry.NavigateUrl = "PreAuthEditSearch.aspx"
       
     'End If
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
        End Function
       
        Function get_YNvalue1(ByVal chkPreAuth As CheckBox) As Int32
            Dim returnval1 As Int32
             If chkPreAuth.Checked Then
              returnval1 = 0
            Else
                returnval1 = 1
            End If
                Return returnval1
        End Function
End Class
