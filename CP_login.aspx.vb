Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration


Public Class CP_login
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtLogin As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtPwd As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnLogin As System.Web.UI.WebControls.Button
    Protected WithEvents pnlmain As System.Web.UI.WebControls.Panel
    Protected WithEvents pnlConfirmation As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
    End Sub
    'this rocedure is called when the login button is pressed by the user
    Private Sub btnLogin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLogin.Click
        'Session.Abandon()
        'ViewState.Clear()

        Dim MyConnection As SqlConnection
        Dim intLogincheckvalue As SqlParameter
        Dim logedinuseid As SqlParameter
        Dim intloginvalue As Int32
        Dim intuserid As Int32
        MyConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        'execueting the stored procedure for checking the user login validity by using the output parameter
        Dim IPAddress As String
        IPAddress = Request.ServerVariables("remote_addr").ToString

        'cmd = New SqlCommand("procLoginCheck", MyConnection)
        cmd = New SqlCommand("procLoginCheckIPAddress", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@loginid", txtLogin.Text.ToString)
        cmd.Parameters.AddWithValue("@loginpwd", txtPwd.Text.ToString)
        cmd.Parameters.AddWithValue("@IPAddress", IPAddress)
        intLogincheckvalue = cmd.Parameters.Add("@returnvalue", SqlDbType.Int)
        logedinuseid = cmd.Parameters.Add("@logedinuserid", SqlDbType.Int)
        intLogincheckvalue.Direction = ParameterDirection.Output
        logedinuseid.Direction = ParameterDirection.Output
        MyConnection.Open()

        cmd.ExecuteNonQuery()
        cmd.Dispose()
        'getting the value to know that whethre the user is vallid user for login or not
        intloginvalue = cmd.Parameters("@returnvalue").Value
        intuserid = cmd.Parameters("@logedinuserid").Value
        MyConnection.Close()
        MyConnection.Dispose()
        check_loginvalue(intloginvalue, intuserid)

    End Sub
    'sub checking the login and navigating the user to the related links
    Sub check_loginvalue(ByVal returnvalue As Int32, ByVal intuserid As Int32)
        '1=wrong login id
        '2=user not active
        '3=user is active but password is wrong
        '0=user login succesfull
        Select Case returnvalue
            Case 0
                Session("loggedin_user") = intuserid

                ' Code for redirect to change password screen
                Dim MyConnection As SqlConnection
                MyConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("corConnectString"))
                Dim cmd As SqlCommand
                Dim strQuery As String

                strQuery = " SELECT ChangePass FROM CORIntSysUsersMaster WHERE LoginID = '" & txtLogin.Text & "'"

                cmd = New SqlCommand(strQuery, MyConnection)
                MyConnection.Open()
                Dim passFlag As Int32 = CType(cmd.ExecuteScalar(), Int32)

                MyConnection.Close()
                MyConnection.Dispose()
                cmd.Dispose()
                If (passFlag = 0) Then
                    Response.Redirect("ChangePassword.aspx")
                Else
                    Response.Redirect("index/CP_Index.aspx")
                End If
            Case 1
                pnlmain.Visible = False
                pnlConfirmation.Visible = True
                lblMessage.Text = "You are not a valid user. You have not entered the correct Login ID"
                hyplnkretry.Text = "Please try again"
                hyplnkretry.NavigateUrl = "cp_login.aspx"

            Case 2
                pnlmain.Visible = False
                pnlConfirmation.Visible = True
                lblMessage.Text = "You have been deactivated as a system user"

            Case 3
                pnlmain.Visible = False
                pnlConfirmation.Visible = True
                lblMessage.Text = "You have not entered the correct password"
                hyplnkretry.Text = "Please try again"
                hyplnkretry.NavigateUrl = "cp_login.aspx"
        End Select

    End Sub
End Class
