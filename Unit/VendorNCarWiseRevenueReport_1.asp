<!-- #include file = "../Includes/SessionDB.inc" -->
<!-- #include file="../Includes/CORIntInc.inc" -->
<%
'////////////////////BSL////////////////////////////////
'Report Name = Vendor/Car-wise revenue Report
'Created By = Rajesh Vishwakrma
'Created Date = 17 Oct 07
'//////////////////////////////////////////////////////
PageName = "VendorNCarWiseRevenueReport_1.asp"
ExcelPage = "VendorNCarWiseRevenueReportExcel_1.asp"


	Server.ScriptTimeout = 90000
	
   set con=server.CreateObject("ADODB.connection") 
   con.ConnectionTimeout ="1000"
   con.CommandTimeout = "1000"
   con.Open strCon_Specs
  '//////////////////////Posted Value///////////////////////
    dim date1
	If Request.Form("FromDate")<>"" then
		date1=Request.Form("FromDate")
	else
		date1 = DATEADD("h", intTimeDifference, now)
        date1 = month(date1)&"/"&day(date1)&"/"&year(date1)
		
	end if

	dim date2
	If Request.Form("ToDate")<>"" then
		date2=Request.Form("ToDate")
	else
		date2 = DATEADD("h", intTimeDifference, now)
        date2 = month(date2)&"/"&day(date2)&"/"&year(date2)
	end if
	
	if Request.Form("CarVendorID")<>""then
		intCarVendorID = Request.Form("CarVendorID")
	end if	
	
  '	dim city
	'If Request.Form("cboCity")<>"" then
	'	city = Request.Form("cboCity")
	'else
	'	city = 0
	'end if
  '//////////////////////Getting Vendor Name////////////////////////////////// 
  sqlGetCarVendors = "select a.CarVendorID, (a.CarVendorName + ' - ' + b.CityName) as VendorName,c.RevenueSharePC as Share from CORIntCarVendorMaster as a, CORIntCityMaster as b,CorIntVendorCarMaster c where a.CarVendorCityID = b.CityID and a.CarVendorID = c.VendorCarID order by a.CarVendorName"
   set rsGetCarVendors = con.Execute (sqlGetCarVendors)

  
  sqlGetUserAccessType	= "SELECT a.AccessType, a.UnitID, b.UnitCityID, c.Region FROM CORIntSysUsersMaster as a, CORIntUnitMaster as b, CORIntUnitCityMaster as c WHERE a.UnitID = b.UnitID and b.UnitCityID = c.UnitCityID and a.SysUserID = "& session("IDMain")
Set rsGetUserAccessType = con.Execute (sqlGetUserAccessType)


sqlGetCity1	= "SELECT distinct a.CityName FROM CORIntCityMaster as a, CORIntUnitCityMaster as b, CORIntUnitMaster as c WHERE a.nearestUnitCityId=b.UnitCityId and a.Active = 1 and a.CityName <> '' and c.UnitCityID = b.UnitCityID "
If Not rsGetUserAccessType.EOF Then
   If rsGetUserAccessType("AccessType") = "SU" Then
	  sqlGetCity1		= sqlGetCity1 & " and c.UnitID = "& rsGetUserAccessType("UnitID")
   ElseIf rsGetUserAccessType("AccessType") = "CT" Then
	  sqlGetCity1		= sqlGetCity1 & " and c.UnitCityID = "& rsGetUserAccessType("UnitCityID")
   ElseIf rsGetUserAccessType("AccessType") = "RN" Then
	  sqlGetCity1		= sqlGetCity1 
   End If
End If

'' Comment while City Not Used  Set rsGetCities			= con.Execute (sqlGetCity1)
	


	'///////////////////// Creating Company and thier user drop down  01102007 //////////////////////////
'	intClientCoID = request.form("ClientCoID")
'		if intClientCoID = "" then
'			intClientCoID = 0
'		end if
		
	'if search = "" then 
'	sqlGetCompanies = "SELECT a.ClientCoID, a.ClientCoName, b.CityName from CORIntClientCoMaster as a, CORIntCityMaster as b where a.ClientCoCityID = b.CityID and a.Active = 1 and b.CityName IN( "& sqlGetCity1 &" ) order by a.ClientCoName"
'else
'	sqlGetCompanies = "SELECT a.ClientCoID, a.ClientCoName, b.CityName from CORIntClientCoMaster as a, CORIntCityMaster as b where a.ClientCoCityID = b.CityID and a.Active = 1 and b.CityName IN("& sqlGetCity1 &") a.ClientCoName like '"&search&"%' order by a.ClientCoName"
'end if

'set rsGetCompanies	= con.Execute (sqlGetCompanies)
	
	

'////////////////////////////////////Main Query///////////////////////////////////////////////////////////////
  'sqlBcgMis = "SELECT * FROM vVendorNCarWiseRevenue where 1=1"
  sqlBcgMis = "SELECT * FROM vVendorNCarWiseRevenue_temp_1 where 1=1"
  
'		if date1 <> date2 then
'			sqlBcgMis = sqlBcgMis & " And PickUpdate between '"& date1 &"' and '"& date2 &"' "
'		else
'			sqlBcgMis = sqlBcgMis & " And PickUpdate = '"& date2 &"' "
'		end if

'		if date1 <> date2 then
			'sqlBcgMis = sqlBcgMis & " And AccountingDate between '"& date1 &" 00:00:00' and '"& date2 &"' 23:59:59"
			sqlBcgMis = sqlBcgMis & " And AccountingDate between '"& date1 &"' and '"& date2 &"'"
'		else
'			sqlBcgMis = sqlBcgMis & " And AccountingDate = '"& date2 &"' "
'		end if
		
		if intCarVendorID <> "" then
			sqlBcgMis		= sqlBcgMis & "And carvendorid = "& intCarVendorID
		end if
		'where carvendorid = 464 ORDER BY Vendor_Name
		sqlBcgMis		= sqlBcgMis & " ORDER BY Vendor_Name"
'///////////////////////////////////////////////////////////////////////////////////////////////////////////	





'Response.Write "<br>"&sqlBcgMis
'Response.end

   'set rsPendBookingsYN = con.Execute (sqlPendBookingsYN)
   'hd=trim(

 if Request.Form("hd") = 1 then  

			Dim iPageCount, iRowCtr, iReccount, perPageRecords, rsBcgMis

			Set rsBcgMis =Server.CreateObject("ADODB.Recordset")

		iPageCount = 1
		iRowCtr = 1
		iReccount = 0
		rsBcgMis.cursorlocation = 3
		
		rsBcgMis.PageSize = 100
		
		rsBcgMis.Open sqlBcgMis , con, 1, 3
		If Not rsBcgMis.EOF Then
		   iReccount = rsBcgMis.RecordCount
		   iPageCount = rsBcgMis.PageCount
		   iPdMainPage = Request("page")
		   If iPdMainPage = "" Then iPdMainPage = "1"
		   rsBcgMis.AbsolutePage = iPdMainPage
		End If
else

end if		
   
%>
<html>
<head>
<title>CarzonRent :: Internal software</title>
<link rel="stylesheet" href="../HertzInt.css" type="text/css">
<script language="JavaScript" src="../Jscripts/datefunc.js"></script>
<script language="JavaScript" src="../Jscripts/cfunc.js"></script>
<script language="JavaScript">
function exportToExcel()
{

   if (document.form1.TN_Excel_Option.value == "All")
   {
		if (document.form1.TN_Total_Record.value > 60000)
		{
			  alert("Sorry you can not export to Excel\n Duo to record exceeds maximum no. rows allowed in Excel.");
			  return false;
		}
		else
		{
			document.form1.action = "<%=ExcelPage%>?page=<%=Request("page")%>";
			document.form1.submit();
			return true;
		}
	}
	document.form1.action = "<%=ExcelPage%>?page=<%=Request("page")%>";
	document.form1.submit();
	
}

function fnNext(frmaction)
{  
	document.form1.hd.value = 1;
	document.form1.action  = frmaction;
 	document.form1.submit();
}
function fnPrev(frmaction)
{
	document.form1.hd.value = 1;
	document.form1.action = frmaction;
	document.form1.submit();
}

function GoToSubmit()
{
	//document.all.item("hdVal").value = 1 ;
	document.form1.hd.value = 1;
	document.form1.action = "<%=PageName%>";
	document.form1.submit();
	
}
</script>
</head>
<body>
 <form name="form1" method="post">
  <table border="0" cellpadding="0" cellspacing="0" align="center" width=100%>
    <!-- #include file = "../Includes/MainHeader.inc" -->
    <tr>
      <td colspan="28" align="center" height=5 ><br>
		<table align="center" border="0" cellspacing="0" cellpadding="0"  width=20% >
 
		<tr>
		     
			<!--<td align="center" nowrap=true>Company name&nbsp;</td><td >
			<!--<select  name="ClientCoID"  onChange="return Change_option()">-->
			<!--&nbsp;&nbsp;&nbsp;<select  name="ClientCoID" >
				<option value="">All</option>
				<%'
				   'Do while not rsGetCompanies.EOF
				   'if (trim((rsGetCompanies("ClientCoID"))) = trim((intClientCoID))) then 
				%>
					<option  value='<%'=rsGetCompanies("ClientCoID")%>' selected><%'=rsGetCompanies("ClientCoName") & " - " & rsGetCompanies("CityName")%></option>
				<%
					'else
				%>
					<option  value=<%'=rsGetCompanies("ClientCoID")%>> <%'=rsGetCompanies("ClientCoName") & " - " & rsGetCompanies("CityName")%></option>	
				<%	'end if
					'rsGetCompanies.MoveNext
				   'Loop
				%>
			</select>
			</td>
			
			</td>-->
			<td  align="right" nowrap=true>Vendor name
			</td>
			<td  align="left">
				<select name="CarVendorID">
				<option value="">ALL</option>
				<%
				   while not rsGetCarVendors.EOF
				   if (trim(Request.Form("CarVendorID"))= trim(rsGetCarVendors("CarVendorID"))) then
				%>
					<option value=<%=rsGetCarVendors("CarVendorID")%> selected><%=rsGetCarVendors("VendorName")&"  ("&rsGetCarVendors("Share")&"%)"%></option>
				<%   
				   else
				%>
				            <option value='<%=rsGetCarVendors("CarVendorID")%>'><%=rsGetCarVendors("VendorName")&"  ("&rsGetCarVendors("Share")&"%)"%></option>
				<%
					end if
				      rsGetCarVendors.MoveNext
				      
				   wend
				%>
				</select>
			</td>
		</tr>
		<tr height=5><td></td></tr>
		</table>
	</td><br>
	</tr>
	<tr>
      <td colspan="28" align="center" height=5>
		<table align="center" border="0" cellspacing="0" cellpadding="0" >
		<tr>	
			<td nowrap=true>
      
			Pick-Up From&nbsp;&nbsp;&nbsp;&nbsp;
			</td>
		<td> 
		<input type="text" name="FromDate" maxlength="10" size="12" readonly="true" value=<%=response.write(date1)%>>
         <a href="javascript:show_calendar('form1.FromDate');" onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;">
         <img src="../images/show-calendar.gif" width="17" height="15"  border=0></a></font>
        &nbsp;&nbsp;&nbsp;&nbsp;</td><td> 
          Pick-Up To	<input type="text" name="ToDate" maxlength="10" size="12" readonly="true" value=<%=response.write(date2)%>>
         <a href="javascript:show_calendar('form1.ToDate');" onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;">
         <img src="../images/show-calendar.gif" width="17" height="15"  border=0></a></font>
         &nbsp;&nbsp;&nbsp;&nbsp;<!--</td><td>
         Pick-Up City&nbsp;&nbsp; 
         </td>
         <td> 
         <select name="cboCity">
         
			<option value=0>ALL</option>
			<%
			   'do while not rsGetCities.EOF
			   'if (trim(city) =trim(rsGetCities("CityID"))) then
			   %>
			            <option value=<%'=rsGetCities("CityID")%> selected><%'=rsGetCities("CityName")%></option>
			<%
				'else
			%>
			            <option value=<%'=rsGetCities("CityID")%>><%'=rsGetCities("CityName")%></option>
			<%	'end if
			     ' rsGetCities.MoveNext
			   'Loop
			%>
			</select>
			</td>--><td>
         &nbsp;&nbsp;&nbsp;<input type="submit" value="Go" name="btnGo" onClick="return GoToSubmit();">
         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td nowrap=true><input type="Button" value="Export To Excel" name="btnGo" onClick="return exportToExcel();">
				  <select name="TN_Excel_Option">
				    <option value="Page">Current Page</option>
				    <option value="All">ALL Page</option>				    
				  </select>
			</td>
			<td nowrap=true>&nbsp;</td>
			<td nowrap=true>&nbsp;</td>
         </td>
        </tr>
        </table>
     </td>
    </tr>  
     <br>
    <tr>
      <td align="center" colspan="28" class="redCopy"><b>Vendor/Car Wise Revenue Report (all rates in INR)</b><br>
      </td>
    </tr>
<%if Request.Form("hd") = 1 then%>    
    <tr>
		<td colspan="6" align="center"><table align="center" border="0" cellspacing="0" cellpadding="0" width="50%" bgcolor="#F3F3F3">	
			      <td><strong><%=iPdMainPage %></strong> Page of <strong><%=iPageCount %> </strong> &nbsp;&nbsp;&nbsp;[Total Records : <strong><%=iReccount%></strong>]</td>
                  <td valign="top" colspan=10 align="right" class="text"> 
		      <% 
					If CInt(iPageCount)>1  Then
		      
					  If CInt(iPdMainPage) > 1 Then Response.Write "<A style='text-decoration:none' href='#' onclick=""fnPrev('"&   Request.servervariables("SCRIPT_NAME") & "?page=1');return false;"" ><b>�First</b></a>&nbsp;&nbsp;"
					  If CInt(iPdMainPage) > 1 Then Response.Write "<A style='text-decoration:none' href='#' onclick=""fnPrev('"&   Request.servervariables("SCRIPT_NAME") & "?page=" &  iPdMainPage - 1 &"');return false;"" ><b>�Previous</b></a>&nbsp;&nbsp;"
					  If CInt(iPdMainPage) => 1  AND CInt(iPageCount) <> CInt(iPdMainPage) Then Response.Write "<A style='text-decoration:none' href='#' onclick=""fnNext('"& Request.servervariables("SCRIPT_NAME") & "?page=" &  iPdMainPage + 1 &"');return false;""><b>Next�</b></a>&nbsp;&nbsp;"
					  If CInt(iPdMainPage) => 1  AND CInt(iPageCount) <> CInt(iPdMainPage) Then Response.Write "<A style='text-decoration:none' href='#' onclick=""fnNext('"& Request.servervariables("SCRIPT_NAME") & "?page=" &  iPageCount &"');return false;""><b>Last�</b></a>&nbsp;"
			
					End If
			  %>					
 		       </td>
 		       <td colspan="20"></td></tr>           
	        </table>
	      </td>  
    </tr>
    </table><br>
   <table border="1" cellspacing="0" cellpadding="0" width="100%"> 
    <tr>
		
		<td align="center" valign="top" class="redCopy" width="5%"><b>S. No.</b></td>
		<td align="center" valign="top" class="redCopy" width="5%"><b>Vendor Name</b></td>
		<td align="center" valign="top" class="redCopy" width="5%"><b>PickUpDate</b></td>
		<td align="center" valign="top" class="redCopy" width="5%"><b>PickUpTime</b></td>
		<td align="center" valign="top" class="redCopy" width="5%"><b>CarCategory</b></td>
		<td align="center" valign="top" class="redCopy" width="5%"><b>Car Booked</b></td>
		<td align="center" valign="top" class="redCopy" width="5%"><b>Car Alloted</b></td>
		<td align="center" valign="top" class="redCopy" width="5%"><b>Car No</b></td>
		<td align="center" valign="top" class="redCopy" width="5%"><b>DutyslipNo</b></td>
		<td align="center" valign="top" class="redCopy" width="5%"><b>DutySlipDate</b></td>
		<td align="center" valign="top" class="redCopy" width="5%"><b>OutTime</b></td>
		<td align="center" valign="top" class="redCopy" width="5%"><b>Intime</b></td>
		<td align="center" valign="top" class="redCopy" width="5%"><b>KM In </b></td>
		<td align="center" valign="top" class="redCopy" width="5%"><b>KM out </b></td>		
		<td align="center" valign="top" class="redCopy" width="5%"><b>TotalKm</b></td>
		<td align="center" valign="top" class="redCopy" width="5%"><b>Vendor Share</b></td>
		<td align="center" valign="top" class="redCopy" width="5%"><b>Hertz Share</b></td>
		<td align="center" valign="top" class="redCopy" width="5%"><b>Total Revenue</b></td>
		<td align="center" valign="top" class="redCopy" width="5%"><b>Knock off Amount</b></td>
				
   </tr>
	
<%
	'///////////Do while loop here//////////////
		If Not rsBcgMis.EOF Then
			dim i
			i = 1
			iReccount = rsBcgMis.RecordCount
			iPageCount = rsBcgMis.PageCount
						   
			rsBcgMis.AbsolutePage = iPdMainPage						

			do while not rsBcgMis.EOF And iRowCtr <= rsBcgMis.PageSize
  'Booking_date,CarID, CarVendorID, ,,,,,, TimeIn ,,Revenue,RevenueSharePC,,                               				
%>	<tr>
		<td align="center" valign="top"  width="5%"><%=i%></td>
		<td align="center" valign="top"  width="5%"><%=rsBcgMis("Vendor_Name")%></td>
		<td align="center" valign="top"  width="5%"><%=rsBcgMis("PickUpDate")%></td>
		<td align="center" valign="top"  width="5%"><%=rsBcgMis("PickUpTime")%></td>
		<td align="center" valign="top"  width="5%"><%=rsBcgMis("CarCatName")%></td>
		<td align="center" valign="top"  width="5%"><%=rsBcgMis("BookedCar")%></td>
		<td align="center" valign="top"  width="5%"><%=rsBcgMis("AllotedCar")%></td>
		<td align="center" valign="top"  width="5%"><%=rsBcgMis("Car_NO")%></td>
		<td align="center" valign="top"  width="5%"><%=rsBcgMis("DutyslipNo")%></td>
		<td align="center" valign="top"  width="5%"><%=rsBcgMis("CreateDate")%></td>
		<td align="center" valign="top"  width="5%"><%=rsBcgMis("TimeOut")%></td>
		<td align="center" valign="top"  width="5%"><%=rsBcgMis("TimeIn")%></td>
		<td align="center" valign="top"  width="5%"><%=rsBcgMis("KMIn")%></td>
		<td align="center" valign="top"  width="5%"><%=rsBcgMis("KMOut")%></td>		
		<td align="center" valign="top"  width="5%"><%=rsBcgMis("TotalKM")%></td>
		<td align="center" valign="top"  width="5%"><%=rsBcgMis("VendorShare")%></td>
		<td align="center" valign="top"  width="5%"><%=rsBcgMis("HertzShare")%></td>
		<td align="center" valign="top"  width="5%"><%=rsBcgMis("Revenue")%></td>
		<td align="center" valign="top"  width="5%">
		<%
		chrPaymentMode 	= rsBcgMis("PaymentMode")
		intInvoiceID 	= rsBcgMis("InvoiceID")
		IF UCASE(Trim(""&chrPaymentMode)) = "CR" Then
		
			'Response.Write("Yes")
			SQL = "Select AdjustedAmt, deductions, TotalInvoiceamt from CCSTransKnocking where "
			SQL = SQL & " FKINvoiceID = '" &intInvoiceID&"'"
			Set RSKnock = con.Execute(SQL)
			IF Not RSKnock.EOF Then
				intAdjustedAmt		= RSKnock("AdjustedAmt")
				intDeductions		= RSKnock("deductions")
				intTotalInvoiceAmt	= RSKnock("TotalInvoiceamt")
				
'									response.write "<br>1==" & intTotalInvoiceAmt & "<br>2==" &intAdjustedAmt& "<br>3===" & intDeductions
'									
'									Response.Write("<br>TotalInvoiceamt=" & Round(CDBL("0"&intTotalInvoiceAmt)))
'									Response.Write("<br>TotalInvoiceamt1=" & Round(CDBL("0"&intAdjustedAmt) + CDBL("0"&intDeductions)))
				
				IF Trim(""&intDeductions) = "" Then
					intDeductions = 0
				End IF
				
				IF CDBL("0"&intTotalInvoiceAmt) <= CDBL("0"&intAdjustedAmt) + CDBL(intDeductions) Then
					'Response.write "Yes"
					Temp_AMT_01 = 0
					Temp_AMT_01 = Round(CDBL("0"&intAdjustedAmt) + CDBL(intDeductions))
					Temp_AMT_01 = FormatNumber(Temp_AMT_01,0)
					Response.write Temp_AMT_01
				Else
					'Response.write "No"
					Response.write "0"
				End IF
			Else
				'Response.write "No"
				Response.write "0"
			End IF
			RSKnock.Close
		Else
			'Response.write "NA"
			Response.Write("0")
		End IF
		%>
		</td>
		
	</tr>	
								
<%			i = i + 1
			rsBcgMis.MoveNext
			iRowCtr = iRowCtr + 1
			loop
		End If
%>

<%
	   rsBcgMis.Close
		 Set rsBcgMis = Nothing

		rsGetCarVendors.Close
		Set rsGetCarVendors = Nothing
else
end if
%>

</table>
<input type="hidden" id="hd" name="hd">
   <input type="hidden" name="TN_Total_Record" value="<%=iReccount%>">
   <input type="hidden" id="hdVal" name="hdVal" value="">		
 </form>
</body>
</html>
<%
 

'objrs.Close
'set objrs = nothing


  rsGetUserAccessType.Close
   Set rsGetUserAccessType = Nothing
   
	
   
 '  rsGetClientCoIndiv.Close
 '  Set rsGetClientCoIndiv = Nothing
   
   con.Close
   Set con = Nothing
%>
