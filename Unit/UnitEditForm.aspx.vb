Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class UnitEditForm
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtrefname As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnReset As System.Web.UI.WebControls.Button
    Protected WithEvents txtfname As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtmidname As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtlname As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlDesig As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtaddress As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlUnitCty As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtph1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtphone2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtphone3 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtfax As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtemail As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtservicetx As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtsvtax As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtGSTIN As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtGSTINAddress As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtedut As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txthdut As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtdst As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkoperate As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkair As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkhotel As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkcorporate As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkselfdrive As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkmainct As System.Web.UI.WebControls.CheckBox
    Protected WithEvents txtarearemarks As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkActive As System.Web.UI.WebControls.CheckBox
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    Protected WithEvents txtCityAbb As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtVatUnitAdd As System.Web.UI.WebControls.TextBox
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btnSubmit.Attributes("onClick") = "return validate_input();"
        If Not Page.IsPostBack Then
            populateddl()

            Dim dtrreader As SqlDataReader
            Dim accessdata As clsutility
            accessdata = New clsutility
            'dtrreader = accessdata.funcGetSQLDataReader("select *,isnull(SDCityABB,'') as SDCityABB1,isnull(VatUnitAddress,'') as VatUnitAddress1 from dbo.corintunitmaster where providerId=" & Session("provider_Id") & " and unitid=" & Request.QueryString("id") & " ")
            dtrreader = accessdata.funcGetSQLDataReader("select *,isnull(SDCityABB,'') as SDCityABB1,isnull(VatUnitAddress,'') as VatUnitAddress1 from dbo.corintunitmaster where unitid=" & Request.QueryString("id") & " ")
            dtrreader.Read()

            txtrefname.Text = dtrreader("unitname") & ""
            txtfname.Text = dtrreader("InChargefname") & ""
            txtmidname.Text = dtrreader("InChargemname") & ""
            txtlname.Text = dtrreader("InChargelname") & ""
            ddlDesig.Items.FindByValue(dtrreader("inchargedesigid")).Selected = True
            txtaddress.Text = dtrreader("unitaddress") & ""
            txtphone2.Text = dtrreader("unitphone2") & ""
            txtphone3.Text = dtrreader("unitphone3") & ""
            txtph1.Text = dtrreader("unitphone1") & ""
            txtfax.Text = dtrreader("unitfax") & ""
            txtemail.Text = dtrreader("unitemailid") & ""
            'txtservicetx.Text = dtrreader("servicetaxpercent") & ""
            txtsvtax.Text = dtrreader("servicetaxno") & ""
            'txtedut.Text = dtrreader("educesspercent") & ""
            'txtdst.Text = dtrreader("dstpercent") & ""
            chkoperate.Checked = dtrreader("operates24_7yn")
            chkhotel.Checked = dtrreader("hotelyn")
            chkair.Checked = dtrreader("airportyn")
            chkselfdrive.Checked = dtrreader("selfdriveyn")
            chkcorporate.Checked = dtrreader("corporateyn")
            chkmainct.Checked = dtrreader("maincityunityn")
            txtarearemarks.Text = dtrreader("remarks") & ""
            txtCityAbb.Text = dtrreader("SDCityABB1") & ""
            txtVatUnitAdd.Text = dtrreader("VatUnitAddress1") & ""
            chkActive.Checked = dtrreader("active")
            ddlUnitCty.Items.FindByValue(dtrreader("unitcityid")).Selected = True
            txtGSTIN.Text = dtrreader("GSTIN")
            txtGSTINAddress.Text = dtrreader("GSTRegisteredAddress")
            dtrreader.Close()
            accessdata.Dispose()

        End If
    End Sub

    Sub populateddl()
        'Dim dtrreader As SqlDataReader
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility

        ddlDesig.DataSource = objAcessdata.funcGetSQLDataReader("select designame,desigid from CORIntDesigMaster where active=1  order by designame")
        ddlDesig.DataValueField = "desigid"
        ddlDesig.DataTextField = "designame"
        ddlDesig.DataBind()
        ddlDesig.Items.Insert(0, New ListItem("", ""))

        'ddlUnitCty.DataSource = objAcessdata.funcGetSQLDataReader("select a.UnitCityID ,a.cityname from dbo.CORIntUnitCityMaster as a inner join dbo.corintcitymaster as b on a.UnitCityID =b. NearestUnitCityID where a.active=1 and b.ProviderId=" & Session("provider_Id") & "  order by a.cityname")
        ddlUnitCty.DataSource = objAcessdata.funcGetSQLDataReader("select a.UnitCityID ,a.cityname from dbo.CORIntUnitCityMaster as a inner join dbo.corintcitymaster as b on a.UnitCityID =b. NearestUnitCityID where a.active=1 order by a.cityname")

        ddlUnitCty.DataValueField = "UnitCityID"
        ddlUnitCty.DataTextField = "cityname"
        ddlUnitCty.DataBind()
        ddlUnitCty.Items.Insert(0, New ListItem("", ""))
        objAcessdata.Dispose()

    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("UnitEditForm.aspx?ID=" & Request.QueryString("id"))
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        Dim intuniqcheck As SqlParameter
        Dim intuniqvalue As int32

        cmd = New SqlCommand("procUpdateUnitMaster", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@unitid ", Request.QueryString("id"))
        cmd.Parameters.AddWithValue("@unitname", txtrefname.Text)
        cmd.Parameters.AddWithValue("@inchargefname", txtfname.Text)
        cmd.Parameters.AddWithValue("@inchargemname", txtmidname.Text)
        cmd.Parameters.AddWithValue("@inchargelname", txtlname.Text)
        cmd.Parameters.AddWithValue("@inchargedesigid", ddlDesig.SelectedItem.Value)
        cmd.Parameters.AddWithValue("@unitaddress ", txtaddress.Text)
        cmd.Parameters.AddWithValue("@unitcityid ", ddlUnitCty.SelectedItem.Value)
        cmd.Parameters.AddWithValue("@unitphone1 ", txtph1.Text)
        cmd.Parameters.AddWithValue("@unitphone2 ", txtphone2.Text)
        cmd.Parameters.AddWithValue("@unitphone3 ", txtphone3.Text)
        cmd.Parameters.AddWithValue("@unitfax", txtfax.Text)
        cmd.Parameters.AddWithValue("@unitemailid ", txtemail.Text)
        'cmd.Parameters.AddWithValue("@servicetaxpercent ", txtservicetx.Text)
        cmd.Parameters.AddWithValue("@servicetaxno ", txtsvtax.Text)
        'cmd.Parameters.AddWithValue("@educesspercent ", txtedut.Text)
        'cmd.Parameters.AddWithValue("@hducesspercent ", txthdut.Text)
        'cmd.Parameters.AddWithValue("@dstpercent ", txtdst.Text)
        cmd.Parameters.AddWithValue("@operates24_7yn ", get_YNvalue(chkoperate))
        cmd.Parameters.AddWithValue("@airportyn ", get_YNvalue(chkair))
        cmd.Parameters.AddWithValue("@hotelyn ", get_YNvalue(chkhotel))
        cmd.Parameters.AddWithValue("@corporateyn ", get_YNvalue(chkcorporate))
        cmd.Parameters.AddWithValue("@selfdriveyn ", get_YNvalue(chkselfdrive))
        cmd.Parameters.AddWithValue("@maincityunityn ", get_YNvalue(chkmainct))
        cmd.Parameters.AddWithValue("@CityAbb", txtCityAbb.Text)
        cmd.Parameters.AddWithValue("@VatUnitAddress", txtVatUnitAdd.Text)
        cmd.Parameters.AddWithValue("@remarks", txtarearemarks.Text)
        cmd.Parameters.AddWithValue("@modifiedby", Session("loggedin_user"))
        cmd.Parameters.AddWithValue("@active ", get_YNvalue(chkActive))
        cmd.Parameters.AddWithValue("@GSTIN", txtGSTIN.Text)
        cmd.Parameters.AddWithValue("@GSTRegisteredAddress", txtGSTINAddress.Text)

        intuniqcheck = cmd.Parameters.AddWithValue("@uniqcheckval", SqlDbType.Int)
        intuniqcheck.Direction = ParameterDirection.Output
        '  Try
        MyConnection.Open()
        cmd.ExecuteNonQuery()
        intuniqvalue = cmd.Parameters("@uniqcheckval").Value
        MyConnection.Close()
        If Not intuniqvalue = 0 Then
            lblErrorMsg.visible = True
            lblErrorMsg.text = "Unit already exist."
            Exit Sub
        Else
            lblErrorMsg.visible = False
            'pnlmainform.Visible = False
            pnlconfirmation.Visible = True

            lblMessage.Text = "You have updated the Unit successfully"
            hyplnkretry.Text = "Edit another Unit"
            hyplnkretry.NavigateUrl = "UnitEditSearch.aspx"
        End If
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function
End Class
