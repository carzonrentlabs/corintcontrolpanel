<%@ Page Language="vb" AutoEventWireup="false" Src="UnitEditForm.aspx.vb" Inherits="UnitEditForm" %>

<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>CarzonRent :: Internal software Control panel</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <link href="../HertzInt.css" type="text/css" rel="stylesheet">
    <script language="JavaScript" src="../utilityfunction.js"></script>
    <script language="JavaScript">
        function validate_input() {
            if (document.forms[0].txtrefname.value == "") {
                alert("Please make sure all the fields marked with * are filled in.")
                document.forms[0].txtrefname.focus();
                return false;
            }

            if (document.forms[0].txtfname.value == "") {
                alert("Please make sure all the fields marked with * are filled in.")
                document.forms[0].txtfname.focus();
                return false;
            }

            if (document.forms[0].txtlname.value == "") {
                alert("Please make sure all the fields marked with * are filled in.")
                document.forms[0].txtlname.focus();
                return false;
            }

            if (document.forms[0].ddlDesig.value == "") {
                alert("Please make sure all the fields marked with * are filled in.")
                document.forms[0].ddlDesig.focus();
                return false;
            }

            if (document.forms[0].txtaddress.value == "") {
                alert("Please make sure all the fields marked with * are filled in.")
                document.forms[0].txtaddress.focus();
                return false;
            }

            if (document.forms[0].ddlUnitCty.value == "") {
                alert("Please make sure all the fields marked with * are filled in.")
                document.forms[0].ddlUnitCty.focus();
                return false;
            }

            if (document.forms[0].txtph1.value == "") {
                alert("Please make sure all the fields marked with * are filled in.")
                document.forms[0].txtph1.focus();
                return false;
            }

            if (document.forms[0].txtemail.value == "") {
                alert("Please make sure all the fields marked with * are filled in.")
                document.forms[0].txtemail.focus();
                return false;
            }

            if (document.forms[0].txtGSTIN.value == "") {
                alert("Please Enter GSTIN No.")
                document.forms[0].txtGSTIN.focus();
                return false;
            }

            if (document.forms[0].txtGSTINAddress.value == "") {
                alert("Please Enter GST Registered Address.")
                document.forms[0].txtGSTINAddress.focus();
                return false;
            }


            if (document.forms[0].txtemail.value != "") {
                var theStr = document.forms[0].txtemail.value;
                var atIndex = theStr.indexOf('@');
                var dotIndex = theStr.indexOf('.', atIndex);
                theSub = theStr.substring(0, dotIndex + 1)

                if ((atIndex < 1) || (atIndex != theStr.lastIndexOf('@')) || (dotIndex < atIndex + 2) || (theStr.length <= theSub.length)) {
                    alert("Unit Email ID is not a valid Email ID");
                    return false;
                }
            }

            if (document.forms[0].txtsvtax.value == "") {
                alert("Please make sure all the fields marked with * are filled in.")
                document.forms[0].txtsvtax.focus();
                return false;
            }
        }
    </script>
</head>
<body onload="OnLoadshowLength(document.forms[0].txtarearemarks.value,shwMessage)">
    <form id="Form1" method="post" runat="server">
        <uc1:Headerctrl ID="Headerctrl1" runat="server"></uc1:Headerctrl>
        <center>
            <asp:Panel ID="pnlconfirmation" Visible="False" runat="server">
                <table cellpadding="0" cellspacing="0" border="0">
                    <tr align="center">
                        <td colspan="2">
                            <asp:Label ID="lblMessage" runat="server"></asp:Label></td>
                    </tr>
                    <tr align="center">
                        <td colspan="2">
                            <asp:HyperLink ID="hyplnkretry" runat="server"></asp:HyperLink></td>
                    </tr>
                </table>
            </asp:Panel>
        </center>
        <table align="center">
            <tbody>
                <tr>
                    <td align="center" colspan="2"><b><u>Edit a Unit</u></b></td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <asp:Label ID="lblErrorMsg" CssClass="subRedHead" Visible="false" runat="server"></asp:Label>
                    </td>
                </tr>
                <%--<asp:Panel ID="pnlmainform" Runat="server">--%>
                <tr>
                    <td>Unit Reference Name*
                    </td>
                    <td>
                        <asp:TextBox ID="txtrefname" runat="server" CssClass="input" MaxLength="100"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>&nbsp;In charge�s First Name*
                    </td>
                    <td>
                        <asp:TextBox ID="txtfname" runat="server" CssClass="input" MaxLength="50"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>In charge�s Middle Name</td>
                    <td>
                        <asp:TextBox ID="txtmidname" runat="server" CssClass="input" MaxLength="50"></asp:TextBox></td>
                </tr>
                <tr>
                    <td style="height: 20px">In charge�s Last Name*</td>
                    <td style="height: 20px">
                        <asp:TextBox ID="txtlname" runat="server" CssClass="input" MaxLength="50"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>In charge�s Designation*</td>
                    <td>
                        <asp:DropDownList ID="ddlDesig" runat="server"></asp:DropDownList></td>
                </tr>
                <tr>
                    <td>Unit Address*</td>
                    <td>
                        <asp:TextBox ID="txtaddress" runat="server" CssClass="input" MaxLength="250"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>Unit City*</td>
                    <td>
                        <asp:DropDownList ID="ddlUnitCty" runat="server"></asp:DropDownList></td>
                </tr>
                <tr>
                    <td>&nbsp;Unit Phone 1*</td>
                    <td>
                        <asp:TextBox ID="txtph1" runat="server" CssClass="input" MaxLength="20"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>Unit Phone 2</td>
                    <td>
                        <asp:TextBox ID="txtphone2" runat="server" CssClass="input" MaxLength="20"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>Unit Phone 3</td>
                    <td>
                        <asp:TextBox ID="txtphone3" runat="server" CssClass="input" MaxLength="20"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>Unit Fax</td>
                    <td>
                        <asp:TextBox ID="txtfax" runat="server" CssClass="input" MaxLength="20"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>&nbsp;Unit Email ID*</td>
                    <td>
                        <asp:TextBox ID="txtemail" runat="server" CssClass="input" MaxLength="100"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>Service Tax Number*</td>
                    <td>
                        <asp:TextBox ID="txtsvtax" runat="server" CssClass="input" MaxLength="20"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>* GSTIN</td>
                    <td>
                        <asp:TextBox ID="txtGSTIN" runat="server" CssClass="input" MaxLength="20"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>* GST Registered Address</td>
                    <td>
                        <asp:TextBox ID="txtGSTINAddress" runat="server" CssClass="input"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>Operates 24X7</td>
                    <td>
                        <asp:CheckBox ID="chkoperate" runat="server" CssClass="input"></asp:CheckBox></td>
                </tr>
                <tr>
                    <td>Airport Service</td>
                    <td>
                        <asp:CheckBox ID="chkair" runat="server" CssClass="input"></asp:CheckBox></td>
                </tr>
                <tr>
                    <td>Hotel Service</td>
                    <td>
                        <asp:CheckBox ID="chkhotel" runat="server" CssClass="input"></asp:CheckBox></td>
                </tr>
                <tr>
                    <td>Corporate Service</td>
                    <td>
                        <asp:CheckBox ID="chkcorporate" runat="server" CssClass="input"></asp:CheckBox></td>
                </tr>
                <tr>
                    <td>Self Drive Service</td>
                    <td>
                        <asp:CheckBox ID="chkselfdrive" runat="server" CssClass="input"></asp:CheckBox></td>
                </tr>

                <tr>
                    <td>City Abbreviation</td>
                    <td>
                        <asp:TextBox ID="txtCityAbb" runat="server" CssClass="input" MaxLength="20"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>Vat Unit Address</td>
                    <td>
                        <asp:TextBox ID="txtVatUnitAdd" runat="server" CssClass="input" MaxLength="20"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>Is this the main unit of the city?</td>
                    <td>
                        <asp:CheckBox ID="chkmainct" runat="server" CssClass="input"></asp:CheckBox></td>
                </tr>
                <tr>
                    <td valign="top">Remarks(<span class="shwText" id="shwMessage">0</span>/2000 chars)</td>
                    <td>
                        <asp:TextBox ID="txtarearemarks" onkeydown="showLength(this.form.txtarearemarks,'document.all.shwMessage',2000)" onkeyup="showLength(this.form.txtarearemarks,'document.all.shwMessage',2000)" runat="server" CssClass="input" MaxLength="2000" TextMode="MultiLine" Columns="50" Rows="3"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>Active</td>
                    <td>
                        <asp:CheckBox ID="chkActive" runat="server" CssClass="input"></asp:CheckBox></td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <asp:Button ID="btnSubmit" runat="server" CssClass="input" Text="Submit"></asp:Button>
                        &nbsp;&nbsp;&nbsp;&nbsp;
						<asp:Button ID="btnReset" runat="server" CssClass="input" Text="Reset"></asp:Button>
                    </td>
                </tr>
                <%--</asp:Panel>--%>
            </tbody>
        </table>
    </form>
</body>
</html>
