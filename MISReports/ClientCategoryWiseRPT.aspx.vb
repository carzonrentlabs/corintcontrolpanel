'/* 
'* Developed By :		Deepti Thukral on 31st May '07
'* Description	:		
'*/
Imports commonutility
'Imports queryClass
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Text
Imports System
Public Class ClientCategoryWiseRPT
    Inherits System.Web.UI.Page
    Public objDataReader As SqlDataReader
    Public objDataAdapter As SqlDataAdapter
    Public objDataSet As DataSet
    Public objCommand As SqlCommand

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents CBOClient As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtFromDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtToDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnproceed As System.Web.UI.WebControls.Button
    Protected WithEvents btnExportToExcel As System.Web.UI.WebControls.Button
    Protected WithEvents btnBack As System.Web.UI.WebControls.Button
    Protected WithEvents dgSummary As System.Web.UI.WebControls.DataGrid
    '   Protected WithEvents CBOCategory As System.Web.UI.WebControls.DropDownList

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btnproceed.Attributes("onClick") = "return checkbeforenext();"
        If Not Page.IsPostBack Then
            populateddl()
            dgSummary.DataSource = ""
            dgSummary.DataBind()
            ' setGridData()
        End If
    End Sub
    Sub populateddl()
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility

        CBOClient.DataSource = objAcessdata.funcGetSQLDataReader("select clientconame,clientcoid from CORIntClientCoMaster where active = 1 order by clientconame")
        CBOClient.DataValueField = "clientcoid"
        CBOClient.DataTextField = "clientconame"
        CBOClient.DataBind()
        CBOClient.Items.Insert(0, New ListItem("All", ""))
    End Sub
   
    Private Sub btnproceed_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnproceed.Click
        Dim oConnection As SqlConnection
        oConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        'Dim objQuery As clsQuery
        'objQuery = New clsQuery
        Dim objDataSet As DataSet
        Dim i As Integer
        Dim a1 As Integer

        Dim a2 As Decimal
        Dim a3 As Decimal
        Dim a4 As Decimal
        Dim a5 As Decimal
        objDataSet = New DataSet

        oConnection.Open()
        ' objDataSet = ExecuteDSProcedure("PROC_OwnedCar")
        'objDataSet1 = ExecuteDSProcedure("PROC_HiredCarNonDTO")
        'objDataSet2 = ExecuteDSProcedure("PROC_HiredCarDTO")

        objCommand = New SqlCommand
        objCommand.CommandType = CommandType.StoredProcedure
        objCommand.CommandText = "Proc_ClientWiseBookingRPT"

        objCommand.Parameters.Add("@clientid", CBOClient.SelectedValue)
        objCommand.Parameters.Add("@fromDate", txtFromDate.Text)
        objCommand.Parameters.Add("@toDate", txtToDate.Text)
        objCommand.Connection = oConnection

        objDataAdapter = New SqlDataAdapter(objCommand)
        objDataAdapter.Fill(objDataSet)
        Dim k As Integer
        k = 0
        a1 = 0
        objDataSet.Tables(0).Columns.Add("Total")

        For i = 0 To (objDataSet.Tables(0).Rows.Count - 1)
            a2 = objDataSet.Tables(0).Rows(k)(1)
            If (objDataSet.Tables(0).Rows(i)(1) = a2) Then
                a1 = a1 + Convert.ToInt32(objDataSet.Tables(0).Rows(i)(4))


                'objDataSet.Tables(0).Rows(i)(5) = a1
                'objDataSet.Tables(0).Rows(i)(9) = decTotal

            Else
                '   a1 = Convert.ToInt32(objDataSet.Tables(0).Rows(i)(4))
                objDataSet.Tables(0).Rows(i - 1)(5) = a1
                k = i
                a1 = 0

                a1 = Convert.ToInt32(objDataSet.Tables(0).Rows(i)(4))
            
            End If
            If (i = objDataSet.Tables(0).Rows.Count - 1) Then
                objDataSet.Tables(0).Rows(i)(5) = a1
            End If


            '    objDataSet.Tables(0).Rows(i)(5) = a1
            '  a1 = 0
        Next

        'Dim t As Integer
        't = objDataSet.Tables(0).Rows.Count

        'objDataSet.Tables(0).Rows(t - 1)(5) = a1 + Convert.ToInt32(objDataSet.Tables(0).Rows(t - 1)(4))
        objDataSet.AcceptChanges()

        dgSummary.DataSource = objDataSet 'objQuery.ExecuteProcedure("ProcCarWiseRevenueRPT")
        dgSummary.DataBind()
    End Sub

    Private Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("../index/CP_index.aspx?ID=43")
    End Sub

    Private Sub btnExportToExcel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportToExcel.Click
        'export to excel
        context.Response.Buffer = True
        context.Response.ClearContent()
        context.Response.ClearHeaders()
        context.Response.ContentType = "application/vnd.ms-excel"
        EnableViewState = True
        Dim tw As New System.IO.StringWriter
        Dim hw As New System.Web.UI.HtmlTextWriter(tw)

        dgSummary.RenderControl(hw)
        Context.Response.Write(tw.ToString())
        Context.Response.Flush()
        context.Response.Close()
        context.Response.End()
    End Sub

End Class
