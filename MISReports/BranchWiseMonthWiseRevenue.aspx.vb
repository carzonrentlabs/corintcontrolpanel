'/* 
'* Developed By :		Deepti Thukral on 31st May '07
'* Description	:		
'*/
Imports commonutility
'Imports queryClass
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Text
Imports System
Public Class BranchWiseMonthWiseRevenue
    Inherits System.Web.UI.Page
    Public objDataReader As SqlDataReader
    Public objDataAdapter As SqlDataAdapter
    Public objDataSet As DataSet
    Public objCommand As SqlCommand

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents CBOUnit As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtToDate As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtYearDate As System.Web.UI.WebControls.DropDownList
    Protected WithEvents btnproceed As System.Web.UI.WebControls.Button
    Protected WithEvents btnExportToExcel As System.Web.UI.WebControls.Button
    Protected WithEvents btnBack As System.Web.UI.WebControls.Button
    Protected WithEvents dgSummary As System.Web.UI.WebControls.DataGrid

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btnproceed.Attributes("onClick") = "return checkbeforenext();"
        If Not Page.IsPostBack Then
            populateddl()
            dgSummary.DataSource = ""
            dgSummary.DataBind()
            ' setGridData()
        End If
    End Sub
    Sub populateddl()
        Dim isHQ As Boolean
        Dim strquery1 As StringBuilder
        Dim dtrreader As SqlDataReader
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility
        Dim objAcessdata5 As clsutility
        objAcessdata5 = New clsutility
        Dim oConnection As SqlConnection


        isHQ = False

        dtrreader = objAcessdata.funcGetSQLDataReader("SELECT a.AccessType, a.UnitID, b.UnitCityID, c.Region FROM CORIntSysUsersMaster as a, CORIntUnitMaster as b, CORIntUnitCityMaster as c WHERE a.UnitID = b.UnitID and b.UnitCityID = c.UnitCityID and a.SysUserID = " & Session("loggedin_user"))

        strquery1 = New StringBuilder("SELECT CityName, UnitCityID FROM CORIntUnitCityMaster WHERE Active = 1 and CityName <> '' ")
        While dtrreader.Read
            ' When Access type is Service Unit or City
            If (dtrreader("AccessType") = "SU" Or dtrreader("AccessType") = "CT") Then
                strquery1.Append(" and UnitCityID = " & dtrreader("UnitCityID"))
            End If

            ' When Access type is Region
            If (dtrreader("AccessType") = "RN") Then
                strquery1.Append(" and Region = '" & dtrreader("Region") & "' ")
            End If
            If (dtrreader("AccessType") = "HQ") Then
                isHQ = True
            End If
        End While
        strquery1.Append(" ORDER BY CityName ")
        CBOUnit.DataSource = objAcessdata5.funcGetSQLDataReader(strquery1.ToString)
        'CBOUnit.DataSource = objAcessdata.funcGetSQLDataReader("select cityname,unitcityid from corintunitcitymaster where active = 1 and cityname <>'' order by cityname")
        CBOUnit.DataValueField = "UnitCityID"
        CBOUnit.DataTextField = "cityname"
        CBOUnit.DataBind()
        'If isHQ Then
        CBOUnit.Items.Insert(0, New ListItem("--Select--", "A"))
        ' End If


      

    End Sub

    Private Sub btnproceed_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnproceed.Click
        Dim oConnection As SqlConnection
        'oConnection = New SqlConnection("server=192.168.34.101;database=carzonrent;uid=carzonrent;pwd=carzonrent;pooling=false;")
        oConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim objDataSet As DataSet
        objDataSet = New DataSet

        oConnection.Open()
        'Response.Write(oConnection.ConnectionString)
        'MsgBox(oConnection)

        objCommand = New SqlCommand
        objCommand.CommandTimeout = 0
        objCommand.CommandType = CommandType.StoredProcedure
        objCommand.CommandText = "Proc_BranchWiseMonthwiseRPT"
        objCommand.Parameters.Add("@city", CBOUnit.SelectedValue)
        objCommand.Parameters.Add("@month", txtToDate.SelectedValue)
        objCommand.Parameters.Add("@year", txtYearDate.SelectedValue)
        'objCommand.Parameters.Add("@test", 1)
        objCommand.Connection = oConnection

        objDataAdapter = New SqlDataAdapter(objCommand)
        objDataAdapter.Fill(objDataSet)


        objDataSet.AcceptChanges()

        dgSummary.DataSource = objDataSet 'objQuery.ExecuteProcedure("ProcCarWiseRevenueRPT")
        dgSummary.DataBind()

        
    End Sub
    Private Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("../index/CP_index.aspx?ID=43")
    End Sub

    Private Sub btnExportToExcel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportToExcel.Click
        'export to excel
        context.Response.Buffer = True
        context.Response.ClearContent()
        context.Response.ClearHeaders()
        context.Response.ContentType = "application/vnd.ms-excel"
        EnableViewState = True
        Dim tw As New System.IO.StringWriter
        Dim hw As New System.Web.UI.HtmlTextWriter(tw)

        dgSummary.RenderControl(hw)
        Context.Response.Write(tw.ToString())
        Context.Response.Flush()
        context.Response.Close()
        context.Response.End()
    End Sub

End Class
