'/* 
'* Developed By :		Deepti Thukral on 31st May '07
'* Description	:		
'*/
Imports commonutility
'Imports queryClass
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Text
Imports System
Public Class BookingStatusRPT
    Inherits System.Web.UI.Page
    Public objDataReader As SqlDataReader
    Public objDataAdapter As SqlDataAdapter
    Public objDataSet As DataSet
    Public objCommand As SqlCommand

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents CBOUnit As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtFromDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtToDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnproceed As System.Web.UI.WebControls.Button
    Protected WithEvents btnExportToExcel As System.Web.UI.WebControls.Button
    Protected WithEvents btnBack As System.Web.UI.WebControls.Button
    Protected WithEvents dgSummary As System.Web.UI.WebControls.DataGrid

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btnproceed.Attributes("onClick") = "return checkbeforenext();"
        If Not Page.IsPostBack Then
            populateddl()
            dgSummary.DataSource = ""
            dgSummary.DataBind()
            ' setGridData()
        End If
    End Sub
    Sub populateddl()
        Dim isHQ As Boolean
        Dim strQuery As StringBuilder
        Dim dtrreader1 As SqlDataReader
        Dim objAcessdata1 As clsutility
        objAcessdata1 = New clsutility

        Dim dtrreader2 As SqlDataReader
        Dim objAcessdata2 As clsutility
        objAcessdata2 = New clsutility

        isHQ = False

        dtrreader1 = objAcessdata1.funcGetSQLDataReader("SELECT a.DesigID, a.UnitID, b.UnitCityID, c.Region FROM CORIntSysUsersMaster as a, CORIntUnitMaster as b, CORIntUnitCityMaster as c WHERE a.UnitID = b.UnitID and b.UnitCityID = c.UnitCityID and a.DesigID=1 and a.SysUserID = " & Session("loggedin_user"))

        strQuery = New StringBuilder("SELECT distinct d.carcatid,d.carcatname FROM CORIntUnitCityMaster a,corintcarbooking b,corintcarmodelmaster c,corintcarcatmaster d WHERE a.Active = 1 and a.CityName <> '' and b.pickupcityid=a.unitcityid and c.carmodelid=b.modelid and c.carcatid=d.carcatid ")
        While dtrreader1.Read
            ' When Access type is Service Unit or City

            strQuery.Append(" and a.UnitCityID = " & dtrreader1("UnitCityID"))

        End While
        strQuery.Append(" ORDER BY d.carcatname ")
        CBOUnit.DataSource = objAcessdata2.funcGetSQLDataReader(strQuery.ToString)
        'CBOUnit.DataSource = objAcessdata.funcGetSQLDataReader("select cityname,unitcityid from corintunitcitymaster where active = 1 and cityname <>'' order by cityname")
        CBOUnit.DataValueField = "carcatid"
        CBOUnit.DataTextField = "carcatname"
        CBOUnit.DataBind()
        '  If isHQ Then
        CBOUnit.Items.Insert(0, New ListItem("All", ""))
        '  End If
    End Sub
   
    Private Sub btnproceed_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnproceed.Click
        Dim oConnection As SqlConnection
        oConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        'Dim objQuery As clsQuery
        'objQuery = New clsQuery
        Dim objDataSet As DataSet
        objDataSet = New DataSet

        oConnection.Open()
        ' objDataSet = ExecuteDSProcedure("PROC_OwnedCar")
        'objDataSet1 = ExecuteDSProcedure("PROC_HiredCarNonDTO")
        'objDataSet2 = ExecuteDSProcedure("PROC_HiredCarDTO")

        objCommand = New SqlCommand
        objCommand.CommandType = CommandType.StoredProcedure
        objCommand.CommandText = "proc_BookingStatusRPT"
        objCommand.Parameters.Add("@CarCatid", CBOUnit.SelectedValue)
        objCommand.Parameters.Add("@fromDate", txtFromDate.Text)
        objCommand.Parameters.Add("@toDate", txtToDate.Text)
        objCommand.Connection = oConnection

        objDataAdapter = New SqlDataAdapter(objCommand)
        objDataAdapter.Fill(objDataSet)


        objDataSet.AcceptChanges()

        dgSummary.DataSource = objDataSet 'objQuery.ExecuteProcedure("ProcCarWiseRevenueRPT")
        dgSummary.DataBind()
    End Sub

    Private Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("../index/CP_index.aspx?ID=43")
    End Sub

    Private Sub btnExportToExcel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportToExcel.Click
        'export to excel
        context.Response.Buffer = True
        context.Response.ClearContent()
        context.Response.ClearHeaders()
        context.Response.ContentType = "application/vnd.ms-excel"
        EnableViewState = True
        Dim tw As New System.IO.StringWriter
        Dim hw As New System.Web.UI.HtmlTextWriter(tw)

        dgSummary.RenderControl(hw)
        Context.Response.Write(tw.ToString())
        Context.Response.Flush()
        context.Response.Close()
        context.Response.End()
    End Sub

End Class
