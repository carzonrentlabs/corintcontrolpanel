<%@ Page Language="vb" AutoEventWireup="false" Src="CarModelNameWiseRevenue1.aspx.vb" Inherits="CarModelNameWiseRevenue1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CarModelNameWiseRevenue</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="JavaScript" src="../utilityfunction.js"></script>
		<script language="JavaScript" src="../JScripts/Datefunc.js"></script>
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
		<script language="JavaScript">
		
			function checkbeforenext()
			{
			
			if(document.Form1.txtFromDate.value=="")
				{
					alert("Please select Date.")
					document.Form1.txtFromDate.focus();
					return false;
				}
			if(document.Form1.txtToDate.value=="")
				{
					alert("Please select Date.")
					document.Form1.txtToDate.focus();
					return false;
				}
		}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<table width="780" align="center">
				<TBODY>
					<tr>
						<td><IMG src="../images/logo_carzonret.gif"></td>
						<td align="right"><IMG src="../images/logo_hertz.gif" width="100" align="right"></td>
					</tr>
					<tr>
						<td class="subRedHead" align="center" colSpan="2">Car Model Wise Revenue</td>
					</tr>
					<tr>
						<td colSpan="2">
							<table cellSpacing="0" cellPadding="0" width="100%" border="0">
								<TBODY>
									<tr bgColor="#ffe5e0">
										<td>&nbsp;</td>
										<td>Unit Name:</td>
										<td colSpan="3"><asp:dropdownlist id="CBOUnit" runat="server" CssClass="input"></asp:dropdownlist></td>
									</tr>
									<tr bgColor="#ffe5e0">
										<td>&nbsp;</td>
										<td>From Date: *</td>
										<td><asp:textbox id="txtFromDate" runat="server"></asp:textbox><A onmouseover="window.status='Date Picker';return true;" onmouseout="window.status='';return true;"
												href="javascript:show_calendar('Form1.txtFromDate');"><IMG height="21" src="../images/show-calendar.gif" width="24" border="0"></A></td>
										<td>To DateIn: *</td>
										<td><asp:textbox id="txtToDate" runat="server"></asp:textbox><A onmouseover="window.status='Date Picker';return true;" onmouseout="window.status='';return true;"
												href="javascript:show_calendar('Form1.txtToDate');"><IMG height="21" src="../images/show-calendar.gif" width="24" border="0"></A></td>
									</tr>
									<tr bgColor="#ffe5e0">
										<td align="center" colSpan="5"><asp:button id="btnproceed" runat="server" CssClass="input" Text="Proceed"></asp:button><asp:button id="btnExportToExcel" runat="server" Text="Export to Excel"></asp:button>&nbsp;
											<asp:button id="btnBack" runat="server" Text="Return to Menu"></asp:button></td>
									</tr>
								</TBODY>
							</table>
						</td>
					</tr>
					<tr>
						<td align="center" colSpan="2">
							<div style="OVERFLOW-Y: scroll; OVERFLOW-X: scroll; WIDTH: 780px; HEIGHT: 480px; visible: "
								noWrap><asp:datagrid id="dgSummary" runat="server" Width="760px" AutoGenerateColumns="False" CellPadding="0"
									BorderStyle="None">
									<SelectedItemStyle Wrap="False"></SelectedItemStyle>
									<EditItemStyle Wrap="False"></EditItemStyle>
									<HeaderStyle Wrap="False" HorizontalAlign="Center" CssClass="subRedHead" BackColor="#FFE5BA"></HeaderStyle>
									<Columns>
										<asp:BoundColumn></asp:BoundColumn>
										<asp:BoundColumn DataField="cityname" HeaderText="Unit"></asp:BoundColumn>
										<asp:BoundColumn DataField="model" HeaderText="Model"></asp:BoundColumn>
										<asp:BoundColumn DataField="countofduties" HeaderText="Count of Duties">
											<HeaderStyle Width="120px"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="TypeofDuty" HeaderText="Type of Duty">
											<ItemStyle HorizontalAlign="Left"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="BasicAmount" HeaderText="Revenue">
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
										</asp:BoundColumn>
									</Columns>
								</asp:datagrid></div>
						</td>
					</tr>
				</TBODY>
			</table>
		</form>
	</body>
</HTML>
