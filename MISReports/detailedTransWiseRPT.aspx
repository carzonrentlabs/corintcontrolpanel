<%@ Page Language="vb" AutoEventWireup="false" src="detailedTransWiseRPT.aspx.vb" Inherits="detailedTransWiseRPT" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>detailedTransWiseRPT</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../JScripts/Datefunc.js"></script>
		<script language="JavaScript" src="../utilityfunction.js"></script>
		<script language="javascript">
		function validate()
		{
			// Check for mandatory values
			var strvalues
			strvalues = ('cboUnit, txtFromDate, txtToDate')
			return checkmandatory(strvalues);
		}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<table width="780" align="center">
				<TBODY>
					<tr>
						<td><IMG src="../images/logo_carzonret.gif"></td>
						<td align="right"><IMG src="../images/logo_hertz.gif" width="100" align="right"></td>
					</tr>
					<tr>
						<td class="subRedHead" align="center" colSpan="2">Detail Transaction Wise Report</td>
					</tr>
					<tr>
						<td colSpan="2"><hr>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<TBODY>
									<tr bgColor="#ffe5e0">
										<td>&nbsp;</td>
										<td>Service Unit *</td>
										<td><asp:DropDownList id="cboUnit" runat="server"></asp:DropDownList></td>
										<td>Date-In Date [From] *</td>
										<td valign="top"><asp:TextBox id="txtFromDate" runat="server" Width="80" ReadOnly></asp:TextBox><A onmouseover="window.status='Date Picker';return true;" onmouseout="window.status='';return true;"
												href="javascript:show_calendar('Form1.txtFromDate');"><IMG height="21" src="../images/show-calendar.gif" width="24" border="0"></A></td>
										<td>[To]*</td>
										<td valign="top"><asp:TextBox id="txtToDate" runat="server" Width="80" ReadOnly></asp:TextBox><A onmouseover="window.status='Date Picker';return true;" onmouseout="window.status='';return true;"
												href="javascript:show_calendar('Form1.txtToDate');"><IMG height="21" src="../images/show-calendar.gif" width="24" border="0"></A></td>
									</tr>
									<tr bgColor="#ffe5e0">
										<td colspan="3">&nbsp;</td>
										<td colspan="3">
											<asp:button id="btnProceed" runat="server" Text="Proceed"></asp:button>&nbsp;
											<asp:button id="btnExportToExcel" runat="server" Text="Export to Excel"></asp:button>&nbsp;
											<asp:button id="btnBack" runat="server" Text="Return to Menu"></asp:button></td>
										<td>&nbsp;</td>
									</tr>
								</TBODY>
							</table>
						</td>
					</tr>
					<tr>
						<td align="center" colSpan="2">
							<div style="OVERFLOW-Y: scroll; OVERFLOW-X: scroll; WIDTH: 780px; HEIGHT: 440px; visible: "
								noWrap><asp:datagrid id="dgSummary" runat="server" Width="4100px" CssClass="GridFixedHeader" AutoGenerateColumns="False"
									CellPadding="0" CellSpacing="2">
									<SelectedItemStyle Wrap="False"></SelectedItemStyle>
									<EditItemStyle Wrap="False"></EditItemStyle>
									<HeaderStyle Wrap="False" HorizontalAlign="Center" CssClass="GridFixedHeader" BackColor="#ffe5ba"></HeaderStyle>
									<Columns>
										<asp:BoundColumn></asp:BoundColumn>
										<asp:BoundColumn DataField="BookingUnit" HeaderText="Booking Unit"></asp:BoundColumn>
										<asp:BoundColumn DataField="ServiceUnit" HeaderText="Service Unit"></asp:BoundColumn>
										<asp:BoundColumn DataField="BookingType" HeaderText="Booking Type"></asp:BoundColumn>
										<asp:BoundColumn DataField="BookingDate" HeaderText="Booking Date" DataFormatString="{0:dd-MM-yyyy}"></asp:BoundColumn>
										<asp:BoundColumn DataField="TypeOfDuty" HeaderText="Type of Duty"></asp:BoundColumn>
										<asp:BoundColumn DataField="CarUseDate" HeaderText="Car Use Date" DataFormatString="{0:dd-MM-yyyy}"></asp:BoundColumn>
										<asp:BoundColumn DataField="ClosingDate" HeaderText="Closing Date" DataFormatString="{0:dd-MM-yyyy}"></asp:BoundColumn>
										<asp:BoundColumn DataField="InvoiceNo" HeaderText="Invoice No."></asp:BoundColumn>
										<asp:BoundColumn DataField="CompanyName" HeaderText="Company Name"></asp:BoundColumn>
										<asp:BoundColumn DataField="GuestName" HeaderText="Guest Name"></asp:BoundColumn>
										<asp:BoundColumn DataField="ChauffeurName" HeaderText="Chauffeur Name"></asp:BoundColumn>
										<asp:BoundColumn DataField="CarNo" HeaderText="Car No."></asp:BoundColumn>
										<asp:BoundColumn DataField="CarModel" HeaderText="Car Model"></asp:BoundColumn>
										<asp:BoundColumn DataField="CategoryBooked" HeaderText="Category Booked"></asp:BoundColumn>
										<asp:BoundColumn DataField="CategoryAllotted" HeaderText="Category Allotted"></asp:BoundColumn>
										<asp:BoundColumn DataField="Upgrade" HeaderText="Upgrade"></asp:BoundColumn>
										<asp:BoundColumn DataField="PkgName" HeaderText="Package Slab"></asp:BoundColumn>
										<asp:BoundColumn DataField="DateOut" HeaderText="Date Out" DataFormatString="{0:dd-MM-yyyy}"></asp:BoundColumn>
										<asp:BoundColumn DataField="DateIn" HeaderText="Date In" DataFormatString="{0:dd-MM-yyyy}"></asp:BoundColumn>
										<asp:BoundColumn DataField="TimeOut" HeaderText="Time Out">
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="TimeIn" HeaderText="Time In">
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="KMOut" HeaderText="KM Out">
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="KMIn" HeaderText="KM In">
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="TotalKm" HeaderText="Total Km">
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="TotalHrs" HeaderText="Total Hrs">
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="ExtraKM" HeaderText="Extra KM">
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="ExtraHr" HeaderText="Extra Hr">
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="PkgRate" HeaderText="Basic" DataFormatString="{0:N2}">
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="ExtraKMRate" HeaderText="Extra KM Amount" DataFormatString="{0:N2}">
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="ExtraHrRate" HeaderText="Extra Hrs Amount" DataFormatString="{0:N2}">
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="ParkTollChages" HeaderText="Parking-Interstate" DataFormatString="{0:N2}">
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="AdjustedAmount" HeaderText="Others" DataFormatString="{0:N2}">
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="FuelSurcharge" HeaderText="Fuel Surcharge" DataFormatString="{0:N2}">
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="OutStnAmt" HeaderText="Outstation Allowance" DataFormatString="{0:N2}">
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="NightStayAmt" HeaderText="Night stay Allowance" DataFormatString="{0:N2}">
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="Total" HeaderText="Total" DataFormatString="{0:N2}">
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="ServiceTax" HeaderText="Service Tax" DataFormatString="{0:N2}">
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="VAT" HeaderText="VAT" DataFormatString="{0:N2}">
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="TotalInvoice" HeaderText="Total Invoice Amount" DataFormatString="{0:N2}">
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="VendorName" HeaderText="Vendor Name"></asp:BoundColumn>
										<asp:BoundColumn DataField="VendorRevenue" HeaderText="Revenue %">
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="CarHire" HeaderText="Car Hire Charge">
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="CarType" HeaderText="Car Type"></asp:BoundColumn>
									</Columns>
								</asp:datagrid></div>
						</td>
					</tr>
				</TBODY>
			</table>
			<input type="hidden" id="PageSize" value="24" runat="server"> <input type="hidden" id="CurrentPage" value="1" runat="server">
			<input type="hidden" id="TotalSize" runat="server">
		</form>
	</body>
</HTML>
