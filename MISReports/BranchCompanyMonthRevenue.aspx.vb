'/* 
' * Developed By :		Vivek Sharma on 22nd June '07
' * Description	:		
' */
Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.text
Imports System
Public Class BranchCompanyMonthRevenue
    Inherits System.Web.UI.Page
    Public objDataReader As SqlDataReader
    Public objDataAdapter As SqlDataAdapter
    Public objDataSet As DataSet
    Public objCommand As SqlCommand
    Public oConnection As SqlConnection

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents cboCity As System.Web.UI.WebControls.DropDownList
    Protected WithEvents btnProceed As System.Web.UI.WebControls.Button
    Protected WithEvents btnExportToExcel As System.Web.UI.WebControls.Button
    Protected WithEvents btnBack As System.Web.UI.WebControls.Button
    Protected WithEvents dgSummary As System.Web.UI.WebControls.DataGrid
    Protected WithEvents cboMonth As System.Web.UI.HtmlControls.HtmlSelect
    Protected WithEvents cboYear As System.Web.UI.HtmlControls.HtmlSelect

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not Page.IsPostBack Then
            ' Call function to fill the values in combo
            populateddl()

            ' Show grid header
            dgSummary.DataSource = ""
            dgSummary.DataBind()
        End If
    End Sub

    Sub populateddl()
        Dim strQuery As StringBuilder
        Dim dtrreader1 As SqlDataReader
        Dim objAcessdata1 As clsutility
        objAcessdata1 = New clsutility

        Dim dtrreader2 As SqlDataReader
        Dim objAcessdata2 As clsutility
        objAcessdata2 = New clsutility

        dtrreader1 = objAcessdata1.funcGetSQLDataReader("SELECT a.AccessType, a.UnitID, b.UnitCityID, c.Region FROM CORIntSysUsersMaster as a, CORIntUnitMaster as b, CORIntUnitCityMaster as c WHERE a.UnitID = b.UnitID and b.UnitCityID = c.UnitCityID and a.SysUserID = " & Session("loggedin_user"))

        strQuery = New StringBuilder("SELECT CityName, UnitCityID FROM CORIntUnitCityMaster WHERE Active = 1 and CityName <> '' ")
        While dtrreader1.Read
            ' When Access type is Service Unit or City
            If (dtrreader1("AccessType") = "SU" Or dtrreader1("AccessType") = "CT") Then
                strQuery.Append(" and UnitCityID = " & dtrreader1("UnitCityID"))
            End If

            ' When Access type is Region
            If (dtrreader1("AccessType") = "RN") Then
                strQuery.Append(" and Region = '" & dtrreader1("Region") & "' ")
            End If

        End While
        strQuery.Append(" ORDER BY CityName ")
        cboCity.DataSource = objAcessdata2.funcGetSQLDataReader(strQuery.ToString)

        cboCity.DataValueField = "UnitCityID"
        cboCity.DataTextField = "CityName"
        cboCity.DataBind()

        objAcessdata1.Dispose()
        objAcessdata2.Dispose()
    End Sub

    Private Sub btnProceed_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProceed.Click
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility

        Dim strQuery As StringBuilder
        Dim UnitCityID As Integer
        Dim FinancialMonth As Integer
        Dim SelectedMonth As Integer
        Dim SelectedYear As String
        Dim SelectedLastYear As String
        Dim CurrentDate As String

        UnitCityID = cboCity.SelectedValue
        FinancialMonth = 4
        SelectedMonth = cboMonth.Value
        SelectedYear = cboYear.Value
        SelectedLastYear = Convert.ToInt32(cboYear.Value) - 1
        CurrentDate = Now.Date ' + "/" + Now.Day + "/" + Now.Year

        strQuery = New StringBuilder("SELECT * FROM fn_GetBranchCompany(" & UnitCityID & ", " & FinancialMonth & ", " & SelectedMonth & ", '" & SelectedYear & "', '" & SelectedLastYear & "', '" & CurrentDate & "')")
        dgSummary.DataSource = objAcessdata.funcGetSQLDataReader(strQuery.ToString)
        dgSummary.DataBind()
    End Sub
    Private Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("../index/CP_index.aspx?ID=43")
    End Sub

    Private Sub btnExportToExcel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportToExcel.Click
        'export to excel
        context.Response.Buffer = True
        context.Response.ClearContent()
        context.Response.ClearHeaders()
        context.Response.ContentType = "application/vnd.ms-excel"
        EnableViewState = True
        Dim tw As New System.IO.StringWriter
        Dim hw As New System.Web.UI.HtmlTextWriter(tw)

        dgSummary.RenderControl(hw)
        Context.Response.Write(tw.ToString())
        Context.Response.Flush()
        context.Response.Close()
        context.Response.End()
    End Sub

End Class
