'/* 
'* Developed By :		Deepti Thukral on 31st May '07
'* Description	:		
'*/
Imports commonutility
'Imports queryClass
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Text
Imports System
Public Class CarModelNameWiseRevenue

    Inherits System.Web.UI.Page
    Public objDataReader As SqlDataReader
    Public objDataAdapter As SqlDataAdapter
    Public objDataSet As DataSet
    Public objDataSet1 As DataSet
    Protected WithEvents btnExportToExcel As System.Web.UI.WebControls.Button
    Protected WithEvents CBOModel As System.Web.UI.WebControls.DropDownList
    Public objCommand As SqlCommand

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents CBOUnit As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtFromDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtToDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnproceed As System.Web.UI.WebControls.Button
    Protected WithEvents btnBack As System.Web.UI.WebControls.Button
    Protected WithEvents dgSummary As System.Web.UI.WebControls.DataGrid

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btnproceed.Attributes("onClick") = "return checkbeforenext();"
        If Not Page.IsPostBack Then
            populateddl()
            dgSummary.DataSource = ""
            dgSummary.DataBind()
            ' setGridData()
        End If
    End Sub
    Sub populateddl()
        Dim isHQ As Boolean
        Dim strQuery As StringBuilder
        Dim dtrreader1 As SqlDataReader
        Dim objAcessdata1 As clsutility
        objAcessdata1 = New clsutility

        Dim dtrreader2 As SqlDataReader
        Dim objAcessdata2 As clsutility
        objAcessdata2 = New clsutility

        Dim dtrreader As SqlDataReader
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility

        isHQ = False

        dtrreader1 = objAcessdata1.funcGetSQLDataReader("SELECT a.AccessType, a.UnitID, b.UnitCityID, c.Region FROM CORIntSysUsersMaster as a, CORIntUnitMaster as b, CORIntUnitCityMaster as c WHERE a.UnitID = b.UnitID and b.UnitCityID = c.UnitCityID and a.SysUserID = " & Session("loggedin_user"))

        strQuery = New StringBuilder("SELECT CityName, UnitCityID FROM CORIntUnitCityMaster WHERE Active = 1 and CityName <> '' ")
        While dtrreader1.Read
            ' When Access type is Service Unit or City
            If (dtrreader1("AccessType") = "SU" Or dtrreader1("AccessType") = "CT") Then
                strQuery.Append(" and UnitCityID = " & dtrreader1("UnitCityID"))
            End If

            ' When Access type is Region
            If (dtrreader1("AccessType") = "RN") Then
                strQuery.Append(" and Region = '" & dtrreader1("Region") & "' ")
            End If
            If (dtrreader1("AccessType") = "HQ") Then
                isHQ = True
            End If
        End While
        strQuery.Append(" ORDER BY CityName ")
        CBOUnit.DataSource = objAcessdata2.funcGetSQLDataReader(strQuery.ToString)
        'CBOUnit.DataSource = objAcessdata.funcGetSQLDataReader("select cityname,unitcityid from corintunitcitymaster where active = 1 and cityname <>'' order by cityname")
        CBOUnit.DataValueField = "unitcityid"
        CBOUnit.DataTextField = "cityname"
        CBOUnit.DataBind()
        If isHQ Then
            CBOUnit.Items.Insert(0, New ListItem("All", ""))
        End If

        CBOModel.DataSource = objAcessdata.funcGetSQLDataReader("select carmodelid,carmodelname from corintcarmodelmaster where active = 1 order by carmodelname")
        CBOModel.DataValueField = "carmodelid"
        CBOModel.DataTextField = "carmodelname"
        CBOModel.DataBind()

        CBOModel.Items.Insert(0, New ListItem("All", ""))
    End Sub

    Private Sub btnproceed_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnproceed.Click
        Dim oConnection As SqlConnection
        oConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        'Dim objQuery As clsQuery
        'objQuery = New clsQuery
        Dim objDataSet As DataSet
        objDataSet = New DataSet
        Dim objDataSet1 As DataSet
        objDataSet1 = New DataSet
        'oConnection.ConnectionTimeout = 0
        oConnection.Open()

        ' objDataSet = ExecuteDSProcedure("PROC_OwnedCar")
        'objDataSet1 = ExecuteDSProcedure("PROC_HiredCarNonDTO")
        'objDataSet2 = ExecuteDSProcedure("PROC_HiredCarDTO")

        'objDataSet1.Tables(0).Columns.Add("Cityname")           ' Column no. 51
        'objDataSet1.Tables(0).Columns.Add("TypeOfDuty")      ' Column no. 52
        'objDataSet1.Tables(0).Columns.Add("CountofDuties")             ' Column no. 53
        'objDataSet1.Tables(0).Columns.Add("model")    ' Column no. 54
        'objDataSet1.Tables(0).Columns.Add("BasicAmount")         ' Column no. 55


        objCommand = New SqlCommand
        objCommand.CommandTimeout = 0
        objCommand.CommandType = CommandType.StoredProcedure
        objCommand.CommandText = "Proc_CarModelWiseRevenueRPT"
        objCommand.Parameters.Add("@UnitCityId", CBOUnit.SelectedValue)
        objCommand.Parameters.Add("@fromDate", txtFromDate.Text)
        objCommand.Parameters.Add("@toDate", txtToDate.Text)
        objCommand.Parameters.Add("@modelid", CBOModel.SelectedValue)
        objCommand.Connection = oConnection

        objDataAdapter = New SqlDataAdapter(objCommand)
        objDataAdapter.Fill(objDataSet)
        Dim k As Integer
        Dim i As Integer
        Dim a1 As Integer

        Dim a2 As String
        Dim a3 As Decimal
        Dim a4 As String
        Dim a5 As String
        Dim a6 As String
        Dim a7 As String
        Dim a8 As String

        k = 0
        a1 = 0
        a3 = 0
        objDataSet.Tables(0).Columns.Add("Total")
        Dim Table1 As DataTable
        Table1 = New DataTable("dt")
        'creating a table named Customers

        'declaring three rows for the table

        Dim CityName As DataColumn = New DataColumn("CityName")
        CityName.DataType = System.Type.GetType("System.String")
        Table1.Columns.Add(CityName)

        Dim TypeofDuty As DataColumn = New DataColumn("TypeofDuty")
        TypeofDuty.DataType = System.Type.GetType("System.String")
        Table1.Columns.Add(TypeofDuty)

        Dim countofduties As DataColumn = New DataColumn("countofduties")
        countofduties.DataType = System.Type.GetType("System.String")
        Table1.Columns.Add(countofduties)

        Dim model As DataColumn = New DataColumn("model")
        model.DataType = System.Type.GetType("System.String")
        Table1.Columns.Add(model)

        Dim BasicAmount As DataColumn = New DataColumn("BasicAmount")
        BasicAmount.DataType = System.Type.GetType("System.String")
        Table1.Columns.Add(BasicAmount)
        'declaring a column named Name


        'setting the datatype for the column

        Dim dr As DataRow

        If CBOModel.SelectedItem.Text = "All" Then




            For i = 0 To (objDataSet.Tables(0).Rows.Count - 1)
                a2 = objDataSet.Tables(0).Rows(k)(3)
                a7 = objDataSet.Tables(0).Rows(k)(0)
                a8 = objDataSet.Tables(0).Rows(k)(1)
                If (objDataSet.Tables(0).Rows(i)(3) = a2) Then
                    If ((objDataSet.Tables(0).Rows(i)(0) = a7)) Then
                        If ((objDataSet.Tables(0).Rows(i)(1) = a8)) Then
                            a1 = a1 + Convert.ToInt32(objDataSet.Tables(0).Rows(i)(2))
                            a3 = a3 + Convert.ToInt32(objDataSet.Tables(0).Rows(i)(4))
                            a4 = Convert.ToString(objDataSet.Tables(0).Rows(k)(0))
                            a5 = Convert.ToString(objDataSet.Tables(0).Rows(k)(1))
                            a6 = Convert.ToString(objDataSet.Tables(0).Rows(k)(3))

                            'objDataSet.Tables(0).Rows(i)(5) = a1
                            'objDataSet.Tables(0).Rows(i)(9) = decTotal

                        Else


                            dr = Table1.NewRow()
                            dr.Item("CityName") = a4
                            dr.Item("TypeofDuty") = a5
                            dr.Item("countofduties") = a1
                            dr.Item("model") = a6
                            dr.Item("BasicAmount") = a3
                            Table1.Rows.Add(dr)


                            k = i
                            a1 = 0
                            a3 = 0
                            a1 = Convert.ToInt32(objDataSet.Tables(0).Rows(i)(2))
                            a3 = Convert.ToInt32(objDataSet.Tables(0).Rows(i)(4))
                            a4 = Convert.ToString(objDataSet.Tables(0).Rows(i)(0))
                            a5 = Convert.ToString(objDataSet.Tables(0).Rows(i)(1))
                            a6 = Convert.ToString(objDataSet.Tables(0).Rows(i)(3))

                        End If
                    Else
                        dr = Table1.NewRow()
                        dr.Item("CityName") = a4
                        dr.Item("TypeofDuty") = a5
                        dr.Item("countofduties") = a1
                        dr.Item("model") = a6
                        dr.Item("BasicAmount") = a3
                        Table1.Rows.Add(dr)


                        k = i
                        a1 = 0
                        a3 = 0
                        a1 = Convert.ToInt32(objDataSet.Tables(0).Rows(i)(2))
                        a3 = Convert.ToInt32(objDataSet.Tables(0).Rows(i)(4))
                        a4 = Convert.ToString(objDataSet.Tables(0).Rows(i)(0))
                        a5 = Convert.ToString(objDataSet.Tables(0).Rows(i)(1))
                        a6 = Convert.ToString(objDataSet.Tables(0).Rows(i)(3))

                    End If
                Else
                    dr = Table1.NewRow()
                    dr.Item("CityName") = a4
                    dr.Item("TypeofDuty") = a5
                    dr.Item("countofduties") = a1
                    dr.Item("model") = a6
                    dr.Item("BasicAmount") = a3
                    Table1.Rows.Add(dr)


                    k = i
                    a1 = 0
                    a3 = 0
                    a1 = Convert.ToInt32(objDataSet.Tables(0).Rows(i)(2))
                    a3 = Convert.ToInt32(objDataSet.Tables(0).Rows(i)(4))
                    a4 = Convert.ToString(objDataSet.Tables(0).Rows(i)(0))
                    a5 = Convert.ToString(objDataSet.Tables(0).Rows(i)(1))
                    a6 = Convert.ToString(objDataSet.Tables(0).Rows(i)(3))

                End If

            Next
        Else

            For i = 0 To (objDataSet.Tables(0).Rows.Count - 1)
                'a2 = objDataSet.Tables(0).Rows(k)(3)
                a7 = objDataSet.Tables(0).Rows(k)(0)
                a8 = objDataSet.Tables(0).Rows(k)(1)
                'If (objDataSet.Tables(0).Rows(i)(3) = a2) Then
                If ((objDataSet.Tables(0).Rows(i)(0) = a7)) Then
                    If ((objDataSet.Tables(0).Rows(i)(1) = a8)) Then
                        a1 = a1 + Convert.ToInt32(objDataSet.Tables(0).Rows(i)(2))
                        a3 = a3 + Convert.ToInt32(objDataSet.Tables(0).Rows(i)(4))
                        a4 = Convert.ToString(objDataSet.Tables(0).Rows(k)(0))
                        a5 = Convert.ToString(objDataSet.Tables(0).Rows(k)(1))
                        a6 = Convert.ToString(objDataSet.Tables(0).Rows(k)(3))

                        'objDataSet.Tables(0).Rows(i)(5) = a1
                        'objDataSet.Tables(0).Rows(i)(9) = decTotal

                    Else


                        dr = Table1.NewRow()
                        dr.Item("CityName") = a4
                        dr.Item("TypeofDuty") = a5
                        dr.Item("countofduties") = a1
                        dr.Item("model") = a6
                        dr.Item("BasicAmount") = a3
                        Table1.Rows.Add(dr)


                        k = i
                        a1 = 0
                        a3 = 0
                        a1 = Convert.ToInt32(objDataSet.Tables(0).Rows(i)(2))
                        a3 = Convert.ToInt32(objDataSet.Tables(0).Rows(i)(4))
                        a4 = Convert.ToString(objDataSet.Tables(0).Rows(i)(0))
                        a5 = Convert.ToString(objDataSet.Tables(0).Rows(i)(1))
                        a6 = Convert.ToString(objDataSet.Tables(0).Rows(i)(3))

                    End If
                Else
                    dr = Table1.NewRow()
                    dr.Item("CityName") = a4
                    dr.Item("TypeofDuty") = a5
                    dr.Item("countofduties") = a1
                    dr.Item("model") = a6
                    dr.Item("BasicAmount") = a3
                    Table1.Rows.Add(dr)


                    k = i
                    a1 = 0
                    a3 = 0
                    a1 = Convert.ToInt32(objDataSet.Tables(0).Rows(i)(2))
                    a3 = Convert.ToInt32(objDataSet.Tables(0).Rows(i)(4))
                    a4 = Convert.ToString(objDataSet.Tables(0).Rows(i)(0))
                    a5 = Convert.ToString(objDataSet.Tables(0).Rows(i)(1))
                    a6 = Convert.ToString(objDataSet.Tables(0).Rows(i)(3))

                End If
                'Else
                'dr = Table1.NewRow()
                'dr.Item("CityName") = a4
                'dr.Item("TypeofDuty") = a5
                'dr.Item("countofduties") = a1
                'dr.Item("model") = a6
                'dr.Item("BasicAmount") = a3
                'Table1.Rows.Add(dr)


                'k = i
                'a1 = 0
                'a3 = 0
                'a1 = Convert.ToInt32(objDataSet.Tables(0).Rows(i)(2))
                'a3 = Convert.ToInt32(objDataSet.Tables(0).Rows(i)(4))
                'a4 = Convert.ToString(objDataSet.Tables(0).Rows(i)(0))
                'a5 = Convert.ToString(objDataSet.Tables(0).Rows(i)(1))
                'a6 = Convert.ToString(objDataSet.Tables(0).Rows(i)(3))

                'End If

            Next
            dr = Table1.NewRow()
            dr.Item("CityName") = a4
            dr.Item("TypeofDuty") = a5
            dr.Item("countofduties") = a1
            dr.Item("model") = a6
            dr.Item("BasicAmount") = a3
            Table1.Rows.Add(dr)
        End If


        Dim ds As New DataSet
        ds = New DataSet
        'creating a dataset
        ds.Tables.Add(Table1)
        ds.AcceptChanges()

        objDataSet.AcceptChanges()

        dgSummary.DataSource = ds 'objQuery.ExecuteProcedure("ProcCarWiseRevenueRPT")
        dgSummary.DataBind()
    End Sub
    Private Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("../index/CP_index.aspx?ID=43")
    End Sub

    Private Sub btnExportToExcel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportToExcel.Click
        'export to excel
        context.Response.Buffer = True
        context.Response.ClearContent()
        context.Response.ClearHeaders()
        context.Response.ContentType = "application/vnd.ms-excel"
        EnableViewState = True
        Dim tw As New System.IO.StringWriter
        Dim hw As New System.Web.UI.HtmlTextWriter(tw)

        dgSummary.RenderControl(hw)
        Context.Response.Write(tw.ToString())
        Context.Response.Flush()
        context.Response.Close()
        context.Response.End()
    End Sub
End Class
