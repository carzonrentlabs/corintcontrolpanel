Imports commonutility
'Imports queryClass
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Text
Imports System

Public Class test_CarModel
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents DataGrid1 As System.Web.UI.WebControls.DataGrid

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim oConnection As SqlConnection
        oConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))

        Dim objDataSet As DataSet
        Dim objCommand As SqlCommand
        Dim objDataAdapter As SqlDataAdapter
        objDataSet = New DataSet

        oConnection.Open()

        objCommand = New SqlCommand
        objCommand.CommandTimeout = 0
        objCommand.CommandType = CommandType.StoredProcedure
        objCommand.CommandText = "Proc_CarModelWiseRevenueRPT"

        objCommand.Parameters.Add("@UnitCityId", 8)
        objCommand.Parameters.Add("@fromDate", "06/01/2007")
        objCommand.Parameters.Add("@toDate", "06/05/2007")
        objCommand.Connection = oConnection

        objDataAdapter = New SqlDataAdapter(objCommand)
        objDataAdapter.Fill(objDataSet)

        DataGrid1.DataSource = objDataSet
        DataGrid1.DataBind()
    End Sub

End Class
