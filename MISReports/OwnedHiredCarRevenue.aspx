<%@ Page Language="vb" AutoEventWireup="false" src="OwnedHiredCarRevenue.aspx.vb" Inherits="OwnedHiredCarRevenue" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
  <HEAD>
		<title>OwnedHiredCarRevenue</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="JavaScript" src="../utilityfunction.js"></script>
		<script language="JavaScript" src="../JScripts/Datefunc.js"></script>
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
		<script language="JavaScript">
		
			function checkbeforenext()
			{
			
			if(document.Form1.txtFromDate.value=="")
				{
					alert("Please select Date.")
					document.Form1.txtFromDate.focus();
					return false;
				}
			if(document.Form1.txtToDate.value=="")
				{
					alert("Please select Date.")
					document.Form1.txtToDate.focus();
					return false;
				}
		}
		</script>
</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<table width="780" align="center">
				<TBODY>
					<tr>
						<td><IMG src="../images/logo_carzonret.gif"></td>
						<td align="right"><IMG src="../images/logo_hertz.gif" width="100" align="right"></td>
					</tr>
					<tr>
						<td class="subRedHead" align="center" colSpan="2">Owned Car vs. Hired 
							car&nbsp;Revenue</td>
					</tr>
					<tr>
						<td colSpan="2">
							<table cellSpacing="0" cellPadding="0" width="100%" border="0">
								<TBODY>
									<tr bgColor="#ffe5e0">
										<td colspan="2">Category:
											<asp:DropDownList id="txtCategory" runat="server" CssClass="input"></asp:DropDownList></td>
											<td>From Date: *<asp:TextBox id="txtFromDate" runat="server" ReadOnly="True"></asp:TextBox><A onmouseover="window.status='Date Picker';return true;" onmouseout="window.status='';return true;"
												href="javascript:show_calendar('Form1.txtFromDate');"><IMG height="21" src="../images/show-calendar.gif" width="24" border="0"></A></td>
											<td>To Date: *<asp:TextBox id="txtToDate" runat="server"></asp:TextBox><A onmouseover="window.status='Date Picker';return true;" onmouseout="window.status='';return true;"
												href="javascript:show_calendar('Form1.txtToDate');"><IMG height="21" src="../images/show-calendar.gif" width="24" border="0"></A>
										</td>	
									</tr>
									<tr bgColor="#ffe5e0">
									<td align=center colspan="4">
									<asp:button id="btnProceed" runat="server" CssClass="input" Text="Proceed"></asp:button>&nbsp;<asp:button id="btnExportToExcel" runat="server" Text="Export to Excel"></asp:button>
											<asp:button id="btnBack" runat="server" Text="Return to Menu"></asp:button>
									</td>
									</tr>
								</TBODY>
							</table></td></tr>
					<tr>
						<td colSpan="2"><b>Owned Cars Revenue</b></td>
					</tr>
					<tr>
						<td align="center" colSpan="2"><asp:datagrid id="dgSummary" runat="server" Width="760px" AutoGenerateColumns="False" CellPadding="0"
								BorderStyle="None">
								<SelectedItemStyle Wrap="False"></SelectedItemStyle>
								<EditItemStyle Wrap="False"></EditItemStyle>
								<HeaderStyle Wrap="False" HorizontalAlign="Center" CssClass="subRedHead" BackColor="#ffe5e0"></HeaderStyle>
								<Columns>
									<asp:BoundColumn>
										<HeaderStyle Width="120px"></HeaderStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="carcatname" HeaderText="Particulars">
										<HeaderStyle Width="120px"></HeaderStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="Revenue" HeaderText="Revenue">
										<HeaderStyle Width="120px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Right"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="NoofCars" HeaderText="No of Cars">
										<HeaderStyle Width="120px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Right"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="NoofBookings" HeaderText="No of Bookings">
										<HeaderStyle Width="120px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Right"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="PerCarRevenue" HeaderText="per Car Revenue">
										<HeaderStyle Width="120px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Right"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="PerBookingRevenue" HeaderText="Per Booking Revenue">
										<HeaderStyle Width="120px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Right"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn>
										<HeaderStyle Width="120px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Right"></ItemStyle>
									</asp:BoundColumn>
								</Columns>
							</asp:datagrid>
						</td>
					</tr>
					<tr>
						<td colSpan="2"><b>Hired Cars Revenue( NON DTO)</b></td>
					</tr>
					<tr>
						<td align="center" colSpan="2">
							<asp:datagrid id="dgReport" runat="server" Width="760px" AutoGenerateColumns="False" CellPadding="0"
								BorderStyle="None">
								<SelectedItemStyle Wrap="False"></SelectedItemStyle>
								<EditItemStyle Wrap="False"></EditItemStyle>
								<HeaderStyle Wrap="False" HorizontalAlign="Center" CssClass="subRedHead" BackColor="#ffe5e0"></HeaderStyle>
								<Columns>
									<asp:BoundColumn>
										<HeaderStyle Width="120px"></HeaderStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="carcatname" HeaderText="Particulars">
										<HeaderStyle Width="120px"></HeaderStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="Revenue" HeaderText="Revenue">
										<HeaderStyle Width="120px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Right"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="NoofCars" HeaderText="No of Cars">
										<HeaderStyle Width="120px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Right"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="NoofBookings" HeaderText="No of Bookings">
										<HeaderStyle Width="120px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Right"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="PerCarRevenue" HeaderText="per Car Revenue">
										<HeaderStyle Width="120px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Right"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="PerBookingRevenue" HeaderText="Per Booking Revenue">
										<HeaderStyle Width="120px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Right"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="carhirecharges" HeaderText="Car Hire Charges">
										<HeaderStyle Width="120px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Right"></ItemStyle>
									</asp:BoundColumn>
								</Columns>
							</asp:datagrid>
						</td>
					</tr>
					<tr>
						<td colSpan="2"><b>Hired Cars Revenue(DTO)</b></td>
					</tr>
					<tr>
						<td align="center" colSpan="2">
							<asp:datagrid id="DgReport1" runat="server" Width="760px" AutoGenerateColumns="False" CellPadding="0"
								BorderStyle="None">
								<SelectedItemStyle Wrap="False"></SelectedItemStyle>
								<EditItemStyle Wrap="False"></EditItemStyle>
								<HeaderStyle Wrap="False" HorizontalAlign="Center" CssClass="subRedHead" BackColor="#ffe5e0"></HeaderStyle>
								<Columns>
									<asp:BoundColumn DataField="carhirecharges">
										<HeaderStyle Width="120px"></HeaderStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="carcatname" HeaderText="Particulars">
										<HeaderStyle Width="120px"></HeaderStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="Revenue" HeaderText="Revenue">
										<HeaderStyle Width="120px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Right"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="NoofCars" HeaderText="No of Cars">
										<HeaderStyle Width="120px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Right"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="NoofBookings" HeaderText="No of Bookings">
										<HeaderStyle Width="120px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Right"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="PerCarRevenue" HeaderText="per Car Revenue">
										<HeaderStyle Width="120px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Right"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="PerBookingRevenue" HeaderText="Per Booking Revenue">
										<HeaderStyle Width="120px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Right"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn HeaderText="Car Hire Charges">
										<HeaderStyle Width="120px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Right"></ItemStyle>
									</asp:BoundColumn>
								</Columns>
							</asp:datagrid>
							<DIV></DIV>
						</td>
					</tr></TBODY></table>
		</form>
	</body>
</HTML>
