'/* 
'* Developed By :		Deepti Thukral on 21st May '07
'* Description	:		
'*/
Imports commonutility
'Imports queryClass
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Imports System.text

Public Class GuestWiseRevenue
    Inherits System.Web.UI.Page
    Public objDataReader As SqlDataReader
    Public objDataAdapter As SqlDataAdapter
    Public objDataSet As DataSet
    Public objCommand As SqlCommand
    Protected WithEvents CBOUnit As System.Web.UI.WebControls.DropDownList
    Protected WithEvents btnproceed As System.Web.UI.WebControls.Button
    Protected WithEvents btnBack As System.Web.UI.WebControls.Button
    Protected WithEvents dgSummary As System.Web.UI.WebControls.DataGrid
    Protected WithEvents txtFromDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtToDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnExportToExcel As System.Web.UI.WebControls.Button
    Protected WithEvents CBOCompany As System.Web.UI.WebControls.DropDownList
    Protected WithEvents CBOGuest As System.Web.UI.WebControls.DropDownList
    Public oConnection As SqlConnection

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btnproceed.Attributes("onClick") = "return checkbeforenext();"
        If Not Page.IsPostBack Then
            populateddl()
            dgSummary.DataSource = ""
            dgSummary.DataBind()
        End If
    End Sub
    Sub populateddl()
        Dim isHQ As Boolean
        Dim strQuery As StringBuilder
        Dim dtrreader1 As SqlDataReader
        Dim objAcessdata1 As clsutility
        objAcessdata1 = New clsutility

        Dim dtrreader2 As SqlDataReader
        Dim objAcessdata2 As clsutility
        objAcessdata2 = New clsutility
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility

        

        isHQ = False

        dtrreader1 = objAcessdata1.funcGetSQLDataReader("SELECT a.AccessType, a.UnitID, b.UnitCityID, c.Region FROM CORIntSysUsersMaster as a, CORIntUnitMaster as b, CORIntUnitCityMaster as c WHERE a.UnitID = b.UnitID and b.UnitCityID = c.UnitCityID and a.SysUserID = " & Session("loggedin_user"))

        strQuery = New StringBuilder("SELECT CityName, UnitCityID FROM CORIntUnitCityMaster WHERE Active = 1 and CityName <> '' ")
        While dtrreader1.Read
            ' When Access type is Service Unit or City
            If (dtrreader1("AccessType") = "SU" Or dtrreader1("AccessType") = "CT") Then
                strQuery.Append(" and UnitCityID = " & dtrreader1("UnitCityID"))
            End If

            ' When Access type is Region
            If (dtrreader1("AccessType") = "RN") Then
                strQuery.Append(" and Region = '" & dtrreader1("Region") & "' ")
            End If
            If (dtrreader1("AccessType") = "HQ") Then
                isHQ = True
            End If
        End While
        strQuery.Append(" ORDER BY CityName ")
        CBOUnit.DataSource = objAcessdata2.funcGetSQLDataReader(strQuery.ToString)
        'CBOUnit.DataSource = objAcessdata.funcGetSQLDataReader("select cityname,unitcityid from corintunitcitymaster where active = 1 and cityname <>'' order by cityname")
        CBOUnit.DataValueField = "unitcityid"
        CBOUnit.DataTextField = "cityname"
        CBOUnit.DataBind()
        CBOUnit.Items.Insert(0, New ListItem("--Select--", " "))

       

        CBOCompany.DataSource = objAcessdata.funcGetSQLDataReader("select distinct(c.clientcoid), c.clientconame from corintclientcomaster c where c.active = 1  order by c.clientconame")
        CBOCompany.DataValueField = "clientcoid"
        CBOCompany.DataTextField = "clientconame"
        CBOCompany.DataBind()
        CBOCompany.Items.Insert(0, New ListItem("", ""))

           

       
    End Sub
    Public Function ExecuteDSProcedure(ByVal procedureName As String) As DataSet
        ' // Create object for calling connection method
        Dim objUtility As clsutility
        objUtility = New clsutility

        'objUtility = New clsutility
        'oConnection = New SqlConnection

        Dim oConnection As SqlConnection
        oConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))

        ' // Open connection
        objCommand = New SqlCommand
        objCommand.Connection = oConnection
        oConnection.Open()
        objCommand.CommandTimeout = 0

        ' // Set value for executing stored procedure
        objCommand.CommandType = CommandType.StoredProcedure
        objCommand.CommandText = procedureName
        objCommand.Connection = oConnection

        ' // Create Data Adapter object and fill value in that
        objDataSet = New DataSet
        objDataAdapter = New SqlDataAdapter(objCommand)
        objDataAdapter.Fill(objDataSet)

        '// objDataReader = objCommand.ExecuteReader(CommandBehavior.CloseConnection)
        Return objDataSet
    End Function

    Private Sub btnproceed_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnproceed.Click
        Dim oConnection As SqlConnection
        oConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        'Dim objQuery As clsQuery
        'objQuery = New clsQuery
        Dim objDataSet As DataSet
        objDataSet = New DataSet

        oConnection.Open()
        ' objDataSet = ExecuteDSProcedure("PROC_OwnedCar")
        'objDataSet1 = ExecuteDSProcedure("PROC_HiredCarNonDTO")
        'objDataSet2 = ExecuteDSProcedure("PROC_HiredCarDTO")

        objCommand = New SqlCommand
        objCommand.CommandType = CommandType.StoredProcedure
        objCommand.CommandText = "PROC_GuestWiseRevenueRPT"
        objCommand.Parameters.Add("@UnitCityId", CBOUnit.SelectedValue)
        objCommand.Parameters.Add("@clientcoindivid", CBOGuest.SelectedValue)
        objCommand.Parameters.Add("@fromDate", txtFromDate.Text)
        objCommand.Parameters.Add("@toDate", txtToDate.Text)
        objCommand.Connection = oConnection

        objDataAdapter = New SqlDataAdapter(objCommand)
        objDataAdapter.Fill(objDataSet)
        objDataSet.Tables(0).Columns.Add("Total")
        Dim i As Integer
        Dim decTotal As Decimal
        Dim decBasic As Decimal
        Dim decOther As Decimal
        Dim decParking As Decimal
        Dim decTaxes As Decimal

        For i = 0 To (objDataSet.Tables(0).Rows.Count - 1)
            decBasic = Convert.ToDecimal(objDataSet.Tables(0).Rows(i)(3))
            If (objDataSet.Tables(0).Rows(i)(4).ToString() = "") Then
                decOther = 0
            Else
                decOther = Convert.ToDecimal(objDataSet.Tables(0).Rows(i)(4))
            End If
            'decOther = Convert.ToDecimal(objDataSet.Tables(0).Rows(i)(7) = "" ? "0" : objDataSet.Tables(0).Rows(i)(7))
            decParking = Convert.ToDecimal(objDataSet.Tables(0).Rows(i)(5))
            decTaxes = Convert.ToDecimal(objDataSet.Tables(0).Rows(i)(6))
            decTotal = decBasic + decOther + decParking + decTaxes

            objDataSet.Tables(0).Rows(i)(7) = decTotal
        Next

        objDataSet.AcceptChanges()

        dgSummary.DataSource = objDataSet 'objQuery.ExecuteProcedure("ProcCarWiseRevenueRPT")
        dgSummary.DataBind()
    End Sub

    Private Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("../index/CP_index.aspx?ID=43")
    End Sub

    Private Sub btnExportToExcel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportToExcel.Click
        context.Response.Buffer = True
        context.Response.ClearContent()
        context.Response.ClearHeaders()
        context.Response.ContentType = "application/vnd.ms-excel"
        EnableViewState = True
        Dim tw As New System.IO.StringWriter
        Dim hw As New System.Web.UI.HtmlTextWriter(tw)

        dgSummary.RenderControl(hw)
        Context.Response.Write(tw.ToString())
        Context.Response.Flush()
        context.Response.Close()
        context.Response.End()
    End Sub

    Private Sub CBOCompany_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CBOCompany.SelectedIndexChanged
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility
        If CBOUnit.SelectedValue <> " " Then
            CBOGuest.DataSource = objAcessdata.funcGetSQLDataReader("select distinct(c.clientcoindivid),c.fname + ' ' + c.lname as fname from corintclientcoindivmaster c,corintclientcomaster d where c.clientcoid=d.clientcoid and d.clientcoid=" & CBOCompany.SelectedValue & " order by fname")
            CBOGuest.DataValueField = "clientcoindivid"
            CBOGuest.DataTextField = "fname"
            CBOGuest.DataBind()
            CBOGuest.Items.Insert(0, New ListItem("", ""))
        Else
            CBOGuest.DataSource = ""
            CBOGuest.DataBind()
        End If
    End Sub
End Class
