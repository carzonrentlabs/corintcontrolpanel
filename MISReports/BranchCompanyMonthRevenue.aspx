<%@ Page Language="vb" AutoEventWireup="false" src="BranchCompanyMonthRevenue.aspx.vb" Inherits="BranchCompanyMonthRevenue" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>BranchCompanyMonthRevenue</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../JScripts/Datefunc.js"></script>
		<script language="JavaScript" src="../utilityfunction.js"></script>
		<script language="javascript">
		function validate()
		{
			// Check for mandatory values
			document.getElementById('hidButtonStatus').value = "Y";
			var strvalues
			strvalues = ('cboCity, txtFromDate, txtToDate')
			return checkmandatory(strvalues);
		}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<table width="780" align="center">
				<TBODY>
					<tr>
						<td><IMG src="../images/logo_carzonret.gif"></td>
						<td align="right"><IMG src="../images/logo_hertz.gif" width="100" align="right"></td>
					</tr>
					<tr>
						<td class="subRedHead" align="center" colSpan="2">Branch Wise, Month Wise Revenue 
							Report</td>
					</tr>
					<tr>
						<td colSpan="2">
							<hr>
						</td>
					</tr>
					<tr>
						<td colSpan="2">
							<table cellSpacing="0" cellPadding="0" width="100%" border="0">
								<TBODY>
									<tr bgColor="#ffe5e0">
										<td>&nbsp;</td>
										<td>City Name *</td>
										<td><asp:dropdownlist id="cboCity" runat="server"></asp:dropdownlist></td>
										<td>Month</td>
										<td><SELECT id="cboMonth" runat="server">
												<OPTION selected value="4">Apr</OPTION>
												<option value="5">May</option>
												<option value="6">Jun</option>
												<option value="7">Jul</option>
												<option value="8">Aug</option>
												<option value="9">Sep</option>
												<option value="10">Oct</option>
												<option value="11">Nov</option>
												<option value="12">Dec</option>
												<option value="1">Jan</option>
												<option value="2">Feb</option>
												<option value="3">Mar</option>
											</SELECT>
										</td>
										<td>Year</td>
										<td><SELECT id="cboYear" runat="server">
												<OPTION selected value="2007">2007</OPTION>
												<option value="2006">2006</option>
												<option value="2005">2005</option>
												<option value="2004">2004</option>
												<option value="2003">2003</option>
												<option value="2002">2002</option>
												<option value="2001">2001</option>
												<option value="2000">2000</option>
											</SELECT></td>
									</tr>
									<tr bgColor="#ffe5e0">
										<td colSpan="3">&nbsp;</td>
										<td colSpan="4"><asp:button id="btnProceed" runat="server" Text="Proceed"></asp:button>&nbsp;
											<asp:button id="btnExportToExcel" runat="server" Text="Export to Excel"></asp:button>&nbsp;
											<asp:button id="btnBack" runat="server" Text="Return to Menu"></asp:button></td>
									</tr>
								</TBODY>
							</table>
						</td>
					</tr>
					<tr>
						<td align="center" colSpan="2">
							<div style="OVERFLOW-Y: scroll; OVERFLOW-X: scroll; WIDTH: 780px; HEIGHT: 400px; visible: "
								noWrap><asp:datagrid id="dgSummary" runat="server" Width="1000" CellSpacing="2" CellPadding="0" AutoGenerateColumns="False"
									CssClass="GridFixedHeader">
									<SelectedItemStyle Wrap="False"></SelectedItemStyle>
									<EditItemStyle Wrap="False"></EditItemStyle>
									<HeaderStyle Wrap="False" HorizontalAlign="Center" CssClass="GridFixedHeader" BackColor="#ffe5ba"></HeaderStyle>
									<Columns>
										<asp:BoundColumn></asp:BoundColumn>
										<asp:BoundColumn DataField="ServiceUnit" HeaderText="Branch"></asp:BoundColumn>
										<asp:BoundColumn DataField="CompanyName" HeaderText="Company Name"></asp:BoundColumn>
										<asp:BoundColumn DataField="GuestName" HeaderText="Guest Name"></asp:BoundColumn>
										<asp:BoundColumn DataField="Car" HeaderText="Car"></asp:BoundColumn>
										<asp:BoundColumn DataField="Model" HeaderText="Model"></asp:BoundColumn>
										<asp:BoundColumn DataField="TodayRevenue" HeaderText="Today">
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="MTDCRevenue" HeaderText="MTD Current Year">
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="YTDCRevenue" HeaderText="YTD Current Year">
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="MTDLRevenue" HeaderText="MTD Last Year">
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="YTDLRevenue" HeaderText="YTD Last Year">
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
										</asp:BoundColumn>
									</Columns>
								</asp:datagrid></div>
						</td>
					</tr>
				</TBODY>
			</table>
		</form>
	</body>
</HTML>
