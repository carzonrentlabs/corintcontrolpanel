'/* 
' * Developed By :		Vivek Sharma on 19th June '07
' * Description	:		
' */
Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.text
Imports System

Public Class CarParking
    Inherits System.Web.UI.Page
    Public objDataReader As SqlDataReader
    Public objDataAdapter As SqlDataAdapter
    Public objDataSet As DataSet
    Public objCommand As SqlCommand
    Protected WithEvents hidVendorStatus As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents hidButtonStatus As System.Web.UI.HtmlControls.HtmlInputHidden
    Public oConnection As SqlConnection

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtFromDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtToDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnProceed As System.Web.UI.WebControls.Button
    Protected WithEvents btnExportToExcel As System.Web.UI.WebControls.Button
    Protected WithEvents btnBack As System.Web.UI.WebControls.Button
    Protected WithEvents dgSummary As System.Web.UI.WebControls.DataGrid
    Protected WithEvents cboCity As System.Web.UI.WebControls.DropDownList
    Protected WithEvents chkOwnCar As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkVendorCar As System.Web.UI.WebControls.CheckBox
    Protected WithEvents cboOwnCar As System.Web.UI.WebControls.DropDownList
    Protected WithEvents cboVendorName As System.Web.UI.WebControls.DropDownList
    Protected WithEvents cboVendorCar As System.Web.UI.WebControls.DropDownList

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        btnProceed.Attributes("onClick") = "return validate();"
        If Not Page.IsPostBack Then
            ' Call function to fill the values in combo
            populateddl()

            ' Show grid header
            dgSummary.DataSource = ""
            dgSummary.DataBind()
        End If

        If hidButtonStatus.Value = "N" Then
            If chkOwnCar.Checked Then
                cboOwnCar.Enabled = True
                fillOwnCar()
            Else
                cboOwnCar.Enabled = False
                cboOwnCar.DataSource = ""
                cboOwnCar.DataBind()
            End If

            If chkVendorCar.Checked Then
                cboVendorCar.Enabled = True
                cboVendorName.Enabled = True
                If hidVendorStatus.Value = "0" Then
                    fillVendor()
                End If
            Else
                cboVendorCar.Enabled = False
                cboVendorName.Enabled = False

                cboVendorCar.DataSource = ""
                cboVendorCar.DataBind()

                cboVendorName.DataSource = ""
                cboVendorName.DataBind()

                hidVendorStatus.Value = "0"
            End If
        End If
    End Sub

    Sub populateddl()
        Dim strQuery As StringBuilder
        Dim dtrreader1 As SqlDataReader
        Dim objAcessdata1 As clsutility
        objAcessdata1 = New clsutility

        Dim dtrreader2 As SqlDataReader
        Dim objAcessdata2 As clsutility
        objAcessdata2 = New clsutility

        dtrreader1 = objAcessdata1.funcGetSQLDataReader("SELECT a.AccessType, a.UnitID, b.UnitCityID, c.Region FROM CORIntSysUsersMaster as a, CORIntUnitMaster as b, CORIntUnitCityMaster as c WHERE a.UnitID = b.UnitID and b.UnitCityID = c.UnitCityID and a.SysUserID = " & Session("loggedin_user"))

        strQuery = New StringBuilder("SELECT DISTINCT d.CityID, d.CityName FROM CORIntSysUsersMaster as a, CORIntUnitMaster as b, CORIntUnitCityMaster as c, CORIntCityMaster as d WHERE a.UnitID = b.UnitID and b.UnitCityID = c.UnitCityID and c.CityName = d.CityName and d.Active = 1 and d.CityName <> '' ")
        While dtrreader1.Read
            ' When Access type is Service Unit or City
            If (dtrreader1("AccessType") = "SU" Or dtrreader1("AccessType") = "CT") Then
                strQuery.Append(" and c.UnitCityID = " & dtrreader1("UnitCityID"))
            End If

            ' When Access type is Region
            If (dtrreader1("AccessType") = "RN") Then
                strQuery.Append(" and c.Region = '" & dtrreader1("Region") & "' ")
            End If

        End While
        strQuery.Append(" ORDER BY d.CityName ")
        cboCity.DataSource = objAcessdata2.funcGetSQLDataReader(strQuery.ToString)
        cboCity.DataValueField = "CityID"
        cboCity.DataTextField = "CityName"
        cboCity.DataBind()

        objAcessdata1.Dispose()
        objAcessdata2.Dispose()

        hidVendorStatus.Value = "0"
    End Sub

    Sub fillOwnCar()
        Dim strQuery As StringBuilder
        Dim dtrreader As SqlDataReader
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility

        strQuery = New StringBuilder("SELECT  a.CarID, a.RegnNo FROM CORIntCarMaster as a, CORIntUnitMaster as b, CORIntUnitCityMaster as c, CORIntCityMaster as d WHERE a.UnitID = b.UnitID and b.UnitCityID = c.UnitCityID and c.CityName = d.CityName and a.Active = 1 and d.CityID = " & cboCity.SelectedValue & " ORDER BY a.RegnNo ")

        cboOwnCar.DataSource = objAcessdata.funcGetSQLDataReader(strQuery.ToString)
        cboOwnCar.DataValueField = "CarID"
        cboOwnCar.DataTextField = "RegnNo"
        cboOwnCar.DataBind()

        objAcessdata.Dispose()
    End Sub

    Sub fillVendor()
        Dim strQuery As StringBuilder
        Dim dtrreader As SqlDataReader
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility

        strQuery = New StringBuilder("SELECT a.CarVendorID, a.CarVendorName FROM CORIntCarVendorMaster as a, CORIntCityMaster as b WHERE a.CarVendorCityID = b.CityID and b.CityID = " & cboCity.SelectedValue & " ORDER BY a.CarVendorName ")

        cboVendorName.DataSource = objAcessdata.funcGetSQLDataReader(strQuery.ToString)
        cboVendorName.DataValueField = "CarVendorID"
        cboVendorName.DataTextField = "CarVendorName"
        cboVendorName.DataBind()

        objAcessdata.Dispose()
        hidVendorStatus.Value = "1"
        fillVendorCar()
    End Sub

    Sub fillVendorCar()
        Dim strQuery As StringBuilder
        Dim dtrreader As SqlDataReader
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility

        strQuery = New StringBuilder("SELECT a.VendorCarID, a.RegnNo FROM CORIntVendorCarMaster as a WHERE a.CarVendorID = " & cboVendorName.SelectedValue & " ORDER BY RegnNo ")

        cboVendorCar.DataSource = objAcessdata.funcGetSQLDataReader(strQuery.ToString)
        cboVendorCar.DataValueField = "VendorCarID"
        cboVendorCar.DataTextField = "RegnNo"
        cboVendorCar.DataBind()

        objAcessdata.Dispose()
    End Sub

    Private Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("../index/CP_index.aspx?ID=43")
    End Sub

    Private Sub cboCity_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCity.SelectedIndexChanged
        If chkOwnCar.Checked Then
            fillOwnCar()
        End If

        If chkVendorCar.Checked Then
            cboVendorCar.DataSource = ""
            cboVendorCar.DataBind()
            fillVendor()
        End If
    End Sub

    Private Sub cboVendorName_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboVendorName.SelectedIndexChanged
        fillVendorCar()
    End Sub

    ' /* Execute procedure with procedure name as argument */
    Public Function ExecuteDSProcedure(ByVal procedureName As String) As DataSet
        ' // Create object for calling connection method
        Dim objUtility As clsutility
        objUtility = New clsutility

        Dim oConnection As SqlConnection
        oConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))

        ' // Open connection
        objCommand = New SqlCommand
        objCommand.Connection = oConnection
        oConnection.Open()
        objCommand.CommandTimeout = 0

        Dim intCarID As Integer
        Dim intVendorID As Integer
        Dim intVendorCarID As Integer

        If chkOwnCar.Checked Then
            intCarID = Convert.ToInt32(cboOwnCar.SelectedValue)
        Else
            intCarID = 0
        End If

        If chkVendorCar.Checked Then
            intVendorID = Convert.ToInt32(cboVendorName.SelectedValue)
            intVendorCarID = Convert.ToInt32(cboVendorCar.SelectedValue)
        Else
            intVendorID = 0
            intVendorCarID = 0
        End If

        ' // Set value for executing stored procedure
        objCommand.CommandType = CommandType.StoredProcedure
        objCommand.CommandText = procedureName
        objCommand.Parameters.Add("@CityID", cboCity.SelectedValue)
        objCommand.Parameters.Add("@OwnCarID", intCarID)
        objCommand.Parameters.Add("@VendorID", intVendorID)
        objCommand.Parameters.Add("@VendorCarID", intVendorCarID)
        objCommand.Parameters.Add("@FromDate", txtFromDate.Text)
        objCommand.Parameters.Add("@ToDate", txtToDate.Text)
        objCommand.Connection = oConnection

        ' // Create Data Adapter object and fill value in that
        objDataSet = New DataSet
        objDataAdapter = New SqlDataAdapter(objCommand)
        objDataAdapter.Fill(objDataSet)

        oConnection.Close()
        objCommand.Dispose()
        Return objDataSet
    End Function

    Private Sub btnProceed_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProceed.Click
        ' Create DataSet object for storing DB result
        objDataSet = New DataSet
        objDataSet = ExecuteDSProcedure("Proc_CarParkingRPT")

        dgSummary.DataSource = objDataSet
        dgSummary.DataBind()
        hidButtonStatus.Value = "N"
    End Sub

    Private Sub btnExportToExcel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportToExcel.Click
        'export to excel
        context.Response.Buffer = True
        context.Response.ClearContent()
        context.Response.ClearHeaders()
        context.Response.ContentType = "application/vnd.ms-excel"
        EnableViewState = True
        Dim tw As New System.IO.StringWriter
        Dim hw As New System.Web.UI.HtmlTextWriter(tw)

        dgSummary.RenderControl(hw)
        Context.Response.Write(tw.ToString())
        Context.Response.Flush()
        context.Response.Close()
        context.Response.End()
    End Sub
End Class
