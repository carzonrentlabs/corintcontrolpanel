<%@ Page Language="vb" AutoEventWireup="false" src="CarParking.aspx.vb" Inherits="CarParking" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
  <HEAD>
		<title>CarParking</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../JScripts/Datefunc.js"></script>
		<script language="JavaScript" src="../utilityfunction.js"></script>
		<script language="javascript">
		function validate()
		{
			// Check for mandatory values
			document.getElementById('hidButtonStatus').value = "Y";
			var strvalues
			strvalues = ('cboCity, txtFromDate, txtToDate')
			return checkmandatory(strvalues);
		}
		</script>
</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<table width="780" align="center">
				<TBODY>
					<tr>
						<td><IMG src="../images/logo_carzonret.gif"></td>
						<td align="right"><IMG src="../images/logo_hertz.gif" width="100" align="right"></td>
					</tr>
					<tr>
						<td class="subRedHead" align="center" colSpan="2">Car Parking Detail Report</td>
					</tr>
					<tr>
						<td colSpan="2">
							<hr>
						</td>
					</tr>
					<tr>
						<td colSpan="2">
							<table cellSpacing="0" cellPadding="0" width="100%" border="0">
								<TBODY>
									<tr bgColor="#ffe5e0">
										<td>&nbsp;</td>
										<td>City Name *</td>
										<td colSpan="4"><asp:dropdownlist id="cboCity" runat="server" AutoPostBack="True"></asp:dropdownlist></td>
									</tr>
									<tr bgColor="#ffe5e0">
										<td colSpan="4">&nbsp;</td>
										<td>Vendor Name</td>
										<td>Vendor Car</td>
									</tr>
									<tr bgColor="#ffe5e0">
										<td style="HEIGHT: 23px">&nbsp;</td>
										<td style="HEIGHT: 23px"><asp:checkbox id="chkOwnCar" runat="server" Text="Own Car" AutoPostBack="True"></asp:checkbox></td>
										<td style="HEIGHT: 23px"><asp:dropdownlist id="cboOwnCar" runat="server"></asp:dropdownlist></td>
										<td style="HEIGHT: 23px"><asp:checkbox id="chkVendorCar" runat="server" Text="Vendor Car" AutoPostBack="True"></asp:checkbox></td>
										<td style="HEIGHT: 23px"><asp:dropdownlist id="cboVendorName" runat="server" AutoPostBack="True"></asp:dropdownlist></td>
										<td style="HEIGHT: 24px"><asp:dropdownlist id="cboVendorCar" runat="server" EnableViewState="True"></asp:dropdownlist></td>
									</tr>
									<tr bgColor="#ffe5e0">
										<td>&nbsp;</td>
										<td>Date-In Date [From] *</td>
										<td vAlign="top"><asp:textbox id="txtFromDate" runat="server" ReadOnly Width="80"></asp:textbox><A onmouseover="window.status='Date Picker';return true;" onmouseout="window.status='';return true;"
												href="javascript:show_calendar('Form1.txtFromDate');"><IMG height="21" src="../images/show-calendar.gif" width="24" border="0"></A></td>
										<td>[To]*</td>
										<td vAlign="top" colSpan="2"><asp:textbox id="txtToDate" runat="server" ReadOnly Width="80"></asp:textbox><A onmouseover="window.status='Date Picker';return true;" onmouseout="window.status='';return true;"
												href="javascript:show_calendar('Form1.txtToDate');"><IMG height="21" src="../images/show-calendar.gif" width="24" border="0"></A></td>
									</tr>
									<tr bgColor="#ffe5e0">
										<td colSpan="3">&nbsp;</td>
										<td colSpan="3"><asp:button id="btnProceed" runat="server" Text="Proceed"></asp:button>&nbsp;
											<asp:button id="btnExportToExcel" runat="server" Text="Export to Excel"></asp:button>&nbsp;
											<asp:button id="btnBack" runat="server" Text="Return to Menu"></asp:button></td>
									</tr>
								</TBODY>
							</table>
						</td>
					</tr>
					<tr>
						<td align="center" colSpan="2">
							<div style="OVERFLOW-Y: scroll; OVERFLOW-X: scroll; WIDTH: 780px; HEIGHT: 400px; visible: "
								noWrap><asp:datagrid id="dgSummary" runat="server" Width="100%" CellSpacing="2" CellPadding="0" AutoGenerateColumns="False"
									CssClass="GridFixedHeader">
									<SelectedItemStyle Wrap="False"></SelectedItemStyle>
									<EditItemStyle Wrap="False"></EditItemStyle>
									<HeaderStyle Wrap="False" HorizontalAlign="Center" CssClass="GridFixedHeader" BackColor="#ffe5ba"></HeaderStyle>
									<Columns>
										<asp:BoundColumn></asp:BoundColumn>
										<asp:BoundColumn DataField="CityName" HeaderText="Branch"></asp:BoundColumn>
										<asp:BoundColumn DataField="VendorName" HeaderText="Vendor Name"></asp:BoundColumn>
										<asp:BoundColumn DataField="DSNo" HeaderText="DS No."></asp:BoundColumn>
										<asp:BoundColumn DataField="DateIn" HeaderText="Date" DataFormatString="{0:dd-MM-yyyy}"></asp:BoundColumn>
										<asp:BoundColumn DataField="ParkTollChages" HeaderText="Amount">
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="VCNID" HeaderText="Credit Note No">
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
										</asp:BoundColumn>
									</Columns>
								</asp:datagrid></div>
						</td>
					</tr>
				</TBODY>
			</table>
			<input id="hidVendorStatus" type="hidden" value="0" runat="server"> <input type="hidden" id="hidButtonStatus" value="N" runat="server">
		</form>
	</body>
</HTML>
