'/* 
' * Developed By :		Vivek Sharma on 17th May '07
' * Description	:		
' */
Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.text
Imports System

Public Class detailedTransWiseRPT
    Inherits System.Web.UI.Page
    Public objDataReader As SqlDataReader
    Public objDataAdapter As SqlDataAdapter
    Public objDataSet As DataSet
    Public objCommand As SqlCommand
    Protected WithEvents cboUnit As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtFromDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtToDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnProceed As System.Web.UI.WebControls.Button
    Protected WithEvents PageSize As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents CurrentPage As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents TotalSize As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents btnExportToExcel As System.Web.UI.WebControls.Button
    Public oConnection As SqlConnection

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btnBack As System.Web.UI.WebControls.Button
    Protected WithEvents dgSummary As System.Web.UI.WebControls.DataGrid

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' Declare 
        btnProceed.Attributes("onClick") = "return validate();"
        If Not Page.IsPostBack Then
            ' Call function to fill the values in combo
            populateddl()

            ' Show grid header
            dgSummary.VirtualItemCount = 2000
            dgSummary.DataSource = ""
            dgSummary.DataBind()
        End If
    End Sub

    Sub populateddl()
        Dim strQuery As StringBuilder
        Dim dtrreader1 As SqlDataReader
        Dim objAcessdata1 As clsutility
        objAcessdata1 = New clsutility

        Dim dtrreader2 As SqlDataReader
        Dim objAcessdata2 As clsutility
        objAcessdata2 = New clsutility

        dtrreader1 = objAcessdata1.funcGetSQLDataReader("SELECT a.AccessType, a.UnitID, b.UnitCityID, c.Region FROM CORIntSysUsersMaster as a, CORIntUnitMaster as b, CORIntUnitCityMaster as c WHERE a.UnitID = b.UnitID and b.UnitCityID = c.UnitCityID and a.SysUserID = " & Session("loggedin_user"))

        strQuery = New StringBuilder("SELECT CityName, UnitCityID FROM CORIntUnitCityMaster WHERE Active = 1 and CityName <> '' ")
        While dtrreader1.Read
            ' When Access type is Service Unit or City
            If (dtrreader1("AccessType") = "SU" Or dtrreader1("AccessType") = "CT") Then
                strQuery.Append(" and UnitCityID = " & dtrreader1("UnitCityID"))
            End If

            ' When Access type is Region
            If (dtrreader1("AccessType") = "RN") Then
                strQuery.Append(" and Region = '" & dtrreader1("Region") & "' ")
            End If

        End While
        strQuery.Append(" ORDER BY CityName ")
        cboUnit.DataSource = objAcessdata2.funcGetSQLDataReader(strQuery.ToString)


        ' cboUnit.DataSource = objAcessdata2.funcGetSQLDataReader("SELECT CityName, UnitCityID FROM CORIntUnitCityMaster WHERE Active = 1 ORDER BY CityName")
        cboUnit.DataValueField = "UnitCityID"
        cboUnit.DataTextField = "CityName"
        cboUnit.DataBind()

        objAcessdata1.Dispose()
        objAcessdata2.Dispose()
    End Sub

    Sub setGridData()
        ' Create DataSet object for storing DB result
        'Dim objDataSet As DataSet
        objDataSet = New DataSet
        objDataSet = ExecuteDSProcedure("procDetailTransWiseRPT")

        ' Add column in dataset for calculation
        objDataSet.Tables(0).Columns.Add("Upgrade")         ' Column no. 51
        objDataSet.Tables(0).Columns.Add("Total")           ' Column no. 52
        objDataSet.Tables(0).Columns.Add("ServiceTax")      ' Column no. 53
        objDataSet.Tables(0).Columns.Add("VAT")             ' Column no. 54
        objDataSet.Tables(0).Columns.Add("TotalInvoice")    ' Column no. 55
        objDataSet.Tables(0).Columns.Add("CarHire")         ' Column no. 56

        ' Declare variable for calculation
        Dim i As Integer
        Dim decRevenue As Decimal
        Dim decParking As Decimal
        Dim decFuel As Decimal
        Dim decSevicePer As Decimal
        Dim decEduPer As Decimal
        Dim decSeviceTax As Decimal
        Dim decVat As Decimal
        Dim decTotal As Decimal
        Dim decTotalInvoice As Decimal
        Dim decVendorRevenue As Decimal
        Dim decCarHireAmount As Decimal
        Dim strCatBooked As String
        Dim strCatAllotted As String

        For i = 0 To (objDataSet.Tables(0).Rows.Count - 1)
            ' Check for car upgrade
            strCatBooked = Convert.ToString(objDataSet.Tables(0).Rows(i)(13))
            strCatAllotted = Convert.ToString(objDataSet.Tables(0).Rows(i)(14))
            If (strCatBooked = strCatAllotted) Then
                objDataSet.Tables(0).Rows(i)(51) = "No"
            Else
                objDataSet.Tables(0).Rows(i)(51) = "Yes"
            End If
            ' Get revenue, parking and fuel amount
            decRevenue = Convert.ToDecimal(objDataSet.Tables(0).Rows(i)(32))
            decParking = Convert.ToDecimal(objDataSet.Tables(0).Rows(i)(29))
            decFuel = Convert.ToDecimal(objDataSet.Tables(0).Rows(i)(31))
            decCarHireAmount = 0

            ' Calculate total amount (revenue + parking + fuel amount)
            decTotal = decRevenue + decParking + decFuel

            ' Get service, education and VAT taxes
            decSevicePer = decTotal * Convert.ToDecimal(objDataSet.Tables(0).Rows(i)(40) / 100)
            decEduPer = decSevicePer * Convert.ToDecimal(objDataSet.Tables(0).Rows(i)(41) / 100)
            decVat = decTotal * Convert.ToDecimal(objDataSet.Tables(0).Rows(i)(42) / 100)

            ' Add service tax and education tax
            decSeviceTax = decSevicePer + decEduPer

            ' Calculate Total Invoice amount (Toatl + Taxes)
            decTotalInvoice = decTotal + decSeviceTax + decVat

            ' Insert calculated values in dataset
            objDataSet.Tables(0).Rows(i)(52) = Math.Round(decTotal, 2)
            objDataSet.Tables(0).Rows(i)(53) = Math.Round(decSeviceTax, 2)
            objDataSet.Tables(0).Rows(i)(54) = Math.Round(decVat, 2)
            objDataSet.Tables(0).Rows(i)(55) = Math.Round(decTotalInvoice, 2)

            ' Calculate Car Hire Charge
            ' If it is a Vendor Car
            If (objDataSet.Tables(0).Rows(i)(43)) Then
                ' Check for Payment Option
                If (objDataSet.Tables(0).Rows(i)(46) = "Revenue Sharing") Then
                    decVendorRevenue = Convert.ToDecimal(objDataSet.Tables(0).Rows(i)(45) / 100)
                    decCarHireAmount = decRevenue * decVendorRevenue
                ElseIf (objDataSet.Tables(0).Rows(i)(46) = "Package-Wise") Then
                    decCarHireAmount = decRevenue
                End If
            End If
            objDataSet.Tables(0).Rows(i)(56) = Math.Round(decCarHireAmount, 2)
        Next

        objDataSet.AcceptChanges()

        'Dim objDataView As DataView
        'objDataView = New DataView(objDataSet.Tables(0))
        'objDataView.Sort=
        'dgSummary.VirtualItemCount = objDataSet.Tables(0).Rows.Count
        dgSummary.DataSource = objDataSet
        dgSummary.DataBind()
    End Sub

    ' /* Execute procedure with procedure name as argument */
    Public Function ExecuteDSProcedure(ByVal procedureName As String) As DataSet
        ' // Create object for calling connection method
        Dim objUtility As clsutility
        objUtility = New clsutility

        Dim oConnection As SqlConnection
        oConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))

        ' // Open connection
        objCommand = New SqlCommand
        objCommand.Connection = oConnection
        oConnection.Open()
        objCommand.CommandTimeout = 0

        ' // Set value for executing stored procedure
        objCommand.CommandType = CommandType.StoredProcedure
        objCommand.CommandText = procedureName
        objCommand.Parameters.Add("@UnitCityID", cboUnit.SelectedValue)
        objCommand.Parameters.Add("@PickUpFrom", txtFromDate.Text)
        objCommand.Parameters.Add("@PickUpTo", txtToDate.Text)
        objCommand.Connection = oConnection

        ' // Create Data Adapter object and fill value in that
        objDataSet = New DataSet
        objDataAdapter = New SqlDataAdapter(objCommand)
        objDataAdapter.Fill(objDataSet)

        oConnection.Close()
        objCommand.Dispose()
        Return objDataSet
    End Function

    Private Sub btnProceed_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProceed.Click
        setGridData()
    End Sub

    'Sub dgSummary_pageIndexChanged(ByVal s As Object, ByVal e As DataGridPageChangedEventArgs)
    '    dgSummary.CurrentPageIndex = e.NewPageIndex
    '    setGridData()
    'End Sub

    Private Sub btnExportToExcel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportToExcel.Click
        'export to excel
        context.Response.Buffer = True
        context.Response.ClearContent()
        context.Response.ClearHeaders()
        context.Response.ContentType = "application/vnd.ms-excel"
        EnableViewState = True
        Dim tw As New System.IO.StringWriter
        Dim hw As New System.Web.UI.HtmlTextWriter(tw)

        dgSummary.RenderControl(hw)
        Context.Response.Write(tw.ToString())
        Context.Response.Flush()
        context.Response.Close()
        context.Response.End()
    End Sub

    Private Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("../index/CP_index.aspx?ID=43")
    End Sub
End Class
