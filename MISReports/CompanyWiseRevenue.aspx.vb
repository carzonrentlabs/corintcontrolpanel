'/* 
'* Developed By :		Deepti Thukral on 21st May '07
'* Description	:		
'*/
Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.text
Imports System
Public Class CompanyWiseRevenue
    Inherits System.Web.UI.Page
    Public objDataReader As SqlDataReader
    Public objDataAdapter As SqlDataAdapter
    Public objDataSet As DataSet
    Public objCommand As SqlCommand
    Protected WithEvents CBOUnit As System.Web.UI.WebControls.DropDownList
    Protected WithEvents cboModel As System.Web.UI.WebControls.DropDownList
    Protected WithEvents cboVendor As System.Web.UI.WebControls.DropDownList
    Protected WithEvents cboCategory As System.Web.UI.WebControls.DropDownList
    Protected WithEvents btnSearch As System.Web.UI.WebControls.Button
    Protected WithEvents btnproceed As System.Web.UI.WebControls.Button
    Protected WithEvents CBOCompany As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtFromDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtToDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnExportToExcel As System.Web.UI.WebControls.Button
    Protected WithEvents cboTemp As System.Web.UI.WebControls.DropDownList
    Protected WithEvents LblCompany As System.Web.UI.WebControls.Label
    Public oConnection As SqlConnection

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btnBack As System.Web.UI.WebControls.Button
    Protected WithEvents dgSummary As System.Web.UI.WebControls.DataGrid

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btnproceed.Attributes("onClick") = "return checkbeforenext();"
        If Not Page.IsPostBack Then
            populateddl()
            dgSummary.DataSource = ""
            dgSummary.DataBind()
            ' setGridData()
        End If
    End Sub
    Sub populateddl()
        Dim isHQ As Boolean
        Dim strQuery As StringBuilder
        Dim dtrreader1 As SqlDataReader
        Dim objAcessdata1 As clsutility
        objAcessdata1 = New clsutility

        Dim dtrreader2 As SqlDataReader
        Dim objAcessdata2 As clsutility
        objAcessdata2 = New clsutility
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility

        isHQ = False

        dtrreader1 = objAcessdata1.funcGetSQLDataReader("SELECT a.AccessType, a.UnitID, b.UnitCityID, c.Region FROM CORIntSysUsersMaster as a, CORIntUnitMaster as b, CORIntUnitCityMaster as c WHERE a.UnitID = b.UnitID and b.UnitCityID = c.UnitCityID and a.SysUserID = " & Session("loggedin_user"))

        strQuery = New StringBuilder("SELECT CityName, UnitCityID FROM CORIntUnitCityMaster WHERE Active = 1 and CityName <> '' ")
        While dtrreader1.Read
            ' When Access type is Service Unit or City
            If (dtrreader1("AccessType") = "SU" Or dtrreader1("AccessType") = "CT") Then
                strQuery.Append(" and UnitCityID = " & dtrreader1("UnitCityID"))
            End If

            ' When Access type is Region
            If (dtrreader1("AccessType") = "RN") Then
                strQuery.Append(" and Region = '" & dtrreader1("Region") & "' ")
            End If
            If (dtrreader1("AccessType") = "HQ") Then
                isHQ = True
            End If
        End While
        strQuery.Append(" ORDER BY CityName ")
        CBOUnit.DataSource = objAcessdata2.funcGetSQLDataReader(strQuery.ToString)
        'CBOUnit.DataSource = objAcessdata.funcGetSQLDataReader("select cityname,unitcityid from corintunitcitymaster where active = 1 and cityname <>'' order by cityname")
        CBOUnit.DataValueField = "unitcityid"
        CBOUnit.DataTextField = "cityname"
        CBOUnit.DataBind()
        CBOUnit.Items.Insert(0, New ListItem("All", ""))
        

        CBOCompany.DataSource = objAcessdata.funcGetSQLDataReader("select clientcoid,clientconame from corintclientcomaster where active = 1 order by clientconame")
        CBOCompany.DataValueField = "clientcoid"
        CBOCompany.DataTextField = "clientconame"
        CBOCompany.DataBind()
       
        CBOCompany.Items.Insert(0, New ListItem("All", ""))
    End Sub
    Sub setGridData()
        'Dim objQuery As clsQuery
        'objQuery = New clsQuery
        Dim objDataSet As DataSet
        objDataSet = New DataSet

        objDataSet = ExecuteDSProcedure("PROC_CompanywiseRevenueRPT")
        objDataSet.Tables(0).Columns.Add("Total")

        Dim i As Integer
        Dim decTotal As Decimal
        Dim decBasic As Decimal
        Dim decOther As Decimal
        Dim decParking As Decimal
        Dim decTaxes As Decimal

        For i = 0 To (objDataSet.Tables(0).Rows.Count - 1)
            decBasic = Convert.ToDecimal(objDataSet.Tables(0).Rows(i)(5))
            decOther = Convert.ToDecimal(objDataSet.Tables(0).Rows(i)(6))
            decParking = Convert.ToDecimal(objDataSet.Tables(0).Rows(i)(7))
            decTaxes = Convert.ToDecimal(objDataSet.Tables(0).Rows(i)(8))
            decTotal = decBasic + decOther + decParking + decTaxes

            objDataSet.Tables(0).Rows(i)(9) = decTotal
        Next

        objDataSet.AcceptChanges()

        dgSummary.DataSource = objDataSet 'objQuery.ExecuteProcedure("ProcCarWiseRevenueRPT")
        dgSummary.DataBind()
    End Sub
    ' /* Execute procedure with procedure name as argument */
    Public Function ExecuteDSProcedure(ByVal procedureName As String) As DataSet
        ' // Create object for calling connection method
        Dim objUtility As clsutility
        objUtility = New clsutility

        'objUtility = New clsutility
        'oConnection = New SqlConnection

        Dim oConnection As SqlConnection
        oConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))

        ' // Open connection
        objCommand = New SqlCommand
        objCommand.Connection = oConnection
        objCommand.CommandTimeout = 0


        oConnection.Open()


        ' // Set value for executing stored procedure
        objCommand.CommandType = CommandType.StoredProcedure
        objCommand.CommandText = procedureName
        objCommand.Connection = oConnection

        ' // Create Data Adapter object and fill value in that
        objDataSet = New DataSet
        objDataAdapter = New SqlDataAdapter(objCommand)
        objDataAdapter.Fill(objDataSet)

        '// objDataReader = objCommand.ExecuteReader(CommandBehavior.CloseConnection)
        Return objDataSet
    End Function


    Private Sub btnproceed_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnproceed.Click
        Dim oConnection As SqlConnection
        oConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim objDataSet As DataSet
        objDataSet = New DataSet

        oConnection.Open()
        ' objDataSet = ExecuteDSProcedure("PROC_OwnedCar")
        'objDataSet1 = ExecuteDSProcedure("PROC_HiredCarNonDTO")
        'objDataSet2 = ExecuteDSProcedure("PROC_HiredCarDTO")

        objCommand = New SqlCommand
        objCommand.CommandType = CommandType.StoredProcedure
        objCommand.CommandText = "PROC_CompanywiseRevenueRPT"
        objCommand.Parameters.Add("@UnitCityId", CBOUnit.SelectedValue)
        objCommand.Parameters.Add("@Clientid", CBOCompany.SelectedValue)
        objCommand.Parameters.Add("@fromDate", txtFromDate.Text)
        objCommand.Parameters.Add("@toDate", txtToDate.Text)
        objCommand.Parameters.Add("@temp", cboTemp.SelectedValue)
        objCommand.Connection = oConnection

        objDataAdapter = New SqlDataAdapter(objCommand)
        objDataAdapter.Fill(objDataSet)
        objDataSet.Tables(0).Columns.Add("Total")

        Dim i As Integer
        Dim decTotal As Decimal
        Dim decBasic As Decimal
        Dim decOther As Decimal
        Dim decParking As Decimal
        Dim decTaxes As Decimal

        For i = 0 To (objDataSet.Tables(0).Rows.Count - 1)
            decBasic = Convert.ToDecimal(objDataSet.Tables(0).Rows(i)(5))
            decOther = Convert.ToDecimal(objDataSet.Tables(0).Rows(i)(6))
            decParking = Convert.ToDecimal(objDataSet.Tables(0).Rows(i)(7))
            decTaxes = Convert.ToDecimal(objDataSet.Tables(0).Rows(i)(8))
            decTotal = decBasic + decOther + decParking + decTaxes

            objDataSet.Tables(0).Rows(i)(9) = decTotal
        Next

        objDataSet.AcceptChanges()

        dgSummary.DataSource = objDataSet 'objQuery.ExecuteProcedure("ProcCarWiseRevenueRPT")
        dgSummary.DataBind()
    End Sub

    Private Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("../index/CP_index.aspx?ID=43")
    End Sub

    Private Sub btnExportToExcel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportToExcel.Click
        'export to excel
        context.Response.Buffer = True
        context.Response.ClearContent()
        context.Response.ClearHeaders()
        context.Response.ContentType = "application/vnd.ms-excel"
        EnableViewState = True
        Dim tw As New System.IO.StringWriter
        Dim hw As New System.Web.UI.HtmlTextWriter(tw)

        dgSummary.RenderControl(hw)
        Context.Response.Write(tw.ToString())
        Context.Response.Flush()
        context.Response.Close()
        context.Response.End()
    End Sub

   
    Private Sub cboTemp_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTemp.SelectedIndexChanged
        If cboTemp.SelectedValue = "0" Then
            CBOCompany.Visible = False
            LblCompany.Visible = False
        Else
            CBOCompany.Visible = True
            LblCompany.Visible = True
        End If
    End Sub
End Class
