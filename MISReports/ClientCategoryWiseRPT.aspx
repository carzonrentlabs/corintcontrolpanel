<%@ Page Language="vb" AutoEventWireup="false" src="ClientCategoryWiseRPT.aspx.vb" Inherits="ClientCategoryWiseRPT"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>ClientCategoryWiseRPT</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="JavaScript" src="../utilityfunction.js"></script>
		<script language="JavaScript" src="../JScripts/Datefunc.js"></script>
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
		<script language="JavaScript">
		
			function checkbeforenext()
			{
			
			if(document.Form1.txtFromDate.value=="")
				{
					alert("Please select Date.")
					document.Form1.txtFromDate.focus();
					return false;
				}
			if(document.Form1.txtToDate.value=="")
				{
					alert("Please select Date.")
					document.Form1.txtToDate.focus();
					return false;
				}
		}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<table width="90%" align="center">
				<TBODY>
					<tr>
						<td><IMG src="../images/logo_carzonret.gif"></td>
						<td align="right"><IMG src="../images/logo_hertz.gif" width="100" align="right"></td>
					</tr>
					<tr>
						<td class="subRedHead" align="center" colSpan="2">Client Wise Category Wise Report</td>
					</tr>
					<tr>
						<td colSpan="2"></td>
					</tr>
					<tr bgColor="#ffe5e0">
						<td colSpan="2">
							<TABLE id="Table1" cellSpacing="2" cellPadding="1" width="100%" border="0">
								<TR>
									<TD align="right" width="20%">Client: *</TD>
									<TD style="WIDTH: 105px" align="left" width="105"><asp:dropdownlist id="CBOClient" runat="server" CssClass="input"></asp:dropdownlist></TD>
									<TD align="right" width="27%">From Date: *
										<asp:textbox id="txtFromDate" runat="server"></asp:textbox></TD>
									<TD align="left" width="3%"><A onmouseover="window.status='Date Picker';return true;" onmouseout="window.status='';return true;"
											href="javascript:show_calendar('Form1.txtFromDate');"><IMG height="21" src="../images/show-calendar.gif" width="24" border="0"></A></TD>
									<TD vAlign="middle" align="right" width="27%">To Date: *
										<asp:textbox id="txtToDate" runat="server"></asp:textbox></TD>
									<TD align="left" width="3%"><A onmouseover="window.status='Date Picker';return true;" onmouseout="window.status='';return true;"
											href="javascript:show_calendar('Form1.txtToDate');"><IMG height="21" src="../images/show-calendar.gif" width="24" border="0"></A></TD>
								</TR>
							</TABLE>
						</td>
					</tr>
					<tr bgColor="#ffe5e0">
						<td align="center" colSpan="2">&nbsp;
							<asp:button id="btnproceed" runat="server" CssClass="input" Text="Proceed"></asp:button><asp:button id="btnExportToExcel" runat="server" CssClass="input" Text="Export to Excel"></asp:button><asp:button id="btnBack" runat="server" Text="Return to Menu"></asp:button>&nbsp;&nbsp;&nbsp;</td>
					</tr>
					<tr>
						<td align="center" colSpan="2">
							<div noWrap><asp:datagrid id="dgSummary" runat="server" BorderStyle="None" CellPadding="0" AutoGenerateColumns="False"
									Width="100%">
									<SelectedItemStyle Wrap="False"></SelectedItemStyle>
									<EditItemStyle Wrap="False"></EditItemStyle>
									<HeaderStyle Wrap="False" HorizontalAlign="Center" Height="20px" CssClass="subRedHead" BackColor="#FFE5BA"></HeaderStyle>
									<Columns>
										<asp:BoundColumn></asp:BoundColumn>
										<asp:BoundColumn DataField="clientconame" HeaderText="Client">
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="carcatname" HeaderText="Category">
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="NoofBookings" HeaderText="No of Booking">
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="Total" HeaderText="Total">
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
										</asp:BoundColumn>
									</Columns>
								</asp:datagrid></div>
						</td>
					</tr>
				</TBODY>
			</table>
		</form>
	</body>
</HTML>
