'/* 
'* Developed By :		Deepti Thukral on 21st May '07
'* Description	:		
'*/
Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class OwnedHiredCarRevenue
    Inherits System.Web.UI.Page
    Public objDataReader As SqlDataReader
    Public objDataAdapter As SqlDataAdapter
    Public objDataAdapter1 As SqlDataAdapter
    Public objDataAdapter2 As SqlDataAdapter
    Public objDataSet As DataSet
    Public objCommand As SqlCommand
    Public objCommand1 As SqlCommand
    Public objCommand2 As SqlCommand
    Protected WithEvents cboUnit As System.Web.UI.WebControls.DropDownList
    Protected WithEvents cboModel As System.Web.UI.WebControls.DropDownList
    Protected WithEvents cboVendor As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtCategory As System.Web.UI.WebControls.DropDownList
    Protected WithEvents btnSearch As System.Web.UI.WebControls.Button
    Protected WithEvents dgReport As System.Web.UI.WebControls.DataGrid
    Protected WithEvents DgReport1 As System.Web.UI.WebControls.DataGrid
    Protected WithEvents txtFromDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtToDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents Button1 As System.Web.UI.WebControls.Button
    Protected WithEvents btnExportToExcel As System.Web.UI.WebControls.Button
    Protected WithEvents btnproceed As System.Web.UI.WebControls.Button
    Protected WithEvents Button2 As System.Web.UI.WebControls.Button
    Public oConnection As SqlConnection

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btnBack As System.Web.UI.WebControls.Button
    Protected WithEvents dgSummary As System.Web.UI.WebControls.DataGrid

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btnproceed.Attributes("onClick") = "return checkbeforenext();"
        If Not Page.IsPostBack Then
            populateddl()

            dgSummary.DataSource = ""
            dgSummary.DataBind()

            dgReport.DataSource = ""
            dgReport.DataBind()
            DgReport1.DataSource = ""
            DgReport1.DataBind()
        End If
    End Sub
    Sub populateddl()
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility

        txtCategory.DataSource = objAcessdata.funcGetSQLDataReader("select carcatname,carcatid from CORIntCarCatMaster where active = 1 order by carcatname")
        txtCategory.DataValueField = "carcatid"
        txtCategory.DataTextField = "carcatname"
        txtCategory.DataBind()
        txtCategory.Items.Insert(0, New ListItem("All", ""))
    End Sub
    Sub setGridData()
        Dim oConnection As SqlConnection
        oConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        'Dim objQuery As clsQuery
        'objQuery = New clsQuery
        Dim objDataSet As DataSet
        objDataSet = New DataSet
        Dim objDataSet1 As DataSet
        objDataSet1 = New DataSet
        Dim objDataSet2 As DataSet
        objDataSet2 = New DataSet
        Dim objCommand As SqlCommand
        oConnection.Open()
        ' objDataSet = ExecuteDSProcedure("PROC_OwnedCar")
        'objDataSet1 = ExecuteDSProcedure("PROC_HiredCarNonDTO")
        'objDataSet2 = ExecuteDSProcedure("PROC_HiredCarDTO")

        objCommand = New SqlCommand("PROC_OwnedCar", oConnection)
        objCommand.CommandType = CommandType.StoredProcedure
        objCommand.Parameters.Add("@CarCatId", txtCategory.SelectedValue)
        objCommand.Parameters.Add("@fromDate", txtFromDate.Text)
        objCommand.Parameters.Add("@toDate", txtToDate.Text)

        objCommand.ExecuteNonQuery()
        objCommand = New SqlCommand("PROC_HiredCarNonDTO", oConnection)
        objCommand.CommandType = CommandType.StoredProcedure
        objCommand.Parameters.Add("@CarCatId", txtCategory.SelectedValue)
        objCommand.Parameters.Add("@fromDate", txtFromDate.Text)
        objCommand.Parameters.Add("@toDate", txtToDate.Text)

        objCommand.ExecuteNonQuery()
        objCommand = New SqlCommand("PROC_HiredCarDTO", oConnection)
        objCommand.CommandType = CommandType.StoredProcedure
        objCommand.Parameters.Add("@CarCatId", txtCategory.SelectedValue)
        objCommand.Parameters.Add("@fromDate", txtFromDate.Text)
        objCommand.Parameters.Add("@toDate", txtToDate.Text)

        objCommand.ExecuteNonQuery()


        'dgSummary.DataSource = objDataSet 'objQuery.ExecuteProcedure("ProcCarWiseRevenueRPT")
        'dgSummary.DataBind()
        'dgReport.DataSource = objDataSet1 'objQuery.ExecuteProcedure("ProcCarWiseRevenueRPT")
        'dgReport.DataBind()
        'DgReport1.DataSource = objDataSet2 'objQuery.ExecuteProcedure("ProcCarWiseRevenueRPT")
        'DgReport1.DataBind()
    End Sub
    ' /* Execute procedure with procedure name as argument */
    Public Function ExecuteDSProcedure(ByVal procedureName As String) As DataSet
        ' // Create object for calling connection method
        Dim objUtility As clsutility
        objUtility = New clsutility

        'objUtility = New clsutility
        'oConnection = New SqlConnection

        Dim oConnection As SqlConnection
        oConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))

        ' // Open connection
        objCommand = New SqlCommand
        objCommand.Connection = oConnection
        objCommand.CommandTimeout = 0


        oConnection.Open()


        ' // Set value for executing stored procedure
        objCommand.CommandType = CommandType.StoredProcedure
        objCommand.CommandText = procedureName
        objCommand.Connection = oConnection

        ' // Create Data Adapter object and fill value in that
        objDataSet = New DataSet
        objDataAdapter = New SqlDataAdapter(objCommand)
        objDataAdapter.Fill(objDataSet)

        '// objDataReader = objCommand.ExecuteReader(CommandBehavior.CloseConnection)
        Return objDataSet
    End Function

    Private Sub btnproceed_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnproceed.Click
        Dim oConnection As SqlConnection
        oConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim objDataSet As DataSet
        objDataSet = New DataSet
        Dim objDataSet1 As DataSet
        objDataSet1 = New DataSet
        Dim objDataSet2 As DataSet
        objDataSet2 = New DataSet
        Dim objCommand As SqlCommand

        oConnection.Open()
        ' objDataSet = ExecuteDSProcedure("PROC_OwnedCar")
        'objDataSet1 = ExecuteDSProcedure("PROC_HiredCarNonDTO")
        'objDataSet2 = ExecuteDSProcedure("PROC_HiredCarDTO")

        objCommand = New SqlCommand
        objCommand.CommandType = CommandType.StoredProcedure
        objCommand.CommandText = "PROC_OwnedCar"
        objCommand.Parameters.Add("@CarCatId", txtCategory.SelectedValue)
        objCommand.Parameters.Add("@fromDate", txtFromDate.Text)
        objCommand.Parameters.Add("@toDate", txtToDate.Text)
        objCommand.Connection = oConnection

        objDataAdapter = New SqlDataAdapter(objCommand)
        objDataAdapter.Fill(objDataSet)

        objCommand1 = New SqlCommand
        objCommand1.CommandType = CommandType.StoredProcedure
        objCommand1.CommandText = "PROC_HiredCarNonDTO"
        objCommand1.Parameters.Add("@CarCatId", txtCategory.SelectedValue)
        objCommand1.Parameters.Add("@fromDate", txtFromDate.Text)
        objCommand1.Parameters.Add("@toDate", txtToDate.Text)
        objCommand1.Connection = oConnection

        objDataAdapter1 = New SqlDataAdapter(objCommand1)
        objDataAdapter1.Fill(objDataSet1)

        objCommand2 = New SqlCommand
        objCommand2.CommandType = CommandType.StoredProcedure
        objCommand2.CommandText = "PROC_HiredCarDTO"
        objCommand2.Parameters.Add("@CarCatId", txtCategory.SelectedValue)
        objCommand2.Parameters.Add("@fromDate", txtFromDate.Text)
        objCommand2.Parameters.Add("@toDate", txtToDate.Text)
        objCommand2.Connection = oConnection

        objDataAdapter2 = New SqlDataAdapter(objCommand2)
        objDataAdapter2.Fill(objDataSet2)



        'objCommand.ExecuteNonQuery()

        'objCommand.CommandType = CommandType.StoredProcedure
        'objCommand.Parameters.Add("@CarCatId", txtCategory.SelectedValue)


        'objCommand = New SqlCommand("PROC_HiredCarNonDTO", oConnection)
        'objCommand.CommandType = CommandType.StoredProcedure
        'objCommand.Parameters.Add("@CarCatId", txtCategory.SelectedValue)

        'objCommand.ExecuteNonQuery()
        'objCommand = New SqlCommand("PROC_HiredCarDTO", oConnection)
        'objCommand.CommandType = CommandType.StoredProcedure
        'objCommand.Parameters.Add("@CarCatId", txtCategory.SelectedValue)

        'objCommand.ExecuteNonQuery()
        dgSummary.DataSource = objDataSet 'objQuery.ExecuteProcedure("ProcCarWiseRevenueRPT")
        dgSummary.DataBind()
        dgReport.DataSource = objDataSet1 'objQuery.ExecuteProcedure("ProcCarWiseRevenueRPT")
        dgReport.DataBind()
        DgReport1.DataSource = objDataSet2 'objQuery.ExecuteProcedure("ProcCarWiseRevenueRPT")
        DgReport1.DataBind()
    End Sub


    Private Sub btnExportToExcel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportToExcel.Click
        'export to excel
        context.Response.Buffer = True
        context.Response.ClearContent()
        context.Response.ClearHeaders()
        context.Response.ContentType = "application/vnd.ms-excel"
        EnableViewState = True
        Dim tw As New System.IO.StringWriter
        Dim hw As New System.Web.UI.HtmlTextWriter(tw)

        dgSummary.RenderControl(hw)
        Context.Response.Write(tw.ToString())
        Context.Response.Flush()
        context.Response.Close()
        context.Response.End()
    End Sub
End Class
