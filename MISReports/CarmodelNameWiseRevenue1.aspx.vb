'/* 
'* Developed By :		Deepti Thukral on 31st May '07
'* Description	:		
'*/
Imports commonutility
'Imports queryClass
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Text
Imports System
Public Class CarModelNameWiseRevenue1

    Inherits System.Web.UI.Page
    Public objDataReader As SqlDataReader
    Public objDataAdapter As SqlDataAdapter
    Public objDataSet As DataSet
    Protected WithEvents btnExportToExcel As System.Web.UI.WebControls.Button
    Public objCommand As SqlCommand

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents CBOUnit As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtFromDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtToDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnproceed As System.Web.UI.WebControls.Button
    Protected WithEvents btnBack As System.Web.UI.WebControls.Button
    Protected WithEvents dgSummary As System.Web.UI.WebControls.DataGrid

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btnproceed.Attributes("onClick") = "return checkbeforenext();"
        If Not Page.IsPostBack Then
            populateddl()
            dgSummary.DataSource = ""
            dgSummary.DataBind()
            ' setGridData()
        End If
    End Sub
    Sub populateddl()
        Dim isHQ As Boolean
        Dim strQuery As StringBuilder
        Dim dtrreader1 As SqlDataReader
        Dim objAcessdata1 As clsutility
        objAcessdata1 = New clsutility

        Dim dtrreader2 As SqlDataReader
        Dim objAcessdata2 As clsutility
        objAcessdata2 = New clsutility

        isHQ = False

        dtrreader1 = objAcessdata1.funcGetSQLDataReader("SELECT a.AccessType, a.UnitID, b.UnitCityID, c.Region FROM CORIntSysUsersMaster as a, CORIntUnitMaster as b, CORIntUnitCityMaster as c WHERE a.UnitID = b.UnitID and b.UnitCityID = c.UnitCityID and a.SysUserID = " & Session("loggedin_user"))

        strQuery = New StringBuilder("SELECT CityName, UnitCityID FROM CORIntUnitCityMaster WHERE Active = 1 and CityName <> '' ")
        While dtrreader1.Read
            ' When Access type is Service Unit or City
            If (dtrreader1("AccessType") = "SU" Or dtrreader1("AccessType") = "CT") Then
                strQuery.Append(" and UnitCityID = " & dtrreader1("UnitCityID"))
            End If

            ' When Access type is Region
            If (dtrreader1("AccessType") = "RN") Then
                strQuery.Append(" and Region = '" & dtrreader1("Region") & "' ")
            End If
            If (dtrreader1("AccessType") = "HQ") Then
                isHQ = True
            End If
        End While
        strQuery.Append(" ORDER BY CityName ")
        CBOUnit.DataSource = objAcessdata2.funcGetSQLDataReader(strQuery.ToString)
        'CBOUnit.DataSource = objAcessdata.funcGetSQLDataReader("select cityname,unitcityid from corintunitcitymaster where active = 1 and cityname <>'' order by cityname")
        CBOUnit.DataValueField = "unitcityid"
        CBOUnit.DataTextField = "cityname"
        CBOUnit.DataBind()
        If isHQ Then
            CBOUnit.Items.Insert(0, New ListItem("All", ""))
        End If
    End Sub

    Private Sub btnproceed_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnproceed.Click
        Dim oConnection As SqlConnection
        oConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        'Dim objQuery As clsQuery
        'objQuery = New clsQuery
        Dim objDataSet As DataSet
        objDataSet = New DataSet
        'oConnection.ConnectionTimeout = 0
        oConnection.Open()

        ' objDataSet = ExecuteDSProcedure("PROC_OwnedCar")
        'objDataSet1 = ExecuteDSProcedure("PROC_HiredCarNonDTO")
        'objDataSet2 = ExecuteDSProcedure("PROC_HiredCarDTO")

        objCommand = New SqlCommand
        objCommand.CommandTimeout = 0
        objCommand.CommandType = CommandType.StoredProcedure
        objCommand.CommandText = "Proc_CarModelWiseRevenueRPT"
        objCommand.Parameters.Add("@UnitCityId", CBOUnit.SelectedValue)
        objCommand.Parameters.Add("@fromDate", txtFromDate.Text)
        objCommand.Parameters.Add("@toDate", txtToDate.Text)
        objCommand.Connection = oConnection

        objDataAdapter = New SqlDataAdapter(objCommand)
        objDataAdapter.Fill(objDataSet)

        Dim i As Integer
        Dim strquery As String
        Dim city As String
        Dim duty As String
        Dim count As String
        Dim model As String
        Dim amount As String

        strquery = "delete from tempCarModelWise"
        objCommand = New SqlCommand(strquery.ToString, oConnection)
        objCommand.ExecuteNonQuery()

        For i = 0 To (objDataSet.Tables(0).Rows.Count - 1)
            city = Convert.ToString(objDataSet.Tables(0).Rows(i)(0))
            duty = Convert.ToString(objDataSet.Tables(0).Rows(i)(1))
            count = Convert.ToString(objDataSet.Tables(0).Rows(i)(2))
            model = Convert.ToString(objDataSet.Tables(0).Rows(i)(3))
            amount = Convert.ToString(objDataSet.Tables(0).Rows(i)(4))

            strquery = "Insert into tempCarModelWise (city,duty,count,model,amount) values('" & city & "','" & duty & "','" & count & "','" & model & "','" & amount & "')"
            objCommand = New SqlCommand(strquery.ToString, oConnection)
            objCommand.ExecuteNonQuery()
        Next




        objDataSet.AcceptChanges()

        ' dgSummary.DataSource = objDataSet 'objQuery.ExecuteProcedure("ProcCarWiseRevenueRPT")
        ' dgSummary.DataBind()
    End Sub
    Private Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("../index/CP_index.aspx?ID=43")
    End Sub

    Private Sub btnExportToExcel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportToExcel.Click
        'export to excel
        context.Response.Buffer = True
        context.Response.ClearContent()
        context.Response.ClearHeaders()
        context.Response.ContentType = "application/vnd.ms-excel"
        EnableViewState = True
        Dim tw As New System.IO.StringWriter
        Dim hw As New System.Web.UI.HtmlTextWriter(tw)

        dgSummary.RenderControl(hw)
        Context.Response.Write(tw.ToString())
        Context.Response.Flush()
        context.Response.Close()
        context.Response.End()
    End Sub
End Class
