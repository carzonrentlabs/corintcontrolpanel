<%@ Page Language="vb" AutoEventWireup="false" src="BranchWiseMonthWiseRevenue.aspx.vb" Inherits="BranchWiseMonthWiseRevenue"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>BranchWiseMonthWiseRevenue</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="JavaScript" src="../utilityfunction.js"></script>
		<script language="JavaScript" src="../JScripts/Datefunc.js"></script>
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
		<script language="JavaScript">
		
			function checkbeforenext()
			{
			
			if(document.Form1.txtToDate.value=="")
				{
					alert("Please select Date.")
					document.Form1.txtToDate.focus();
					return false;
				}
		}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<table width="80%" align="center">
				<TBODY>
					<tr>
						<td><IMG src="../images/logo_carzonret.gif"></td>
						<td align="right"><IMG src="../images/logo_hertz.gif" width="100" align="right"></td>
					</tr>
					<tr>
						<td class="subRedHead" align="center" colSpan="2">Branch&nbsp;Wise Revenue</td>
					</tr>
					<tr>
						<td colSpan="2"></td>
					</tr>
					<tr bgColor="#ffe5e0">
						<td colSpan="2">
							<TABLE id="Table1" cellSpacing="2" cellPadding="1" width="100%" border="0">
								<TR>
									<TD align="right" width="20%">Unit Name: *</TD>
									<TD align="left" width="20%"><asp:dropdownlist id="CBOUnit" runat="server" CssClass="input"></asp:dropdownlist></TD>
									<TD vAlign="middle" align="left" width="27%">Month: *
										<asp:dropdownlist id="txtToDate" runat="server" CssClass="input">
											<asp:ListItem Value="0">--</asp:ListItem>
											<asp:ListItem Value="1">Jan</asp:ListItem>
											<asp:ListItem Value="2">Feb</asp:ListItem>
											<asp:ListItem Value="3">Mar</asp:ListItem>
											<asp:ListItem Value="4">Apr</asp:ListItem>
											<asp:ListItem Value="5">May</asp:ListItem>
											<asp:ListItem Value="6">Jun</asp:ListItem>
											<asp:ListItem Value="7">Jul</asp:ListItem>
											<asp:ListItem Value="8">Aug</asp:ListItem>
											<asp:ListItem Value="9">Sep</asp:ListItem>
											<asp:ListItem Value="10">Oct</asp:ListItem>
											<asp:ListItem Value="11">Nov</asp:ListItem>
											<asp:ListItem Value="12">Dec</asp:ListItem>
										</asp:dropdownlist></TD>
									<TD vAlign="middle" align="left" width="27%">Year: *
										<asp:dropdownlist id="txtYearDate" runat="server" CssClass="input">
											<asp:ListItem Value="0">--</asp:ListItem>
											<asp:ListItem Value="2005">2005</asp:ListItem>
											<asp:ListItem Value="2006">2006</asp:ListItem>
											<asp:ListItem Value="2007">2007</asp:ListItem>
										</asp:dropdownlist></TD>
								</TR>
							</TABLE>
						</td>
					</tr>
					<tr bgColor="#ffe5e0">
						<td align="center" colSpan="2">&nbsp;
							<asp:button id="btnproceed" runat="server" CssClass="input" Text="Proceed"></asp:button><asp:button id="btnExportToExcel" runat="server" CssClass="input" Text="Export to Excel"></asp:button><asp:button id="btnBack" runat="server" Text="Return to Menu"></asp:button>&nbsp;&nbsp;&nbsp;</td>
					</tr>
					<tr>
						<td align="center" colSpan="2">
							<div noWrap><asp:datagrid id="dgSummary" runat="server" Width="100%" AutoGenerateColumns="False" CellPadding="0"
									BorderStyle="None">
									<SelectedItemStyle Wrap="False"></SelectedItemStyle>
									<EditItemStyle Wrap="False"></EditItemStyle>
									<HeaderStyle Wrap="False" HorizontalAlign="Center" Height="20px" CssClass="subRedHead" BackColor="#FFE5BA"></HeaderStyle>
									<Columns>
										<asp:BoundColumn></asp:BoundColumn>
										<asp:BoundColumn DataField="cityname" HeaderText="Branch">
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="carcatname" HeaderText="Category">
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="NoofBookings" HeaderText="No of Bookings">
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="BasicAmount" HeaderText="Revenue">
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="Average" HeaderText="Avg Revenue/Per Booking">
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
										</asp:BoundColumn>
									</Columns>
								</asp:datagrid></div>
						</td>
					</tr>
				</TBODY>
			</table>
		</form>
	</body>
</HTML>
