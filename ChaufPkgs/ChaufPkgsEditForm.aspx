<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" Src="ChaufPkgsEditForm.aspx.vb" Inherits="ChaufPkgsEditForm" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>CarzonRent :: Internal software Control panel</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <script language="JavaScript" src="../utilityfunction.js" type="text/jscript"></script>
    <link href="../HertzInt.css" type="text/css" rel="stylesheet">
    <script language="javascript" type="text/javascript">
        function chkFields() {
            if (document.forms[0].chkairport.checked == true) {
                document.forms[0].chkoutstation.checked = false;
                //document.forms[0].txtoutstation.disabled = true;
                //document.forms[0].txtnightstay.disabled = true;
            }
            else {
                if (document.forms[0].chkoutstation.checked == true && document.getElementById('<%=ddldropcity.ClientID%>').selectedIndex == 0) {
                    document.forms[0].chkairport.checked = false;
                    //document.forms[0].ddlpackage.disabled = true;
                    //document.forms[0].txtratehr.disabled = true;
                    //document.forms[0].txthr.disabled=true;
                    //document.forms[0].txtexkm.disabled = true;
                    //document.forms[0].chktwoway.disabled = false;
                    //document.forms[0].ddldropcity.disabled = false;
                }

                if (document.forms[0].chkoutstation.checked == true) {
                    //document.forms[0].txtoutstation.disabled = false;
                }
                else {
                    //document.forms[0].txtoutstation.disabled = true;
                }
            }
        }

        function validation() {
            if (isNaN(document.forms[0].txtrate.value)) {
                alert("Rate should be numeric and upto two decimal digits only.")
                return false;
            }
            else {
                if (parseFloat(document.forms[0].txtrate.value.indexOf(".")) > 0) {
                    var FlotVal
                    FlotVal = document.forms[0].txtrate.value.substr(parseFloat(document.forms[0].txtrate.value.indexOf(".")), document.forms[0].txtrate.value.length)
                    if (parseFloat((FlotVal.length) - 1) > 2) {
                        alert("Rate should be numeric and upto two decimal digits only")
                        return false;
                    }
                }
            }

            if (isNaN(document.forms[0].txtwaiting.value)) {
                alert("Waiting Charges should be numeric and upto two decimal digits only.")
                document.forms[0].txtwaiting.focus();
                document.forms[0].txtwaiting.value = 0;
                return false;
            }

            //if((document.forms[0].chkairport.checked==true) && (document.forms[0].ddlcity.value==""))
            //{
            //   alert("Please select the city.");
            //   return false;
            //}

            if (isNaN(document.forms[0].txtratehr.value)) {
                alert("Rate / Extra Hr should be numeric and upto two decimal digits only.")
                return false;
            }
            else {
                if (parseFloat(document.forms[0].txtratehr.value.indexOf(".")) > 0) {
                    var FlotVal
                    FlotVal = document.forms[0].txtratehr.value.substr(parseFloat(document.forms[0].txtratehr.value.indexOf(".")), document.forms[0].txtratehr.value.length)
                    if (parseFloat((FlotVal.length) - 1) > 2) {
                        alert("Rate / Extra Hr should be numeric and upto two decimal digits only")
                        return false;
                    }
                }
            }

            if (isNaN(document.forms[0].txtratekm.value)) {
                alert("Rate / Extra KM should be numeric and upto two decimal digits only.")
                return false;
            }
            else {
                if (parseFloat(document.forms[0].txtratekm.value.indexOf(".")) > 0) {
                    var FlotVal
                    FlotVal = document.forms[0].txtratekm.value.substr(parseFloat(document.forms[0].txtratekm.value.indexOf(".")), document.forms[0].txtratekm.value.length)
                    if (parseFloat((FlotVal.length) - 1) > 2) {
                        alert("Rate / Extra KM should be numeric and upto two decimal digits only")
                        return false;
                    }
                }
            }


            //if(isNaN(document.forms[0].txthr.value))
            //{
            //	alert("Threshold Extra Hr (Crosses to next package if reached) should be numeric and upto two decimal digits only.")
            //	return false;	
            //}					


            if (isNaN(document.forms[0].txtexkm.value)) {
                alert("Threshold Extra KM (Crosses to next package if reached) should be numeric and upto two decimal digits only.")
                return false;
            }


            if (isNaN(document.forms[0].txtoutstation.value)) {
                alert("Outstation Allowance should be numeric and upto two decimal digits only.")
                return false;
            }
            else {
                if (parseFloat(document.forms[0].txtoutstation.value.indexOf(".")) > 0) {
                    var FlotVal
                    FlotVal = document.forms[0].txtoutstation.value.substr(parseFloat(document.forms[0].txtoutstation.value.indexOf(".")), document.forms[0].txtoutstation.value.length)
                    if (parseFloat((FlotVal.length) - 1) > 2) {
                        alert("Outstation Allowance KM should be numeric and upto two decimal digits only")
                        return false;
                    }
                }
            }


            if (isNaN(document.forms[0].txtnightstay.value)) {
                alert("Night Stay Allowance should be numeric and upto two decimal digits only.")
                return false;
            }
            else {
                if (parseFloat(document.forms[0].txtnightstay.value.indexOf(".")) > 0) {
                    var FlotVal
                    FlotVal = document.forms[0].txtnightstay.value.substr(parseFloat(document.forms[0].txtnightstay.value.indexOf(".")), document.forms[0].txtnightstay.value.length)
                    if (parseFloat((FlotVal.length) - 1) > 2) {
                        alert("Night Stay Allowance should be numeric and upto two decimal digits only")
                        return false;
                    }
                }
            }

            if (isNaN(document.forms[0].ddlngt.value) > isNaN(document.forms[0].ddldays.value)) {
                alert("Nights must be less than days.");
                document.forms[0].ddldays.focus();
                return false;
            }

            var strvalues
            if (document.forms[0].chkCustom.checked == true) {
                strvalues = ('ddlcarcat,txtrate,ddlpackage,ddlpckgkm,ddldays,ddlngt')
            }
            else {
                strvalues = ('ddlcarcat,txtrate,ddlpackage,ddlpckgkm')
            }
            return checkmandatory(strvalues);
        }

        function CalcOSRate() {
            if (document.forms[0].chkoutstation.checked == true) {
                if (document.forms[0].ddlpckgkm.value != "" && document.forms[0].txtratekm.value != "") {
                    document.forms[0].txtrate.value = parseFloat(document.forms[0].ddlpckgkm.value) * document.forms[0].txtratekm.value;
                }
            }
        }

        function airportcheckboxOnload() {
            if (document.forms[0].chkoutstation.checked == true) {
                document.forms[0].chkairport.checked = false;
                document.forms[0].chkCustom.checked = false;
                document.forms[0].chkcity.checked = false;
                //document.forms[0].ddlcity.disabled=true
                //document.forms[0].chktwoway.disabled = false;
                //document.forms[0].ddldropcity.disabled = false;
                //document.forms[0].txtoutstation.disabled = false;
                //document.forms[0].txtnightstay.disabled = false;
                document.forms[0].ddlpackage.value = 1;
                //document.forms[0].ddlpackage.disabled = true;
                //document.forms[0].txtratehr.disabled = true;
                if (document.forms[0].ddlpckgkm.value == 0) {
                    document.forms[0].ddlpckgkm.value = 0;
                }
                document.forms[0].ddldays.value = 0;
                document.forms[0].ddlngt.value = 0;

                //document.forms[0].txthr.disabled=true;
                //document.forms[0].txtexkm.disabled = true;
                document.getElementById('show').style.display = "none";
                document.getElementById('show1').style.display = "none";
                document.getElementById('show2').style.display = "none";
                document.getElementById('show3').style.display = "none";
                document.getElementById('show4').style.display = "none";
                document.getElementById('show5').style.display = "none";
                document.getElementById('show6').style.display = "none";
                //document.getElementById('show7').style.display="block";
                //document.getElementById('show8').style.display="block";		

                if (document.forms[0].ddlpckgkm.value != "" && document.forms[0].txtratekm.value != "") {

                    document.forms[0].txtrate.value = document.forms[0].txtrate.value;
                }
                else {
                    if (document.forms[0].chkCustom.checked == true)
                    { }
                    else
                    {
                        //document.forms[0].txtrate.value = "";
                    }
                }
            }
            else {
                if (document.forms[0].chkCustom.checked == true)
                { }
                else
                {
                    //document.forms[0].txtrate.value = "";
                }
                //document.forms[0].ddlpackage.value=0;
                //document.forms[0].ddlpckgkm.value=0;
                //document.forms[0].ddlpackage.disabled = false;
                //document.forms[0].chktwoway.disabled = true;
                document.forms[0].chktwoway.checked = false;
                document.forms[0].ddldropcity.value = "";
                //document.forms[0].ddldropcity.disabled = true;
                //document.forms[0].txtratehr.disabled = false;
                //document.forms[0].txthr.disabled=false;
                //document.forms[0].txtoutstation.disabled = true;
            }
        }
        function airportcheckbox() {
            if (document.forms[0].chkoutstation.checked == true) {
                document.forms[0].chkairport.checked = false;
                document.forms[0].chkCustom.checked = false;
                document.forms[0].chkcity.checked = false;
                //document.forms[0].chktwoway.disabled = false;
                //document.forms[0].ddldropcity.disabled = false;
                //document.forms[0].txtoutstation.disabled = false;
                //document.forms[0].txtnightstay.disabled = false;
                document.forms[0].ddlpackage.value = 1;
                //document.forms[0].ddlpackage.disabled = true;
                //document.forms[0].txtratehr.disabled = true;
                if (document.forms[0].ddlpckgkm.value == 0) {
                    document.forms[0].ddlpckgkm.value = 0;
                }
                document.forms[0].ddldays.value = 0;
                document.forms[0].ddlngt.value = 0;

                //document.forms[0].txtexkm.disabled = true;
                document.getElementById('show').style.display = "none";
                document.getElementById('show1').style.display = "none";
                document.getElementById('show2').style.display = "none";
                document.getElementById('show3').style.display = "none";
                document.getElementById('show4').style.display = "none";
                document.getElementById('show5').style.display = "none";
                document.getElementById('show6').style.display = "none";
                //document.getElementById('show7').style.display="block";
                //document.getElementById('show8').style.display="block";		

                if (document.forms[0].ddlpckgkm.value != "" && document.forms[0].txtratekm.value != "") {

                    document.forms[0].txtrate.value = parseFloat(document.forms[0].ddlpckgkm.value) * document.forms[0].txtratekm.value;
                }
                else {
                    if (document.forms[0].chkCustom.checked == true)
                    { }
                    else
                    {
                        //document.forms[0].txtrate.value = "";
                    }
                }
            }
            else {
                if (document.forms[0].chkCustom.checked == true)
                { }
                else
                {
                    //document.forms[0].txtrate.value = "";
                }
                //document.forms[0].ddlpackage.value=0;
                //document.forms[0].ddlpckgkm.value=0;
                //document.forms[0].ddlpackage.disabled = false;
                //document.forms[0].chktwoway.disabled = true;
                document.forms[0].chktwoway.checked = false;
                document.forms[0].ddldropcity.value = "";
                //document.forms[0].ddldropcity.disabled = true;
                //document.forms[0].txtratehr.disabled = false;
                //document.forms[0].txthr.disabled=false;
                //document.forms[0].txtexkm.disabled = false;
                //document.forms[0].txtoutstation.disabled = true;
            }
        }
        function enablecity() {

            if (document.forms[0].chkairport.checked == true) {
                //document.forms[0].ddlcity.disabled=false
                document.forms[0].chkcity.checked = false;
                document.forms[0].chkoutstation.checked = false;
                //document.forms[0].txtoutstation.disabled = true;
                //document.forms[0].txtnightstay.disabled = true;
                document.forms[0].chkCustom.checked = false;
                if (document.forms[0].ddlpackage.value == 0) {
                    document.forms[0].ddlpackage.value = 0;
                }
                if (document.forms[0].ddlpckgkm.value == 0) {
                    document.forms[0].ddlpckgkm.value = 0;
                }
                document.forms[0].ddldays.value = 0;
                document.forms[0].ddlngt.value = 0;
                //document.forms[0].ddlpackage.disabled = false;
                //document.forms[0].txtratehr.disabled = false;
                //document.forms[0].txthr.disabled=false;
                //document.forms[0].txtexkm.disabled = false;
                //document.forms[0].ddldropcity.disabled = true;
                document.forms[0].ddldropcity.value = "";
                document.forms[0].chktwoway.checked = false;
                //document.forms[0].chktwoway.disabled = false;
                document.getElementById('show').style.display = "none";
                document.getElementById('show1').style.display = "none";
                document.getElementById('show2').style.display = "none";
                document.getElementById('show3').style.display = "none";
                document.getElementById('show4').style.display = "none";
                document.getElementById('show5').style.display = "none";
                document.getElementById('show6').style.display = "none";
            }
            else {
                //document.forms[0].ddlpackage.disabled = false;
                //document.forms[0].txtnightstay.disabled = false;
                if (document.forms[0].chkoutstation.checked == false) {
                    document.forms[0].chktwoway.checked = false;
                    //document.forms[0].chktwoway.disabled = true;
                }
            }
        }

        function citycheckbox() {
            if (document.forms[0].chkcity.checked == true) {
                document.forms[0].chkoutstation.checked = false;
                document.forms[0].chkairport.checked = false;
                //document.forms[0].txtoutstation.disabled = true;
                document.forms[0].chkCustom.checked = false;
                //document.forms[0].txtnightstay.disabled = true;
                if (document.forms[0].ddlpackage.value == 0) {
                    document.forms[0].ddlpackage.value = 0;
                }
                if (document.forms[0].ddlpckgkm.value == 0) {
                    document.forms[0].ddlpckgkm.value = 0;
                }
                document.forms[0].ddldays.value = 0;
                document.forms[0].ddlngt.value = 0;
                //document.forms[0].ddlpackage.disabled = false;
                //document.forms[0].txtratehr.disabled = false;
                //document.forms[0].txtexkm.disabled = false;
                //document.forms[0].ddldropcity.disabled = true;
                document.forms[0].ddldropcity.value = "";
                document.forms[0].chktwoway.checked = false;
                //document.forms[0].chktwoway.disabled = true;
                document.getElementById('show').style.display = "none";
                document.getElementById('show1').style.display = "none";
                document.getElementById('show2').style.display = "none";
                document.getElementById('show3').style.display = "none";
                document.getElementById('show4').style.display = "none";
                document.getElementById('show5').style.display = "none";
                document.getElementById('show6').style.display = "none";
                //document.getElementById('show7').style.display="none";
                //document.getElementById('show8').style.display="block";
            }
            else {
                //document.forms[0].ddlpackage.disabled = false;
                //document.forms[0].txtnightstay.disabled = false;
                if (document.forms[0].chkoutstation.checked == false) {
                    document.forms[0].chktwoway.checked = false;
                    //document.forms[0].chktwoway.disabled = true;
                }
            }
        }


        function Customcheckbox() {
            if (document.forms[0].chkCustom.checked == true) {
                document.forms[0].chkcity.checked = false;
                document.forms[0].chkairport.checked = false;
                document.forms[0].chkoutstation.checked = false;
                //document.forms[0].ddldropcity.disabled = true;
                document.forms[0].ddldropcity.value = "";
                //document.forms[0].txtratehr.disabled = false;
                //document.forms[0].txtexkm.disabled = false;
                //document.forms[0].txtoutstation.disabled = false;
                document.getElementById('show').style.display = "block";
                document.getElementById('show1').style.display = "block";
                document.getElementById('show2').style.display = "block";
                document.getElementById('show3').style.display = "block";
                document.getElementById('show4').style.display = "block";
                document.getElementById('show5').style.display = "block";
                document.getElementById('show6').style.display = "block";
                document.getElementById('show7').style.display = "none";
                document.getElementById('show8').style.display = "none";
            }
            else {
                document.getElementById('show').style.display = "none";
                document.getElementById('show1').style.display = "none";
                document.getElementById('show2').style.display = "none";
                document.getElementById('show3').style.display = "none";
                document.getElementById('show4').style.display = "none";
                document.getElementById('show5').style.display = "none";
                document.getElementById('show6').style.display = "none";
                document.getElementById('show7').style.display = "block";
                document.getElementById('show8').style.display = "block";
                //document.forms[0].ddlpackage.disabled = false;

                if (document.forms[0].chkoutstation.checked == false) {
                    //document.forms[0].ddldropcity.disabled = true;
                    document.forms[0].ddldropcity.value = "";
                    document.forms[0].chktwoway.checked = false;
                    //document.forms[0].chktwoway.disabled = true;
                    //document.forms[0].txtoutstation.disabled = false;
                }
                else {
                    //document.forms[0].txtoutstation.disabled = false;
                }
            }
        }

    </script>
</head>
<body ms_positioning="GridLayout" onload="airportcheckboxOnload();chkFields();enablecity();Customcheckbox();">

    <form id="Form1" method="post" runat="server">
        <uc1:Headerctrl ID="Headerctrl1" runat="server"></uc1:Headerctrl>
        <table id="Table1" align="center">
            <tbody>
                <asp:Panel ID="pnlmainform" runat="server">
                    <tr>
                        <td align="center" colspan="2"><strong><u>Deactivate a Chauffeur Package</u></strong></td>
                    </tr>

                    <tr>
                        <td align="center" colspan="2">&nbsp;&nbsp;</td>
                    </tr>

                    <tr>
                        <td>* Car Category</td>
                        <td>
                            <asp:DropDownList ID="ddlcarcat" Enabled="false" runat="server" CssClass="input"></asp:DropDownList></td>
                    </tr>

                    <tr>
                        <div id="show">
                            <td>Car Model</td>
                            <td>
                                <asp:DropDownList ID="ddlcarModel" Enabled="false" runat="server" CssClass="input"></asp:DropDownList></td>
                        </div>
                    </tr>

                    <tr>
                        <td>Tariff Type</td>
                        <td>
                            <asp:DropDownList ID="ddTariffType" Enabled="false" runat="server" CssClass="input">
                                <asp:ListItem Text="Special" Value="S" />
                            </asp:DropDownList></td>
                    </tr>

                    <tr>
                        <td>Airport Transfer</td>
                        <td>
                            <asp:CheckBox ID="chkairport" Enabled="false" onclick="enablecity();" runat="server" CssClass="input"></asp:CheckBox></td>
                    </tr>

                    <tr>
                        <td>Pick-up City Name</td>
                        <td>
                            <asp:DropDownList ID="ddlcity" Enabled="false" runat="server" CssClass="input"></asp:DropDownList></td>
                    </tr>

                    <tr>
                        <div id="show2">
                            <td>* No of Days</td>
                            <td>
                                <asp:DropDownList ID="ddldays" Enabled="false" runat="server" CssClass="input"></asp:DropDownList></td>
                        </div>
                    </tr>

                    <tr>
                        <div id="show1">
                            <td>* No Of Night</td>
                            <td>
                                <asp:DropDownList ID="ddlngt" Enabled="false" runat="server" CssClass="input"></asp:DropDownList>
                            </td>
                        </div>
                    </tr>

                    <tr>
                        <td>* Package Hours (1 Day in case of outstation)</td>
                        <td>
                            <asp:DropDownList ID="ddlpackage" Enabled="false" runat="server" CssClass="input"></asp:DropDownList>
                        </td>
                    </tr>

                    <tr>
                        <td>* Package KMs</td>
                        <td>
                            <asp:DropDownList ID="ddlpckgkm" Enabled="false" onBlur="CalcOSRate();" runat="server" CssClass="input"></asp:DropDownList>
                        </td>
                    </tr>

                    <tr>
                        <td>Outstation</td>
                        <td>
                            <asp:CheckBox ID="chkoutstation" Enabled="false" onclick="airportcheckbox();" runat="server" CssClass="input"></asp:CheckBox>
                        </td>
                    </tr>

                    <tr>
                        <td>City Transter</td>
                        <td>
                            <asp:CheckBox ID="chkcity" Enabled="false" onclick="citycheckbox();" runat="server" CssClass="input"></asp:CheckBox></td>
                    </tr>

                    <tr>
                        <td>Custom PkgYN</td>
                        <td>
                            <asp:CheckBox ID="chkCustom" Enabled="false" onclick="Customcheckbox();" runat="server" CssClass="input"></asp:CheckBox></td>
                    </tr>

                    <tr>
                        <td>PKPM PkgYN</td>
                        <td>
                            <asp:CheckBox ID="chkPKPM" Enabled="false" onclick="Customcheckbox();" runat="server" CssClass="input" AutoPostBack="true"></asp:CheckBox></td>
                    </tr>

                    <tr>
                        <td>LocalOneWayDropYN</td>
                        <td>
                            <asp:CheckBox ID="chkLocalOneWay" Enabled="false" onclick="Customcheckbox();" runat="server" CssClass="input" AutoPostBack="true"></asp:CheckBox></td>
                    </tr>

                    <tr>
                        <td>* Rate</td>
                        <td>
                            <asp:TextBox ID="txtrate" Enabled="false" runat="server" CssClass="input" MaxLength="50"></asp:TextBox></td>
                    </tr>

                    <tr>
                        <div id="show4">
                            <td>Extra Day Charges</td>
                            <td>
                                <asp:TextBox ID="txtdaychgs" Enabled="false" runat="server" CssClass="input" MaxLength="50"></asp:TextBox>
                            </td>
                        </div>
                    </tr>

                    <tr>
                        <div id="show3">
                            <td style="height: 19px">Extra Night Charges</td>
                            <td style="height: 19px">
                                <asp:TextBox ID="txtngtchgs" Enabled="false" runat="server" CssClass="input" MaxLength="50"></asp:TextBox>

                            </td>
                        </div>
                    </tr>

                    <tr>
                        <td>Rate / Extra Hr</td>
                        <td>
                            <asp:TextBox ID="txtratehr" Enabled="false" runat="server" CssClass="input" MaxLength="50"></asp:TextBox></td>
                    </tr>

                    <tr>
                        <td>Rate / Extra KM</td>
                        <td>
                            <asp:TextBox ID="txtratekm" Enabled="false" onBlur="CalcOSRate();" runat="server" CssClass="input" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td>Threshold Extra Hr (Crosses to next package if reached)</td>
                        <td>
                            <asp:DropDownList ID="ddlHr" runat="server" Enabled="false">
                                <asp:ListItem Text="00" Value="0" />
                                <asp:ListItem Text="01" Value="1" />
                                <asp:ListItem Text="02" Value="2" />
                                <asp:ListItem Text="03" Value="3" />
                                <asp:ListItem Text="04" Value="4" />
                                <asp:ListItem Text="05" Value="5" />
                                <asp:ListItem Text="06" Value="6" />
                                <asp:ListItem Text="07" Value="7" />
                                <asp:ListItem Text="08" Value="8" />
                                <asp:ListItem Text="09" Value="9" />
                                <asp:ListItem Text="10" Value="10" />
                                <asp:ListItem Text="11" Value="11" />
                                <asp:ListItem Text="12" Value="12" />
                                <asp:ListItem Text="13" Value="13" />
                                <asp:ListItem Text="14" Value="14" />
                                <asp:ListItem Text="15" Value="15" />
                                <asp:ListItem Text="16" Value="16" />
                                <asp:ListItem Text="17" Value="17" />
                                <asp:ListItem Text="18" Value="18" />
                                <asp:ListItem Text="19" Value="19" />
                                <asp:ListItem Text="20" Value="20" />
                                <asp:ListItem Text="21" Value="21" />
                                <asp:ListItem Text="22" Value="22" />
                                <asp:ListItem Text="23" Value="23" />
                                <asp:ListItem Text="24" Value="24" />
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddlMin" runat="server" Enabled="false">
                                <asp:ListItem Text="00" Value="00" />
                                <asp:ListItem Text="15" Value="25" />
                                <asp:ListItem Text="30" Value="50" />
                                <asp:ListItem Text="45" Value="75" />
                            </asp:DropDownList></td>
                    </tr>

                    <tr>
                        <td>Threshold Extra KM (Crosses to next package if reached)</td>
                        <td>
                            <asp:TextBox ID="txtexkm" Enabled="false" runat="server" CssClass="input" MaxLength="50"></asp:TextBox></td>
                    </tr>

                    <tr>
                        <td>Outstation Allowance</td>
                        <td>
                            <asp:TextBox ID="txtoutstation" Enabled="false" runat="server" CssClass="input" MaxLength="50"></asp:TextBox>
                            <asp:HiddenField ID="hdoutstation" runat="server" />
                        </td>
                    </tr>

                    <tr>
                        <td>Night Stay Allowance</td>
                        <td>
                            <asp:TextBox ID="txtnightstay" Enabled="false" runat="server" CssClass="input" MaxLength="50"></asp:TextBox></td>
                    </tr>

                    <tr>
                        <div id="show5">
                            <td>Taxes</td>
                            <td>
                                <asp:TextBox ID="txttaxes" Enabled="false" runat="server" CssClass="input" MaxLength="50"></asp:TextBox>

                            </td>
                        </div>
                    </tr>

                    <tr>
                        <td>Waiting Charges</td>
                        <td>
                            <asp:TextBox ID="txtwaiting" Enabled="false" runat="server" CssClass="input" MaxLength="50"></asp:TextBox></td>
                    </tr>

                    <tr>
                        <div id="show7">
                            <td>Drop-off City Name</td>
                            <td>
                                <asp:DropDownList ID="ddldropcity" Enabled="false" runat="server" CssClass="input"></asp:DropDownList></td>
                        </div>
                    </tr>

                    <tr>
                        <div id="show8">
                            <td>To Way Transfer YN</td>
                            <td>
                                <asp:CheckBox ID="chktwoway" Enabled="false" runat="server" CssClass="input"></asp:CheckBox></td>
                        </div>
                    </tr>

                    <tr>
                        <div id="show6">
                            <td valign="top">Description</td>
                            <td>
                                <asp:TextBox ID="txtdescription" Enabled="false" runat="server" CssClass="input" MaxLength="500" Rows="3" Columns="50" TextMode="MultiLine"></asp:TextBox>
                            </td>
                        </div>
                    </tr>

                    <tr>
                        <td valign="top">Remarks(<span class="shwText" id="shwMessage">0</span>/2000 chars)</td>
                        <td>
                            <asp:TextBox ID="txtRemarks" onkeydown="showLength(this.form.txtRemarks,'document.all.shwMessage',2000)"
                                onkeyup="showLength(this.form.txtRemarks,'document.all.shwMessage',2000)" runat="server"
                                CssClass="input" MaxLength="2000" Rows="3" Columns="50" TextMode="MultiLine"></asp:TextBox></td>
                    </tr>

                    <tr>
                        <td>Active</td>
                        <td>
                            <asp:CheckBox ID="chkactive" runat="server" CssClass="input" Checked="True"></asp:CheckBox></td>
                    </tr>

                    <tr>
                        <td align="center" colspan="2">
                            <asp:Button ID="btnsubmit" runat="server" CssClass="button" Text="Submit"></asp:Button>
                            &nbsp;&nbsp;
	                        <asp:Button ID="btnreset" runat="server" CssClass="button" Text="Reset" CausesValidation="False"></asp:Button>
                        </td>
                    </tr>

                </asp:Panel>
                <asp:Panel ID="pnlconfirmation" runat="server" Visible="False">
                    <tr align="center">
                        <td colspan="2">
                            <asp:Label ID="lblMessage" runat="server"></asp:Label></td>
                    </tr>

                    <tr align="center">
                        <td colspan="2">
                            <asp:HyperLink ID="hyplnkretry" runat="server"></asp:HyperLink></td>
                    </tr>
                </asp:Panel>
            </tbody>
        </table>
    </form>
</body>
</html>
