Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.text
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Imports System.Web.UI
Public Class ChaufPkgsEditResult
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tblRecDetail As System.Web.UI.WebControls.Table

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not Page.IsPostBack Then
            getvalue("C.carcatname")
        End If
    End Sub
    Sub getvalue(ByVal strorderby As String)
        Dim strquery As StringBuilder
        Dim dtrreader As SqlDataReader
        Dim accessdata As clsutility
        accessdata = New clsutility
        strquery = New StringBuilder("select M.pkgid,A.cityname,C.carcatname,M.apttransyn,convert(varchar(10),pkghrs)+'/'+ convert(varchar(10),pkgkms) as pcakages , M.ExtraHrRate, M.ExtraKMRate, outstationyn, case M.TariffType when 'N' then 'National' when 'S' then 'Special' end as TariffType, case M.active when '1' then 'Active' when '0' then 'Not Active' end as active from CORIntChaufPkgsMaster M left outer join CORIntCityMaster A on A.cityid=M.cityid,CORIntCarCatMaster C where C.carcatid=M.carcatid  ")
        If Request.QueryString("PkgID") <> "" Then
            strquery.Append(" and M.pkgid=" & Request.QueryString("pkgid") & " ")
        End If
        If Request.QueryString("CarCat") <> "-1" Then
            strquery.Append(" and M.carcatid=" & Request.QueryString("CarCat") & " ")
        End If
        If Request.QueryString("Tariff") <> "" Then
            strquery.Append(" and M.TariffType='" & Request.QueryString("Tariff") & "' ")
        End If
        If Request.QueryString("Apt") <> "" Then
            strquery.Append(" and M.apttransyn=" & Request.QueryString("Apt") & " ")
        End If
        If Request.QueryString("City") <> "-1" Then
            strquery.Append(" and M.cityid=" & Request.QueryString("City") & " ")
        End If
        If Request.QueryString("PkgHrs") <> "" Then
            strquery.Append(" and pkghrs=" & Request.QueryString("PkgHrs") & " ")
        End If
        If Request.QueryString("PkgKMs") <> "" Then
            strquery.Append(" and pkgkms=" & Request.QueryString("PkgKMs") & " ")
        End If
        If Request.QueryString("OS") <> "" Then
            strquery.Append(" and outstationyn=" & Request.QueryString("OS") & " ")
        End If
        If Request.QueryString("Active") <> "" Then
            strquery.Append(" and M.active=" & Request.QueryString("Active") & " ")
        End If
        strquery.Append(" order by " & strorderby & "")

        dtrreader = accessdata.funcGetSQLDataReader(strquery.ToString)
        While dtrreader.Read
            Dim Temprow As New TableRow
            Temprow.HorizontalAlign = HorizontalAlign.Center
            Dim Tempcel10 As New TableCell
            Tempcel10.Controls.Add(New LiteralControl(dtrreader("PkgID") & ""))
            Temprow.Cells.Add(Tempcel10)

            Dim Tempcell As New TableCell
            Tempcell.Controls.Add(New LiteralControl(dtrreader("carcatname") & ""))
            Temprow.Cells.Add(Tempcell)

            Dim Tempcel2 As New TableCell
            Tempcel2.Controls.Add(New LiteralControl(dtrreader("apttransyn") & ""))
            Temprow.Cells.Add(Tempcel2)

            Dim Tempcel4 As New TableCell
            Tempcel4.Controls.Add(New LiteralControl(dtrreader("cityname") & ""))
            Temprow.Cells.Add(Tempcel4)

            Dim Tempcel5 As New TableCell
            Tempcel5.Controls.Add(New LiteralControl(dtrreader("pcakages") & ""))
            Temprow.Cells.Add(Tempcel5)

            Dim Tempcel12 As New TableCell
            Tempcel12.Controls.Add(New LiteralControl(dtrreader("ExtraHrRate") & ""))
            Temprow.Cells.Add(Tempcel12)

            Dim Tempcel13 As New TableCell
            Tempcel13.Controls.Add(New LiteralControl(dtrreader("ExtraKMRate") & ""))
            Temprow.Cells.Add(Tempcel13)

            Dim Tempcel6 As New TableCell
            Tempcel6.Controls.Add(New LiteralControl(dtrreader("outstationyn") & ""))
            Temprow.Cells.Add(Tempcel6)

            Dim Tempcel9 As New TableCell
            Tempcel9.Controls.Add(New LiteralControl(dtrreader("TariffType") & ""))
            Temprow.Cells.Add(Tempcel9)

            Dim Tempcel7 As New TableCell
            Tempcel7.Controls.Add(New LiteralControl(dtrreader("active") & ""))
            Temprow.Cells.Add(Tempcel7)

            Dim Tempcel8 As New TableCell
            Tempcel8.Controls.Add(New LiteralControl("<a href=ChaufPkgsEditForm.aspx?ID=" & dtrreader("pkgid") & " >Edit</a>"))
            Temprow.Cells.Add(Tempcel8)
            'adding the Tables rows to the table
            tblRecDetail.Rows.Add(Temprow)

        End While
        dtrreader.Close()
        accessdata.Dispose()


    End Sub
    Sub SortGird(ByVal sender As Object, ByVal e As System.EventArgs)
        Select Case sender.id
            Case "Linkbutton8"
                getvalue("M.PkgID")
            Case "Linkbutton1"
                getvalue("C.carcatname")
            Case "Linkbutton2"
                getvalue("M.apttransyn")
            Case "Linkbutton3"
                getvalue("A.cityname")
            Case "Linkbutton4"
                getvalue("pcakages")
            Case "Linkbutton5"
                getvalue("pcakages")
            Case "Linkbutton10"
                getvalue("ExtraHrRate")
            Case "Linkbutton11"
                getvalue("ExtraKMRate")
            Case "Linkbutton7"
                getvalue("TariffType")
            Case "Linkbutton6"
                getvalue("active")
        End Select
    End Sub
End Class
