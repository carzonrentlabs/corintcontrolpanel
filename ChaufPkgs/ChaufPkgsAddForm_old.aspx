<%@ Page Language="vb" AutoEventWireup="false" src="ChaufPkgsAddForm.aspx.vb" Inherits="ChaufPkgsAddForm"%>
<%@ Register TagPrefix="uc1" TagName="Headerctrl" src="~/usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
'dim ddldropcityid1
'ddldropcityid1=request.form("ddldropcityid")
%>
<HTML>
  <HEAD>
		<title>CarzonRent :: Internal software Control panel</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="JavaScript" src="../utilityfunction.js"></script>
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
		<script language="javascript">
function validation()
{
	if(isNaN(document.forms[0].txtrate.value))
	{
		alert("Rate should be numeric and upto two decimal digits only.")
		return false;	
	}
	else
	{
		if ((document.forms[0].txtrate.value==0) || (document.forms[0].txtrate.value==""))
		{
			alert("Please enter package rate")
			return false;
		}
		if (parseFloat(document.forms[0].txtrate.value.indexOf("."))>0)
		{
			var FlotVal
			FlotVal=document.forms[0].txtrate.value.substr(parseFloat(document.forms[0].txtrate.value.indexOf(".")),document.forms[0].txtrate.value.length)
			if(parseFloat((FlotVal.length)-1)>2)
			{
				alert("Rate should be numeric and upto two decimal digits only")
				return false;
			}
		}
	}

	//if((document.forms[0].chkairport.checked==true) && (document.forms[0].ddlcity.value==""))
	//{
	//	alert("Please select the city.");
	//	return false;
	//}
					
	if(isNaN(document.forms[0].txtratehr.value))
	{
		alert("Rate / Extra Hr should be numeric and upto two decimal digits only.")
		return false;	
	}
	else
	{
		if (parseFloat(document.forms[0].txtratehr.value.indexOf("."))>0)
		{
			var FlotVal
			FlotVal=document.forms[0].txtratehr.value.substr(parseFloat(document.forms[0].txtratehr.value.indexOf(".")),document.forms[0].txtratehr.value.length)
			if(parseFloat((FlotVal.length)-1)>2)
			{
				alert("Rate / Extra Hr should be numeric and upto two decimal digits only")
				return false;
			}
		}	
	}
				
	if(document.forms[0].chkoutstation.checked==true && document.getElementById('<%=ddldropcity.ClientID%>').selectedIndex==0)
	{
		//&& document.forms[0].ddldropcity.SelectedItem.Value== 0
				
		if (document.forms[0].txtratekm.value == "" || document.forms[0].txtratekm.value == 0)
		{
			alert("Please enter extra KM rate");
			//alert(document.form1.ddldropcity.selectedItem.value);
			//alert(document.getElementById('<%=ddldropcity.ClientID%>').selectedIndex);
			return false;
		}
	}
				
	if(isNaN(document.forms[0].txtratekm.value))
	{
		alert("Rate / Extra KM should be numeric and upto two decimal digits only.")
		return false;	
	}
	else
	{
		if (parseFloat(document.forms[0].txtratekm.value.indexOf("."))>0)
		{
			var FlotVal
			FlotVal=document.forms[0].txtratekm.value.substr(parseFloat(document.forms[0].txtratekm.value.indexOf(".")),document.forms[0].txtratekm.value.length)
			if(parseFloat((FlotVal.length)-1)>2)
			{
				alert("Rate / Extra KM should be numeric and upto two decimal digits only")
				return false;
			}
		}	
	}	

	if(isNaN(document.forms[0].txtexkm.value))
	{
		alert("Threshold Extra KM (Crosses to next package if reached) should be numeric and upto two decimal digits only.")
		return false;	
	}					
						
	if(isNaN(document.forms[0].txtoutstation.value))
	{
		alert("Outstation Allowance should be numeric and upto two decimal digits only.")
		return false;	
	}
	else
	{
		if (parseFloat(document.forms[0].txtoutstation.value.indexOf("."))>0)
		{
			var FlotVal
			FlotVal=document.forms[0].txtoutstation.value.substr(parseFloat(document.forms[0].txtoutstation.value.indexOf(".")),document.forms[0].txtoutstation.value.length)
			if(parseFloat((FlotVal.length)-1)>2)
			{
				alert("Outstation Allowance KM should be numeric and upto two decimal digits only")
				return false;
			}
		}	
	}			

	if(isNaN(document.forms[0].txtnightstay.value))
	{
		alert("Night Stay Allowance should be numeric and upto two decimal digits only.")
		return false;	
	}
	else
	{
		if (parseFloat(document.forms[0].txtnightstay.value.indexOf("."))>0)
		{
			var FlotVal
			FlotVal=document.forms[0].txtnightstay.value.substr(parseFloat(document.forms[0].txtnightstay.value.indexOf(".")),document.forms[0].txtnightstay.value.length)
			if(parseFloat((FlotVal.length)-1)>2)
			{
				alert("Night Stay Allowance should be numeric and upto two decimal digits only")
				return false;
			}

		}	
	}		

	if(isNaN(document.forms[0].ddlngt.value) > isNaN(document.forms[0].ddldays.value))
	{
		alert("Nights must be less than days.");
		document.forms[0].ddldays.focus();
		return false;
	}
	
	
	
	var strvalues
	if(document.forms[0].chkCustom.checked==true)
	{
		strvalues=('ddlcarcat,txtrate,ddlpackage,ddlpckgkm,ddldays,ddlngt')
	}
	else
	{
		strvalues=('ddlcarcat,txtrate,ddlpackage,ddlpckgkm')	
	}
	return checkmandatory(strvalues);		
}		

function CalcOSRate()
{
	if(document.forms[0].chkoutstation.checked==true)
	{
		if (document.forms[0].ddlpckgkm.value != "" && document.forms[0].txtratekm.value != "")
		{
			document.forms[0].txtrate.value = parseFloat(document.forms[0].ddlpckgkm.value) * document.forms[0].txtratekm.value;
    	}
	}
}

function airportcheckbox()
{
	if(document.forms[0].chkoutstation.checked==true)
	{
		document.forms[0].chkairport.checked=false;
		document.forms[0].chkCustom.checked=false;
		document.forms[0].chkcity.checked=false;
		//document.forms[0].ddlcity.disabled=true
		//document.forms[0].chktwoway.Enabled=true;	
		document.forms[0].chktwoway.disabled=false;
		document.forms[0].ddldropcity.disabled=false;
		document.forms[0].txtoutstation.disabled=false;
		document.forms[0].txtnightstay.disabled=false;
		document.forms[0].ddlpackage.value=1;
		document.forms[0].ddlpackage.disabled=true;
		document.forms[0].txtratehr.disabled=true;
		//document.forms[0].txthr.disabled=true;
		document.forms[0].txtexkm.disabled=true;
		document.getElementById('show').style.display="none";
		document.getElementById('show1').style.display="none";
		document.getElementById('show2').style.display="none";
		document.getElementById('show3').style.display="none";
		document.getElementById('show4').style.display="none";
		document.getElementById('show5').style.display="none";
		document.getElementById('show6').style.display="none";
		//document.getElementById('show7').style.display="block";
		//document.getElementById('show8').style.display="block";

        if (document.forms[0].ddlpckgkm.value != "" && document.forms[0].txtratekm.value != "")
        {
        	document.forms[0].txtrate.value = parseFloat(document.forms[0].ddlpckgkm.value) * document.forms[0].txtratekm.value;
        }
        else
        {
    	    document.forms[0].txtrate.value = "";
	    }
	}
	else
	{
		//document.getElementById('show7').style.display="none";
		//document.getElementById('show8').style.display="none";
   		document.forms[0].txtrate.value = "";
		document.forms[0].ddlpackage.value=0;
		//document.forms[0].chktwoway.checked=false;
		document.forms[0].chktwoway.disabled=true;
		document.forms[0].chktwoway.checked=false;
		document.forms[0].ddldropcity.value="";
		document.forms[0].ddldropcity.disabled=true;					
		document.forms[0].ddlpackage.disabled=false;
		document.forms[0].txtratehr.disabled=false;
		//document.forms[0].txthr.disabled=false;
		//document.forms[0].txtexkm.disabled=false;
		//document.forms[0].txtoutstation.disabled=true;
	}	
}

function enablecity()
{
	if(document.forms[0].chkairport.checked==true)
	{
		//document.forms[0].ddlcity.disabled=false
		document.forms[0].chkcity.checked=false;
		document.forms[0].chkoutstation.checked=false;
		document.forms[0].txtoutstation.disabled=true;
		document.forms[0].chkCustom.checked=false;
		document.forms[0].txtnightstay.disabled=true;
		document.forms[0].ddlpackage.value=0;
		document.forms[0].ddlpackage.disabled=false;
		document.forms[0].txtratehr.disabled=false;
		//document.forms[0].txthr.disabled=false;
		document.forms[0].txtexkm.disabled=false;
	   	document.forms[0].ddldropcity.disabled = true;
		document.forms[0].ddldropcity.value="";	
		//document.forms[0].chktwoway.disabled=true;
		document.forms[0].chktwoway.checked=false;
		document.forms[0].chktwoway.disabled=false;
		document.getElementById('show').style.display="none";
		document.getElementById('show1').style.display="none";
		document.getElementById('show2').style.display="none";
		document.getElementById('show3').style.display="none";
		document.getElementById('show4').style.display="none";
		document.getElementById('show5').style.display="none";
		document.getElementById('show6').style.display="none";
		//document.getElementById('show7').style.display="none";
		//document.getElementById('show8').style.display="block";
	}
	else
	{
		//document.forms[0].ddlcity.disabled=true
		//document.forms[0].txtoutstation.disabled=false;
		//document.getElementById('show7').style.display="none";
		//document.getElementById('show8').style.display="none";
		document.forms[0].txtnightstay.disabled=false;
		document.forms[0].chktwoway.checked=false;					
		document.forms[0].chktwoway.disabled=true;
	}	
}

function citycheckbox()
{
	if(document.forms[0].chkcity.checked==true)
	{
		document.forms[0].chkoutstation.checked=false;
		document.forms[0].chkairport.checked=false;
		document.forms[0].txtoutstation.disabled=true;
		document.forms[0].chkCustom.checked=false;
		document.forms[0].txtnightstay.disabled=true;
		document.forms[0].ddlpackage.value=0;
		document.forms[0].ddlpackage.disabled=false;
		document.forms[0].txtratehr.disabled=false;
		document.forms[0].txtexkm.disabled=false;
	   	document.forms[0].ddldropcity.disabled = true;
		document.forms[0].ddldropcity.value="";	
		document.forms[0].chktwoway.checked=false;
		document.forms[0].chktwoway.disabled=true;
		document.getElementById('show').style.display="none";
		document.getElementById('show1').style.display="none";
		document.getElementById('show2').style.display="none";
		document.getElementById('show3').style.display="none";
		document.getElementById('show4').style.display="none";
		document.getElementById('show5').style.display="none";
		document.getElementById('show6').style.display="none";
		//document.getElementById('show7').style.display="none";
		//document.getElementById('show8').style.display="block";
	}
	else
	{
		document.forms[0].txtnightstay.disabled=false;
		document.forms[0].chktwoway.checked=false;					
		document.forms[0].chktwoway.disabled=true;
	}
}

function Customcheckbox()
{
	if(document.forms[0].chkCustom.checked==true)
	{
		document.forms[0].chkcity.checked=false;
		document.forms[0].ddlpackage.disabled=false;
		document.forms[0].ddlpackage.value=0;
		document.forms[0].ddlpckgkm.value=0;		
		//document.forms[0].ddlpackage.value="";
		document.forms[0].chkairport.checked=false;
		document.forms[0].chkoutstation.checked=false;
	   	document.forms[0].ddldropcity.disabled = true;
		document.forms[0].ddldropcity.value="";
		document.forms[0].txtratehr.disabled=false;
		document.forms[0].txtexkm.disabled=false;
		document.forms[0].txtoutstation.disabled=false;
		document.getElementById('show').style.display="block";
		document.getElementById('show1').style.display="block";
		document.getElementById('show2').style.display="block";
		document.getElementById('show3').style.display="block";
		document.getElementById('show4').style.display="block";
		document.getElementById('show5').style.display="block";
		document.getElementById('show6').style.display="block";
		document.getElementById('show7').style.display="none";
		document.getElementById('show8').style.display="none";
	}
	else
	{
		document.getElementById('show').style.display="none";
		document.getElementById('show1').style.display="none";
		document.getElementById('show2').style.display="none";
		document.getElementById('show3').style.display="none";
		document.getElementById('show4').style.display="none";
		document.getElementById('show5').style.display="none";
		document.getElementById('show6').style.display="none";
		document.getElementById('show7').style.display="block";
		document.getElementById('show8').style.display="block";
		document.forms[0].txtoutstation.disabled=true;
	   	document.forms[0].ddldropcity.disabled = true;
		document.forms[0].ddldropcity.value="";	
		document.forms[0].chktwoway.checked=false;
		document.forms[0].chktwoway.disabled=true;
	}
}

	
		</script>
</HEAD>
	<body MS_POSITIONING="GridLayout" onLoad="airportcheckbox();enablecity();Customcheckbox();">
		<form id="Form1" method="post" runat="server" >
			<uc1:headerctrl id="Headerctrl1" runat="server"></uc1:headerctrl>
			<TABLE id="Table1" align="center">
				<asp:panel id="pnlmainform" Runat="server">
  <TBODY>
	<TR>
    <TD align=center colSpan=2><STRONG><U>A Add a Chauffeur Package</U></STRONG></TD>
	</TR>
	
	<TR>
    <TD align=center colSpan=2>&nbsp;&nbsp;</TD>
	</TR>
	
	<TR>
    <TD width="278">* Car Category </TD>
    <TD width="177"><asp:DropDownList id=ddlcarcat OnSelectedIndexChanged="ddlcarcat_SelectIndexChanged" runat="server" AutoPostBack="true"  CssClass="input"></asp:DropDownList></TD>
	</TR>
	
	<TR id="show">
    <TD> Car Model</TD>
    <TD><asp:DropDownList id=ddlcarModel runat="server" CssClass="input"></asp:DropDownList></TD>
	</TR>
	
	<TR>
    <TD>Tariff Type</TD>
    <TD><asp:DropDownList id=ddTariffType runat="server" cssclass="input">
		<asp:listitem Text="Special" Value="S" />
		<asp:listitem Text="National" Value="N" />
		</asp:DropDownList></TD>
	</TR>
	
	<TR>
    <TD>Airport Transfer</TD>
    <TD><asp:checkbox id=chkairport onclick=enablecity(); runat="server" CssClass="input"></asp:checkbox></TD>
	</TR>
	
	<TR>
    <TD>Pick-up City Name</TD>
    <TD><asp:DropDownList id=ddlcity runat="server" CssClass="input"></asp:DropDownList></TD>
	</TR>

	<TR id="show2">
	<TD>* No of Days</TD>
	<TD><asp:DropDownList id=ddldays runat="server" CssClass="input"></asp:DropDownList></TD>
	</TR>

	<TR id="show1">
    <TD>* No Of Night</TD>
    <TD><asp:DropDownList id=ddlngt runat="server" CssClass="input"></asp:DropDownList></TD>
	</TR>

	<TR>
    <TD>* Package Hours (1 Day in case of outstation)</TD>
    <TD><asp:DropDownList id=ddlpackage runat="server" CssClass="input"></asp:DropDownList></TD>
	</TR>

	<TR>
	<TD>* Package KMs</TD>
	<TD><asp:DropDownList id=ddlpckgkm runat="server" CssClass="input"></asp:DropDownList></TD>
	</TR>
	
	<TR>
	<TD>Outstation</TD>
	<TD><asp:checkbox id=chkoutstation onclick="airportcheckbox();" runat="server" CssClass="input"></asp:checkbox></TD>
	</TR>
		
	<TR>
	<TD>City Transter</TD>
	<TD><asp:checkbox id=chkcity onclick="citycheckbox();" runat="server" CssClass="input"></asp:checkbox></TD>
	</TR>
	
	<TR>
	<TD>Custom PkgYN</TD>
	<TD><asp:checkbox id=chkCustom onclick="Customcheckbox();" runat="server" CssClass="input"></asp:checkbox></TD>
	</TR>
	
	<TR>
    <TD>* Rate</TD>
    <TD><asp:textbox id=txtrate runat="server" CssClass="input" MaxLength="50"></asp:textbox></TD>
	</TR>

	<TR id="show4">
    <TD>Extra Day Charges</TD>
    <TD><asp:textbox id=txtdaychgs runat="server" CssClass="input" MaxLength="50"></asp:textbox></TD>
	</TR>

	<TR id="show3">
    <TD style="HEIGHT: 19px">Extra Night Charges</TD>
    <TD style="HEIGHT: 19px"><asp:textbox id=txtngtchgs runat="server" CssClass="input" MaxLength="50"></asp:textbox></TD>
	</TR>

	<TR>
    <TD style="HEIGHT: 19px">Rate / Extra Hr</TD>
    <TD style="HEIGHT: 19px"><asp:textbox id=txtratehr runat="server" CssClass="input" MaxLength="50"></asp:textbox></TD>
	</TR>

	<TR>
    <TD>Rate / Extra KM</TD>
    <TD><asp:textbox id=txtratekm runat="server" CssClass="input" MaxLength="50"></asp:textbox></TD>
	</TR>
	
	<TR>
    <TD>Threshold Extra Hr (Crosses to next package if reached)</TD>
    <TD><asp:DropDownList id=ddlHr runat="server">
		<asp:listitem Text="00" Value="0"/>
		<asp:listitem Text="01" Value="1"/>
		<asp:listitem Text="02" Value="2"/>
		</asp:DropDownList>
		<asp:DropDownList id=ddlMin runat="server">
		<asp:listitem Text="00" Value="0"/>
		<asp:listitem Text="15" Value="25"/>
		<asp:listitem Text="30" Value="50"/>
		<asp:listitem Text="45" Value="75"/>
		</asp:DropDownList></TD>
	</TR>
	
	<TR>
    <TD>Threshold Extra KM (Crosses to next package if reached)</TD>
    <TD><asp:textbox id=txtexkm runat="server" CssClass="input" MaxLength="50"></asp:textbox></TD>
	</TR>
	
	<TR>
    <TD>Outstation Allowance</TD>
    <TD><asp:textbox id=txtoutstation disabled runat="server" CssClass="input" MaxLength="50"></asp:textbox></TD>
	</TR>
	
	<TR>
    <TD>Night Stay Allowance</TD>
    <TD><asp:textbox id=txtnightstay runat="server" CssClass="input" MaxLength="50"></asp:textbox></TD>
	</TR>

	<TR id="show5">
    <TD>Taxes</TD>
    <TD><asp:textbox id=txttaxes runat="server" CssClass="input" MaxLength="50"></asp:textbox></TD>
	</TR>
	
	<TR id="show7">
    <TD>Drop-off City Name</TD>
    <TD><asp:DropDownList id="ddldropcity" name="ddldropcity"  runat="server" CssClass="input"></asp:DropDownList></TD>
	</TR>

	<TR id="show8">
    <TD>To Way Transfer YN</TD>
    <TD><asp:checkbox id=chktwoway  runat="server" CssClass="input"></asp:checkbox></TD>
	</TR>

	<TR id="show6">
    <TD vAlign=top>Description</TD>
    <TD><asp:textbox id=txtdescription runat="server" CssClass="input" MaxLength="500" Rows="3" Columns="50" TextMode="MultiLine"></asp:textbox></TD>
	</TR>

	<TR>
    <TD vAlign=top>Remarks(<SPAN class=shwText id=shwMessage>0</SPAN>/2000 chars)</TD>
    <TD><asp:textbox id=txtRemarks onkeydown="showLength(this.form.txtRemarks,'document.all.shwMessage',2000)" onkeyup="showLength(this.form.txtRemarks,'document.all.shwMessage',2000)" runat="server" CssClass="input" MaxLength="2000" Rows="3" Columns="50" TextMode="MultiLine"></asp:textbox></TD>
	</TR>
	
	<TR>
    <TD>Active</TD>
    <TD><asp:checkbox id=chkactive runat="server" CssClass="input" Checked="True"></asp:checkbox></TD>
	</TR>

	<TR>
    <TD align=center colSpan=2><asp:button id=btnsubmit runat="server" CssClass="button" Text="Submit"></asp:button>&nbsp;&nbsp; 
<asp:button id=btnreset runat="server" CssClass="button" Text="Reset" CausesValidation="False"></asp:button></TD>
	</TR>
	
	</asp:panel><asp:panel id="pnlconfirmation" Runat="server" Visible="False">
	
	<TR align=center>
    <TD colSpan=2><asp:Label id=lblMessage runat="server"></asp:Label></TD>
	</TR>
	
	<TR align=center>
    <TD colSpan=2><asp:HyperLink id=hyplnkretry runat="server"></asp:HyperLink></TD>
	</TR>
	
	</asp:panel></TBODY></TABLE>
	</form>
	</body>
</HTML>