<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Src="ChaufPkgsEditSearch.aspx.vb" Inherits="ChaufPkgsEditSearch"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CarzonRent :: Internal software control Panel</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<uc1:Headerctrl id="Headerctrl1" runat="server"></uc1:Headerctrl>
			<br>
			<br>
			<TABLE align="center">
				<TR>
					<TD colspan="2" align="center"><STRONG><U>Edit a Chauffeur Package where</U></STRONG>
						<br>
						<br>
					</TD>
				</TR>
				<TR>
					<TD>Package ID</TD>
					<TD>
						<asp:Textbox id="txtPkgID" runat="server" CssClass="input" size="10"></asp:Textbox></TD>
				</TR>
				<TR>
					<TD>Car Category</TD>
					<TD>
						<asp:DropDownList id="ddlcarcat" runat="server" CssClass="input"></asp:DropDownList></TD>
				</TR>
				<TR>
					<TD>Tariff Type</TD>
					<TD>
						 <asp:DropDownList id="ddTariffType" runat="server" cssclass="input">
				          <asp:listitem Text="Any" Value="" />
				          <asp:listitem Text="National" Value="N" />
				          <asp:listitem Text="Special" Value="S" />
				       </asp:DropDownList></TD>
				</TR>
				<TR>
					<TD>Airport Transfer?</TD>
					<TD>
						 <asp:DropDownList id="ddAptTrns" runat="server" cssclass="input">
				          <asp:listitem Text="Any" Value="" />
				          <asp:listitem Text="Yes" Value="1" />
				          <asp:listitem Text="No" Value="0" />
				       </asp:DropDownList></TD>
				</TR>
				<TR>
					<TD>City</TD>
					<TD>
						 <asp:DropDownList id="ddlcity" runat="server" cssclass="input">
				       </asp:DropDownList></TD>
				</TR>
				<TR>
					<TD>Package Hours</TD>
					<TD>
						 <asp:DropDownList id="ddlpackage" runat="server" cssclass="input">
				       </asp:DropDownList></TD>
				</TR>
				<TR>
					<TD>Package KMs</TD>
					<TD>
						 <asp:DropDownList id="ddlpckgkm" runat="server" cssclass="input">
				       </asp:DropDownList></TD>
				</TR>
				<TR>
					<TD>Outstation?</TD>
					<TD>
						 <asp:DropDownList id="ddOutStn" runat="server" cssclass="input">
				          <asp:listitem Text="Any" Value="" />
				          <asp:listitem Text="Yes" Value="1" />
				          <asp:listitem Text="No" Value="0" />
				       </asp:DropDownList></TD>
				</TR>
				<TR>
					<TD>Active?</TD>
					<TD>
						 <asp:DropDownList id="ddActive" runat="server" cssclass="input">
				          <asp:listitem Text="Any" Value="" />
				          <asp:listitem Text="Yes" Value="1" />
				          <asp:listitem Text="No" Value="0" />
				       </asp:DropDownList></TD>
				</TR>
				<TR>
					<TD>&nbsp;</TD>
					<TD></TD>
				</TR>
				<TR>
					<TD colspan="2" align="center">
						<asp:Button id="btnSubmit" runat="server" Text="Submit" CssClass="button"></asp:Button>
						<asp:Button id="btnReset" runat="server" Text="Reset" CssClass="button"></asp:Button></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
