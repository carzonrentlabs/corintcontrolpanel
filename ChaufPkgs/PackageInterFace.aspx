<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>

<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PackageInterFace.aspx.vb"
    Inherits="ChaufPkgs_PackageInterFace" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>CarzonRent :: Internal software Control panel</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="../HertzInt.css" type="text/css" rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script type="text/javascript" language="javascript">

        $(document).ready(function () {
            $("#<%=txtEffectiveDate.ClientID %>").datepicker();
        });


        function validate_input() {
            var RB1 = document.getElementById("<%=rdPkgOption.ClientID%>");
            var radio = RB1.getElementsByTagName("input");

            if (document.getElementById("<%=ddlcompanyname.ClientID%>").selectedIndex == 0) {
                alert("Please Select Client.")
                document.getElementById("<%=ddlcompanyname.ClientID%>").focus();
                return false;

            }
            else if (document.getElementById("<%=PkgFromSD.ClientID%>").value == "" && radio[0].checked == true) {
                alert("Please Package ID.")
                document.getElementById("<%=PkgFromSD.ClientID%>").focus();
                return false;
            }
            else if (document.getElementById("<%=PkgToSD.ClientID%>").value == "" && radio[0].checked == true) {
                alert("Please Package ID.")
                document.getElementById("<%=PkgToSD.ClientID%>").focus();
                return false;
            }
            else if (document.getElementById("<%=pkgMultiple.ClientID%>").value == "" && radio[1].checked == true) {
                alert("Enter package id with comma separated.")
                document.getElementById("<%=pkgMultiple.ClientID%>").focus();
                return false;
            }
}
function validate_input_Remove() {


    var RB1 = document.getElementById("<%=rdPkgOption.ClientID%>");
    var radio = RB1.getElementsByTagName("input");
    //alert(radio[0].checked);

    if (document.getElementById("<%=ddlcompanyname.ClientID%>").selectedIndex == 0) {
                alert("Please Select Client.")
                document.getElementById("<%=ddlcompanyname.ClientID%>").focus();
                return false;

            }
            else if (document.getElementById("<%=PkgFromSD.ClientID%>").value == "" && radio[0].checked == true) {
                alert("Please Package ID.")
                document.getElementById("<%=PkgFromSD.ClientID%>").focus();
                return false;
            }
            else if (document.getElementById("<%=PkgToSD.ClientID%>").value == "" && radio[0].checked == true) {
                alert("Please Package ID.")
                document.getElementById("<%=PkgToSD.ClientID%>").focus();
                return false;
            }
            else if (document.getElementById("<%=pkgMultiple.ClientID%>").value == "" && radio[1].checked == true) {
                alert("Enter package id with comma separated.")
                document.getElementById("<%=pkgMultiple.ClientID%>").focus();
                return false;
            }
    //            var where_to= confirm("Do you really want to go to this page??");
    //            if (where_to== true)
    //            {
    //                return true;
    //            }
    //            else
    //            {
    //                else false;
    //            }
}

function validate_EffectiveDate() {
    if (document.getElementById("<%=txtEffectiveDate.ClientID%>").value == "") {
        alert("Select Package Effective Date From.");
        document.getElementById("<%=txtEffectiveDate.ClientID %>").focus();
                return false;

            }

        }


    </script>
</head>
<body>
    <form id="Form1" method="post" runat="server">
        <uc1:Headerctrl ID="Headerctrl1" runat="server"></uc1:Headerctrl>
        <table align="center">
            <tbody>
                <tr>
                    <td colspan="3">
                        <asp:Label ID="lblMessage" runat="server" ForeColor="red"></asp:Label><br />
                        <asp:Label ID="lblPackageID" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;
                    </td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="3">Company Name &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:DropDownList ID="ddlcompanyname" runat="server" CssClass="input" AutoPostBack="true">
                    </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td colspan="3"></td>
                </tr>
                <tr>
                    <td colspan="3"></td>
                </tr>
                <tr>
                    <td colspan="3" align="left">Package Selection Option
                    <asp:RadioButtonList ID="rdPkgOption" runat="server" RepeatDirection="Horizontal"
                        AutoPostBack="true">
                        <asp:ListItem Text="From To" Value="1" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Multiple" Value="2"></asp:ListItem>
                    </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">Package IDs &nbsp;&nbsp;&nbsp;&nbsp; From:
                    <asp:TextBox ID="PkgFromSD" size="5" runat="server" CssClass="input"></asp:TextBox>
                        To:
                    <asp:TextBox ID="PkgToSD" size="5" runat="server" CssClass="input"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;&nbsp;&nbsp;&nbsp; <font color="red" size="2">Enter comma separated package id
                        (170992,170996,170999) </font>
                        <br />
                        <asp:TextBox ID="pkgMultiple" TextMode="MultiLine" Rows="5" Columns="50" runat="server"
                            CssClass="input" Enabled="false"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="3"></td>
                </tr>
                <tr>
                    <td colspan="3"></td>
                </tr>
                <tr>
                    <td colspan="3">Package Effective Date From &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:TextBox ID="txtEffectiveDate" runat="server" CssClass="input"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:FileUpload ID="UploadPackage" runat="server" /><asp:Button ID="btnUploadPkg"
                            runat="server" Text="Upload Package" OnClientClick="return validate_EffectiveDate();" />
                        &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btnAssignPkg" runat="server" Text="Assign Package" OnClientClick="return validate_input();" />
                        <asp:Button ID="btnRemove" runat="server" Text="Remove Package" OnClientClick="return validate_input_Remove();" />
                        <asp:Button ID="bntShowYN" runat="server" Text="No Show Package" OnClientClick="return validate_input_Remove();" />
                    </td>
                </tr>
                <tr>
                    <td colspan="3" align="left">
                        <b><u>Below is sample Excel File Format and file name should be (Package.xlsx) and sheet name should be (Sheet1)</u></b>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center" style="width: 100%">
                        <table border="1">
                            <tr style="background-color: Yellow;">
                                <td>CarModelName
                                </td>
                                <td>CarCatName
                                </td>
                                <td>CityName
                                </td>
                                <td>DropOffCityName
                                </td>
                                <td>PkgHrs
                                </td>
                                <td>PkgKMs
                                </td>
                                <td>PKPM
                                </td>
                                <td>LocalOneWayDropYN
                                </td>
                                <td>AptTransYN
                                </td>
                                <td>OutStationYN
                                </td>
                                <td>CityTransYN
                                </td>
                                <td>CustomPkgYN
                                </td>
                                <td>PkgRate
                                </td>
                                <td>ExtraHrRate
                                </td>
                                <td>ExtraKMRate
                                </td>
                                <td>ThresholdExtraHr
                                </td>
                                <td>ThresholdExtraKM
                                </td>
                                <td>OutStationAllowance
                                </td>
                                <td>NightStayAllowance
                                </td>
                                <td>Remarks
                                </td>
                                <td>Active
                                </td>
                                <td>ToNFroYN
                                </td>
                                <td>NoOfNgts
                                </td>
                                <td>NoOfDys
                                </td>
                                <td>ExNgtAmt
                                </td>
                                <td>ExdayAmt
                                </td>
                                <td>FxdTaxes
                                </td>
                                <td>Description
                                </td>
                                <td>WaitingCharges
                                </td>
                                <td>IsRateIncludingServiceTax
                                </td>
                                <td>NightChargeMultiplier
                                </td>
                                <td>FGRHrs
                                </td>
                                <td>FGRKms
                                </td>
                                <td>FuelMilage
                                </td>
                                <td>ContractFuelRate
                                </td>
                                <td>CurrentFuelRateMapCity
                                </td>
                            </tr>
                            <tr>
                                <td>Beat
                                </td>
                                <td>Intermediate
                                </td>
                                <td>Delhi
                                </td>
                                <td>&nbsp;
                                </td>
                                <td>4
                                </td>
                                <td>40
                                </td>
                                <td>0
                                </td>
                                <td>0
                                </td>
                                <td>0
                                </td>
                                <td>0
                                </td>
                                <td>0
                                </td>
                                <td>0
                                </td>
                                <td>1000
                                </td>
                                <td>120
                                </td>
                                <td>8
                                </td>
                                <td>1
                                </td>
                                <td>0
                                </td>
                                <td>0
                                </td>
                                <td>0
                                </td>
                                <td>Demo Package
                                </td>
                                <td>1
                                </td>
                                <td>0
                                </td>
                                <td>0
                                </td>
                                <td>0
                                </td>
                                <td>0
                                </td>
                                <td>0
                                </td>
                                <td>0
                                </td>
                                <td>&nbsp;
                                </td>
                                <td>0
                                </td>
                                <td>0
                                </td>
                                <td>0
                                </td>
                                <td>0
                                </td>
                                <td>0
                                </td>
                                <td>10
                                </td>
                                <td>90
                                </td>
                                <td>Delhi
                                </td>
                            </tr>
                            <tr>
                                <td>City
                                </td>
                                <td>Standard
                                </td>
                                <td>Delhi
                                </td>
                                <td>&nbsp;
                                </td>
                                <td>4
                                </td>
                                <td>40
                                </td>
                                <td>0
                                </td>
                                <td>0
                                </td>
                                <td>0
                                </td>
                                <td>0
                                </td>
                                <td>0
                                </td>
                                <td>0
                                </td>
                                <td>1200
                                </td>
                                <td>100
                                </td>
                                <td>12
                                </td>
                                <td>1
                                </td>
                                <td>0
                                </td>
                                <td>0
                                </td>
                                <td>0
                                </td>
                                <td>Demo Package
                                </td>
                                <td>1
                                </td>
                                <td>0
                                </td>
                                <td>0
                                </td>
                                <td>0
                                </td>
                                <td>0
                                </td>
                                <td>0
                                </td>
                                <td>0
                                </td>
                                <td>&nbsp;
                                </td>
                                <td>0
                                </td>
                                <td>0
                                </td>
                                <td>0
                                </td>
                                <td>1
                                </td>
                                <td>20
                                </td>
                                <td>12
                                </td>
                                <td>93
                                </td>
                                <td>Bangalore
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</body>
</html>
