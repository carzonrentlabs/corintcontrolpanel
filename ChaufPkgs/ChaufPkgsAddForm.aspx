<%@ Page Language="vb" AutoEventWireup="false" Src="ChaufPkgsAddForm.aspx.vb" Inherits="ChaufPkgsAddForm" %>

<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
    'dim ddldropcityid1
    'ddldropcityid1=request.form("ddldropcityid")
%>
<html>
<head>
    <title>CarzonRent :: Internal software Control panel</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <script language="JavaScript" src="../utilityfunction.js" type="text/jscript"></script>
    <link href="../HertzInt.css" type="text/css" rel="stylesheet">
    <script language="javascript" type="text/javascript">
        function validation() {
            if (isNaN(document.forms[0].txtrate.value)) {
                alert("Rate should be numeric and upto two decimal digits only.")
                return false;
            }
            else {
                if ((document.forms[0].txtrate.value == 0) || (document.forms[0].txtrate.value == "")) {
                    alert("Please enter package rate")
                    return false;
                }
                if (parseFloat(document.forms[0].txtrate.value.indexOf(".")) > 0) {
                    var FlotVal
                    FlotVal = document.forms[0].txtrate.value.substr(parseFloat(document.forms[0].txtrate.value.indexOf(".")), document.forms[0].txtrate.value.length)
                    if (parseFloat((FlotVal.length) - 1) > 2) {
                        alert("Rate should be numeric and upto two decimal digits only")
                        return false;
                    }
                }
            }


            if (isNaN(document.forms[0].txtwaiting.value)) {
                alert("Waiting Charges should be numeric and upto two decimal digits only.")
                document.forms[0].txtwaiting.focus();
                document.forms[0].txtwaiting.value = 0;
                return false;
            }
            //if((document.forms[0].chkairport.checked==true) && (document.forms[0].ddlcity.value==""))
            //{
            //	alert("Please select the city.");
            //	return false;
            //}

            if (isNaN(document.forms[0].txtratehr.value)) {
                alert("Rate / Extra Hr should be numeric and upto two decimal digits only.")
                return false;
            }
            else {
                if (parseFloat(document.forms[0].txtratehr.value.indexOf(".")) > 0) {
                    var FlotVal
                    FlotVal = document.forms[0].txtratehr.value.substr(parseFloat(document.forms[0].txtratehr.value.indexOf(".")), document.forms[0].txtratehr.value.length)
                    if (parseFloat((FlotVal.length) - 1) > 2) {
                        alert("Rate / Extra Hr should be numeric and upto two decimal digits only")
                        return false;
                    }
                }
            }

            if (document.forms[0].chkoutstation.checked == true && document.getElementById('<%=ddldropcity.ClientID%>').selectedIndex == 0) {
                //&& document.forms[0].ddldropcity.SelectedItem.Value== 0

                if (document.forms[0].txtratekm.value == "" || document.forms[0].txtratekm.value == 0) {
                    alert("Please enter extra KM rate");
                    //alert(document.form1.ddldropcity.selectedItem.value);
                    //alert(document.getElementById('<%=ddldropcity.ClientID%>').selectedIndex);
                    return false;
                }
            }

            if (isNaN(document.forms[0].txtratekm.value)) {
                alert("Rate / Extra KM should be numeric and upto two decimal digits only.")
                return false;
            }
            else {
                if (parseFloat(document.forms[0].txtratekm.value.indexOf(".")) > 0) {
                    var FlotVal
                    FlotVal = document.forms[0].txtratekm.value.substr(parseFloat(document.forms[0].txtratekm.value.indexOf(".")), document.forms[0].txtratekm.value.length)
                    if (parseFloat((FlotVal.length) - 1) > 2) {
                        alert("Rate / Extra KM should be numeric and upto two decimal digits only")
                        return false;
                    }
                }
            }

            if (isNaN(document.forms[0].txtexkm.value)) {
                alert("Threshold Extra KM (Crosses to next package if reached) should be numeric and upto two decimal digits only.")
                return false;
            }

            if (isNaN(document.forms[0].txtoutstation.value)) {
                alert("Outstation Allowance should be numeric and upto two decimal digits only.")
                return false;
            }
            else {
                if (parseFloat(document.forms[0].txtoutstation.value.indexOf(".")) > 0) {
                    var FlotVal
                    FlotVal = document.forms[0].txtoutstation.value.substr(parseFloat(document.forms[0].txtoutstation.value.indexOf(".")), document.forms[0].txtoutstation.value.length)
                    if (parseFloat((FlotVal.length) - 1) > 2) {
                        alert("Outstation Allowance KM should be numeric and upto two decimal digits only")
                        return false;
                    }
                }
            }

            if (isNaN(document.forms[0].txtnightstay.value)) {
                alert("Night Stay Allowance should be numeric and upto two decimal digits only.")
                return false;
            }
            else {
                if (parseFloat(document.forms[0].txtnightstay.value.indexOf(".")) > 0) {
                    var FlotVal
                    FlotVal = document.forms[0].txtnightstay.value.substr(parseFloat(document.forms[0].txtnightstay.value.indexOf(".")), document.forms[0].txtnightstay.value.length)
                    if (parseFloat((FlotVal.length) - 1) > 2) {
                        alert("Night Stay Allowance should be numeric and upto two decimal digits only")
                        return false;
                    }

                }
            }

            if (isNaN(document.forms[0].ddlngt.value) > isNaN(document.forms[0].ddldays.value)) {
                alert("Nights must be less than days.");
                document.forms[0].ddldays.focus();
                return false;
            }



            var strvalues
            if (document.forms[0].chkCustom.checked == true) {
                strvalues = ('ddlcarcat,txtrate,ddlpackage,ddlpckgkm,ddldays,ddlngt')
            }
            else {
                strvalues = ('ddlcarcat,txtrate,ddlpackage,ddlpckgkm')
            }
            return checkmandatory(strvalues);
        }

        function CalcOSRate() {
            if (document.forms[0].chkoutstation.checked == true) {
                if (document.forms[0].ddlpckgkm.value != "" && document.forms[0].txtratekm.value != "") {
                    document.forms[0].txtrate.value = parseFloat(document.forms[0].ddlpckgkm.value) * document.forms[0].txtratekm.value;
                }
            }
        }

        function airportcheckbox() {
            if (document.forms[0].chkoutstation.checked == true) {
                document.forms[0].chkairport.checked = false;
                document.forms[0].chkCustom.checked = false;
                document.forms[0].chkcity.checked = false;
                //document.forms[0].ddlcity.disabled=true
                //document.forms[0].chktwoway.Enabled=true;	
                document.forms[0].chktwoway.disabled = false;
                document.forms[0].ddldropcity.disabled = false;
                document.forms[0].txtoutstation.disabled = false;
                document.forms[0].txtnightstay.disabled = false;
                document.forms[0].ddlpackage.value = 1;
                document.forms[0].ddlpackage.disabled = true;
                document.forms[0].txtratehr.disabled = true;
                //document.forms[0].txthr.disabled=true;
                document.forms[0].txtexkm.disabled = true;
                document.getElementById('show').style.display = "none";
                document.getElementById('show1').style.display = "none";
                document.getElementById('show2').style.display = "none";
                document.getElementById('show3').style.display = "none";
                document.getElementById('show4').style.display = "none";
                document.getElementById('show5').style.display = "none";
                document.getElementById('show6').style.display = "none";
                //document.getElementById('show7').style.display="block";
                //document.getElementById('show8').style.display="block";

                if (document.forms[0].ddlpckgkm.value != "" && document.forms[0].txtratekm.value != "") {
                    document.forms[0].txtrate.value = parseFloat(document.forms[0].ddlpckgkm.value) * document.forms[0].txtratekm.value;
                }
                else {
                    document.forms[0].txtrate.value = "";
                }
            }
            else {
                //document.getElementById('show7').style.display="none";
                //document.getElementById('show8').style.display="none";
                document.forms[0].txtrate.value = "";
                document.forms[0].ddlpackage.value = 0;
                //document.forms[0].chktwoway.checked=false;
                document.forms[0].chktwoway.disabled = true;
                document.forms[0].chktwoway.checked = false;
                document.forms[0].ddldropcity.value = "";
                document.forms[0].ddldropcity.disabled = true;
                document.forms[0].ddlpackage.disabled = false;
                document.forms[0].txtratehr.disabled = false;
                //document.forms[0].txthr.disabled=false;
                //document.forms[0].txtexkm.disabled=false;
                //document.forms[0].txtoutstation.disabled=true;
            }
        }

        function enablecity() {
            if (document.forms[0].chkairport.checked == true) {
                //document.forms[0].ddlcity.disabled=false
                document.forms[0].chkcity.checked = false;
                document.forms[0].chkoutstation.checked = false;
                document.forms[0].txtoutstation.disabled = true;
                document.forms[0].chkCustom.checked = false;
                document.forms[0].txtnightstay.disabled = true;
                document.forms[0].ddlpackage.value = 0;
                document.forms[0].ddlpackage.disabled = false;
                document.forms[0].txtratehr.disabled = false;
                //document.forms[0].txthr.disabled=false;
                document.forms[0].txtexkm.disabled = false;
                document.forms[0].ddldropcity.disabled = true;
                document.forms[0].ddldropcity.value = "";
                //document.forms[0].chktwoway.disabled=true;
                document.forms[0].chktwoway.checked = false;
                document.forms[0].chktwoway.disabled = false;
                document.getElementById('show').style.display = "none";
                document.getElementById('show1').style.display = "none";
                document.getElementById('show2').style.display = "none";
                document.getElementById('show3').style.display = "none";
                document.getElementById('show4').style.display = "none";
                document.getElementById('show5').style.display = "none";
                document.getElementById('show6').style.display = "none";
                //document.getElementById('show7').style.display="none";
                //document.getElementById('show8').style.display="block";
            }
            else {
                //document.forms[0].ddlcity.disabled=true
                //document.forms[0].txtoutstation.disabled=false;
                //document.getElementById('show7').style.display="none";
                //document.getElementById('show8').style.display="none";
                document.forms[0].txtnightstay.disabled = false;
                document.forms[0].chktwoway.checked = false;
                document.forms[0].chktwoway.disabled = true;
            }
        }

        function citycheckbox() {
            if (document.forms[0].chkcity.checked == true) {
                document.forms[0].chkoutstation.checked = false;
                document.forms[0].chkairport.checked = false;
                document.forms[0].txtoutstation.disabled = true;
                document.forms[0].chkCustom.checked = false;
                document.forms[0].txtnightstay.disabled = true;
                document.forms[0].ddlpackage.value = 0;
                document.forms[0].ddlpackage.disabled = false;
                document.forms[0].txtratehr.disabled = false;
                document.forms[0].txtexkm.disabled = false;
                document.forms[0].ddldropcity.disabled = true;
                document.forms[0].ddldropcity.value = "";
                document.forms[0].chktwoway.checked = false;
                document.forms[0].chktwoway.disabled = true;
                document.getElementById('show').style.display = "none";
                document.getElementById('show1').style.display = "none";
                document.getElementById('show2').style.display = "none";
                document.getElementById('show3').style.display = "none";
                document.getElementById('show4').style.display = "none";
                document.getElementById('show5').style.display = "none";
                document.getElementById('show6').style.display = "none";
                //document.getElementById('show7').style.display="none";
                //document.getElementById('show8').style.display="block";
            }
            else {
                document.forms[0].txtnightstay.disabled = false;
                document.forms[0].chktwoway.checked = false;
                document.forms[0].chktwoway.disabled = true;
            }
        }

        function Customcheckbox() {
            if (document.forms[0].chkCustom.checked == true) {
                document.forms[0].chkcity.checked = false;
                document.forms[0].ddlpackage.disabled = false;
                document.forms[0].ddlpackage.value = 0;
                document.forms[0].ddlpckgkm.value = 0;
                //document.forms[0].ddlpackage.value="";
                document.forms[0].chkairport.checked = false;
                document.forms[0].chkoutstation.checked = false;
                document.forms[0].ddldropcity.disabled = true;
                document.forms[0].ddldropcity.value = "";
                document.forms[0].txtratehr.disabled = false;
                document.forms[0].txtexkm.disabled = false;
                document.forms[0].txtoutstation.disabled = false;
                document.getElementById('show').style.display = "block";
                document.getElementById('show1').style.display = "block";
                document.getElementById('show2').style.display = "block";
                document.getElementById('show3').style.display = "block";
                document.getElementById('show4').style.display = "block";
                document.getElementById('show5').style.display = "block";
                document.getElementById('show6').style.display = "block";
                document.getElementById('show7').style.display = "none";
                document.getElementById('show8').style.display = "none";
            }
            else {
                document.getElementById('show').style.display = "none";
                document.getElementById('show1').style.display = "none";
                document.getElementById('show2').style.display = "none";
                document.getElementById('show3').style.display = "none";
                document.getElementById('show4').style.display = "none";
                document.getElementById('show5').style.display = "none";
                document.getElementById('show6').style.display = "none";
                document.getElementById('show7').style.display = "block";
                document.getElementById('show8').style.display = "block";
                document.forms[0].txtoutstation.disabled = true;
                document.forms[0].ddldropcity.disabled = true;
                document.forms[0].ddldropcity.value = "";
                document.forms[0].chktwoway.checked = false;
                document.forms[0].chktwoway.disabled = true;
            }
        }


    </script>
</head>
<body ms_positioning="GridLayout" onload="airportcheckbox();enablecity();Customcheckbox();">
    <form id="Form1" method="post" runat="server">
        <uc1:Headerctrl ID="Headerctrl1" runat="server"></uc1:Headerctrl>
        <table id="Table1" align="center">
            <asp:Panel ID="pnlmainform" runat="server">
                <tbody>
                    <tr>
                        <td align="center" colspan="2"><strong><u>A Add a Chauffeur Package</u></strong></td>
                    </tr>

                    <tr>
                        <td align="center" colspan="2">&nbsp;&nbsp;</td>
                    </tr>

                    <tr>
                        <td width="278">* Car Category </td>
                        <td width="177">
                            <asp:DropDownList ID="ddlcarcat" OnSelectedIndexChanged="ddlcarcat_SelectIndexChanged" runat="server" AutoPostBack="true" CssClass="input"></asp:DropDownList></td>
                    </tr>

                    <tr>
                        <div id="show">
                            <td>Car Model</td>
                            <td>
                                <asp:DropDownList ID="ddlcarModel" runat="server" CssClass="input"></asp:DropDownList></td>
                        </div>
                    </tr>

                    <tr>
                        <td>Tariff Type</td>
                        <td>
                            <asp:DropDownList ID="ddTariffType" runat="server" CssClass="input">
                                <asp:ListItem Text="Special" Value="S" />
                                <asp:ListItem Text="National" Value="N" />
                            </asp:DropDownList></td>
                    </tr>

                    <tr>
                        <td>Airport Transfer</td>
                        <td>
                            <asp:CheckBox ID="chkairport" onclick="enablecity();" runat="server" CssClass="input"></asp:CheckBox></td>
                    </tr>

                    <tr>
                        <td>Pick-up City Name</td>
                        <td>
                            <asp:DropDownList ID="ddlcity" runat="server" CssClass="input"></asp:DropDownList></td>
                    </tr>

                    <tr>
                        <div id="show2">
                            <td>* No of Days</td>
                            <td>
                                <asp:DropDownList ID="ddldays" runat="server" CssClass="input"></asp:DropDownList></td>
                        </div>
                    </tr>

                    <tr>
                        <div id="show1">
                            <td>* No Of Night</td>
                            <td>
                                <asp:DropDownList ID="ddlngt" runat="server" CssClass="input"></asp:DropDownList></td>
                        </div>
                    </tr>

                    <tr>
                        <td>* Package Hours (1 Day in case of outstation)</td>
                        <td>
                            <asp:DropDownList ID="ddlpackage" runat="server" CssClass="input"></asp:DropDownList></td>
                    </tr>

                    <tr>
                        <td>* Package KMs</td>
                        <td>
                            <asp:DropDownList ID="ddlpckgkm" runat="server" CssClass="input"></asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td>Outstation</td>
                        <td>
                            <asp:CheckBox ID="chkoutstation" onclick="airportcheckbox();" runat="server" CssClass="input"></asp:CheckBox></td>
                    </tr>
                    <tr>
                        <td>City Transter</td>
                        <td>
                            <asp:CheckBox ID="chkcity" onclick="citycheckbox();" runat="server" CssClass="input"></asp:CheckBox></td>
                    </tr>

                    <tr>
                        <td>Custom PkgYN</td>
                        <td>
                            <asp:CheckBox ID="chkCustom" onclick="Customcheckbox();" runat="server" CssClass="input" AutoPostBack="true"></asp:CheckBox></td>
                    </tr>

                    <tr>
                        <td>PKPM PkgYN</td>
                        <td>
                            <asp:CheckBox ID="chkPKPM" onclick="Customcheckbox();" runat="server" CssClass="input" AutoPostBack="true"></asp:CheckBox></td>
                    </tr>

                    <tr>
                        <td>LocalOneWayDropYN</td>
                        <td>
                            <asp:CheckBox ID="chkLocalOneWay" onclick="Customcheckbox();" runat="server" CssClass="input" AutoPostBack="true"></asp:CheckBox></td>
                    </tr>

                    <tr>
                        <td>* Rate</td>
                        <td>
                            <asp:TextBox ID="txtrate" runat="server" CssClass="input" MaxLength="50"></asp:TextBox></td>
                    </tr>

                    <tr>
                        <div id="show4">
                            <td>Extra Day Charges</td>
                            <td>
                                <asp:TextBox ID="txtdaychgs" runat="server" CssClass="input" MaxLength="50"></asp:TextBox></td>
                        </div>
                    </tr>

                    <tr>
                        <div id="show3">
                            <td style="height: 19px">Extra Night Charges</td>
                            <td style="height: 19px">
                                <asp:TextBox ID="txtngtchgs" runat="server" CssClass="input" MaxLength="50"></asp:TextBox></td>
                        </div>
                    </tr>

                    <tr>
                        <td style="height: 19px">Rate / Extra Hr</td>
                        <td style="height: 19px">
                            <asp:TextBox ID="txtratehr" runat="server" CssClass="input" MaxLength="50"></asp:TextBox></td>
                    </tr>

                    <tr>
                        <td>Rate / Extra KM</td>
                        <td>
                            <asp:TextBox ID="txtratekm" runat="server" CssClass="input" MaxLength="50"></asp:TextBox></td>
                    </tr>

                    <tr>
                        <td>Threshold Extra Hr (Crosses to next package if reached)</td>
                        <td>
                            <asp:DropDownList ID="ddlHr" runat="server">
                                <asp:ListItem Text="00" Value="0" />
                                <asp:ListItem Text="01" Value="1" />
                                <asp:ListItem Text="02" Value="2" />
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddlMin" runat="server">
                                <asp:ListItem Text="00" Value="0" />
                                <asp:ListItem Text="15" Value="25" />
                                <asp:ListItem Text="30" Value="50" />
                                <asp:ListItem Text="45" Value="75" />
                            </asp:DropDownList></td>
                    </tr>

                    <tr>
                        <td>Threshold Extra KM (Crosses to next package if reached)</td>
                        <td>
                            <asp:TextBox ID="txtexkm" runat="server" CssClass="input" MaxLength="50"></asp:TextBox></td>
                    </tr>

                    <tr>
                        <td>Outstation Allowance</td>
                        <td>
                            <asp:TextBox ID="txtoutstation" disabled runat="server" CssClass="input" MaxLength="50"></asp:TextBox></td>
                    </tr>

                    <tr>
                        <td>Night Stay Allowance</td>
                        <td>
                            <asp:TextBox ID="txtnightstay" runat="server" CssClass="input" MaxLength="50"></asp:TextBox></td>
                    </tr>

                    <tr>
                        <div id="show5">
                            <td>Taxes</td>
                            <td>
                                <asp:TextBox ID="txttaxes" runat="server" CssClass="input" MaxLength="50"></asp:TextBox></td>
                        </div>
                    </tr>

                    <tr>
                        <td>Waiting Charges</td>
                        <td>
                            <asp:TextBox ID="txtwaiting" runat="server" CssClass="input" MaxLength="50"></asp:TextBox></td>
                    </tr>

                    <tr>
                        <div id="show7">
                            <td>Drop-off City Name</td>
                            <td>
                                <asp:DropDownList ID="ddldropcity" name="ddldropcity" runat="server" CssClass="input"></asp:DropDownList></td>
                        </div>
                    </tr>

                    <tr>
                        <div id="show8">
                            <td>To Way Transfer YN</td>
                            <td>
                                <asp:CheckBox ID="chktwoway" runat="server" CssClass="input"></asp:CheckBox></td>
                        </div>
                    </tr>

                    <tr>
                        <td>FGR Hrs
                        </td>
                        <td>
                            <asp:TextBox ID="txtfgrhrs" runat="server" CssClass="input" MaxLength="2"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td>FGR Kms
                        </td>
                        <td>
                            <asp:TextBox ID="txtfgrkms" runat="server" CssClass="input" MaxLength="2"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <div id="show6">
                            <td valign="top">Description</td>
                            <td>
                                <asp:TextBox ID="txtdescription" runat="server" CssClass="input" MaxLength="500" Rows="3" Columns="50" TextMode="MultiLine"></asp:TextBox></td>
                        </div>
                    </tr>

                    <tr>
                        <td valign="top">Remarks(<span class="shwText" id="shwMessage">0</span>/2000 chars)</td>
                        <td>
                            <asp:TextBox ID="txtRemarks" onkeydown="showLength(this.form.txtRemarks,'document.all.shwMessage',2000)" onkeyup="showLength(this.form.txtRemarks,'document.all.shwMessage',2000)" runat="server" CssClass="input" MaxLength="2000" Rows="3" Columns="50" TextMode="MultiLine"></asp:TextBox></td>
                    </tr>

                    <tr>
                        <td>Active</td>
                        <td>
                            <asp:CheckBox ID="chkactive" runat="server" CssClass="input" Checked="True"></asp:CheckBox></td>
                    </tr>

                    <tr>
                        <td align="center" colspan="2">
                            <asp:Button ID="btnsubmit" runat="server" CssClass="button" Text="Submit"></asp:Button>&nbsp;&nbsp; 
                            <asp:Button ID="btnreset" runat="server" CssClass="button" Text="Reset" CausesValidation="False"></asp:Button></td>
                    </tr>

            </asp:Panel>
            <asp:Panel ID="pnlconfirmation" runat="server" Visible="False">

                <tr align="center">
                    <td colspan="2">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label></td>
                </tr>

                <tr align="center">
                    <td colspan="2">
                        <asp:HyperLink ID="hyplnkretry" runat="server"></asp:HyperLink></td>
                </tr>

            </asp:Panel>
            </TBODY>
        </table>
    </form>
</body>
</html>
