Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class ChaufPkgsEditForm
    Inherits System.Web.UI.Page
    Dim txthr As String

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ddlcarcat As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlcarModel As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddTariffType As System.Web.UI.WebControls.DropDownList
    Protected WithEvents chkairport As System.Web.UI.WebControls.CheckBox
    Protected WithEvents ddlcity As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddldropcity As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlpackage As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlpckgkm As System.Web.UI.WebControls.DropDownList
    Protected WithEvents chkoutstation As System.Web.UI.WebControls.CheckBox
    Protected WithEvents txtrate As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtratehr As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtratekm As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtexkm As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtoutstation As System.Web.UI.WebControls.TextBox
    Protected WithEvents chktwoway As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkCustom As System.Web.UI.WebControls.CheckBox
    Protected WithEvents txtnightstay As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtRemarks As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkactive As System.Web.UI.WebControls.CheckBox
    Protected WithEvents btnsubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnreset As System.Web.UI.WebControls.Button
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    Protected WithEvents ddlHr As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlMin As System.Web.UI.WebControls.DropDownList
    Protected WithEvents chkcity As System.Web.UI.WebControls.CheckBox
    Protected WithEvents ddldays As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlngt As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtngtchgs As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtdaychgs As System.Web.UI.WebControls.TextBox
    Protected WithEvents txttaxes As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtwaiting As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtdescription As System.Web.UI.WebControls.TextBox
    Protected WithEvents hdoutstation As System.Web.UI.WebControls.HiddenField
    Protected WithEvents chkPKPM As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkLocalOneWay As System.Web.UI.WebControls.CheckBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        'btnsubmit.Attributes("onClick") = "return  validation();"
        If Not Page.IsPostBack Then
            populateddl()
            Dim dtrreader As SqlDataReader
            Dim accessdata As clsutility
            accessdata = New clsutility
            Dim valDisc As String
            Dim valDiscDec As String
            Dim arrDisc As Array

            dtrreader = accessdata.funcGetSQLDataReader("select PkgID,CarCatID,AptTransYN,CityID,isnull(PkgHrs,0) as PkgHrs,isnull(PkgKMs,0) as PkgKMs,OutStationYN,PkgRate,ExtraHrRate,ExtraKMRate,ThresholdExtraHr,ThresholdExtraKM,OutStationAllowance,NightStayAllowance,Remarks,Active,CreateDate,CreatedBy,ModifyDate,ModifiedBy,TariffType,CityTransYN,DropOffCityID,ToNFroYN,isnull(CustomPkgYN,0) as CustomPkgYN,CarModelID,isnull(NoOfNgts,0) as NoOfNgts,isnull(NoOfDys,0) as NoOfDys,ExNgtAmt,ExdayAmt,FxdTaxes,Description,isnull(WaitingCharges,0) as WaitingCharges, ISNULL(IsMinuteWiseBilling,0) AS IsMinuteWiseBilling, ISNULL(LocalOneWayDropYN,0) AS LocalOneWayDropYN from dbo.CORIntChaufPkgsMaster where  pkgid=" & Request.QueryString("id") & " ")
            dtrreader.Read()

            autoselec_ddl(ddlcarcat, dtrreader("carcatid"))
            populatemodel()

            If IsDBNull(dtrreader("CarModelID")) = True Then
                ddlcarModel.SelectedValue = ""
            Else
                autoselec_ddl(ddlcarModel, dtrreader("CarModelID"))
            End If

            chkairport.Checked = dtrreader("apttransyn")

            If IsDBNull(dtrreader("cityid")) = True Then
                ddlcity.SelectedValue = ""
            Else
                autoselec_ddl(ddlcity, dtrreader("cityid"))
            End If

            If IsDBNull(dtrreader("DropOffCityID")) = True Then
                ddldropcity.SelectedValue = ""
            Else
                autoselec_ddl(ddldropcity, dtrreader("DropOffCityID"))
            End If
            chkCustom.Checked = dtrreader("CustomPkgYN")

            'autoselec_ddl(ddlcity, dtrreader("cityid"))

            If IsDBNull(dtrreader("pkghrs")) = True Then
                ddlpackage.SelectedValue = 0
            Else
                autoselec_ddl(ddlpackage, dtrreader("pkghrs"))
            End If

            If IsDBNull(dtrreader("pkgkms")) = True Then
                ddlpckgkm.SelectedValue = 0
            Else
                autoselec_ddl(ddlpckgkm, dtrreader("pkgkms"))
            End If

            If IsDBNull(dtrreader("NoOfNgts")) = True Then
                ddlngt.SelectedValue = 0
            Else
                autoselec_ddl(ddlngt, dtrreader("NoOfNgts"))
            End If

            If IsDBNull(dtrreader("NoOfDys")) = True Then
                ddldays.SelectedValue = 0
            Else
                autoselec_ddl(ddldays, dtrreader("NoOfDys"))
            End If

            chkoutstation.Checked = dtrreader("outstationyn")
            chktwoway.Checked = dtrreader("ToNFroYN")

            txtrate.Text = dtrreader("pkgrate") & ""
            txtngtchgs.Text = dtrreader("ExNgtAmt") & ""
            txtdaychgs.Text = dtrreader("ExdayAmt") & ""

            txtratehr.Text = dtrreader("extrahrrate") & ""
            txtratekm.Text = dtrreader("extrakmrate") & ""
            txttaxes.Text = dtrreader("FxdTaxes") & ""
            txtwaiting.Text = dtrreader("WaitingCharges") & ""
            txtdescription.Text = dtrreader("Description") & ""

            txthr = dtrreader("thresholdextrahr") & ""

            arrDisc = CStr(txthr).Split(".")

            If arrDisc.Length = 2 Then
                valDisc = arrDisc(0)
                valDiscDec = arrDisc(1)
                If valDisc = "" Then valDisc = "0"
                If valDiscDec = "00" Or valDiscDec = "" Then valDiscDec = "0"
            End If

            autoselec_ddl(ddlHr, valDisc)
            autoselec_ddl(ddlMin, valDiscDec)

            txtexkm.Text = dtrreader("thresholdextrakm") & ""
            txtoutstation.Text = dtrreader("outstationallowance") & ""
            hdoutstation.Value = dtrreader("outstationallowance") & ""
            txtnightstay.Text = dtrreader("nightstayallowance") & ""

            txtRemarks.Text = dtrreader("remarks") & ""
            'ddTariffType.Items.FindByValue(dtrreader("TariffType")).Selected = True
            chkactive.Checked = dtrreader("active")

            If (chkactive.Checked = False) Then
                chkactive.Enabled = False
            Else
                chkactive.Enabled = True
            End If

            chkcity.Checked = dtrreader("CityTransYN")

            chkPKPM.Checked = dtrreader("IsMinuteWiseBilling")
            chkLocalOneWay.Checked = dtrreader("LocalOneWayDropYN")

            dtrreader.Close()
            accessdata.Dispose()

        End If
    End Sub
    Function autoselec_ddl(ByVal ddlname As DropDownList, ByVal selectvalue As Int32)
        If Not selectvalue = "0" Then
            Try
                ddlname.Items.FindByValue(selectvalue).Selected = True
            Catch ex As Exception

            End Try

        End If
    End Function
    Sub populatemodel()
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility

        ddlcarModel.DataSource = objAcessdata.funcGetSQLDataReader("select CarModelName ,CarModelID  from dbo.CORIntCarModelMaster where CarCatID = " & ddlcarcat.SelectedItem.Value & " order by CarModelName")
        ddlcarModel.DataValueField = "CarModelID"
        ddlcarModel.DataTextField = "CarModelName"
        ddlcarModel.DataBind()
        ddlcarModel.Items.Insert(0, New ListItem("", ""))
        objAcessdata.Dispose()
    End Sub

    Sub populateddl()
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility

        ddlcity.DataSource = objAcessdata.funcGetSQLDataReader("select CityName ,CityID  from dbo.CORIntCityMaster order by CityName")
        ddlcity.DataValueField = "CityID"
        ddlcity.DataTextField = "CityName"
        ddlcity.DataBind()
        ddlcity.Items.Insert(0, New ListItem("", ""))

        ddldropcity.DataSource = objAcessdata.funcGetSQLDataReader("select CityName ,CityID  from dbo.CORIntCityMaster order by CityName")
        ddldropcity.DataValueField = "CityID"
        ddldropcity.DataTextField = "CityName"
        ddldropcity.DataBind()
        ddldropcity.Items.Insert(0, New ListItem("", ""))

        ddlcarcat.DataSource = objAcessdata.funcGetSQLDataReader("select CarCatName ,CarCatID  from dbo.CORIntCarCatMaster order by CarCatName")
        ddlcarcat.DataValueField = "CarCatID"
        ddlcarcat.DataTextField = "CarCatName"
        ddlcarcat.DataBind()
        ddlcarcat.Items.Insert(0, New ListItem("", ""))

        objAcessdata.funcpopulatenumddw(31, 0, ddlngt)
        objAcessdata.funcpopulatenumddw(31, 0, ddldays)

        objAcessdata.funcpopulatenumddw(1000, 0, ddlpackage)
        objAcessdata.funcpopulatenumddw(5000, 0, ddlpckgkm)

        objAcessdata.Dispose()

    End Sub

    Private Sub btnreset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnreset.Click
        Response.Redirect("ChaufPkgsEditSearch.aspx")
    End Sub

    Private Sub btnsubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("corConnectString"))
        Dim cmd As SqlCommand

        'If (ddldropcity.SelectedItem.Value = "") Then
        'ddldropcity.SelectedItem.Value = 0
        'End If

        'If (ddlcity.SelectedItem.Value = "" Or ddlcity.SelectedItem.Value = "0") Then
        'MyConnection.Close()
        'Response.Write("Please Select Pick-up City Name")
        'Response.End()
        'End If

        'If (get_YNvalue(chkoutstation) = 1) Then
        ''If (get_YNvalue(chktwoway) = 1 And ddldropcity.SelectedItem.Value = 0) Then
        'If (get_YNvalue(chktwoway) = 1 And ddldropcity.SelectedItem.Value = 0) Then
        'MyConnection.Close()
        'Response.Write("Please Select Drop Off City")
        'Response.End()
        'End If
        'End If

        'cmd = New SqlCommand("procEditChaufPkgsMaster_1", MyConnection)
        'cmd = New SqlCommand("procEditChaufPkgsMaster", MyConnection)
        cmd = New SqlCommand("procDeactivateChaufPkgsMaster", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.AddWithValue("@rowid", Request.QueryString("id"))

        'cmd.Parameters.AddWithValue("@carcatid", ddlcarcat.SelectedItem.Value)
        'cmd.Parameters.AddWithValue("@TariffType", ddTariffType.SelectedItem.Value)
        'cmd.Parameters.AddWithValue("@apttransyn", get_YNvalue(chkairport))
        'cmd.Parameters.AddWithValue("@cityid", ddlcity.SelectedItem.Value)
        'cmd.Parameters.AddWithValue("@ddldropcity", ddldropcity.SelectedItem.Value)
        'If Not ddlpackage.SelectedItem.Value = "" Then
        'cmd.Parameters.AddWithValue("@pkghrs", ddlpackage.SelectedItem.Value)
        'End If
        'cmd.Parameters.AddWithValue("@pkgkms", ddlpckgkm.SelectedItem.Value)
        'cmd.Parameters.AddWithValue("@outstationyn", get_YNvalue(chkoutstation))
        'cmd.Parameters.AddWithValue("@chktwoway", get_YNvalue(chktwoway))
        'cmd.Parameters.AddWithValue("@pkgrate", txtrate.Text)

        'If Not txtratehr.Text = "" Then
        'cmd.Parameters.AddWithValue("@extrahrrate", txtratehr.Text)
        'End If
        'If Not txtratekm.Text = "" Then
        'cmd.Parameters.AddWithValue("@extrakmrate", txtratekm.Text)
        'End If
        'txthr = ddlHr.SelectedItem.Value + "." + ddlMin.SelectedItem.Value
        'cmd.Parameters.AddWithValue("@thresholdextrahr", txthr)
        'If Not txtexkm.Text = "" Then
        'cmd.Parameters.AddWithValue("@thresholdextrakm", txtexkm.Text)
        'End If
        'If Not txtoutstation.Text = "" Then
        'cmd.Parameters.Add("@outstationallowance", txtoutstation.Text)
        'cmd.Parameters.Add("@outstationallowance", hdoutstation.Value())
        'End If
        'cmd.Parameters.AddWithValue("@outstationallowance", hdoutstation.Value())
        'If Not txtnightstay.Text = "" Then
        'cmd.Parameters.AddWithValue("@nightstayallowance", txtnightstay.Text)
        'End If
        cmd.Parameters.AddWithValue("@remarks", txtRemarks.Text)
        cmd.Parameters.AddWithValue("@active", get_YNvalue(chkactive))
        cmd.Parameters.AddWithValue("@modifiedby", Session("loggedin_user"))

        'Added By Rahul on 11 June 2010
        'cmd.Parameters.AddWithValue("@citytransyn", get_YNvalue(chkcity))
        'If Not ddlcarModel.SelectedItem.Value = "" Then
        'cmd.Parameters.AddWithValue("@CarModelID", ddlcarModel.SelectedItem.Value)
        'End If
        'If Not ddlngt.SelectedItem.Value = "" Then
        'cmd.Parameters.AddWithValue("@NoOfNgts", ddlngt.SelectedItem.Value)
        'End If
        'If Not ddldays.SelectedItem.Value = "" Then
        'cmd.Parameters.AddWithValue("@NoOfDys", ddldays.SelectedItem.Value)
        'End If
        'If Not txtngtchgs.Text = "" Then
        'cmd.Parameters.AddWithValue("@ExNgtAmt", txtngtchgs.Text)
        'End If
        'If Not txtdaychgs.Text = "" Then
        'cmd.Parameters.AddWithValue("@ExDayAmt", txtdaychgs.Text)
        'End If
        'If Not txttaxes.Text = "" Then
        'cmd.Parameters.AddWithValue("@FxdTaxes", txttaxes.Text)
        'End If
        'cmd.Parameters.AddWithValue("@Description", txtdescription.Text)
        'cmd.Parameters.AddWithValue("@CustomPkgYN", get_YNvalue(chkCustom))
        'If txtwaiting.Text = "" Then
        'txtwaiting.Text = 0
        'End If
        'cmd.Parameters.AddWithValue("@WaitingCharges", txtwaiting.Text) 'Waiting charges txtwaiting.Text
        'cmd.Parameters.AddWithValue("@IsMinuteWiseBilling", get_YNvalue(chkPKPM))
        'cmd.Parameters.AddWithValue("@LocalOneWayDropYN", get_YNvalue(chkLocalOneWay))
        'End of column added by Rahul

        MyConnection.Open()
        cmd.ExecuteNonQuery()
        MyConnection.Close()
        pnlmainform.Visible = False
        pnlconfirmation.Visible = True

        lblMessage.Text = "You have Deactivated the Chauffeur Package successfully"
        hyplnkretry.Text = "Deactivate another Chauffeur Package"
        hyplnkretry.NavigateUrl = "ChaufPkgsEditSearch.aspx"
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function
End Class