Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class ChaufPkgsAddForm
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtRemarks As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkactive As System.Web.UI.WebControls.CheckBox
    Protected WithEvents btnsubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnreset As System.Web.UI.WebControls.Button
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    Protected WithEvents ddlcarcat As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlcarModel As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddTariffType As System.Web.UI.WebControls.DropDownList
    Protected WithEvents chkairport As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkcity As System.Web.UI.WebControls.CheckBox
    Protected WithEvents ddlcity As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddldropcity As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlpackage As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlpckgkm As System.Web.UI.WebControls.DropDownList
    Protected WithEvents chkoutstation As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chktwoway As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkCustom As System.Web.UI.WebControls.CheckBox
    Protected WithEvents txtrate As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtratehr As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtratekm As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtexkm As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtoutstation As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtnightstay As System.Web.UI.WebControls.TextBox
    Protected WithEvents txthr As System.Web.UI.WebControls.TextBox
    Protected WithEvents DropDownList1 As System.Web.UI.WebControls.DropDownList
    Protected WithEvents DropDownList2 As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlHr As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlMin As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddldays As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlngt As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtngtchgs As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtdaychgs As System.Web.UI.WebControls.TextBox
    Protected WithEvents txttaxes As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtdescription As System.Web.UI.WebControls.TextBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btnsubmit.Attributes("onClick") = "return  validation();"
        If Not Page.IsPostBack Then
            populateddl()
        End If
    End Sub
    Sub populateddl()
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility

        ddlcity.DataSource = objAcessdata.funcGetSQLDataReader("select CityName ,CityID  from CORIntCityMaster   where active=1 order by CityName")
        ddlcity.DataValueField = "CityID"
        ddlcity.DataTextField = "CityName"
        ddlcity.DataBind()
        ddlcity.Items.Insert(0, New ListItem("", ""))

        ddldropcity.DataSource = objAcessdata.funcGetSQLDataReader("select CityName ,CityID  from CORIntCityMaster   where active=1 order by CityName")
        ddldropcity.DataValueField = "CityID"
        ddldropcity.DataTextField = "CityName"
        ddldropcity.DataBind()
        ddldropcity.Items.Insert(0, New ListItem("", ""))

        ddlcarcat.DataSource = objAcessdata.funcGetSQLDataReader("select CarCatName ,CarCatID  from CORIntCarCatMaster (nolock) where active=1 order by CarCatName")
        ddlcarcat.DataValueField = "CarCatID"
        ddlcarcat.DataTextField = "CarCatName"
        ddlcarcat.DataBind()
        ddlcarcat.Items.Insert(0, New ListItem("", ""))

        objAcessdata.funcpopulatenumddw(1000, 0, ddlpackage)
        objAcessdata.funcpopulatenumddw(5000, 0, ddlpckgkm)


        objAcessdata.Dispose()

    End Sub
    Public Sub ddlcarcat_SelectIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'ddlcarModel()
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility
        ddlcarModel.DataSource = objAcessdata.funcGetSQLDataReader("select CarModelName ,CarModelID  from CORIntCarModelMaster (nolock) where active=1 and CarCatID = " & ddlcarcat.SelectedItem.Value & " order by CarModelName")
        ddlcarModel.DataValueField = "CarModelID"
        ddlcarModel.DataTextField = "CarModelName"
        ddlcarModel.DataBind()
        ddlcarModel.Items.Insert(0, New ListItem("", ""))
        objAcessdata.funcpopulatenumddw(31, 0, ddlngt)
        objAcessdata.funcpopulatenumddw(31, 0, ddldays)

        objAcessdata.Dispose()
        'Response.Write("You are here..")
    End Sub

    Private Sub btnsubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsubmit.Click

        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        Dim intuniqcheck As SqlParameter
        Dim intuniqvalue as int32
        Dim intPkgidParam As SqlParameter
        Dim intPkgid as int32
        Dim txtHr As String
        If (ddldropcity.SelectedItem.Value = "") Then
            ddldropcity.SelectedItem.Value = 0
        End If

        If txtngtchgs.Text = "" Then
            txtngtchgs.Text = 0
        End If
        If txtdaychgs.Text = "" Then
            txtdaychgs.Text = 0
        End If

        If (get_YNvalue(chkoutstation) = 1) Then
            If (get_YNvalue(chktwoway) = 1 And ddldropcity.SelectedItem.Value = 0) Then
                Response.Write("Please Select Drop Off City")
                Response.End()
            End If
        End If

        cmd = New SqlCommand("procAddChaufPkgsMaster_1", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("@carcatid", ddlcarcat.SelectedItem.Value)
        cmd.Parameters.Add("@TariffType", ddTariffType.SelectedItem.Value)
        cmd.Parameters.Add("@apttransyn", get_YNvalue(chkairport))
        cmd.Parameters.Add("@cityid", ddlcity.SelectedItem.Value)

        If Not ddlpackage.SelectedItem.Value = "" Then
            cmd.Parameters.Add("@pkghrs", ddlpackage.SelectedItem.Value)
        End If
        cmd.Parameters.Add("@pkgkms", ddlpckgkm.SelectedItem.Value)
        cmd.Parameters.Add("@outstationyn", get_YNvalue(chkoutstation))
        cmd.Parameters.Add("@pkgrate", txtrate.Text)
        If Not txtratehr.Text = "" Then
            cmd.Parameters.Add("@extrahrrate", txtratehr.Text)
        End If
        If Not txtratekm.Text = "" Then
            cmd.Parameters.Add("@extrakmrate", txtratekm.Text)
        End If

        If get_YNvalue(chkCustom) = 1 Then
            If ddlpackage.SelectedItem.Value = "" Then
                ddlpackage.SelectedItem.Value = 0
            End If
            If ddlpckgkm.SelectedItem.Value = "" Then
                ddlpckgkm.SelectedItem.Value = 0
            End If
        End If

        txtHr = ddlHr.SelectedItem.Value + "." + ddlMin.SelectedItem.Value
        cmd.Parameters.Add("@thresholdextrahr", txtHr)
        If Not txtexkm.Text = "" Then
            cmd.Parameters.Add("@thresholdextrakm", txtexkm.Text)
        End If
        If Not txtoutstation.Text = "" Then
            cmd.Parameters.Add("@outstationallowance", txtoutstation.Text)
        End If
        If Not txtnightstay.Text = "" Then
            cmd.Parameters.Add("@nightstayallowance", txtnightstay.Text)
        End If
        cmd.Parameters.Add("@twoway", get_YNvalue(chktwoway))
        cmd.Parameters.Add("@dropcityid", ddldropcity.SelectedItem.Value)
        cmd.Parameters.Add("@remarks", txtRemarks.Text)
        cmd.Parameters.Add("@active", get_YNvalue(chkactive))
        cmd.Parameters.Add("@createdby", Session("loggedin_user"))

        'Added By Rahul on 11 June 2010
        cmd.Parameters.Add("@citytransyn", get_YNvalue(chkcity))
        If Not ddlcarModel.SelectedItem.Value = "" Then
            cmd.Parameters.Add("@CarModelID", ddlcarModel.SelectedItem.Value)
        End If
        If Not ddlngt.SelectedItem.Value = "" Then
            cmd.Parameters.Add("@NoOfNgts", ddlngt.SelectedItem.Value)
        End If
        If Not ddldays.SelectedItem.Value = "" Then
            cmd.Parameters.Add("@NoOfDys", ddldays.SelectedItem.Value)
        End If
        If Not txtngtchgs.Text = "" Then
            cmd.Parameters.Add("@ExNgtAmt", txtngtchgs.Text)
        End If
        If Not txtdaychgs.Text = "" Then
            cmd.Parameters.Add("@ExDayAmt", txtdaychgs.Text)
        End If
        If Not txttaxes.Text = "" Then
            cmd.Parameters.Add("@FxdTaxes", txttaxes.Text)
        End If
        cmd.Parameters.Add("@Description", "") 'txtdescription.Text)
        cmd.Parameters.Add("@CustomPkgYN", get_YNvalue(chkCustom))
        'End of column added by Rahul

        intuniqcheck = cmd.Parameters.Add("@uniqcheckval", SqlDbType.Int)
        intuniqcheck.Direction = ParameterDirection.Output
        intPkgidParam = cmd.Parameters.Add("@Pkgid", SqlDbType.Int)
        intPkgidParam.Direction = ParameterDirection.Output


        '  Try
        MyConnection.Open()
        cmd.ExecuteNonQuery()
        intuniqvalue = cmd.Parameters("@uniqcheckval").Value
        intPkgid = cmd.Parameters("@Pkgid").Value
        MyConnection.Close()
        If Not intuniqvalue = 0 Then
            pnlmainform.Visible = False
            pnlconfirmation.Visible = True

            lblMessage.Text = "This particular package already exists!<br> The Package ID is <b>" & intPkgid & "</b>"
            hyplnkretry.Text = "Add another Chauffeur Package"
            hyplnkretry.NavigateUrl = "ChaufPkgsAddForm.aspx"
        Else

            pnlmainform.Visible = False
            pnlconfirmation.Visible = True

            lblMessage.Text = "You have added the Chauffeur Package successfully <br> The Package ID is <b>" & intPkgid & "</b>"
            hyplnkretry.Text = "Add another Chauffeur Package"
            hyplnkretry.NavigateUrl = "ChaufPkgsAddForm.aspx"
        End If
        '   Catch
        '   End Try
    End Sub

    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function

End Class
