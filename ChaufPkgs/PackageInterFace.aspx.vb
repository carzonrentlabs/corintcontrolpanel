Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Imports System.Data.Sql
Imports System.Data.OleDb
Imports System.IO
Partial Class ChaufPkgs_PackageInterFace
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Text = ""
        lblPackageID.Text = ""
        'Dim formattedDate As String = DateTime.Now.ToString("dd-MMM-yyyy")

        'Response.Write(formattedDate)
        'txtEffectiveDate.Text = Date.Now().ToShortDateString()

        If Not Page.IsPostBack Then
            Dim dtrreader As SqlDataReader
            Dim accessdata As clsutility
            accessdata = New clsutility
            dtrreader = accessdata.funcGetSQLDataReader("select ClientCoID ,ClientCoName  from dbo.CORIntClientCoMaster WITH (NOLOCK) where TariffType='S' and active=1 order by ClientCoName")
            ddlcompanyname.DataSource = dtrreader
            ddlcompanyname.DataValueField = "ClientCoID"
            ddlcompanyname.DataTextField = "ClientCoName"
            ddlcompanyname.DataBind()
            ddlcompanyname.Items.Insert(0, New ListItem("Any", -1))
            dtrreader.Close()
        End If

    End Sub

    Protected Sub btnUploadPkg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUploadPkg.Click

        Dim Newfilename As String
        Dim strFile As String
        Dim filepath As String
        Dim filename As String
        Dim Dt As DataTable = New DataTable()
        Dim Adap As SqlDataAdapter

        filepath = "D:\CorInt\ChauffeurPkg\"

        If Not Directory.Exists(filepath) Then
            filepath = "F:\CorInt\ChauffeurPkg\"
        End If


        Dim di As DirectoryInfo = New DirectoryInfo(filepath)
        If di.Exists Then
            'MsgBox("That path exists already.")
        Else
            di.Create()
        End If

        If UploadPackage.PostedFile.FileName <> "" And UploadPackage.PostedFile.ContentLength > 0 Then
            strFile = Path.GetFileName(UploadPackage.FileName)
            'End If
            filename = Path.GetFileName(UploadPackage.FileName)

            If File.Exists(filepath + filename) Then
                File.Delete(filepath + filename)
            End If
            If strFile <> "" And txtEffectiveDate.Text <> "" Then

                If filename = "Package.xlsx" Or filename = "Package.xls" Then

                    Newfilename = System.DateTime.Now.Year.ToString() + "_" + System.DateTime.Now.Month.ToString() + "_" + System.DateTime.Now.Day.ToString() + "_" + System.DateTime.Now.Hour.ToString() + "_" + System.DateTime.Now.Minute.ToString() + "_" + System.DateTime.Now.Second.ToString() + ".xlsx"

                    UploadPackage.SaveAs(filepath + Newfilename)

                    'End If
                    'Dim connStringExcel As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=D:\CorInt\ChauffeurPkg\Package.xlsx;Extended Properties=""Excel 12.0;HDR=YES;"""
                    Dim connStringExcel As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filepath + Newfilename + ";Extended Properties=""Excel 12.0;HDR=YES;"""
                    Dim excelConn As New OleDbConnection(connStringExcel)
                    Dim excelCmd As New OleDbCommand("Select * From [Sheet1$]", excelConn)
                    'Dim sqlCommand1 As SqlCommand
                    'Dim sqlCommand2 As SqlCommand
                    'Dim sqlCommand3 As SqlCommand
                    Try
                        excelConn.Open()
                        Dim excelReader As OleDbDataReader = excelCmd.ExecuteReader()
                        Dim Pkglist As New ArrayList
                        Dim Citylist As New ArrayList
                        Dim CarCatlist As New ArrayList
                        Dim intPkgidParam As SqlParameter
                        lblPackageID.Text = ""
                        Dim MyConnection As SqlConnection
                        MyConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("corConnectString"))
                        Try
                            MyConnection.Open()
                            Dim num As Integer
                            While (excelReader.Read())

                                If IsNothing(excelReader("CarCatName")) Or IsDBNull(excelReader("CarCatName")) Then
                                    'If (lblPackageID.Text = "") Then
                                    '    lblMessage.Text = "Sheet Format is not correct!,"
                                    'Else
                                    '    lblPackageID.Text += "Incorrect Data!,"
                                    'End If
                                Else
                                    Dim Str As New StringBuilder

                                    Dim cmd As SqlCommand
                                    Dim intPkgID As Int32

                                    cmd = New SqlCommand("Prc_Upload_Pkg", MyConnection)
                                    'cmd = New SqlCommand("Prc_Upload_Pkg_New", MyConnection)
                                    cmd.CommandType = CommandType.StoredProcedure
                                    cmd.CommandTimeout = 6000
                                    cmd.Parameters.AddWithValue("@CarmodelName", excelReader("Carmodelname").ToString())
                                    cmd.Parameters.AddWithValue("@carcatname", excelReader("CarCatName").ToString())
                                    cmd.Parameters.AddWithValue("@CityName", excelReader("CityName").ToString())
                                    cmd.Parameters.AddWithValue("@DropCityName", excelReader("DropOffCityName").ToString())
                                    cmd.Parameters.AddWithValue("@PkgHrs", Convert.ToDouble(excelReader("PkgHrs")))
                                    cmd.Parameters.AddWithValue("@PkgKMs", Convert.ToDouble(excelReader("PkgKMs")))
                                    cmd.Parameters.AddWithValue("@AptTransYN", Convert.ToBoolean(excelReader("AptTransYN")))
                                    cmd.Parameters.AddWithValue("@OutStationYN", Convert.ToBoolean(excelReader("OutStationYN")))
                                    cmd.Parameters.AddWithValue("@CityTransYN", Convert.ToBoolean(excelReader("CityTransYN")))
                                    cmd.Parameters.AddWithValue("@CustomPkgYN", Convert.ToBoolean(excelReader("CustomPkgYN")))

                                    cmd.Parameters.AddWithValue("@PkgRate", Convert.ToDouble(excelReader("PkgRate")))
                                    cmd.Parameters.AddWithValue("@ExtraHrRate", Convert.ToDouble(excelReader("ExtraHrRate")))
                                    cmd.Parameters.AddWithValue("@ExtraKMRate", Convert.ToDouble(excelReader("ExtraKMRate")))

                                    cmd.Parameters.AddWithValue("@ThresholdExtraHr", Convert.ToDouble(excelReader("ThresholdExtraHr")))
                                    cmd.Parameters.AddWithValue("@ThresholdExtraKM", Convert.ToDouble(excelReader("ThresholdExtraKM")))
                                    cmd.Parameters.AddWithValue("@OutStationAllowance", Convert.ToDouble(excelReader("OutStationAllowance")))
                                    cmd.Parameters.AddWithValue("@NightStayAllowance", Convert.ToDouble(excelReader("NightStayAllowance")))
                                    cmd.Parameters.AddWithValue("@Remarks", excelReader("Remarks").ToString())
                                    cmd.Parameters.AddWithValue("@Active", Convert.ToBoolean(excelReader("Active")))
                                    cmd.Parameters.AddWithValue("@loggedin_user", Convert.ToInt32(Session("loggedin_user")))
                                    'cmd.Parameters.AddWithValue("@TariffType", "S") 'excelReader("TariffType").ToString())

                                    cmd.Parameters.AddWithValue("@ToNFroYN", Convert.ToBoolean(excelReader("ToNFroYN")))

                                    cmd.Parameters.AddWithValue("@NoOfNgts", Convert.ToInt32(excelReader("NoOfNgts")))
                                    cmd.Parameters.AddWithValue("@NoOfDys", Convert.ToInt32(excelReader("NoOfDys")))
                                    cmd.Parameters.AddWithValue("@ExNgtAmt", Convert.ToDouble(excelReader("ExNgtAmt")))
                                    cmd.Parameters.AddWithValue("@ExdayAmt", Convert.ToDouble(excelReader("ExdayAmt")))
                                    cmd.Parameters.AddWithValue("@FxdTaxes", Convert.ToDouble(excelReader("FxdTaxes")))
                                    cmd.Parameters.AddWithValue("@Description", excelReader("Description").ToString())

                                    cmd.Parameters.AddWithValue("@WaitingCharges", Convert.ToDouble(excelReader("WaitingCharges")))
                                    cmd.Parameters.AddWithValue("@EffectiveToDate", Convert.ToDateTime(txtEffectiveDate.Text.ToString()))
                                    cmd.Parameters.AddWithValue("@IsRateIncludingServiceTax", Convert.ToBoolean(excelReader("IsRateIncludingServiceTax")))
                                    cmd.Parameters.AddWithValue("@IsMinuteWiseBilling", Convert.ToBoolean(excelReader("PKPM")))
                                    cmd.Parameters.AddWithValue("@NightChargeMultiplier", Convert.ToDouble(excelReader("NightChargeMultiplier")))
                                    'If IsNothing(excelReader("LocalOneWayDropYN")) Or IsDBNull(excelReader("LocalOneWayDropYN")) Then
                                    If IsNothing(excelReader("LocalOneWayDropYN")) Then
                                        cmd.Parameters.AddWithValue("@LocalOneWayDropYN", Convert.ToBoolean(0))
                                    Else
                                        cmd.Parameters.AddWithValue("@LocalOneWayDropYN", Convert.ToBoolean(excelReader("LocalOneWayDropYN")))
                                    End If

                                    If IsNothing(excelReader("FGRHrs")) Then
                                        cmd.Parameters.AddWithValue("@FGRHrs", Convert.ToDouble(0))
                                    Else
                                        cmd.Parameters.AddWithValue("@FGRHrs", Convert.ToDouble(excelReader("FGRHrs")))
                                    End If

                                    If IsNothing(excelReader("FGRKms")) Then
                                        cmd.Parameters.AddWithValue("@FGRKms", Convert.ToDouble(0))
                                    Else
                                        cmd.Parameters.AddWithValue("@FGRKms", Convert.ToDouble(excelReader("FGRKms")))
                                    End If

                                    cmd.Parameters.AddWithValue("@ModelFuelAvg", Convert.ToDouble(excelReader("FuelMilage")))

                                    cmd.Parameters.AddWithValue("@ContractFuelRate", Convert.ToDouble(excelReader("ContractFuelRate")))

                                    cmd.Parameters.AddWithValue("@CurrentFuelRateMapCity", Convert.ToString(excelReader("CurrentFuelRateMapCity")))


                                    intPkgidParam = cmd.Parameters.Add("@Pkgid", SqlDbType.Int)
                                    intPkgidParam.Direction = ParameterDirection.Output
                                    cmd.ExecuteNonQuery()

                                    intPkgID = cmd.Parameters("@Pkgid").Value

                                    If (Convert.ToInt32(intPkgID) <> 0) Then
                                        If (lblPackageID.Text = "") Then
                                            lblPackageID.Text = "Package Id's ="
                                        End If
                                        If (Convert.ToInt32(intPkgID) = -1) Then
                                            lblPackageID.Text += "Incorrect Car Model Name,"
                                        End If
                                        If (Convert.ToInt32(intPkgID) = -2) Then
                                            lblPackageID.Text += "Incorrect City Name,"
                                        End If
                                        If (Convert.ToInt32(intPkgID) = -3) Then
                                            lblPackageID.Text += "Incorrect Car Category Name,"
                                        End If
                                        If (Convert.ToInt32(intPkgID) = -4) Then
                                            lblPackageID.Text += "City Name is Required,"
                                        End If
                                        If (Convert.ToInt32(intPkgID) = -5) Then
                                            lblPackageID.Text += "Enter Fuel Average,"
                                        End If
                                        If (Convert.ToInt32(intPkgID) = -6) Then
                                            lblPackageID.Text += "Enter Contract Fuel Rate,"
                                        End If
                                        If (Convert.ToInt32(intPkgID) > 0) Then
                                            lblPackageID.Text += Convert.ToString(intPkgID) + ","
                                        End If
                                    End If
                                End If
                            End While
                            excelConn.Close()
                            excelConn.Dispose()
                            If (lblMessage.Text = "") Then
                                lblMessage.Text = "Packages Insert successfully!"
                            End If

                        Catch exs As Exception
                            lblMessage.Text = exs.Message
                        Finally
                            MyConnection.Close()
                        End Try
                    Catch exo As Exception
                        lblMessage.Text = exo.Message
                    Finally

                    End Try
                Else
                    lblMessage.Text = "Please select package file!"
                End If
            End If

        Else
            lblMessage.Text = "Please Select File!"
        End If

    End Sub

    Protected Sub btnAssignPkg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAssignPkg.Click

        Try
            Dim MyConnection As SqlConnection
            Dim SqlCmd As SqlCommand
            'Dim InsertSqlCmd As SqlCommand
            Dim InsertSqlCmd1 As SqlCommand
            Dim Dt As DataTable = New DataTable()
            Dim Adap As SqlDataAdapter
            Dim num As Integer
            Dim PackageId As String

            MyConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("corConnectString"))
            If rdPkgOption.SelectedValue = 1 Then
                SqlCmd = New SqlCommand("select Pkgid from CORIntChaufPkgsMaster where pkgid between " + PkgFromSD.Text + " and " + PkgToSD.Text + "", MyConnection)
                MyConnection.Open()
                Adap = New SqlDataAdapter(SqlCmd)
                Adap.Fill(Dt)
                Dim Str As New StringBuilder
                MyConnection.Close()
                For num = 0 To Dt.Rows.Count - 1

                    InsertSqlCmd1 = New SqlCommand("procAssignedPkgsCDCo_New", MyConnection)
                    InsertSqlCmd1.CommandType = CommandType.StoredProcedure
                    InsertSqlCmd1.Parameters.AddWithValue("@PkgID", Convert.ToString(Dt.Rows(num)("Pkgid")))
                    InsertSqlCmd1.Parameters.AddWithValue("@ClientID", ddlcompanyname.SelectedItem.Value)
                    MyConnection.Open()
                    Try
                        InsertSqlCmd1.ExecuteNonQuery()
                    Catch exp As SqlException
                    End Try
                    MyConnection.Close()
                Next
            Else
                PackageId = pkgMultiple.Text
                PackageId = PackageId.Replace("\r\n", ",")
                PackageId = PackageId.Replace(",,", ",")
                If PackageId.EndsWith(",") Then
                    Dim LengthRequired As Integer
                    LengthRequired = PackageId.LastIndexOf(",")
                    If (LengthRequired >= 0) Then
                        PackageId = PackageId.Substring(0, LengthRequired)
                    End If
                End If
                Dim words() As String = PackageId.Split(",")
                For Each pkgid As String In words
                    SqlCmd = New SqlCommand("select Pkgid from CORIntChaufPkgsMaster where pkgid between " + pkgid.ToString + " and " + pkgid.ToString + "", MyConnection)
                    MyConnection.Open()
                    InsertSqlCmd1 = New SqlCommand("procAssignedPkgsCDCo_New", MyConnection)
                    InsertSqlCmd1.CommandType = CommandType.StoredProcedure
                    InsertSqlCmd1.Parameters.AddWithValue("@PkgID", pkgid)
                    InsertSqlCmd1.Parameters.AddWithValue("@ClientID", ddlcompanyname.SelectedItem.Value)
                    Try
                        InsertSqlCmd1.ExecuteNonQuery()
                    Catch exp As SqlException
                    End Try
                    MyConnection.Close()

                Next
            End If

        Catch ex As Exception
            Response.Write(ex.Message.ToString())
        Finally
            'Response.Redirect("PackageInterFace.aspx")
            lblMessage.Text = "Record Added successfully"
            PkgFromSD.Text = ""
            PkgToSD.Text = ""


        End Try

    End Sub

    Protected Sub btnRemove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemove.Click
        Dim MyConnection As SqlConnection
        Dim SqlCmd As SqlCommand
        Dim InsertSqlCmd As SqlCommand
        Dim PackageId As String
        If ddlcompanyname.SelectedItem.Value <> -1 Then
            MyConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("corConnectString"))
            If rdPkgOption.SelectedValue = 1 Then
                InsertSqlCmd = New SqlCommand("procDelAssignPkg_new", MyConnection)
                InsertSqlCmd.CommandType = CommandType.StoredProcedure
                InsertSqlCmd.Parameters.AddWithValue("@ClientType", "C")
                InsertSqlCmd.Parameters.AddWithValue("@ClientID", ddlcompanyname.SelectedItem.Value)
                InsertSqlCmd.Parameters.AddWithValue("@pkgIDFrom", Convert.ToInt32(PkgFromSD.Text))
                InsertSqlCmd.Parameters.AddWithValue("@pkgIDTo", Convert.ToInt32(PkgToSD.Text))
				InsertSqlCmd.Parameters.AddWithValue("@SysUserID", Convert.ToInt32(Session("loggedin_user")))				
                MyConnection.Open()
                Try
                    InsertSqlCmd.ExecuteNonQuery()
                Catch exp As SqlException
                End Try
                MyConnection.Close()
            Else
                PackageId = pkgMultiple.Text
                PackageId = PackageId.Replace("\r\n", ",")
                PackageId = PackageId.Replace(",,", ",")
                If PackageId.EndsWith(",") Then
                    Dim LengthRequired As Integer
                    LengthRequired = PackageId.LastIndexOf(",")
                    If (LengthRequired >= 0) Then
                        PackageId = PackageId.Substring(0, LengthRequired)
                    End If
                End If
                Dim words() As String = PackageId.Split(",")
                For Each pkgid As String In words
                    InsertSqlCmd = New SqlCommand("procDelAssignPkg_new", MyConnection)
                    InsertSqlCmd.CommandType = CommandType.StoredProcedure
                    InsertSqlCmd.Parameters.AddWithValue("@ClientType", "C")
                    InsertSqlCmd.Parameters.AddWithValue("@ClientID", ddlcompanyname.SelectedItem.Value)
                    InsertSqlCmd.Parameters.AddWithValue("@pkgIDFrom", pkgid)
                    InsertSqlCmd.Parameters.AddWithValue("@pkgIDTo", pkgid)
					InsertSqlCmd.Parameters.AddWithValue("@SysUserID", Convert.ToInt32(Session("loggedin_user")))
                    MyConnection.Open()
                    Try
                        InsertSqlCmd.ExecuteNonQuery()
                    Catch exp As SqlException
                    End Try
                    MyConnection.Close()
                Next

            End If
            lblMessage.Text = "Package Remove!"
            PkgFromSD.Text = ""
            PkgToSD.Text = ""
        Else
            lblMessage.Text = "Please Select Client!"

        End If


    End Sub

    Protected Sub bntShowYN_Click(sender As Object, e As System.EventArgs) Handles bntShowYN.Click

        Dim MyConnection As SqlConnection
        Dim SqlCmd As SqlCommand
        Dim InsertSqlCmd As SqlCommand
        Dim PackageId As String
        If ddlcompanyname.SelectedItem.Value <> -1 Then
            MyConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("corConnectString"))
            If rdPkgOption.SelectedValue = 1 Then
                InsertSqlCmd = New SqlCommand("Prc_NoShowPackage", MyConnection)
                InsertSqlCmd.CommandType = CommandType.StoredProcedure
                InsertSqlCmd.Parameters.AddWithValue("@ClientType", "C")
                InsertSqlCmd.Parameters.AddWithValue("@ClientID", ddlcompanyname.SelectedItem.Value)
                InsertSqlCmd.Parameters.AddWithValue("@pkgIDFrom", Convert.ToInt32(PkgFromSD.Text))
                InsertSqlCmd.Parameters.AddWithValue("@pkgIDTo", Convert.ToInt32(PkgToSD.Text))
                InsertSqlCmd.Parameters.AddWithValue("@SysUserId", Convert.ToInt32(Session("loggedin_user")))
                MyConnection.Open()
                Try
                    InsertSqlCmd.ExecuteNonQuery()
                Catch exp As SqlException
                End Try
                MyConnection.Close()
            Else
                PackageId = pkgMultiple.Text
                PackageId = PackageId.Replace("\r\n", ",")
                PackageId = PackageId.Replace(",,", ",")
                If PackageId.EndsWith(",") Then
                    Dim LengthRequired As Integer
                    LengthRequired = PackageId.LastIndexOf(",")
                    If (LengthRequired >= 0) Then
                        PackageId = PackageId.Substring(0, LengthRequired)
                    End If
                End If
                Dim words() As String = PackageId.Split(",")
                For Each pkgid As String In words
                    InsertSqlCmd = New SqlCommand("Prc_NoShowPackage", MyConnection)
                    InsertSqlCmd.CommandType = CommandType.StoredProcedure
                    InsertSqlCmd.Parameters.AddWithValue("@ClientType", "C")
                    InsertSqlCmd.Parameters.AddWithValue("@ClientID", ddlcompanyname.SelectedItem.Value)
                    InsertSqlCmd.Parameters.AddWithValue("@pkgIDFrom", pkgid)
                    InsertSqlCmd.Parameters.AddWithValue("@pkgIDTo", pkgid)
                    InsertSqlCmd.Parameters.AddWithValue("@SysUserId", Convert.ToInt32(Session("loggedin_user")))
                    MyConnection.Open()
                    Try
                        InsertSqlCmd.ExecuteNonQuery()
                    Catch exp As SqlException
                    End Try
                    MyConnection.Close()
                Next
            End If

            lblMessage.Text = "No Show successfully."
            PkgFromSD.Text = ""
            PkgToSD.Text = ""
        Else
            lblMessage.Text = "Please Select Client!"
        End If

    End Sub

    Protected Sub rdPkgOption_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles rdPkgOption.SelectedIndexChanged
        If rdPkgOption.SelectedValue = 1 Then
            PkgFromSD.Enabled = True
            PkgToSD.Enabled = True
            pkgMultiple.Enabled = False
        Else
            pkgMultiple.Enabled = True
            PkgFromSD.Enabled = False
            PkgToSD.Enabled = False
        End If
    End Sub
End Class
