<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Src="POReasonAddForm.aspx.vb" Inherits="POReasonAddForm"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CarzonRent :: Internal software Control panel</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="JavaScript" src="../utilityfunction.js"></script>
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
		<script language="javascript">
		function validation()
		{
	
				if(isNaN(document.forms[0].txtmax.value))
					{
						alert("Max Amount Allowed Without Approval should be numeric and upto two decimal digits only.")
						return false;	
					}
					else
					{
					if (parseFloat(document.forms[0].txtmax.value.indexOf("."))>0)
						{
							var FlotVal
							FlotVal=document.forms[0].txtmax.value.substr(parseFloat(document.forms[0].txtmax.value.indexOf(".")),document.forms[0].txtmax.value.length)
							if(parseFloat((FlotVal.length)-1)>2)
							{
							alert("Max Amount Allowed Without Approval should be numeric and upto two decimal digits only")
							return false;
							}

						}	
					}
						
				if(isNaN(document.forms[0].txtlevel1.value))
					{
						alert("Approval Level 1 � Max Amount should be numeric and upto two decimal digits only.")
						return false;	
					}
					else
					{
	
					if (parseFloat(document.forms[0].txtlevel1.value.indexOf("."))>0)
						{
							var FlotVal
							FlotVal=document.forms[0].txtlevel1.value.substr(parseFloat(document.forms[0].txtlevel1.value.indexOf(".")),document.forms[0].txtlevel1.value.length)
							if(parseFloat((FlotVal.length)-1)>2)
							{
							alert("Approval Level 1 � Max Amount should be numeric and upto two decimal digits only")
							return false;
							}

						}	
					if (document.forms[0].txtlevel1.value!="")
						{
							if(document.forms[0].ddllevel1.value=="")
										{
											alert("Please select Approval Level 1� Designation")
											return false;
										}
						}	
	
					}						
				if(isNaN(document.forms[0].txtlevel2.value))
					{
						alert("Approval Level 2 � Max Amount should be numeric and upto two decimal digits only.")
						return false;	
					}
					else
					{
					if (parseFloat(document.forms[0].txtlevel2.value.indexOf("."))>0)
						{
							var FlotVal
							FlotVal=document.forms[0].txtlevel2.value.substr(parseFloat(document.forms[0].txtlevel2.value.indexOf(".")),document.forms[0].txtlevel2.value.length)
							if(parseFloat((FlotVal.length)-1)>2)
							{
							alert("Approval Level 2 � Max Amount should be numeric and upto two decimal digits only")
							return false;
						}
						}
						if (document.forms[0].txtlevel2.value!="")
						{
						if(document.forms[0].ddllevel2.value=="")
								{
									alert("Please select Approval Level 2� Designation")
									return false;
								}
							}
	
					}	
				if(isNaN(document.forms[0].txtlevel3.value))
					{
						alert("Approval Level 3 � Max Amount should be numeric and upto two decimal digits only.")
						return false;	
					}
					else
					{
					if (parseFloat(document.forms[0].txtlevel3.value.indexOf("."))>0)
						{
							var FlotVal
							FlotVal=document.forms[0].txtlevel3.value.substr(parseFloat(document.forms[0].txtlevel3.value.indexOf(".")),document.forms[0].txtlevel3.value.length)
							if(parseFloat((FlotVal.length)-1)>2)
							{
							alert("Approval Level 3 � Max Amount should be numeric and upto two decimal digits only")
							return false;
						}
							}
						if (document.forms[0].txtlevel3.value!="")
						{
						if(document.forms[0].ddllevel3.value=="")
								{
									alert("Please select Approval Level 3 � Designation")
									return false;
								}
						}
	
					}	
				if(isNaN(document.forms[0].txtlevel4.value))
					{
						alert("Approval Level 4 � Max Amount should be numeric and upto two decimal digits only.")
						return false;	
					}
					else
					{
					if (parseFloat(document.forms[0].txtlevel4.value.indexOf("."))>0)
						{
							var FlotVal
							FlotVal=document.forms[0].txtlevel4.value.substr(parseFloat(document.forms[0].txtlevel4.value.indexOf(".")),document.forms[0].txtlevel4.value.length)
							if(parseFloat((FlotVal.length)-1)>2)
							{
							alert("Approval Level 4 � Max Amount should be numeric and upto two decimal digits only")
							return false;
						}
							}
					if (document.forms[0].txtlevel4.value!="")
						{
						if(document.forms[0].ddllevel4.value=="")
								{
									alert("Please select Approval Level 4 � Designation")
									return false;
								}
						}
	
					}	
				if(isNaN(document.forms[0].txtlevel5.value))
					{
						alert("Approval Level 5 � Max Amount should be numeric and upto two decimal digits only.")
						return false;	
					}
					else
					{
					if (parseFloat(document.forms[0].txtlevel5.value.indexOf("."))>0)
						{
							var FlotVal
							FlotVal=document.forms[0].txtlevel5.value.substr(parseFloat(document.forms[0].txtlevel5.value.indexOf(".")),document.forms[0].txtlevel5.value.length)
							if(parseFloat((FlotVal.length)-1)>2)
							{
							alert("Approval Level 5 � Max Amount should be numeric and upto two decimal digits only")
							return false;
						}
							}
					if (document.forms[0].txtlevel5.value!="")
						{
						if(document.forms[0].ddllevel5.value=="")
								{
									alert("Please select Approval Level 5 � Designation")
									return false;
								}
						}
	
					}
						
		

		
			
			var strvalues
			strvalues=('txtporeason,txtmax')
			return checkmandatory(strvalues);		
		}		
		
		
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<uc1:Headerctrl id="Headerctrl1" runat="server"></uc1:Headerctrl>
			<TABLE id="Table1" align="center">
				<asp:panel id="pnlmainform" Runat="server">
					<TBODY>
						<TR>
							<TD align="center" colSpan="2"><STRONG><U>Add a Reason for PO generation</U></STRONG>
							</TD>
						</TR>
						<tr><td colspan="2" align="center"><asp:Label id="lblErrorMsg" cssclass="subRedHead" visible="false" runat="server"></asp:Label></td></tr>
						<TR>
							<TD align="center" colSpan="2">&nbsp;&nbsp;</TD>
						</TR>
						<TR>
							<TD>* Reason for PO Generation
							</TD>
							<TD>
								<asp:textbox id="txtporeason" runat="server" MaxLength="250" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>* Max Amount Allowed Without Approval</TD>
							<TD>
								<asp:textbox id="txtmax" runat="server" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>Approval Level 1 � Designation
							</TD>
							<TD>
								<asp:DropDownList id="ddllevel1" runat="server" CssClass="input"></asp:DropDownList></TD>
						</TR>
						<TR>
							<TD>Approval Level 1 � Max Amount
							</TD>
							<TD>
								<asp:textbox id="txtlevel1" runat="server" MaxLength="50" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>Approval Level 2 � Designation
							</TD>
							<TD>
								<asp:DropDownList id="ddllevel2" runat="server" CssClass="input"></asp:DropDownList></TD>
						</TR>
						<TR>
							<TD>Approval Level 2 � Max Amount
							</TD>
							<TD>
								<asp:textbox id="txtlevel2" runat="server" MaxLength="50" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>Approval Level 3 � Designation
							</TD>
							<TD>
								<asp:DropDownList id="ddllevel3" runat="server" CssClass="input"></asp:DropDownList></TD>
						</TR>
						<TR>
							<TD>Approval Level 3 � Max Amount
							</TD>
							<TD>
								<asp:textbox id="txtlevel3" runat="server" MaxLength="50" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>Approval Level 4 � Designation
							</TD>
							<TD>
								<asp:DropDownList id="ddllevel4" runat="server" CssClass="input"></asp:DropDownList></TD>
						</TR>
						<TR>
							<TD>Approval Level 4 � Max Amount
							</TD>
							<TD>
								<asp:textbox id="txtlevel4" runat="server" MaxLength="50" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>Approval Level 5 � Designation
							</TD>
							<TD>
								<asp:DropDownList id="ddllevel5" runat="server" CssClass="input"></asp:DropDownList></TD>
						</TR>
						<TR>
							<TD>Approval Level 5 � Max Amount
							</TD>
							<TD>
								<asp:textbox id="txtlevel5" runat="server" MaxLength="50" CssClass="input"></asp:textbox></TD>
						</TR>
						<TR>
							<TD vAlign="top">Remarks(<SPAN class="shwText" id="shwMessage">0</SPAN>/2000 chars)</TD>
							<TD>
								<asp:textbox id="txtRemarks" onkeydown="showLength(this.form.txtRemarks,'document.all.shwMessage',2000)"
									onkeyup="showLength(this.form.txtRemarks,'document.all.shwMessage',2000)" runat="server"
									MaxLength="2000" CssClass="input" Rows="3" Columns="50" TextMode="MultiLine"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>Active</TD>
							<TD>
								<asp:checkbox id="chkactive" runat="server" CssClass="input" Checked="True"></asp:checkbox></TD>
						</TR>
						<TR>
							<TD align="center" colSpan="2">
								<asp:button id="btnsubmit" runat="server" CssClass="button" Text="Submit"></asp:button>&nbsp;&nbsp;
								<asp:button id="btnreset" runat="server" CssClass="button" Text="Reset" CausesValidation="False"></asp:button></TD>
						</TR>
				</asp:panel><asp:panel id="pnlconfirmation" Runat="server" Visible="False">
					<TR align="center">
						<TD colSpan="2">
							<asp:Label id="lblMessage" runat="server"></asp:Label></TD>
					</TR>
					<TR align="center">
						<TD colSpan="2">
							<asp:HyperLink id="hyplnkretry" runat="server"></asp:HyperLink></TD>
					</TR>
				</asp:panel></TBODY></TABLE>
		</form>
	</body>
</HTML>
