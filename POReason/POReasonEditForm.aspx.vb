Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class POReasonEditForm
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtporeason As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtmax As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddllevel1 As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtlevel1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddllevel2 As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtlevel2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddllevel3 As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtlevel3 As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddllevel4 As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtlevel4 As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddllevel5 As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtlevel5 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtRemarks As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkactive As System.Web.UI.WebControls.CheckBox
    Protected WithEvents btnsubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnreset As System.Web.UI.WebControls.Button
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btnsubmit.Attributes("onClick") = "return validation();"
        If Not Page.IsPostBack Then
            populateddl()
            Dim dtrreader As SqlDataReader
            Dim accessdata As clsutility
            accessdata = New clsutility
            dtrreader = accessdata.funcGetSQLDataReader("select * from CORIntPOReasonMaster where  poreasonid=" & Request.QueryString("id") & " ")
            dtrreader.Read()


            txtporeason.Text = dtrreader("poreason") & ""
            txtmax.Text = dtrreader("amtwoapprove") & ""
            autoselec_ddl(ddllevel1, dtrreader("desigid1"))
            autoselec_ddl(ddllevel2, dtrreader("desigid2"))
            autoselec_ddl(ddllevel3, dtrreader("desigid3"))
            autoselec_ddl(ddllevel4, dtrreader("desigid4"))
            autoselec_ddl(ddllevel5, dtrreader("desigid5"))
            txtlevel1.Text = dtrreader("approveamt1") & ""
            txtlevel2.Text = dtrreader("approveamt2") & ""
            txtlevel3.Text = dtrreader("approveamt3") & ""
            txtlevel4.Text = dtrreader("approveamt4") & ""
            txtlevel5.Text = dtrreader("approveamt5") & ""

            txtRemarks.Text = dtrreader("remarks") & ""
            chkactive.Checked = dtrreader("active")
            dtrreader.Close()
            accessdata.Dispose()
        End If
    End Sub

    Sub populateddl()

        Dim objAcessdata As clsutility
        objAcessdata = New clsutility
        ddllevel1.DataSource = objAcessdata.funcGetSQLDataReader("select DesigID ,DesigName  from CORIntDesigMaster   where active=1 order by DesigName")
        ddllevel1.DataValueField = "DesigID"
        ddllevel1.DataTextField = "DesigName"
        ddllevel1.DataBind()
        ddllevel1.Items.Insert(0, New ListItem("", ""))


        ddllevel2.DataSource = objAcessdata.funcGetSQLDataReader("select DesigID ,DesigName  from CORIntDesigMaster   where active=1 order by DesigName")
        ddllevel2.DataValueField = "DesigID"
        ddllevel2.DataTextField = "DesigName"
        ddllevel2.DataBind()
        ddllevel2.Items.Insert(0, New ListItem("", ""))


        ddllevel3.DataSource = objAcessdata.funcGetSQLDataReader("select DesigID ,DesigName  from CORIntDesigMaster   where active=1 order by DesigName")
        ddllevel3.DataValueField = "DesigID"
        ddllevel3.DataTextField = "DesigName"
        ddllevel3.DataBind()
        ddllevel3.Items.Insert(0, New ListItem("", ""))


        ddllevel4.DataSource = objAcessdata.funcGetSQLDataReader("select DesigID ,DesigName  from CORIntDesigMaster   where active=1 order by DesigName")
        ddllevel4.DataValueField = "DesigID"
        ddllevel4.DataTextField = "DesigName"
        ddllevel4.DataBind()
        ddllevel4.Items.Insert(0, New ListItem("", ""))


        ddllevel5.DataSource = objAcessdata.funcGetSQLDataReader("select DesigID ,DesigName  from CORIntDesigMaster   where active=1 order by DesigName")
        ddllevel5.DataValueField = "DesigID"
        ddllevel5.DataTextField = "DesigName"
        ddllevel5.DataBind()
        ddllevel5.Items.Insert(0, New ListItem("", ""))
        objAcessdata.Dispose()

    End Sub

    Function autoselec_ddl(ByVal ddlname As DropDownList, ByVal selectvalue As Int32)
        If Not selectvalue = "0" Then
            ddlname.Items.FindByValue(selectvalue).Selected = True
        End If
    End Function

    Private Sub btnreset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnreset.Click
        Response.Redirect("POReasonEditForm.aspx?id=" & Request.QueryString("id"))
    End Sub

    Private Sub btnsubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        Dim MyConnection As SqlConnection
        Dim intuniqcheck As SqlParameter
        Dim intuniqvalue as int32
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        cmd = New SqlCommand("procEditPOReason", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@rowid", Request.QueryString("id"))
        cmd.Parameters.Add("@poreason", txtporeason.Text)
        cmd.Parameters.Add("@amtwoapprove", txtmax.Text)
        cmd.Parameters.Add("@desigid1", ddllevel1.SelectedItem.Value)
if not txtlevel1.Text="" then
        cmd.Parameters.Add("@approveamt1", txtlevel1.Text)
end if
        cmd.Parameters.Add("@desigid2", ddllevel2.SelectedItem.Value)
if not txtlevel2.Text="" then
        cmd.Parameters.Add("@approveamt2", txtlevel2.Text)
end if
        cmd.Parameters.Add("@desigid3", ddllevel3.SelectedItem.Value)
if not txtlevel3.Text="" then
        cmd.Parameters.Add("@approveamt3", txtlevel3.Text)
end if
        cmd.Parameters.Add("@desigid4", ddllevel4.SelectedItem.Value)
if not txtlevel4.Text="" then
        cmd.Parameters.Add("@approveamt4", txtlevel4.Text)
end if
        cmd.Parameters.Add("@desigid5", ddllevel5.SelectedItem.Value)
if not txtlevel5.Text="" then
        cmd.Parameters.Add("@approveamt5", txtlevel5.Text)
end if
        cmd.Parameters.Add("@remarks", txtRemarks.Text)
        cmd.Parameters.Add("@active", get_YNvalue(chkactive))
        cmd.Parameters.Add("@modifiedby", Session("loggedin_user"))
       intuniqcheck = cmd.Parameters.Add("@uniqcheckval", SqlDbType.Int)
       intuniqcheck.Direction = ParameterDirection.Output

        '  Try
        MyConnection.Open()
        cmd.ExecuteNonQuery()
           intuniqvalue=cmd.Parameters("@uniqcheckval").Value
        MyConnection.Close()
           if not intuniqvalue = 0 then
                lblErrorMsg.visible=true
                lblErrorMsg.text="Reason already exist."
                exit sub
            else
		        lblErrorMsg.visible=false
                pnlmainform.Visible = False
                pnlconfirmation.Visible = True

                lblMessage.Text = "You have updated the Reason for PO generation successfully"
                hyplnkretry.Text = "Edit another Reason for PO generation"
                hyplnkretry.NavigateUrl = "POReasonEditSearch.aspx"
            end if
        '   Catch
        '   End Try
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function

End Class
