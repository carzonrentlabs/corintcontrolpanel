Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class POReasonAddForm
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtporeason As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtmax As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddllevel1 As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddllevel2 As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtlevel2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddllevel3 As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtlevel3 As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddllevel4 As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtlevel4 As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddllevel5 As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtlevel5 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtRemarks As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkactive As System.Web.UI.WebControls.CheckBox
    Protected WithEvents btnsubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnreset As System.Web.UI.WebControls.Button
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    Protected WithEvents txtlevel1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label



    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btnsubmit.Attributes("onClick") = "return validation();"
        If Not Page.IsPostBack Then
            populateddl()
        End If
    End Sub
    Sub populateddl()

        Dim objAcessdata As clsutility
        objAcessdata = New clsutility
        ddllevel1.DataSource = objAcessdata.funcGetSQLDataReader("select DesigID ,DesigName  from CORIntDesigMaster   where active=1 order by DesigName")
        ddllevel1.DataValueField = "DesigID"
        ddllevel1.DataTextField = "DesigName"
        ddllevel1.DataBind()
        ddllevel1.Items.Insert(0, New ListItem("", ""))


        ddllevel2.DataSource = objAcessdata.funcGetSQLDataReader("select DesigID ,DesigName  from CORIntDesigMaster   where active=1 order by DesigName")
        ddllevel2.DataValueField = "DesigID"
        ddllevel2.DataTextField = "DesigName"
        ddllevel2.DataBind()
        ddllevel2.Items.Insert(0, New ListItem("", ""))


        ddllevel3.DataSource = objAcessdata.funcGetSQLDataReader("select DesigID ,DesigName  from CORIntDesigMaster   where active=1 order by DesigName")
        ddllevel3.DataValueField = "DesigID"
        ddllevel3.DataTextField = "DesigName"
        ddllevel3.DataBind()
        ddllevel3.Items.Insert(0, New ListItem("", ""))


        ddllevel4.DataSource = objAcessdata.funcGetSQLDataReader("select DesigID ,DesigName  from CORIntDesigMaster   where active=1 order by DesigName")
        ddllevel4.DataValueField = "DesigID"
        ddllevel4.DataTextField = "DesigName"
        ddllevel4.DataBind()
        ddllevel4.Items.Insert(0, New ListItem("", ""))


        ddllevel5.DataSource = objAcessdata.funcGetSQLDataReader("select DesigID ,DesigName  from CORIntDesigMaster   where active=1 order by DesigName")
        ddllevel5.DataValueField = "DesigID"
        ddllevel5.DataTextField = "DesigName"
        ddllevel5.DataBind()
        ddllevel5.Items.Insert(0, New ListItem("", ""))
        objAcessdata.Dispose()

    End Sub

    Private Sub btnsubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsubmit.Click

        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        Dim intuniqcheck As SqlParameter
        Dim intuniqvalue as int32
        cmd = New SqlCommand("procAddPOReason", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("@poreason", txtporeason.Text)
        cmd.Parameters.Add("@amtwoapprove", txtmax.Text)
        cmd.Parameters.Add("@desigid1", ddllevel1.SelectedItem.Value)
if not txtlevel1.Text="" then
        cmd.Parameters.Add("@approveamt1", txtlevel1.Text)
end if
        cmd.Parameters.Add("@desigid2", ddllevel2.SelectedItem.Value)
if not txtlevel2.Text="" then
        cmd.Parameters.Add("@approveamt2", txtlevel2.Text)
end if
        cmd.Parameters.Add("@desigid3", ddllevel3.SelectedItem.Value)
if not txtlevel3.Text="" then
        cmd.Parameters.Add("@approveamt3", txtlevel3.Text)
end if
        cmd.Parameters.Add("@desigid4", ddllevel4.SelectedItem.Value)
if not txtlevel4.Text="" then
        cmd.Parameters.Add("@approveamt4", txtlevel4.Text)
end if
        cmd.Parameters.Add("@desigid5", ddllevel5.SelectedItem.Value)
if not txtlevel5.Text="" then
        cmd.Parameters.Add("@approveamt5", txtlevel5.Text)
end if
        cmd.Parameters.Add("@remarks", txtRemarks.Text)
        cmd.Parameters.Add("@active", get_YNvalue(chkactive))
        cmd.Parameters.Add("@createdby", Session("loggedin_user"))
       intuniqcheck = cmd.Parameters.Add("@uniqcheckval", SqlDbType.Int)
       intuniqcheck.Direction = ParameterDirection.Output

        '  Try
        MyConnection.Open()
        cmd.ExecuteNonQuery()
           intuniqvalue=cmd.Parameters("@uniqcheckval").Value
        MyConnection.Close()
           if not intuniqvalue = 0 then
                lblErrorMsg.visible=true
                lblErrorMsg.text="Reason already exist."
                exit sub
            else
		        lblErrorMsg.visible=false
                pnlmainform.Visible = False
                pnlconfirmation.Visible = True

                lblMessage.Text = "You have added the Reason for PO generation successfully"
                hyplnkretry.Text = "Add another Reason for PO generation"
                hyplnkretry.NavigateUrl = "POReasonAddForm.aspx"
            end if
        '   Catch
        '   End Try
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function

    Private Sub btnreset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnreset.Click
        Response.Redirect("POReasonAddForm.aspx")
    End Sub
End Class
