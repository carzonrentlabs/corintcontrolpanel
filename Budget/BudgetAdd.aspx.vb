Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class BudgetAdd
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ddlCityID As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlservice As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents TxtsdAmt As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnsubmit As System.Web.UI.WebControls.Button
    Protected WithEvents chkActive As System.Web.UI.WebControls.CheckBox
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        btnsubmit.Attributes("onClick") = "return validation();"
        If Not Page.IsPostBack() Then
            populateddl()
        End If
    End Sub
    Sub populateddl()
        Dim objAcessdata As clsutility

        Dim dtrreader As SqlDataReader
        Dim accessdata As clsutility
        accessdata = New clsutility
        dtrreader = accessdata.funcGetSQLDataReader("select cityid,cityname from corintcitymaster where Active=1 order by cityname")
        ddlCityID.DataSource = dtrreader
        ddlCityID.DataValueField = "cityid"
        ddlCityID.DataTextField = "cityname"
        ddlCityID.DataBind()
        'ddlCityID.Items.Insert(0, New ListItem("Any", 0))
        dtrreader.Close()
    End Sub

    Private Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        Dim intFlagCheck As SqlParameter
        Dim intFlag As Int32
        Dim intSDParam As SqlParameter
        Dim intSDid As Int32
        Dim txtHr As String
        Dim txtservice As String

        If (TxtsdAmt.Text) = "" Then
            TxtsdAmt.Text = 0
        End If

        txtservice = ddlservice.SelectedItem.Value
        
        'If ddlCityID.SelectedItem.Text = "Chandigarh Airport" Or ddlCityID.SelectedItem.Text = "Hyderabad Airport" Or ddlCityID.SelectedItem.Text = "Mumbai Airport" Or ddlCityID.SelectedItem.Text = "Bangalore Airport" Or ddlCityID.SelectedItem.Text = "Mumbai Airport - 1B" Then
        If ddlCityID.SelectedItem.Text Like "*Airport*" Then
            txtservice = "Airport Transfer"
            'response.write("You are in Airport")
            'response.end()
        End If

        'If ddlCityID.SelectedItem.Text = "Limousine Mumbai" Or ddlCityID.SelectedItem.Text = "Limousine Delhi" Or ddlCityID.SelectedItem.Text = "Limousine Bangalore" Or ddlCityID.SelectedItem.Text = "Limousine Hyderabad" Then
        If ddlCityID.SelectedItem.Text Like "*Limousine*" Then
            txtservice = "Limousine Drive"
            'response.write("You are in Limousine")
            'response.end()
        End If

        'response.write(ddlCityID.SelectedItem.Text)
        'response.write("<br>")
        'response.write(txtservice)
        'response.end()

        If (txtDate.text) = "" Then
            txtDate.text = now
        End If

        cmd = New SqlCommand("Proc_AddProjected_sales", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@CityID", ddlCityID.SelectedValue)
        'cmd.Parameters.Add("@Service", ddlservice.SelectedItem.Value)
        cmd.Parameters.Add("@Service", txtservice)
        cmd.Parameters.Add("@month_name", month(txtDate.Text))
        cmd.Parameters.Add("@year_name", year(txtDate.Text))
        cmd.Parameters.Add("@Self_Drive", TxtsdAmt.Text)
        cmd.Parameters.Add("@Active", get_YNvalue(chkActive))
        cmd.Parameters.Add("@CreatedBy", Session("loggedin_user"))
        intFlagCheck = cmd.Parameters.Add("@StatusFlag", SqlDbType.Int)
        intFlagCheck.Direction = ParameterDirection.Output
        intSDParam = cmd.Parameters.Add("@ID", SqlDbType.Int)
        intSDParam.Direction = ParameterDirection.Output

        MyConnection.Open()

        cmd.ExecuteNonQuery()
        intFlag = cmd.Parameters("@StatusFlag").Value
        '   response.write(intFlag)
        '   response.end()
        intSDid = cmd.Parameters("@ID").Value

        MyConnection.Close()
        lblErrorMsg.visible = True

        If Trim("" & intFlag) = "2" Then
            pnlmainform.visible = False
            pnlconfirmation.visible = True
            lblErrorMsg.Text = "This Budget for the month already exists!<br> The ID is <b>" & intSDid & "</b>"
            'lblErrorMsg.visible = True
            'response.write(lblErrorMsg.Text)
            hyplnkretry.Text = "Add another Budget"
            hyplnkretry.NavigateUrl = "BudgetAdd.aspx"
        Else
            pnlmainform.Visible = False
            pnlconfirmation.Visible = True
            ' response.write("You Are here")
            lblErrorMsg.Text = "You have added the new Budget <br> The ID is <b>" & intSDid & "</b>"
            hyplnkretry.Text = "Add another Budget"
            hyplnkretry.NavigateUrl = "BudgetAdd.aspx"
        End If
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function
End Class