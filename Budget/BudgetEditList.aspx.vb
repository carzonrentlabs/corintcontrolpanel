Imports commonutility
Imports System.Data
Imports System.text
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System

Public Class BudgetEditList
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tbl1 As System.Web.UI.WebControls.Table
    Protected WithEvents MainDiv As System.Web.UI.HtmlControls.HtmlGenericControl

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not Page.IsPostBack Then
            getvalue()
        End If
    End Sub
    Sub getvalue()
        Dim strquery As StringBuilder
        Dim str As StringBuilder
        Dim dtrreader As SqlDataReader
        Dim accessdata As clsutility
        Dim createby As String = ""
        Dim ModifiedBy As String = ""
        Dim ModifiedDate As String = ""
        Dim Service As String = ""

        accessdata = New clsutility
        strquery = New StringBuilder("Exec Proc_Projected_sales '" & Request.QueryString("CityID") & "',1")

        dtrreader = accessdata.funcGetSQLDataReader(strquery.ToString)
        If dtrreader.HasRows Then
            str = New StringBuilder("<table ID=""tbl1"" HorizontalAlign=""Center"" BorderColor=""#cccc99"" BorderStyle=""Solid"" GridLines=""both"" border=""1"" CellSpacing=""0"" CellPadding=""0"">")
            str.Append("<tr>")
            str.Append("<td Class=""btnstyle"">&nbsp;&nbsp;<b>ID</b>&nbsp;&nbsp;</td>")
            str.Append("<td Class=""btnstyle"">&nbsp;&nbsp;<b>City Name</b>&nbsp;&nbsp;</td>")
            str.Append("<td Class=""btnstyle"">&nbsp;&nbsp;<b>Month Name</b>&nbsp;&nbsp;</td>")
            str.Append("<td Class=""btnstyle"">&nbsp;&nbsp;<b>Year Name</b>&nbsp;&nbsp;</td>")
            str.Append("<td Class=""btnstyle"">&nbsp;&nbsp;<b>Service</b>&nbsp;&nbsp;</td>")
            str.Append("<td Class=""btnstyle"">&nbsp;&nbsp;<b>Budget Amout</b>&nbsp;&nbsp;</td>")
            str.Append("<td Class=""btnstyle"">&nbsp;&nbsp;<b>Created By</b>&nbsp;&nbsp;</td>")
            str.Append("<td Class=""btnstyle"">&nbsp;&nbsp;<b>Create Date</b>&nbsp;&nbsp;</td>")
            str.Append("<td Class=""btnstyle"">&nbsp;&nbsp;<b>Modified By</b>&nbsp;&nbsp;</td>")
            str.Append("<td Class=""btnstyle"">&nbsp;&nbsp;<b>Modify Date</b>&nbsp;&nbsp;</td>")
            str.Append("<td Class=""btnstyle"">&nbsp;&nbsp;<b>Active</b>&nbsp;&nbsp;</td>")
            str.Append("<td Class=""btnstyle"">&nbsp;&nbsp;<b>Edit</b>&nbsp;&nbsp;</td>")
            str.Append("</tr>")
            While dtrreader.Read

                str.Append("<tr>")
                'str.Append("<td>&nbsp;&nbsp;" & dtrreader("CityName") & "&nbsp;&nbsp;</td>")
                str.Append("<td>" & dtrreader("ID") & "</td>")
                str.Append("<td>" & dtrreader("CityName") & "</td>")
                str.Append("<td>" & monthname(dtrreader("month_name")) & "</td>")
                str.Append("<td>" & dtrreader("year_name") & "</td>")

                If Trim("" & dtrreader("Service") = "Airport Transfer") Then
                    Service = "Airport"
                ElseIf Trim("" & dtrreader("Service") = "Limousine Drive") Then
                    Service = "Limousine"
                Else
                    Service = dtrreader("Service")
                End If

                'str.Append("<td>" & dtrreader("Service") & "</td>")
                str.Append("<td>" & Service & "</td>")
                str.Append("<td>" & dtrreader("Projected_Amt") & "</td>")

                If Trim("" & dtrreader("Created_By")) <> "" Then
                    createby = getuserName(Convert.ToString(dtrreader("Created_By")))
                Else
                    createby = "-"
                End If
                'str.Append("<td>&nbsp;&nbsp;" & createby & "&nbsp;&nbsp;</td>")
                str.Append("<td>" & createby & "</td>")
                str.Append("<td>" & dtrreader("Creation_Date") & "</td>")
                If IsDBNull(dtrreader("Modified_By")) Then
                    ModifiedBy = "-"
                Else
                    ModifiedBy = getuserName(Convert.ToString(dtrreader("Modified_By")))
                End If
                str.Append("<td>" & ModifiedBy & "</td>")
                str.Append("<td>" & dtrreader("Modified_Date") & "</td>")
                If dtrreader("Active") Then
                    'str.Append("<td>&nbsp;&nbsp;Active&nbsp;&nbsp;</td>")
                    str.Append("<td>Active</td>")
                Else
                    'str.Append("<td>&nbsp;&nbsp;Not Active&nbsp;&nbsp;</td>")
                    str.Append("<td>Not Active</td>")
                End If

                'str.Append("<td>&nbsp;&nbsp;" & ModifiedBy & "&nbsp;&nbsp;</td>")
                'str.Append("<td>&nbsp;&nbsp;" & dtrreader("CreateDate") & "&nbsp;&nbsp;</td>")
                'str.Append("<td>&nbsp;&nbsp;" & dtrreader("ModifyDate") & "&nbsp;&nbsp;</td>")
                str.Append("<td>&nbsp;&nbsp;<a href=BudgetEdit.aspx?ID=" & dtrreader("ID") & " >Edit</a>&nbsp;&nbsp;</td>")
                str.Append("</tr>")

            End While
            str.Append("</table>")
            MainDiv.InnerHtml = str.ToString
        Else
            str = New StringBuilder("<table ID=""tblCDP1"" HorizontalAlign=""Center"" BorderColor=""#cccc99"" BorderStyle=""Solid"" GridLines=""both"" border=""1"" CellSpacing=""0"" CellPadding=""0"">")
            str.Append("<tr>")
            str.Append("<td Class=""btnstyle"">&nbsp;&nbsp;<b>No record exists.</b>&nbsp;&nbsp;</td>")
            str.Append("</tr>")
            str.Append("</table>")
            MainDiv.InnerHtml = str.ToString
        End If
        dtrreader.Close()
        accessdata.Dispose()
    End Sub
    Function getuserName(ByVal userid As Integer) As String
        Dim strquery As StringBuilder
        Dim oConnection As SqlConnection
        Dim strConnection As String = System.Configuration.ConfigurationSettings.AppSettings("corConnectString")

        oConnection = New SqlConnection(strConnection)
        strquery = New StringBuilder("select top 1 Fname + ' ' + ISNull(Mname,'') + ' ' + IsNull(lname,'') as UName from CORIntSysUsersMaster where sysuserid=" & userid)

        Dim Command = New SqlCommand(strquery.ToString, oConnection)
        oConnection.Open()
        Dim strreturnvalue As String = ""
        strreturnvalue = Convert.ToString(Command.ExecuteScalar())
        If oConnection.State = ConnectionState.Open Then
            oConnection.Close()
        End If
        Return strreturnvalue
    End Function
End Class
