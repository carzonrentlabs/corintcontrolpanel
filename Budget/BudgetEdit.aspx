<%@ Page Language="vb" AutoEventWireup="false" Codebehind="BudgetEdit.aspx.vb" Inherits="BudgetEdit" Src="BudgetEdit.aspx.vb" %>
<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>CarzonRent :: Internal software Control panel</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="JavaScript" src="../utilityfunction.js"></script>
		<link href="../HertzInt.css" type="text/css" rel="stylesheet">
		<script language="javascript">
		function validation()
		{
			if(isNaN(document.forms[0].TxtsdAmt.value))
			{
				alert("Amount should be numeric only.")
				return false;	
			}
			if(document.getElementById('TxtsdAmt').value == "" || document.getElementById('TxtsdAmt').value == 0)
			{
				alert("Please enter the Budget Amount")
				document.getElementById('TxtsdAmt').focus();
				return false;	
			}
				return true;	
		}		
		
		function dateReg(obj)
        {
            if(obj.value!="")
            {
                // alert(obj.value);
                var reg = /((0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[0-1])-(201[0-9]))/
                if(reg.test(obj.value))
                {
                    //alert('valid');
                }
                else
                {
                    alert('notvalid');
                    obj.value="";
                }
            }
        }		
		</script>
	</head>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<uc1:Headerctrl id="Headerctrl1" runat="server"></uc1:Headerctrl>
			<table id="Table1" align="center">
				<asp:panel id="pnlmainform" Runat="server">
					<tbody>
						<tr>
							<td align="center" colSpan="2"><strong><u>Edit a Budget</u></strong>
							</td>
						</tr>
						<tr>
							<td align="center" colSpan="2">&nbsp;&nbsp;</td>
						</tr>
						<tr>
							<td>City</td>
							<td><asp:Label ID="lblCityID" Runat="server"></asp:Label></td>
						</tr>
						<tr>
							<td>Month</td>
						<td><asp:Label id="txtDate" runat="server"></asp:Label></td>
						</tr>
						
						<tr>
							<td>* Budget</td>
							<td><asp:textbox id="TxtsdAmt" runat="server" value=""></asp:textbox></td>
						</td>
						</tr>
						
						<tr>
							<td>Active</td>
							<td>
								<asp:checkbox id="chk_active" runat="server" CssClass="input" Checked="True"></asp:checkbox></td>
						</tr>


						<tr>
							<td align="center" colSpan="2">
								<asp:button id="btnsubmit" runat="server" CssClass="button" Text="Submit"></asp:button>&nbsp;&nbsp;
								<asp:button id="btnreset" runat="server" CssClass="button" Text="Reset" CausesValidation="False"></asp:button></td>
						</tr>
				</tbody></asp:panel>
				<asp:panel id="pnlconfirmation" Runat="server" Visible="False">
					<tr align="center">
						<td colspan="2">
							<asp:Label id="lblErrorMsg" runat="server"></asp:Label></td>
					</tr>
					<tr align="center">
						<td  colspan="2">
							<asp:HyperLink id="hyplnkretry" runat="server"></asp:HyperLink></td>
					</tr>

				</asp:panel></table>

		</form>
	</body>
</html>
