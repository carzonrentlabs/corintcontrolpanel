Imports commonutility
Imports System.Data
Imports System.text
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class BudgetEdit
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object
    Protected WithEvents lblCityID As System.Web.UI.WebControls.Label
    Protected WithEvents FromMM As System.Web.UI.WebControls.Label
    Protected WithEvents txtDate As System.Web.UI.WebControls.Label
    Protected WithEvents TxtsdAmt As System.Web.UI.WebControls.TextBox
    Protected WithEvents chk_Active As System.Web.UI.WebControls.CheckBox
    Protected WithEvents btnsubmit As System.Web.UI.WebControls.Button
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel



    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        ' btnsubmit.Attributes("onClick") = "return validation();"
        If Trim("" & Request.QueryString("ID")) <> "" Then
            Dim intID As Integer
            intID = Request.QueryString("ID")
            If Not IsPostBack() Then
                getvalue(intID)
            End If
        Else
            Response.Write("Bad request.")
            Response.End()
        End If
    End Sub
    Sub getvalue(ByVal intID As Integer)
        Dim strquery As String
        'Dim StrUrl As StringBuilder
        Dim str As StringBuilder
        Dim dtrreader As SqlDataReader
        Dim accessdata As clsutility
        Dim createby As String = ""
        Dim ModifiedBy As String = ""
        accessdata = New clsutility

        strquery = "Exec Proc_Projected_sales '" & intID & "',2"

        dtrreader = accessdata.funcGetSQLDataReader(strquery.ToString)
        'If dtrreader.HasRows Then
        While dtrreader.Read

            txtDate.Text = monthname(dtrreader("month_name")) & "-" & dtrreader("year_name")
            'If IsDBNull(dtrreader("CityName")) Then
            'lblCityID.Text = "Any"
            ' Else
            lblCityID.Text = dtrreader("CityName")
            'End If

            TxtsdAmt.Text = dtrreader("Projected_Amt")
            If dtrreader("Active") Then
                chk_Active.Checked = True
            Else
                chk_Active.Checked = False
            End If
        End While
        'End If
    End Sub

    Private Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        Dim Flag As Integer
        Dim intAmount1, intAmount2, intAmount3 As Integer
        Dim intAmount As Double

        If (TxtsdAmt.Text) = "" Then
            TxtsdAmt.Text = 0
        End If
        
        intAmount = TxtsdAmt.Text
        
        'Response.Write("Txt_Amount==" & intAmount)

        cmd = New SqlCommand("Proc_EditProjected_sales", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@ID", Request.QueryString("ID"))
        cmd.Parameters.Add("@Projected", intAmount)
        cmd.Parameters.Add("@Active", get_YNvalue(chk_Active))
        cmd.Parameters.Add("@CreatedBy", Session("loggedin_user"))

        MyConnection.Open()
        Flag = cmd.ExecuteNonQuery()

        MyConnection.Close()
        lblErrorMsg.Visible = True
        'hyplnkretry.Visible=True
        'Response.Write("Flag=" & Flag)

        If Trim("" & Flag) = "0" Then

            pnlmainform.Visible = False
            pnlconfirmation.Visible = True

            lblErrorMsg.Text = "Unable to edit Budget!<br> The ID is <b>" & Request.QueryString("ID") & "</b>"
            hyplnkretry.Text = "Edit another Budget"
            hyplnkretry.NavigateUrl = "BudgetSearch.aspx"
        Else

            pnlmainform.Visible = False
            pnlconfirmation.Visible = True
            ' response.write("You Are here")

            lblErrorMsg.Text = "You have edited the Budget<br> The ID is <b>" & Request.QueryString("ID") & "</b>"
            hyplnkretry.Text = "Edit another Budget"
            hyplnkretry.NavigateUrl = "BudgetSearch.aspx"
        End If
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function
End Class
