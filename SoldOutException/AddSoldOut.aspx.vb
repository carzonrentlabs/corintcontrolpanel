Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Partial Class SoldOutException_AddSoldOut
    Inherits System.Web.UI.Page
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents btnSaveException As System.Web.UI.WebControls.Button
    'Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    'Protected WithEvents btnclose As System.Web.UI.WebControls.Label
    'Protected WithEvents ddlClient As System.Web.UI.WebControls.Label
    'Protected WithEvents ddlCategory As System.Web.UI.WebControls.Label
    'Protected WithEvents ddlUnit As System.Web.UI.WebControls.Label
    'Protected WithEvents chkStatus As System.Web.UI.WebControls.CheckBox
    'Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink


    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            populateddl()
            chkStatus.Checked = True
        End If
        InsertResult.Visible = False
        Insert.Visible = True


    End Sub

    Protected Sub btnSaveException_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveException.Click
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        Dim intuniqcheck As Integer
        Dim intuniqvalue As Int32
        Dim active As Integer
        active = 0
        'If (chkStatus.Checked <> True) Then
        '    active = 0
        'End If
        If ddlClient.SelectedValue = "0" Then
            ClientScript.RegisterStartupScript(Me.GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('Select Client');</script>")
            Return
        End If

        If chkStatus.Checked = True Then
            active = 1
        End If

        cmd = New SqlCommand("prc_InsertSoldOutException", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@ClientId", ddlClient.SelectedValue.ToString())
        cmd.Parameters.AddWithValue("@CarCatId", ddlCategory.SelectedValue.ToString())
        cmd.Parameters.AddWithValue("@UnitId", ddlUnit.SelectedValue.ToString())
        cmd.Parameters.AddWithValue("@uid", Session("loggedin_user"))
        cmd.Parameters.AddWithValue("@Active", active)
        '  Try
        MyConnection.Open()
        intuniqcheck = cmd.ExecuteNonQuery()
        MyConnection.Close()
        If intuniqcheck = 0 Then
            lblMessage.Visible = True
            lblMessage.Text = "Problem in inserted record !"
        ElseIf intuniqcheck = 1 Then
            lblMessage.Visible = True
            lblMessage.Text = "You have added SoldOut Exception successfully."
            Insert.Visible = False
            InsertResult.Visible = True
        Else
            lblMessage.Visible = True
            lblMessage.Text = "Problem in inserted record !"
        End If

        '   Catch
        '   End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        populateddl()
        lblMessage.Text = ""
    End Sub

    Sub populateddl()
        'Dim dtrreader As SqlDataReader
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility

        ddlClient.DataSource = objAcessdata.funcGetSQLDataReader("select ClientCoID,ClientCoName from CORIntClientCoMaster where Active=1 ")
        ddlClient.DataValueField = "ClientCoID"
        ddlClient.DataTextField = "ClientCoName"
        ddlClient.DataBind()
        ddlClient.Items.Insert(0, New ListItem("-Select-", "0"))
        ddlClient.AppendDataBoundItems = True


        ddlUnit.DataSource = objAcessdata.funcGetSQLDataReader("select UnitID,UnitName from CORIntUnitMaster where Active=1")
        ddlUnit.DataValueField = "UnitID"
        ddlUnit.DataTextField = "UnitName"
        ddlUnit.DataBind()
        ddlUnit.Items.Insert(0, New ListItem("-All-", "0"))
        ddlUnit.AppendDataBoundItems = True


        ddlCategory.DataSource = objAcessdata.funcGetSQLDataReader("select CarCatID,CarCatName from CORIntCarCatMaster where Active=1")
        ddlCategory.DataValueField = "CarCatID"
        ddlCategory.DataTextField = "CarCatName"
        ddlCategory.DataBind()
        ddlCategory.Items.Insert(0, New ListItem("-All-", "0"))
        ddlCategory.AppendDataBoundItems = True

        objAcessdata.Dispose()

    End Sub

End Class
