using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using CCS;


public partial class testSoldOutException : System.Web.UI.Page
{
    DataTable dt = new DataTable();
    DataSet ds = new DataSet();
    clsPayment ObjPay = new clsPayment();
    protected void Page_Load(object sender, EventArgs e)
    {
        DivAddException.Visible = false;
        if (!IsPostBack)
        {
            FillSearchClient();
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        grvUnit.Visible = true;
        DivAddException.Visible = false;
        FillGrid();

    }

    public void FillGrid()
    {

        ds = ObjPay.chkSoldOutException(ddlSearchClient.SelectedValue.ToString(), 1);
        //ds = ObjPay.chkSoldOutException(txtSearchClientName.Text, 1);
        if (ds.Tables[0].Rows.Count > 0)
        {
            grvUnit.DataSource = ds.Tables[0];
            grvUnit.DataBind();

        }
        else
        {
            dt = ds.Tables[0];
            dt.Rows.Add(dt.NewRow());
            grvUnit.DataSource = dt;
            grvUnit.DataBind();
            int TotalColumns = grvUnit.Rows[0].Cells.Count;
            grvUnit.Rows[0].Cells.Clear();
            grvUnit.Rows[0].Cells.Add(new TableCell());
            grvUnit.Rows[0].Cells[0].ColumnSpan = TotalColumns;
            grvUnit.Rows[0].Cells[0].Text = "No Record Found";
        }
 
    }

    public void FillSearchClient()
    {
        ds = ObjPay.chkSoldOutException(ddlSearchClient.SelectedValue.ToString(), 3);
        //ds = ObjPay.chkSoldOutException(txtSearchClientName.Text, 1);
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlSearchClient.Items.Add(new ListItem("All", "0"));
            ddlSearchClient.AppendDataBoundItems = true;
            ddlSearchClient.DataSource = ds.Tables[0];
            ddlSearchClient.DataTextField = "ClientCoName";
            ddlSearchClient.DataValueField = "ClientCoID";
            ddlSearchClient.DataBind();

        }
        else
        {
            ddlSearchClient.Items.Add(new ListItem("All", "0"));
        }
        

    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        DivAddException.Visible = true;
        grvUnit.Visible = false;
        chkStatus.Checked = true;
        ds = ObjPay.chkSoldOutException(ddlSearchClient.SelectedValue.ToString(), 2);
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlClient.Items.Clear();
            ddlClient.Items.Add(new ListItem("-Select Client-", "0"));
            ddlClient.AppendDataBoundItems = true;
            ddlClient.DataSource = ds.Tables[0];
            ddlClient.DataValueField = Convert.ToString(ds.Tables[0].Columns[0]);
            ddlClient.DataTextField = Convert.ToString(ds.Tables[0].Columns[1]);
            ddlClient.DataBind();
        }
        if (ds.Tables[1].Rows.Count > 0)
        {
            ddlUnit.Items.Clear();
            ddlUnit.Items.Add(new ListItem("-Select Unit-", "0"));
            ddlUnit.AppendDataBoundItems = true;
            ddlUnit.DataSource = ds.Tables[1];
            ddlUnit.DataValueField = Convert.ToString(ds.Tables[1].Columns[0]);
            ddlUnit.DataTextField = Convert.ToString(ds.Tables[1].Columns[1]);
            ddlUnit.DataBind();
        }
        if (ds.Tables[2].Rows.Count > 0)
        {
            ddlCategory.Items.Clear();
            ddlCategory.Items.Add(new ListItem("-Select Catagory-", "0"));
            ddlCategory.AppendDataBoundItems = true;
            ddlCategory.DataSource = ds.Tables[2];
            ddlCategory.DataValueField = Convert.ToString(ds.Tables[2].Columns[0]);
            ddlCategory.DataTextField = Convert.ToString(ds.Tables[2].Columns[1]);
            ddlCategory.DataBind();
        }
    }
    
    protected void btnClose_Click(object sender, EventArgs e)
    {
        DivAddException.Visible = false;
        //grvUnit.Visible = true;

    }
    protected void btnSaveException_Click(object sender, EventArgs e)
    {
        string clientId = ddlClient.SelectedValue.ToString();
        string unitid = ddlUnit.SelectedValue.ToString();
        string carcatId = ddlCategory.SelectedValue.ToString();
        int active=0;
        if(chkStatus.Checked==true)
        {
            active=1;
        }
        else
        {
            active=0;
        }
        int result = ObjPay.InsertSoldOutException(clientId, carcatId, unitid, 173, active);
        if(result>0)
        {
            //txtSearchClientName.Text = ddlClient.SelectedItem.Text;
            ddlSearchClient.SelectedValue = ddlClient.SelectedValue.ToString();
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "script", "javascript:alert('Record Inserted Sucessfully!')", true);
            DivAddException.Visible = false;
            grvUnit.Visible = true;
            FillGrid();
        
        }
        else
        {
            
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "script", "javascript:alert('Error in inserting the record!')", true);
        }
     

    }
    protected void grvUnit_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lblID = (Label)e.Row.FindControl("lblID");
            ds = ObjPay.chkSoldOutException(ddlSearchClient.SelectedValue.ToString(), 2);
            CheckBox cmbType = (CheckBox)e.Row.FindControl("chkEditStatus");
            Label lblStatus1 = (Label)e.Row.FindControl("lblStatus1");
            DropDownList ddlGrdUnitName = (DropDownList)e.Row.FindControl("ddlGrdUnitName");
            DropDownList ddlGrdClientName = (DropDownList)e.Row.FindControl("ddlGrdClientName");
            DropDownList ddlGrdCatagoryName = (DropDownList)e.Row.FindControl("ddlGrdCatagoryName");
            if (cmbType != null)
            {
                if (lblStatus1.Text == "True")
                {
                    cmbType.Checked = true;
                }
                else
                {
                    cmbType.Checked = false;
                }
            }

            if (ddlGrdUnitName != null)
            {
                ddlGrdUnitName.Items.Clear();
                ddlGrdUnitName.Items.Add(new ListItem("- All -", "0"));
                ddlGrdUnitName.AppendDataBoundItems = true;
                ddlGrdUnitName.DataSource = ds.Tables[1];
                ddlGrdUnitName.DataTextField = "UnitName";
                ddlGrdUnitName.DataValueField = "UnitID";
                ddlGrdUnitName.DataBind();
                DataRowView dr = e.Row.DataItem as DataRowView;
                ddlGrdUnitName.SelectedValue = dr["UnitID"].ToString();
            }
            if (ddlGrdClientName != null)
            {
                ddlGrdClientName.Items.Clear();
                ddlGrdClientName.Items.Add(new ListItem("- All -", "0"));
                ddlGrdClientName.AppendDataBoundItems = true;
                ddlGrdClientName.DataSource = ds.Tables[0];
                ddlGrdClientName.DataTextField = "ClientCoName";
                ddlGrdClientName.DataValueField = "ClientCoID";
                ddlGrdClientName.DataBind();
                DataRowView dr = e.Row.DataItem as DataRowView;
                ddlGrdClientName.SelectedValue = dr["ClientID"].ToString();
            }

            if (ddlGrdCatagoryName != null)
            {
                ddlGrdCatagoryName.Items.Clear();
                ddlGrdCatagoryName.Items.Add(new ListItem("- All -", "0"));
                ddlGrdCatagoryName.AppendDataBoundItems = true;
                ddlGrdCatagoryName.DataSource = ds.Tables[2];
                ddlGrdCatagoryName.DataTextField = "CarCatName";
                ddlGrdCatagoryName.DataValueField = "CarCatID";
                ddlGrdCatagoryName.DataBind();
                DataRowView dr = e.Row.DataItem as DataRowView;
                ddlGrdCatagoryName.SelectedValue = dr["CategoryID"].ToString();
            }
        }  
    }
    protected void grvUnit_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        int status = 0;
        Label lblId = (Label)grvUnit.Rows[e.RowIndex].FindControl("lblID");
        DropDownList ddlGrdUnitName = (DropDownList)grvUnit.Rows[e.RowIndex].FindControl("ddlGrdUnitName");
        DropDownList ddlGrdCatagoryName = (DropDownList)grvUnit.Rows[e.RowIndex].FindControl("ddlGrdCatagoryName");
        DropDownList ddlGrdClientName = (DropDownList)grvUnit.Rows[e.RowIndex].FindControl("ddlGrdClientName");
        CheckBox chb = (CheckBox)grvUnit.Rows[e.RowIndex].FindControl("chkEditStatus");
        if (chb.Checked == true)
        {
            status = 1;
        }
        else
        {
            status = 0;
        }
        try
        {
            int update = ObjPay.UpdateSoldOutException(Convert.ToInt32(lblId.Text), Convert.ToInt32(ddlGrdUnitName.SelectedValue), Convert.ToInt32(ddlGrdCatagoryName.SelectedValue),Convert.ToInt32(ddlGrdClientName.SelectedValue), status, 173);
            if (update > 0)
            {
                //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "script", "javascript:alert('Record updated sucessfully!')", true);
            }
            else
            {
                //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "script", "javascript:alert('Problem in update record!')", true);
            }
        }
        catch
        {
        }
        grvUnit.EditIndex = -1;
        FillGrid();
                 
       
    }
    protected void grvUnit_RowEditing(object sender, GridViewEditEventArgs e)
    {

        grvUnit.EditIndex = e.NewEditIndex;
        FillGrid();

    }
    protected void grvUnit_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {

        grvUnit.EditIndex = -1;
        FillGrid();

    }
    protected void grvUnit_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        grvUnit.PageIndex = e.NewPageIndex;
        grvUnit.DataBind();
        FillGrid();
        
    }
}
