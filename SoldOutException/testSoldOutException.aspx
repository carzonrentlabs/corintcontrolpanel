<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="~/usercontrol/Headerctrl.ascx" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeFile="testSoldOutException.aspx.cs" Inherits="testSoldOutException" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>CarzonRent :: Internal software Control panel</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
	<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta name="vs_defaultClientScript" content="JavaScript">
	<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
    <script language="JavaScript" src="../utilityfunction.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <uc1:Headerctrl id="Headerctrl1" runat="server"></uc1:Headerctrl>
    <div>
        <table align="center" style="padding-top: 60px; font-size: small;">
        <tbody>
            <tr>
                <td colspan="4">
                    &nbsp;</td>
            </tr>
            <tr>
                <td colspan="4">
                    &nbsp;</td>
            </tr>
            <tr>
                <td colspan="4">
                    &nbsp;</td>
            </tr>
            <tr>
                <td colspan="4">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="font-size:small;">
                    Client Name</td>
                <td align="center">
                    <strong>&nbsp;:&nbsp;</strong></td>
                <td>
                    <%--<asp:TextBox ID="txtSearchClientName" runat="server"></asp:TextBox>--%>
         
                    <asp:DropDownList ID="ddlSearchClient" runat="server">
                    </asp:DropDownList>
                </td>
                <td>
                    <%--<asp:CheckBox ID="chkAll" runat="server" Text="All City" OnCheckedChanged="chkAll_CheckedChanged" />--%>
                    <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" />
                    <asp:Button ID="btnSave" runat="server" Text="Add" OnClick="btnSave_Click" />
                </td>
            </tr>
            <tr>
                <td colspan="4">
                </td>
            </tr>
            <tr>
                <td colspan="4" style="padding-top: 20px; padding-bottom: 30px; text-align:center;">
                
                    <asp:GridView ID="grvUnit" runat="server" AutoGenerateColumns="False" BackColor="LightGoldenrodYellow"
                        BorderColor="Tan" BorderWidth="1px" CellPadding="2" AllowPaging="True" Font-Size="Small" OnPageIndexChanging="grvUnit_PageIndexChanging" 
                        OnRowCancelingEdit="grvUnit_RowCancelingEdit" 
                        OnRowDataBound="grvUnit_RowDataBound" 
                        OnRowEditing="grvUnit_RowEditing"
                        OnRowUpdating="grvUnit_RowUpdating" ForeColor="Black" GridLines="Both">
                        <FooterStyle BackColor="Tan" />
                        <PagerStyle ForeColor="DarkSlateBlue" HorizontalAlign="Center" BackColor="PaleGoldenrod" />
                        <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
                        <HeaderStyle BackColor="Tan" Font-Bold="True" />
                        <Columns>
                          
                            <asp:TemplateField HeaderText="ID" SortExpression="PromoCodeDesc" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblID" Text=' <%# Eval("ID")%>' runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Unit Name" SortExpression="PromoCodeDesc">
                                <ItemTemplate>
                                    <asp:Label ID="lblUnitName" Text=' <%# Eval("UnitName")%>' runat="server" />
                                </ItemTemplate>
                                <EditItemTemplate>
                                    
                                    <asp:DropDownList ID="ddlGrdUnitName" runat="server" AutoPostBack="true">
                                    </asp:DropDownList>

                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Car Category Name" SortExpression="PromoCodeDesc">
                                <ItemTemplate>
                                    <asp:Label ID="lblCatName" Text=' <%# Eval("CarCatName")%>' runat="server" />
                                </ItemTemplate>
                                <EditItemTemplate>
                                    
                                       <asp:DropDownList ID="ddlGrdCatagoryName" runat="server" AutoPostBack="true">
                                       </asp:DropDownList>
                                    
                                </EditItemTemplate>
                            </asp:TemplateField>
                            
                            <asp:TemplateField HeaderText="Client Name" SortExpression="PromoCodeDesc">
                                <ItemTemplate>
                                    <asp:Label ID="lblClientName" Text=' <%# Eval("ClientCoName")%>' runat="server" />
                                </ItemTemplate>
                                <EditItemTemplate>
                                   <asp:DropDownList ID="ddlGrdClientName" runat="server" AutoPostBack="true" Enabled="false">
                                   </asp:DropDownList>
                                
                                </EditItemTemplate>
                            </asp:TemplateField>
                            
                            <asp:TemplateField HeaderText="Active" SortExpression="PromoCodeDesc">
                                <ItemTemplate>
                                    <asp:Label ID="lblStatus" Text=' <%# Eval("Active")%>' runat="server" />
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblStatus1" Text=' <%# Eval("Active")%>' runat="server" Visible="false" />
                                    <asp:CheckBox ID="chkEditStatus" runat="server" />
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Edit" ShowHeader="False">
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnedit" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:LinkButton ID="btnupdate" runat="server" CommandName="Update" Text="Update"></asp:LinkButton>
                                    <asp:LinkButton ID="btncancel" runat="server" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                </EditItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle BackColor="PaleGoldenrod" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
            <td style="text-align:left;" colspan="4">
                <div id="DivAddException" runat="server" visible="false" style="padding-top: 30px;
                        position: absolute; background-color: ButtonFace; border-color: FFF7E7; border-style: groove; text-align:center;">
                        <table align="center" style="padding-top: 50px;">
                            <tbody>
                                <tr>
                                    <td colspan="3" style="text-align: center; padding-top: 10px; color:Red; font-weight:bold;">
                                        Add SoldOut Exception</td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                    </td>
                                </tr>
                                <tr align="center">
                                    <td align="right">
                                        * Client Name</td>
                                    <td>
                                        <strong>&nbsp;:&nbsp;</strong></td>
                                    <td style="width: 147px; text-align:left;">
                                        <asp:DropDownList ID="ddlClient" runat="server">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="rfvClient" runat="server" ErrorMessage="*" InitialValue="0" ControlToValidate="ddlClient"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                    </td>
                                </tr>
                                
                                <tr align="center">
                                    <td align="right">
                                        Car Category Name</td>
                                    <td>
                                        <strong>&nbsp;:&nbsp;</strong></td>
                                    <td style="width: 147px;text-align:left;">
                                    <asp:DropDownList ID="ddlCategory" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                    </td>
                                </tr>
                                <tr align="center">
                                    <td align="right">
                                         Unit Name</td>
                                    <td>
                                        <strong>&nbsp;:&nbsp;</strong></td>
                                    <td style="width: 147px;text-align:left;">
                                     <asp:DropDownList ID="ddlUnit" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                    </td>
                                </tr>
                                <tr align="center">
                                    <td align="right">
                                         Active</td>
                                    <td>
                                        <strong>&nbsp;:&nbsp;</strong></td>
                                    <td style="width: 147px">
                                        <asp:CheckBox ID="chkStatus" runat="server" Text="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" align="center">
                                        <asp:Button ID="btnSaveException" runat="server" Text="Add " OnClick="btnSaveException_Click" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close " CausesValidation="false" OnClick="btnClose_Click" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
            </td>
            </tr>
        </tbody>
    </table>
    
    </div>
    </form>
</body>
</html>
