<%@ Page Language="vb" AutoEventWireup="false" CodeFile="AddSoldOut.aspx.vb" Inherits="SoldOutException_AddSoldOut" %>

<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>CarzonRent :: Internal software Control Panel</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />

    <script language="JavaScript" src="../utilityfunction.js"></script>

    <link href="../HertzInt.css" type="text/css" rel="stylesheet" />

    <script language="JavaScript">
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <uc1:Headerctrl ID="Headerctrl1" runat="server"></uc1:Headerctrl>
        <table id="InsertResult" runat="server" align="center">
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    Want To insert Another Record:
                    <asp:HyperLink ID="hlnk" runat="server" ForeColor="red" NavigateUrl="~/SoldOutException/AddSoldOut.aspx">Click Here!</asp:HyperLink></td>
            </tr>
        </table>
        <table id="Insert" runat="server" align="center" style="padding-top: 50px;">
            <tbody>
                <tr>
                    <td colspan="3" style="text-align: center; padding-top: 10px; color: Red; font-weight: bold;">
                        Add SoldOut Exception</td>
                </tr>
                <tr>
                    <td colspan="3" style="text-align: center;">
                    </td>
                </tr>
                <tr align="center">
                    <td align="right">
                        * Client Name</td>
                    <td>
                        <strong>&nbsp;:&nbsp;</strong></td>
                    <td style="width: 147px; text-align: left;">
                        <asp:DropDownList ID="ddlClient" runat="server">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rfvClient" runat="server" ErrorMessage="*" InitialValue="0"
                            ControlToValidate="ddlClient"></asp:RequiredFieldValidator>
                           
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                    </td>
                </tr>
                <tr align="center">
                    <td align="right">
                        Car Category Name</td>
                    <td>
                        <strong>&nbsp;:&nbsp;</strong></td>
                    <td style="width: 147px; text-align: left;">
                        <asp:DropDownList ID="ddlCategory" runat="server">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                    </td>
                </tr>
                <tr align="center">
                    <td align="right">
                        Unit Name</td>
                    <td>
                        <strong>&nbsp;:&nbsp;</strong></td>
                    <td style="width: 147px; text-align: left;">
                        <asp:DropDownList ID="ddlUnit" runat="server">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                    </td>
                </tr>
                <tr align="center">
                    <td align="right">
                        Active</td>
                    <td>
                        <strong>&nbsp;:&nbsp;</strong></td>
                    <td style="width: 147px">
                        <asp:CheckBox ID="chkStatus" runat="server" Text="" />
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                    </td>
                </tr>
                <tr>
                    <td colspan="3" align="center">
                        <%-- <asp:Button ID="btnSaveException" runat="server" Text="Add " OnClick="btnSaveException_Click" />--%>
                        <asp:Button ID="btnSaveException" runat="server" Text="Add"></asp:Button>
                        <asp:Button ID="btnClose" runat="server" Text="Close " CausesValidation="false" OnClick="btnClose_Click" />
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</body>
</html>
