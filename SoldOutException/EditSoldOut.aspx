<%@ Page Language="VB" AutoEventWireup="false" CodeFile="EditSoldOut.aspx.vb" Inherits="SoldOutException_EditSoldOut" %>

<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>CarzonRent :: Internal software Control panel</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">

    <script language="JavaScript" src="../utilityfunction.js"></script>

    <link href="../HertzInt.css" type="text/css" rel="stylesheet">

    <script language="JavaScript">
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div>
            <uc1:Headerctrl ID="Headerctrl1" runat="server"></uc1:Headerctrl>
            <table id="UpdateResult" runat="server" align="center">
                <tr>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblMessage" runat="server"></asp:Label><br />
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        Want to modify another then :
                        <asp:HyperLink ID="hLnkModifyRecord" runat="server" NavigateUrl="~/SoldOutException/SearchSoldOut.aspx"
                            ForeColor="red">click here!</asp:HyperLink></td>
                </tr>
            </table>
            <table id="Update" runat="server" align="center" style="padding-top: 50px;">
                <tbody>
                   <tr>
                <td colspan="3">
                    &nbsp;</td>
            </tr>
                    <tr>
                        <td colspan="3" style="text-align: center; padding-top: 10px; color: Red; font-weight: bold;">
                            Modify SoldOut Exception</td>
                    </tr>
                    <tr>
                        <td colspan="3">&nbsp;
                        </td>
                    </tr>
                    <tr align="center">
                        <td align="right">
                            * Client Name</td>
                        <td>
                            <strong>&nbsp;:&nbsp;</strong></td>
                        <td style="width: 147px; text-align: left;">
                            <asp:DropDownList ID="ddlClient" runat="server">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvClient" runat="server" ErrorMessage="*" InitialValue="0"
                                ControlToValidate="ddlClient"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                        </td>
                    </tr>
                    <tr align="center">
                        <td align="right">
                            Car Category Name</td>
                        <td>
                            <strong>&nbsp;:&nbsp;</strong></td>
                        <td style="width: 147px; text-align: left;">
                            <asp:DropDownList ID="ddlCategory" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                        </td>
                    </tr>
                    <tr align="center">
                        <td align="right">
                            Unit Name</td>
                        <td>
                            <strong>&nbsp;:&nbsp;</strong></td>
                        <td style="width: 147px; text-align: left;">
                            <asp:DropDownList ID="ddlUnit" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                        </td>
                    </tr>
                    <tr align="center">
                        <td align="right">
                            Active</td>
                        <td>
                            <strong>&nbsp;:&nbsp;</strong></td>
                        <td style="width: 147px">
                            <asp:CheckBox ID="chkStatus" runat="server" Text="" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" align="center">
                            <asp:Button ID="btnUpdateException" runat="server" Text="Update " />
                            <asp:Button ID="btnClose" runat="server" Text="Close " CausesValidation="false" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </form>
</body>
</html>
