Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Partial Class SoldOutException_EditSoldOut
    Inherits System.Web.UI.Page
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btnSaveException As System.Web.UI.WebControls.Button
    'Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    'Protected WithEvents btnclose As System.Web.UI.WebControls.Label
    'Protected WithEvents ddlClient As System.Web.UI.WebControls.Label
    'Protected WithEvents ddlCategory As System.Web.UI.WebControls.Label
    'Protected WithEvents ddlUnit As System.Web.UI.WebControls.Label
    'Protected WithEvents chkStatus As System.Web.UI.WebControls.CheckBox
    'Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink


    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            populateddl()

            Dim dtrreader As SqlDataReader
            Dim accessdata As clsutility
            accessdata = New clsutility
            Dim strQuery As StringBuilder
            strQuery = New StringBuilder
            strQuery = strQuery.Append("select cs.ID, cs.UnitID, cs.CategoryID,cs.ClientID")
            strQuery = strQuery.Append(",case cs.UnitID when 0 then 'All' else cu.UnitName end as UnitName ")
            strQuery = strQuery.Append(",case cs.CategoryID when 0 then 'All' else cc.CarCatName end  as CarCatName,")
            strQuery = strQuery.Append("ccm.ClientCoName, cs.Active")
            strQuery = strQuery.Append(" from CorIntSoldOutException as cs inner join CORIntClientCoMaster as ccm")
            strQuery = strQuery.Append(" on cs.ClientID=ccm.ClientCoID left outer join CORIntUnitMaster as cu")
            strQuery = strQuery.Append(" on cs.UnitID=cu.UnitID left outer join CORIntCarCatMaster as cc")

            strQuery = strQuery.Append(" on cs.CategoryID=cc.CarCatID  where  cs.ID=" & Request.QueryString("id") & " ")
            dtrreader = accessdata.funcGetSQLDataReader(strQuery.ToString())

            ''Response.Write(strQuery.ToString())
            ''Response.End()

            dtrreader.Read()
            ddlClient.Items.FindByValue(dtrreader("ClientID")).Selected = True
            ddlUnit.Items.FindByValue(dtrreader("UnitID")).Selected = True
            ddlCategory.Items.FindByValue(dtrreader("CategoryID")).Selected = True
            chkStatus.Checked = dtrreader("active")
            UpdateResult.Visible = False
            Update.Visible = True
        End If

    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Response.Redirect("EditSoldOut.aspx")

    End Sub

   
    Protected Sub btnUpdateException_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateException.Click
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        Dim intuniqcheck As SqlParameter
        Dim intuniqvalue As Int32
        Dim status As Int32
        status = 0
        If chkStatus.Checked = True Then
            status = 1
        End If

        cmd = New SqlCommand("prc_UpdateSoldOutException", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Id", Request.QueryString("id"))
        cmd.Parameters.AddWithValue("@UnitID", Convert.ToInt32(ddlUnit.SelectedValue.ToString()))
        cmd.Parameters.AddWithValue("@CategoryID", Convert.ToInt32(ddlCategory.SelectedValue.ToString()))
        cmd.Parameters.AddWithValue("@ClientID", Convert.ToInt32(ddlClient.SelectedValue.ToString()))
        cmd.Parameters.AddWithValue("@Active", status)
        cmd.Parameters.AddWithValue("@uid", Session("loggedin_user"))
        '  Try
        MyConnection.Open()
        intuniqvalue = cmd.ExecuteNonQuery()
        MyConnection.Close()
        If intuniqvalue = 0 Then
            lblMessage.Visible = True
            lblMessage.Text = "Problem in updating the Record!."
        ElseIf intuniqvalue = 1 Then
            lblMessage.Visible = True
            lblMessage.Text = "You have updated  Sold Out exception  successfully"
            UpdateResult.Visible = True
            Update.Visible = False
        ElseIf intuniqvalue = -1 Then
            lblMessage.Visible = True
            lblMessage.Text = "Problem in updating the Record!."
        End If
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function

    Sub populateddl()
        'Dim dtrreader As SqlDataReader
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility

        ddlClient.DataSource = objAcessdata.funcGetSQLDataReader("select ClientCoID,ClientCoName from CORIntClientCoMaster where Active=1 ")
        ddlClient.DataValueField = "ClientCoID"
        ddlClient.DataTextField = "ClientCoName"
        ddlClient.DataBind()
        ddlClient.Items.Insert(0, New ListItem("-Select-", "0"))
        ddlClient.AppendDataBoundItems = True


        ddlUnit.DataSource = objAcessdata.funcGetSQLDataReader("select UnitID,UnitName from CORIntUnitMaster where Active=1")
        ddlUnit.DataValueField = "UnitID"
        ddlUnit.DataTextField = "UnitName"
        ddlUnit.DataBind()
        ddlUnit.Items.Insert(0, New ListItem("-All-", "0"))
        ddlUnit.AppendDataBoundItems = True


        ddlCategory.DataSource = objAcessdata.funcGetSQLDataReader("select CarCatID,CarCatName from CORIntCarCatMaster where Active=1")
        ddlCategory.DataValueField = "CarCatID"
        ddlCategory.DataTextField = "CarCatName"
        ddlCategory.DataBind()
        ddlCategory.Items.Insert(0, New ListItem("-All-", "0"))
        ddlCategory.AppendDataBoundItems = True

        objAcessdata.Dispose()

    End Sub
End Class
