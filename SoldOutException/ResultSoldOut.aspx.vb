Imports commonutility
Imports System.text
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Imports System.Web.UI
Partial Class SoldOutException_ResultSoldOut
    Inherits System.Web.UI.Page
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents tblRecDetail As System.Web.UI.WebControls.Table

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim clientCoId As String
        If Request.QueryString("id") <> "-1" Then
            clientCoId = Convert.ToString(Request.QueryString("id"))
        End If
        If Not IsPostBack Then
            getvalue(clientCoId)

        End If
    End Sub
    Sub getvalue(ByVal strorderby As String)
        Dim strquery As StringBuilder
        Dim dtrreader As SqlDataReader
        Dim accessdata As clsutility
        accessdata = New clsutility
        If strorderby = "" Then
            strquery = New StringBuilder("select cs.ID, cs.UnitID, cs.CategoryID,cs.ClientID")
            strquery = strquery.Append(", case cs.UnitID when 0 then 'All' else cu.UnitName end as UnitName")
            strquery = strquery.Append(", case cs.CategoryID when 0 then 'All' else cc.CarCatName end  as CarCatName")
            strquery = strquery.Append(", ccm.ClientCoName, cs.Active")
            strquery = strquery.Append(" from CorIntSoldOutException as cs inner join CORIntClientCoMaster as ccm")
            strquery = strquery.Append(" on cs.ClientID=ccm.ClientCoID left outer join CORIntUnitMaster as cu")
            strquery = strquery.Append(" on cs.UnitID=cu.UnitID left outer join CORIntCarCatMaster as cc")
            strquery = strquery.Append(" on cs.CategoryID=cc.CarCatID")
            If Request.QueryString("id") <> "-1" Then
                strquery.Append(" where ccm.ClientCoID=" & Request.QueryString("id") & " ")
            End If
            'strquery = strquery.Append(" order by " & strorderby & "")

        ElseIf strorderby <> "" Then
            strquery = New StringBuilder("select cs.ID, cs.UnitID, cs.CategoryID,cs.ClientID")
            strquery = strquery.Append(", case cs.UnitID when 0 then 'All' else cu.UnitName end as UnitName")
            strquery = strquery.Append(", case cs.CategoryID when 0 then 'All' else cc.CarCatName end  as CarCatName")
            strquery = strquery.Append(", ccm.ClientCoName, cs.Active")
            strquery = strquery.Append(" from CorIntSoldOutException as cs inner join CORIntClientCoMaster as ccm")
            strquery = strquery.Append(" on cs.ClientID=ccm.ClientCoID left outer join CORIntUnitMaster as cu")
            strquery = strquery.Append(" on cs.UnitID=cu.UnitID left outer join CORIntCarCatMaster as cc")
            strquery = strquery.Append(" on cs.CategoryID=cc.CarCatID")
            If Request.QueryString("id") <> "-1" Then
                strquery.Append(" where ccm.ClientCoID=" & Request.QueryString("id") & " ")
            End If
            If strorderby = "cs.CarCatName" Or strorderby = "cs.CarCatName" Or strorderby = "cs.CarCatName" Then
                strquery = strquery.Append(" order by " & strorderby & "")
            End If

            'Response.Write(strquery.ToString())
            'Response.End()
        End If


            ''strquery.Append(" order by " & strorderby & "")
            'End If



            dtrreader = accessdata.funcGetSQLDataReader(strquery.ToString())
            While dtrreader.Read
                Dim Temprow As New TableRow
                Temprow.HorizontalAlign = HorizontalAlign.Center


                Dim Tempcell As New TableCell
                Tempcell.Controls.Add(New LiteralControl(dtrreader("UnitName") & ""))
                Temprow.Cells.Add(Tempcell)

                Dim Tempcel2 As New TableCell
                Tempcel2.Controls.Add(New LiteralControl(dtrreader("CarCatName") & ""))
                Temprow.Cells.Add(Tempcel2)

                Dim Tempcel3 As New TableCell
                Tempcel3.Controls.Add(New LiteralControl(dtrreader("ClientCoName") & ""))
                Temprow.Cells.Add(Tempcel3)

                Dim Tempcel4 As New TableCell
                Tempcel4.Controls.Add(New LiteralControl(dtrreader("active") & ""))
                Temprow.Cells.Add(Tempcel4)

                Dim Tempcel5 As New TableCell
                Tempcel5.Controls.Add(New LiteralControl("<a href=EditSoldOut.aspx?ID=" & dtrreader("ID") & " >Edit</a>"))
                Temprow.Cells.Add(Tempcel5)
                'adding the Tables rows to the table
                tblRecDetail.Rows.Add(Temprow)

            End While
            dtrreader.Close()
    End Sub
    Sub SortGird(ByVal sender As Object, ByVal e As System.EventArgs)
        Select Case sender.id
            Case "lnkBtnCat"
                getvalue("cc.CarCatName")
            Case "lnkBtnUnit"
                getvalue("cu.UnitName")
            Case "lnkBtnClient"
                getvalue("ccm.ClientCoName")

        End Select

    End Sub


End Class
