<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ResultSoldOut.aspx.vb" Inherits="SoldOutException_ResultSoldOut" %>

<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>CarzonRent :: Internal software Control panel</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">

    <script language="JavaScript" src="../utilityfunction.js"></script>

    <link href="../HertzInt.css" type="text/css" rel="stylesheet">
</head>
<body>
    <form id="Form1" method="post" runat="server">
        <uc1:Headerctrl ID="Headerctrl1" runat="server"></uc1:Headerctrl>
        <table align="center">
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="center">
                    <strong><u>Search Sold Out Result</u></strong></td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td valign="top">
                    <asp:Table ID="tblRecDetail" HorizontalAlign="Center" runat="server" BorderColor="#cccc99"
                        BorderStyle="Solid" GridLines="both" CellSpacing="0" CellPadding="0">
                        <asp:TableRow>
                            <asp:TableHeaderCell>
                                &nbsp;&nbsp;<asp:LinkButton runat="server" Text="Unit Name" OnClick="SortGird" ID="lnkBtnUnit"></asp:LinkButton>&nbsp;&nbsp;
                            </asp:TableHeaderCell>
                            <asp:TableHeaderCell>
                                &nbsp;&nbsp;<asp:LinkButton runat="server" Text="Category Name" OnClick="SortGird"
                                    ID="lnkBtnCat"></asp:LinkButton>&nbsp;&nbsp;
                            </asp:TableHeaderCell>
                            <asp:TableHeaderCell>
                                &nbsp;&nbsp;<asp:LinkButton runat="server" Text="Client Name" OnClick="SortGird"
                                    ID="lnkBtnClient"></asp:LinkButton>&nbsp;&nbsp;
                            </asp:TableHeaderCell>
                            <asp:TableHeaderCell>
                                &nbsp;&nbsp;<asp:LinkButton runat="server" Text="Active" OnClick="SortGird" ID="lnkActive"></asp:LinkButton>&nbsp;&nbsp;
                            </asp:TableHeaderCell>
                        </asp:TableRow>
                    </asp:Table>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
