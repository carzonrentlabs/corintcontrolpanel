<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Src="PkgsAssignForm.aspx.vb" Inherits="PkgsAssignForm"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CarzonRent :: Internal software Control panel</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
		<script language="javascript">
	function assignmodules()
		{
		if (parseInt(document.forms[0].lstavailablemodule.selectedIndex,10) >= 0 )
		    {

			for(i = document.forms[0].lstavailablemodule.length - 1; i >= 0;i--)
			{
				if (document.forms[0].lstavailablemodule[i].selected)
				  {
					var oOption = document.createElement("OPTION");
					document.forms[0].lstAssignedModules.options.add(oOption);
					oOption.innerText = document.forms[0].lstavailablemodule.options[document.forms[0].lstavailablemodule.selectedIndex].text;
					oOption.value = document.forms[0].lstavailablemodule.options[document.forms[0].lstavailablemodule.selectedIndex].value;
					document.forms[0].lstavailablemodule.options[document.forms[0].lstavailablemodule.selectedIndex] = null ;
				}

			}
		  }
		else { alert("Please select a package");}
		}
	function assignmodulesSD()
		{
		if (parseInt(document.forms[0].lstavailablemoduleSD.selectedIndex,10) >= 0 )
		    {

			for(i = document.forms[0].lstavailablemoduleSD.length - 1; i >= 0;i--)
			{
				if (document.forms[0].lstavailablemoduleSD[i].selected)
				  {
					var oOption = document.createElement("OPTION");
					document.forms[0].lstAssignedModulesSD.options.add(oOption);
					oOption.innerText = document.forms[0].lstavailablemoduleSD.options[document.forms[0].lstavailablemoduleSD.selectedIndex].text;
					oOption.value = document.forms[0].lstavailablemoduleSD.options[document.forms[0].lstavailablemoduleSD.selectedIndex].value;
					document.forms[0].lstavailablemoduleSD.options[document.forms[0].lstavailablemoduleSD.selectedIndex] = null ;
				}

			}
		  }
		else { alert("Please select a package");}
		}
	function removemodules()
		{
		if (parseInt(document.forms[0].lstAssignedModules.selectedIndex,10) >= 0 )
		    {

			for(i = document.forms[0].lstAssignedModules.length - 1; i >= 0;i--)
			{
				if (document.forms[0].lstAssignedModules[i].selected)
				  {
					var oOption = document.createElement("OPTION");
					document.forms[0].lstavailablemodule.options.add(oOption);
					oOption.innerText = document.forms[0].lstAssignedModules.options[document.forms[0].lstAssignedModules.selectedIndex].text;
					oOption.value = document.forms[0].lstAssignedModules.options[document.forms[0].lstAssignedModules.selectedIndex].value;
					document.forms[0].lstAssignedModules.options[document.forms[0].lstAssignedModules.selectedIndex] = null ;
				}
			}
		  }
		else { alert("Please select a package");}
		}
	function removemodulesSD()
		{
		if (parseInt(document.forms[0].lstAssignedModulesSD.selectedIndex,10) >= 0 )
		    {

			for(i = document.forms[0].lstAssignedModulesSD.length - 1; i >= 0;i--)
			{
				if (document.forms[0].lstAssignedModulesSD[i].selected)
				  {
					var oOption = document.createElement("OPTION");
					document.forms[0].lstavailablemoduleSD.options.add(oOption);
					oOption.innerText = document.forms[0].lstAssignedModulesSD.options[document.forms[0].lstAssignedModulesSD.selectedIndex].text;
					oOption.value = document.forms[0].lstAssignedModulesSD.options[document.forms[0].lstAssignedModulesSD.selectedIndex].value;
					document.forms[0].lstAssignedModulesSD.options[document.forms[0].lstAssignedModulesSD.selectedIndex] = null ;
				}
			}
		  }
		else { alert("Please select a package");}
		}
		
	function postForm()
	{
	var strTmpValues = new String();
	var bFlag  = false;
	for(i = 0; i != document.forms[0].lstAssignedModules.length;i++)
		{
			strTmpValues += document.forms[0].lstAssignedModules.options[i].value ;
			if (i + 1 !=  document.forms[0].lstAssignedModules.length)
				{
				strTmpValues += ",";
				}
		}

	document.forms[0].hdnSelectedlst.value  = strTmpValues;

	var strTmpValuesSD = new String();
	var bFlagSD  = false;
	for(i = 0; i != document.forms[0].lstAssignedModulesSD.length;i++)
		{
			strTmpValuesSD += document.forms[0].lstAssignedModulesSD.options[i].value ;
			if (i + 1 !=  document.forms[0].lstAssignedModulesSD.length)
				{
				strTmpValuesSD += ",";
				}
		}

	document.forms[0].hdnSelectedlstSD.value  = strTmpValuesSD;
	
		}
		</script>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<uc1:headerctrl id="Headerctrl1" runat="server"></uc1:headerctrl>
			<table align="center">
				<TBODY>
					<asp:Panel ID="pnlmainform" Runat="server">
						<TR>
							<TD align="center" colSpan="3"><STRONG><U>Assign special "chauffeur drive packages" to 
										&nbsp;
										<asp:label id="lblusername" runat="server" CssClass="input"></asp:label></U></STRONG><INPUT id="hdnSelectedlst" type="hidden" name="hdnSelectedlst" runat="server"></TD>
						</TR>
						<TR>
							<TD align="center" colSpan="3">List of available special packages<BR>
								<asp:listbox id="lstavailablemodule" runat="server" CssClass="input" Rows="15" Font-Size="X-Small"></asp:listbox></TD>
						</TR>
						<TR>
							<TD vAlign="middle" align="center" colSpan="3" rowSpan="1"><INPUT class="formButton" id="btnPut" onclick="assignmodules()" type="button" value="  >>  Add       ">
								<INPUT class="formButton" id="btnRemove" onclick="removemodules()" type="button" value="  <<  Remove">
							</TD>
						</TR>
						<TR>
							<TD align="center" colSpan="3" rowSpan="1">List of all assigned special packages<BR>
								<asp:listbox id="lstAssignedModules" runat="server" CssClass="input" Rows="15" Font-Size="X-Small"></asp:listbox></TD>
						</TR>
						<TR>
							<TD align="center" colSpan="3"><STRONG><U>Assign special "self drive packages" to &nbsp;
										<asp:label id="lblusernameSD" runat="server" CssClass="input"></asp:label></U></STRONG><INPUT id="hdnSelectedlstSD" type="hidden" name="hdnSelectedlstSD" runat="server"></TD>
						</TR>
						<TR>
							<TD align="center" colSpan="3">List of available special packages<BR>
								<asp:listbox id="lstavailablemoduleSD" runat="server" CssClass="input" Rows="15" Font-Size="X-Small"></asp:listbox></TD>
						</TR>
						<TR>
							<TD vAlign="middle" align="center" colSpan="3" rowSpan="1"><INPUT class="formButton" id="btnPutSD" onclick="assignmodulesSD()" type="button" value="  >>  Add       ">
								<INPUT class="formButton" id="btnRemoveSD" onclick="removemodulesSD()" type="button" value="  <<  Remove">
							</TD>
						</TR>
						<TR>
							<TD align="center" colSpan="3" rowSpan="1">List of all assigned special packages<BR>
								<asp:listbox id="lstAssignedModulesSD" runat="server" CssClass="input" Rows="15" Font-Size="X-Small"></asp:listbox></TD>
						</TR>
						<TR>
							<TD align="center" colSpan="3">
								<asp:Button id="btnsubmit" runat="server" CssClass="input" Text="Submit"></asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;
								<asp:Button id="btnreset" runat="server" CssClass="input" Text="Reset"></asp:Button></TD>
						</TR>
					</asp:Panel>
					<asp:Panel ID="pnlconfirmation" Visible="False" Runat="server">
						<TR align="center">
							<TD colSpan="3"><BR>
								<BR>
								<BR>
								<BR>
								<BR>
								<asp:Label id="lblMessage" runat="server"></asp:Label></TD>
						</TR>
						<TR align="center">
							<TD colSpan="3">
								<asp:HyperLink id="hyplnkretry" runat="server"></asp:HyperLink></TD>
						</TR>
					</asp:Panel>
				</TBODY>
			</table>
		</form>
	</body>
</HTML>
