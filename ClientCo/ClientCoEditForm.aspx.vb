Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Mail
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Drawing
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.HtmlControls
Imports System.IO
Imports System.IO.FileStream
Imports System.IO.File
Imports System.Net
Imports System.Text
Imports System.Object
Imports System.MarshalByRefObject
Imports System.Net.WebRequest
Public Class ClientCoEditForm
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtcompanyName As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtSpecialInstructions As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtaddress As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtbilladdress As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlcity As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlcityBillTo As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlsourceCity As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtfname1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtmidname1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtlname1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtphone1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtdesig1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtfname2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtmnae2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtlname2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtphone2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtdesig2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtfname3 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtmname2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtlname3 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtphone3 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtdesig3 As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtfax As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtemail As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtbookmode As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtbookmode As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlcompanytype As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddldiscount As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddldiscountdecimal As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlpayment As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtcclimit As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlbillingbasis As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents tstsvctax As System.Web.UI.WebControls.TextBox
    'Protected WithEvents ddlairport As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents ddlhotel As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents ddlcorporate As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents ddlselfdrive As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddTariffType As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddCreditType As System.Web.UI.WebControls.DropDownList
    Protected WithEvents chkbta As System.Web.UI.WebControls.CheckBox
    Protected WithEvents txtamex As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtRemarks As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtdetail As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkactive As System.Web.UI.WebControls.CheckBox
    Protected WithEvents enablePersonalTrip As System.Web.UI.WebControls.CheckBox
    Protected WithEvents enableOdometerMask As System.Web.UI.WebControls.CheckBox
    Protected WithEvents odometerMaskDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkDispDiscInv As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkEmail As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkApplyServiceTaxYN As System.Web.UI.WebControls.CheckBox
    'Protected WithEvents chkApplyEduCessYN As System.Web.UI.WebControls.CheckBox
    'Protected WithEvents chkApplyHduCessYN As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkSEZClientYN As System.Web.UI.WebControls.CheckBox
    Protected WithEvents txtSEZExpiry As System.Web.UI.WebControls.TextBox
    'Protected WithEvents chkApplyDSTYN As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkApplyCCYN As System.Web.UI.WebControls.CheckBox
    'added by rahul on 13 May 2010
    Protected WithEvents chkCreditMailYN As System.Web.UI.WebControls.CheckBox
    Protected WithEvents txtContractDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkFuelSurchargeYN As System.Web.UI.WebControls.CheckBox
    Protected WithEvents btnsubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnreset As System.Web.UI.WebControls.Button
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents hyplnkretry2 As System.Web.UI.WebControls.HyperLink
    Protected WithEvents hyplnkretry3 As System.Web.UI.WebControls.HyperLink
    Protected WithEvents hyplnkretry4 As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    Protected WithEvents lblerr As System.Web.UI.WebControls.Label
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    Protected WithEvents txtUserName As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtPassword As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtFrom As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtTo As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtSubDays As System.Web.UI.WebControls.TextBox
    Protected WithEvents rdStatus1 As System.Web.UI.WebControls.RadioButton
    Protected WithEvents rdStatus2 As System.Web.UI.WebControls.RadioButton
    Protected WithEvents txtActiveDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents Textbox2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Textbox3 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Textbox4 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Textbox5 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Textbox6 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Textbox7 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Textbox8 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Textbox9 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Textbox10 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Textbox11 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Textbox12 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Textbox13 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Textbox14 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Textbox15 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Textbox16 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtPanno As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtTanno As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtAvgMonRev As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlRelationshpMgr As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtEmailAH As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtEmailFM As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtEmailCFO As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtPaymentCP As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtCrdLimit1 As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtCrdLimit2 As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtCrdLimit3 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtCreditMailLimit As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtwarningLimit As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtCircuitbreaker As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtContractStartDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtContractEndDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkEmailRequiredYN As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkVRFRequiredYN As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkHigherExtraAmt As System.Web.UI.WebControls.CheckBox
    'Protected WithEvents chkPrintDS As System.Web.UI.WebControls.CheckBox
    Protected WithEvents HyperLink1 As System.Web.UI.WebControls.HyperLink
    Protected WithEvents txtFileName As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtBSP As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtBSPhone As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtBSPEmail As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtCreditDays As System.Web.UI.WebControls.TextBox
    Protected WithEvents FileUpload1 As System.Web.UI.WebControls.FileUpload
    Protected WithEvents chkEdit As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chk_Entity As System.Web.UI.WebControls.CheckBox
    Protected WithEvents ddlGroupEntity As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtSopFile As System.Web.UI.WebControls.TextBox
    Protected WithEvents SopFileUpload As System.Web.UI.WebControls.FileUpload
    Protected WithEvents hlkSopFile As System.Web.UI.WebControls.HyperLink
    Protected WithEvents chkEditSop As System.Web.UI.WebControls.CheckBox
    Protected WithEvents txtFuelSurchargesThreshold As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkFuelSurchargeYNNegative As System.Web.UI.WebControls.CheckBox
    Protected WithEvents ddlEbillingYN As System.Web.UI.WebControls.DropDownList
    Protected WithEvents chkCAM As System.Web.UI.WebControls.CheckBox
    Protected WithEvents fluCam As System.Web.UI.WebControls.FileUpload
    Protected WithEvents hlkCam As System.Web.UI.WebControls.HyperLink
    Protected WithEvents txtCamFileName As System.Web.UI.WebControls.TextBox
    Protected WithEvents lnkView As System.Web.UI.WebControls.LinkButton
    Protected WithEvents lnkSOAPFile As System.Web.UI.WebControls.LinkButton
    Protected WithEvents lnkDownloads As System.Web.UI.WebControls.LinkButton
    Protected WithEvents ddlAgreementType As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtclientPoc As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkInvoiceMailonClosureYN As System.Web.UI.WebControls.CheckBox
    Protected WithEvents ddlServiceType As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlClientBilling As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtCentralizeEffectiveDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkBilling As System.Web.UI.WebControls.CheckBox
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btnsubmit.Attributes("onClick") = "return validation();"
        If Not Page.IsPostBack Then
            populateddl()
            Dim dtrreader As SqlDataReader
            Dim accessdata As clsutility
            accessdata = New clsutility

            Dim valDisc As String
            Dim valDiscDec As String
            Dim arrDisc As Array
            'dtrreader = accessdata.funcGetSQLDataReader("select a.*, b.ClientCoAddress from CORIntClientCoMaster as a, CORIntClientCoAddMaster as b where a.ClientCoID = b.ClientCoID and a.clientcoid=" & Request.QueryString("id") & " and b.Counter=1 ")

            'Query modified by BSL on 25-April-07
            'dtrreader = accessdata.funcGetSQLDataReader("select a.*, (SELECT ClientCoAddress from CORIntClientCoAddMaster as A where clientcoid = " & Request.QueryString("id") & " and Counter = '1') as ClientCoAddress from CORIntClientCoMaster as a where a.clientcoid = " & Request.QueryString("id"))
            'dtrreader = accessdata.funcGetSQLDataReader("select a.*, (SELECT ClientCoAddress from CORIntClientCoAddMaster_17Jan09 as A where clientcoid = " & Request.QueryString("id") & " and Counter = '1') as ClientCoAddress,(SELECT clientcoBilladdress from CORIntClientCoAddMaster_17Jan09 as A where clientcoid = " & Request.QueryString("id") & " and Counter = '1') as clientcoBilladdress from CORIntClientCoMaster_17Jan09 as a where a.clientcoid = " & Request.QueryString("id"))
            dtrreader = accessdata.funcGetSQLDataReader("select isnull(ClientBillingPkgType,'fornightly') as ClientBilling,a.*, (SELECT ClientCoAddress from CORIntClientCoAddMaster as A where clientcoid = " & Request.QueryString("id") & " and Counter = '1') as ClientCoAddress,(SELECT clientcoBilladdress from CORIntClientCoAddMaster as A where clientcoid = " & Request.QueryString("id") & " and Counter = '1') as clientcoBilladdress from CORIntClientCoMaster as a where a.clientcoid = " & Request.QueryString("id"))
            dtrreader.Read()


            txtcompanyName.Text = dtrreader("clientconame") & ""
            txtaddress.Text = dtrreader("ClientCoAddress") & ""
            txtbilladdress.Text = dtrreader("clientcoBilladdress") & ""
            autoselec_ddl(ddlcity, dtrreader("clientcocityid"))
            If Not IsDBNull(dtrreader("clientcocityidBillTo")) Then
                autoselec_ddl(ddlcityBillTo, dtrreader("clientcocityidBillTo"))
            End If
            'Bill to City ID added by Rahul on 27-May-2011
            If Not IsDBNull(dtrreader("SourceCityID")) Then
                If dtrreader("SourceCityID") <> "0" Then
                    autoselec_ddl(ddlsourceCity, dtrreader("SourceCityID")) 'SourceCityID added by Rahul on 21-Mar-2016
                End If
            End If

            txtActiveDate.Text = dtrreader("activateon") & ""
            txtContractDate.Text = dtrreader("ContractDate") & ""
            txtContractStartDate.Text = dtrreader("ContractStartDate") & ""
            txtContractEndDate.Text = dtrreader("ContractEndDate") & ""
            txtfname1.Text = dtrreader("contact1fname") & ""
            'txtmidname1.Text = dtrreader("contact1mname") & ""
            'txtlname1.Text = dtrreader("contact1lname") & ""
            txtphone1.Text = dtrreader("contact1ph") & ""
            txtEmailAH.Text = dtrreader("ContactEmail1") & ""
            'txtdesig1.Text = dtrreader("contact1desig") & ""
            txtfname2.Text = dtrreader("contact2fname") & ""
            'txtmnae2.Text = dtrreader("contact2mname") & ""
            'txtlname2.Text = dtrreader("contact2lname") & ""
            txtphone2.Text = dtrreader("contact2ph") & ""
            txtEmailFM.Text = dtrreader("ContactEmail2") & ""
            'txtdesig2.Text = dtrreader("contact2desig") & ""
            txtfname3.Text = dtrreader("contact3fname") & ""
            'txtmname2.Text = dtrreader("contact3mname") & ""
            'txtlname3.Text = dtrreader("contact3lname") & ""
            txtphone3.Text = dtrreader("contact3ph") & ""
            txtEmailCFO.Text = dtrreader("ContactEmail3") & ""
            'txtdesig3.Text = dtrreader("contact3desig") & ""
            'txtfax.Text = dtrreader("fax") & ""
            txtemail.Text = dtrreader("emailid") & ""
            'txtbookmode.Text = dtrreader("bookingmode") & ""
            txtbookmode.Items.FindByValue(dtrreader("bookingmode")).Selected = True

            ' Code addded by BSL on 24-April-07, display user name and password
            txtUserName.Text = dtrreader("loginid") & ""
            txtPassword.Text = dtrreader("copwd") & ""

            txtclientPoc.Text = dtrreader("ClientPOC") & ""

            autoselec_ddl(ddlcompanytype, dtrreader("cotypeid"))
            If Not IsDBNull(dtrreader("discountpc")) Then
                arrDisc = CStr(dtrreader("discountpc")).Split(".")
            Else
                arrDisc = CStr("0").Split(".")
            End If


            If arrDisc.Length = 2 Then
                valDisc = arrDisc(0)
                valDiscDec = arrDisc(1)
                If valDisc = "" Then valDisc = "0"
                If valDiscDec = "00" Or valDiscDec = "" Then valDiscDec = "0"
                valDiscDec = Left(valDiscDec, 1)
            Else
                valDisc = arrDisc(0)
                valDiscDec = "0"
            End If

            If (String.IsNullOrEmpty(dtrreader("sopFilename").ToString())) Then
                txtSopFile.Text = "No Image"
                SopFileUpload.Visible = True
                hlkSopFile.Text = ""
                'HyperLink1.Text = ""
            Else
                txtSopFile.Text = dtrreader("sopFilename")
                SopFileUpload.Visible = False
                hlkSopFile.Visible = True
                hlkSopFile.Text = txtSopFile.Text
                hlkSopFile.NavigateUrl = "http://insta.carzonrent.com/MailUpload/ClientBilling_SOP/" + txtSopFile.Text
                'HyperLink1.Target = "_blank"
                lnkSOAPFile.Visible = True
                hlkSopFile.Text = ""
                'HyperLink1.Text = ""
            End If

            If (Convert.ToString(dtrreader("FileName")) = "" Or IsDBNull(dtrreader("FileName"))) Then
                txtFileName.Text = "No Image"
                FileUpload1.Visible = True
                'HyperLink1.Text = ""
            Else
                txtFileName.Text = dtrreader("FileName")
                FileUpload1.Visible = False
                'HyperLink1.Visible = True
                'HyperLink1.Text = txtFileName.Text
                'HyperLink1.NavigateUrl = "http://insta.carzonrent.com/MailUpload/ClientContract/" + txtFileName.Text
                'HyperLink1.Target = "_blank"

            End If

            'CAM 
            If (IsDBNull(dtrreader("CAMFileName"))) Then
                txtCamFileName.Text = "No Image"
                fluCam.Visible = True
                hlkCam.Text = ""
                lnkView.Visible = True
            ElseIf (String.IsNullOrEmpty(dtrreader("CAMFileName"))) Then
                txtCamFileName.Text = "No Image"
                fluCam.Visible = True
                hlkCam.Text = ""
                lnkView.Visible = True

            Else
                txtCamFileName.Text = dtrreader("CAMFileName")
                fluCam.Visible = False
                hlkCam.Visible = True
                'hlkCam.Text = txtCamFileName.Text
                'hlkCam.NavigateUrl = "http://insta.carzonrent.com/MailUpload/ClientBilling_CAM/" + txtCamFileName.Text
                hlkCam.Target = "_blank"
                lnkView.Visible = True
                hlkCam.Text = ""
            End If

            'End of CAM

            autoselec_ddl(ddldiscount, valDisc)
            autoselec_ddl(ddldiscountdecimal, valDiscDec)
            ddlpayment.Items.FindByValue(dtrreader("paymentterms")).Selected = True

            txtcclimit.Text = dtrreader("creditlimit") & ""

            ddlbillingbasis.Items.FindByValue(dtrreader("billingbasis")).Selected = True

            'tstsvctax.Text = dtrreader("servicetaxno") & ""

            'autoselec_ddl(ddlairport, dtrreader("airportunitid"))
            'autoselec_ddl(ddlhotel, dtrreader("hotelunitid"))
            'autoselec_ddl(ddlcorporate, dtrreader("corpunitid"))
            'autoselec_ddl(ddlselfdrive, dtrreader("sdunitid"))
            autoselec_ddl(ddlGroupEntity, dtrreader("GroupEntity"))


            'ddTariffType.Items.FindByValue(dtrreader("TariffType")).Selected = True
            If Not (IsDBNull(dtrreader("CreditType"))) Then
                ddCreditType.Items.FindByValue(dtrreader("CreditType")).Selected = True
            End If
            chkFuelSurchargeYN.Checked = dtrreader("FuelSurchargeYN") & ""
            chkFuelSurchargeYNNegative.Checked = dtrreader("NegativeFuelSurchargeYN") & ""
            If Not IsDBNull(dtrreader("btayn")) Then
                chkbta.Checked = dtrreader("btayn")
            End If

            'txtamex.Text = dtrreader("amexcardno") & ""

            txtRemarks.Text = dtrreader("remarks") & ""
            txtSpecialInstructions.Text = dtrreader("SpecialInstructions") & ""
            txtdetail.Text = dtrreader("DetailsRequired") & ""

            If (Convert.ToBoolean(dtrreader("active")) = True) Then
                chkactive.Enabled = True
            End If
            chkactive.Checked = dtrreader("active") & ""
            If Not IsDBNull(dtrreader("EnablePersonalTripsatCorporateRatesYN")) Then
                enablePersonalTrip.Checked = dtrreader("EnablePersonalTripsatCorporateRatesYN") & ""
            End If
            If Not IsDBNull(dtrreader("EnableOdometerMaskYN")) Then
                enableOdometerMask.Checked = dtrreader("EnableOdometerMaskYN") & ""
            End If
            odometerMaskDate.Text = dtrreader("OdometerMaskEffectiveDate") & ""
            chkEmailRequiredYN.Checked = dtrreader("UploadEmailRequired") & "" 'Email Required
            chkVRFRequiredYN.Checked = dtrreader("UploadVRFRequired") & "" 'VRF Required
            chkHigherExtraAmt.Checked = dtrreader("ApplyHigherExtraAmt") & "" 'VRF Required

            'If Not IsDBNull(dtrreader("PrintOnDsYN")) Then
            '    chkPrintDS.Checked = dtrreader("PrintOnDsYN") & ""
            'End If

            chkDispDiscInv.Checked = dtrreader("DiplayDiscInvYN")
            chkEmail.Checked = dtrreader("EmailReqYN")
            chkApplyServiceTaxYN.Checked = dtrreader("ApplyServiceTaxYN")
            'chkApplyEduCessYN.Checked = dtrreader("ApplyEduCessYN")
            'chkApplyHduCessYN.Checked = dtrreader("ApplyHduCessYN")
            If Not IsDBNull(dtrreader("SEZClientYN")) Then
                chkSEZClientYN.Checked = dtrreader("SEZClientYN")
            Else
                chkSEZClientYN.Checked = False
            End If



            If (IsDBNull(dtrreader("SEZExpiry"))) Then
            Else
                txtSEZExpiry.Text = dtrreader("SEZExpiry")
            End If

            'chkApplyDSTYN.Checked = dtrreader("ApplyDSTYN")

            'chkApplyDSTYN.Checked = dtrreader("ApplyDSTYN")

            chkApplyCCYN.Checked = dtrreader("Credit_LimitActive")
            chkCreditMailYN.Checked = dtrreader("SendCreditMailYN")

            If (Not IsDBNull(dtrreader("SendInvoiceOnClosureYN"))) Then
                chkInvoiceMailonClosureYN.Checked = dtrreader("SendInvoiceOnClosureYN")
            Else
                chkInvoiceMailonClosureYN.Checked = False
            End If




            ' txtFrom.Text = dtrreader("BillPeriodFromDays")
            ' txtTo.Text = dtrreader("BillPeriodtoDays")
            ' txtSubDays.Text = dtrreader("BillSubmissionDays")

            If Not IsDBNull(dtrreader("BillPeriodFromDays")) Then
                txtFrom.Text = dtrreader("BillPeriodFromDays")
            End If
            If Not IsDBNull(dtrreader("BillPeriodtoDays")) Then
                txtTo.Text = dtrreader("BillPeriodtoDays")
            End If
            If Not IsDBNull(dtrreader("BillSubmissionDays")) Then
                txtSubDays.Text = dtrreader("BillSubmissionDays")
            End If


            If dtrreader("ActivationStatus") = False Then
                rdStatus1.Checked = True
                'txtActiveDate.Text = ""
            ElseIf dtrreader("ActivationStatus") = True Then
                rdStatus2.Checked = True
                txtActiveDate.Text = dtrreader("ActivateOn")
            End If
            'Textbox2.Text = dtrreader("CCS_ContactName1") & ""
            'Textbox3.Text = dtrreader("CCS_ContactNo1") & ""
            'Textbox4.Text = dtrreader("CCS_EmailID1") & ""

            'Textbox5.Text = dtrreader("CCS_ContactName2") & ""
            'Textbox6.Text = dtrreader("CCS_ContactNo2") & ""
            'Textbox7.Text = dtrreader("CCS_EmailID2") & ""

            'Textbox8.Text = dtrreader("CCS_ContactName3") & ""
            'Textbox9.Text = dtrreader("CCS_ContactNo3") & ""
            'Textbox10.Text = dtrreader("CCS_EmailID3") & ""

            'Textbox11.Text = dtrreader("CCS_ContactName4") & ""
            'Textbox12.Text = dtrreader("CCS_ContactNo4") & ""
            'Textbox13.Text = dtrreader("CCS_EmailID4") & ""

            'Textbox14.Text = dtrreader("CCS_ContactName5") & ""
            'Textbox15.Text = dtrreader("CCS_ContactNo5") & ""
            'Textbox16.Text = dtrreader("CCS_EmailID5") & ""
            'txtPaymentCP.Text = dtrreader("PaymentCP") & ""
            txtPanno.Text = dtrreader("PANNo") & ""
            txtTanno.Text = dtrreader("TANNo") & ""
            ddlRelationshpMgr.SelectedValue = dtrreader("RelationshipManager") & ""
            txtAvgMonRev.Text = dtrreader("BusinessPotential") & ""

            If Not IsDBNull(dtrreader("SendCreditMailLimit")) Then
                txtCreditMailLimit.Text = dtrreader("SendCreditMailLimit")
            End If

            If Not IsDBNull(dtrreader("WarningLimit")) Then
                txtwarningLimit.Text = dtrreader("WarningLimit")
            End If

            If Not IsDBNull(dtrreader("CircuitBreaker")) Then
                txtCircuitbreaker.Text = dtrreader("CircuitBreaker")
            End If

            'code return by alka on 10 october 2012
            If dtrreader("billSubmissionPerson").ToString() <> "" Then
                txtBSP.Text = dtrreader("billSubmissionPerson")
            Else
                txtBSP.Text = ""
            End If
            If dtrreader("billSubmissionPersonPhone").ToString() <> "" Then
                txtBSPhone.Text = dtrreader("billSubmissionPersonPhone")
            Else
                txtBSPhone.Text = ""
            End If
            If dtrreader("billSubmissionPersonEmail").ToString() <> "" Then
                txtBSPEmail.Text = dtrreader("billSubmissionPersonEmail")
            Else
                txtBSPEmail.Text = ""
            End If
            If Not String.IsNullOrEmpty(dtrreader("CreditDays").ToString()) Then
                'If dtrreader("CreditDays").ToString() <> "" Or dtrreader("CreditDays").ToString() <> Null Then
                txtCreditDays.Text = dtrreader("CreditDays")
            Else
                txtCreditDays.Text = ""
            End If
            If IsDBNull(dtrreader("FuelSurchargesThreshold")) Then
                txtFuelSurchargesThreshold.Text = "0"
            Else
                txtFuelSurchargesThreshold.Text = dtrreader("FuelSurchargesThreshold")
            End If

            If IsDBNull(dtrreader("EBillingYN")) Then
                ddlEbillingYN.SelectedValue = ""
            Else
                If dtrreader("EBillingYN") = True Then
                    ddlEbillingYN.SelectedValue = 1
                Else
                    ddlEbillingYN.SelectedValue = 0
                End If
            End If

            If IsDBNull(dtrreader("AgreementType")) Then
                ddlAgreementType.SelectedValue = "Standard Agreement"
            Else
                ddlAgreementType.SelectedValue = dtrreader("AgreementType")
            End If

            If IsDBNull(dtrreader("ServiceTypeId")) Then
                ddlServiceType.SelectedValue = "1"
            Else
                ddlServiceType.SelectedValue = dtrreader("ServiceTypeId")
            End If

            txtCentralizeEffectiveDate.Text = dtrreader("CentralizedBillingEffectivedate") & ""
            If Not IsDBNull(dtrreader("CentralizedBilling")) Then
                chkBilling.Checked = dtrreader("CentralizedBilling")
            End If
            autoselec_ddl(ddlClientBilling, dtrreader("ClientBilling"))

            '******************************
            chkEdit.Checked = False
            chkEditSop.Checked = False
            dtrreader.Close()
            accessdata.Dispose()
            hyplnkretry3.NavigateUrl = "ClientCoEditAddressResult.aspx?ID=" & Request.QueryString("ID")
            hyplnkretry4.NavigateUrl = "ClientCoAddAddress.aspx?ID=" & Request.QueryString("ID")

        End If
    End Sub
    Sub populateddl()
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility

        ddlcompanytype.DataSource = objAcessdata.funcGetSQLDataReader("select CoTypeName ,CoTypeID  from CORIntCoTypeMaster   where active=1 order by CoTypeName")
        ddlcompanytype.DataValueField = "CoTypeID"
        ddlcompanytype.DataTextField = "CoTypeName"
        ddlcompanytype.DataBind()
        ddlcompanytype.Items.Insert(0, New ListItem("", ""))

        'ddlcity.DataSource = objAcessdata.funcGetSQLDataReader("select cityname,cityid from CORIntCityMaster  where providerId=" & Session("provider_id") & " order by cityname")
        ddlcity.DataSource = objAcessdata.funcGetSQLDataReader("select cityname,cityid from CORIntCityMaster order by cityname")
        ddlcity.DataValueField = "cityid"
        ddlcity.DataTextField = "cityname"
        ddlcity.DataBind()
        ddlcity.Items.Insert(0, New ListItem("", ""))

        ddlcityBillTo.DataSource = objAcessdata.funcGetSQLDataReader("select cityname,cityid from CORIntCityMaster order by cityname")
        ddlcityBillTo.DataValueField = "cityid"
        ddlcityBillTo.DataTextField = "cityname"
        ddlcityBillTo.DataBind()
        ddlcityBillTo.Items.Insert(0, New ListItem("", ""))

        'ddlsourceCity.DataSource = objAcessdata.funcGetSQLDataReader("select cityname,cityid from CORIntCityMaster where providerId=" & Session("provider_id") & "  order by cityname")
        ddlsourceCity.DataSource = objAcessdata.funcGetSQLDataReader("select cityname,cityid from CORIntCityMaster order by cityname")
        ddlsourceCity.DataValueField = "cityid"
        ddlsourceCity.DataTextField = "cityname"
        ddlsourceCity.DataBind()
        ddlsourceCity.Items.Insert(0, New ListItem("", ""))

        'ddlairport.DataSource = objAcessdata.funcGetSQLDataReader("select UnitName ,UnitID  from CORIntUnitMaster   where (Operates24_7YN = 1 or AirportYN = 1 or MainCityUnitYN = 1)   order by UnitName")
        'ddlairport.DataValueField = "UnitID"
        'ddlairport.DataTextField = "UnitName"
        'ddlairport.DataBind()
        'ddlairport.Items.Insert(0, New ListItem("", ""))

        'ddlhotel.DataSource = objAcessdata.funcGetSQLDataReader("select UnitName ,UnitID  from CORIntUnitMaster   where (Operates24_7YN = 1 or AirportYN = 1 or MainCityUnitYN = 1)   order by UnitName")
        'ddlhotel.DataValueField = "UnitID"
        'ddlhotel.DataTextField = "UnitName"
        'ddlhotel.DataBind()
        'ddlhotel.Items.Insert(0, New ListItem("", ""))

        'ddlcorporate.DataSource = objAcessdata.funcGetSQLDataReader("select UnitName ,UnitID  from CORIntUnitMaster   where (Operates24_7YN = 1 or AirportYN = 1 or MainCityUnitYN = 1)   order  by UnitName")
        'ddlcorporate.DataValueField = "UnitID"
        'ddlcorporate.DataTextField = "UnitName"
        'ddlcorporate.DataBind()
        'ddlcorporate.Items.Insert(0, New ListItem("", ""))

        'ddlselfdrive.DataSource = objAcessdata.funcGetSQLDataReader("select UnitName ,UnitID  from CORIntUnitMaster   where (Operates24_7YN = 1 or AirportYN = 1 or MainCityUnitYN = 1)  order by UnitName")
        'ddlselfdrive.DataValueField = "UnitID"
        'ddlselfdrive.DataTextField = "UnitName"
        'ddlselfdrive.DataBind()
        'ddlselfdrive.Items.Insert(0, New ListItem("", ""))

        'ddlRelationshpMgr.DataSource = objAcessdata.funcGetSQLDataReader("select sysuserId, Fname + ' ' + Lname as Name from corintsysusersmaster where Active = 1 and providerId=" & Session("provider_id") & "  order by Fname")
        ddlRelationshpMgr.DataSource = objAcessdata.funcGetSQLDataReader("select sysuserId, Fname + ' ' + Lname as Name from corintsysusersmaster where Active = 1 order by Fname")
        ddlRelationshpMgr.DataValueField = "sysuserId"
        ddlRelationshpMgr.DataTextField = "Name"
        ddlRelationshpMgr.DataBind()
        ddlRelationshpMgr.Items.Insert(0, New ListItem("", ""))


        'ddlGroupEntity.DataSource = objAcessdata.funcGetSQLDataReader("select distinct GroupEntity  from CORIntClientCoMaster where active=1 and providerId=" & Session("provider_id") & "  order by GroupEntity")
        ddlGroupEntity.DataSource = objAcessdata.funcGetSQLDataReader("select distinct GroupEntity  from CORIntClientCoMaster where active=1 order by GroupEntity")
        ddlGroupEntity.DataTextField = "GroupEntity"
        ddlGroupEntity.DataBind()
        ddlGroupEntity.Items.Insert(0, New ListItem("", ""))

        ddlServiceType.DataSource = objAcessdata.funcGetSQLDataReader("select ServiceTypeId,ServiceName  from CorintServiceTypeMaster where Active=1")
        ddlServiceType.DataValueField = "ServiceTypeId"
        ddlServiceType.DataTextField = "ServiceName"
        ddlServiceType.DataBind()


        objAcessdata.funcpopulatenumddw(100, 0, ddldiscount)
        objAcessdata.funcpopulatenumddw(9, 0, ddldiscountdecimal)
        ddldiscount.Enabled = False
        ddldiscountdecimal.Enabled = False
        'Dim intcounter As Int32
        'For intcounter = 0 To 100
        ' ddldiscount.Items.Insert(intcounter, New ListItem(intcounter, intcounter))
        'Next
        objAcessdata.Dispose()

    End Sub
    Function autoselec_ddl(ByVal ddlname As DropDownList, ByVal selectvalue As Int32)
        If Not IsDBNull(selectvalue) Then
            ' If Not selectvalue = 0 Then
            Try
                ddlname.Items.FindByValue(selectvalue).Selected = True
            Catch ex As Exception

            End Try


            'End If
        End If
    End Function
    Function autoselec_ddl(ByVal ddlname As DropDownList, ByVal selectvalue As String)
        If Not IsDBNull(selectvalue) Then
            ' If Not selectvalue = 0 Then
            Try
                ddlname.Items.FindByText(selectvalue).Selected = True
            Catch ex As Exception

            End Try


            'End If
        End If
    End Function

    ' Code Added by Binary Semantics on 24-April-07
    Function IsUserNameExist(ByVal userName As String) As Boolean
        Dim userNameCheck As Boolean
        userNameCheck = False
        Dim dtrreader As SqlDataReader
        Dim accessdata As clsutility
        accessdata = New clsutility
        'dtrreader = accessdata.funcGetSQLDataReader("SELECT Count(LoginID) as UserName FROM CORIntClientCoMaster WHERE LoginID =  '" & userName & "' and ClientCoID <> " & Request.QueryString("id"))
        'dtrreader = accessdata.funcGetSQLDataReader("SELECT Count(LoginID) as UserName FROM CORIntClientCoMaster_17Jan09 WHERE LoginID =  '" & userName & "' and ClientCoID <> " & Request.QueryString("id"))
        dtrreader = accessdata.funcGetSQLDataReader("SELECT Count(LoginID) as UserName FROM CORIntClientCoMaster WHERE LoginID =  '" & userName & "' and ClientCoID <> " & Request.QueryString("id"))
        dtrreader.Read()

        If dtrreader("UserName") > 0 Then
            userNameCheck = True
        End If

        dtrreader.Close()
        accessdata.Dispose()

        Return userNameCheck
    End Function

    Private Sub btnsubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsubmit.Click

        '**********************************************************************
        Dim str As String
        Dim SOPStr As String
        'Response.Write(FileUpload1.PostedFile)
        'Response.End()
        'If txtFileName.Text = "No Image" Then
        '    Dim filename As String
        '    Dim SOPfilename As String
        '    Dim camFileName As String
        '    filename = Path.GetFileName(FileUpload1.FileName)
        '    str = filename
        '    'SOPfilename = Path.GetFileName(SopFileUpload.FileName)
        '    'SOPStr = SOPfilename
        '    'SOPStr = Request.QueryString("id").ToString() + "_SOP_" + SOPStr
        '    SOPStr = txtSopFile.Text
        '    'If (SopFileUpload.PostedFile.FileName <> "" And SopFileUpload.PostedFile.ContentLength > 0) Or get_YNvalue(chkactive) = 0 Then
        '    If str <> "" Or get_YNvalue(chkactive) = 0 Then
        '        If (FileUpload1.PostedFile.FileName <> "" And FileUpload1.PostedFile.ContentLength > 0) Or get_YNvalue(chkactive) = 0 Then
        '            If get_YNvalue(chkactive) = 1 Then
        '                FileUpload1.SaveAs("D:\Upload\ClientContract\" + filename)
        '                'SopFileUpload.SaveAs("F:\Upload\ClientBilling_SOP\" + SOPfilename)
        '            End If

        '            If str <> "" Or get_YNvalue(chkactive) = 0 Then
        '                Dim checkUserName As Boolean
        '                checkUserName = IsUserNameExist(txtUserName.Text)

        '                If Not checkUserName Then
        '                    Dim MyConnection As SqlConnection
        '                    MyConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("corConnectString"))
        '                    Dim cmd As SqlCommand
        '                    Dim intuniqcheck As SqlParameter
        '                    Dim intuniqvalue As Int32

        '                    Dim disdiscount As String
        '                    disdiscount = ddldiscount.SelectedItem.Value + "." + ddldiscountdecimal.SelectedItem.Value
        '                    'cmd = New SqlCommand("procEditClientcarmaster_New", MyConnection)
        '                    cmd = New SqlCommand("ProcEditClientcarmaster", MyConnection)
        '                    cmd.CommandType = CommandType.StoredProcedure
        '                    cmd.Parameters.AddWithValue("@rowid", Request.QueryString("id"))
        '                    cmd.Parameters.AddWithValue("@clientconame", txtcompanyName.Text)
        '                    cmd.Parameters.AddWithValue("@clientcocityid", ddlcity.SelectedItem.Value)
        '                    cmd.Parameters.AddWithValue("@clientcocityidBillTo", ddlcityBillTo.SelectedItem.Value) 'added by Rahul on 27-May-2011
        '                    cmd.Parameters.AddWithValue("@clientSourceCity", ddlsourceCity.SelectedItem.Value) 'added by Rahul on 21-Mar-2016
        '                    cmd.Parameters.AddWithValue("@contact1fname", txtfname1.Text)
        '                    cmd.Parameters.AddWithValue("@contact1mname", "")
        '                    cmd.Parameters.AddWithValue("@contact1lname", "")
        '                    cmd.Parameters.AddWithValue("@contact1ph", txtphone1.Text)
        '                    cmd.Parameters.AddWithValue("@contact1desig", "")
        '                    cmd.Parameters.AddWithValue("@ContactEmail1", txtEmailAH.Text)
        '                    cmd.Parameters.AddWithValue("@contact2fname", txtfname2.Text)
        '                    cmd.Parameters.AddWithValue("@contact2mname", "")
        '                    cmd.Parameters.AddWithValue("@contact2lname", "")
        '                    cmd.Parameters.AddWithValue("@contact2ph", txtphone2.Text)
        '                    cmd.Parameters.AddWithValue("@contact2desig", "")
        '                    cmd.Parameters.AddWithValue("@ContactEmail2", txtEmailFM.Text)
        '                    cmd.Parameters.AddWithValue("@contact3fname", txtfname3.Text)
        '                    cmd.Parameters.AddWithValue("@contact3mname", "")
        '                    cmd.Parameters.AddWithValue("@contact3lname", "")
        '                    cmd.Parameters.AddWithValue("@contact3ph", txtphone3.Text)
        '                    cmd.Parameters.AddWithValue("@contact3desig", "")
        '                    cmd.Parameters.AddWithValue("@ContactEmail3", txtEmailCFO.Text)
        '                    cmd.Parameters.AddWithValue("@fax", txtfax.Text)
        '                    cmd.Parameters.AddWithValue("@emailid", txtemail.Text)
        '                    cmd.Parameters.AddWithValue("@bookingmode", txtbookmode.SelectedItem.Value)
        '                    cmd.Parameters.AddWithValue("@loginName", txtUserName.Text)
        '                    cmd.Parameters.AddWithValue("@password", txtPassword.Text)

        '                    cmd.Parameters.AddWithValue("@cotypeid", ddlcompanytype.SelectedItem.Value)
        '                    cmd.Parameters.AddWithValue("@discountpc", disdiscount)

        '                    cmd.Parameters.AddWithValue("@paymentterms", ddlpayment.SelectedItem.Value)
        '                    If Not txtcclimit.Text = "" Then
        '                        cmd.Parameters.AddWithValue("@creditlimit", txtcclimit.Text)
        '                    Else
        '                        cmd.Parameters.AddWithValue("@creditlimit", Null)
        '                    End If
        '                    cmd.Parameters.AddWithValue("@billingbasis", ddlbillingbasis.SelectedItem.Value)
        '                    cmd.Parameters.AddWithValue("@servicetaxno", tstsvctax.Text)
        '                    cmd.Parameters.AddWithValue("@airportunitid", ddlairport.SelectedItem.Value)
        '                    cmd.Parameters.AddWithValue("@hotelunitid", ddlhotel.SelectedItem.Value)
        '                    cmd.Parameters.AddWithValue("@corpunitid", ddlcorporate.SelectedItem.Value)
        '                    cmd.Parameters.AddWithValue("@sdunitid", ddlselfdrive.SelectedItem.Value)
        '                    cmd.Parameters.AddWithValue("@TariffType", ddTariffType.SelectedItem.Value)
        '                    cmd.Parameters.AddWithValue("@CreditType", ddCreditType.SelectedItem.Value)
        '                    cmd.Parameters.AddWithValue("@btayn", get_YNvalue(chkbta))
        '                    'cmd.Parameters.AddWithValue("@amexcardno", "")
        '                    cmd.Parameters.AddWithValue("@remarks", txtRemarks.Text)
        '                    cmd.Parameters.AddWithValue("@DetailsRequired", txtdetail.Text)
        '                    cmd.Parameters.AddWithValue("@active", get_YNvalue(chkactive))
        '                    cmd.Parameters.AddWithValue("@modifiedby", Session("loggedin_user"))
        '                    cmd.Parameters.AddWithValue("@DiplayDiscInvYN", get_YNvalue(chkDispDiscInv))
        '                    cmd.Parameters.AddWithValue("@EmailReqYN", get_YNvalue(chkEmail))
        '                    cmd.Parameters.AddWithValue("@ApplyServiceTaxYN", get_YNvalue(chkApplyServiceTaxYN))
        '                    'cmd.Parameters.AddWithValue("@ApplyEduCessYN", get_YNvalue(chkApplyEduCessYN))
        '                    'cmd.Parameters.AddWithValue("@ApplyHduCessYN", get_YNvalue(chkApplyHduCessYN))
        '                    cmd.Parameters.AddWithValue("@SEZClientYN", get_YNvalue(chkSEZClientYN))
        '                    cmd.Parameters.AddWithValue("@SEZExpiry", txtSEZExpiry.Text)
        '                    cmd.Parameters.AddWithValue("@ApplyDSTYN", get_YNvalue(chkApplyDSTYN))
        '                    cmd.Parameters.AddWithValue("@ApplyCCYN", get_YNvalue(chkApplyCCYN))
        '                    'added by rahul on 13 May 2010
        '                    cmd.Parameters.AddWithValue("@ApplyCreditMailYN", get_YNvalue(chkCreditMailYN))
        '                    cmd.Parameters.AddWithValue("@ContractDate", txtContractDate.Text)
        '                    cmd.Parameters.AddWithValue("@ContractStartDate", txtContractStartDate.Text)
        '                    cmd.Parameters.AddWithValue("@ContractEndDate", txtContractEndDate.Text)
        '                    cmd.Parameters.AddWithValue("@FuelSurchargeYN", get_YNvalue(chkFuelSurchargeYN))

        '                    cmd.Parameters.AddWithValue("@clientPOC", txtclientPoc.Text)  'Add at 20Jan2016


        '                    If chk_Entity.Checked = True Then
        '                        cmd.Parameters.AddWithValue("@GroupEntity", txtcompanyName.Text.Trim())
        '                    Else
        '                        cmd.Parameters.AddWithValue("@GroupEntity", ddlGroupEntity.SelectedItem.Text)
        '                    End If

        '                    intuniqcheck = cmd.Parameters.AddWithValue("@uniqcheckval", SqlDbType.Int)

        '                    'Added On ****5/7/2007*******************

        '                    cmd.Parameters.AddWithValue("@BFrom", txtFrom.Text)
        '                    cmd.Parameters.AddWithValue("@BTo", txtTo.Text)
        '                    cmd.Parameters.AddWithValue("@Bs", txtSubDays.Text)
        '                    Dim activeStatus As Integer
        '                    If rdStatus1.Checked = True Then
        '                        activeStatus = 0
        '                        'txtActiveDate.Text = ""
        '                    Else
        '                        activeStatus = 1
        '                        'txtActiveDate.Text = ""
        '                    End If
        '                    cmd.Parameters.AddWithValue("@Ast", activeStatus)
        '                    cmd.Parameters.AddWithValue("@Ad", txtActiveDate.Text)

        '                    'added by rahul on 13 May 2010
        '                    cmd.Parameters.AddWithValue("@CreditLimitSender", txtCreditMailLimit.Text)
        '                    cmd.Parameters.AddWithValue("@WarningLimit", txtwarningLimit.Text)
        '                    cmd.Parameters.AddWithValue("@CircuitBreaker", txtCircuitbreaker.Text)
        '                    cmd.Parameters.AddWithValue("@EmailRequired", get_YNvalue(chkEmailRequiredYN))
        '                    cmd.Parameters.AddWithValue("@VRFRequired", get_YNvalue(chkVRFRequiredYN))
        '                    '*************
        '                    'cmd.Parameters.AddWithValue("@PaymentCP", txtPaymentCP.Text)
        '                    cmd.Parameters.AddWithValue("@Panno", txtPanno.Text)
        '                    cmd.Parameters.AddWithValue("@Tanno", txtTanno.Text)
        '                    If (Not String.IsNullOrEmpty(ddlRelationshpMgr.SelectedValue)) Then
        '                        cmd.Parameters.AddWithValue("@RelationshipMngr", ddlRelationshpMgr.SelectedValue)
        '                    Else
        '                        cmd.Parameters.AddWithValue("@RelationshipMngr", Null)
        '                    End If

        '                    If (Not String.IsNullOrEmpty(txtAvgMonRev.Text)) Then
        '                        cmd.Parameters.AddWithValue("@AvgMonRev", txtAvgMonRev.Text)
        '                    Else
        '                        cmd.Parameters.AddWithValue("@AvgMonRev", Null)
        '                    End If

        '                    cmd.Parameters.AddWithValue("@FileName", str)
        '                    cmd.Parameters.AddWithValue("@spInstructions", txtSpecialInstructions.Text)
        '                    cmd.Parameters.AddWithValue("@ApplyHigherExtraAmt", get_YNvalue(chkHigherExtraAmt))
        '                    cmd.Parameters.AddWithValue("@PrintOnDsYN", get_YNvalue(chkPrintDS))
        '                    'code add by alka on 03 oct 2012 for adding extra details at time of client creation

        '                    cmd.Parameters.AddWithValue("@BillSubPer", txtBSP.Text)
        '                    cmd.Parameters.AddWithValue("@BillSubPerPhone", txtBSPhone.Text)
        '                    cmd.Parameters.AddWithValue("@BillSubPerEmail", txtBSPEmail.Text)
        '                    If Not txtCreditDays.Text = "" Then
        '                        cmd.Parameters.AddWithValue("@CreditDays", Convert.ToInt32(txtCreditDays.Text))
        '                    Else
        '                        cmd.Parameters.AddWithValue("@CreditDays", txtCreditDays.Text)
        '                    End If
        '                    cmd.Parameters.AddWithValue("@SOPFileName", Request.QueryString("id") + "_SOP_" + SOPStr)

        '                    If String.IsNullOrEmpty(txtFuelSurchargesThreshold.Text) Then
        '                        cmd.Parameters.AddWithValue("@FuelSurchargesThreshold", Null)
        '                    Else
        '                        cmd.Parameters.AddWithValue("@FuelSurchargesThreshold", Convert.ToDecimal(txtFuelSurchargesThreshold.Text.ToString()))
        '                    End If
        '                    cmd.Parameters.AddWithValue("@NegativeFuelSurchargeYN", get_YNvalue(chkFuelSurchargeYNNegative))

        '                    cmd.Parameters.AddWithValue("@EBillingYN", ddlEbillingYN.SelectedValue)

        '                    cmd.Parameters.AddWithValue("@AgreementType", ddlAgreementType.SelectedValue)
        '                    cmd.Parameters.AddWithValue("@SendInvoiceOnClosureYN", get_YNvalue(chkInvoiceMailonClosureYN))

        '                    '*********************************************************************************
        '                    intuniqcheck.Direction = ParameterDirection.Output

        '                    '     Try
        '                    MyConnection.Open()
        '                    cmd.ExecuteNonQuery()
        '                    intuniqvalue = cmd.Parameters("@uniqcheckval").Value
        '                    MyConnection.Close()

        '                    If intuniqvalue = 1 Then
        '                        lblErrorMsg.Visible = True
        '                        lblErrorMsg.Text = "Client company already exist."
        '                        Exit Sub
        '                    ElseIf intuniqvalue = 0 Then
        '                        lblErrorMsg.Visible = False
        '                        pnlmainform.Visible = False
        '                        pnlconfirmation.Visible = True

        '                        lblMessage.Text = "You have updated the Client company successfully"
        '                        hyplnkretry.Text = "Edit another Client company"
        '                        hyplnkretry.NavigateUrl = "ClientCoEditSearch.aspx"
        '                    ElseIf intuniqvalue = 2 Then
        '                        lblErrorMsg.Visible = True
        '                        lblErrorMsg.Text = "Image " & str & " already Uploaded."
        '                        Exit Sub
        '                    Else
        '                        lblErrorMsg.Visible = False
        '                        pnlmainform.Visible = False
        '                        pnlconfirmation.Visible = True

        '                        lblMessage.Text = "You have updated the Client Company successfully"
        '                        hyplnkretry.Text = "Edit another Client Company"
        '                        hyplnkretry.NavigateUrl = "ClientCoEditSearch.aspx"
        '                        If ddTariffType.SelectedItem.Value = "S" Then
        '                            hyplnkretry2.Text = "<b>PLEASE SELECT THE SPECIAL TARIFFS FOR THIS CLIENT</b>"
        '                            hyplnkretry2.NavigateUrl = "PkgsAssignForm.aspx?ID=" & Request.QueryString("id")
        '                        End If
        '                    End If
        '                    '   Catch
        '                    '      lblerr.Visible = True
        '                    '       lblerr.Text = "Login ID already exist."
        '                    '  End Try
        '                Else
        '                    ' UserName exist, display message
        '                    lblErrorMsg.Visible = True
        '                    lblErrorMsg.Text = "User Name already exists."
        '                End If
        '                End If
        '        Else
        '            lblErrorMsg.Visible = True
        '            lblErrorMsg.Text = "Please select Image to Upload."
        '            Exit Sub
        '        End If
        '    Else
        '        lblErrorMsg.Visible = True
        '        lblErrorMsg.Text = "Please select Image to Upload."
        '        Exit Sub
        '    End If
        '    'Else
        '    '    lblErrorMsg.Visible = True
        '    '    lblErrorMsg.Text = "Please select SOP Image to Upload."
        '    '    Exit Sub
        '    'End If
        'Else
        Try

            Dim filename As String
            Dim SOPfilename As String
            Dim camFileName As String
            If chkEdit.Checked = True Then
                filename = Path.GetFileName(FileUpload1.FileName)
                str = System.DateTime.Now.ToString("dd-MM-yyyy HH-mm-ss") + "_" + filename
                str = Request.QueryString("id").ToString() + "_Agreement_" + str
                If (FileUpload1.PostedFile.FileName <> "" And FileUpload1.PostedFile.ContentLength > 0) Or get_YNvalue(chkactive) = 0 Then
                    If get_YNvalue(chkactive) = 1 Then
                        FileUpload1.SaveAs("F:\Upload\ClientContract\" + str)
                    End If
                Else
                    lblErrorMsg.Visible = True
                    lblErrorMsg.Text = "Please select Image to Upload."
                    Exit Sub
                End If

            Else
                str = txtFileName.Text
            End If

            If chkEditSop.Checked = True Then
                SOPfilename = Path.GetFileName(SopFileUpload.FileName)
                SOPStr = System.DateTime.Now.ToString("dd-MM-yyyy HH-mm-ss") + "_" + SOPfilename
                SOPStr = Request.QueryString("id").ToString() + "_SOP_" + SOPStr
                If (SopFileUpload.PostedFile.FileName <> "" And SopFileUpload.PostedFile.ContentLength > 0) Or get_YNvalue(chkactive) = 0 Then
                    If get_YNvalue(chkactive) = 1 Then
                        SopFileUpload.SaveAs("F:\Upload\ClientBilling_SOP\" + SOPStr)
                    End If
                Else
                    lblErrorMsg.Visible = True
                    lblErrorMsg.Text = "Please select SOP Image to Upload."
                    Exit Sub
                End If

            Else
                SOPStr = txtSopFile.Text
            End If

            'CAM File Edit

            If chkCAM.Checked = True Then
                camFileName = Path.GetFileName(fluCam.FileName)
                camFileName = System.DateTime.Now.ToString("dd-MM-yyyy HH-mm-ss") + "_" + camFileName
                camFileName = Request.QueryString("id").ToString() + "_CAM_" + camFileName
                If (fluCam.PostedFile.FileName <> "" And fluCam.PostedFile.ContentLength > 0) Or get_YNvalue(chkactive) = 0 Then
                    If get_YNvalue(chkactive) = 1 Then
                        fluCam.SaveAs("F:\Upload\ClientBilling_CAM\" + camFileName)
                    End If
                Else
                    lblErrorMsg.Visible = True
                    lblErrorMsg.Text = "Please select CAM Image to Upload."
                    Exit Sub
                End If

            Else
                camFileName = txtCamFileName.Text
            End If


            'End CAM File Edit


            Dim checkUserName As Boolean
            checkUserName = IsUserNameExist(txtUserName.Text)

            If Not checkUserName Then
                ' UserName not exist, insert the record

                Dim MyConnection As SqlConnection
                MyConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("corConnectString"))
                Dim cmd As SqlCommand
                Dim intuniqcheck As SqlParameter
                Dim intuniqvalue As Int32

                Dim disdiscount As String
                disdiscount = ddldiscount.SelectedItem.Value + "." + ddldiscountdecimal.SelectedItem.Value

                'cmd = New SqlCommand("ProcEditClientcarmaster_PersonalTrip", MyConnection)
                'cmd = New SqlCommand("procEditClientcarmaster_OdometerMask", MyConnection)
                cmd = New SqlCommand("procEditClientcarmaster_ClientBilling", MyConnection)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@rowid", Request.QueryString("id"))
                cmd.Parameters.AddWithValue("@clientconame", txtcompanyName.Text)
                cmd.Parameters.AddWithValue("@clientcocityid", ddlcity.SelectedItem.Value)
                cmd.Parameters.AddWithValue("@clientcocityidBillTo", ddlcityBillTo.SelectedItem.Value) 'added by Rahul on 27-May-2011
                cmd.Parameters.AddWithValue("@clientSourceCity", ddlsourceCity.SelectedItem.Value) 'added by Rahul on 21-Mar-2016
                cmd.Parameters.AddWithValue("@contact1fname", txtfname1.Text)
                cmd.Parameters.AddWithValue("@contact1mname", "")
                cmd.Parameters.AddWithValue("@contact1lname", "")
                cmd.Parameters.AddWithValue("@contact1ph", txtphone1.Text)
                cmd.Parameters.AddWithValue("@contact1desig", "")
                cmd.Parameters.AddWithValue("@ContactEmail1", txtEmailAH.Text)
                cmd.Parameters.AddWithValue("@contact2fname", txtfname2.Text)
                cmd.Parameters.AddWithValue("@contact2mname", "")
                cmd.Parameters.AddWithValue("@contact2lname", "")
                cmd.Parameters.AddWithValue("@contact2ph", txtphone2.Text)
                cmd.Parameters.AddWithValue("@contact2desig", "")
                cmd.Parameters.AddWithValue("@ContactEmail2", txtEmailFM.Text)
                cmd.Parameters.AddWithValue("@contact3fname", txtfname3.Text)
                cmd.Parameters.AddWithValue("@contact3mname", "")
                cmd.Parameters.AddWithValue("@contact3lname", "")
                cmd.Parameters.AddWithValue("@contact3ph", txtphone3.Text)
                cmd.Parameters.AddWithValue("@contact3desig", "")
                cmd.Parameters.AddWithValue("@ContactEmail3", txtEmailCFO.Text)
                'cmd.Parameters.AddWithValue("@fax", txtfax.Text)
                cmd.Parameters.AddWithValue("@emailid", txtemail.Text)
                'cmd.Parameters.AddWithValue("@bookingmode", txtbookmode.Text)
                cmd.Parameters.AddWithValue("@bookingmode", txtbookmode.SelectedItem.Value)

                ' Parameter added by BSL on 24-April-07, for storing user name and password
                cmd.Parameters.AddWithValue("@loginName", txtUserName.Text)
                cmd.Parameters.AddWithValue("@password", txtPassword.Text)

                cmd.Parameters.AddWithValue("@cotypeid", ddlcompanytype.SelectedItem.Value)
                cmd.Parameters.AddWithValue("@discountpc", disdiscount)

                cmd.Parameters.AddWithValue("@paymentterms", ddlpayment.SelectedItem.Value)
                If Not txtcclimit.Text = "" Then
                    cmd.Parameters.AddWithValue("@creditlimit", txtcclimit.Text)
                Else
                    cmd.Parameters.AddWithValue("@creditlimit", Null)
                End If
                cmd.Parameters.AddWithValue("@billingbasis", ddlbillingbasis.SelectedItem.Value)
                'cmd.Parameters.AddWithValue("@servicetaxno", tstsvctax.Text)
                'cmd.Parameters.AddWithValue("@airportunitid", Null) ' ddlairport.SelectedItem.Value)
                'cmd.Parameters.AddWithValue("@hotelunitid", Null) ' ddlhotel.SelectedItem.Value)
                'cmd.Parameters.AddWithValue("@corpunitid", Null) ' ddlcorporate.SelectedItem.Value)
                'cmd.Parameters.AddWithValue("@sdunitid", Null) ' ddlselfdrive.SelectedItem.Value)

                'cmd.Parameters.AddWithValue("@airportunitid", ddlairport.SelectedItem.Value)
                'cmd.Parameters.AddWithValue("@hotelunitid", ddlhotel.SelectedItem.Value)
                'cmd.Parameters.AddWithValue("@corpunitid", ddlcorporate.SelectedItem.Value)
                'cmd.Parameters.AddWithValue("@sdunitid", ddlselfdrive.SelectedItem.Value)

                cmd.Parameters.AddWithValue("@TariffType", "S") 'ddTariffType.SelectedItem.Value)
                cmd.Parameters.AddWithValue("@CreditType", ddCreditType.SelectedItem.Value)
                cmd.Parameters.AddWithValue("@btayn", get_YNvalue(chkbta))
                'cmd.Parameters.AddWithValue("@amexcardno", "")
                cmd.Parameters.AddWithValue("@remarks", txtRemarks.Text)
                cmd.Parameters.AddWithValue("@DetailsRequired", txtdetail.Text)
                cmd.Parameters.AddWithValue("@active", get_YNvalue(chkactive))
                cmd.Parameters.AddWithValue("@enablePersonalTrip", get_YNvalue(enablePersonalTrip))
                cmd.Parameters.AddWithValue("@enableOdometerMask", get_YNvalue(enableOdometerMask))
                cmd.Parameters.AddWithValue("@odometerMaskDate", odometerMaskDate.Text)
                cmd.Parameters.AddWithValue("@modifiedby", Session("loggedin_user"))
                cmd.Parameters.AddWithValue("@DiplayDiscInvYN", get_YNvalue(chkDispDiscInv))
                cmd.Parameters.AddWithValue("@EmailReqYN", get_YNvalue(chkEmail))
                cmd.Parameters.AddWithValue("@ApplyServiceTaxYN", get_YNvalue(chkApplyServiceTaxYN))
                'cmd.Parameters.AddWithValue("@ApplyEduCessYN", get_YNvalue(chkApplyEduCessYN))
                'cmd.Parameters.AddWithValue("@ApplyHduCessYN", get_YNvalue(chkApplyHduCessYN))
                cmd.Parameters.AddWithValue("@SEZClientYN", get_YNvalue(chkSEZClientYN))
                cmd.Parameters.AddWithValue("@SEZExpiry", txtSEZExpiry.Text)
                cmd.Parameters.AddWithValue("@ApplyDSTYN", False) 'get_YNvalue(chkApplyDSTYN))
                cmd.Parameters.AddWithValue("@ApplyCCYN", get_YNvalue(chkApplyCCYN))
                'added by rahul on 13 May 2010
                cmd.Parameters.AddWithValue("@ApplyCreditMailYN", get_YNvalue(chkCreditMailYN))
                cmd.Parameters.AddWithValue("@ContractDate", txtContractDate.Text)
                cmd.Parameters.AddWithValue("@ContractStartDate", txtContractStartDate.Text)
                cmd.Parameters.AddWithValue("@ContractEndDate", txtContractEndDate.Text)
                cmd.Parameters.AddWithValue("@FuelSurchargeYN", get_YNvalue(chkFuelSurchargeYN))

                cmd.Parameters.AddWithValue("@clientPOC", txtclientPoc.Text)
                intuniqcheck = cmd.Parameters.AddWithValue("@uniqcheckval", SqlDbType.Int)

                'Added On ****5/7/2007*******************

                cmd.Parameters.AddWithValue("@BFrom", txtFrom.Text)
                cmd.Parameters.AddWithValue("@BTo", txtTo.Text)
                cmd.Parameters.AddWithValue("@Bs", txtSubDays.Text)
                Dim activeStatus As Integer
                If rdStatus1.Checked = True Then
                    activeStatus = 0
                    'txtActiveDate.Text = ""
                Else
                    activeStatus = 1
                    'txtActiveDate.Text = ""
                End If
                cmd.Parameters.AddWithValue("@Ast", activeStatus)
                cmd.Parameters.AddWithValue("@Ad", txtActiveDate.Text)

                'added by rahul on 13 May 2010
                cmd.Parameters.AddWithValue("@CreditLimitSender", txtCreditMailLimit.Text)
                cmd.Parameters.AddWithValue("@WarningLimit", txtwarningLimit.Text)
                cmd.Parameters.AddWithValue("@CircuitBreaker", txtCircuitbreaker.Text)
                cmd.Parameters.AddWithValue("@EmailRequired", get_YNvalue(chkEmailRequiredYN))
                cmd.Parameters.AddWithValue("@VRFRequired", get_YNvalue(chkVRFRequiredYN))
                '*************
                'cmd.Parameters.AddWithValue("@PaymentCP", txtPaymentCP.Text)
                cmd.Parameters.AddWithValue("@Panno", txtPanno.Text)
                cmd.Parameters.AddWithValue("@Tanno", txtTanno.Text)
                If (Not String.IsNullOrEmpty(ddlRelationshpMgr.SelectedValue)) Then
                    cmd.Parameters.AddWithValue("@RelationshipMngr", ddlRelationshpMgr.SelectedValue)
                Else
                    cmd.Parameters.AddWithValue("@RelationshipMngr", Null)
                End If

                If (Not String.IsNullOrEmpty(txtAvgMonRev.Text)) Then
                    cmd.Parameters.AddWithValue("@AvgMonRev", txtAvgMonRev.Text)
                Else
                    cmd.Parameters.AddWithValue("@AvgMonRev", Null)
                End If


                cmd.Parameters.AddWithValue("@FileName", str)
                cmd.Parameters.AddWithValue("@spInstructions", txtSpecialInstructions.Text)
                cmd.Parameters.AddWithValue("@ApplyHigherExtraAmt", get_YNvalue(chkHigherExtraAmt))
                'cmd.Parameters.AddWithValue("@PrintOnDsYN", get_YNvalue(chkPrintDS))
                'code add by alka on 03 oct 2012 for adding extra details at time of client creation

                cmd.Parameters.AddWithValue("@BillSubPer", txtBSP.Text)
                cmd.Parameters.AddWithValue("@BillSubPerPhone", txtBSPhone.Text)
                cmd.Parameters.AddWithValue("@BillSubPerEmail", txtBSPEmail.Text)
                cmd.Parameters.AddWithValue("@CreditDays", Convert.ToInt32(txtCreditDays.Text))
                If chk_Entity.Checked = True Then
                    cmd.Parameters.AddWithValue("@GroupEntity", txtcompanyName.Text.Trim())
                Else
                    cmd.Parameters.AddWithValue("@GroupEntity", ddlGroupEntity.SelectedItem.Text)
                End If
                cmd.Parameters.AddWithValue("@SOPFileName", SOPStr)

                If String.IsNullOrEmpty(txtFuelSurchargesThreshold.Text) Then
                    cmd.Parameters.AddWithValue("@FuelSurchargesThreshold", Null)
                Else
                    cmd.Parameters.AddWithValue("@FuelSurchargesThreshold", Convert.ToDecimal(txtFuelSurchargesThreshold.Text.ToString()))
                End If
                cmd.Parameters.AddWithValue("@NegativeFuelSurchargeYN", get_YNvalue(chkFuelSurchargeYNNegative))
                cmd.Parameters.AddWithValue("@EBillingYN", ddlEbillingYN.SelectedValue)
                cmd.Parameters.AddWithValue("@CAMFileName", camFileName)
                cmd.Parameters.AddWithValue("@ServiceTypeId", Convert.ToInt16(ddlServiceType.SelectedValue))
                cmd.Parameters.AddWithValue("@AgreementType", ddlAgreementType.SelectedValue)
                cmd.Parameters.AddWithValue("@SendInvoiceOnClosureYN", get_YNvalue(chkInvoiceMailonClosureYN))
                cmd.Parameters.AddWithValue("@ChkBilling", get_YNvalue(chkBilling))
                cmd.Parameters.AddWithValue("@ClientBillingType", ddlClientBilling.SelectedValue)
                cmd.Parameters.AddWithValue("@CentralizeEffectiveDate", txtCentralizeEffectiveDate.Text)
                '*********************************************************************************
                intuniqcheck.Direction = ParameterDirection.Output

                '     Try
                MyConnection.Open()
                cmd.ExecuteNonQuery()
                intuniqvalue = cmd.Parameters("@uniqcheckval").Value
                MyConnection.Close()

                If intuniqvalue = 1 Then
                    lblErrorMsg.Visible = True
                    lblErrorMsg.Text = "Client company already exist."
                    Exit Sub
                ElseIf intuniqvalue = 0 Then
                    lblErrorMsg.Visible = False
                    pnlmainform.Visible = False
                    pnlconfirmation.Visible = True

                    lblMessage.Text = "You have updated the Client company successfully"
                    hyplnkretry.Text = "Edit another Client company"
                    hyplnkretry.NavigateUrl = "ClientCoEditSearch.aspx"
                ElseIf intuniqvalue = 2 Then
                    lblErrorMsg.Visible = True
                    lblErrorMsg.Text = "Image " & str & " already Uploaded."
                    Exit Sub
                Else
                    lblErrorMsg.Visible = False
                    pnlmainform.Visible = False
                    pnlconfirmation.Visible = True

                    lblMessage.Text = "You have updated the Client Company successfully"
                    hyplnkretry.Text = "Edit another Client Company"
                    hyplnkretry.NavigateUrl = "ClientCoEditSearch.aspx"

                    'If ddTariffType.SelectedItem.Value = "S" Then
                    'hyplnkretry2.Text = "<b>PLEASE SELECT THE SPECIAL TARIFFS FOR THIS CLIENT</b>"
                    'hyplnkretry2.NavigateUrl = "PkgsAssignForm.aspx?ID=" & Request.QueryString("id")
                    'End If

                End If
                '   Catch
                '      lblerr.Visible = True
                '       lblerr.Text = "Login ID already exist."
                '  End Try
            Else
                ' UserName exist, display message
                lblErrorMsg.Visible = True
                lblErrorMsg.Text = "User Name already exists."
            End If
        Catch ex As Exception
            lblErrorMsg.Visible = True
            lblErrorMsg.Text = ex.Message
        End Try
        '    End If
        '*************************************************************************************************
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function
    Private Sub chkEdit_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkEdit.CheckedChanged
        If chkEdit.Checked = True Then
            txtFileName.Enabled = True
            FileUpload1.Visible = True
        ElseIf chkEdit.Checked = False Then
            txtFileName.Enabled = False
            FileUpload1.Visible = False
        End If

    End Sub

    Private Sub chkEditSop_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkEditSop.CheckedChanged
        If chkEditSop.Checked = True Then
            txtSopFile.Enabled = True
            SopFileUpload.Visible = True
        ElseIf chkEdit.Checked = False Then
            txtSopFile.Enabled = False
            SopFileUpload.Visible = False
        End If

    End Sub
    Private Sub chkCAM_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCAM.CheckedChanged
        If chkCAM.Checked = True Then
            txtCamFileName.Enabled = True
            fluCam.Visible = True
        ElseIf chkCAM.Checked = False Then
            txtCamFileName.Enabled = False
            fluCam.Visible = False
        End If

    End Sub

    Protected Sub lnkView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkView.Click
        If Not Request.QueryString("ID") Is Nothing Then
            Dim url As String = "../ViewUploadDocument.aspx?ClientId=" & Request.QueryString("ID") & "&fType=C"
            Dim script As String = "<script type=""text/javascript"">window.open('" & url.ToString & "','_blank');</script>"
            ClientScript.RegisterStartupScript(Me.GetType, "openWindow", script)
        End If

    End Sub
    Protected Sub lnkSOAPFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSOAPFile.Click
        If Not Request.QueryString("ID") Is Nothing Then
            Dim url As String = "../ViewUploadDocument.aspx?ClientId=" & Request.QueryString("ID") & "&fType=S"
            Dim script As String = "<script type=""text/javascript"">window.open('" & url.ToString & "','_blank');</script>"
            ClientScript.RegisterStartupScript(Me.GetType, "openWindow", script)
        End If

    End Sub

    Protected Sub lnkDownloads_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDownloads.Click
        If Not Request.QueryString("ID") Is Nothing Then
            Dim url As String = "../ViewUploadDocument.aspx?ClientId=" & Request.QueryString("ID") & "&fType=A"
            Dim script As String = "<script type=""text/javascript"">window.open('" & url.ToString & "','_blank');</script>"
            ClientScript.RegisterStartupScript(Me.GetType, "openWindow", script)
        End If

    End Sub

End Class
