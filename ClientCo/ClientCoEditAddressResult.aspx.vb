Imports commonutility
Imports System.text
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Imports System.Web.UI
Public Class ClientCoEditAddressResult
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tblRecDetail As System.Web.UI.WebControls.Table

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not Page.IsPostBack Then
            getvalue("Counter")
        End If
    End Sub
    Sub getvalue(ByVal strorderby As String)
        Dim strquery As StringBuilder
        Dim dtrreader As SqlDataReader
        Dim accessdata As clsutility
        accessdata = New clsutility
        'strquery = New StringBuilder("select ClientCoAddID, ClientCoID, Counter, ClientCoAddress from CORIntClientCoAddMaster where ClientCoID = " & Request.QueryString("ID") & " order by Counter")
        'strquery = New StringBuilder("select ClientCoAddID, ClientCoID, Counter, ClientCoAddress,clientcoBilladdress from CORIntClientCoAddMaster_17Jan09 where ClientCoID = " & Request.QueryString("ID") & " order by Counter")
        strquery = New StringBuilder("select ClientCoAddID, ClientCoID, Counter, ClientCoAddress,clientcoBilladdress from CORIntClientCoAddMaster where ClientCoID = " & Request.QueryString("ID") & " order by Counter")
        dtrreader = accessdata.funcGetSQLDataReader(strquery.ToString)
        While dtrreader.Read
            Dim Temprow As New TableRow
            Temprow.HorizontalAlign = HorizontalAlign.Center
            Dim Tempcell As New TableCell
            Tempcell.Controls.Add(New LiteralControl(dtrreader("Counter") & ""))
            Temprow.Cells.Add(Tempcell)

            Dim Tempcel2 As New TableCell
            Tempcel2.Controls.Add(New LiteralControl(dtrreader("ClientCoAddress") & ""))
            Temprow.Cells.Add(Tempcel2)

            Dim Tempcel4 As New TableCell
            Tempcel4.Controls.Add(New LiteralControl(dtrreader("clientcoBilladdress") & ""))
            Temprow.Cells.Add(Tempcel4)

            Dim Tempcel6 As New TableCell
            Tempcel6.Controls.Add(New LiteralControl("<a href=ClientCoEditAddress.aspx?ID=" & dtrreader("ClientCoID") & "&RID=" & dtrreader("ClientCoAddID") & ">Edit</a>"))
            Temprow.Cells.Add(Tempcel6)
            'adding the Tables rows to the table
            tblRecDetail.Rows.Add(Temprow)

        End While
        dtrreader.Close()
    End Sub
End Class
