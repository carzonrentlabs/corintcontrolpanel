<%@ Page Language="vb" AutoEventWireup="false" Src="ClientCoEditAddressResult.aspx.vb" Inherits="ClientCoEditAddressResult"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CarzonRent :: Internal software Control panel</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<table align="center">
				<tr>
					<td align="center"><STRONG><U>Edit the addresses<br>(The first address is the main one)</U></STRONG></td>
				</tr>
				<tr>
					<td valign="top">
						<asp:table ID="tblRecDetail" HorizontalAlign="Center" Runat="server" BorderColor="#cccc99"
							BorderStyle="Solid" GridLines="both" CellSpacing="0" CellPadding="0">
							<asp:tablerow>
								<asp:tableheadercell>
									&nbsp;&nbsp;No.&nbsp;&nbsp;
								</asp:tableheadercell>
								<asp:tableheadercell>
									&nbsp;&nbsp;Address&nbsp;&nbsp;
									</asp:tableheadercell>
								<asp:tableheadercell>
									&nbsp;&nbsp;Billing Address&nbsp;&nbsp;
								</asp:tableheadercell>
								<asp:tableheadercell>
									&nbsp;&nbsp;
								</asp:tableheadercell>
							</asp:tablerow>
						</asp:table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
