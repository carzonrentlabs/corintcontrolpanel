<%@ Page Language="vb" AutoEventWireup="false" Inherits="ClientCoEditForm" Src="ClientCoEditForm.aspx.vb" %>

<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="~/usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>CarzonRent :: Internal software Control panel</title>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" />
    <script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <script language="JavaScript" type="text/javascript" src="../utilityfunction.js"></script>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

    <link href="../HertzInt.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            $("#<%=txtSEZExpiry.ClientID %>").datepicker();
            $("#<%=txtContractDate.ClientID%>").datepicker();
            $("#<%=txtActiveDate.ClientID%>").datepicker();
            $("#<%=txtContractStartDate.ClientID%>").datepicker();
            $("#<%=txtContractEndDate.ClientID%>").datepicker();
            $("#<%=txtActiveDate.ClientID%>").datepicker();
            $("#<%=txtContractStartDate.ClientID %>").datepicker();
            $("#<%=odometerMaskDate.ClientID%>").datepicker();
            $("#<%=txtCentralizeEffectiveDate.ClientID%>").datepicker();
        });

        $(document).ready(function () {
            $("#txtclientPoc").keydown(function (event) {
                // Allow only backspace and delete
                if (event.keyCode == 46 || event.keyCode == 8) {
                    // let it happen, don't do anything
                }
                else {
                    // Ensure that it is a number and stop the keypress
                    if (event.keyCode < 48 || event.keyCode > 57 && event.keyCode < 96 || event.keyCode > 105) {
                        event.preventDefault();
                    }
                }
            });
        });

        function FuelSurchargeEvent() {
            if (document.forms[0].chkFuelSurchargeYN.checked == false) {
                document.forms[0].chkFuelSurchargeYNNegative.checked = false;
            }
        }

        function validation() {
            if (document.forms[0].ddlpayment.value == "Cr") {
                if (document.forms[0].txtcclimit.value == "") {
                    alert("Please fill Credit Limit")
                    document.forms[0].txtcclimit.focus();
                    return false;
                }
                if (document.forms[0].txtCreditDays.value == "") {
                    alert("Please fill Credit Days");
                    document.forms[0].txtCreditDays.focus();
                    return false;
                }
            }
            if (document.forms[0].ddlpayment.value == "") {

                alert("Please Select Payment terms.");
                document.forms[0].ddlpayment.focus();
                return false;
            }

            if (document.forms[0].ddlcityBillTo.value == "") {
                alert("Please select Bill to City.");
                document.forms[0].ddlcityBillTo.focus();
                return false;
            }

            if (document.forms[0].ddlsourceCity.value == "") {
                alert("Please select Source City.");
                document.forms[0].ddlsourceCity.focus();
                return false;
            }

            if (document.forms[0].chkbta.checked == true) {
                if (document.forms[0].txtamex.value == "") {
                    alert("Please fill Amex Card #")
                    document.forms[0].txtamex.focus();
                    return false;
                }
            }

            if (document.forms[0].txtemail.value != "") {

                var theStr = document.forms[0].txtemail.value;
                var atIndex = theStr.indexOf('@');
                var dotIndex = theStr.indexOf('.', atIndex);
                theSub = theStr.substring(0, dotIndex + 1)

                if ((atIndex < 1) || (atIndex != theStr.lastIndexOf('@')) || (dotIndex < atIndex + 2) || (theStr.length <= theSub.length)) {
                    alert("Email ID is not a valid Email ID");
                    document.forms[0].txtemail.focus();
                    return false;
                }
            }

            if (document.forms[0].ddlEbillingYN.value == "") {
                alert("Select Ebilling option.");
                document.forms[0].ddlEbillingYN.focus();
                return false;
            }

            if (isNaN(document.forms[0].txtFrom.value)) {
                alert("Billing Days from should be numeric only");
                document.forms[0].txtFrom.focus();
                return false;
            }
            if (isNaN(document.forms[0].txtTo.value)) {
                alert("Billing Days to should be numeric only");
                document.forms[0].txtTo.focus();
                return false;
            }
            if (isNaN(document.forms[0].txtSubDays.value)) {
                alert("Billing submission days should be numeric only");
                document.forms[0].txtSubDays.focus();
                return false;
            }

            if (document.forms[0].txtFrom.value == "") {
                alert("Please fill Billing Days from");
                document.forms[0].txtFrom.focus();
                return false;
            }
            if (document.forms[0].txtTo.value == "") {
                alert("Please fill Billing Days to");
                document.forms[0].txtTo.focus();
                return false;
            }
            if (document.forms[0].txtSubDays.value == "") {
                alert("Please fill Billing submission days");
                document.forms[0].txtSubDays.focus();
                return false;
            }


            if (Date.parse(document.forms[0].txtContractStartDate.value) > Date.parse(document.forms[0].txtContractEndDate.value)) {
                alert("Contract start date should not be greater than contract end date.")
                document.forms[0].txtContractStartDate.focus();
                return false;
            }

            var vtxtTanNo = document.forms[0].txtTanno.value;
            vtxtTanNo = vtxtTanNo.replace(/^\s+/, ""); //Removes Left Blank Spaces
            vtxtTanNo = vtxtTanNo.replace(/\s+$/, ""); //Removes Right Blank Spaces

             if (vtxtTanNo == "") {
                alert("Please fill Tan no.");
                document.forms[0].txtTanno.focus();
                return false;
            }

            var vtxtPanNo = document.forms[0].txtPanno.value;
            vtxtPanNo = vtxtPanNo.replace(/^\s+/, ""); //Removes Left Blank Spaces
            vtxtPanNo = vtxtPanNo.replace(/\s+$/, ""); //Removes Right Blank Spaces



            if (vtxtPanNo == "") {
                alert("Enter Pan card number.");
                document.forms[0].txtPanno.focus();
                return false;
            }

            if (vtxtPanNo != "") {

                var pan = document.forms[0].txtPanno.value;
                var regpan = /^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/;

                if (regpan.test(pan) == false) {
                    alert("Enter Valid Pan card number.");
                    document.forms[0].txtPanno.focus();
                    return false;
                }
            }

            //***********************added on 10-July -2007*************************
            if (document.forms[0].txtcclimit.value != "") {
                //Commented by rahul on 13 May 2010

                if (document.forms[0].txtCreditMailLimit.value == "") {
                    alert("Warning Limit can't be left blank");
                    document.forms[0].txtCreditMailLimit.focus();
                    return false;
                }

                if (isNaN(document.forms[0].txtCreditMailLimit.value)) {
                    alert("Warning Limit should be numeric only");
                    document.forms[0].txtCreditMailLimit.focus();
                    return false;
                }
                if ((document.forms[0].txtCreditMailLimit.value) > 100) {
                    alert("Warning Limit can't be greater than 100");
                    document.forms[0].txtCreditMailLimit.focus();
                    return false;
                }

                if (document.forms[0].txtwarningLimit.value == "") {
                    alert("Warning Limit can't be left blank");
                    document.forms[0].txtwarningLimit.focus();
                    return false;
                }

                if (isNaN(document.forms[0].txtwarningLimit.value)) {
                    alert("Warning Limit should be numeric only");
                    document.forms[0].txtwarningLimit.focus();
                    return false;
                }
                if ((document.forms[0].txtwarningLimit.value) > 100) {
                    alert("Warning Limit can't be greater than 100");
                    document.forms[0].txtwarningLimit.focus();
                    return false;
                }

                if ((document.forms[0].txtCircuitbreaker.value) == "") {
                    alert("Circuit Breaker Limit can't be left blank");
                    document.forms[0].txtCircuitbreaker.focus();
                    return false;
                }
                if (isNaN(document.forms[0].txtCircuitbreaker.value)) {
                    alert("Circuit Breaker should be numeric only");
                    document.forms[0].txtCircuitbreaker.focus();
                    return false;
                }
            }


            if (isNaN(document.forms[0].txtcclimit.value)) {
                alert("Credit Limit should be numeric only")
                document.forms[0].txtcclimit.focus();
                return false;
            }

            if (document.forms[0].chk_Entity.checked == false) {
                if (document.forms[0].ddlGroupEntity.value == "") {
                    alert("Please check company group entity.");
                    document.forms[0].ddlGroupEntity.focus();
                    return false;
                }
            }
            if (Date.parse(document.forms[0].txtCentralizeEffectiveDate.value) > Date.parse(Date.now)) {
                alert("Billing Effective Date should be greater than current date.")
                document.forms[0].txtCentralizeEffectiveDate.focus();
                return false;
            }

            if (document.forms[0].chkBilling.checked == true) {
                if (document.forms[0].txtCentralizeEffectiveDate.value == "") {
                    alert("Please select centralized effective date.");
                    return false;
                }
            var strvalues
            strvalues = ('txtcompanyName,txtaddress,txtbilladdress,ddlcity,txtContractDate,txtemail,txtUserName,txtPassword,ddlcompanytype,ddlpayment,txtFrom,txtTo,txtSubDays,txtContractStartDate,txtContractEndDate,txtCreditDays,ddlEbillingYN,ddlsourceCity')
            return checkmandatory(strvalues);
        }

        function dateReg(obj) {
            if (obj.value != "") {
                // alert(obj.value);
                var reg = /((0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[0-1])-(201[0-9]))/
                if (reg.test(obj.value)) {
                    //alert('valid');
                }
                else {
                    alert('notvalid');
                    obj.value = "";
                }
            }
        }

    </script>
</head>
<body>
    <form id="Form1" method="post" runat="server">
        <uc1:Headerctrl ID="Headerctrl1" runat="server"></uc1:Headerctrl>
        <table id="Table1" align="center">
            <tbody>
                <asp:Panel ID="pnlmainform" runat="server">
                    <tr>
                        <td align="center" colspan="2">
                            <strong><u>Edit a Client Company</u></strong>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <asp:Label ID="lblErrorMsg" runat="server" Visible="false" CssClass="subRedHead"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <asp:Label ID="lblerr" runat="server" Visible="False" CssClass="subRedHead"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td width="296">* Company Name
                        </td>
                        <td width="341">
                            <asp:TextBox ID="txtcompanyName" runat="server" CssClass="input" MaxLength="100"
                                ReadOnly="true"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>* Address
                        </td>
                        <td>
                            <asp:TextBox ID="txtaddress" runat="server" CssClass="input" MaxLength="250" ReadOnly="true"></asp:TextBox>
                            <asp:HyperLink ID="hyplnkretry3" runat="server" Target="_New">Edit addresses</asp:HyperLink>
                            <asp:HyperLink ID="hyplnkretry4" runat="server" Target="_New"> | Add more addresses in the <b>
										SAME CITY</b></asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td>* Billing Address
                        </td>
                        <td>
                            <asp:TextBox ID="txtbilladdress" runat="server" CssClass="input" MaxLength="250"
                                ReadOnly="true"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>* City
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlcity" runat="server" CssClass="input">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>* Bill To City
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlcityBillTo" runat="server" CssClass="input">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>* Source City
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlsourceCity" runat="server" CssClass="input">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>* Contract date
                        </td>
                        <td>
                            <asp:TextBox ID="txtContractDate" runat="server" CssClass="input"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Admin Head
                        </td>
                        <td>
                            <asp:TextBox ID="txtfname1" runat="server" CssClass="input" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Admin Head - Phone
                        </td>
                        <td>
                            <asp:TextBox ID="txtphone1" runat="server" CssClass="input" MaxLength="20"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Admin Head � Email
                        </td>
                        <td>
                            <asp:TextBox ID="txtEmailAH" runat="server" CssClass="input" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Finance Manager
                        </td>
                        <td>
                            <asp:TextBox ID="txtfname2" runat="server" CssClass="input" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Finance Manager - Phone
                        </td>
                        <td>
                            <asp:TextBox ID="txtphone2" runat="server" CssClass="input" MaxLength="20"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Finance Manager � Email
                        </td>
                        <td>
                            <asp:TextBox ID="txtEmailFM" runat="server" CssClass="input" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>CFO
                        </td>
                        <td>
                            <asp:TextBox ID="txtfname3" runat="server" CssClass="input" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>CFO � Phone
                        </td>
                        <td>
                            <asp:TextBox ID="txtphone3" runat="server" CssClass="input" MaxLength="20"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>CFO � Email
                        </td>
                        <td>
                            <asp:TextBox ID="txtEmailCFO" runat="server" CssClass="input" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Bill Submission Person
                        </td>
                        <td>
                            <asp:TextBox ID="txtBSP" runat="server" CssClass="input" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Bill Submission Person � Phone
                        </td>
                        <td>
                            <asp:TextBox ID="txtBSPhone" runat="server" CssClass="input" MaxLength="20"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Bill Submission Person � Email
                        </td>
                        <td>
                            <asp:TextBox ID="txtBSPEmail" runat="server" CssClass="input" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <%--<tr>
                        <td>Fax
                        </td>
                        <td>
                            <asp:TextBox ID="txtfax" runat="server" CssClass="input" MaxLength="20"></asp:TextBox>
                        </td>
                    </tr>--%>
                    <tr>
                        <td>* Email ID
                        </td>
                        <td>
                            <asp:TextBox ID="txtemail" runat="server" CssClass="input" MaxLength="100"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>* Mode Of Reservation
                        </td>
                        <td>
                            <asp:DropDownList ID="txtbookmode" runat="server" CssClass="input">
                                <asp:ListItem Text="" Value="" />
                                <asp:ListItem Text="Email" Value="Em" />
                                <asp:ListItem Text="Telephone" Value="Te" />
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>* User Name
                        </td>
                        <td>
                            <asp:TextBox ID="txtUserName" runat="server" CssClass="input" MaxLength="80"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>* Password
                        </td>
                        <td>
                            <asp:TextBox ID="txtPassword" runat="server" CssClass="input" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>* Company Type
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlcompanytype" runat="server" CssClass="input">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>* Credit Days
                        </td>
                        <td>
                            <asp:TextBox ID="txtCreditDays" runat="server" CssClass="input" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Discount %
                        </td>
                        <td>
                            <asp:DropDownList ID="ddldiscount" runat="server" CssClass="input">
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddldiscountdecimal" runat="server" CssClass="input">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>Email Required?
                        </td>
                        <td>
                            <asp:CheckBox ID="chkEmail" runat="server" CssClass="input" Checked="True"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Display Discount % on Invoice?
                        </td>
                        <td>
                            <asp:CheckBox ID="chkDispDiscInv" runat="server" CssClass="input" Checked="True"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Apply GST?
                        </td>
                        <td>
                            <asp:CheckBox ID="chkApplyServiceTaxYN" runat="server" CssClass="input" Checked="True"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td>SEZ Client?
                        </td>
                        <td>
                            <asp:CheckBox ID="chkSEZClientYN" runat="server" CssClass="input" Checked="false"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td>SEZ Expiry date
                        </td>
                        <td>
                            <asp:TextBox ID="txtSEZExpiry" runat="server" CssClass="input"></asp:TextBox>
                        </td>
                    </tr>
                    <%--<tr>
                        <td>Apply DST?
                        </td>
                        <td>
                            <asp:CheckBox ID="chkApplyDSTYN" runat="server" CssClass="input" Enabled="false"
                                Checked="False"></asp:CheckBox>
                        </td>
                    </tr>--%>
                    <tr>
                        <td>Apply CC LIMIT?
                        </td>
                        <td>
                            <asp:CheckBox ID="chkApplyCCYN" runat="server" CssClass="input" Checked="False"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Send Credit Mail?
                        </td>
                        <td>
                            <asp:CheckBox ID="chkCreditMailYN" runat="server" CssClass="input" Checked="True"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Send Invoice Mail on Closure?
                        </td>
                        <td>
                            <asp:CheckBox ID="chkInvoiceMailonClosureYN" runat="server" CssClass="input" Checked="True"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Upload Email Required?
                        </td>
                        <td>
                            <asp:CheckBox ID="chkEmailRequiredYN" runat="server" CssClass="input" Checked="True"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td>VRF Required?
                        </td>
                        <td>
                            <asp:CheckBox ID="chkVRFRequiredYN" runat="server" CssClass="input" Checked="false"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td>* Payment Terms
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlpayment" runat="server" CssClass="input">
                                <asp:ListItem Text="" Value="" />
                                <asp:ListItem Text="Credit" Value="Cr" />
                                <asp:ListItem Text="Credit Card" Value="CC" />
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>Credit Limit
                        </td>
                        <td>
                            <asp:TextBox ID="txtcclimit" runat="server" CssClass="input" MaxLength="100" ReadOnly="true"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>* Billing Basis
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlbillingbasis" runat="server" CssClass="input">
                                <asp:ListItem Text="" Value="" />
                                <asp:ListItem Text="Point To Point" Value="pp" />
                                <asp:ListItem Text="Garage To Garage" Value="gg" />
                            </asp:DropDownList>
                        </td>
                    </tr>

                                        <tr>
   <td>*Client Billing Type
    </td>
    <td>
         <asp:DropDownList ID="ddlClientBilling" runat="server" CssClass="input">
          
            <asp:ListItem Text="fornightly" Value="fornightly" />
            <asp:ListItem Text="monthly" Value="monthly" />
        </asp:DropDownList>
    </td>
</tr>
<tr>
    <td>Centralized Billing
    </td>
    <td>
        <asp:CheckBox ID="chkBilling" runat="server" CssClass="input" Checked="false"></asp:CheckBox>
    </td>
</tr>
<tr>
    <td style="height: 25px" valign="top">Centralized Billing Effective Date
    </td>
    <td style="height: 18px">
        <asp:TextBox ID="txtCentralizeEffectiveDate" runat="server" CssClass="input"></asp:TextBox>
    </td>
</tr>
                    <%--<tr>
                        <td>Service Tax #
                        </td>
                        <td>
                            <asp:TextBox ID="tstsvctax" runat="server" CssClass="input" MaxLength="20"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td>* Airport Service Unit
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlairport" runat="server" CssClass="input">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>* Hotel Service Unit
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlhotel" runat="server" CssClass="input">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>* Corporate Service Unit
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlcorporate" runat="server" CssClass="input">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>* Self Drive Service Unit
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlselfdrive" runat="server" CssClass="input">
                            </asp:DropDownList>
                        </td>
                    </tr>--%>

                    <%--<tr>
                        <td>Tariff Type
                        </td>
                        <td>
                            <asp:DropDownList ID="ddTariffType" runat="server" CssClass="input">
                                <asp:ListItem Text="National" Value="N" />
                                <asp:ListItem Text="Special" Value="S" />
                            </asp:DropDownList>
                        </td>
                    </tr>--%>
                    <tr>
                        <td>Credit Type
                        </td>
                        <td>
                            <asp:DropDownList ID="ddCreditType" runat="server" CssClass="input">
                                <asp:ListItem Text="Credit" Value="Credit" />
                                <asp:ListItem Text="Advance" Value="Advance" />
                                <asp:ListItem Text="Float" Value="Float" />
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>Apply fuel impact (Positive)
                        </td>
                        <td>
                            <asp:CheckBox ID="chkFuelSurchargeYN" onchange="FuelSurchargeEvent()" runat="server"
                                CssClass="input" Checked="False"></asp:CheckBox>
                        </td>
                    </tr>
                    <%--Added by Rahul on 04-Mar-2015--%>
                    <tr>
                        <td>Apply fuel impact (Negative)
                        <br />
                            <b>* Only Applied when "Apply fuel impact (Positive)" is applied</b>
                        </td>
                        <td>
                            <asp:CheckBox ID="chkFuelSurchargeYNNegative" onchange="FuelSurchargeEvent()" runat="server"
                                CssClass="input" Checked="False"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Fuel Surcharges Threshold %
                        </td>
                        <td>
                            <asp:TextBox ID="txtFuelSurchargesThreshold" runat="server" MaxLength="6" CssClass="input"
                                Style="width: 80px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>BTA Form Required
                        </td>
                        <td>
                            <asp:CheckBox ID="chkbta" runat="server" CssClass="input"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Apply Higher Extra Amount?
                        </td>
                        <td>
                            <asp:CheckBox ID="chkHigherExtraAmt" runat="server" CssClass="input" Checked="false"></asp:CheckBox>
                        </td>
                    </tr>
                    <%--<tr>
                        <td>Print On Duty Slip?
                        </td>
                        <td>
                            <asp:CheckBox ID="chkPrintDS" runat="server" CssClass="input" Checked="false"></asp:CheckBox>
                        </td>
                    </tr>--%>
                    <tr>
                        <td valign="top">Remarks(<span class="shwText" id="shwMessage">0</span>/2000 chars)
                        </td>
                        <td>
                            <asp:TextBox ID="txtRemarks" onkeydown="showLength(this.form.txtRemarks,'document.all.shwMessage',2000)"
                                onkeyup="showLength(this.form.txtRemarks,'document.all.shwMessage',2000)" runat="server"
                                CssClass="input" MaxLength="2000" Rows="3" Columns="50" TextMode="MultiLine"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">Special Instructions
                        </td>
                        <td>
                            <asp:TextBox ID="txtSpecialInstructions" runat="server" CssClass="input" MaxLength="5000"
                                TextMode="MultiLine" Rows="3" Columns="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Mandetory Details Required
                        </td>
                        <td>
                            <asp:TextBox ID="txtdetail" runat="server" CssClass="input" MaxLength="20"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 18px" valign="top">* Billing Period (In Days)&nbsp;From:
                        </td>
                        <td style="height: 18px">
                            <asp:TextBox ID="txtFrom" runat="server" CssClass="input" MaxLength="2" Width="48px"></asp:TextBox>&nbsp;
                        &nbsp;To &nbsp;
                        <asp:TextBox ID="txtTo" runat="server" CssClass="input" MaxLength="2" Width="48px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 18px" valign="top">* Billing&nbsp;Submition&nbsp;Days
                        </td>
                        <td style="height: 18px">
                            <asp:TextBox ID="txtSubDays" runat="server" CssClass="input" MaxLength="2" Width="48px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 18px" valign="top">Activation Status
                        </td>
                        <td style="height: 18px">
                            <asp:RadioButton ID="rdStatus1" runat="server" Text="Activate Now" GroupName="ActiveStatus"></asp:RadioButton>
                            <asp:RadioButton ID="rdStatus2" runat="server" Text="Activate Latter" GroupName="ActiveStatus"></asp:RadioButton>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 25px" valign="top">Activation Date
                        </td>
                        <td style="height: 18px">
                            <asp:TextBox ID="txtActiveDate" runat="server" CssClass="input"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 25px" valign="top">* Contract Start Date
                        </td>
                        <td style="height: 18px">
                            <asp:TextBox ID="txtContractStartDate" runat="server" CssClass="input"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 25px" valign="top">* Contract End Date
                        </td>
                        <td style="height: 18px">
                            <asp:TextBox ID="txtContractEndDate" runat="server" CssClass="input"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" colspan="2">
                            <table style="width: 608px; height: 108px">
                                <tbody>
                                    <tr>
                                        <td style="width: 172px" colspan="2">
                                            <strong>Credit Limit Exhausting (%)</strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 193px; height: 27px;">* Credit Mail Limit&nbsp;(%)
                                        </td>
                                        <td style="height: 27px">
                                            <asp:TextBox ID="txtCreditMailLimit" Text="80" Enabled="false" runat="server" CssClass="input"
                                                MaxLength="5" Width="40px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 193px">* Warning Limit&nbsp;(%)
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtwarningLimit" Text="90" Enabled="false" runat="server" CssClass="input"
                                                MaxLength="3" Width="40px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 193px; height: 21px">* Circuit breaker Limit&nbsp;(%)
                                        </td>
                                        <td style="height: 21px">
                                            <asp:TextBox Enabled="false" Text="95" value="110" ID="txtCircuitbreaker" runat="server"
                                                CssClass="input" MaxLength="3" Width="40px"></asp:TextBox>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>PAN No #
                        </td>
                        <td>
                            <asp:TextBox ID="txtPanno" runat="server" CssClass="input" MaxLength="20"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>TAN No #
                        </td>
                        <td>
                            <asp:TextBox ID="txtTanno" runat="server" CssClass="input" MaxLength="20"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Relationship Manager #
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlRelationshpMgr" runat="server" CssClass="input">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>Expected Avg. Monthly Revenue #
                        </td>
                        <td>
                            <asp:TextBox ID="txtAvgMonRev" Text="0" runat="server" CssClass="input" MaxLength="20"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Active
                        </td>
                        <td>
                            <asp:CheckBox ID="chkactive" runat="server" CssClass="input" Checked="false" Enabled="false"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Enable Personal Trip</td>
                        <td>
                            <asp:CheckBox ID="enablePersonalTrip" runat="server" CssClass="input" /></td>
                    </tr>
                    <tr>
                        <td>Odometer Mask Enabled?</td>
                        <td>
                            <asp:CheckBox ID="enableOdometerMask" runat="server" CssClass="input" /></td>
                    </tr>
                         <tr>
                        <td> Odometer Mask Effective Date
                        </td>
                        <td>
                            <asp:TextBox ID="odometerMaskDate" runat="server" CssClass="input"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Company Group Entity
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlGroupEntity" runat="server" CssClass="input">
                            </asp:DropDownList>
                            <asp:CheckBox ID="chk_Entity" runat="server" Text="Same as Company" />
                        </td>
                    </tr>
                    <tr>
                        <td>* Agreement Type
                        </td>
                        <td align="left" colspan="3">
                            <asp:DropDownList ID="ddlAgreementType" runat="server">
                                <asp:ListItem Text="Standard Agreement" Value="Standard Agreement" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Non Standard Agreement" Value="Non Standard Agreement"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>* Service Type
                        </td>
                        <td align="Left" colspan="3">
                            <asp:DropDownList ID="ddlServiceType" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>Want to edit agreement file
                        </td>
                        <td>
                            <asp:CheckBox ID="chkEdit" runat="server" CssClass="input" Checked="True" AutoPostBack="true"></asp:CheckBox>
                    </tr>
                    <tr>
                        <td>Agreement file
                        </td>
                        <td>
                             <asp:TextBox ID="txtFileName" runat="server" Enabled="false" CssClass="input" MaxLength="20"></asp:TextBox>
                            <asp:FileUpload ID="FileUpload1" runat="server" />
                           <%-- <asp:HyperLink ID="HyperLink1" Text="abc" runat="server"></asp:HyperLink>--%>
                            <asp:LinkButton ID="lnkDownloads" runat="server" Text="View Agreement Document"></asp:LinkButton>
                        </td>
                    </tr>
                   <%-- <tr>
                        <td>Agreement file Name
                        </td>
                        <td>
                            <asp:TextBox ID="txtFileName" runat="server" Enabled="false" CssClass="input" MaxLength="20"></asp:TextBox>
                        </td>
                    </tr>--%>

                    <tr>
                        <td>Want to Edit SOP File
                        </td>
                        <td>
                            <asp:CheckBox ID="chkEditSop" runat="server" CssClass="input" Checked="True" AutoPostBack="true"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td>*Billing SOP File
                        </td>
                        <td>
                            <asp:TextBox ID="txtSopFile" runat="server" Enabled="false" CssClass="input" MaxLength="50"></asp:TextBox>
                            <asp:FileUpload ID="SopFileUpload" runat="server" />
                            <asp:HyperLink ID="hlkSopFile" Text="abc" runat="server"></asp:HyperLink>
                            <asp:LinkButton ID="lnkSOAPFile" runat="server" Text="View SOAP Document"></asp:LinkButton>
                        </td>
                    </tr>
                    
                   
                    <tr>
                        <td>*E Billing option
                        </td>
                        <td align="left" colspan="3">
                            <asp:DropDownList ID="ddlEbillingYN" runat="server">
                                <asp:ListItem Text="" Value=""></asp:ListItem>
                                <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>Want to Edit CAM
                        </td>
                        <td>
                            <asp:CheckBox ID="chkCAM" runat="server" CssClass="input" AutoPostBack="true"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td>*CAM File
                        </td>
                        <td>
                            <asp:TextBox ID="txtCamFileName" runat="server" Enabled="false" CssClass="input"
                                MaxLength="50"></asp:TextBox>
                            <asp:FileUpload ID="fluCam" runat="server" />
                            <asp:HyperLink ID="hlkCam" Text="" runat="server"></asp:HyperLink>
                            <asp:LinkButton ID="lnkView" runat="server" Text="View CAM Document"></asp:LinkButton>
                        </td>
                    </tr>
                    
                    <tr>
                        <td>Client POC
                        </td>
                        <td>
                            <asp:TextBox ID="txtclientPoc" runat="server" MaxLength="10"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <asp:Button ID="btnsubmit" runat="server" CssClass="button" Text="Submit" OnClientClick="return validation();"></asp:Button>&nbsp;&nbsp;
                        <asp:Button ID="btnreset" runat="server" CssClass="button" Text="Reset" CausesValidation="False"></asp:Button>
                        </td>
                    </tr>
                </asp:Panel>
                <asp:Panel ID="pnlconfirmation" runat="server" Visible="False">
                    <tr align="center">
                        <td colspan="2">
                            <asp:Label ID="lblMessage" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr align="center">
                        <td colspan="2">
                            <asp:HyperLink ID="hyplnkretry2" runat="server"></asp:HyperLink><br />
                            <br />
                        </td>
                    </tr>
                    <tr align="center">
                        <td colspan="2">
                            <asp:HyperLink ID="hyplnkretry" runat="server"></asp:HyperLink>
                        </td>
                    </tr>
                </asp:Panel>
            </tbody>
        </table>
    </form>
</body>
</html>
