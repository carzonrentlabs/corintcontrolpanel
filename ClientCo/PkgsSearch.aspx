<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Src="PkgsSearch.aspx.vb" Inherits="PkgsSearch"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CarzonRent :: Internal software control Panel</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<uc1:Headerctrl id="Headerctrl1" runat="server"></uc1:Headerctrl>
			<br>
			<br>
			<TABLE align="center">
				<TR>
					<TD colspan="2" align="center"><STRONG><U>Assign Special Chauffeur Packages where</U></STRONG>
						<br>
						<br>
					</TD>
				</TR>
				<TR>
					<TD>Car Category</TD>
					<TD>
						<asp:DropDownList id="ddlcarcat" runat="server" CssClass="input"></asp:DropDownList></TD>
				</TR>
				<TR>
					<TD>Airport Transfer?</TD>
					<TD>
						 <asp:DropDownList id="ddAptTrns" runat="server" cssclass="input">
				          <asp:listitem Text="Any" Value="" />
				          <asp:listitem Text="Yes" Value="1" />
				          <asp:listitem Text="No" Value="0" />
				       </asp:DropDownList></TD>
				</TR>
				<TR>
					<TD>City</TD>
					<TD>
						 <asp:DropDownList id="ddlcity" runat="server" cssclass="input">
				       </asp:DropDownList></TD>
				</TR>
				<TR>
					<TD>Package Hours</TD>
					<TD>
						 <asp:DropDownList id="ddlpackage" runat="server" cssclass="input">
				       </asp:DropDownList></TD>
				</TR>
				<TR>
					<TD>Package KMs</TD>
					<TD>
						 <asp:DropDownList id="ddlpckgkm" runat="server" cssclass="input">
				       </asp:DropDownList></TD>
				</TR>
				<TR>
					<TD>Outstation?</TD>
					<TD>
						 <asp:DropDownList id="ddOutStn" runat="server" cssclass="input">
				          <asp:listitem Text="Any" Value="" />
				          <asp:listitem Text="Yes" Value="1" />
				          <asp:listitem Text="No" Value="0" />
				       </asp:DropDownList></TD>
				</TR>
				<TR>
					<TD>Package IDs</TD>
					<TD>
						 From: <asp:textbox id="PkgFromCD" size="5" runat="server" CssClass="input"></asp:textbox>
						 To: <asp:textbox id="PkgToCD" size="5" runat="server" CssClass="input"></asp:textbox>
               </TD>
				</TR>
				<TR>
					<TD>&nbsp;</TD>
					<TD></TD>
				</TR>
				<TR>
					<TD colspan="2" align="center"><STRONG><U>Assign Special Self Drive Packages where</U></STRONG>
						<br>
						<br>
					</TD>
				</TR>
				<TR>
					<TD>Car Category</TD>
					<TD>
						<asp:DropDownList id="ddlcarcatSD" runat="server" CssClass="input"></asp:DropDownList></TD>
				</TR>
				<TR>
					<TD>City</TD>
					<TD>
						 <asp:DropDownList id="ddlcitySD" runat="server" cssclass="input">
				       </asp:DropDownList></TD>
				</TR>
				<TR>
					<TD>Package Days</TD>
					<TD>
						 <asp:DropDownList id="ddlpackageSD" runat="server" cssclass="input">
				       </asp:DropDownList></TD>
				</TR>
				<TR>
					<TD>Package KMs</TD>
					<TD>
						 <asp:DropDownList id="ddlpckgkmSD" runat="server" cssclass="input">
				       </asp:DropDownList></TD>
				</TR>
				<TR>
					<TD>Package IDs</TD>
					<TD>
						 From: <asp:textbox id="PkgFromSD" size="5" runat="server" CssClass="input"></asp:textbox>
						 To: <asp:textbox id="PkgToSD" size="5" runat="server" CssClass="input"></asp:textbox>
               </TD>
				</TR>
				<TR>
					<TD>&nbsp;</TD>
					<TD></TD>
				</TR>
				<TR>
					<TD colspan="2" align="center">
						<asp:Button id="btnSubmit" runat="server" Text="Submit" CssClass="button"></asp:Button>
						<asp:Button id="btnReset" runat="server" Text="Reset" CssClass="button"></asp:Button></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
