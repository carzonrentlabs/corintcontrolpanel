Imports System
Imports System.Collections
Imports System.ComponentModel
Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Drawing
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Web.Mail
Imports System.IO
Imports System.IO.FileStream
Imports System.IO.File
Imports System.Net
Imports System.Text
Imports System.Object
Imports System.MarshalByRefObject
Imports System.Net.WebRequest


Public Class ClientCoAddForm
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtRemarks As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtSpecialInstructions As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtdetail As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkactive As System.Web.UI.WebControls.CheckBox
    Protected WithEvents enablePersonalTrip As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkDispDiscInv As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkApplyServiceTaxYN As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkSEZClientYN As System.Web.UI.WebControls.CheckBox
    Protected WithEvents txtSEZExpiry As System.Web.UI.WebControls.TextBox
    'Protected WithEvents chkApplyEduCessYN As System.Web.UI.WebControls.CheckBox
    'Protected WithEvents chkApplyHduCessYN As System.Web.UI.WebControls.CheckBox
    'Protected WithEvents chkApplyDSTYN As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkApplyCCYN As System.Web.UI.WebControls.CheckBox
    'added by rahul on 13 May 2010
    Protected WithEvents chkCreditMailYN As System.Web.UI.WebControls.CheckBox
    Protected WithEvents btnsubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnreset As System.Web.UI.WebControls.Button
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents hyplnkretry2 As System.Web.UI.WebControls.HyperLink
    Protected WithEvents hyplnkretry3 As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel
    Protected WithEvents txtcompanyName As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtaddress As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtbilladdress As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlcity As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlcityBillTo As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlsourceCity As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtfname1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtmidname1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtlname1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtphone1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtdesig1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtfname2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtmnae2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtlname2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtphone2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtdesig2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtfname3 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtmname2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtlname3 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtphone3 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtdesig3 As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtfax As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtemail As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtbookmode As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtbookmode As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtContractDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkFuelSurchargeYN As System.Web.UI.WebControls.CheckBox
    Protected WithEvents ddlcompanytype As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddldiscount As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddldiscountdecimal As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlpayment As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlbillingbasis As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    Protected WithEvents txtcclimit As System.Web.UI.WebControls.TextBox
    Protected WithEvents tstsvctax As System.Web.UI.WebControls.TextBox
    'Protected WithEvents ddlairport As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents ddlhotel As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents ddlcorporate As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents ddlselfdrive As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddTariffType As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddCreditType As System.Web.UI.WebControls.DropDownList
    Protected WithEvents chkbta As System.Web.UI.WebControls.CheckBox
    Protected WithEvents txtamex As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblerr As System.Web.UI.WebControls.Label
    Protected WithEvents txtUserName As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtPassword As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtFrom As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtTo As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtSubDays As System.Web.UI.WebControls.TextBox
    Protected WithEvents rdStatus1 As System.Web.UI.WebControls.RadioButton
    Protected WithEvents rdStatus2 As System.Web.UI.WebControls.RadioButton
    Protected WithEvents txtActiveDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents Textbox2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Textbox3 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Textbox4 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Textbox5 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Textbox6 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Textbox7 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Textbox8 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Textbox9 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Textbox10 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Textbox11 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Textbox12 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Textbox13 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Textbox14 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Textbox15 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Textbox16 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtPanno As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtTanno As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtAvgMonRev As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlRelationshpMgr As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtEmailAH As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtEmailFM As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtEmailCFO As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtPaymentCP As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtCrdLimit1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtCrdLimit2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtCrdLimit3 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtCreditMailLimit As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtwarningLimit As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtCircuitbreaker As System.Web.UI.WebControls.TextBox
    Protected WithEvents Textbox1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtContractEndDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtContractStartDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkEmailRequiredYN As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkVRFRequiredYN As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkHigherExtraAmt As System.Web.UI.WebControls.CheckBox
    'Protected WithEvents chkPrintDS As System.Web.UI.WebControls.CheckBox
    Protected WithEvents txtBSP As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtBSPhone As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtBSPEmail As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtCreditDays As System.Web.UI.WebControls.TextBox
    Protected WithEvents FileUpload1 As System.Web.UI.WebControls.FileUpload
    Protected WithEvents chk_Entity As System.Web.UI.WebControls.CheckBox
    Protected WithEvents SopFileUpload As System.Web.UI.WebControls.FileUpload
    Protected WithEvents txtFuelSurchargesThreshold As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkFuelSurchargeYNNegative As System.Web.UI.WebControls.CheckBox
    Protected WithEvents ddlEbillingYN As System.Web.UI.WebControls.DropDownList
    Protected WithEvents fluCam As System.Web.UI.WebControls.FileUpload
    Protected WithEvents ddlAgreementType As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtclientPoc As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkInvoiceMailonClosureYN As System.Web.UI.WebControls.CheckBox
    Protected WithEvents ddlServiceType As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlClientBilling As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtCentralizeEffectiveDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkBilling As System.Web.UI.WebControls.CheckBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btnsubmit.Attributes.Add("onClick", "return validation();")
        If Not Page.IsPostBack Then
            populateddl()
        End If
    End Sub
    Sub populateddl()
        Dim objAcessdata As clsutility
        objAcessdata = New clsutility

        ddlcompanytype.DataSource = objAcessdata.funcGetSQLDataReader("select CoTypeName ,CoTypeID  from CORIntCoTypeMaster   where active=1 order by CoTypeName")
        ddlcompanytype.DataValueField = "CoTypeID"
        ddlcompanytype.DataTextField = "CoTypeName"
        ddlcompanytype.DataBind()
        ddlcompanytype.Items.Insert(0, New ListItem("", ""))

        ddlcity.DataSource = objAcessdata.funcGetSQLDataReader("select cityname,cityid from CORIntCityMaster  where active=1 order by cityname")
        ddlcity.DataValueField = "cityid"
        ddlcity.DataTextField = "cityname"
        ddlcity.DataBind()
        ddlcity.Items.Insert(0, New ListItem("", ""))

        ddlcityBillTo.DataSource = objAcessdata.funcGetSQLDataReader("select cityname,cityid from CORIntCityMaster  where active=1 order by cityname")
        ddlcityBillTo.DataValueField = "cityid"
        ddlcityBillTo.DataTextField = "cityname"
        ddlcityBillTo.DataBind()
        ddlcityBillTo.Items.Insert(0, New ListItem("", ""))

        ddlsourceCity.DataSource = objAcessdata.funcGetSQLDataReader("select cityname,cityid from CORIntCityMaster  where active=1 order by cityname")
        ddlsourceCity.DataValueField = "cityid"
        ddlsourceCity.DataTextField = "cityname"
        ddlsourceCity.DataBind()
        ddlsourceCity.Items.Insert(0, New ListItem("", ""))

        'ddlairport.DataSource = objAcessdata.funcGetSQLDataReader("select UnitName ,UnitID  from CORIntUnitMaster   where active=1  order by UnitName")
        'ddlairport.DataValueField = "UnitID"
        'ddlairport.DataTextField = "UnitName"
        'ddlairport.DataBind()
        'ddlairport.Items.Insert(0, New ListItem("", ""))

        'ddlhotel.DataSource = objAcessdata.funcGetSQLDataReader("select UnitName ,UnitID  from CORIntUnitMaster   where active=1 and (Operates24_7YN = 1 or AirportYN = 1 or MainCityUnitYN = 1)  order by UnitName")
        'ddlhotel.DataValueField = "UnitID"
        'ddlhotel.DataTextField = "UnitName"
        'ddlhotel.DataBind()
        'ddlhotel.Items.Insert(0, New ListItem("", ""))

        'ddlcorporate.DataSource = objAcessdata.funcGetSQLDataReader("select UnitName ,UnitID  from CORIntUnitMaster   where active=1 and (Operates24_7YN = 1 or AirportYN = 1 or MainCityUnitYN = 1)  order by UnitName")
        'ddlcorporate.DataValueField = "UnitID"
        'ddlcorporate.DataTextField = "UnitName"
        'ddlcorporate.DataBind()
        'ddlcorporate.Items.Insert(0, New ListItem("", ""))

        'ddlselfdrive.DataSource = objAcessdata.funcGetSQLDataReader("select UnitName ,UnitID  from CORIntUnitMaster   where active=1 and (Operates24_7YN = 1 or AirportYN = 1 or MainCityUnitYN = 1) order by UnitName")
        'ddlselfdrive.DataValueField = "UnitID"
        'ddlselfdrive.DataTextField = "UnitName"
        'ddlselfdrive.DataBind()
        'ddlselfdrive.Items.Insert(0, New ListItem("", ""))

        ddlRelationshpMgr.DataSource = objAcessdata.funcGetSQLDataReader("select sysuserId, Fname + ' ' + Lname as Name from corintsysusersmaster where Active = 1 order by Fname")
        ddlRelationshpMgr.DataValueField = "sysuserId"
        ddlRelationshpMgr.DataTextField = "Name"
        ddlRelationshpMgr.DataBind()
        ddlRelationshpMgr.Items.Insert(0, New ListItem("", ""))

        ddlServiceType.DataSource = objAcessdata.funcGetSQLDataReader("select ServiceTypeId,ServiceName  from CorintServiceTypeMaster where Active=1")
        ddlServiceType.DataValueField = "ServiceTypeId"
        ddlServiceType.DataTextField = "ServiceName"
        ddlServiceType.DataBind()
        'ddlServiceType.Items.Insert(0, New ListItem("", ""))

        objAcessdata.funcpopulatenumddw(100, 0, ddldiscount)
        objAcessdata.funcpopulatenumddw(9, 0, ddldiscountdecimal)
        ddldiscount.SelectedValue = "0"
        ddldiscountdecimal.SelectedValue = "0"
        ddldiscount.Enabled = False
        ddldiscountdecimal.Enabled = False
        objAcessdata.Dispose()

    End Sub

    ' Code Added by Binary Semantics on 24-April-07
    Function IsUserNameExist(ByVal userName As String) As Boolean
        Dim userNameCheck As Boolean
        userNameCheck = False
        Dim dtrreader As SqlDataReader
        Dim accessdata As clsutility
        accessdata = New clsutility
        dtrreader = accessdata.funcGetSQLDataReader("SELECT Count(LoginID) as UserName FROM CORIntClientCoMaster WHERE LoginID =  '" & userName & "' ")
        dtrreader.Read()

        If dtrreader("UserName") > 0 Then
            userNameCheck = True
        End If

        dtrreader.Close()
        accessdata.Dispose()

        Return userNameCheck
    End Function

    Private Sub btnsubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        '**********************************************************************
        Dim str As String
        Dim SopStr As String
        Try

            If SopFileUpload.PostedFile.FileName <> "" And SopFileUpload.PostedFile.ContentLength > 0 Then
                SopStr = Path.GetFileName(SopFileUpload.FileName)
                If SopStr <> "" Then
                    If FileUpload1.PostedFile.FileName <> "" And FileUpload1.PostedFile.ContentLength > 0 Then
                        str = Path.GetFileName(FileUpload1.FileName)
                        If str <> "" Then
                            Dim SopFilename As String
                            Dim filename As String
                            Dim camFilename As String
                            Dim cmd2 As SqlCommand
                            Dim intclientcoidParam2 As SqlParameter
                            Dim MyConnection As SqlConnection
                            Dim intClientCoIdMax As String
                            MyConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("corConnectString"))
                            cmd2 = New SqlCommand("prc_MaxClientID", MyConnection)
                            cmd2.CommandType = CommandType.StoredProcedure
                            intclientcoidParam2 = cmd2.Parameters.Add("@ClientCoId", SqlDbType.Int)
                            intclientcoidParam2.Direction = ParameterDirection.Output
                            MyConnection.Open()
                            cmd2.ExecuteNonQuery()
                            intClientCoIdMax = cmd2.Parameters("@ClientCoId").Value
                            MyConnection.Close()

                            SopFilename = Path.GetFileName(SopFileUpload.FileName)
                            SopFileUpload.SaveAs("F:\Upload\ClientBilling_SOP\" + intClientCoIdMax + "_SOP_" + SopFilename)

                            filename = Path.GetFileName(FileUpload1.FileName)
                            FileUpload1.SaveAs("F:\Upload\ClientContract\" + intClientCoIdMax + "_Agreement_" + filename)

                            ' CAM File Uploading
                            camFilename = Path.GetFileName(fluCam.FileName)
                                fluCam.SaveAs("F:\Upload\ClientBilling_CAM\" + intClientCoIdMax + "_CAM_" + camFilename)
                                'End of CAM File UPload

                                Dim checkUserName As Boolean
                                checkUserName = IsUserNameExist(txtUserName.Text)
                                If Not checkUserName Then

                                    Dim cmd As SqlCommand

                                    Dim intuniqcheck As SqlParameter
                                    Dim intuniqvalue As Int32
                                    Dim intclientcoidParam As SqlParameter
                                    Dim intclientcoid As Int32
                                    Dim disdiscount As String
                                    disdiscount = ddldiscount.SelectedItem.Value + "." + ddldiscountdecimal.SelectedItem.Value
                                'cmd = New SqlCommand("procAddClientcarmaster_New", MyConnection)
                                'cmd = New SqlCommand("ProcAddClientcarmaster_PersonalTrip", MyConnection)
                                cmd = New SqlCommand("procAddClientcarmaster_ClientBilling", MyConnection)
                                cmd.CommandType = CommandType.StoredProcedure
                                    cmd.CommandTimeout = 6000
                                    cmd.Parameters.AddWithValue("@clientconame", txtcompanyName.Text.Trim())
                                    cmd.Parameters.AddWithValue("@clientcocityid", ddlcity.SelectedItem.Value)
                                    cmd.Parameters.AddWithValue("@clientcocityidBillTo", ddlcityBillTo.SelectedItem.Value)  'Added by Rahul on 27-May-2011
                                    cmd.Parameters.AddWithValue("@clientSourceCity", ddlsourceCity.SelectedItem.Value)  'Added by Rahul on 27-May-2011
                                    cmd.Parameters.AddWithValue("@contact1fname", txtfname1.Text)
                                    cmd.Parameters.AddWithValue("@contact1mname", "")
                                    cmd.Parameters.AddWithValue("@contact1lname", "")
                                    cmd.Parameters.AddWithValue("@contact1ph", txtphone1.Text)
                                    cmd.Parameters.AddWithValue("@ContactEmail1", txtEmailAH.Text)
                                    cmd.Parameters.AddWithValue("@contact1desig", "")
                                    cmd.Parameters.AddWithValue("@contact2fname", txtfname2.Text)
                                    cmd.Parameters.AddWithValue("@contact2mname", "")
                                    cmd.Parameters.AddWithValue("@contact2lname", "")
                                    cmd.Parameters.AddWithValue("@contact2ph", txtphone2.Text)
                                    cmd.Parameters.AddWithValue("@ContactEmail2", txtEmailFM.Text)
                                    cmd.Parameters.AddWithValue("@contact2desig", "")
                                    cmd.Parameters.AddWithValue("@contact3fname", txtfname3.Text)
                                    cmd.Parameters.AddWithValue("@contact3mname", "")
                                    cmd.Parameters.AddWithValue("@contact3lname", "")
                                    cmd.Parameters.AddWithValue("@contact3ph", txtphone3.Text)
                                    cmd.Parameters.AddWithValue("@ContactEmail3", txtEmailCFO.Text)
                                    cmd.Parameters.AddWithValue("@contact3desig", "")
                                'cmd.Parameters.AddWithValue("@fax", txtfax.Text)
                                cmd.Parameters.AddWithValue("@emailid", txtemail.Text)
                                    cmd.Parameters.AddWithValue("@bookingmode", txtbookmode.SelectedItem.Value)
                                    cmd.Parameters.AddWithValue("@loginName", txtUserName.Text)
                                    cmd.Parameters.AddWithValue("@password", txtPassword.Text)
                                    cmd.Parameters.AddWithValue("@cotypeid", ddlcompanytype.SelectedItem.Value)
                                    cmd.Parameters.AddWithValue("@discountpc", disdiscount)
                                    cmd.Parameters.AddWithValue("@paymentterms", ddlpayment.SelectedItem.Value)
                                    If Not txtcclimit.Text = "" Then
                                        cmd.Parameters.AddWithValue("@creditlimit", txtcclimit.Text)
                                    End If
                                    cmd.Parameters.AddWithValue("@billingbasis", ddlbillingbasis.SelectedItem.Value)
                                    cmd.Parameters.AddWithValue("@servicetaxno", tstsvctax.Text)

                                '' cmd.Parameters.AddWithValue("@airportunitid", Null) 'ddlairport.SelectedItem.Value)
                                '' cmd.Parameters.AddWithValue("@hotelunitid", Null) ''ddlhotel.SelectedItem.Value)
                                '' cmd.Parameters.AddWithValue("@corpunitid", Null) ''ddlcorporate.SelectedItem.Value)
                                '' cmd.Parameters.AddWithValue("@sdunitid", Null) ''ddlselfdrive.SelectedItem.Value)


                                'cmd.Parameters.AddWithValue("@airportunitid", ddlairport.SelectedItem.Value)
                                'cmd.Parameters.AddWithValue("@hotelunitid", ddlhotel.SelectedItem.Value)
                                'cmd.Parameters.AddWithValue("@corpunitid", ddlcorporate.SelectedItem.Value)
                                'cmd.Parameters.AddWithValue("@sdunitid", ddlselfdrive.SelectedItem.Value)


                                cmd.Parameters.AddWithValue("@TariffType", "S") 'ddTariffType.SelectedItem.Value)
                                    cmd.Parameters.AddWithValue("@CreditType", ddCreditType.SelectedItem.Value)
                                    cmd.Parameters.AddWithValue("@btayn", get_YNvalue(chkbta))
                                    'cmd.Parameters.AddWithValue("@amexcardno", "")
                                    cmd.Parameters.AddWithValue("@remarks", txtRemarks.Text)
                                    cmd.Parameters.AddWithValue("@DetailsRequired", txtdetail.Text)
                                    'cmd.Parameters.AddWithValue("@active", get_YNvalue(chkactive))
                                    cmd.Parameters.AddWithValue("@active", 0)
                                    cmd.Parameters.AddWithValue("@enablePersonalTrip", get_YNvalue(enablePersonalTrip))
                                    cmd.Parameters.AddWithValue("@createdby", Session("loggedin_user"))
                                    cmd.Parameters.AddWithValue("@DiplayDiscInvYN", get_YNvalue(chkDispDiscInv))
                                    cmd.Parameters.AddWithValue("@ApplyServiceTaxYN", get_YNvalue(chkApplyServiceTaxYN))
                                    'cmd.Parameters.AddWithValue("@ApplyEduCessYN", get_YNvalue(chkApplyEduCessYN))
                                    'cmd.Parameters.AddWithValue("@ApplyHduCessYN", get_YNvalue(chkApplyHduCessYN))
                                    cmd.Parameters.AddWithValue("@ApplyDSTYN", False) 'get_YNvalue(chkApplyDSTYN))
                                    cmd.Parameters.AddWithValue("@ApplyCCYN", get_YNvalue(chkApplyCCYN))
                                    cmd.Parameters.AddWithValue("@ApplyCreditMailYN", get_YNvalue(chkCreditMailYN))
                                    cmd.Parameters.AddWithValue("@ContractDate", txtContractDate.Text)
                                    cmd.Parameters.AddWithValue("@ContractStartDate", txtContractStartDate.Text)
                                    cmd.Parameters.AddWithValue("@ContractEndDate", txtContractEndDate.Text)
                                    cmd.Parameters.AddWithValue("@FuelSurchargeYN", get_YNvalue(chkFuelSurchargeYN))
                                    cmd.Parameters.AddWithValue("@clientPOC", txtclientPoc.Text)
                                    cmd.Parameters.AddWithValue("@SendInvoiceOnClosureYN", get_YNvalue(chkInvoiceMailonClosureYN))
                                    cmd.Parameters.AddWithValue("@SEZClientYN", get_YNvalue(chkSEZClientYN))
                                    cmd.Parameters.AddWithValue("@SEZExpiry", txtSEZExpiry.Text)

                                    If chk_Entity.Checked = True Then
                                        cmd.Parameters.AddWithValue("@GroupEntity", txtcompanyName.Text.Trim())
                                    End If

                                    intuniqcheck = cmd.Parameters.Add("@uniqcheckval", SqlDbType.Int)

                                    cmd.Parameters.AddWithValue("@BFrom", txtFrom.Text)
                                    cmd.Parameters.AddWithValue("@BTo", txtTo.Text)
                                    cmd.Parameters.AddWithValue("@Bs", txtSubDays.Text)
                                    Dim activeStatus As Integer
                                    If rdStatus1.Checked = True Then
                                        activeStatus = 0
                                    Else
                                        activeStatus = 1
                                    End If
                                    cmd.Parameters.AddWithValue("@Ast", activeStatus)
                                    cmd.Parameters.AddWithValue("@Ad", txtActiveDate.Text)
                                    cmd.Parameters.AddWithValue("@CreditLimitSender", txtCreditMailLimit.Text)
                                    cmd.Parameters.AddWithValue("@WarningLimit", txtwarningLimit.Text)
                                    cmd.Parameters.AddWithValue("@CircuitBreaker", txtCircuitbreaker.Text)
                                    cmd.Parameters.AddWithValue("@EmailRequired", get_YNvalue(chkEmailRequiredYN))
                                    cmd.Parameters.AddWithValue("@VRFRequired", get_YNvalue(chkVRFRequiredYN))
                                    '*************
                                    cmd.Parameters.AddWithValue("@Panno", txtPanno.Text)
                                    cmd.Parameters.AddWithValue("@Tanno", txtTanno.Text)
                                    cmd.Parameters.AddWithValue("@RelationshipMngr", ddlRelationshpMgr.SelectedValue)
                                    cmd.Parameters.AddWithValue("@AvgMonRev", txtAvgMonRev.Text)
                                    cmd.Parameters.AddWithValue("@FileName", str)
                                    cmd.Parameters.AddWithValue("@spInstructions", txtSpecialInstructions.Text)
                                    cmd.Parameters.AddWithValue("@ApplyHigherExtraAmt", get_YNvalue(chkHigherExtraAmt))
                                'cmd.Parameters.AddWithValue("@PrintOnDSYN", get_YNvalue(chkPrintDS))
                                cmd.Parameters.AddWithValue("@clientcoaddress", txtaddress.Text)
                                    cmd.Parameters.AddWithValue("@clientcoBilladdress", txtbilladdress.Text)
                                    cmd.Parameters.AddWithValue("@BillSubPer", txtBSP.Text)
                                    cmd.Parameters.AddWithValue("@BillSubPerPhone", txtBSPhone.Text)
                                    cmd.Parameters.AddWithValue("@BillSubPerEmail", txtBSPEmail.Text)
                                    cmd.Parameters.AddWithValue("@CreditDays", Convert.ToInt32(txtCreditDays.Text))
                                    cmd.Parameters.AddWithValue("@SOPFileName", intClientCoIdMax + "_SOP_" + SopStr)

                                    If String.IsNullOrEmpty(txtFuelSurchargesThreshold.Text) Then
                                        cmd.Parameters.AddWithValue("@FuelSurchargesThreshold", Null)
                                    Else
                                        cmd.Parameters.AddWithValue("@FuelSurchargesThreshold", Convert.ToDecimal(txtFuelSurchargesThreshold.Text.ToString()))
                                    End If
                                    cmd.Parameters.AddWithValue("@NegativeFuelSurchargeYN", get_YNvalue(chkFuelSurchargeYNNegative))
                                    cmd.Parameters.AddWithValue("@EBillingYN", ddlEbillingYN.SelectedValue)
                                    cmd.Parameters.AddWithValue("@CAMFileName", intClientCoIdMax + "_CAM_" + camFilename)
                                cmd.Parameters.AddWithValue("@AgreementType", ddlAgreementType.SelectedValue)
                                cmd.Parameters.AddWithValue("@ServiceTypeId", Convert.ToInt16(ddlServiceType.SelectedValue))
                                'cmd.Parameters.AddWithValue("@ProviderId", Session("provider_Id"))
                                cmd.Parameters.AddWithValue("@ClientBillingType", ddlClientBilling.SelectedValue)
                                If chkBilling.Checked Then

                                    cmd.Parameters.AddWithValue("@ChkBilling", True)
                                    cmd.Parameters.AddWithValue("@CentralizeEffectiveDate", txtCentralizeEffectiveDate.Text)
                                Else
                                    cmd.Parameters.AddWithValue("@ChkBilling", False)
                                    cmd.Parameters.AddWithValue("@CentralizeEffectiveDate", "")
                                End If
                                '*********************************************************************************
                                intuniqcheck.Direction = ParameterDirection.Output
                                    intclientcoidParam = cmd.Parameters.Add("@clientcoid", SqlDbType.Int)
                                    intclientcoidParam.Direction = ParameterDirection.Output

                                    MyConnection.Open()
                                    cmd.ExecuteNonQuery()
                                    intuniqvalue = cmd.Parameters("@uniqcheckval").Value
                                    MyConnection.Close()
                                    If intuniqvalue = 4 Then
                                        lblErrorMsg.Visible = True
                                        lblErrorMsg.Text = "Username should be unique."
                                        Exit Sub
                                    ElseIf intuniqvalue = 3 Then
                                        lblErrorMsg.Visible = True
                                        lblErrorMsg.Text = "Client company already exists."
                                        Exit Sub
                                    ElseIf intuniqvalue = 2 Then
                                        lblErrorMsg.Visible = True
                                        lblErrorMsg.Text = "Agreement name should be unique."
                                        Exit Sub
                                    ElseIf intuniqvalue = 1 Then
                                        lblErrorMsg.Visible = True
                                        lblErrorMsg.Text = "Client name should be difference in the same city."
                                        Exit Sub
                                    ElseIf Not intuniqvalue = 0 Then
                                        lblErrorMsg.Visible = True
                                        lblErrorMsg.Text = "Client company already exists."
                                        Exit Sub
                                    Else
                                        lblErrorMsg.Visible = False
                                        pnlmainform.Visible = False
                                        pnlconfirmation.Visible = True
                                        Dim sremailmessage As System.Text.StringBuilder
                                        sremailmessage = New System.Text.StringBuilder("Dear " & txtcompanyName.Text & " representative,<br> You can now check the status of the bookings your company has made through Carzonrent (I) Pvt Ltd.<br> ")
                                        sremailmessage.Append(" You can view the details by logging onto https://www.carzonrent.com, as Corporate.<br>It is imperative that you change your password as soon as you login for the first time, failing which you might be endangering misuse by unauthorised persons.<br> ")
                                        sremailmessage.Append("Your Login ID is: " & intclientcoid & " <br> ")
                                        sremailmessage.Append("Your Password is: " & intclientcoid & "<br>")
                                        sremailmessage.Append("Warm Regards, <br>")
                                        sremailmessage.Append("Carzonrent (I) Pvt Ltd")
                                        lblMessage.Text = "You have added the Client Company successfully"
                                        hyplnkretry.Text = "Add another Client Company"
                                        hyplnkretry.NavigateUrl = "ClientCoAddForm.aspx"

                                        'If ddTariffType.SelectedItem.Value = "S" Then
                                        'hyplnkretry2.Text = "<b>PLEASE SELECT THE SPECIAL TARIFFS FOR THIS CLIENT</b>"
                                        'hyplnkretry2.NavigateUrl = "PkgsAssignForm.aspx?ID=" & intclientcoid
                                        'End If

                                        hyplnkretry3.Text = "Add more addresses in the <b>SAME CITY</b> for this company"
                                        hyplnkretry3.NavigateUrl = "ClientCoAddAddress.aspx?ID=" & intclientcoid
                                    End If
                                Else
                                    ' UserName exist, display message
                                    lblErrorMsg.Visible = True
                                    lblErrorMsg.Text = "User Name already exists."
                                End If
                                '*************************************************************************************************
                            Else
                                lblErrorMsg.Visible = True
                            lblErrorMsg.Text = "Upload Agreement."
                        End If
                    Else
                        lblErrorMsg.Visible = True
                        lblErrorMsg.Text = "Upload Agreement."
                    End If
                Else
                    lblErrorMsg.Visible = True
                    lblErrorMsg.Text = "Upload SOP File."
                End If
            Else
                lblErrorMsg.Visible = True
                lblErrorMsg.Text = "Upload SOP File."
            End If
        Catch ex As Exception
            lblErrorMsg.Visible = True
            lblErrorMsg.Text = ex.Message
        End Try
        '*************************************************************************************************
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function

    Private Sub btnreset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnreset.Click
        Response.Redirect("clientcoaddform.aspx")
    End Sub

    Function procSendMail(ByVal strMessage As String, ByVal EMailTO As String) As Boolean

        Dim stresult As Boolean


        Dim StrMailBody As String
        StrMailBody = strMessage
        Dim objMail As MailMessage
        objMail = New MailMessage
        objMail.From = "reserve@carzonrent.com"
        objMail.To = EMailTO

        objMail.Subject = "Your Login details at Carzonrent"

        objMail.Body = StrMailBody.ToString
        objMail.BodyFormat = MailFormat.Html
        System.Web.Mail.SmtpMail.SmtpServer = System.Configuration.ConfigurationManager.AppSettings("SmtpServerName")
        Try
            System.Web.Mail.SmtpMail.Send(objMail)
            stresult = True

        Catch
            stresult = True
        End Try

        Return stresult

    End Function

End Class
