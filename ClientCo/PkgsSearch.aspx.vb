Imports commonutility
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class PkgsSearch
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtPkgID As System.Web.UI.WebControls.Textbox
    Protected WithEvents ddlcarcat As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlcarcatSD As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddAptTrns As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlcity As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlcitySD As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlpackage As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlpckgkm As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlpackageSD As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlpckgkmSD As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddOutStn As System.Web.UI.WebControls.DropDownList
    Protected WithEvents PkgFromCD As System.Web.UI.WebControls.TextBox
    Protected WithEvents PkgFromSD As System.Web.UI.WebControls.TextBox
    Protected WithEvents PkgToCD As System.Web.UI.WebControls.TextBox
    Protected WithEvents PkgToSD As System.Web.UI.WebControls.TextBox

    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnReset As System.Web.UI.WebControls.Button

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not Page.IsPostBack Then
            Dim dtrreader As SqlDataReader
            Dim accessdata As clsutility
            accessdata = New clsutility
            dtrreader = accessdata.funcGetSQLDataReader("select CarCatID  ,CarCatName from CORIntCarCatMaster order by CarCatName")
            ddlcarcat.DataSource = dtrreader
            ddlcarcat.DataValueField = "CarCatID"
            ddlcarcat.DataTextField = "CarCatName"
            ddlcarcat.DataBind()
            ddlcarcat.Items.Insert(0, New ListItem("Any", ""))

            dtrreader = accessdata.funcGetSQLDataReader("select CityName ,CityID from CORIntCityMaster where active=1 order by CityName")
            ddlcity.DataSource = dtrreader
            ddlcity.DataValueField = "CityID"
            ddlcity.DataTextField = "CityName"
            ddlcity.DataBind()
            ddlcity.Items.Insert(0, New ListItem("Any", ""))

            dtrreader = accessdata.funcGetSQLDataReader("select CarCatID  ,CarCatName from CORIntCarCatMaster order by CarCatName")
            ddlcarcatSD.DataSource = dtrreader
            ddlcarcatSD.DataValueField = "CarCatID"
            ddlcarcatSD.DataTextField = "CarCatName"
            ddlcarcatSD.DataBind()
            ddlcarcatSD.Items.Insert(0, New ListItem("Any", ""))

            dtrreader = accessdata.funcGetSQLDataReader("select CityName ,CityID from CORIntCityMaster where active=1 order by CityName")
            ddlcitySD.DataSource = dtrreader
            ddlcitySD.DataValueField = "CityID"
            ddlcitySD.DataTextField = "CityName"
            ddlcitySD.DataBind()
            ddlcitySD.Items.Insert(0, New ListItem("Any", ""))

            accessdata.funcpopulatenumddw(1000, 0, ddlpackage)
            accessdata.funcpopulatenumddw(5000, 0, ddlpckgkm)

	         accessdata.funcpopulatenumddw(365, 0, ddlpackageSD)
	         accessdata.funcpopulatenumddw(5000, 0, ddlpckgkmSD)

            dtrreader.Close()
            accessdata.Dispose()

        End If
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Response.Redirect("PkgsAssignForm.aspx?id=" & Request.QueryString("ID") & "&Cat=" & ddlcarcat.SelectedItem.Value & "&CatSD=" & ddlcarcatSD.SelectedItem.Value & "&Apt=" & ddAptTrns.SelectedItem.Value & "&Cty=" & ddlcity.SelectedItem.Value & "&CtySD=" & ddlcitySD.SelectedItem.Value & "&PkgHrs=" & ddlpackage.SelectedItem.Value & "&PkgDays=" & ddlpackageSD.SelectedItem.Value & "&PkgKMs=" & ddlpckgkm.SelectedItem.Value & "&PkgKMsSD=" & ddlpckgkmSD.SelectedItem.Value & "&OS=" & ddOutStn.SelectedItem.Value & "&PkgIDFCD=" & PkgFromCD.Text & "&PkgIDTCD=" & PkgToCD.Text & "&PkgIDFSD=" & PkgFromSD.Text & "&PkgIDTSD=" & PkgToSD.Text)
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("PkgsSearch.aspx?id=" & Request.QueryString("ID"))
    End Sub
End Class
