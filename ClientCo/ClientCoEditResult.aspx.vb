Imports commonutility
Imports System.text
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Imports System.Web.UI
Public Class ClientCoEditResult
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tblRecDetail As System.Web.UI.WebControls.Table

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not Page.IsPostBack Then
            getvalue("C.clientconame")
        End If
    End Sub
    Sub getvalue(ByVal strorderby As String)
        Dim strquery As StringBuilder
        Dim dtrreader As SqlDataReader
        Dim accessdata As clsutility
        accessdata = New clsutility
        'strquery = New StringBuilder("select C.clientcoid,C.clientconame,M.cityname, C.Contact1FName + ' ' + C.Contact1LName as ContactName, C.Contact1Ph, C.EmailID, C.CreditLimit, C.TariffType, A.ClientCoAddress, case C.active when '1' then 'Active' when '0' then 'Not Active' end as active, C.LoginID, C.CoPwd from CORIntClientCoMaster C, CORIntCityMaster M, CORIntClientCoAddMaster A where M.cityid = C.clientcocityid and C.ClientCoID = A.ClientCoID ")

        ' Query Modified by BSL on 25-April-07
        'strquery = New StringBuilder("select C.clientcoid,C.clientconame,M.cityname, C.Contact1FName + ' ' + C.Contact1LName as ContactName, C.Contact1Ph, C.EmailID, C.CreditLimit, C.TariffType, (SELECT ClientCoAddress from CORIntClientCoAddMaster where clientcoid = " & Request.QueryString("id") & "  and Counter = '1') as ClientCoAddress, case C.active when '1' then 'Active' when '0' then 'Not Active' end as active, C.LoginID, C.CoPwd from CORIntClientCoMaster C, CORIntCityMaster M where M.cityid = C.clientcocityid ")
        'strquery = New StringBuilder("select C.clientcoid,C.clientconame,M.cityname, C.Contact1FName + ' ' + C.Contact1LName as ContactName, C.Contact1Ph, C.EmailID, C.CreditLimit, C.TariffType, (SELECT ClientCoAddress from CORIntClientCoAddMaster_17Jan09 where clientcoid = " & Request.QueryString("id") & "  and Counter = '1') as ClientCoAddress, case C.active when '1' then 'Active' when '0' then 'Not Active' end as active, C.LoginID, C.CoPwd from CORIntClientCoMaster_17Jan09 C, CORIntCityMaster M where M.cityid = C.clientcocityid ")
        strquery = New StringBuilder("select C.clientcoid,C.clientconame,M.cityname, C.Contact1FName + ' ' + C.Contact1LName as ContactName, C.Contact1Ph, C.EmailID, C.CreditLimit, C.TariffType, (SELECT ClientCoAddress from CORIntClientCoAddMaster where clientcoid = " & Request.QueryString("id") & "  and Counter = '1') as ClientCoAddress, case C.active when '1' then 'Active' when '0' then 'Not Active' end as active, C.LoginID, C.CoPwd from CORIntClientCoMaster C, CORIntCityMaster M where M.cityid = C.clientcocityid ")
        If Request.QueryString("id") <> "-1" Then
            strquery.Append(" and C.clientcoid = " & Request.QueryString("id"))
        End If
        strquery.Append(" order by " & strorderby & "")
  

        dtrreader = accessdata.funcGetSQLDataReader(strquery.ToString)
        While dtrreader.Read
            Dim Temprow As New TableRow
            Temprow.HorizontalAlign = HorizontalAlign.Center


            Dim Tempcell As New TableCell
            Tempcell.Controls.Add(New LiteralControl(dtrreader("clientconame") & ""))
            Temprow.Cells.Add(Tempcell)

            Dim Tempcel2 As New TableCell
            Tempcel2.Controls.Add(New LiteralControl(dtrreader("cityname") & ""))
            Temprow.Cells.Add(Tempcel2)

            Dim Tempcel5 As New TableCell
            Tempcel5.Controls.Add(New LiteralControl(dtrreader("ClientCoAddress") & ""))
            Temprow.Cells.Add(Tempcel5)

            Dim Tempcel6 As New TableCell
            Tempcel6.Controls.Add(New LiteralControl(dtrreader("ContactName") & ""))
            Temprow.Cells.Add(Tempcel6)

            ' Code started by BSL on 25-April-07
            ' Display User name and password
            Dim Tempcel11 As New TableCell
            Tempcel11.Controls.Add(New LiteralControl(dtrreader("LoginID") & ""))
            Temprow.Cells.Add(Tempcel11)

            Dim Tempcel12 As New TableCell
            Tempcel12.Controls.Add(New LiteralControl(dtrreader("CoPwd") & ""))
            Temprow.Cells.Add(Tempcel12)
            ' Code ended by BSL

            Dim Tempcel7 As New TableCell
            Tempcel7.Controls.Add(New LiteralControl(dtrreader("Contact1Ph") & ""))
            Temprow.Cells.Add(Tempcel7)

            Dim Tempcel8 As New TableCell
            Tempcel8.Controls.Add(New LiteralControl(dtrreader("EmailID") & ""))
            Temprow.Cells.Add(Tempcel8)

            Dim Tempcel9 As New TableCell
            Tempcel9.Controls.Add(New LiteralControl(dtrreader("CreditLimit") & ""))
            Temprow.Cells.Add(Tempcel9)

            Dim Tempcel10 As New TableCell
            Tempcel10.Controls.Add(New LiteralControl(Replace(Replace(dtrreader("TariffType"), "S", "Special"), "N", "National") & ""))
            Temprow.Cells.Add(Tempcel10)

            Dim Tempcel3 As New TableCell
            Tempcel3.Controls.Add(New LiteralControl(dtrreader("active") & ""))
            Temprow.Cells.Add(Tempcel3)

            Dim Tempcel4 As New TableCell
            Tempcel4.Controls.Add(New LiteralControl("<a href=ClientCoEditForm.aspx?ID=" & dtrreader("clientcoid") & " >Edit</a>"))
            Temprow.Cells.Add(Tempcel4)
            'adding the Tables rows to the table
            tblRecDetail.Rows.Add(Temprow)

        End While
        dtrreader.Close()
    End Sub
    Sub SortGird(ByVal sender As Object, ByVal e As System.EventArgs)
        Select Case sender.id
            Case "Linkbutton1"
                getvalue("C.clientconame")
            Case "Linkbutton2"
                getvalue("M.cityname")
            Case "Linkbutton5"
                getvalue("A.ClientCoAddress")
            Case "Linkbutton6"
                getvalue("ContactName")
            Case "Linkbutton7"
                getvalue("C.Contact1Ph")
            Case "Linkbutton8"
                getvalue("C.EmailID")
            Case "Linkbutton9"
                getvalue("C.CreditLimit")
            Case "Linkbutton10"
                getvalue("C.TariffType")
            Case "Linkbutton3"
                getvalue("C.active")
                ' Code started by BSL on 25-April-07
            Case "aUserName"
                getvalue("C.LoginID")
            Case "aPassword"
                getvalue("C.CoPwd")

        End Select

    End Sub

End Class
