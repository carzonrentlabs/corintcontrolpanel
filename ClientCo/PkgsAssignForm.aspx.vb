Imports commonutility
Imports System.text
Imports System.data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class PkgsAssignForm
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblusername As System.Web.UI.WebControls.Label
    Protected WithEvents lblusernameSD As System.Web.UI.WebControls.Label
    Protected WithEvents lstavailablemodule As System.Web.UI.WebControls.ListBox
    Protected WithEvents lstAssignedModules As System.Web.UI.WebControls.ListBox
    Protected WithEvents lstavailablemoduleSD As System.Web.UI.WebControls.ListBox
    Protected WithEvents lstAssignedModulesSD As System.Web.UI.WebControls.ListBox
    Protected WithEvents btnsubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnreset As System.Web.UI.WebControls.Button
    Protected WithEvents hdnSelectedlst As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents hdnSelectedlstSD As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        btnsubmit.Attributes("onClick") = "return postForm();"
        'Put user code to initialize the page here
        If Not Page.IsPostBack Then
           Dim strquery As StringBuilder
            Dim dtrreader As SqlDataReader
            Dim accessdata As clsutility
            accessdata = New clsutility
	        strquery = New StringBuilder("select a.PkgID, (CAST(a.PkgID as varchar) + '-' + CAST(b.CarCatName as varchar) + '-' + CAST(Replace(Replace(a.AptTransYN, '1', 'Airport Trans'), '0', '') as varchar) + '-' + CAST(Replace(Replace(a.OutStationYN, '1', 'Outstn'), '0', '') as varchar) + '-' + isNull(CAST(c.CityName as varchar),'') + '-' + CAST(a.PkgHrs as varchar) + 'Hrs/' + CAST(a.PkgKMs as varchar) + 'KMs-Rate:Rs' + CAST(a.PkgRate as varchar) + '- Extra(Hr):Rs' + CAST(a.ExtraHrRate as varchar) + '- Extra(KM):Rs' + CAST(a.ExtraKMRate as varchar) + '- Threshold Hr:' + CAST(a.ThresholdExtraHr as varchar) + '- Threshold KM:' + CAST(a.ThresholdExtraKM as varchar) + '- Outstn:Rs' + CAST(a.OutStationAllowance as varchar) + '- Night stay:Rs' + CAST(a.NightStayAllowance as varchar)) as PkgDesc from CORIntChaufPkgsMaster as a Left outer Join CORIntCityMaster as c on a.CityID = c.CityID, CORIntCarCatMaster as b where a.CarCatID = b.CarCatID and a.Active = 1 and TariffType = 'S' ")
	        If Request.QueryString("Cat") <> "" Then
	            strquery.Append(" and a.CarCatID=" & Request.QueryString("Cat") & " ")
	        End If
	        If Request.QueryString("Apt") <> "" Then
	            strquery.Append(" and a.AptTransYN='" & Request.QueryString("Apt") & "' ")
	        End If
	        If Request.QueryString("Cty") <> "" Then
	            strquery.Append(" and a.CityID=" & Request.QueryString("Cty") & " ")
	        End If
	        If Request.QueryString("PkgHrs") <> "" Then
	            strquery.Append(" and a.PkgHrs=" & Request.QueryString("PkgHrs") & " ")
	        End If
	        If Request.QueryString("PkgKMs") <> "" Then
	            strquery.Append(" and a.PkgKMs=" & Request.QueryString("PkgKMs") & " ")
	        End If
	        If Request.QueryString("OS") <> "" Then
	            strquery.Append(" and a.OutStationYN='" & Request.QueryString("OS") & "' ")
	        End If
	        If Request.QueryString("PkgIDFCD") <> "" Then
	            strquery.Append(" and a.PkgID >='" & Request.QueryString("PkgIDFCD") & "' ")
	        End If
	        If Request.QueryString("PkgIDTCD") <> "" Then
	            strquery.Append(" and a.PkgID <='" & Request.QueryString("PkgIDTCD") & "' ")
	        End If

	        strquery.Append(" and a.PkgID not in(select PkgID from CORIntChaufSplPkgsClientLink where ClientType = 'C' and ClientID = " & Request.QueryString("ID") & ") order by a.PkgID ")
           dtrreader = accessdata.funcGetSQLDataReader(strquery.ToString)
            lstavailablemodule.DataSource = dtrreader
            lstavailablemodule.DataValueField = "PkgID"
            lstavailablemodule.DataTextField = "PkgDesc"
            lstavailablemodule.DataBind()
            dtrreader.Close()

            dtrreader = accessdata.funcGetSQLDataReader("select a.PkgID, (CAST(a.PkgID as varchar) + '-' + CAST(c.CarCatName as varchar) + '-' + CAST(Replace(Replace(a.AptTransYN, '1', 'Airport Trans'), '0', '') as varchar) + '-' + CAST(Replace(Replace(a.OutStationYN, '1', 'Outstn'), '0', '') as varchar) + '-' + isNull(CAST(d.CityName as varchar),'') + '-' + CAST(a.PkgHrs as varchar) + 'Hrs/' + CAST(a.PkgKMs as varchar) + 'KMs-Rate: Rs' + CAST(a.PkgRate as varchar) + '-Extra(Hr): Rs' + CAST(a.ExtraHrRate as varchar) + '-Extra(KM)Rate: Rs' + CAST(a.ExtraKMRate as varchar) + '-Threshold Hr:' + CAST(a.ThresholdExtraHr as varchar) + '-Threshold KM:' + CAST(a.ThresholdExtraKM as varchar) + '-Outstn:Rs' + CAST(a.OutStationAllowance as varchar) + '-Night stay:Rs' + CAST(a.NightStayAllowance as varchar)) as PkgDesc from CORIntChaufSplPkgsClientLink as b, CORIntChaufPkgsMaster as a Left outer Join CORIntCityMaster as d on a.CityID = d.CityID, CORIntCarCatMaster as c where a.PkgID = b.PkgID and  a.CarCatID = c.CarCatID and  b.ClientType = 'C' and a.Active = 1 and a.TariffType = 'S' and b.ClientID =" & Request.QueryString("id") & "")
            lstAssignedModules.DataSource = dtrreader
            lstAssignedModules.DataValueField = "PkgID"
            lstAssignedModules.DataTextField = "PkgDesc"
            lstAssignedModules.DataBind()
            dtrreader.Close()

            dtrreader = accessdata.funcGetSQLDataReader("select ClientCoName from CORIntClientCoMaster where ClientCoID=" & Request.QueryString("id") & "")
            dtrreader.Read()
            lblusername.Text = dtrreader("ClientCoName")
            dtrreader.Close()


	        strquery = New StringBuilder("select a.PkgID, (CAST(a.PkgID as varchar) + '-' + CAST(b.CarCatName as varchar) + '-' + isNull(CAST(c.CityName as varchar),'') + '-' + CAST(a.PkgDays as varchar) + 'Days/' + CAST(a.PkgKMs as varchar) + 'KMs-Rate:Rs' + CAST(a.PkgRate as varchar) + '- Extra(Day):Rs' + CAST(a.ExtraDayRate as varchar) + '- Extra(KM):Rs' + CAST(a.ExtraKMRate as varchar) + '- Threshold Day:' + CAST(a.ThresholdExtraDay as varchar) + '- Threshold KM:' + CAST(a.ThresholdExtraKM as varchar) + '- Deposit:Rs' + CAST(a.DepositAmt as varchar)) as PkgDesc from CORIntSDPkgsMaster as a Left outer Join CORIntCityMaster as c on a.CityID = c.CityID, CORIntCarCatMaster as b where a.CarCatID = b.CarCatID and a.Active = 1 and TariffType = 'S' ")
	        If Request.QueryString("CatSD") <> "" Then
	            strquery.Append(" and a.CarCatID=" & Request.QueryString("CatSD") & " ")
	        End If
	        If Request.QueryString("CtySD") <> "" Then
	            strquery.Append(" and a.CityID=" & Request.QueryString("CtySD") & " ")
	        End If
	        If Request.QueryString("PkgDays") <> "" Then
	            strquery.Append(" and a.PkgDays=" & Request.QueryString("PkgDays") & " ")
	        End If
	        If Request.QueryString("PkgKMsSD") <> "" Then
	            strquery.Append(" and a.PkgKMs=" & Request.QueryString("PkgKMsSD") & " ")
	        End If
	        If Request.QueryString("PkgIDFSD") <> "" Then
	            strquery.Append(" and a.PkgID >='" & Request.QueryString("PkgIDFSD") & "' ")
	        End If
	        If Request.QueryString("PkgIDTSD") <> "" Then
	            strquery.Append(" and a.PkgID <='" & Request.QueryString("PkgIDTSD") & "' ")
	        End If

	        strquery.Append(" and a.PkgID not in(select PkgID from CORIntSDSplPkgsClientLink where ClientType = 'C' and ClientID = " & Request.QueryString("ID") & ") order by a.PkgID")
           dtrreader = accessdata.funcGetSQLDataReader(strquery.ToString)
            lstavailablemoduleSD.DataSource = dtrreader
            lstavailablemoduleSD.DataValueField = "PkgID"
            lstavailablemoduleSD.DataTextField = "PkgDesc"
            lstavailablemoduleSD.DataBind()
            dtrreader.Close()

            dtrreader = accessdata.funcGetSQLDataReader("select a.PkgID, (CAST(a.PkgID as varchar) + '-' + CAST(c.CarCatName as varchar) + '-' + isNull(CAST(d.CityName as varchar),'') + '-' + CAST(a.PkgDays as varchar) + 'Days/' + CAST(a.PkgKMs as varchar) + 'KMs-Rate:Rs' + CAST(a.PkgRate as varchar) + '- Extra(Day):Rs' + CAST(a.ExtraDayRate as varchar) + '- Extra(KM):Rs' + CAST(a.ExtraKMRate as varchar) + '- Threshold Day:' + CAST(a.ThresholdExtraDay as varchar) + '- Threshold KM:' + CAST(a.ThresholdExtraKM as varchar) + '- Deposit:Rs' + CAST(a.DepositAmt as varchar)) as PkgDesc from CORIntSDSplPkgsClientLink as b, CORIntSDPkgsMaster as a Left outer Join CORIntCityMaster as d on a.CityID = d.CityID, CORIntCarCatMaster as c where a.PkgID = b.PkgID and  a.CarCatID = c.CarCatID and  b.ClientType = 'C' and a.Active = 1 and a.TariffType = 'S' and b.ClientID =" & Request.QueryString("id") & "")
            lstAssignedModulesSD.DataSource = dtrreader
            lstAssignedModulesSD.DataValueField = "PkgID"
            lstAssignedModulesSD.DataTextField = "PkgDesc"
            lstAssignedModulesSD.DataBind()
            dtrreader.Close()

            dtrreader = accessdata.funcGetSQLDataReader("select ClientCoName from CORIntClientCoMaster where ClientCoID=" & Request.QueryString("id") & "")
            dtrreader.Read()
            lblusernameSD.Text = dtrreader("ClientCoName")
            dtrreader.Close()
            accessdata.Dispose()
        End If
    End Sub

    Private Sub btnsubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsubmit.Click

        Dim arrModuleid As Array
        Dim intcounter As Int32
        arrModuleid = Request.Form("hdnSelectedlst").Split(",")

        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        '#######################################
        'cmd = New SqlCommand("delete from CORIntChaufSplPkgsClientLink where ClientType = 'C' and ClientID = " & Request.QueryString("id"), MyConnection)
        'MyConnection.Open()
        'cmd.ExecuteNonQuery()
        'MyConnection.Close()
        cmd = New SqlCommand("procDelAssignPkg", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@ClientType", "C")
        cmd.Parameters.Add("@ClientID", Request.QueryString("id"))
        MyConnection.Open()
        Try
            cmd.ExecuteNonQuery()
        Catch exp As SqlException
        End Try
        MyConnection.Close()

        '##############################################################################
        For intcounter = 0 To UBound(arrModuleid)
            cmd = New SqlCommand("procAssignedPkgsCDCo", MyConnection)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@PkgID", arrModuleid(intcounter))
            cmd.Parameters.Add("@ClientID", Request.QueryString("ID"))
            MyConnection.Open()
            Try
                cmd.ExecuteNonQuery()
            Catch exp As SqlException
            End Try
            MyConnection.Close()

        Next

        Dim arrModuleidSD As Array
        Dim intcounterSD As Int32
        arrModuleidSD = Request.Form("hdnSelectedlstSD").Split(",")

        Dim MyConnectionSD As SqlConnection
        MyConnectionSD = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmdSD As SqlCommand

        cmdSD = New SqlCommand("delete from CORIntSDSplPkgsClientLink where ClientType = 'C' and ClientID = " & Request.QueryString("id"), MyConnectionSD)
        MyConnectionSD.Open()
        cmdSD.ExecuteNonQuery()
        MyConnectionSD.Close()


        For intcounterSD = 0 To UBound(arrModuleidSD)
            cmdSD = New SqlCommand("procAssignedPkgsSDCo", MyConnectionSD)
            cmdSD.CommandType = CommandType.StoredProcedure
            cmdSD.Parameters.Add("@PkgIDSD", arrModuleidSD(intcounterSD))
            cmdSD.Parameters.Add("@ClientIDSD", Request.QueryString("ID"))
            MyConnectionSD.Open()
            Try
                cmdSD.ExecuteNonQuery()
            Catch expSD As SqlException
            End Try
            MyConnectionSD.Close()

        Next

        pnlmainform.Visible = False
        pnlconfirmation.Visible = True

        lblMessage.Text = "You have Assigned the special packages successfully."
        hyplnkretry.Text = "Assign special packages to another client company"
        hyplnkretry.NavigateUrl = "PkgsAssignSearch.aspx"
       
    End Sub

    Private Sub btnreset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnreset.Click
        Response.Redirect("PkgsAssignForm.aspx?id=" & Request.QueryString("ID") & "")
    End Sub
End Class