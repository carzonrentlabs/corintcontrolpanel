<%@ Page Language="vb" AutoEventWireup="false" Inherits="ClientCoAddForm" Src="ClientCoAddForm.aspx.vb" %>

<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="~/usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>CarzonRent :: Internal software Control panel</title>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <script language="JavaScript" src="../utilityfunction.js"></script>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

    <link href="../HertzInt.css" type="text/css" rel="stylesheet">
    <script language="javascript">

        $(document).ready(function () {
            $("#<%=txtSEZExpiry.ClientID %>").datepicker();
            $("#<%=txtContractDate.ClientID%>").datepicker();
            $("#<%=txtActiveDate.ClientID%>").datepicker();
            $("#<%=txtContractStartDate.ClientID%>").datepicker();
            $("#<%=txtContractEndDate.ClientID%>").datepicker();
            $("#<%=txtActiveDate.ClientID%>").datepicker();
            $("#<%=txtContractStartDate.ClientID %>").datepicker();
            $("#<%=txtCentralizeEffectiveDate.ClientID%>").datepicker();
        });

        $(document).ready(function () {
            $("#txtclientPoc,#txtphone1,#txtphone2,#txtBSPhone,#txtphone3,#txtcclimit,#txtCreditDays,#txtFrom,#txtTo,#txtSubDays,").keydown(function (event) {
                // Allow only backspace and delete
                if (event.keyCode == 46 || event.keyCode == 8) {
                    // let it happen, don't do anything
                }
                else {
                    // Ensure that it is a number and stop the keypress
                    if (event.keyCode < 48 || event.keyCode > 57 && event.keyCode < 96 || event.keyCode > 105) {
                        event.preventDefault();
                    }
                }
            });
        });

        function FuelSurchargeEvent() {
            if (document.forms[0].chkFuelSurchargeYN.checked == false) {
                document.forms[0].chkFuelSurchargeYNNegative.checked = false;
            }
        }

        function validation() {


            if (document.forms[0].ddlpayment.value == "Cr") {
                if (document.forms[0].txtcclimit.value == "") {
                    alert("Please fill Credit Limit");
                    document.forms[0].txtcclimit.focus();
                    return false;
                }
                if (document.forms[0].txtCreditDays.value == "") {
                    alert("Please fill Credit Days");
                    document.forms[0].txtCreditDays.focus();
                    return false;
                }
            }

            if (document.forms[0].ddlpayment.value == "") {

                alert("Please Select Payment terms.");
                document.forms[0].ddlpayment.focus();
                return false;

            }
            if (document.forms[0].chk_Entity.checked == false) {
                alert("Please check company group entity.");
                return false;
            }

            //var spInt = document.forms[0].txtSpecialInstructions.value;
            //spInt = spInt.replace(/^\s+/,""); 	//Removes Left Blank Spaces
            //spInt = spInt.replace(/\s+$/,""); 	//Removes Right Blank Spaces

            //if (spInt == "")
            //{
            //    alert("Please Enter Special Instruction for the Client.");
            //    document.forms[0].txtSpecialInstructions.focus();
            //    return false;
            //}

            if (document.forms[0].ddlcityBillTo.value == "") {
                alert("Please select Bill to City.");
                document.forms[0].ddlcityBillTo.focus();
                return false;
            }

            if (document.forms[0].ddlsourceCity.value == "") {
                alert("Please select Source City.");
                document.forms[0].ddlsourceCity.focus();
                return false;
            }

            if (document.forms[0].chkbta.checked == true) {
                if (document.forms[0].txtamex.value == "") {
                    alert("Please fill Amex Card #");
                    return false;
                }
            }

            if (isNaN(document.forms[0].txtcclimit.value)) {
                alert("Credit Limit should be numeric only");
                document.forms[0].txtcclimit.focus();
                return false;
            }
            //Add by bk Sharma on 02-Dec-2013 due to change fuel surcharges threshold
            if (isNaN(document.forms[0].txtFuelSurchargesThreshold.value)) {
                alert("Fuel surcharges threshold should be numeric only");
                document.forms[0].txtFuelSurchargesThreshold.focus();
                return false;
            }
            //End
            if (document.forms[0].txtemail.value != "") {

                var theStr = document.forms[0].txtemail.value;
                var atIndex = theStr.indexOf('@');
                var dotIndex = theStr.indexOf('.', atIndex);
                theSub = theStr.substring(0, dotIndex + 1);

                if ((atIndex < 1) || (atIndex != theStr.lastIndexOf('@')) || (dotIndex < atIndex + 2) || (theStr.length <= theSub.length)) {
                    alert("Email ID is not a valid Email ID");
                    document.forms[0].txtemail.focus();
                    return false;
                }
            }


            if (isNaN(document.forms[0].txtFrom.value)) {
                alert("Billing Days from should be numeric only");
                document.forms[0].txtFrom.focus();
                return false;
            }
            if (isNaN(document.forms[0].txtTo.value)) {
                alert("Billing Days to should be numeric only");
                document.forms[0].txtTo.focus();
                return false;
            }
            if (isNaN(document.forms[0].txtSubDays.value)) {
                alert("Billing submission days should be numeric only");
                document.forms[0].txtSubDays.focus();
                return false;
            }

            if (document.forms[0].txtFrom.value == "") {
                alert("Please fill Billing Days from");
                document.forms[0].txtFrom.focus();
                return false;
            }
            if (document.forms[0].txtTo.value == "") {
                alert("Please fill Billing Days to");
                document.forms[0].txtTo.focus();
                return false;
            }
            if (document.forms[0].txtSubDays.value == "") {
                alert("Please fill Billing submission days");
                document.forms[0].txtSubDays.focus();
                return false;
            }
            if (Date.parse(document.forms[0].txtContractStartDate.value) > Date.parse(document.forms[0].txtContractEndDate.value)) {
                alert("Contract start date should not be greater than contract end date.")
                document.forms[0].txtContractStartDate.focus();
                return false;
            }

            if (Date.parse(document.forms[0].txtCentralizeEffectiveDate.value) > Date.parse(Date.now)) {
                alert("Billing Effective Date should be greater than current date.")
                document.forms[0].txtCentralizeEffectiveDate.focus();
                return false;
            }
              var vtxtTanNo = document.forms[0].txtTanno.value;
            vtxtTanNo = vtxtTanNo.replace(/^\s+/, ""); //Removes Left Blank Spaces
            vtxtTanNo = vtxtTanNo.replace(/\s+$/, ""); //Removes Right Blank Spaces

             if (vtxtTanNo == "") {
                alert("Please fill Tan no.");
                document.forms[0].txtTanno.focus();
                return false;
            }

            var vtxtPanNo = document.forms[0].txtPanno.value;
            vtxtPanNo = vtxtPanNo.replace(/^\s+/, ""); //Removes Left Blank Spaces
            vtxtPanNo = vtxtPanNo.replace(/\s+$/, ""); //Removes Right Blank Spaces



            if (vtxtPanNo == "") {
                alert("Enter Pan card number.");
                document.forms[0].txtPanno.focus();
                return false;
            }

            if (vtxtPanNo != "") {

                var pan = document.forms[0].txtPanno.value;
                var regpan = /^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/;

                if (regpan.test(pan) == false) {
                    alert("Enter Valid Pan card number.");
                    document.forms[0].txtPanno.focus();
                    return false;
                }
            }


            /*
            if (document.forms[0].Textbox4.value != "") {

                var theStr = document.forms[0].Textbox4.value;
                var atIndex = theStr.indexOf('@');
                var dotIndex = theStr.indexOf('.', atIndex);
                theSub = theStr.substring(0, dotIndex + 1)

                if ((atIndex < 1) || (atIndex != theStr.lastIndexOf('@')) || (dotIndex < atIndex + 2) || (theStr.length <= theSub.length)) {
                    alert("Email ID is not a valid Email ID");
                    return false;
                }
            }

            if (document.forms[0].Textbox7.value != "") {

                var theStr = document.forms[0].Textbox7.value;
                var atIndex = theStr.indexOf('@');
                var dotIndex = theStr.indexOf('.', atIndex);
                theSub = theStr.substring(0, dotIndex + 1)

                if ((atIndex < 1) || (atIndex != theStr.lastIndexOf('@')) || (dotIndex < atIndex + 2) || (theStr.length <= theSub.length)) {
                    alert("Email ID is not a valid Email ID");
                    return false;
                }
            }
            if (document.forms[0].Textbox10.value != "") {

                var theStr = document.forms[0].Textbox10.value;
                var atIndex = theStr.indexOf('@');
                var dotIndex = theStr.indexOf('.', atIndex);
                theSub = theStr.substring(0, dotIndex + 1)

                if ((atIndex < 1) || (atIndex != theStr.lastIndexOf('@')) || (dotIndex < atIndex + 2) || (theStr.length <= theSub.length)) {
                    alert("Email ID is not a valid Email ID");
                    return false;
                }
            }
            if (document.forms[0].Textbox13.value != "") {

                var theStr = document.forms[0].txtemail.value;
                var atIndex = theStr.indexOf('@');
                var dotIndex = theStr.indexOf('.', atIndex);
                theSub = theStr.substring(0, dotIndex + 1)

                if ((atIndex < 1) || (atIndex != theStr.lastIndexOf('@')) || (dotIndex < atIndex + 2) || (theStr.length <= theSub.length)) {
                    alert("Email ID is not a valid Email ID");
                    return false;
                }
            }
            if (document.forms[0].Textbox16.value != "") {

                var theStr = document.forms[0].txtemail.value;
                var atIndex = theStr.indexOf('@');
                var dotIndex = theStr.indexOf('.', atIndex);
                theSub = theStr.substring(0, dotIndex + 1)

                if ((atIndex < 1) || (atIndex != theStr.lastIndexOf('@')) || (dotIndex < atIndex + 2) || (theStr.length <= theSub.length)) {
                    alert("Email ID is not a valid Email ID");
                    return false;
                }
            }*/

            if (document.forms[0].ddlEbillingYN.value == "") {
                alert("Select Ebilling option.");
                document.forms[0].ddlEbillingYN.focus();
                return false;
            }



            /*if(document.forms[0].txtUserName.value.length < 5)
            {
            alert("User Name must be 5 digits long.");
            return false;
            }
            if(document.forms[0].txtPassword.value.length < 5)
            {
            alert("Password must be 5 digits long.");
            return false;
            }*/

            if (document.forms[0].txtcclimit.value != "") {
                //Commented by rahul on 13 May 2010				

                //if(document.forms[0].txtCrdLimit1.value == "" ) 
                //{
                //alert("Credit Limit Ist can't be left blank");
                //return false;
                //}

                //if (isNaN(document.forms[0].txtCrdLimit1.value))
                //{
                //alert("Credit Limit I should be numeric only");
                //return false;
                //}
                //if ((document.forms[0].txtCrdLimit1.value)>100)
                //{
                //	alert("Credit Limit I can't be greater than 100");
                //	return false;
                //}


                //if((document.forms[0].txtCrdLimit2.value) == "") 
                //{
                //alert("Credit Limit II can't be left blank");
                //return false;
                //}
                //if (isNaN(document.forms[0].txtCrdLimit2.value))
                //{
                //	alert("Credit Limit II should be numeric only");
                //	return false;
                //}
                //if ((document.forms[0].txtCrdLimit2.value)>100)
                //{
                //	alert("Credit Limit II can't be greater than 100");
                //	return false;
                //}

                //if((document.forms[0].txtCrdLimit3.value) == "") 
                //{
                //alert("Credit Limit III can't be left blank");
                //return false;
                //}
                //if (isNaN(document.forms[0].txtCrdLimit3.value))
                //{
                //	alert("Credit Limit III should be numeric only");
                //	return false;
                //}
                //if ((document.forms[0].txtCrdLimit3.value)>100)
                //{
                //	alert("Credit Limit III can't be greater than 100");
                //	return false;
                //}
                if (document.forms[0].txtCreditMailLimit.value == "") {
                    alert("Warning Limit can't be left blank");
                    document.forms[0].txtCreditMailLimit.focus();
                    return false;
                }

                if (isNaN(document.forms[0].txtCreditMailLimit.value)) {
                    alert("Warning Limit should be numeric only");
                    document.forms[0].txtCreditMailLimit.focus();
                    return false;
                }
                if ((document.forms[0].txtCreditMailLimit.value) > 100) {
                    alert("Warning Limit can't be greater than 100");
                    document.forms[0].txtCreditMailLimit.focus();
                    return false;
                }

                if (document.forms[0].txtwarningLimit.value == "") {
                    alert("Warning Limit can't be left blank");
                    document.forms[0].txtwarningLimit.focus();
                    return false;
                }

                if (isNaN(document.forms[0].txtwarningLimit.value)) {
                    alert("Warning Limit should be numeric only");
                    document.forms[0].txtwarningLimit.focus();
                    return false;
                }
                if ((document.forms[0].txtwarningLimit.value) > 100) {

                    alert("Warning Limit can't be greater than 100");
                    document.forms[0].txtwarningLimit.focus();
                    return false;
                }

                if ((document.forms[0].txtCircuitbreaker.value) == "") {
                    alert("Circuit Breaker Limit can't be left blank");
                    document.forms[0].txtCircuitbreaker.focus();
                    return false;
                }
                if (isNaN(document.forms[0].txtCircuitbreaker.value)) {
                    alert("Circuit Breaker should be numeric only");
                    document.forms[0].txtCircuitbreaker.focus();
                    return false;
                }
                if (isNaN(document.forms[0].txtAvgMonRev.value)) {
                    alert("Warning Avg. Monthly Revenue should be numeric only");
                    document.forms[0].txtAvgMonRev.focus();
                    return false;
                }

                if (document.forms[0].chkBilling.checked == true) {
                    if (document.forms[0].txtCentralizeEffectiveDate.value == "") {
                        alert("Please select centralized effective date.");
                        return false;
                    }
                    
                //if ((document.forms[0].txtCircuitbreaker.value)>100)
                //{
                //	alert("Circuit Breaker can't be greater than 100");
                //	return false;
                //}

            }

            var strvalues;
                strvalues = ('txtcompanyName,txtaddress,txtbilladdress,ddlcity,txtContractDate,txtemail,txtUserName,txtPassword,ddlcompanytype,ddlpayment,txtFrom,txtTo,txtSubDays,txtContractStartDate,txtContractEndDate,txtCreditDays,ddlEbillingYN,ddlsourceCity,ddlbillingbasis,txtbookmode,ddlClientBilling');
            return checkmandatory(strvalues);
        }

        function dateReg(obj) {
            if (obj.value != "") {
                // alert(obj.value);
                var reg = /((0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[0-1])-(201[0-9]))/
                if (reg.test(obj.value)) {
                    //alert('valid');
                }
                else {
                    alert('notvalid');
                    obj.value = "";
                }
            }
        }
    </script>
</head>
<body>
    <form id="Form1" method="post" runat="server">
        <uc1:Headerctrl ID="Headerctrl1" runat="server"></uc1:Headerctrl>
        <table id="Table1" align="center">
            <tbody>
                <asp:Panel ID="pnlmainform" runat="server">
                    <tr>
                        <td align="center" colspan="2">
                            <strong><u>Add a Client Company</u></strong>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <asp:Label ID="lblErrorMsg" runat="server" CssClass="subRedHead" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <asp:Label ID="lblerr" runat="server" CssClass="subRedHead" Visible="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>* Company Name
                        </td>
                        <td>
                            <asp:TextBox ID="txtcompanyName" runat="server" CssClass="input" MaxLength="100"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>* Address
                        </td>
                        <td>
                            <asp:TextBox ID="txtaddress" runat="server" CssClass="input" MaxLength="250"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>* Billing Address
                        </td>
                        <td>
                            <asp:TextBox ID="txtbilladdress" runat="server" CssClass="input" MaxLength="250"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>* City
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlcity" runat="server" CssClass="input">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>* Bill To City
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlcityBillTo" runat="server" CssClass="input">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>* Source City
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlsourceCity" runat="server" CssClass="input">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>* Contract date
                        </td>
                        <td>
                            <asp:TextBox ID="txtContractDate" runat="server" CssClass="input"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Admin Head
                        </td>
                        <td>
                            <asp:TextBox ID="txtfname1" runat="server" CssClass="input" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <%--<TR>
							<TD>Contact 1 - Middle Name
							</TD>
							<TD>
								<asp:textbox id="txtmidname1" runat="server" CssClass="input" MaxLength="50"></asp:textbox></TD>
						</TR>--%>
                    <%--<TR>
							<TD>* Contact 1 - Last Name</TD>
							<TD>
								<asp:textbox id="txtlname1" runat="server" CssClass="input" MaxLength="50"></asp:textbox></TD>
						</TR>--%>
                    <tr>
                        <td>Admin Head - Phone
                        </td>
                        <td>
                            <asp:TextBox ID="txtphone1" runat="server" CssClass="input" MaxLength="20"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Admin Head � Email
                        </td>
                        <td>
                            <asp:TextBox ID="txtEmailAH" runat="server" CssClass="input" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Finance Manager
                        </td>
                        <td>
                            <asp:TextBox ID="txtfname2" runat="server" CssClass="input" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <%--<TR>
							<TD>Contact 2 - Middle Name
							</TD>
							<TD>
								<asp:textbox id="txtmnae2" runat="server" CssClass="input" MaxLength="50"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>Contact 2 - Last Name
							</TD>
							<TD>
								<asp:textbox id="txtlname2" runat="server" CssClass="input" MaxLength="50"></asp:textbox></TD>
						</TR>--%>
                    <tr>
                        <td>Finance Manager - Phone
                        </td>
                        <td>
                            <asp:TextBox ID="txtphone2" runat="server" CssClass="input" MaxLength="20"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Finance Manager � Email
                        </td>
                        <td>
                            <asp:TextBox ID="txtEmailFM" runat="server" CssClass="input" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>CFO
                        </td>
                        <td>
                            <asp:TextBox ID="txtfname3" runat="server" CssClass="input" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <%--<TR>
							<TD>Contact 3 - Middle Name
							</TD>
							<TD>
								<asp:textbox id="txtmname2" runat="server" CssClass="input" MaxLength="50"></asp:textbox></TD>
						</TR>
						<TR>
							<TD>Contact 3 - Last Name
							</TD>
							<TD>
								<asp:textbox id="txtlname3" runat="server" CssClass="input" MaxLength="50"></asp:textbox></TD>
						</TR>--%>
                    <tr>
                        <td>CFO � Phone
                        </td>
                        <td>
                            <asp:TextBox ID="txtphone3" runat="server" CssClass="input" MaxLength="20"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>CFO � Email
                        </td>
                        <td>
                            <asp:TextBox ID="txtEmailCFO" runat="server" CssClass="input" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Bill Submission Person
                        </td>
                        <td>
                            <asp:TextBox ID="txtBSP" runat="server" CssClass="input" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Bill Submission Person � Phone
                        </td>
                        <td>
                            <asp:TextBox ID="txtBSPhone" runat="server" CssClass="input" MaxLength="20"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Bill Submission Person � Email
                        </td>
                        <td>
                            <asp:TextBox ID="txtBSPEmail" runat="server" CssClass="input" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <%--<tr>
                        <td>Fax
                        </td>
                        <td>
                            <asp:TextBox ID="txtfax" runat="server" CssClass="input" MaxLength="20"></asp:TextBox>
                        </td>
                    </tr>--%>
                    <tr>
                        <td>* Email ID
                        </td>
                        <td>
                            <asp:TextBox ID="txtemail" runat="server" CssClass="input" MaxLength="100"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>* Mode Of Reservation
                        </td>
                        <td>
                            <asp:DropDownList ID="txtbookmode" runat="server" CssClass="input">
                                <asp:ListItem Text="" Value="" />
                                <asp:ListItem Text="Email" Value="Em" />
                                <asp:ListItem Text="Telephone" Value="Te" />
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>* User Name
                        </td>
                        <td>
                            <asp:TextBox ID="txtUserName" runat="server" CssClass="input" MaxLength="80"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>* Password
                        </td>
                        <td>
                            <asp:TextBox ID="txtPassword" runat="server" CssClass="input" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>* Company Type
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlcompanytype" runat="server" CssClass="input">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>* Credit Days
                        </td>
                        <td>
                            <asp:TextBox ID="txtCreditDays" runat="server" CssClass="input" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <%-- <tr>
                        <td>
                            * Average Monthly Business
                        </td>
                        <td>
                            <asp:TextBox ID="txtAverageMonthlyBusiness" runat="server" CssClass="input" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>--%>
                    <tr>
                        <td>* Discount %
                        </td>
                        <td>
                            <asp:DropDownList ID="ddldiscount" runat="server" CssClass="input">
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddldiscountdecimal" runat="server" CssClass="input">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>Display Discount % on Invoice?
                        </td>
                        <td>
                            <asp:CheckBox ID="chkDispDiscInv" runat="server" CssClass="input" Checked="True"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Apply GST?
                        </td>
                        <td>
                            <asp:CheckBox ID="chkApplyServiceTaxYN" runat="server" CssClass="input" Checked="True"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td>SEZ Client?
                        </td>
                        <td>
                            <asp:CheckBox ID="chkSEZClientYN" runat="server" CssClass="input" Checked="false"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td>SEZ Expiry date
                        </td>
                        <td>
                            <asp:TextBox ID="txtSEZExpiry" runat="server" CssClass="input"></asp:TextBox>
                        </td>
                    </tr>
                    <%--<tr>
                        <td>Apply DST?
                        </td>
                        <td>
                            <asp:CheckBox ID="chkApplyDSTYN" runat="server" CssClass="input" Enabled="false"
                                Checked="False"></asp:CheckBox>
                        </td>
                    </tr>--%>
                    <tr>
                        <td>Apply CC LIMIT?
                        </td>
                        <td>
                            <asp:CheckBox ID="chkApplyCCYN" runat="server" CssClass="input" Checked="False"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Send Credit Mail?
                        </td>
                        <td>
                            <asp:CheckBox ID="chkCreditMailYN" runat="server" CssClass="input" Checked="True"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Send Invoice Mail on Closure?
                        </td>
                        <td>
                            <asp:CheckBox ID="chkInvoiceMailonClosureYN" runat="server" CssClass="input" Checked="True"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Upload Email Required?
                        </td>
                        <td>
                            <asp:CheckBox ID="chkEmailRequiredYN" runat="server" CssClass="input" Checked="false"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td>VRF Required?
                        </td>
                        <td>
                            <asp:CheckBox ID="chkVRFRequiredYN" runat="server" CssClass="input" Checked="false"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td>* Payment Terms
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlpayment" runat="server" CssClass="input">
                                <asp:ListItem Text="" Selected="True" Value="" />
                                <asp:ListItem Text="Credit" Value="Cr" />
                                <asp:ListItem Text="Credit Card" Value="CC" />
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>* Credit Limit
                        </td>
                        <td>
                            <asp:TextBox ID="txtcclimit" runat="server" CssClass="input" MaxLength="100"></asp:TextBox>
                        </td>
                    </tr>
                    <%--<TR>
							<TD>Payment Credit Period
							</TD>
							<TD>
								<asp:textbox id="txtPaymentCP" runat="server" CssClass="input" MaxLength="100"></asp:textbox></TD>
						</TR>--%>
                    <tr>
                        <td>* Billing Basis
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlbillingbasis" runat="server" CssClass="input">
                                <asp:ListItem Text="" Selected="True" Value="" />
                                <asp:ListItem Text="Point To Point" Value="pp" />
                                <asp:ListItem Text="Garage To Garage" Value="gg" />
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <%--<tr>
                        <td>Service Tax #
                        </td>
                        <td>
                            <asp:TextBox ID="tstsvctax" runat="server" CssClass="input" MaxLength="20"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>* Airport Service Unit
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlairport" runat="server" CssClass="input">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>* Hotel Service Unit
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlhotel" runat="server" CssClass="input">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>* Corporate Service Unit
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlcorporate" runat="server" CssClass="input">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>* Self Drive Service Unit
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlselfdrive" runat="server" CssClass="input">
                            </asp:DropDownList>
                        </td>
                    </tr>--%>
                   <%-- <tr>
                        <td>Tariff Type
                        </td>
                        <td>
                            <asp:DropDownList ID="ddTariffType" runat="server" CssClass="input">
                                <asp:ListItem Text="National" Value="N" />
                                <asp:ListItem Text="Special" Value="S" />
                            </asp:DropDownList>
                        </td>
                    </tr>--%>
                    <tr>
                        <td>Credit Type
                        </td>
                        <td>
                            <asp:DropDownList ID="ddCreditType" runat="server" CssClass="input">
                                <asp:ListItem Text="Credit" Value="Credit" />
                                <asp:ListItem Text="Advance" Value="Advance" />
                                <asp:ListItem Text="Float" Value="Float" />
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>Apply fuel impact (Positive)
                        </td>
                        <td>
                            <asp:CheckBox ID="chkFuelSurchargeYN" onchange="FuelSurchargeEvent()" runat="server"
                                CssClass="input" Checked="False"></asp:CheckBox>
                        </td>
                    </tr>
                    <%--Added by Rahul on 04-Mar-2015--%>
                    <tr>
                        <td>Apply fuel impact (Negative)
                        <br />
                            <b>* Only Applied when "Apply fuel impact (Positive)" is applied</b>
                        </td>
                        <td>
                            <asp:CheckBox ID="chkFuelSurchargeYNNegative" onchange="FuelSurchargeEvent()" runat="server"
                                CssClass="input" Checked="False"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Fuel Surcharges Threshold %
                        </td>
                        <td>
                            <asp:TextBox ID="txtFuelSurchargesThreshold" runat="server" MaxLength="6" CssClass="input"
                                Style="width: 80px" Text="0"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>BTA Form Required
                        </td>
                        <td>
                            <asp:CheckBox ID="chkbta" runat="server" CssClass="input"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Apply Higher Extra Amount?
                        </td>
                        <td>
                            <asp:CheckBox ID="chkHigherExtraAmt" runat="server" CssClass="input" Checked="false"></asp:CheckBox>
                        </td>
                    </tr>
                        <%--<tr>
                            <td>Print On Duty Slip?
                            </td>
                            <td>
                                <asp:CheckBox ID="chkPrintDS" runat="server" CssClass="input" Checked="false" />
                            </td>
                        </tr>--%>
                    <%--<TR>
							<TD>Amex Card #
							</TD>
							<TD>
								<asp:textbox id="txtamex" runat="server" CssClass="input" MaxLength="20"></asp:textbox></TD>
						</TR>--%>
                    <tr>
                        <td valign="top">Remarks(<span class="shwText" id="shwMessage">0</span>/2000 chars)
                        </td>
                        <td>
                            <asp:TextBox ID="txtRemarks" onkeydown="showLength(this.form.txtRemarks,'document.all.shwMessage',2000)"
                                onkeyup="showLength(this.form.txtRemarks,'document.all.shwMessage',2000)" runat="server"
                                CssClass="input" MaxLength="2000" TextMode="MultiLine" Rows="3" Columns="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">Special Instructions
                        </td>
                        <td>
                            <asp:TextBox ID="txtSpecialInstructions" runat="server" CssClass="input" MaxLength="5000"
                                TextMode="MultiLine" Rows="3" Columns="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Mandatory Details Required
                        </td>
                        <td>
                            <asp:TextBox ID="txtdetail" runat="server" CssClass="input" MaxLength="20"></asp:TextBox>
                        </td>
                    </tr>
                    <%-- <tr>--%>
                    <tr>
                        <td style="height: 23px" valign="top">* Billing Period (In Days) From:
                        </td>
                        <td style="height: 23px">
                            <asp:TextBox ID="txtFrom" runat="server" CssClass="input" MaxLength="2" Width="48px"></asp:TextBox>&nbsp;
                        &nbsp;To &nbsp;
                        <asp:TextBox ID="txtTo" runat="server" CssClass="input" MaxLength="2" Width="48px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 18px" valign="top">* Billing&nbsp;Submition&nbsp;Days
                        </td>
                        <td style="height: 18px">
                            <asp:TextBox ID="txtSubDays" runat="server" CssClass="input" MaxLength="2" Width="48px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 24px" valign="top">Activation Status
                        </td>
                        <td style="height: 24px">
                            <asp:RadioButton ID="rdStatus1" runat="server" Text="Activate Now" GroupName="ActiveStatus"
                                Checked="true"></asp:RadioButton><asp:RadioButton ID="rdStatus2" runat="server" Text="Activate Later"
                                    GroupName="ActiveStatus"></asp:RadioButton>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 25px" valign="top">Activation Date
                        </td>
                        <td style="height: 18px">
                            <asp:TextBox ID="txtActiveDate" runat="server" CssClass="input"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 25px" valign="top">*Contract Start Date
                        </td>
                        <td style="height: 18px">
                            <asp:TextBox ID="txtContractStartDate" runat="server" CssClass="input"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 25px" valign="top">*Contract End Date
                        </td>
                        <td style="height: 18px">
                            <asp:TextBox ID="txtContractEndDate" runat="server" CssClass="input"></asp:TextBox>
                        </td>
                    </tr>
                    <!--	<TR>
							<TD style="HEIGHT: 25px" vAlign="top" colSpan="2">
								<TABLE style="WIDTH: 488px; HEIGHT: 158px">
									<TR>
										<TD>Contact Name</TD>
										<TD>Phone No.</TD>
										<TD>Email Id</TD>
									</TR>
									<TR>
										<TD><asp:textbox id="Textbox2" runat="server" CssClass="input" MaxLength="50"></asp:textbox></TD>
										<TD><asp:textbox id="Textbox3" runat="server" CssClass="input" MaxLength="20"></asp:textbox></TD>
										<TD><asp:textbox id="Textbox4" runat="server" CssClass="input" MaxLength="50"></asp:textbox></TD>
									</TR>
									<TR>
										<TD><asp:textbox id="Textbox5" runat="server" CssClass="input" MaxLength="50"></asp:textbox></TD>
										<TD><asp:textbox id="Textbox6" runat="server" CssClass="input" MaxLength="20"></asp:textbox></TD>
										<TD><asp:textbox id="Textbox7" runat="server" CssClass="input" MaxLength="50"></asp:textbox></TD>
									</TR>
									<TR>
										<TD><asp:textbox id="Textbox8" runat="server" CssClass="input" MaxLength="50"></asp:textbox></TD>
										<TD><asp:textbox id="Textbox9" runat="server" CssClass="input" MaxLength="20"></asp:textbox></TD>
										<TD><asp:textbox id="Textbox10" runat="server" CssClass="input" MaxLength="50"></asp:textbox></TD>
									</TR>
									<TR>
										<TD><asp:textbox id="Textbox11" runat="server" CssClass="input" MaxLength="50"></asp:textbox></TD>
										<TD><asp:textbox id="Textbox12" runat="server" CssClass="input" MaxLength="20"></asp:textbox></TD>
										<TD><asp:textbox id="Textbox13" runat="server" CssClass="input" MaxLength="50"></asp:textbox></TD>
									</TR>
									<TR>
										<TD><asp:textbox id="Textbox14" runat="server" CssClass="input" MaxLength="50"></asp:textbox></TD>
										<TD><asp:textbox id="Textbox15" runat="server" CssClass="input" MaxLength="20"></asp:textbox></TD>
										<TD><asp:textbox id="Textbox16" runat="server" CssClass="input" MaxLength="50"></asp:textbox></TD>
									</TR>
								</TABLE>
							</TD>
						</TR>-->
                    <tr>
                        <td valign="top" colspan="2">
                            <table style="width: 536px; height: 94px">
                                <tbody>
                                    <tr>
                                        <td style="width: 172px" colspan="2">
                                            <strong>Credit Limit Exhausting (%)</strong>
                                        </td>
                                    </tr>
                                    <!--	
										<TR>
											<TD style="WIDTH: 193px">* Credit Limit I&nbsp;(%)</TD>
											<td><asp:textbox id="txtCrdLimit1" runat="server" CssClass="input" MaxLength="3" Width="40px"></asp:textbox></td>
										</TR>
										<TR>
											<TD style="WIDTH: 193px; HEIGHT: 21px">* Credit Limit II&nbsp;(%)</TD>
											<td style="HEIGHT: 21px"><asp:textbox id="txtCrdLimit2" runat="server" CssClass="input" MaxLength="3" Width="40px"></asp:textbox></td>
										</TR>
										<TR>
											<TD style="WIDTH: 193px">* Credit Limit III&nbsp;(%)</TD>
											<td><asp:textbox id="txtCrdLimit3" runat="server" CssClass="input" MaxLength="3" Width="40px"></asp:textbox></td>
										</TR>-->
                                    <tr>
                                        <td style="width: 193px">* Credit Mail Limit&nbsp;(%)
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtCreditMailLimit" Text="80" Enabled="false" runat="server" CssClass="input"
                                                MaxLength="5" Width="40px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 193px">* Warning Limit&nbsp;(%)
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtwarningLimit" Text="90" Enabled="false" runat="server" CssClass="input"
                                                MaxLength="3" Width="40px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 193px; height: 21px">* Circuit breaker Limit&nbsp;(%)
                                        </td>
                                        <td style="height: 21px">
                                            <asp:TextBox Text="95" ID="txtCircuitbreaker" Enabled="false" runat="server" CssClass="input"
                                                MaxLength="3" Width="40px"></asp:TextBox>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>PAN No #
                        </td>
                        <td>
                            <asp:TextBox ID="txtPanno" runat="server" CssClass="input" MaxLength="20"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>TAN No #
                        </td>
                        <td>
                            <asp:TextBox ID="txtTanno" runat="server" CssClass="input" MaxLength="20"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Relationship Manager #
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlRelationshpMgr" runat="server" CssClass="input">
                            </asp:DropDownList>
                        </td>
                    </tr>
     <tr>
    <td>*Client Billing Type
    </td>
    <td>
         <asp:DropDownList ID="ddlClientBilling" runat="server" CssClass="input">
          
            <asp:ListItem Text="fornightly" Value="fornightly" />
            <asp:ListItem Text="monthly" Value="monthly" />
        </asp:DropDownList>
    </td>
</tr>
<tr>
    <td>Centralized Billing
    </td>
    <td>
        <asp:CheckBox ID="chkBilling" runat="server" CssClass="input" Checked="false"></asp:CheckBox>
    </td>
</tr>
<tr>
    <td style="height: 25px" valign="top">Centralized Billing Effective Date
    </td>
    <td style="height: 18px">
        <asp:TextBox ID="txtCentralizeEffectiveDate" runat="server" CssClass="input"></asp:TextBox>
    </td>
</tr>
                    <tr>
                        <td>Expected Avg. Monthly Revenue #
                        </td>
                        <td>
                            <asp:TextBox ID="txtAvgMonRev" Text="0" runat="server" CssClass="input" MaxLength="20"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Active
                        </td>
                        <td>
                            <%--<asp:CheckBox ID="chkactive" runat="server" CssClass="input" Checked="True"></asp:CheckBox>--%>
                            <asp:CheckBox ID="chkactive" Enabled="false" runat="server" CssClass="input" Checked="false"></asp:CheckBox>

                        </td>
                    </tr>

                    <tr>
                        <td>Enable Personal Trip</td>
                        <td>
                            <asp:CheckBox ID="enablePersonalTrip" runat="server" CssClass="input" /></td>
                    </tr>
                    <tr>
                        <td>* Company Group Entity
                        </td>
                        <td>
                            <asp:CheckBox ID="chk_Entity" runat="server" Text="Same as Company" />
                        </td>
                    </tr>
                    <tr>
                        <td>* Agreement Type
                        </td>
                        <td align="Left" colspan="3">
                            <asp:DropDownList ID="ddlAgreementType" runat="server">
                                <asp:ListItem Text="Standard Agreement" Value="Standard Agreement" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Non Standard Agreement" Value="Non Standard Agreement"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                     <tr>
                        <td>* Service Type
                        </td>
                        <td align="Left" colspan="3">
                            <asp:DropDownList ID="ddlServiceType" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>*Billing SOP File<b>:</b>
                        </td>
                        <td align="Left" colspan="3">
                            <asp:FileUpload ID="SopFileUpload" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>*E Billing option
                        </td>
                        <td align="Left" colspan="3">
                            <asp:DropDownList ID="ddlEbillingYN" runat="server">
                                <asp:ListItem Text="" Value=""></asp:ListItem>
                                <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>*CAM File<b>:</b>
                        </td>
                        <td align="Left" colspan="3">
                            <asp:FileUpload ID="fluCam" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>Agreement
                        </td>
                        <td>
                            <asp:FileUpload ID="FileUpload1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>Client POC
                        </td>
                        <td>
                            <asp:TextBox ID="txtclientPoc" runat="server" MaxLength="10"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <asp:Button ID="btnsubmit" runat="server" CssClass="button" Text="Submit" OnClientClick="return validation();"></asp:Button>&nbsp;&nbsp;
                        <asp:Button ID="btnreset" runat="server" CssClass="button" Text="Reset" CausesValidation="False"></asp:Button>
                        </td>
                    </tr>
                </asp:Panel>
                <asp:Panel ID="pnlconfirmation" runat="server" Visible="False">
                    <tr align="center">
                        <td colspan="2">
                            <asp:Label ID="lblMessage" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr align="center">
                        <td colspan="2">
                            <asp:HyperLink ID="hyplnkretry2" runat="server"></asp:HyperLink><br>
                            <br />
                        </td>
                    </tr>
                    <tr align="center">
                        <td colspan="2">
                            <asp:HyperLink ID="hyplnkretry3" runat="server" Target="_New"></asp:HyperLink><br>
                            <br />
                        </td>
                    </tr>
                    <tr align="center">
                        <td colspan="2">
                            <asp:HyperLink ID="hyplnkretry" runat="server"></asp:HyperLink>
                        </td>
                    </tr>
                </asp:Panel>
            </tbody>
        </table>
    </form>
</body>
</html>
