<%@ Page Language="vb" AutoEventWireup="false" Src="ServiceDetailMasterAdd.aspx.vb" Codebehind="ServiceDetailMasterAdd.aspx.vb" Inherits="ServiceDetailMasterAdd"%>
<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CarzonRent :: Internal software Control panel</title>
		<meta  content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta  content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript" >
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" >
		<script language="JavaScript" src="../utilityfunction.js"></script>
		<script language="JavaScript" src="../JScripts/Datefunc.js"></script>
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
		<script language="javascript">
		function validation()
		{
				if (document.forms[0].ddlServiceID.value== "" )
				{
					alert("Please select Service Name..")
					return false;	
				}
				
				if(isNaN(document.forms[0].TxtAmount.value))
				{
					alert("Rate should be numeric only.")
					return false;	
				}
			
		}		
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<uc1:Headerctrl id="Headerctrl1" runat="server"></uc1:Headerctrl>
			<TABLE id="Table1" align="center">
				<asp:panel id="pnlmainform" Runat="server">
					<TBODY>
						<TR>
							<TD align="center" colSpan="2"><STRONG><U>Add a Service Detail Master</U></STRONG></TD>
						</TR>
						<TR>
							<TD align="center" colSpan="2">&nbsp;&nbsp;</TD>
						</TR>
						<TR>
							<TD>* Service Name</TD>
							<TD><asp:DropDownList id="ddlServiceID" runat="server" CssClass="input"></asp:DropDownList></TD>
						</TR>
						<TR>
							<TD>* City</TD>
							<TD><asp:DropDownList id="ddlCityID" runat="server" CssClass="input"></asp:DropDownList></TD>
						</TR>
						<TR>
							<TD>* Car Category</TD>
							<TD><asp:DropDownList id="ddlCarCatID" runat="server" CssClass="input"></asp:DropDownList></TD>
						</TR>
						<TR>
							<TD>* Amount</TD>
							<TD><asp:textbox id="TxtAmount" runat="server" CssClass="input" MaxLength="50"></asp:textbox></TD>
						</TR>
							
						<TR>
							<TD>Active</TD>
							<TD><asp:checkbox id="chkactive" runat="server" CssClass="input" Checked="True"></asp:checkbox></TD>
						</TR>
						<TR>
							<TD align="center" colSpan="2">
				<asp:button id="btnsubmit" runat="server" CssClass="button" Text="Submit"></asp:button>&nbsp;&nbsp;
				<asp:button id="btnreset" runat="server" CssClass="button" Text="Reset" CausesValidation="False"></asp:button></TD>
						</TR>
				</asp:panel>
				<asp:panel id="pnlconfirmation" Runat="server" Visible="False">
					<TR align="center">
						<TD colSpan="2"><asp:Label id="lblErrorMsg" runat="server"></asp:Label></TD>
					</TR>
					<TR align="center">
						<TD colSpan="2"><asp:HyperLink id="hyplnkretry" runat="server"></asp:HyperLink></TD>
					</TR>

				</asp:panel></TBODY></TABLE>

		</form>
	</body>
</HTML>
