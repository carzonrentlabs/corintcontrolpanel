Imports commonutility
Imports System.Data
Imports System.text
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System
Public Class ServiceDetailMasterEdit
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Protected WithEvents ID1 As System.Web.UI.WebControls.Label
    Protected WithEvents ddlServiceID As System.Web.UI.WebControls.Label
    'Protected WithEvents ddlCarCatID As System.Web.UI.WebControls.Label
    Protected WithEvents ddlCarCatID As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblCityID As System.Web.UI.WebControls.DropDownList
    Protected WithEvents TxtAmount As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnsubmit As System.Web.UI.WebControls.Button
    Protected WithEvents chkActive As System.Web.UI.WebControls.CheckBox
    Protected WithEvents lblErrorMsg As System.Web.UI.WebControls.Label
    Protected WithEvents hyplnkretry As System.Web.UI.WebControls.HyperLink
    Protected WithEvents chk_Active As System.Web.UI.WebControls.CheckBox

    Protected WithEvents pnlmainform As System.Web.UI.WebControls.Panel
    Protected WithEvents pnlconfirmation As System.Web.UI.WebControls.Panel



    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btnsubmit.Attributes("onClick") = "return validation();"
        If Not Page.IsPostBack Then

            populateddl()
            If Trim("" & Request.QueryString("ID")) <> "" Then
                Dim intID As Integer
                intID = Request.QueryString("ID")
                If Not IsPostBack() Then
                    getvalue(intID)
                End If
            Else
                Response.Write("Bad request.")
                Response.End()
            End If
        End If
    End Sub

    Sub populateddl()
        Dim objAcessdata, objAcessdata1 As clsutility
        Dim dtrreader As SqlDataReader
        Dim accessdata As clsutility
        accessdata = New clsutility
        dtrreader = accessdata.funcGetSQLDataReader("select cityid,cityname from corintcitymaster where active=1 order by cityname")
        lblCityID.DataSource = dtrreader
        lblCityID.DataValueField = "cityid"
        lblCityID.DataTextField = "cityname"
        lblCityID.DataBind()
        lblCityID.Items.Insert(0, New ListItem("Any", 0))
        dtrreader.Close()

        objAcessdata = New clsutility
        ddlCarCatID.DataSource = objAcessdata.funcGetSQLDataReader("Select CarcatID, CarCatName from CORIntCarCatMaster Where Active = 1 order by CarCatName")
        ddlCarCatID.DataValueField = "CarcatID"
        ddlCarCatID.DataTextField = "CarCatName"
        ddlCarCatID.DataBind()
        ddlCarCatID.Items.Insert(0, New ListItem("Any", 0))
        objAcessdata.Dispose()

        'objAcessdata1 = New clsutility
        'ddlServiceID.DataSource = objAcessdata1.funcGetSQLDataReader("Select ServiceID, Description from CorIntServicesMaster Where Active = 1 order by Description")
        'ddlServiceID.DataValueField = "ServiceID"
        'ddlServiceID.DataTextField = "Description"
        'ddlServiceID.DataBind()
        'ddlServiceID.Items.Insert(0, New ListItem("Any", 0))
        'objAcessdata1.Dispose()
    End Sub

    Sub getvalue(ByVal intID As Integer)
        Dim strquery As String
        Dim StrUrl As StringBuilder
        Dim str As StringBuilder
        Dim dtrreader As SqlDataReader
        Dim accessdata As clsutility
        Dim createby As String = ""
        Dim ModifiedBy As String = ""

        accessdata = New clsutility

        strquery = "Exec Proc_CP_ServiceDetail " & intID & ",2"

        'response.write(strquery)
        'response.end()

        dtrreader = accessdata.funcGetSQLDataReader(strquery.ToString)
        'If dtrreader.HasRows Then
        While dtrreader.Read
            ID1.Text = dtrreader("SDetailID")

            ddlServiceID.Text = dtrreader("Description")
            'autoselec_ddl(ddlServiceID, dtrreader("ServiceID"))
            autoselec_ddl(lblCityID, dtrreader("CityID"))
            autoselec_ddl(ddlCarCatID, dtrreader("CarcatID"))
            TxtAmount.Text = dtrreader("Amount")

            If dtrreader("Active") Then
                chk_Active.Checked = True
            Else
                chk_Active.Checked = False
            End If
        End While
        'End If
    End Sub
    Function autoselec_ddl(ByVal ddlname As DropDownList, ByVal selectvalue As Int32)
        If Not selectvalue = "0" Then
            'If Not IsDBNull(selectvalue) Then
            ddlname.Items.FindByValue(selectvalue).Selected = True
        End If
    End Function
    Private Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        Dim MyConnection As SqlConnection
        MyConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("corConnectString"))
        Dim cmd As SqlCommand
        Dim Flag As Integer
        Dim intAmount As Integer

        If (TxtAmount.Text) = "" Then
            TxtAmount.Text = 0
        End If

        cmd = New SqlCommand("Proc_CP_EditServiceDetail", MyConnection)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@ID", Request.QueryString("ID"))
        cmd.Parameters.Add("@CityID", lblCityID.SelectedItem.Value)
        cmd.Parameters.Add("@CarCatID", ddlCarCatID.SelectedItem.Value)
        cmd.Parameters.Add("@Amount", TxtAmount.Text)
        cmd.Parameters.Add("@Active", get_YNvalue(chk_Active))
        cmd.Parameters.Add("@CreatedBy", Session("loggedin_user"))

        MyConnection.Open()
        Flag = cmd.ExecuteNonQuery()

        MyConnection.Close()
        lblErrorMsg.Visible = True
        'Response.Write("Flag=" & Flag)

        If Trim("" & Flag) = "0" Then

            pnlmainform.Visible = False
            pnlconfirmation.Visible = True

            lblErrorMsg.Text = "Unable to edit Service Detail Master!<br> The Service Detail Master ID is <b>" & Request.QueryString("id") & "</b>"
            hyplnkretry.Text = "Edit another Service Detail Master"
            hyplnkretry.NavigateUrl = "ServiceDetailMasterSearch.aspx"
        Else

            pnlmainform.Visible = False
            pnlconfirmation.Visible = True

            lblErrorMsg.Text = "You have edited the Service Detail Master <br> The Service Detail Master ID is <b>" & Request.QueryString("id") & "</b>"
            hyplnkretry.Text = "Edit another Service Detail Master"
            hyplnkretry.NavigateUrl = "ServiceDetailMasterSearch.aspx"
        End If
    End Sub
    Function get_YNvalue(ByVal chkbox As CheckBox) As Int32
        Dim returnval As Int32
        If chkbox.Checked Then
            returnval = 1
        Else
            returnval = 0
        End If
        Return returnval
    End Function
End Class
