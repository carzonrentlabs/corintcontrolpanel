<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ServiceDetailMasterEdit.aspx.vb" Inherits="ServiceDetailMasterEdit" Src="ServiceDetailMasterEdit.aspx.vb" %>
<%@ Register TagPrefix="uc1" TagName="Headerctrl" Src="../usercontrol/Headerctrl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CarzonRent :: Internal software Control panel</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="JavaScript" src="../utilityfunction.js"></script>
		<script language="JavaScript" src="../JScripts/Datefunc.js"></script>
		<LINK href="../HertzInt.css" type="text/css" rel="stylesheet">
		<script language="javascript">
		function validation()
		{
			if(isNaN(document.forms[0].TxtAmount.value))
				{
					alert("Rate should be numeric only.");
					return false;	
				}

		}		
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<blockquote>
		  <form id="Form1" method="post" runat="server">
		    <uc1:Headerctrl id="Headerctrl1" runat="server"></uc1:Headerctrl>
		    <TABLE id="Table1" align="center">
		      <asp:panel id="pnlmainform" Runat="server">
		        <TBODY>
		          <TR>
		            <TD align="center" colSpan="2"><STRONG><U>Edit a Service Detail Master</U></STRONG>		              </TD>
				    </TR>
		          <TR>
		            <TD align="center" colSpan="2">&nbsp;&nbsp;</TD>
				    </TR>
		            <TR>
		            <TD>ID</TD>
					  <TD><asp:Label ID="ID1" Runat="server"></asp:Label></td>
				    </TR>
					<TR>
						<TD>Service ID</TD>
						  <TD><asp:Label ID="ddlServiceID" Runat="server"></asp:Label></td>
				    </TR>
					<TR>
		            <TD>City</TD>
					  <TD><asp:DropDownList id="lblCityID" runat="server" CssClass="input"></asp:DropDownList></TD>
				    </TR>
		          <TR>
		            <TD>Car Category</TD>
					<TD><asp:DropDownList id="ddlCarCatID" runat="server" CssClass="input"></asp:DropDownList></TD>
				    </TR>
				  <TR>
		            <TD>Amount</TD>
				    <TD><asp:textbox id="TxtAmount" runat="server" value=""></asp:textbox>				      </TD>
				    </TR>
		         
		          <TR>
		            <TD>Active</TD>
					  <TD>
					      <asp:checkbox id="chk_active" runat="server" Checked="True"></asp:checkbox></TD>
				    </TR>
		          
		          
		          <TR>
		            <TD align="center" colSpan="2">
		              <asp:button id="btnsubmit" runat="server"  Text="Submit"></asp:button>&nbsp;&nbsp;
		              <asp:button id="btnreset" runat="server"  Text="Reset" CausesValidation="False"></asp:button></TD>
				    </TR>
	            </asp:panel>
		      <asp:panel id="pnlconfirmation" Runat="server" Visible="False">
		        <TR align="center">
		          <TD colSpan="2">
		            <asp:Label id="lblErrorMsg" runat="server"></asp:Label></TD>
				  </TR>
		        <TR align="center">
		          <TD colSpan="2">
		            <asp:HyperLink id="hyplnkretry" runat="server"></asp:HyperLink></TD>
				  </TR>
		        
		        </asp:panel></TBODY></TABLE>
		</form>
    </blockquote>
	</body>
</HTML>
